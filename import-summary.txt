ECLIPSE ANDROID PROJECT IMPORT SUMMARY
======================================

Ignored Files:
--------------
The following files were *not* copied into the new Gradle project; you
should evaluate whether these are still needed in your project and if
so manually move them:

* .DS_Store

Moved Files:
------------
Android Gradle projects use a different directory structure than ADT
Eclipse projects. Here's how the projects were restructured:

* AndroidManifest.xml => tuSDK/src/main/AndroidManifest.xml
* libs/TuSDKCore-2.4.0.jar => tuSDK/libs/TuSDKCore-2.4.0.jar
* libs/android-support-v7-recyclerview.jar => tuSDK/libs/android-support-v7-recyclerview.jar
* libs/armeabi-v7a/libtusdk-image.so => tuSDK/src/main/jniLibs/armeabi-v7a/libtusdk-image.so
* libs/armeabi-v7a/libtusdk-library.so => tuSDK/src/main/jniLibs/armeabi-v7a/libtusdk-library.so
* libs/armeabi/libtusdk-image.so => tuSDK/src/main/jniLibs/armeabi/libtusdk-image.so
* libs/armeabi/libtusdk-library.so => tuSDK/src/main/jniLibs/armeabi/libtusdk-library.so
* libs/universal-image-loader-1.9.4.jar => tuSDK/libs/universal-image-loader-1.9.4.jar
* res/ => tuSDK/src/main/res/
* src/ => tuSDK/src/main/java/

Next Steps:
-----------
You can now build the project. The Gradle project needs network
connectivity to download dependencies.

Bugs:
-----
If for some reason your project does not build, and you determine that
it is due to a bug or limitation of the Eclipse to Gradle importer,
please file a bug at http://b.android.com with category
Component-Tools.

(This import summary is for your information only, and can be deleted
after import once you are satisfied with the results.)
