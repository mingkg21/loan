/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */
package com.bk.android.code;

public final class R {
	public static final class anim {
		public static final int slide_in_from_bottom = 0x7f04000f;
		public static final int slide_in_from_top = 0x7f040010;
		public static final int slide_out_to_bottom = 0x7f040011;
		public static final int slide_out_to_top = 0x7f040012;
	}
	public static final class attr {
		public static final int activeColor = 0x7f01001e;
		public static final int activeType = 0x7f010023;
		public static final int allowSingleTap = 0x7f01002d;
		public static final int animateOnClick = 0x7f01002e;
		public static final int barColor = 0x7f010013;
		public static final int barLength = 0x7f01001a;
		public static final int barWidth = 0x7f010019;
		public static final int behindOffset = 0x7f010045;
		public static final int behindScrollScale = 0x7f010047;
		public static final int behindWidth = 0x7f010046;
		public static final int bottomOffset = 0x7f01002b;
		public static final int centered = 0x7f010010;
		public static final int circleColor = 0x7f010018;
		public static final int clipPadding = 0x7f01000b;
		public static final int column_count = 0x7f010000;
		public static final int column_count_landscape = 0x7f010002;
		public static final int column_count_portrait = 0x7f010001;
		public static final int content = 0x7f010029;
		public static final int contourColor = 0x7f01001b;
		public static final int contourSize = 0x7f01001c;
		public static final int customTypeface = 0x7f010027;
		public static final int delayMillis = 0x7f010017;
		public static final int fadeDegree = 0x7f01004d;
		public static final int fadeDelay = 0x7f010070;
		public static final int fadeEnabled = 0x7f01004c;
		public static final int fadeLength = 0x7f010071;
		public static final int fadeOut = 0x7f010021;
		public static final int fades = 0x7f01006f;
		public static final int fillColor = 0x7f010064;
		public static final int footerColor = 0x7f01000c;
		public static final int footerIndicatorHeight = 0x7f01006a;
		public static final int footerIndicatorStyle = 0x7f010069;
		public static final int footerIndicatorUnderlinePadding = 0x7f01006b;
		public static final int footerLineHeight = 0x7f01000d;
		public static final int footerPadding = 0x7f01006c;
		public static final int footerTriangleHeight = 0x7f010026;
		public static final int gapWidth = 0x7f010068;
		public static final int grid_paddingBottom = 0x7f010007;
		public static final int grid_paddingLeft = 0x7f010004;
		public static final int grid_paddingRight = 0x7f010005;
		public static final int grid_paddingTop = 0x7f010006;
		public static final int handle = 0x7f010028;
		public static final int inactiveColor = 0x7f01001f;
		public static final int inactiveType = 0x7f010022;
		public static final int item_margin = 0x7f010003;
		public static final int linePosition = 0x7f01006d;
		public static final int lineWidth = 0x7f010067;
		public static final int mode = 0x7f010042;
		public static final int orientation = 0x7f01002a;
		public static final int pageColor = 0x7f010065;
		public static final int ptrAdapterViewBackground = 0x7f01003f;
		public static final int ptrAnimationStyle = 0x7f01003b;
		public static final int ptrDrawable = 0x7f010035;
		public static final int ptrDrawableBottom = 0x7f010041;
		public static final int ptrDrawableEnd = 0x7f010037;
		public static final int ptrDrawableStart = 0x7f010036;
		public static final int ptrDrawableTop = 0x7f010040;
		public static final int ptrHeaderBackground = 0x7f010030;
		public static final int ptrHeaderSubTextColor = 0x7f010032;
		public static final int ptrHeaderTextAppearance = 0x7f010039;
		public static final int ptrHeaderTextColor = 0x7f010031;
		public static final int ptrListViewExtrasEnabled = 0x7f01003d;
		public static final int ptrMode = 0x7f010033;
		public static final int ptrOverScroll = 0x7f010038;
		public static final int ptrRefreshableViewBackground = 0x7f01002f;
		public static final int ptrRotateDrawableWhilePulling = 0x7f01003e;
		public static final int ptrScrollingWhileRefreshingEnabled = 0x7f01003c;
		public static final int ptrShowIndicator = 0x7f010034;
		public static final int ptrSubHeaderTextAppearance = 0x7f01003a;
		public static final int radius = 0x7f010008;
		public static final int rimColor = 0x7f010014;
		public static final int rimWidth = 0x7f010015;
		public static final int selectedBold = 0x7f01000e;
		public static final int selectedColor = 0x7f010011;
		public static final int selectedSize = 0x7f010025;
		public static final int selectorDrawable = 0x7f01004f;
		public static final int selectorEnabled = 0x7f01004e;
		public static final int shadowDrawable = 0x7f01004a;
		public static final int shadowWidth = 0x7f01004b;
		public static final int sidebuffer = 0x7f01001d;
		public static final int snap = 0x7f010024;
		public static final int spacing = 0x7f010020;
		public static final int spinSpeed = 0x7f010016;
		public static final int strokeColor = 0x7f010066;
		public static final int strokeWidth = 0x7f010062;
		public static final int swipeActionLeft = 0x7f010058;
		public static final int swipeActionRight = 0x7f010059;
		public static final int swipeAnimationTime = 0x7f010051;
		public static final int swipeBackView = 0x7f010056;
		public static final int swipeCloseAllItemsWhenMoveList = 0x7f010054;
		public static final int swipeDrawableChecked = 0x7f01005a;
		public static final int swipeDrawableUnchecked = 0x7f01005b;
		public static final int swipeFrontView = 0x7f010055;
		public static final int swipeMode = 0x7f010057;
		public static final int swipeOffsetLeft = 0x7f010052;
		public static final int swipeOffsetRight = 0x7f010053;
		public static final int swipeOpenOnLongPress = 0x7f010050;
		public static final int text = 0x7f010012;
		public static final int textColor = 0x7f010009;
		public static final int textSize = 0x7f01000a;
		public static final int titlePadding = 0x7f01000f;
		public static final int topOffset = 0x7f01002c;
		public static final int topPadding = 0x7f01006e;
		public static final int touchModeAbove = 0x7f010048;
		public static final int touchModeBehind = 0x7f010049;
		public static final int unselectedColor = 0x7f010063;
		public static final int viewAbove = 0x7f010043;
		public static final int viewBehind = 0x7f010044;
		public static final int vpiCirclePageIndicatorStyle = 0x7f01005c;
		public static final int vpiIconPageIndicatorStyle = 0x7f01005d;
		public static final int vpiLinePageIndicatorStyle = 0x7f01005e;
		public static final int vpiTabPageIndicatorStyle = 0x7f010060;
		public static final int vpiTitlePageIndicatorStyle = 0x7f01005f;
		public static final int vpiUnderlinePageIndicatorStyle = 0x7f010061;
	}
	public static final class bool {
		public static final int default_circle_indicator_centered = 0x7f080000;
		public static final int default_circle_indicator_snap = 0x7f080001;
		public static final int default_line_indicator_centered = 0x7f080002;
		public static final int default_title_indicator_selected_bold = 0x7f080003;
		public static final int default_underline_indicator_fades = 0x7f080004;
	}
	public static final class color {
		public static final int default_circle_indicator_fill_color = 0x7f070008;
		public static final int default_circle_indicator_page_color = 0x7f070009;
		public static final int default_circle_indicator_stroke_color = 0x7f07000a;
		public static final int default_line_indicator_selected_color = 0x7f07000b;
		public static final int default_line_indicator_unselected_color = 0x7f07000c;
		public static final int default_title_indicator_footer_color = 0x7f07000d;
		public static final int default_title_indicator_selected_color = 0x7f07000e;
		public static final int default_title_indicator_text_color = 0x7f07000f;
		public static final int default_underline_indicator_selected_color = 0x7f070010;
		public static final int vpi__background_holo_dark = 0x7f070000;
		public static final int vpi__background_holo_light = 0x7f070001;
		public static final int vpi__bright_foreground_disabled_holo_dark = 0x7f070004;
		public static final int vpi__bright_foreground_disabled_holo_light = 0x7f070005;
		public static final int vpi__bright_foreground_holo_dark = 0x7f070002;
		public static final int vpi__bright_foreground_holo_light = 0x7f070003;
		public static final int vpi__bright_foreground_inverse_holo_dark = 0x7f070006;
		public static final int vpi__bright_foreground_inverse_holo_light = 0x7f070007;
		public static final int vpi__dark_theme = 0x7f070037;
		public static final int vpi__light_theme = 0x7f070038;
	}
	public static final class dimen {
		public static final int default_circle_indicator_radius = 0x7f0a0000;
		public static final int default_circle_indicator_stroke_width = 0x7f0a0001;
		public static final int default_line_indicator_gap_width = 0x7f0a0003;
		public static final int default_line_indicator_line_width = 0x7f0a0002;
		public static final int default_line_indicator_stroke_width = 0x7f0a0004;
		public static final int default_title_indicator_clip_padding = 0x7f0a0005;
		public static final int default_title_indicator_footer_indicator_height = 0x7f0a0007;
		public static final int default_title_indicator_footer_indicator_underline_padding = 0x7f0a0008;
		public static final int default_title_indicator_footer_line_height = 0x7f0a0006;
		public static final int default_title_indicator_footer_padding = 0x7f0a0009;
		public static final int default_title_indicator_text_size = 0x7f0a000a;
		public static final int default_title_indicator_title_padding = 0x7f0a000b;
		public static final int default_title_indicator_top_padding = 0x7f0a000c;
	}
	public static final class drawable {
		public static final int vpi__tab_indicator = 0x7f0202a5;
		public static final int vpi__tab_selected_focused_holo = 0x7f0202a6;
		public static final int vpi__tab_selected_holo = 0x7f0202a7;
		public static final int vpi__tab_selected_pressed_holo = 0x7f0202a8;
		public static final int vpi__tab_unselected_focused_holo = 0x7f0202a9;
		public static final int vpi__tab_unselected_holo = 0x7f0202aa;
		public static final int vpi__tab_unselected_pressed_holo = 0x7f0202ab;
	}
	public static final class id {
		public static final int both = 0x7f060015;
		public static final int bottom = 0x7f060028;
		public static final int choice = 0x7f060023;
		public static final int disabled = 0x7f060016;
		public static final int dismiss = 0x7f060024;
		public static final int fill = 0x7f060013;
		public static final int flip = 0x7f06001c;
		public static final int fullscreen = 0x7f060020;
		public static final int left = 0x7f06001e;
		public static final int manualOnly = 0x7f060017;
		public static final int margin = 0x7f060021;
		public static final int none = 0x7f060022;
		public static final int pullDownFromTop = 0x7f060018;
		public static final int pullFromEnd = 0x7f060019;
		public static final int pullFromStart = 0x7f06001a;
		public static final int pullUpFromBottom = 0x7f06001b;
		public static final int reveal = 0x7f060025;
		public static final int right = 0x7f06001f;
		public static final int rotate = 0x7f06001d;
		public static final int selected_view = 0x7f060000;
		public static final int stroke = 0x7f060014;
		public static final int top = 0x7f060029;
		public static final int triangle = 0x7f060026;
		public static final int underline = 0x7f060027;
	}
	public static final class integer {
		public static final int default_circle_indicator_orientation = 0x7f090000;
		public static final int default_title_indicator_footer_indicator_style = 0x7f090001;
		public static final int default_title_indicator_line_position = 0x7f090002;
		public static final int default_underline_indicator_fade_delay = 0x7f090003;
		public static final int default_underline_indicator_fade_length = 0x7f090004;
	}
	public static final class style {
		public static final int TextAppearance_TabPageIndicator = 0x7f0b0003;
		public static final int Theme_PageIndicatorDefaults = 0x7f0b0000;
		public static final int Widget = 0x7f0b0001;
		public static final int Widget_IconPageIndicator = 0x7f0b0004;
		public static final int Widget_TabPageIndicator = 0x7f0b0002;
	}
	public static final class styleable {
		public static final int[] CircleFlowIndicator = { 0x7f010008, 0x7f010010, 0x7f01001e, 0x7f01001f, 0x7f010020, 0x7f010021, 0x7f010022, 0x7f010023, 0x7f010024 };
		public static final int CircleFlowIndicator_activeColor = 2;
		public static final int CircleFlowIndicator_activeType = 7;
		public static final int CircleFlowIndicator_centered = 1;
		public static final int CircleFlowIndicator_fadeOut = 5;
		public static final int CircleFlowIndicator_inactiveColor = 3;
		public static final int CircleFlowIndicator_inactiveType = 6;
		public static final int CircleFlowIndicator_radius = 0;
		public static final int CircleFlowIndicator_snap = 8;
		public static final int CircleFlowIndicator_spacing = 4;
		public static final int[] CirclePageIndicator = { 0x010100c4, 0x010100d4, 0x7f010008, 0x7f010010, 0x7f010024, 0x7f010062, 0x7f010064, 0x7f010065, 0x7f010066 };
		public static final int CirclePageIndicator_android_background = 1;
		public static final int CirclePageIndicator_android_orientation = 0;
		public static final int CirclePageIndicator_centered = 3;
		public static final int CirclePageIndicator_fillColor = 6;
		public static final int CirclePageIndicator_pageColor = 7;
		public static final int CirclePageIndicator_radius = 2;
		public static final int CirclePageIndicator_snap = 4;
		public static final int CirclePageIndicator_strokeColor = 8;
		public static final int CirclePageIndicator_strokeWidth = 5;
		public static final int[] LinePageIndicator = { 0x010100d4, 0x7f010010, 0x7f010011, 0x7f010062, 0x7f010063, 0x7f010067, 0x7f010068 };
		public static final int LinePageIndicator_android_background = 0;
		public static final int LinePageIndicator_centered = 1;
		public static final int LinePageIndicator_gapWidth = 6;
		public static final int LinePageIndicator_lineWidth = 5;
		public static final int LinePageIndicator_selectedColor = 2;
		public static final int LinePageIndicator_strokeWidth = 3;
		public static final int LinePageIndicator_unselectedColor = 4;
		public static final int[] ProgressWheel = { 0x7f010008, 0x7f010009, 0x7f01000a, 0x7f010012, 0x7f010013, 0x7f010014, 0x7f010015, 0x7f010016, 0x7f010017, 0x7f010018, 0x7f010019, 0x7f01001a, 0x7f01001b, 0x7f01001c };
		public static final int ProgressWheel_barColor = 4;
		public static final int ProgressWheel_barLength = 11;
		public static final int ProgressWheel_barWidth = 10;
		public static final int ProgressWheel_circleColor = 9;
		public static final int ProgressWheel_contourColor = 12;
		public static final int ProgressWheel_contourSize = 13;
		public static final int ProgressWheel_delayMillis = 8;
		public static final int ProgressWheel_radius = 0;
		public static final int ProgressWheel_rimColor = 5;
		public static final int ProgressWheel_rimWidth = 6;
		public static final int ProgressWheel_spinSpeed = 7;
		public static final int ProgressWheel_text = 3;
		public static final int ProgressWheel_textColor = 1;
		public static final int ProgressWheel_textSize = 2;
		public static final int[] PullToRefresh = { 0x7f01002f, 0x7f010030, 0x7f010031, 0x7f010032, 0x7f010033, 0x7f010034, 0x7f010035, 0x7f010036, 0x7f010037, 0x7f010038, 0x7f010039, 0x7f01003a, 0x7f01003b, 0x7f01003c, 0x7f01003d, 0x7f01003e, 0x7f01003f, 0x7f010040, 0x7f010041 };
		public static final int PullToRefresh_ptrAdapterViewBackground = 16;
		public static final int PullToRefresh_ptrAnimationStyle = 12;
		public static final int PullToRefresh_ptrDrawable = 6;
		public static final int PullToRefresh_ptrDrawableBottom = 18;
		public static final int PullToRefresh_ptrDrawableEnd = 8;
		public static final int PullToRefresh_ptrDrawableStart = 7;
		public static final int PullToRefresh_ptrDrawableTop = 17;
		public static final int PullToRefresh_ptrHeaderBackground = 1;
		public static final int PullToRefresh_ptrHeaderSubTextColor = 3;
		public static final int PullToRefresh_ptrHeaderTextAppearance = 10;
		public static final int PullToRefresh_ptrHeaderTextColor = 2;
		public static final int PullToRefresh_ptrListViewExtrasEnabled = 14;
		public static final int PullToRefresh_ptrMode = 4;
		public static final int PullToRefresh_ptrOverScroll = 9;
		public static final int PullToRefresh_ptrRefreshableViewBackground = 0;
		public static final int PullToRefresh_ptrRotateDrawableWhilePulling = 15;
		public static final int PullToRefresh_ptrScrollingWhileRefreshingEnabled = 13;
		public static final int PullToRefresh_ptrShowIndicator = 5;
		public static final int PullToRefresh_ptrSubHeaderTextAppearance = 11;
		public static final int[] SlidingDrawer = { 0x7f010028, 0x7f010029, 0x7f01002a, 0x7f01002b, 0x7f01002c, 0x7f01002d, 0x7f01002e };
		public static final int SlidingDrawer_allowSingleTap = 5;
		public static final int SlidingDrawer_animateOnClick = 6;
		public static final int SlidingDrawer_bottomOffset = 3;
		public static final int SlidingDrawer_content = 1;
		public static final int SlidingDrawer_handle = 0;
		public static final int SlidingDrawer_orientation = 2;
		public static final int SlidingDrawer_topOffset = 4;
		public static final int[] SlidingMenu = { 0x7f010042, 0x7f010043, 0x7f010044, 0x7f010045, 0x7f010046, 0x7f010047, 0x7f010048, 0x7f010049, 0x7f01004a, 0x7f01004b, 0x7f01004c, 0x7f01004d, 0x7f01004e, 0x7f01004f };
		public static final int SlidingMenu_behindOffset = 3;
		public static final int SlidingMenu_behindScrollScale = 5;
		public static final int SlidingMenu_behindWidth = 4;
		public static final int SlidingMenu_fadeDegree = 11;
		public static final int SlidingMenu_fadeEnabled = 10;
		public static final int SlidingMenu_mode = 0;
		public static final int SlidingMenu_selectorDrawable = 13;
		public static final int SlidingMenu_selectorEnabled = 12;
		public static final int SlidingMenu_shadowDrawable = 8;
		public static final int SlidingMenu_shadowWidth = 9;
		public static final int SlidingMenu_touchModeAbove = 6;
		public static final int SlidingMenu_touchModeBehind = 7;
		public static final int SlidingMenu_viewAbove = 1;
		public static final int SlidingMenu_viewBehind = 2;
		public static final int[] StaggeredGridView = { 0x7f010000, 0x7f010001, 0x7f010002, 0x7f010003, 0x7f010004, 0x7f010005, 0x7f010006, 0x7f010007 };
		public static final int StaggeredGridView_column_count = 0;
		public static final int StaggeredGridView_column_count_landscape = 2;
		public static final int StaggeredGridView_column_count_portrait = 1;
		public static final int StaggeredGridView_grid_paddingBottom = 7;
		public static final int StaggeredGridView_grid_paddingLeft = 4;
		public static final int StaggeredGridView_grid_paddingRight = 5;
		public static final int StaggeredGridView_grid_paddingTop = 6;
		public static final int StaggeredGridView_item_margin = 3;
		public static final int[] SwipeListView = { 0x7f010050, 0x7f010051, 0x7f010052, 0x7f010053, 0x7f010054, 0x7f010055, 0x7f010056, 0x7f010057, 0x7f010058, 0x7f010059, 0x7f01005a, 0x7f01005b };
		public static final int SwipeListView_swipeActionLeft = 8;
		public static final int SwipeListView_swipeActionRight = 9;
		public static final int SwipeListView_swipeAnimationTime = 1;
		public static final int SwipeListView_swipeBackView = 6;
		public static final int SwipeListView_swipeCloseAllItemsWhenMoveList = 4;
		public static final int SwipeListView_swipeDrawableChecked = 10;
		public static final int SwipeListView_swipeDrawableUnchecked = 11;
		public static final int SwipeListView_swipeFrontView = 5;
		public static final int SwipeListView_swipeMode = 7;
		public static final int SwipeListView_swipeOffsetLeft = 2;
		public static final int SwipeListView_swipeOffsetRight = 3;
		public static final int SwipeListView_swipeOpenOnLongPress = 0;
		public static final int[] TitleFlowIndicator = { 0x7f010009, 0x7f01000a, 0x7f01000b, 0x7f01000c, 0x7f01000d, 0x7f01000e, 0x7f01000f, 0x7f010011, 0x7f010025, 0x7f010026, 0x7f010027 };
		public static final int TitleFlowIndicator_clipPadding = 2;
		public static final int TitleFlowIndicator_customTypeface = 10;
		public static final int TitleFlowIndicator_footerColor = 3;
		public static final int TitleFlowIndicator_footerLineHeight = 4;
		public static final int TitleFlowIndicator_footerTriangleHeight = 9;
		public static final int TitleFlowIndicator_selectedBold = 5;
		public static final int TitleFlowIndicator_selectedColor = 7;
		public static final int TitleFlowIndicator_selectedSize = 8;
		public static final int TitleFlowIndicator_textColor = 0;
		public static final int TitleFlowIndicator_textSize = 1;
		public static final int TitleFlowIndicator_titlePadding = 6;
		public static final int[] TitlePageIndicator = { 0x01010095, 0x01010098, 0x010100d4, 0x7f01000b, 0x7f01000c, 0x7f01000d, 0x7f01000e, 0x7f01000f, 0x7f010011, 0x7f010069, 0x7f01006a, 0x7f01006b, 0x7f01006c, 0x7f01006d, 0x7f01006e };
		public static final int TitlePageIndicator_android_background = 2;
		public static final int TitlePageIndicator_android_textColor = 1;
		public static final int TitlePageIndicator_android_textSize = 0;
		public static final int TitlePageIndicator_clipPadding = 3;
		public static final int TitlePageIndicator_footerColor = 4;
		public static final int TitlePageIndicator_footerIndicatorHeight = 10;
		public static final int TitlePageIndicator_footerIndicatorStyle = 9;
		public static final int TitlePageIndicator_footerIndicatorUnderlinePadding = 11;
		public static final int TitlePageIndicator_footerLineHeight = 5;
		public static final int TitlePageIndicator_footerPadding = 12;
		public static final int TitlePageIndicator_linePosition = 13;
		public static final int TitlePageIndicator_selectedBold = 6;
		public static final int TitlePageIndicator_selectedColor = 8;
		public static final int TitlePageIndicator_titlePadding = 7;
		public static final int TitlePageIndicator_topPadding = 14;
		public static final int[] UnderlinePageIndicator = { 0x010100d4, 0x7f010011, 0x7f01006f, 0x7f010070, 0x7f010071 };
		public static final int UnderlinePageIndicator_android_background = 0;
		public static final int UnderlinePageIndicator_fadeDelay = 3;
		public static final int UnderlinePageIndicator_fadeLength = 4;
		public static final int UnderlinePageIndicator_fades = 2;
		public static final int UnderlinePageIndicator_selectedColor = 1;
		public static final int[] ViewFlow = { 0x7f01001d };
		public static final int ViewFlow_sidebuffer = 0;
		public static final int[] ViewPagerIndicator = { 0x7f01005c, 0x7f01005d, 0x7f01005e, 0x7f01005f, 0x7f010060, 0x7f010061 };
		public static final int ViewPagerIndicator_vpiCirclePageIndicatorStyle = 0;
		public static final int ViewPagerIndicator_vpiIconPageIndicatorStyle = 1;
		public static final int ViewPagerIndicator_vpiLinePageIndicatorStyle = 2;
		public static final int ViewPagerIndicator_vpiTabPageIndicatorStyle = 4;
		public static final int ViewPagerIndicator_vpiTitlePageIndicatorStyle = 3;
		public static final int ViewPagerIndicator_vpiUnderlinePageIndicatorStyle = 5;
	}
}
