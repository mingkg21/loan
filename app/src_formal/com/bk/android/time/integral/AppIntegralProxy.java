package com.bk.android.time.integral;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bk.android.app.BaseDialog;
import com.bk.android.time.app.App;
import com.bk.android.time.entity.UserInfo;
import com.bk.android.time.integral.IntegralManager.IntegralCallBack;
import com.bk.android.time.model.lightweight.UserInfoModel;
import com.bk.android.time.util.ToastUtil;
import com.bk.android.util.DimensionsUtil;

import java.util.ArrayList;

public class AppIntegralProxy{
	private static ArrayList<IntegralManager> sManagers = new ArrayList<IntegralManager>();
	
	static{
		sManagers.add(BKIntegralManager.getInstance());
	}

	public static boolean hasIntegral() {
		return !sManagers.isEmpty();
	}
	
	public static void onInitApp() {
		for (IntegralManager manager : sManagers) {
			try {
				manager.onInitApp();
			} catch (Exception e) {}
		}
	}
	
	public static void onEnterApp(Activity activity) {
		for (IntegralManager manager : sManagers) {
			if(manager instanceof AbsIntegralManager){
				((AbsIntegralManager) manager).setIntegralCallBack(mIntegralCallBack);
			}
			manager.onStart(activity);
		}
	}
	
	public static void onExitApp() {
		for (IntegralManager manager : sManagers) {
			try {
				manager.onDestory();
			} catch (Exception e) {}
		}
	}

	public static void handleIntentForWX(Intent intent) {
		for (IntegralManager manager : sManagers) {
			try {
				manager.handleIntent(intent);
			} catch (Exception e) {}
		}
	}

	public static boolean isNeedShowInteriorSplashAd(){
		return false;
	}
	
	public static void setInteriorSplashAd(final Activity activity,Intent intent,final RelativeLayout splashLayout,final Runnable gotoMainRunnable,final Runnable gotoMainCallback){
	}
	
	private static void warning(Context context,final OnClickListener l){
		final BaseDialog dialog = new BaseDialog(context);
		dialog.setGravity(Gravity.CENTER);
		dialog.setTitle("重要！！！注意事项：");
		LinearLayout contentView = new LinearLayout(context);
		// 实例化广告条
//		contentView.addView(new taly(context, taiy.FIT_SCREEN));

		contentView.setPadding(DimensionsUtil.DIPToPX(20), DimensionsUtil.DIPToPX(0), DimensionsUtil.DIPToPX(20), DimensionsUtil.DIPToPX(10));
		contentView.setOrientation(LinearLayout.VERTICAL);
		contentView.setGravity(Gravity.CENTER);
		TextView textView = new TextView(context);
		textView.setText("1：下载应用没有及时得到贝金，请尝试退出软件重进。" +
			"\n2：游戏应用可能会有扣费陷阱，请谨慎下载使用" +
			"\n如果不幸被扣费，请直接拨打运营商客服电话进行投诉恶意被扣费要回" +
			"\n3：有些机型或者手机本身的问题，无法获得贝金，此类问题暂时无法解决" +
			"\n4：有些手机本身以前安装过的应用，再次下载是没有贝金的" +
			"\n5：如果遇到除以上之外的其他问题，请联系所在的粉丝群管理员");
		contentView.addView(textView);
		Button button = new Button(context);
		button.setText("进入");
		button.setPadding(DimensionsUtil.DIPToPX(30), DimensionsUtil.DIPToPX(5), DimensionsUtil.DIPToPX(30), DimensionsUtil.DIPToPX(5));
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
				if(l != null){
					l.onClick(v);
				}
			}
		});
		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
		lp.topMargin = DimensionsUtil.DIPToPX(20);
		contentView.addView(button,lp);
		dialog.setContentView(contentView);
		dialog.show();
	}
	
	public static void gotoView(final Context context) {
		if(context instanceof Activity && !sManagers.isEmpty()){
			if(sManagers.size() == 1){
				gotoView(context, 0);
			}else{
				warning(context, new OnClickListener() {
					@Override
					public void onClick(View v) {
						final BaseDialog dialog = new BaseDialog(context);
						dialog.setGravity(Gravity.CENTER);
						ListView listView = new ListView(context);
						View view = new View(context);
						view.setLayoutParams(new AbsListView.LayoutParams(0, 0));
						listView.addHeaderView(view);
						listView.setCacheColorHint(Color.TRANSPARENT);
						listView.setDivider(new ColorDrawable(0xffbbbbbb));
						listView.setDividerHeight(DimensionsUtil.DIPToPX(1));
						dialog.setContentView(listView);
						dialog.setTitle("金矿列表");
						listView.setAdapter(new BaseAdapter() {
							@Override
							public View getView(int position, View convertView, ViewGroup parent) {
								TextView textView = new TextView(context);
								textView.setTextSize(TypedValue.COMPLEX_UNIT_PX,DimensionsUtil.DIPToPX(20));
								textView.setBackgroundResource(android.R.drawable.list_selector_background);
								textView.setText("贝金金矿  " + (position + 1));
								textView.setPadding(DimensionsUtil.DIPToPX(20), DimensionsUtil.DIPToPX(10), DimensionsUtil.DIPToPX(20), DimensionsUtil.DIPToPX(10));
								return textView;
							}
							
							@Override
							public long getItemId(int position) {
								return position;
							}
							
							@Override
							public Object getItem(int position) {
								return position;
							}
							
							@Override
							public int getCount() {
								return sManagers.size();
							}
						});
						listView.setOnItemClickListener(new OnItemClickListener() {
							@Override
							public void onItemClick(AdapterView<?> parent, View view,
									int position, long id) {
								gotoView(context, position - 1);
								dialog.dismiss();
							}
						});
						dialog.show();
					}
				});
			}
		}
	}
	
	public static void gotoView(Context context,int type) {
		if(type >= 0 && type < sManagers.size()){
			try {
				sManagers.get(type).gotoView(context);
			} catch (Exception e) {}
		}
	}

	public static void gotoView(Context context, String type) {
		try {
			for (IntegralManager integralManager : sManagers) {
				if (type.equals(integralManager.getType())) {
					integralManager.gotoView(context);
					break;
				}
			}
		} catch (Exception e) {}
	}
	
	private static IntegralCallBack mIntegralCallBack = new IntegralCallBack() {
		private UserInfoModel mUserInfoModel = new UserInfoModel();
		private Context mContext = App.getInstance(); 
		@Override
		public void onGetIntegralSuccess(int reward,int total,boolean isSyncCoin) {
			if(reward > 0){
				ToastUtil.showCoinToast(mContext, reward);
			}
			UserInfo userInfo = mUserInfoModel.getUserInfo();
			if(userInfo != null){
//				userInfo.setCoin(total);
				mUserInfoModel.setUserInfo(userInfo);
			}
		}

		@Override
		public void onGetIntegralError(int errorCode, String errorMessage) {
			if(errorMessage != null){
				switch (errorCode) {
					case AbsIntegralManager.ERR_NET_ERROR:// 网络不稳定
						ToastUtil.showToast(mContext, errorMessage);
						break;
					case AbsIntegralManager.ERR_DUPLICATE_ACTIVATION:// 重复激活
						Toast.makeText(mContext, errorMessage,Toast.LENGTH_SHORT).show();
						break;
			
					case AbsIntegralManager.ERR_ADVERTSING_EXPIRED:// 应用已下架
						Toast.makeText(mContext, errorMessage,Toast.LENGTH_SHORT).show();
						break;
			
					case AbsIntegralManager.ERR_ACTIVATION_FAILURE:// 激活失败
						Toast.makeText(mContext, errorMessage,Toast.LENGTH_SHORT).show();
						break;
				}
			}
		}
	};
}
