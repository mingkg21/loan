package com.bk.android.time.integral;

import java.io.Serializable;
import java.util.HashMap;

import android.content.Context;

import com.bk.android.data.net.RequestData;
import com.bk.android.time.data.request.AbsNetDataRequest;
import com.bk.android.time.entity.BaseEntity;
import com.bk.android.time.entity.SimpleData;
import com.bk.android.time.integral.BKIntegralLogUtil;

public class SubmitLogRequest extends AbsNetDataRequest {
	private static final long serialVersionUID = 3384249049278505037L;

	public SubmitLogRequest() {
	}

	@Override
	protected boolean isNeedCheckLogin() {
		return false;
	}

	@Override
	protected boolean isUserRelevance() {
		return false;
	}

	@Override
	protected String createDataKey() {
		return createKey(this);
	}

	@Override
	protected Serializable onRunTask(Context context) {
		synchronized (this) {
			StringBuffer log = new StringBuffer();
			HashMap<String, String> sendCompositeData = new HashMap<String, String>();
			String function = "sub_integral_log";
			String bkLog = BKIntegralLogUtil.getLog();
			if(bkLog != null){
				log.append("[");
				if(bkLog != null){
					log.append(bkLog);
					log.append(",");
				}
				log.append("]");
			}
			if(log.length() > 0){
				sendCompositeData.put("params",log.toString());
				sendCompositeData.put("function",function);
				return connection(new RequestData(RequestData.REQUEST_METHOD_POST,sendCompositeData,"submiterror"),SimpleData.class);
			}else{
				return new SimpleData().setResultCode(SimpleData.CODE_SUCCESS);
			}
		}
	}
}
