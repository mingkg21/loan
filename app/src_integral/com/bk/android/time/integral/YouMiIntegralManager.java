package com.bk.android.time.integral;

import android.app.Activity;
import android.app.ActivityManager.RunningTaskInfo;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.bk.android.dao.DBKeyValueProviderProxy;
import com.bk.android.time.app.App;
import com.bk.android.time.data.UserData;
import com.bk.android.time.data.dao.DBPreferencesProvider;
import com.bk.android.time.ui.activiy.MainActivity;
import com.bk.android.time.weixin.WXAPIManager;
import com.bk.android.util.ActivityTaskUtil;
import com.bk.android.util.AppUtil;
import com.bk.android.util.LogUtil;

import xm.w.ts.AdManager;
import xm.w.ts.os.OffersManager;

/** 有米
 *
 */
public class YouMiIntegralManager extends AbsIntegralManager {
	private static YouMiIntegralManager instance;

	public static YouMiIntegralManager getInstance() {
		if (instance == null) {
			instance = new YouMiIntegralManager();
		}
		return instance;
	}

	private boolean isInit;
	private boolean isExit;
	
	YouMiIntegralManager() {
		super(BabyMoneyAddRequest.TYPE_YOU_MI);
	}
	
	private void init(){
		if(isInit){
			return;
		}
		isInit = true;
		YMIntegralLogUtil.onManagerStart();

		//请务必在应用第一个 Activity（启动的第一个类）的 onCreate 中调用以下代码
		//appId 和 appSecret 分别为应用的发布 ID 和密钥，由有米后台自动生成，通过在有米后台 > 应用详细信息 可以获得。
		//isTestModel : 是否开启测试模式，true 为是，false 为否。（上传有米审核及发布到市场版本，请设置为 false）
		//isEnableYoumiLog: 是否开启有米的Log输出，默认为开启状态
		//上传到有米主站进行审核时，务必开启有米的Log，这样才能保证通过审核
		//开发者发布apk到各大市场的时候，强烈建议关闭有米的Log
		AdManager.getInstance(App.getInstance()).init("c247932bdde74715", "463b8fbdcf582b13", false, false);

		// userid 不能为空 或者 空串,否则设置无效, 字符串长度必须要小于50
		OffersManager.getInstance(App.getInstance()).setCustomUserId(getUserId());
		// 有米Android SDK v4.10之后的sdk还需要配置下面代码，以告诉sdk使用了服务器回调
		OffersManager.getInstance(App.getInstance()).setUsingServerCallBack(true);

		OffersManager.getInstance(getContext()).onAppLaunch();

//		// 设置积分墙列表页标题文字
//		OffersBrowserConfig.getInstance(App.getInstance()).setBrowserTitleText(String title);
//		// 设置积分墙标题背景颜色
//		OffersBrowserConfig.getInstance(App.getInstance()).setBrowserTitleBackgroundColor(int color);
//		// 设置积分余额区域是否显示
//		// true ：显示（默认值）
//		// false：不显示
//		OffersBrowserConfig.getInstance(App.getInstance()).setPointsLayoutVisibility(boolean flag);

		// 只有返回true，即成功注册，才能正常使用后续功能
		boolean isRegisterSuccess = OffersManager.getInstance(App.getInstance()).registerToWx(WXAPIManager.APP_ID);

		YMIntegralLogUtil.onInit((int) 0f,isInit());
		if(!isInit()){
			setInit();
		}
	}
	
	@Override
	public void onStart(Activity activity) {
		isExit = false;
		init();
	}
	
	@Override
	public void onDestory() {
		isExit = true;
		if(!isInit){
			return;
		}
		isInit = false;

		OffersManager.getInstance(App.getInstance()).onAppExit();

		YMIntegralLogUtil.onManagerDestory();
	}

	private String getUserId() {
		String uid = UserData.getUserId();
		if (TextUtils.isEmpty(uid)) {
			uid = AppUtil.getDeviceId(App.getInstance());
		}
		return uid;
	}

	@Override
	protected void onSubmitSingleRewardFail(String key, int reward) {
		LogUtil.i(TAG,"initSyncFail reward="+reward+" balance="+0);
		YMIntegralLogUtil.onSubmitSingleRewardFail(reward,0);
	}

	@Override
	protected void onSubmitRewardSuccess(String key, int reward, int total) {
		int balance = 0;
		if (reward > 0){
			LogUtil.i(TAG,"SubmitRewardSuccess reward="+reward+" balance="+balance);
		}
		YMIntegralLogUtil.onSubmitRewardSuccess(reward,total,balance);
	}

	public void gotoView(Context context) {
		OffersManager.getInstance(context).showOffersWall();

		YMIntegralLogUtil.onManagerGotoView();
	}

	private static DBKeyValueProviderProxy getProxy(){
		return DBPreferencesProvider.getProxy();
	}
	
	public void setInit() {
		getProxy().putBoolean("YouMiIntegralManagerInit", true, "YouMiIntegralManagerType");
	}
	
	public boolean isInit() {
		return getProxy().hasData("YouMiIntegralManagerInit", "YouMiIntegralManagerType");
	}

	@Override
	public String getName() {
		return "贝金金矿 ";
	}

	@Override
	public void onInitApp() {
		App.getHandler().post(new Runnable() {
			@Override
			public void run() {
				RunningTaskInfo runningTaskInfo = ActivityTaskUtil.getTopActivityTask(getContext());
				if(runningTaskInfo == null || isExit){
					return;
				}
				if(runningTaskInfo.topActivity.getPackageName().equals(getContext().getPackageName())){
					if(runningTaskInfo.topActivity.getClassName().equals(OffersManager.class.getName())){
						Intent intent = new Intent(getContext(),MainActivity.class);
						intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						intent.putExtra(MainActivity.EXTRA_CURRENT_ID, MainActivity.ID_MY);
						getContext().startActivity(intent);
						gotoView(getContext());
					}
				}else{
					App.getHandler().postDelayed(this, 500);
				}
			}
		});
	}
}