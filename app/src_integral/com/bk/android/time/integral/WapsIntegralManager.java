package com.bk.android.time.integral;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;

import com.bk.android.time.app.App;
import com.bk.android.time.data.UserData;
import com.bk.android.util.AppUtil;
import com.youyou.AppConnect;

/** 万普
 *
 */
public class WapsIntegralManager extends AbsIntegralManager {
	private static WapsIntegralManager instance;

	public static WapsIntegralManager getInstance(){
		if(instance == null){
			instance = new WapsIntegralManager();
		}
		return instance;
	}

	private boolean isStart;

	WapsIntegralManager(){
		super(BabyMoneyAddRequest.TYPE_WAPS);
	}

	@Override
	public void onStart(Activity activity) {
		if(isStart){
			return;
		}
		isStart = true;

		AppConnect.getInstance("1de4248d939ae541326f7cb3c96d4995", AppUtil.getUMengChannel(App.getInstance()), activity);
	}

	@Override
	public void onDestory() {
		if(!isStart){
			return;
		}
		isStart = false;
		AppConnect.getInstance(getContext()).close();
	}

	private String getUserId() {
		String uid = UserData.getUserId();
		if (TextUtils.isEmpty(uid)) {
			uid = AppUtil.getDeviceId(App.getInstance());
		}
		return uid;
	}

	@Override
	public void gotoView(Context context) {
		AppConnect.getInstance(context).showOffers(context, getUserId());
	}
	
	@Override
	public String getName() {
		return "贝金金矿 ";
	}

	@Override
	public void onInitApp() {
	}
}
