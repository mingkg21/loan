//package com.bk.android.time.integral;
//
//import android.app.Activity;
//import android.content.Context;
//
//import com.bk.android.time.app.App;
//import com.bk.android.time.model.common.CommonDialogViewModel;
//import com.bk.android.util.AppUtil;
//
//import cn.dow.android.DOW;
//
//public class DomobIntegralManager extends AbsIntegralManager {
//	private static DomobIntegralManager instance;
//
//	private static final String PUBLISHID = "96ZJ2+FAzfiUrwTPA+";
//
//	private boolean isStart;
//	private CommonDialogViewModel mDialog;
//
//	DomobIntegralManager() {
//		super(BabyMoneyAddRequest.TYPE_DOMOB);
//	}
//
//	public static DomobIntegralManager getInstance() {
//		if (instance == null) {
//			instance = new DomobIntegralManager();
//		}
//		return instance;
//	}
//
//	@Override
//	public void onStart(Activity activity) {}
//
//	private void init(Context context){
//		if(isStart){
//			return;
//		}
//		isStart = true;
//		final Activity activity = (Activity) context;
//		DOW.getInstance(activity).init(AppUtil.getDeviceId(App.getInstance()));
//
//		DMIntegralLogUtil.onManagerStart();
//	}
//
//	@Override
//	public void onDestory() {
//		if(!isStart){
//			return;
//		}
//		isStart = false;
//		DMIntegralLogUtil.onManagerDestory();
//	}
//
//	@Override
//	public void gotoView(Context context) {
//		boolean isStarted = isStart && mDialog == null;
//		init(context);
//		if(!isStart){
//			return;
//		}
//		DOW.getInstance(getContext()).setUserId(AppUtil.getDeviceId(App.getInstance()));
//		if(isStarted){
//			DOW.getInstance(getContext()).show(context);
//		}
//		DMIntegralLogUtil.onManagerGotoView();
//	}
//
//	@Override
//	public String getName() {
//		return "贝币金矿 ";
//	}
//
//	@Override
//	public void onInitApp() {
//	}
//}
