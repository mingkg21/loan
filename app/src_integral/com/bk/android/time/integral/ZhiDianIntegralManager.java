//package com.bk.android.time.integral;
//
//import android.app.Activity;
//import android.content.Context;
//
//import com.bk.android.time.app.App;
//import com.bk.android.time.data.UserData;
//import com.zhidian.wall.AdManager;
//import com.zhidian.wall.PointsChangeNotify;
//import com.zhidian.wall.manager.PointsChangeManager;
//
//public class ZhiDianIntegralManager extends AbsIntegralManager {
//	private static ZhiDianIntegralManager instance;
//
//	public static ZhiDianIntegralManager getInstance(){
//		if(instance == null){
//			instance = new ZhiDianIntegralManager();
//		}
//		return instance;
//	}
//
//	private boolean isStart;
//
//	ZhiDianIntegralManager() {
//		super(BabyMoneyAddRequest.TYPE_ZHIDIAN);
//	}
//
//	@Override
//	public void onInitApp() {}
//
//	@Override
//	public void onStart(Activity activity) {
//		if(isStart){
//			return;
//		}
//		isStart = true;
//		AdManager.getInstance(App.getInstance()).init("F99CEFE77D5EBA1CFEDA45DBC3A32745", UserData.getUserId());
//		AdManager.getInstance(App.getInstance()).setCustomParam(UserData.getUserId());
//		PointsChangeManager.getinstance().setPointChangeNotify(new PointsChangeNotify() {
//			@Override
//			public void onPointBalanceChange(int point) {
//				submitBalance(point);
//			}
//		});
//	}
//
//	@Override
//	public void onDestory() {
//		if(!isStart){
//			return;
//		}
//		isStart = false;
//		AdManager.onDestroy();
//	}
//
//	@Override
//	public void gotoView(Context context) {
//		AdManager.getInstance(App.getInstance()).init("F99CEFE77D5EBA1CFEDA45DBC3A32745", UserData.getUserId());
//		AdManager.getInstance(App.getInstance()).setCustomParam(UserData.getUserId());
//		AdManager.getInstance(App.getInstance()).queryPoint();
//		AdManager.getInstance(context).showWall();
//	}
//
//	@Override
//	public String getName() {
//		return "贝金金矿 ";
//	}
//}
