package com.bk.android.time.integral;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;

import com.bk.android.time.app.App;
import com.bk.android.time.data.UserData;
import com.bk.android.util.AppUtil;
import com.yql.dr.sdk.DRSdk;

/** 点入
 *
 */
public class DianruIntegralManager extends AbsIntegralManager {
	private static DianruIntegralManager instance;

	public static DianruIntegralManager getInstance(){
		if(instance == null){
			instance = new DianruIntegralManager();
		}
		return instance;
	}

	private boolean isStart;

	DianruIntegralManager(){
		super(BabyMoneyAddRequest.TYPE_DIANRU);
	}

	@Override
	public void onStart(Activity activity) {
		if(isStart){
			return;
		}
		isStart = true;

		DRSdk.initialize(activity, false, AppUtil.getDeviceId(App.getInstance()));
		DRSdk.setUserId(getUserId());
	}

	@Override
	public void onDestory() {
		if(!isStart){
			return;
		}
		isStart = false;
	}

	private String getUserId() {
		String uid = UserData.getUserId();
		if (TextUtils.isEmpty(uid)) {
			uid = AppUtil.getDeviceId(App.getInstance());
		}
		return uid;
	}

	@Override
	public void gotoView(Context context) {
		DRSdk.setUserId(getUserId());
		DRSdk.showOfferWall(context, DRSdk.DR_OFFER);
	}
	
	@Override
	public String getName() {
		return "贝金金矿 ";
	}

	@Override
	public void onInitApp() {
	}
}
