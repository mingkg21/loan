package com.bk.android.time.integral;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.bk.android.time.app.App;
import com.bk.android.time.data.UserData;
import com.bk.android.util.AppUtil;

import xm.w.ts.os.OffersManager;

/** 有米分享
 *
 */
public class YouMiShareIntegralManager extends AbsIntegralManager {

	private static YouMiShareIntegralManager instance;

	public static YouMiShareIntegralManager getInstance(){
		if(instance == null){
			instance = new YouMiShareIntegralManager();
		}
		return instance;
	}

	private boolean isStart;

	YouMiShareIntegralManager(){
		super(BabyMoneyAddRequest.TYPE_YOU_MI_SHARE);
	}

	@Override
	public void onStart(Activity activity) {
		if(isStart){
			return;
		}
		isStart = true;
	}

	@Override
	public void onDestory() {
		if(!isStart){
			return;
		}
		isStart = false;
	}

	@Override
	public void handleIntent(Intent intent) {
		super.handleIntent(intent);
		OffersManager.getInstance(App.getInstance()).handleIntent(intent);
	}

	private String getUserId() {
		String uid = UserData.getUserId();
		if (TextUtils.isEmpty(uid)) {
			uid = AppUtil.getDeviceId(App.getInstance());
		}
		return uid;
	}

	@Override
	public void gotoView(Context context) {
		OffersManager.getInstance(context).showShareWall();
	}
	
	@Override
	public String getName() {
		return "贝金金矿 ";
	}

	@Override
	public void onInitApp() {
	}
}
