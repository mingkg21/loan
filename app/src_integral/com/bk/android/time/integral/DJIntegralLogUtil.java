package com.bk.android.time.integral;

import java.util.ArrayList;
import java.util.List;

import android.app.ActivityManager.RunningAppProcessInfo;

import com.bk.android.dao.DBKeyValueProviderProxy;
import com.bk.android.time.app.App;
import com.bk.android.time.data.dao.DBPreferencesProvider;
import com.bk.android.util.AppUtil;
import com.bk.android.util.FormatUtil;

public class DJIntegralLogUtil{
	private static final String PREFERENCE_TYPE_DJ_INTEGRAL_LOG = "PREFERENCE_TYPE_DJ_INTEGRAL_LOG";
	
	private static DBKeyValueProviderProxy getProxy(){
		return DBPreferencesProvider.getProxy();
	}
	
	private DJIntegralLogUtil(){};

	public static String getLog() {
		StringBuffer log = new StringBuffer();
		ArrayList<String[]> mKeyValues = getProxy().getAllKeyValueByType(PREFERENCE_TYPE_DJ_INTEGRAL_LOG);
		if(mKeyValues == null){
			return null;
		}
		log.append("{");
		for (int i = 0;i < mKeyValues.size();i++) {
			String[] keyValue = mKeyValues.get(i);
			log.append("'");
			log.append(keyValue[0]);
			log.append("':'");
			log.append(keyValue[1]);
			log.append("'");
			if(i < mKeyValues.size() - 1){
				log.append(",");
			}
		}
		log.append("}");
		return log.toString();
	}

	public static void onManagerStart() {
		setStartItem();
		StringBuffer recentStr = new StringBuffer();
		List<RunningAppProcessInfo> processInfos = AppUtil.getRunningAppProcesses(App.getInstance());
		if(processInfos != null){
			for (RunningAppProcessInfo processInfo : processInfos) {
				if(processInfo != null){
					recentStr.append("processName="+processInfo.processName + "_importance=" + processInfo.importance);
					recentStr.append("\n");
				}
			}
		}
		putAction("onManagerStart","__recentStr_"+recentStr);
	}

	public static void onManagerDestory() {
		clear();
		putAction("onManagerDestory");
	}

	public static void onManagerGotoView() {
		putAction("onManagerGotoView","--------------------------");
	}
	
	public static void putAction(String name){
		putAction(name," ");
	}
	
	public static void putAction(String name,String data){
		if("interiorTest".equals(AppUtil.getUMengChannel(App.getInstance()))){
			getProxy().putString(name + "_" + FormatUtil.formatSimpleDate("yyyy_MM_dd_HH:mm:ss", System.currentTimeMillis()), data, PREFERENCE_TYPE_DJ_INTEGRAL_LOG);
		}
	}
	
	public static void setStartItem(){
		if("interiorTest".equals(AppUtil.getUMengChannel(App.getInstance())) && !getProxy().hasData("time", PREFERENCE_TYPE_DJ_INTEGRAL_LOG)){
			getProxy().putLong("time", System.currentTimeMillis(), PREFERENCE_TYPE_DJ_INTEGRAL_LOG);
		}
	}
	
	public static long getStartItem(){
		if("interiorTest".equals(AppUtil.getUMengChannel(App.getInstance()))){
			return getProxy().getLong("time", PREFERENCE_TYPE_DJ_INTEGRAL_LOG,0);
		}
		return 0;
	}
	
	private static void clear(){
		long time = System.currentTimeMillis() - getStartItem();
		if(time < 0 || time > 3600000 * 24){
			getProxy().clear(PREFERENCE_TYPE_DJ_INTEGRAL_LOG);
		}
	}

	public static void onGetBalance(int balance) {
		putAction("onGetBalance","balance_"+balance);
	}

	public static void submitReward(int reward) {
		putAction("submitReward","reward_"+reward);
	}

	public static void onGetBalanceError(String errorMessage) {
		putAction("onGetBalanceError","errorMessage_"+errorMessage);
	}

	public static void submitRewardError(int errorCode, String errorMessage) {
		putAction("submitRewardError","errorCode_"+errorCode+"__errorMessage_"+errorMessage);
	}

	public static void onSubmitRewardSuccess(int reward, int total) {
		putAction("onSubmitRewardSuccess","reward_"+reward+"__total_"+total);
	}
}
