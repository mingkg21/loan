package com.bk.android.time.weixin;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

/**
 * @author mingkg21
 * @email mingkg21@gmail.com
 * @date 2014年9月17日
 *
 */
public class UserInfo implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 965733298531504248L;
	@SerializedName("openid")
	private String openId;
	@SerializedName("nickname")
	private String nickname;
	@SerializedName("sex")
	private int sex;
	@SerializedName("province")
	private String province;
	@SerializedName("city")
	private String city;
	@SerializedName("country")
	private String country;
	@SerializedName("headimgurl")
	private String headImgUrl;
	@SerializedName("unionid")
	private String unionId;
	
	public String getOpenId() {
		return openId;
	}
	public void setOpenId(String openId) {
		this.openId = openId;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public int getSex() {
		return sex;
	}
	public void setSex(int sex) {
		this.sex = sex;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getHeadImgUrl() {
		return headImgUrl;
	}
	public void setHeadImgUrl(String headImgUrl) {
		this.headImgUrl = headImgUrl;
	}
	public String getUnionId() {
		return unionId;
	}
	public void setUnionId(String unionId) {
		this.unionId = unionId;
	}

}
