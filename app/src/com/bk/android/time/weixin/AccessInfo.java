package com.bk.android.time.weixin;

import com.google.gson.annotations.SerializedName;

/**
 * @author mingkg21
 * @email mingkg21@gmail.com
 * @date 2014年9月17日
 *
 */
public class AccessInfo {
	
	@SerializedName("access_token")
	private String accessToken;
	@SerializedName("expires_in")
	private int expiresIn;
	@SerializedName("refresh_token")
	private String refreshToken;
	@SerializedName("openid")
	private String openId;
	@SerializedName("scope")
	private String scope;
	
	public String getAccessToken() {
		return accessToken;
	}
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	public int getExpiresIn() {
		return expiresIn;
	}
	public void setExpiresIn(int expiresIn) {
		this.expiresIn = expiresIn;
	}
	public String getRefreshToken() {
		return refreshToken;
	}
	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}
	public String getOpenId() {
		return openId;
	}
	public void setOpenId(String openId) {
		this.openId = openId;
	}
	public String getScope() {
		return scope;
	}
	public void setScope(String scope) {
		this.scope = scope;
	}

}
