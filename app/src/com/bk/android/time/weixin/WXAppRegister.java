package com.bk.android.time.weixin;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;

/**
 * @author mingkg21
 * @email mingkg21@gmail.com
 * @date 2013-9-14
 */
public class WXAppRegister extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		final IWXAPI api = WXAPIFactory.createWXAPI(context, WXAPIManager.APP_ID);

		api.registerApp(WXAPIManager.APP_ID);
	}
}
