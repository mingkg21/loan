package com.bk.android.time.weixin;

import android.content.Context;

import com.bk.android.weixin.AppMessage;
import com.bk.android.weixin.Message;
import com.bk.android.weixin.WXAPIImpl;
import com.bk.android.weixin.WebpageMessage;
import com.tencent.mm.sdk.modelmsg.SendAuth;

/** 微信的管理类
 * @author mingkg21
 * @email mingkg21@gmail.com
 * @date 2013-4-10
 */
public class WXAPIManager extends WXAPIImpl {
	public static final String APP_ID = "wx703c9daa29fd9ec1";
	public static final String APP_SECRET = "6c3a28d8ae970d7fd9ef5fa4f089702f";
	
	public static final String AUTH_STATE = "wechat_sdk_demo_test";
	
	private static WXAPIManager instance;
	
	private WXAPIManager(Context context){
		super(context);
	}
	
	public static WXAPIManager getInstance(Context context){
		if(instance == null){
			instance = new WXAPIManager(context.getApplicationContext());
		}
		return instance;
	}

	@Override
	protected String getAppID() {
		return APP_ID;
	}
	
	public void login() {
		final SendAuth.Req req = new SendAuth.Req();
		req.scope = "snsapi_userinfo";
		req.state = AUTH_STATE;
		api.sendReq(req);
	}
	
	public String getAccessTokenUrl(String code) {
		return String.format("https://api.weixin.qq.com/sns/oauth2/access_token?appid=%s&secret=%s&code=%s&grant_type=authorization_code", APP_ID, APP_SECRET, code);
	}
	
	public String getUserInfoUrl(String accessToken, String openId) {
		return String.format("https://api.weixin.qq.com/sns/userinfo?access_token=%s&openid=%s", accessToken, openId);
	}
	
	public void shareApp(String title, String desc, byte[] bitmapByte, String webpageUrl, boolean isSession){
		if(title != null && title.length() > 40){
			title = title.substring(0, 38) + "…";
		}
		if(desc != null && desc.length() > 70){
			desc = desc.substring(0, 68) + "…";
		}
		WebpageMessage message = new WebpageMessage(webpageUrl, title, desc, isSession);
		message.setThumbData(bitmapByte);
		sendWebpageMessage(message);
		showOpenWXTip();
	}
	
	public void sendImage(String contentName, String desc, String imgPath, boolean isSession){
		if(contentName != null && contentName.length() > 40){
			contentName = contentName.substring(0, 38) + "…";
		}
		if(desc != null && desc.length() > 70){
			desc = desc.substring(0, 68) + "…";
		}
		Message message = new Message(contentName, desc, true);
		message.setImgPath(imgPath);
		message.setSession(isSession);
		sendImgMessage(message);
		showOpenWXTip();
	}
	
	public void shareApp(String contentName, String desc, byte[] bitmapByte, boolean isSession){
		AppMessage message = new AppMessage(contentName, desc, true, null);
		message.setThumbData(bitmapByte);
		message.setSession(isSession);
		sendAppMessage(message);
		showOpenWXTip();
	}
	
//	public boolean sendPayReq(OrderInfo info){
//		return sendPayReq(info.getAppid(), info.getPartnerid(), info.getPrepayid(), info.getNoncestr(), info.getTimestamp(), info.getPackageData(), info.getSign());
//	}

	private void showOpenWXTip() {
//		if (Preferences.getInstance().isShowOpenWXTip()) {
//			ToastUtil.showLongToast(mContext, "跳转微信中，请稍后");
//			Preferences.getInstance().setShowOpenWXTip();
//		}
	}
	
}
