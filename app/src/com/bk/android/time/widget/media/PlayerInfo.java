package com.bk.android.time.widget.media;

import java.io.File;
import java.net.URL;
import java.net.URLDecoder;

import org.apache.http.client.utils.URIUtils;

import com.bk.android.util.FileUtil;
import com.bk.android.util.URLUtil;

public class PlayerInfo{
	/**
	 * map3比特率 毫秒
	 */
	private int mBitrate;
	/**
	 * map3比特率 秒
	 */
	private int mThousandFoldBitrate;
	/**
	 * 音频详情
	 */
	private Object mDataInfo;
	/**
	 * 开始播放点秒数
	 */
	private int mStartSeconds;
	/**
	 * 开始加载数据点
	 */
	private long mStartLoadPosition;
	/**
	 * 最大秒数
	 */
	private int mMaxSeconds;
	/**
	 * 下载时从服务器获取的文件大小
	 */
	private long mMaxFileSize;
	/**
	 * 缓存文件
	 */
	private File mDownloadingMediaFile;
	/**
	 * 音频下载地址
	 */
	private String mMediaUrl;
	/**
	 * 重复播放
	 */
	private boolean mNeedRepeat; 
	
	public PlayerInfo(Object dataInfo,String mediaUrl,int startSeconds){
		mDataInfo = dataInfo;
		setMediaUrl(mediaUrl);
		setStartSeconds(startSeconds);
	}
	
	public void init(int bitrate,long maxFileSize){
		mBitrate = bitrate;
		mThousandFoldBitrate = mBitrate * 1000;
		this.mMaxFileSize = maxFileSize;
		this.mMaxSeconds = (int) (mMaxFileSize * 8 / mThousandFoldBitrate);
	}
	
	public Object getDataInfo() {
		return mDataInfo;
	}
	/**
	 * @return the mNeedRepeat
	 */
	public boolean isNeedRepeat() {
		return mNeedRepeat;
	}
	/**
	 * @param mNeedRepeat the mNeedRepeat to set
	 */
	public void setNeedRepeat(boolean needRepeat) {
		this.mNeedRepeat = needRepeat;
	}
	/**
	 * 开始播放点秒数
	 */
	public int getStartSeconds() {
		return mStartSeconds;
	}
	
	void setStartSeconds(int startSeconds) {
		this.mStartSeconds = startSeconds;
	}
	
	public long getStartLoadPositionLength(){
		return mStartLoadPosition;
	}
	
	public long getStartLoadPositionMillisecond(){
		return mStartLoadPosition * 8 / mBitrate;
	}
	
	void setStartLoadPosition(long currentMilliseconds) {
		this.mStartLoadPosition = currentMilliseconds / 8 * mBitrate;
	}
	
	public int getMaxSeconds() {
		return mMaxSeconds;
	}
	
	File getMediaFile() {
		return mDownloadingMediaFile;
	}
	
	public long getDownloadingMediaMaxMilliseconds(){
		return mDownloadingMediaFile.length() * 8 / mBitrate;
	}
	
	void setDownloadingMediaFile(File mDownloadingMediaFile) {
		this.mDownloadingMediaFile = mDownloadingMediaFile;
	}
	
	public String getMediaUrl() {
		return mMediaUrl;
	}
	
	public void setMediaUrl(String mMediaUrl) {
		this.mMediaUrl = mMediaUrl;
//		try {
//			this.mMediaUrl = URIUtil.encodePath(mMediaUrl);
//		} catch (URIException e) {
//			this.mMediaUrl = mMediaUrl;
//		}
	}
	
	public boolean hasLocalCache(){
		return mDownloadingMediaFile != null;
	}
	
	public long getDownloadBufferLength(){
		if(hasLocalCache()){
			return mStartLoadPosition + mDownloadingMediaFile.length();
		}else{
			return 0;
		}
	}
	
	public long getDownloadBufferSeconds(){
		return  getDownloadBufferLength() * 8 / mThousandFoldBitrate;
	}
	
	public int computePlayPosition(int playPosition){
		return (int) (playPosition - getStartLoadPositionMillisecond());
	}
	
	public long getMaxFileSize() {
		return mMaxFileSize;
	}
	
	public boolean isLocalUri(){
		return FileUtil.isFileExists(getMediaUrl());
	}
	
	public long conversionMillisecondsToLength(long milliseconds){
		return milliseconds / 8 * mBitrate;
	}
}
