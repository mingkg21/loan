package com.bk.android.time.widget.media;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.text.TextUtils;

import com.bk.android.data.net.AbsConnect;
import com.bk.android.time.app.App;
import com.bk.android.util.ApnUtil;
import com.bk.android.util.LogUtil;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.URL;
import java.util.List;
import java.util.Map.Entry;

public class VoiceUtils {
	private static final String TAG = "VoiceUtils";
	public static int INVALID_BITRATE = -1;

	public static int getMP3BitrateForUrl(Context context,String url) {
		int bitrate = INVALID_BITRATE;
		if (!TextUtils.isEmpty(url)) {
			String encodUrl = "";
			try {
				encodUrl = Uri.encode(url);
			} catch (Exception e) {
				encodUrl = url;
			}
			if(bitrate == INVALID_BITRATE){
				try {
					DownloadInputStream downloadInputStream = getDownloadInputStream(context, encodUrl);
					InputStream inputStream = downloadInputStream.getStream();
					bitrate = getMP3Bitrate(inputStream);
					downloadInputStream.shutdown();
					if (inputStream != null) {
						inputStream.close();
					}
				} catch (IOException e) {
					LogUtil.e(TAG, e);
				}
			}
		}
		return bitrate;
	}

	public static int getMP3BitrateForUrl(Context context,String url,long length) {
		int bitrate = INVALID_BITRATE;
		if (!TextUtils.isEmpty(url)) {
			String encodUrl = url;
//			try {
//				encodUrl = java.net.URLEncoder.encode(url,"UTF-8");
//			} catch (Exception e) {
//				encodUrl = url;
//			}
			if(bitrate == INVALID_BITRATE){
				try {
					MediaPlayer mediaPlayer = new MediaPlayer();
					mediaPlayer.setDataSource(encodUrl);
					mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
					mediaPlayer.prepare();
					bitrate  = (int) (length * 8 / mediaPlayer.getDuration());
					mediaPlayer.release();
				} catch (IOException e) {
					LogUtil.e(TAG, e);
				}
			}
		}
		return bitrate;
	}
	
	public static int getMP3Bitrate(InputStream stream) throws IOException {
		int bitrate = INVALID_BITRATE;
		if (stream != null) {
			String cache = App.getInstance().getCacheDir() + File.separator + "GetMP3Bitrate.dat";
			File file = new File(cache);
			FileOutputStream out = new FileOutputStream(file,false);
			byte buf[] = new byte[1024 * 10];
			int numread = stream.read(buf);
			out.write(buf, 0, numread);
			out.close();
		}
		return bitrate;
	}
	
	public static int getMP3BitrateFile(String path) {
		File file = null;
		if (!TextUtils.isEmpty(path)) {
			file = new File(path);
		}
		return getMP3BitrateFile(file);
	}

	public static int getMP3BitrateFile(File file) {
		int bitrate = INVALID_BITRATE;
		if (file != null && file.exists()) {
			try {
				MediaPlayer mediaPlayer = new MediaPlayer();
				mediaPlayer.setDataSource(new FileInputStream(file).getFD());
				mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
				mediaPlayer.prepare();
				bitrate = (int) (file.length() * 8 / mediaPlayer.getDuration());
				mediaPlayer.release();
			} catch (Exception e) {
				LogUtil.e(TAG, e);
			}
			if(bitrate <= 0){
				try {
					InputStream inputStream = new FileInputStream(file);
					bitrate = getMP3Bitrate(inputStream);
					if (inputStream != null) {
						inputStream.close();
					}
				} catch (IOException e) {
					LogUtil.e(TAG, e);
				}
			}
		}
		return bitrate;
	}
	
	public static DownloadInputStream getDownloadInputStream(Context context,
			String url) throws ClientProtocolException, IOException {
		return getDownloadInputStream(context, url, 0);
	}

	public static DownloadInputStream getDownloadInputStream(Context context,
			String url, long startLoadPosition) throws ClientProtocolException,
			IOException {
		DefaultHttpClient mHttpClient = null;
		HttpURLConnection mHttpURLConnection = null;
		InputStream stream = null;
		long totalBytes = -1;
		try {
			if (ApnUtil.isWifiWork(context)) {
				mHttpClient = AbsConnect.getDefaultHttpClient(context);
				HttpGet mHttpGet = new HttpGet(url);
				mHttpGet.setHeader("Range", "bytes=" + startLoadPosition + "-");
				HttpResponse rp = mHttpClient.execute(mHttpGet);
				for (Header header : rp.getAllHeaders()) {
					LogUtil.v(TAG, "respone header: " + header.getName()
							+ " value: " + header.getValue());
				}
				int statusCode = rp.getStatusLine().getStatusCode();
				if (statusCode == HttpStatus.SC_OK
						|| statusCode == HttpStatus.SC_PARTIAL_CONTENT) {
					Header h = rp.getFirstHeader("Content-Length");
					if (h != null && h.getValue() != null
							&& TextUtils.isDigitsOnly(h.getValue())) {
						totalBytes = Integer.valueOf(h.getValue());
					}
					stream = rp.getEntity().getContent();
				}

			} else {
				String proxyHost = android.net.Proxy.getDefaultHost();
				if (proxyHost != null) {
					java.net.Proxy p = new java.net.Proxy(
							java.net.Proxy.Type.HTTP, new InetSocketAddress(
									android.net.Proxy.getDefaultHost(),
									android.net.Proxy.getDefaultPort()));

					mHttpURLConnection = (HttpURLConnection) new URL(url)
							.openConnection(p);
				} else {
					mHttpURLConnection = (HttpURLConnection) new URL(url)
							.openConnection();
				}
				mHttpURLConnection.setRequestProperty("Connection",
						"Keep-Alive");
				mHttpURLConnection.setRequestProperty("Range", "bytes="
						+ startLoadPosition + "-");
				mHttpURLConnection.setReadTimeout(5000);
				mHttpURLConnection.connect();
				mHttpURLConnection.getResponseCode();

				for (Entry<String, List<String>> header : mHttpURLConnection
						.getHeaderFields().entrySet()) {
					LogUtil.i("Voice player head", header.getKey() + ": "
							+ header.getValue());
				}

				String contentLength = mHttpURLConnection
						.getHeaderField("Content-Length");
				if (contentLength != null
						&& TextUtils.isDigitsOnly(contentLength)) {
					totalBytes = Integer.valueOf(contentLength);
				}
				stream = mHttpURLConnection.getInputStream();
			}
			// 模拟chunk模式
			// totalBytes = -1;
			// if(totalBytes == -1 || totalBytes == 0){
			// totalBytes = mPlayerInfo.getMaxFileSize() - startLoadPosition;
			// }else{
			// mPlayerInfo.setMaxFileSize(totalBytes + startLoadPosition);
			// }
		} catch (Exception e) {
			e.printStackTrace();
		}
		final HttpURLConnection finalHttpConnection = mHttpURLConnection;
		final DefaultHttpClient finalHttpClient = mHttpClient;
		IHttpController httpController = new IHttpController() {
			@Override
			public void shutdown() {
				if (finalHttpClient != null) {
					finalHttpClient.getConnectionManager().shutdown();
				}
				if (finalHttpConnection != null) {
					finalHttpConnection.disconnect();
				}
			}
		};
		return new DownloadInputStream(stream, totalBytes, httpController);
	}

	public final static class DownloadInputStream {
		private InputStream stream;
		private long totalBytes;
		private IHttpController mHttpController;

		private DownloadInputStream(InputStream stream, long totalBytes,
				IHttpController httpController) {
			this.stream = stream;
			this.totalBytes = totalBytes;
			this.mHttpController = httpController;
		}

		/**
		 * @return the stream
		 */
		public InputStream getStream() {
			return stream;
		}

		/**
		 * @return the totalBytes
		 */
		public long getTotalBytes() {
			return totalBytes;
		}

		/**
		 * 中断网络
		 */
		public void shutdown() {
			if (mHttpController != null) {
				//有些机型，shutdown后会有网络动作，造成NetworkRunOnMainThread错误
				new Thread() {
					@Override
					public void run() {
						try {
							mHttpController.shutdown();
						} catch (Exception e) {}
					}
				}.start();
			}
		}
	}

	private interface IHttpController {
		public void shutdown();
	}
}
