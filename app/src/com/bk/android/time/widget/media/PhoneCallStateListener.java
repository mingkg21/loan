package com.bk.android.time.widget.media;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;

public class PhoneCallStateListener extends BroadcastReceiver {
	@Override
	public void onReceive(Context context, Intent intent) {
		if(intent == null || intent.getAction() == null || context == null){
			return;
		}
		if(intent.getAction().equals(TelephonyManager.ACTION_PHONE_STATE_CHANGED)){ 
			onPhoneStateChange(context);
		}
	}
	
	private void onPhoneStateChange(Context context){
		TelephonyManager tm = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);                       
        
        switch (tm.getCallState()) {
        	//来电
            case TelephonyManager.CALL_STATE_RINGING:
            	StreamPlayer.getInstance().temporaryPause();
            	break;
            //接听
            case TelephonyManager.CALL_STATE_OFFHOOK:                                
            	break;
            //挂断
            case TelephonyManager.CALL_STATE_IDLE: 
            	StreamPlayer.getInstance().resumeTemporaryPause();
            	break;
        } 
	}
}
