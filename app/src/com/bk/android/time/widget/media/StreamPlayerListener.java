package com.bk.android.time.widget.media;

public interface StreamPlayerListener {
	public static final byte ERR_TYPE_CONNECTION = 1;
	public static final byte ERR_TYPE_FILE = 2;
	public static final byte ERR_TYPE_INIT = 3;
	public static final byte ERR_TYPE_PLAYER = 4;
	public static final byte ERR_TYPE_URL = 5;

	public static final byte STATE_START = 0;
	public static final byte STATE_STOP = 1;
	public static final byte STATE_COMPLETION = 2;
	public static final byte STATE_PAUSE = 3;
	public static final byte STATE_BUFFER_FINISH = 4;
	public static final byte STATE_PREPAREING = 5;
	public static final byte STATE_ERR = 6;

	public void onPlayStateChange(byte state, String voiceSrc);
	public void onError(byte type, String voiceSrc);
	public void onLoadProgressChange(float progress, String voiceSrc);
	public void onPlayProgressChange(float progress, int maxSeconds, String voiceSrc);
	public void onBuffering(float progress, String voiceSrc);
}
