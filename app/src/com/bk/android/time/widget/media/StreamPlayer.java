package com.bk.android.time.widget.media;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.app.NotificationCompat.Builder;
import android.text.TextUtils;

import com.bk.android.assistant.R;
import com.bk.android.time.app.App;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class StreamPlayer implements StreamPlayerListener {
	private static final String TAG = "SimpleAudioPlayer";
	private static final String ACTION_DELETE_NOTIFY = "ACTION_DELETE_NOTIFY_SIMPLEAUDIOPLAYER";
	private static final String EXTRA_SRC = "EXTRA_SRC";
	
	private static StreamPlayer mInstance;

	public static synchronized StreamPlayer getInstance(){
		if(mInstance == null){
			mInstance = new StreamPlayer();
		}
		return mInstance;
	}
	
	private PlayerControl mPlayerControl;
	private NotificationManager mNotificationManager;
	private ArrayList<WeakReference<StreamPlayerListener>> mPlayerListeners;

	private StreamPlayer(){
		mPlayerControl = new PlayerControl(this);
		mNotificationManager = (NotificationManager) getContext().getSystemService(Context.NOTIFICATION_SERVICE);
		getContext().registerReceiver(new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				String src = intent.getStringExtra(EXTRA_SRC);
				if(!TextUtils.isEmpty(src) && isPlaying(src)){
					stop();
				}
			}
		}, new IntentFilter(ACTION_DELETE_NOTIFY));
		mPlayerListeners = new ArrayList<WeakReference<StreamPlayerListener>>();
	}

	private Context getContext() {
		return App.getInstance();
	}

	public void switchPlay(String voiceSrc){
		switchPlay(voiceSrc,0);
	}
	
	public void switchPlay(String voiceSrc,int startPosition){
		if(isPrepared(voiceSrc)){
			if(mPlayerControl.isPlaying(voiceSrc)){
				mPlayerControl.pause();
			}else{
				mPlayerControl.start();
			}
		}else{
			if(isPrepareing(voiceSrc)){
				mPlayerControl.stop();
			}else{
				playAudio(voiceSrc,startPosition);
			}
		}
	}

	public void playAudio(String voiceSrc){
		playAudio(voiceSrc,-1);
	}
	
	public void playAudio(String voiceSrc, boolean needRepeat){
		playAudio(voiceSrc,-1,needRepeat);
	}
	
	public void playAudio(String voiceSrc,int startPosition){
		startAudio(voiceSrc, startPosition, false);
	}
	
	public void playAudio(String voiceSrc,int startPosition, boolean needRepeat){
		if(TextUtils.isEmpty(voiceSrc)){
			return;
		}
		if(!isCurrentPlaySrc(voiceSrc)){
			startAudio(voiceSrc,startPosition,needRepeat);
		}else{
			if(isPrepared(voiceSrc)){
				if(startPosition > -1){
					mPlayerControl.seekTo((int) startPosition);
				}
				if(!mPlayerControl.isPlaying(voiceSrc)){
					mPlayerControl.start();
				}
			}else if(!isPrepareing(voiceSrc)){
				startAudio(voiceSrc,startPosition,needRepeat);
			}
		}
	}
	
	private void startAudio(String playSrc,int startPosition, boolean needRepeat){
		PlayerInfo playerInfo = new PlayerInfo(null, playSrc,startPosition / 1000);
		playerInfo.setNeedRepeat(needRepeat);
		mPlayerControl.prepared(playerInfo);
	}

	public byte getState() {
		return getState(getCurrentPlaySrc());
	}
	
	public byte getState(String voiceSrc) {
		if(isCurrentPlaySrc(voiceSrc)){
			if(isPlaying(voiceSrc)){
				return StreamPlayerListener.STATE_START;
			}else{
				if(isPrepared(voiceSrc)){
					return StreamPlayerListener.STATE_PAUSE;
				}
				if(isPrepareing(voiceSrc)){
					return StreamPlayerListener.STATE_PREPAREING;
				}
			}
		}
		return StreamPlayerListener.STATE_STOP;
	}
	
	public boolean isPrepareing(String voiceSrc) {
		return mPlayerControl.isPrepareing(voiceSrc);
	}
	
	public boolean isPrepared(String voiceSrc) {
		return mPlayerControl.isPrepared(voiceSrc);
	}
	
	public boolean isPlaying(String voiceSrc) {
		return mPlayerControl.isPlaying(voiceSrc);
	}
	
	public boolean isCurrentPlaySrc(String voiceSrc){
		return mPlayerControl.isCurrentPlaySrc(voiceSrc);
	}
	
	public String getCurrentPlaySrc(){
		return mPlayerControl.getCurrentPlaySrc();
	}
	
	public int getCurrentPositionSeconds(){
		return mPlayerControl.getCurrentPositionSeconds();
	}
	
	public int getMaxSeconds(){
		return mPlayerControl.getMaxSeconds();
	}
	
	private WeakReference<StreamPlayerListener> getPlayerListenersReference(StreamPlayerListener l){
		for (WeakReference<StreamPlayerListener> playerListener : mPlayerListeners) {
			if(playerListener.get() != null && playerListener.get().equals(l)){
				return playerListener;
			}
		}
		return null;
	}
	
	private boolean containsPlayerListeners(StreamPlayerListener l){
		return getPlayerListenersReference(l) != null;
	}
	
	public final void addPlayerListener(StreamPlayerListener l) {
		synchronized (mPlayerListeners) {
			if(!containsPlayerListeners(l)){
				mPlayerListeners.add(new WeakReference<StreamPlayerListener>(l));
			}
		}
	}
	
	public final void removePlayerListener(StreamPlayerListener l) {
		synchronized (mPlayerListeners) {
			WeakReference<StreamPlayerListener> object = getPlayerListenersReference(l);
			if(object != null){
				mPlayerListeners.remove(object);
			}
		}
	}
	
	public void stop() {
		mPlayerControl.stop();
	}
	
	public void pause() {
		mPlayerControl.pause();
	}
	
	public void start() {
		mPlayerControl.start();
	}
	
	public boolean seekTo(int position) {
		return mPlayerControl.seekTo(position / 1000);
	}
	
	public void playBackground(String title,PendingIntent pendingIntent) {
		if(getCurrentPlaySrc() != null){
			Intent intent = new Intent(ACTION_DELETE_NOTIFY);
			intent.putExtra(EXTRA_SRC, getCurrentPlaySrc());
			
			Builder builder = new Builder(getContext());
			builder.setSmallIcon(R.drawable.ic_launcher);
			builder.setContentTitle(title);
			builder.setContentText("后台播放中...点击打开");
			builder.setAutoCancel(true);
			builder.setTicker(title);
			builder.setContentIntent(pendingIntent);
			builder.setDeleteIntent(PendingIntent.getBroadcast(
					getContext(),
					getCurrentPlaySrc().hashCode(), 
					intent,
					PendingIntent.FLAG_UPDATE_CURRENT));
			mNotificationManager.notify(TAG, getCurrentPlaySrc().hashCode() , builder.build());
		}
	}
	
	public void temporaryPause() {
		mPlayerControl.temporaryPause();
	}

	public void resumeTemporaryPause() {
		mPlayerControl.resumeTemporaryPause();
	}
	
	@Override
	public void onPlayStateChange(byte state, String voiceSrc) {
		if(voiceSrc != null && state != STATE_START){
			mNotificationManager.cancel(TAG,voiceSrc.hashCode());
		}
		for (WeakReference<StreamPlayerListener> l : mPlayerListeners) {
			if(l.get() != null){
				l.get().onPlayStateChange(state, voiceSrc);
			}
		}
	}

	@Override
	public void onError(byte type, String voiceSrc) {
		for (WeakReference<StreamPlayerListener> l : mPlayerListeners) {
			if(l.get() != null){
				l.get().onError(type, voiceSrc);
			}
		}
	}

	@Override
	public void onLoadProgressChange(float progress, String voiceSrc) {
		for (WeakReference<StreamPlayerListener> l : mPlayerListeners) {
			if(l.get() != null){
				l.get().onLoadProgressChange(progress, voiceSrc);
			}
		}
	}

	@Override
	public void onPlayProgressChange(float progress, int maxSeconds,
			String voiceSrc) {
		for (WeakReference<StreamPlayerListener> l : mPlayerListeners) {
			if(l.get() != null){
				l.get().onPlayProgressChange(progress, maxSeconds,voiceSrc);
			}
		}
	}

	@Override
	public void onBuffering(float progress, String voiceSrc) {
		for (WeakReference<StreamPlayerListener> l : mPlayerListeners) {
			if(l.get() != null){
				l.get().onBuffering(progress, voiceSrc);
			}
		}
	}
}
