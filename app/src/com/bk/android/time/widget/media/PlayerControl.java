package com.bk.android.time.widget.media;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;

import android.app.ActivityManager;
import android.app.ActivityManager.MemoryInfo;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;

import com.bk.android.time.app.App;
import com.bk.android.time.util.AppFileUtil;
import com.bk.android.time.widget.media.VoiceUtils.DownloadInputStream;
import com.bk.android.util.ApnUtil;
import com.bk.android.util.FileUtil;
import com.bk.android.util.LogUtil;
import com.bk.android.widget.media.BaseMediaPlayer;

public class PlayerControl implements OnCompletionListener, OnErrorListener,OnPreparedListener,OnBufferingUpdateListener{
	private static final String PLAYER_PREFERENCES_NAME = "PLAYER_PREFERENCES_NAME";
	private static final String PLAYER_BITRATE = "PLAYER_BITRATE_";
	private static final String PLAYER_LENGTH = "PLAYER_LENGTH_";
	
	private static final int PLAY_FILE_TYPE_ERR = -1;
	private static final int PLAY_FILE_TYPE_LOCAL = 0;
	private static final int PLAY_FILE_TYPE_SDCARD = 1;
	private static final int PLAY_FILE_TYPE_CACHE = 2;

	private static final String TAG = PlayerControl.class.getSimpleName();
	private int PLAYSIZE = 1024 * 1024 * 50;
	private int BUFFER_SECONDS = 10;
	private StreamPlayerListener mStreamPlayerListener;
	private PlayerInfo mPlayerInfo;
	private BaseMediaPlayer mMediaPlayer;
	private Handler mHandler = new Handler(Looper.getMainLooper());
	private PlayerDataListener mPlayerDataListener;
	private boolean isPrepared = false;
	private DownloadCacheThread mDownloadCacheThread;
	private boolean isInit = false;
	private boolean isOnlineSeekFinish = false;
	private int preSeekPosition = 0;
	private int lastPlayPosition = 0;
	private InitTask mInitTask;
	private SharedPreferences mSharedPreferences;
	private int mPlayFileType;
	
	public PlayerControl(StreamPlayerListener streamPlayerListener){
		mStreamPlayerListener = streamPlayerListener;
		mSharedPreferences = getContext().getSharedPreferences(PLAYER_PREFERENCES_NAME, Context.MODE_PRIVATE);
	}
	
	private void onPlayStateChange(byte state){
		if(mStreamPlayerListener != null && mPlayerInfo != null){
			mStreamPlayerListener.onPlayStateChange(state,mPlayerInfo.getMediaUrl());
		}
	}
	
	public void pause() {
		if(!isInit){
			return;
		}
		if(!isPlaying()){
			return;
		}
		mMediaPlayer.pause();
		onPlayStateChange(StreamPlayerListener.STATE_PAUSE);
	}

	public void stop() {
		stop(true);
	}
	
	public void stop(boolean isNeedNotice) {
//		onPlayProgressChange(0,0);
//		onLoadProgressChange(0);
//		onBufferFinish();
		isInit = false;
		isPrepared = false;
		if(mMediaPlayer != null){
			mMediaPlayer.stop();
			mMediaPlayer.release();
			mMediaPlayer = null;
		}
		if(mInitTask != null){
			mInitTask.stopTask();
			mInitTask = null;
		}
		if(mPlayerDataListener != null){
			mPlayerDataListener.stop();
		}
		if(mDownloadCacheThread != null){
			mDownloadCacheThread.stopThread();
			mDownloadCacheThread = null;
		}
		if(isNeedNotice){
			onPlayStateChange(StreamPlayerListener.STATE_STOP);
		}
	}
	
	private void createMediaPlayer(){
		if(mMediaPlayer != null){
			mMediaPlayer.stop();
			mMediaPlayer.reset();
			mMediaPlayer.release();
			mMediaPlayer.setOnBufferingUpdateListener(null);
			mMediaPlayer.setOnErrorListener(null);
			mMediaPlayer.setOnCompletionListener(null);
			mMediaPlayer.setOnPreparedListener(null);
			mMediaPlayer = null;
		}
		mMediaPlayer = new PrivateMediaPlayer();
		mMediaPlayer.setOnBufferingUpdateListener(this);
		mMediaPlayer.setOnErrorListener(this);
		mMediaPlayer.setOnCompletionListener(this);
		mMediaPlayer.setOnPreparedListener(this);
	}
	
	public void reset(){
		stop();
		mPlayerInfo = null;
		lastPlayPosition = 0;
	}
	
	public void start() {
		if(!isInit){
			return;
		}
		if(isPrepared){
			mMediaPlayer.start();
			onPlayStateChange(StreamPlayerListener.STATE_START);
		}
	}
	
	public boolean handlerUserChangeState(){
		if(isPlaying()){
			pause();
		}else{
			if(isInit && mPlayerDataListener.isBufferFinished()){
				start();
			}else if(mPlayerInfo != null){
				prepared(mPlayerInfo);
			}else{
				return false;
			}
		}
		return true;
	}
	
	private final void runOnUiThread(Runnable action) {
        if (Thread.currentThread() != mHandler.getLooper().getThread()) {
            mHandler.post(action);
        } else {
            action.run();
        }
    }
	
	private String getCacheFile(String dir,PlayerInfo playerInfo){
		return dir + "_" + playerInfo.getStartLoadPositionLength() + "_downloadingMedia.dat";
	}
	
	private String getSDCacheDir(PlayerInfo playerInfo){
		return AppFileUtil.getMediaCachePath() + File.separator + playerInfo.getMediaUrl().hashCode() + File.separator;
	}
	
	private String getCacheDir(PlayerInfo playerInfo){
		return getContext().getCacheDir() + File.separator + playerInfo.getMediaUrl().hashCode() + File.separator;
	}
	
	private void makeFile(PlayerInfo playerInfo){
		File downloadingMediaFile = null;
		if(mPlayFileType == PLAY_FILE_TYPE_SDCARD){
			downloadingMediaFile = new File(getCacheFile(getSDCacheDir(playerInfo),playerInfo));
			File fileDir = new File(getSDCacheDir(playerInfo));
			if(!fileDir.exists()){
				fileDir.mkdirs();
			}
		}else if(mPlayFileType == PLAY_FILE_TYPE_CACHE){
			downloadingMediaFile = new File(getCacheFile(getCacheDir(playerInfo),playerInfo));
			File fileDir = new File(getCacheDir(playerInfo));
			if(!fileDir.exists()){
				fileDir.mkdirs();
			}
		}else if(mPlayFileType == PLAY_FILE_TYPE_LOCAL){
			downloadingMediaFile = new File(playerInfo.getMediaUrl());
		}
		if(downloadingMediaFile != null && downloadingMediaFile.exists() && downloadingMediaFile.isDirectory()){
			LogUtil.i(TAG, "delete folder");
			FileUtil.delFiles(downloadingMediaFile);
		}
		playerInfo.setDownloadingMediaFile(downloadingMediaFile);
	}
	
	private boolean init(PlayerInfo playerInfo){
		LogUtil.i(TAG, "正在初始化播放器 url="+playerInfo.getMediaUrl());
		createMediaPlayer();
		mPlayFileType = PLAY_FILE_TYPE_ERR;
		//初始缓存目录
		if(playerInfo.isLocalUri()){
			File file = new File(playerInfo.getMediaUrl());
			if(file.exists()){
				mPlayFileType = PLAY_FILE_TYPE_LOCAL;
			}
		}
		if(mPlayFileType == PLAY_FILE_TYPE_ERR){
			if(playerInfo.getMediaUrl().startsWith("http://")){
				if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)
					    && getAllSize() > PLAYSIZE ){
						// 有SD卡
						mPlayFileType = PLAY_FILE_TYPE_SDCARD;
						LogUtil.i(TAG, "正在初始化播放器 --> 初始化缓存目录  --> 使用SD目录做本地缓冲");
					} else if (getAvailMemory() > PLAYSIZE) {
						// 没有SD卡
						mPlayFileType = PLAY_FILE_TYPE_CACHE;
						LogUtil.i(TAG, "正在初始化播放器 --> 初始化缓存目录  --> 使用缓存目录做本地缓冲");
					}else{
						LogUtil.i(TAG, "正在初始化播放器  --> 初始化缓存目录 --> 无法构建本地缓冲");
					}
			}else{
				LogUtil.i(TAG, "正在初始化播放器  --> 初始化缓存目录 --> 非 HTTP URL");
			}
		}
		//初始化播放器
		boolean isInitMediaPlayerSucceed = false;
		try {
			if(mPlayFileType == PLAY_FILE_TYPE_ERR){
				isOnlineSeekFinish = true;
				preSeekPosition = 0;
				if(playerInfo.getMediaUrl().startsWith("file:///android_asset/")){
					AssetFileDescriptor fd = getContext().getAssets().openFd(playerInfo.getMediaUrl().substring("file:///android_asset/".length()));
					mMediaPlayer.setDataSource(fd.getFileDescriptor(), fd.getStartOffset(), fd.getLength());
					LogUtil.i(TAG, "正在初始化播放器  --> 初始化MediaPlayer --> 加载本地asset媒体");
				}else{
					mMediaPlayer.setDataSource(playerInfo.getMediaUrl());
					LogUtil.i(TAG, "正在初始化播放器  --> 初始化MediaPlayer --> 使用URI方式加载媒体");
				}
				mMediaPlayer.prepareAsync();
				onBuffering(0,true);
			}else{
				LogUtil.i(TAG, "正在初始化播放器  --> 初始化MediaPlayer --> 使用本地缓冲方式加载媒体");
				mPlayerInfo.setStartLoadPosition(mPlayerInfo.getStartSeconds() * 1000);
				if(mPlayerInfo.getStartLoadPositionLength() < 0){
					mPlayerInfo.setStartLoadPosition(0);
					mPlayerInfo.setStartSeconds(0);
				}
				makeFile(playerInfo);
			}
			isInitMediaPlayerSucceed = true;
		}catch (Exception e) {
			LogUtil.e(TAG, e);
		}
		return isInitMediaPlayerSucceed;
	}

	public void prepared(PlayerInfo playerInfo) {
		stop();
		if(playerInfo == null || playerInfo.getMediaUrl() == null){
			return;
		}
		mPlayerInfo = playerInfo;
		lastPlayPosition = playerInfo.getStartSeconds() * 1000;
		boolean isInitSucceed = init(playerInfo);
		if(!isInitSucceed){
			LogUtil.i(TAG, "初始化播放器时出错");
			onError(StreamPlayerListener.ERR_TYPE_INIT);
			return;
		}
		if(!isPlayLocalFile()){
			onLoadProgressChange(0);
		}else{
			onLoadProgressChange(1);
		}
		mInitTask = new InitTask(playerInfo);
		if(mInitTask.canRunUiThread()){
			mInitTask.run();
		}else{
			new Thread(mInitTask).start();
		}
		onPlayStateChange(StreamPlayerListener.STATE_PREPAREING);
	}

	private boolean isPlayLocalFile() {
		return mPlayFileType == PLAY_FILE_TYPE_LOCAL;
	}

	private class InitTask implements Runnable{
		private boolean isStop;
		private PlayerInfo mPlayerInfo;
		private DownloadInputStream mDownloadInputStream;
		private int mBitrate = -1;
		private long mTotalBytes = -1;

		private InitTask(PlayerInfo playerInfo){
			mPlayerInfo = playerInfo;
		}
		
		private boolean canRunUiThread(){
			return isPlayLocalFile() ||
					(mSharedPreferences.getInt(PLAYER_BITRATE + mPlayerInfo.getMediaUrl(), -1) > 0
							&& mSharedPreferences.getLong(PLAYER_LENGTH + mPlayerInfo.getMediaUrl(), -1) > 0);
		}
		
		@Override
		public void run() {
			long time = System.currentTimeMillis();
			if(isStop){
				LogUtil.e(TAG, "中断播放器初始化");
				return;
			}
			mTotalBytes = mSharedPreferences.getLong(PLAYER_LENGTH + mPlayerInfo.getMediaUrl(), -1);
			mBitrate = mSharedPreferences.getInt(PLAYER_BITRATE + mPlayerInfo.getMediaUrl(), -1);
			if(mBitrate <= 0 || mTotalBytes <= 0){
				if(isPlayLocalFile()){
					mTotalBytes = mPlayerInfo.getMediaFile().length();
					mBitrate = VoiceUtils.getMP3BitrateFile(mPlayerInfo.getMediaFile());
				}else if(mPlayerInfo.getMediaUrl().startsWith("http://")){
					try {
						mDownloadInputStream = VoiceUtils.getDownloadInputStream(getContext(), mPlayerInfo.getMediaUrl());
						mTotalBytes = mDownloadInputStream.getTotalBytes();
						if(mTotalBytes > 0 && !isStop){
							try {
								mBitrate = VoiceUtils.getMP3Bitrate(mDownloadInputStream.getStream());
							} catch (Exception e) {
								e.printStackTrace();
							}
							if(mBitrate <= 0){
								mBitrate = VoiceUtils.getMP3BitrateForUrl(getContext(), mPlayerInfo.getMediaUrl(),mTotalBytes);
							}
						}
						mDownloadInputStream.shutdown();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}else if(mPlayerInfo.getMediaUrl().startsWith("file:///android_asset/")){
					try {
						AssetFileDescriptor fd = getContext().getAssets().openFd(mPlayerInfo.getMediaUrl().substring("file:///android_asset/".length()));
						mBitrate = VoiceUtils.getMP3Bitrate(fd.createInputStream());
						mTotalBytes = fd.getLength();
					} catch (Exception e) {}
				}
				mSharedPreferences.edit().putLong(PLAYER_LENGTH + mPlayerInfo.getMediaUrl(),mTotalBytes).putInt(PLAYER_BITRATE + mPlayerInfo.getMediaUrl(), mBitrate).commit();
			}
			if(isStop){
				LogUtil.e(TAG, "中断播放器初始化");
				return;
			}
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					if(isStop){
						LogUtil.e(TAG, "中断播放器初始化");
						return;
					}
					if(mBitrate > 0){
						mPlayerInfo.init(mBitrate, mTotalBytes);
						float progress = mPlayerInfo.getStartSeconds() * 1.0f / mPlayerInfo.getMaxSeconds();
						onPlayProgressChange(progress,mPlayerInfo.getMaxSeconds());
						if(mPlayerInfo.hasLocalCache() && !isPlayLocalFile()){
							startDownloadCacheThread(mPlayerInfo);
						}
						mPlayerDataListener = new PlayerDataListener(mHandler, mPlayerInfo);
						mPlayerDataListener.start();
						LogUtil.i(TAG, "播放器初始化完成");
						isInit = true;
					}else{
						onError(StreamPlayerListener.ERR_TYPE_INIT);
					}
					isStop = true;
				}
			});
			LogUtil.i(TAG, "播放器初始化 总耗时="+(System.currentTimeMillis() - time));
		}
		
		public void stopTask(){
			isStop = true;
			if(mDownloadInputStream != null){
				mDownloadInputStream.shutdown();
			}
		}
	}
	
	@Override
	public void onPrepared(MediaPlayer mp) {
		LogUtil.i(TAG, "播放器就绪");
		isPrepared = true;
		mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
	}

	@Override
	public boolean onError(MediaPlayer mp, int what, int extra) {
		LogUtil.e(TAG, "播放器报错 ：onPlayerError : what="+what+" extra="+extra);
		onError(StreamPlayerListener.ERR_TYPE_PLAYER);
		if (extra == -11) {
			if (mMediaPlayer != null) {
				createMediaPlayer();
			}
		} else if (what == -38) {
			if (mMediaPlayer != null) {
				createMediaPlayer();
			}
		} else if (what == 1 && extra == -1) {
			if (mMediaPlayer != null) {
				createMediaPlayer();
			}
		}
		return false;
	}
	
	private boolean checkPlayFinish(int handlerPosition){
		return checkPlayFinish(handlerPosition,2000);
	}
	
	private boolean checkPlayFinish(int handlerPosition,int estimate){
		if(mPlayerInfo == null || mPlayerInfo.getMaxFileSize() == 0){
			return false;
		}
		int surplus = mPlayerInfo.getMaxSeconds() * 1000 - handlerPosition;
		LogUtil.i(TAG, "播放器-检查是否正常结束 " + "剩余毫秒数 ="+surplus +" 是否结束="+ (surplus < estimate) );
		if (surplus < estimate) {
			onPlayFinish();
			return true;
		}
		return false;
	}
	
	@Override
	public void onCompletion(MediaPlayer mp) {
		int handlerPosition = getCurrentPositionMilliSeconds();
		LogUtil.i(TAG, "播放器-onCompletion " + "当前播放="+handlerPosition);
		mPlayerInfo.setStartSeconds(handlerPosition / 1000);
		if (!checkPlayFinish(handlerPosition)) {
			if(mPlayerInfo.hasLocalCache()){
				handlerSeek(handlerPosition);
			}else{
				onError(StreamPlayerListener.ERR_TYPE_PLAYER);
			}
		}
	}
	
	private void handlerSeek(int currentMilliseconds){
		LogUtil.i(TAG, "播放器-handlerSeek 有本地缓冲"
				+"\n 处理点(毫秒)="+currentMilliseconds
				+"\n 已缓存(毫秒)="+mPlayerInfo.getDownloadingMediaMaxMilliseconds()
				+"\n 缓存开始点(毫秒)="+( mPlayerInfo.getStartLoadPositionMillisecond())
				+"\n 最大毫秒数="+(mPlayerInfo.getMaxSeconds() * 1000));
		
		long currentSeconds = currentMilliseconds / 1000;
		long bufferSeconds = mPlayerInfo.getDownloadBufferSeconds();
		long difference = currentSeconds - bufferSeconds;
		boolean isOverstepBuffer = mPlayerInfo.getStartLoadPositionMillisecond() > currentMilliseconds 
				|| difference > BUFFER_SECONDS;
		//触发重启缓冲等待条件：
		//	1.缓冲多于的内容只够继续播放1秒以内
		//  2.处理点大于缓冲进度
		//  3.处理点小于缓冲开始位置
		if(Math.abs(difference) <= 1 
				|| difference > 0 
				|| isOverstepBuffer){
			LogUtil.i(TAG, "播放器-handlerSeek --> 准备重启等待缓冲");
			if(ApnUtil.isNetAvailable(getContext()) && !mPlayerInfo.isLocalUri()){
				LogUtil.i(TAG, "播放器-handlerSeek --> 重启等待缓冲成功");
				if(isOverstepBuffer){
					if(mDownloadCacheThread != null && !mDownloadCacheThread.isStop()){
						mDownloadCacheThread.stopThread();
					}
					mPlayerInfo.setStartLoadPosition(currentMilliseconds);
					mDownloadCacheThread = new DownloadCacheThread(mPlayerInfo);
					mDownloadCacheThread.start();
					onLoadProgressChange(0);
				}
				mPlayerDataListener.startBuffering();
			}else{
				if(mPlayerInfo.isLocalUri()){
					onError(StreamPlayerListener.ERR_TYPE_FILE);
				}else{
					onError(StreamPlayerListener.ERR_TYPE_CONNECTION);
				}
			}
		}else if(currentSeconds < bufferSeconds){
			LogUtil.i(TAG, "播放器-handlerSeek --> 未知原因意外终止，执行修复代码 ");
			try {
				if(isPrepared){
					createMediaPlayer();
					mMediaPlayer.setDataSource(new FileInputStream(mPlayerInfo.getMediaFile()).getFD());
					mMediaPlayer.prepare();
					mMediaPlayer.seekTo(mPlayerInfo.computePlayPosition(currentMilliseconds));
					start();
				}
			} catch (Exception e) {
				LogUtil.e(TAG, e);
			}
		}
	}
	
	@Override
	public void onBufferingUpdate(MediaPlayer mp, int percent) {
		if(isInit && mPlayFileType == PLAY_FILE_TYPE_ERR){
			if(mPlayerDataListener.isBufferFinished()){
				mPlayerDataListener.startBuffering();
			}
			onBuffering(percent / 100.0f,true);
		}
	}
	/**
	 * 跳转
	 * @param position 秒数
	 */
	public boolean seekTo(int position){
		if(!isInit){
			return false;
		}
		if (mPlayerInfo.getMaxSeconds() <= position) {
			onPlayFinish();
			return true;
		}
		LogUtil.i(TAG, "跳转   position="+position);
		if(mPlayerInfo.hasLocalCache()){
			pause();
			mPlayerInfo.setStartSeconds(position);
			handlerSeek(position * 1000);
		}else if(isPrepared){
			LogUtil.i(TAG, "跳转   -->在线收听跳转 position="+position);
			lastPlayPosition = position * 1000;
			if(isOnlineSeekFinish){
				isOnlineSeekFinish = false;
				preSeekPosition = getCurrentPositionSeconds();
			}
			pause();
			mPlayerInfo.setStartSeconds(position);
			mMediaPlayer.seekTo(position * 1000);
			start();
		}else{
			return false;
		}
		return true;
	}
	
	// 根据获得的URL地址下载数据
	private void startDownloadCacheThread(final PlayerInfo playerInfo) {
		if(mDownloadCacheThread != null && !mDownloadCacheThread.isStop()){
			mDownloadCacheThread.stopThread();
		}
		mDownloadCacheThread = new DownloadCacheThread(playerInfo);
		mDownloadCacheThread.start();
	}
		
	// SDK剩余空间
	private long getAllSize() {
		return FileUtil.getStorageSize();
	}

	private long getAvailMemory() {// 获取android当前可用内存大小
		ActivityManager am = (ActivityManager) getContext().getSystemService(Context.ACTIVITY_SERVICE);
		MemoryInfo mi = new MemoryInfo();
		am.getMemoryInfo(mi);// mi.availMem; 当前系统的可用内存
		return mi.availMem;// 将获取的内存大小规格化
	}
	
	private Context getContext(){
		return App.getInstance();
	}
	
	public int getMaxSeconds() {
		return mPlayerInfo != null ? mPlayerInfo.getMaxSeconds() : 0;
	}
	
	public int getCurrentPositionSeconds() {
		return getCurrentPositionMilliSeconds() / 1000;
	}
		
	public int getCurrentPositionMilliSeconds() {
		if (mMediaPlayer == null || !isPrepared) {
			return 0;
		}
		int currentPosition = mMediaPlayer.getCurrentPosition();
		currentPosition += mPlayerInfo.getStartLoadPositionMillisecond();
		return currentPosition;
	}
	
	public int getMediaPlayerPositionMilli() {
		if (mMediaPlayer == null || !isPrepared) {
			return lastPlayPosition;
		}
		int currentPosition = getCurrentPositionMilliSeconds();
		return currentPosition;
	}
	
	public int getMediaPlayerPosition() {
		return getMediaPlayerPositionMilli() / 1000;
	}
	
	private void onBuffering(float progress,boolean isNeedNotify){
		if(progress >= 1){
			progress = 1;
		}
		if(progress < 0){
			progress = 0;
		}
		if(mStreamPlayerListener != null && mPlayerInfo != null && isNeedNotify){
			mStreamPlayerListener.onBuffering(progress,mPlayerInfo.getMediaUrl());
		}
	}
	
	private void onPlayProgressChange(float progress,int maxSeconds){
		if(mStreamPlayerListener != null && mPlayerInfo != null){
			mStreamPlayerListener.onPlayProgressChange(progress,maxSeconds,mPlayerInfo.getMediaUrl());
		}
	}
	
	private void onLoadProgressChange(final float progress){
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if(mStreamPlayerListener != null && mPlayerInfo != null){
					mStreamPlayerListener.onLoadProgressChange(progress,mPlayerInfo.getMediaUrl());
				}
			}
		});
	}
	
	private void onError(final byte type){
		LogUtil.i(TAG, "播放器-出错了  --> 错误类型："+type);
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if(!checkPlayFinish(lastPlayPosition)){
					if(mStreamPlayerListener != null && mPlayerInfo != null){
						mStreamPlayerListener.onPlayStateChange(StreamPlayerListener.STATE_ERR,mPlayerInfo.getMediaUrl());
						mStreamPlayerListener.onError(type,mPlayerInfo.getMediaUrl());
					}
					stop(false);
				}
			}
		});
	}
	
	private void onPlayFinish(){
		if(isPrepared){
			pause();
		}
		onPlayProgressChange(1,mPlayerInfo.getMaxSeconds());
		mPlayerInfo.setStartSeconds(0);
		LogUtil.i(TAG, "播放器-onCompletion  --> 正常播放结束");
		onPlayStateChange(StreamPlayerListener.STATE_COMPLETION);
		if(mPlayerInfo.isNeedRepeat()){
			start();
		}
	}

	private void onBufferFinish(){
		if(mPlayerInfo != null){
			onPlayStateChange(StreamPlayerListener.STATE_BUFFER_FINISH);
		}
	}
	
	public String getCurrentPlaySrc() {
		return mPlayerInfo != null ? mPlayerInfo.getMediaUrl() : null;
	}
	
	public boolean isCurrentPlaySrc(String voiceSrc){
		return mPlayerInfo != null && mPlayerInfo.getMediaUrl().equals(voiceSrc);
	}
	
	public boolean isPrepared(String voiceSrc) {
		if(!isCurrentPlaySrc(voiceSrc)){
			return false;
		}
		return isPrepared;
	}
	
	public boolean isPrepareing(String voiceSrc) {
		if(!isCurrentPlaySrc(voiceSrc)){
			return false;
		}
		return !isPrepared && ((mInitTask != null && !mInitTask.isStop) || (mPlayerDataListener != null && mPlayerDataListener.isStart));
	}
	
	public float getBufferingProgress(){
		if(mPlayerDataListener != null && !mPlayerDataListener.isBufferFinished){
			return mPlayerDataListener.getBufferingProgress();
		}
		return 0;
	}
	
	public boolean isPlaying(String voiceSrc) {
		if(voiceSrc != null && !isCurrentPlaySrc(voiceSrc)){
			return false;
		}
		return isPlaying();
	}
	
	private boolean isPlaying() {
		if (mMediaPlayer != null) {
			return mMediaPlayer.isPlaying();
		}
		return false;
	}

	public void resumeTemporaryPause() {
		if(mMediaPlayer != null){
			mMediaPlayer.resumeTemporaryPause();
		}
	}

	public void temporaryPause() {
		if(mMediaPlayer != null){
			mMediaPlayer.temporaryPause();
		}
	}
	
	private class PlayerDataListener implements Runnable{
		private static final int RETRIES_MAX_SIZE = 5;
		private long mDelayMillis = 200;
		private Handler mHandler;
		private PlayerInfo mPlayerInfo;
		private boolean isStart = false;
		private long oldBufferLength = -1;
		private long bufferSize = -1;
		private boolean isBufferFinished;
		private boolean isBufferFinishedPostStarted = false;
		private int updateSum = 0;
		private int retriesSize = 0;
		private int oldNotifyPosition = -1;

		public PlayerDataListener(Handler handler,PlayerInfo playerInfo){
			mHandler = handler;
			mPlayerInfo = playerInfo;
			isBufferFinishedPostStarted = false;
			isBufferFinished = isPlayLocalFile();
			oldBufferLength = -1;
			bufferSize = -1;
			retriesSize = 0;
		}
		
		public void start(){
			if(isStart){
				return;
			}
			isStart = true;
			mHandler.postDelayed(this, mDelayMillis);
		}
		
		public void stop(){
			if(!isStart){
				return;
			}
			isStart = false;
			mHandler.removeCallbacks(this);
		}
		
		public boolean isBufferFinished(){
			return isBufferFinished;
		}
		
		public void startBuffering(){
			isBufferFinished = false;
			oldBufferLength = -1;
			bufferSize = -1;
			isBufferFinishedPostStarted = false;
			onPlayStateChange(StreamPlayerListener.STATE_PAUSE);
		}
		
		private void cancelBuffering(){
			isBufferFinished = true;
			isBufferFinishedPostStarted = true;
			onBufferFinish();
		}
		
		@Override
		public void run() {
			if(!this.equals(mPlayerDataListener)){
				isStart = false;
				return;
			}
			if(!isStart){
				return;
			}
			if(updateSum == Integer.MAX_VALUE){
				updateSum = 0;
			}
			updateSum ++;
			listenLocalCache();
			listenPlayProgress();
			mHandler.postDelayed(this, mDelayMillis);
		}
		/**
		 *如果有本地缓存监听缓存
		 */
		private void listenLocalCache(){
			if(mPlayerInfo.hasLocalCache()){
				if(!isBufferFinished){
					if(bufferSize == -1){
						oldBufferLength = mPlayerInfo.getDownloadBufferLength();
						long bufferPosition = ( mPlayerInfo.getStartSeconds() + BUFFER_SECONDS );
						if(bufferPosition > mPlayerInfo.getMaxSeconds()){
							bufferPosition = mPlayerInfo.getMaxSeconds();
						}
						bufferSize = mPlayerInfo.conversionMillisecondsToLength(bufferPosition * 1000) - oldBufferLength;
						onBuffering(0,true);
						LogUtil.i(TAG, "播放器数据监听   --> 初始化缓冲监听   缓冲点(秒)="+bufferPosition
								+" 当前以下载缓冲大小="+oldBufferLength);
					}
					long bufferLength = mPlayerInfo.getDownloadBufferLength();
					int differential = (int) (bufferLength - oldBufferLength);
					float progress = 0;
					if(bufferSize > 0){
						progress = differential * 1.0f / bufferSize;
					}else{
						progress = 1;
					}
					if(progress >= 1){
						isBufferFinished = true;
					}
					onBuffering(progress,bufferLength != oldBufferLength);
				}else if(!isPlaying() && !isBufferFinishedPostStarted 
						&& mPlayerInfo.getMediaFile() != null 
						&& mPlayerInfo.getMediaFile().exists()){
					LogUtil.i(TAG, "播放器数据监听   -->  缓冲完成开始播放   播放点（好秒）="+mPlayerInfo.getStartSeconds() * 1000);
					isBufferFinishedPostStarted = true;
					onBufferFinish();
					retriesSize = 0;
					int startPosition = mPlayerInfo.getStartSeconds() * 1000;
					if(!checkPlayFinish(startPosition)){
						try {
							createMediaPlayer();
							File file = mPlayerInfo.getMediaFile();
							mMediaPlayer.setDataSource(new FileInputStream(file).getFD());
							mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
							mMediaPlayer.prepare();
							int seek = mPlayerInfo.computePlayPosition(startPosition);
							LogUtil.i(TAG, "播放器数据监听   -->  缓冲完成开始播放  转换后的 播放点（好秒）Seek="+seek+" Duration="+mMediaPlayer.getDuration());
//							有些手机在这里调用mMediaPlayer.getDuration()，MediaPlayer会出错
//							if(mMediaPlayer.getDuration() < seek){
//								seek = mMediaPlayer.getDuration() - 1;
//							}
							mMediaPlayer.seekTo(seek);
							isPrepared = true;
							PlayerControl.this.start();
						}catch (Exception e) {
							LogUtil.e(TAG, e);
							onError(StreamPlayerListener.ERR_TYPE_PLAYER);
						}
					}
				} 
				
				if(!isBufferFinished && mDownloadCacheThread != null 
						&& !mDownloadCacheThread.isFinish() 
						&& mDownloadCacheThread.isStop()
						&& !isPlayLocalFile()){
					if(ApnUtil.isNetAvailable(getContext())){
						if(retriesSize < RETRIES_MAX_SIZE){
							retriesSize++;
							LogUtil.i(TAG, "播放器数据监听   --> 缓冲还未结束，重启下载缓冲线程   isFinish="
									+mDownloadCacheThread.isFinish()
									+" isStop="
									+mDownloadCacheThread.isStop());
							mDownloadCacheThread = new DownloadCacheThread(mPlayerInfo);
							mDownloadCacheThread.start();
						}else{
							onError(StreamPlayerListener.ERR_TYPE_CONNECTION);
						}
					}else{
						onError(StreamPlayerListener.ERR_TYPE_CONNECTION);
					}
				}
			}else  if(isPrepared && !isPlaying() && !isBufferFinishedPostStarted){
				LogUtil.i(TAG, "播放器数据监听   --> 在线播放已经准备好  开始播放   播放点（好秒）="+mPlayerInfo.getStartSeconds() * 1000);
				cancelBuffering();
				mMediaPlayer.seekTo(mPlayerInfo.getStartSeconds() * 1000);
				PlayerControl.this.start();
			}
		}
		
		private float getBufferingProgress(){
			long bufferLength = mPlayerInfo.getDownloadBufferLength();
			int differential = (int) (bufferLength - oldBufferLength);
			float progress = 0;
			if(bufferSize > 0){
				progress = differential * 1.0f / bufferSize;
			}else{
				progress = 1;
			}
			return progress;
		}
		/**
		 * 监听播放进度
		 */
		private void listenPlayProgress(){
			int currentPosition = 0;
			if(isPrepared){
//				LogUtil.e(TAG, "播放器数据监听   --> 在线播放 监听播放进度  当前播放点="+currentPosition +"  preSeekPosition="+preSeekPosition);
				lastPlayPosition = getCurrentPositionMilliSeconds();
				if(!isBufferFinished){
					cancelBuffering();
				}
				currentPosition = getCurrentPositionSeconds();
				if(!mPlayerInfo.hasLocalCache()){//isOnlineSeekFinish
					if(!isOnlineSeekFinish ){
						LogUtil.v(TAG, "播放器数据监听   --> 在线播放 监听播放进度  当前播放点="+currentPosition +"  preSeekPosition="+preSeekPosition);
						if(currentPosition == preSeekPosition){
							currentPosition = mPlayerInfo.getStartSeconds();
						}else{
							isOnlineSeekFinish = true;
						}
					}
				}
			}else{
				currentPosition = mPlayerInfo.getStartSeconds();
			}
			//通知UI的数据有变化，才发起通知
			if(oldNotifyPosition != currentPosition){
				float progress = currentPosition * 1.0f / mPlayerInfo.getMaxSeconds();
				if(progress > 1){
					progress = 1;
				}
				LogUtil.v(TAG, "播放器数据监听   --> 在线播放 监听播放进度  播放进度="+ progress +"  最大秒数="+mPlayerInfo.getMaxSeconds());
				onPlayProgressChange(progress,mPlayerInfo.getMaxSeconds());
				oldNotifyPosition  = currentPosition;
			}
		}
	}
	
	private class DownloadCacheThread extends Thread{
		/**
		 * 缓冲大小
		 */
		private int BUFFER_SIZE = 1024 * 50;
		private PlayerInfo mPlayerInfo;
		private boolean isStop;
		private boolean isDownLoadLoop;
		private boolean isFinish;
		private long startLoadPosition;
		private DownloadInputStream mDownloadInputStream;
		public DownloadCacheThread(PlayerInfo playerInfo){
			mPlayerInfo = playerInfo;
			this.startLoadPosition = playerInfo.getStartLoadPositionLength();
			isDownLoadLoop = false;
			isStop = false;
			isFinish = false;
			setName("有声本地缓冲下载线程");
		}
		
		public boolean isStop(){
			return isStop;
		}
		
		public void stopThread(){
			LogUtil.i(TAG, "停止 下载音频缓冲");
			isDownLoadLoop = false;
			if(mDownloadInputStream != null){
				mDownloadInputStream.shutdown();
			}
		}
		
		public boolean isFinish(){
			return isFinish;
		}
		
		@Override
		public void run() {
			isDownLoadLoop = true;
			isStop = false;
			InputStream stream = null;
			FileOutputStream out = null;
			int numread = 0;
			long totalBytes = -1;
			try {
				LogUtil.i(TAG, "下载音频缓冲  --> 下载地址="+mPlayerInfo.getMediaUrl() +" 下载开始点为 ："+ startLoadPosition );
				Uri uri = Uri.parse(mPlayerInfo.getMediaUrl());
				if(uri == null || uri.getHost() == null){
					onError(StreamPlayerListener.ERR_TYPE_URL);
				}
				makeFile(mPlayerInfo);
				final File downloadingMediaFile = mPlayerInfo.getMediaFile();
				File[] files = downloadingMediaFile.getParentFile().listFiles(new FileFilter() {
					@Override
					public boolean accept(File pathname) {
						return downloadingMediaFile.getName().equals(pathname);
					}
				});
				if(files != null){
					for (File file : files) {
						FileUtil.delFiles(file);
					}
				}
				long startPosition = startLoadPosition + downloadingMediaFile.length();
				mDownloadInputStream = VoiceUtils.getDownloadInputStream(getContext(),mPlayerInfo.getMediaUrl(),startPosition);
				stream = mDownloadInputStream.getStream();
				totalBytes = mDownloadInputStream.getTotalBytes();
				//模拟chunk模式
//				totalBytes = -1;
				if(totalBytes == -1 || totalBytes == 0){
					totalBytes = mPlayerInfo.getMaxFileSize() - startPosition;
				}
				if(totalBytes > 0 && stream != null){
					LogUtil.i(TAG, "下载音频缓冲 --> 开始下载文件  -->下载开始点为 ："+ startLoadPosition +" 需要下载文件大小为："+totalBytes +" 文件总大小为："+(totalBytes + startLoadPosition));
					if(isDownLoadLoop){
						out = new FileOutputStream(downloadingMediaFile,true);
						byte buf[] = new byte[BUFFER_SIZE];
						while (-1 != (numread = stream.read(buf)) && isDownLoadLoop) {
							if (numread > 0) {
								out.write(buf, 0, numread);
								onLoadProgressChange(getCurrentLength() * 1.0f / ( totalBytes + startPosition ));
							}
						}
					}
				}
			} catch (Exception e) {
				LogUtil.e(TAG, "下载音频缓冲 --> 出现异常");
				LogUtil.e(TAG,e);
			} finally {
				if (stream != null) {
					try {
						stream.close();
					} catch (Exception e) {LogUtil.e(TAG,e);}
				}
				if(out != null){
					try {
						out.close();
					} catch (Exception e) {LogUtil.e(TAG,e);}
				}
			}
			LogUtil.i(TAG, "下载音频缓冲 --> 退出 已下载文件大小："+getCurrentLength() +" 文件总大小为："+(totalBytes + startLoadPosition));
			if(numread == -1){
				isFinish = true;
			}
			isStop = true;
			isDownLoadLoop = false;
		}
		
		public long getCurrentLength(){
			if(mPlayerInfo != null && mPlayerInfo.getMediaFile() != null){
				return mPlayerInfo.getMediaFile().length() + startLoadPosition;
			}
			return 0;
		}
	}
	
	private class PrivateMediaPlayer extends BaseMediaPlayer{
		@Override
		protected String getCurrentPlaySrc() {
			return mPlayerInfo.getMediaUrl();
		}
	}
}
