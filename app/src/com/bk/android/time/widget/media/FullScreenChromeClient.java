package com.bk.android.time.widget.media;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.webkit.WebChromeClient;
import android.widget.RelativeLayout;

public class FullScreenChromeClient extends WebChromeClient implements OnKeyListener{
	private CustomViewCallback mCustomViewCallback;  
    private int mOriginalOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;  
	private Activity mActivity;
	private LayoutParams mLayoutParams;
	private RelativeLayout mContenLayout;
	private boolean isShowWindow;
	private Integer mOldFlags;
	
	public FullScreenChromeClient(Activity activity){
		mActivity = activity;
		mLayoutParams = new LayoutParams();
		mLayoutParams.width = LayoutParams.MATCH_PARENT;
		mLayoutParams.height = LayoutParams.MATCH_PARENT;
		mLayoutParams.type = WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY;
		mLayoutParams.format = PixelFormat.TRANSPARENT;
		
		mContenLayout = new RelativeLayout(mActivity){
			@Override
			public boolean dispatchTouchEvent(MotionEvent ev) {
				try {
					return super.dispatchTouchEvent(ev);
				} catch (Exception e) {
					return false;
				}
			}
		};
		mContenLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT
				,ViewGroup.LayoutParams.MATCH_PARENT));
		mContenLayout.setClickable(true);
		mContenLayout.setFocusable(true);
		mContenLayout.setOnKeyListener(this);
		mContenLayout.setBackgroundColor(Color.BLACK);
		mContenLayout.setVisibility(View.GONE);
		Window window = mActivity.getWindow();
		window.addContentView(mContenLayout, mLayoutParams);
	}
	
	private void quitFullScreen(){
		if(mOldFlags != null){
			LayoutParams lp = mActivity.getWindow().getAttributes();
			lp.flags = mOldFlags;
			mActivity.getWindow().setAttributes(lp);
			mOldFlags = null;
		}
	}
	
	private void setFullScreen(){
		mOldFlags = mActivity.getWindow().getAttributes().flags;
		mActivity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
	}
	
	public void showWindow(View contentView, int requestedOrientation,CustomViewCallback callback){
		try {
			if(mContenLayout.getVisibility() == View.VISIBLE){
				mContenLayout.removeAllViews();
		        if(mCustomViewCallback != null){
					mCustomViewCallback.onCustomViewHidden();
			        mCustomViewCallback = null;
		        }
			}
			isShowWindow = true;
			mContenLayout.setVisibility(View.VISIBLE);
			setFullScreen();
			DisplayMetrics dm = mActivity.getResources().getDisplayMetrics();
			RelativeLayout.LayoutParams lp;
			if(requestedOrientation == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE){
				lp = new RelativeLayout.LayoutParams(dm.heightPixels,dm.widthPixels);
			}else{
				lp = new RelativeLayout.LayoutParams(dm.widthPixels,dm.heightPixels);
			}
			lp.addRule(RelativeLayout.CENTER_IN_PARENT);
			contentView.setLayoutParams(lp);
			mContenLayout.addView(contentView);
			mCustomViewCallback = callback;
	        mOriginalOrientation = mActivity.getRequestedOrientation();
			mActivity.setRequestedOrientation(requestedOrientation);
		} catch (Exception e) {
			e.printStackTrace();
			hideWindow();
		}
	}
	
	public void hideWindow(){
		isShowWindow = false;
		try {
			if(mContenLayout.getVisibility() == View.VISIBLE){
				quitFullScreen();
				mContenLayout.setVisibility(View.GONE);
		        mActivity.setRequestedOrientation(mOriginalOrientation);  
				mContenLayout.removeAllViews();
			}
			if(mCustomViewCallback != null){
	        	mCustomViewCallback.onCustomViewHidden();
		        mCustomViewCallback = null;
	        }
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public boolean isShowWindow() {
		return isShowWindow;
	}

	@Override  
    public void onShowCustomView(final View view, final CustomViewCallback callback) {
    	onShowCustomView(view,ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE, callback);
    }
    
    @Override
    public void onShowCustomView(final View view,final int requestedOrientation,final CustomViewCallback callback) {
    	mContenLayout.post(new Runnable() {
			@Override
			public void run() {
		    	showWindow(view,requestedOrientation, callback);  
			}
		});
    }
    
    @Override  
    public void onHideCustomView() {  
    	mContenLayout.post(new Runnable() {
			@Override
			public void run() {
		    	hideWindow();
			}
		});
    }

	@Override
	public boolean onKey(View v, int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_BACK){
			if(event.getAction() == KeyEvent.ACTION_DOWN){
				hideWindow();	
			}
			return true;
		}
		return false;
	}  
}
