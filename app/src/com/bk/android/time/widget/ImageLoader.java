package com.bk.android.time.widget;

import android.content.ContentResolver;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.net.Uri;
import android.text.TextUtils;

import com.bk.android.time.app.App;
import com.bk.android.time.util.AppFileUtil;
import com.bk.android.util.BitmapUtil;
import com.bk.android.util.BitmapUtil.LoadNetCallback;
import com.bk.android.util.FileUtil;
import com.bk.android.widget.imageload.BaseImageLoader;

import java.io.File;
import java.io.IOException;

/**
 * @author mingkg21
 * @email mingkg21@gmail.com
 * @date 2011-9-15
 */
public class ImageLoader extends BaseImageLoader {
	private static ImageLoader mInstance;
	public static ImageLoader getInstance(){
		if(mInstance == null){
			mInstance = new ImageLoader();
		}
		return mInstance;
	}

	private String mImgCachePath;
	private String mAppPath;

	private ImageLoader() {}
	
	public Bitmap getLocalImage(String url) {
		return getLocalImage(url, BitmapUtil.IMG_QUALITY_1);
	}
	/** 通过网络地址url查找缓存和本地是否有图片
	 * @param url
	 * @return
	 */
	public Bitmap getLocalImage(String url, int quality) {
		Bitmap bitmap = null;
		try {
			Uri uri = Uri.parse(url);
			if(uri != null){
				String scheme = uri.getScheme();
				if("http".equals(scheme)){
					String path = urlToPath(url);
					bitmap = loadImageInCache(getId(url));
					if(bitmap == null) {
						bitmap = loadImageFromSdcard(path,url,quality);
					}
				}else if(ContentResolver.SCHEME_ANDROID_RESOURCE.equals(scheme)
	            		|| ContentResolver.SCHEME_CONTENT.equals(scheme)){
					bitmap = BitmapUtil.clipScreenBoundsBitmap(App.getInstance().getResources(),uri,BitmapUtil.getQualityScale(BitmapUtil.IMG_QUALITY_2));
				}else{
					String path = url;
					if(ContentResolver.SCHEME_FILE.equals(scheme)){
						path = uri.getPath();
					}
					bitmap = BitmapUtil.getBitmapInSdcard(path, quality);
				}
			}
		} catch (IOException e) {}
		return bitmap;
	}

	@Override
	public String getPath(String imageId, String url) {
		if(mImgCachePath == null){
			mImgCachePath = AppFileUtil.getImgCachePath();
		}
		try {
			if(!TextUtils.isEmpty(url)){
				Uri uri = Uri.parse(url);
				if((url.startsWith(ContentResolver.SCHEME_FILE) || TextUtils.isEmpty(uri.getScheme())) && uri.getPath() != null){
					File file = new File(uri.getPath());
					if(file.exists()){
						imageId = imageId + "_" + file.lastModified();
					}
				}
			}
		} catch (Exception e) {}
		return mImgCachePath + imageId;
	}
	
	public String urlToPath(String url) {
		return urlToPath(url, false);
	}
	
	public String urlToPath(String url, boolean isMp4) {
		return getPath(getId(url), url) + (isMp4 ? "mp4" : "");
	}
	
	@Override
	public void loadImageFromNetwork(String cacheId, String path, String url,int quality,LoadNetCallback callback) {
		super.loadImageFromNetwork(cacheId, path, url,quality , callback);
	}

	@Override
	public Bitmap loadImageFromResource(String imageCacheId, String path,String url, int quality) {
		if(url != null && url.lastIndexOf(".gif") != -1 && FileUtil.isFileExists(path)){
			int[] wh = BitmapUtil.getLocalWH(path);
			if(wh == null){
				wh = new int[]{1,1};
			}
			return Bitmap.createBitmap(wh[0], wh[1], Config.ARGB_4444);
		}
		return super.loadImageFromResource(imageCacheId, path, url, quality);
	}
	
	@Override
	public String getThumbBasePath() {
		if(mAppPath == null){
			mAppPath = AppFileUtil.getAppPath();
		}
		return mAppPath + "ImgCacheThumb";
	}

	@Override
	public String getThumbPathVersion() {
//		return "1";//2.5.0
//		return "2";//2.5.5
		return "3";//2.5.6
	}
}
