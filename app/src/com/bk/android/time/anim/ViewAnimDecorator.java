package com.bk.android.time.anim;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;

import com.bk.android.app.BaseApp;
import com.bk.android.time.app.App;
/**
 * 控制View的显示和隐藏动画
 * @author Administrator
 *
 */
public class ViewAnimDecorator{
	public static void showView(View view){
		showView(view, true);
	}
	
	public static void showView(View view,boolean isStartAnim){
		clearHideRunnable(view);
		if(view == null || view.getVisibility() == View.VISIBLE){
			return;
		}
		if(isStartAnim){
			view.setAnimation(AnimationUtils.loadAnimation(BaseApp.getInstance(), android.R.anim.fade_in));
		}else{
			view.setAnimation(null);
		}
		view.setVisibility(View.VISIBLE);
	}
	
	private static void clearHideRunnable(final View view){
		Object runnable = view.getTag();
		view.setAnimation(null);
		if(runnable instanceof Runnable){
			App.getHandler().removeCallbacks((Runnable) runnable);
		}
		view.setTag(null);
	}
	
	public static void hideView(final View view){
		hideView(view, true);
	}
	
	public static void hideView(final View view,boolean isStartAnim){
		clearHideRunnable(view);
		if(view == null || view.getVisibility() != View.VISIBLE){
			return;
		}
		final Runnable runnable = new Runnable() {
			@Override
			public void run() {
				if(view.getTag() != null){
					view.setVisibility(View.GONE);
				}
				clearHideRunnable(view);
			}
		};
		view.setTag(runnable);
		if(isStartAnim){
			Animation animation = AnimationUtils.loadAnimation(BaseApp.getInstance(), android.R.anim.fade_out);
			animation.setAnimationListener(new AnimationListener() {
				@Override
				public void onAnimationStart(Animation animation) {}
				
				@Override
				public void onAnimationRepeat(Animation animation) {}
				
				@Override
				public void onAnimationEnd(Animation animation) {
					Object runnable = view.getTag();
					if(runnable instanceof Runnable){
						App.getHandler().post((Runnable) runnable);
					}
				}
			});
			animation.setFillBefore(true);
			view.startAnimation(animation);
			App.getHandler().postDelayed(runnable, animation.getDuration() + 100);
		}else{
			runnable.run();
		}
	}
}
