package com.bk.android.time.anim;

import android.graphics.Canvas;

import com.bk.android.ui.widget.slidingmenu.SlidingMenu.CanvasTransformer;

public class MenuTransformer implements CanvasTransformer{
	
	@Override
	public void transformCanvas(Canvas canvas, int width, int height,
			boolean isTouchScroll,float percentOpen) {
		canvas.scale(percentOpen, percentOpen,canvas.getWidth() / 2, height / 2);
		canvas.translate(-(1f - percentOpen) * width * 1.5f, 0);
	}
}
