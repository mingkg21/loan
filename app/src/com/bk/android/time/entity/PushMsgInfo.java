package com.bk.android.time.entity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mingkg21 on 2016/12/19.
 */

public class PushMsgInfo extends BaseDataEntity {

    public static final String RESOURCE_TYPE_ATLAS = "image_atlas";
    public static final String RESOURCE_TYPE_APP = "package";

    public static final String PUSH_POSITION_INSIDE_GAME  = "inside_game";//游戏内海报
    public static final String PUSH_POSITION_NOTICE_LARGE = "notice_large";//通知栏大海报
    public static final String PUSH_POSITION_NOTICE_SMALL = "notice_small";//通知栏小海报
    public static final String PUSH_POSITION_NOTICE_TEXT  = "notice_text";//通知栏

    private static final long serialVersionUID = -6911094388481517871L;

    @SerializedName("pushId")
    private int pushId;

    @SerializedName("pushTitle")
    private String pushTitle;

    @SerializedName("pushContent")
    private String pushContent;

    @SerializedName("cover")
    private String cover;

    @SerializedName("secondCover")
    private String secondCover;

    @SerializedName("pushPosition")
    private String pushPosition;

    @SerializedName("packageName")
    private String packageName;

    @SerializedName("packageId")
    private String packageId;

    @SerializedName("packageTitle")
    private String packageTitle;

    @SerializedName("icon")
    private String icon;

    @SerializedName("downloadSize")
    private String downloadSize;

    @SerializedName("downloadUrl")
    private String downloadUrl;

    @SerializedName("intervalTime")
    private String intervalTime;

    @SerializedName("pushTicket")
    private String pushTicket;

    @SerializedName("resourceType")
    private String resourceType;

    @SerializedName("resourceId")
    private String resourceId;

    public String getCover() {
        return cover;
    }

    public String getDownloadSize() {
        return downloadSize;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public String getIcon() {
        return icon;
    }

    public String getIntervalTime() {
        return intervalTime;
    }

    public String getPackageId() {
        return packageId;
    }

    public String getPackageName() {
        return packageName;
    }

    public String getPackageTitle() {
        return packageTitle;
    }

    public String getPushContent() {
        return pushContent;
    }

    public int getPushId() {
        return pushId;
    }

    public String getPushPosition() {
        return pushPosition;
    }

    public String getPushTicket() {
        return pushTicket;
    }

    public String getPushTitle() {
        return pushTitle;
    }

    public String getResourceId() {
        return resourceId;
    }

    public String getResourceType() {
        return resourceType;
    }

    public String getSecondCover() {
        return secondCover;
    }
}
