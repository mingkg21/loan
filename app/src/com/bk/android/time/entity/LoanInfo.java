package com.bk.android.time.entity;

import com.google.gson.annotations.SerializedName;


public class LoanInfo extends BaseDataEntity {

    private static final long serialVersionUID = -6587108087459440003L;

    @SerializedName("loanId")
    private long id;

    @SerializedName("loanTitle")
    private String title;

    @SerializedName("icon")
    private String icon;

    @SerializedName("loanUrl")
    private String url;

    @SerializedName("directOpenUrlFlag")
    private int urlFlag;

    @SerializedName("loanSummary")
    private String summary;

    @SerializedName("loanType")
    private String type;

    @SerializedName("interestType")
    private String interestType;

    @SerializedName("minInterestRate")
    private String minInterestRate;

    @SerializedName("maxInterestRate")
    private String maxInterestRate;

    @SerializedName("minLoan")
    private int minLoan;

    @SerializedName("maxLoan")
    private int maxLoan;

    @SerializedName("browseDatetime")
    private String browseDatetime;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getUrlFlag() {
        return urlFlag;
    }

    public void setUrlFlag(int urlFlag) {
        this.urlFlag = urlFlag;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getInterestType() {
        return interestType;
    }

    public void setInterestType(String interestType) {
        this.interestType = interestType;
    }

    public String getMinInterestRate() {
        return minInterestRate;
    }

    public void setMinInterestRate(String minInterestRate) {
        this.minInterestRate = minInterestRate;
    }

    public String getMaxInterestRate() {
        return maxInterestRate;
    }

    public void setMaxInterestRate(String maxInterestRate) {
        this.maxInterestRate = maxInterestRate;
    }

    public int getMinLoan() {
        return minLoan;
    }

    public void setMinLoan(int minLoan) {
        this.minLoan = minLoan;
    }

    public int getMaxLoan() {
        return maxLoan;
    }

    public void setMaxLoan(int maxLoan) {
        this.maxLoan = maxLoan;
    }

    public String getBrowseDatetime() {
        return browseDatetime;
    }

    public void setBrowseDatetime(String browseDatetime) {
        this.browseDatetime = browseDatetime;
    }
}
