package com.bk.android.time.entity;

import com.google.gson.annotations.SerializedName;

public class FeedbackList extends BaseListEntity<FeedbackInfo> {
	private static final long serialVersionUID = 9109539028468551415L;
	@SerializedName("c_qq")
	private String qq;
	@SerializedName("c_mobile")
	private String number;
	@SerializedName("unreadcount")
	private int unReadSize;
	
	/**
	 * @return the qq
	 */
	public String getQq() {
		return qq;
	}
	/**
	 * @param qq the qq to set
	 */
	public void setQq(String qq) {
		this.qq = qq;
	}
	/**
	 * @return the number
	 */
	public String getNumber() {
		return number;
	}
	/**
	 * @param number the number to set
	 */
	public void setNumber(String number) {
		this.number = number;
	}
	/**
	 * @return the unReadSize
	 */
	public int getUnReadSize() {
		return unReadSize;
	}
	/**
	 * @param unReadSize the unReadSize to set
	 */
	public void setUnReadSize(int unReadSize) {
		this.unReadSize = unReadSize;
	}
}
