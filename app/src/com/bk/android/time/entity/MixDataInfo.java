package com.bk.android.time.entity;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.net.URLDecoder;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import android.net.Uri;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

public class MixDataInfo implements Serializable{
	private static final long serialVersionUID = -2564849844375343458L;
	public static final String TAG = MixDataInfo.class.getSimpleName();
	public static final String COMPOSITE_IMG_SEPARATOR = "BKTimeFGInfo=";
	public static final int TYPE_NORMAL = 0;
	public static final int TYPE_STOCK = 1;
	public static final int TYPE_STOCK_TITLE = 2;
	public static final int TYPE_STEP = 3;
	public static final int TYPE_STEP_TITLE = 4;
	
	public static final String CLAZZ_CONTENT = "content";
	public static final String CLAZZ_IMG = "img";
	public static final String CLAZZ_ALBUM = "album";
	public static final String CLAZZ_VIDEO = "video";
	public static final String CLAZZ_ALBUM_COMPLETE = "class='" + CLAZZ_ALBUM + "'";
	
	private int type;
	private String clazz;
	private String text;
	private String dataUrl;
	private long duration;
	private long size;
	private String linkData;
	private String imgUrl;
	private int imgW;
	private int imgH;
	private String foreground;
	private ArrayList<LinkInfo> linkInfos;
	/**
	 * @return the clazz
	 */
	public String getClazz() {
		return clazz;
	}
	/**
	 * @param clazz the clazz to set
	 */
	public void setClazz(String clazz) {
		this.clazz = clazz;
	}
	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}
	/**
	 * @param text the text to set
	 */
	public void setText(String text) {
		this.text = text;
	}
	/**
	 * @return
	 */
	public String getCompositeImgUrl(){
		return getImgUrl() + COMPOSITE_IMG_SEPARATOR + getForeground();
	}
	/**
	 * @return the foreground
	 */
	public String getForeground() {
		return foreground;
//		return "http://osscdn2.banketime.com/apk/_39614459975343409.png?viewX=88&viewY=336&viewW=575&viewH=605&imgW=720&imgH=1156";
	}
	/**
	 * @param foreground the foreground to set
	 */
	public void setForeground(String foreground) {
		this.foreground = foreground;
	}
	/**
	 * @return the linkData
	 */
	public String getLinkData() {
		return linkData;
	}
	/**
	 * @param linkData the linkData to set
	 */
	public void setLinkData(String linkData) {
		this.linkData = linkData;
	}
	/**
	 * @return the imgUrl
	 */
	public String getImgUrl() {
		return imgUrl;
	}
	/**
	 * @param imgUrl the imgUrl to set
	 */
	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}
	/**
	 * @return the imgW
	 */
	public int getImgW() {
		return imgW;
	}
	/**
	 * @param imgW the imgW to set
	 */
	public void setImgW(int imgW) {
		this.imgW = imgW;
	}
	/**
	 * @return the imgH
	 */
	public int getImgH() {
		return imgH;
	}
	/**
	 * @param imgH the imgH to set
	 */
	public void setImgH(int imgH) {
		this.imgH = imgH;
	}
	/**
	 * @return the type
	 */
	public int getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(int type) {
		this.type = type;
	}
	/**
	 * @return the dataUrl
	 */
	public String getDataUrl() {
		return dataUrl;
	}
	/**
	 * @param dataUrl the dataUrl to set
	 */
	public void setDataUrl(String dataUrl) {
		this.dataUrl = dataUrl;
	}
	/**
	 * @return the duration
	 */
	public long getDuration() {
		return duration;
	}
	/**
	 * @param duration the duration to set
	 */
	public void setDuration(long duration) {
		this.duration = duration;
	}
	/**
	 * @return the size
	 */
	public long getSize() {
		return size;
	}
	/**
	 * @param size the size to set
	 */
	public void setSize(long size) {
		this.size = size;
	}
	/**
	 * @return the linkInfos
	 */
	public ArrayList<LinkInfo> getLinkInfos() {
		return linkInfos;
	}
	/**
	 * @param linkInfos the linkInfos to set
	 */
	public void setLinkInfos(ArrayList<LinkInfo> linkInfos) {
		this.linkInfos = linkInfos;
	}
	
	private static void getMixDataInfos(String html,ArrayList<MixDataInfo> mixDataInfos){
		if(!TextUtils.isEmpty(html)){
			StringBuffer sb = new StringBuffer();
			sb.append("<body>");
			sb.append(html);
			sb.append("</body>");
			try {
				DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
				ByteArrayInputStream is = new ByteArrayInputStream(sb.toString().getBytes());
				Document dom = builder.parse(is);
				Element root = dom.getDocumentElement();  
				NodeList items = root.getElementsByTagName("p");
				for (int i = 0; i < items.getLength(); i++) {
					MixDataInfo mixDataInfo = null;
					Element personNode = (Element) items.item(i);
					String classStr = personNode.getAttribute("class");
					String type = personNode.getAttribute("type");
					if(CLAZZ_CONTENT.equals(classStr)){
						mixDataInfo = new MixDataInfo();
						StringBuffer textContent = new StringBuffer();
						NodeList nodeList = personNode.getChildNodes();
						ArrayList<LinkInfo> linkInfos = null;
						if(nodeList != null){
							for (int j = 0; j < nodeList.getLength(); j++) {
								Node childNode = nodeList.item(j);
								int start = textContent.length();
								textContent.append(childNode.getTextContent());
								int end = textContent.length();
								if("a".equalsIgnoreCase(childNode.getNodeName())){
									if(linkInfos == null){
										linkInfos = new ArrayList<LinkInfo>();
									}
									LinkInfo linkInfo = new LinkInfo();
									linkInfo.setStart(start);
									linkInfo.setEnd(end);
									if(childNode instanceof Element){
										Element element = (Element) childNode;
										if(element.hasAttribute("href")){
											String linkData = URLDecoder.decode(element.getAttribute("href"), "UTF-8");
											linkInfo.setData(linkData);
										}
									}
									linkInfos.add(linkInfo);
								}
							}
						}
						mixDataInfo.setLinkInfos(linkInfos);
						mixDataInfo.setText(textContent.toString());
					}else{
						NodeList nodeList = personNode.getElementsByTagName("img");
						if(nodeList != null && nodeList.getLength() > 0){
							mixDataInfo = new MixDataInfo();
							Element childNode = (Element) nodeList.item(0);
							mixDataInfo.setImgUrl(childNode.getAttribute("src"));
							mixDataInfo.setForeground(childNode.getAttribute("foreground"));
							mixDataInfo.setImgW(getIntValue(childNode.getAttribute("imgWidth")));
							mixDataInfo.setImgH(getIntValue(childNode.getAttribute("imgHeight")));
							
							if(childNode.getParentNode() instanceof Element){
								Element parentElement = (Element) childNode.getParentNode();
								if("a".equalsIgnoreCase(parentElement.getNodeName())){
									if(parentElement.hasAttribute("href")){
										String linkData = URLDecoder.decode(parentElement.getAttribute("href"), "UTF-8");
										mixDataInfo.setLinkData(linkData);
									}
								}
							}
							
							if(CLAZZ_VIDEO.equals(classStr)){
								mixDataInfo.setDataUrl(personNode.getAttribute("dataUrl"));
								String duration = personNode.getAttribute("duration");
								if(!TextUtils.isEmpty(duration) && TextUtils.isDigitsOnly(duration)){
									mixDataInfo.setDuration(Long.valueOf(duration));
								}
								String size = personNode.getAttribute("size");
								if(!TextUtils.isEmpty(size) && TextUtils.isDigitsOnly(size)){
									mixDataInfo.setSize(Long.valueOf(size));
								}
							}
						}
					}
					if(mixDataInfo != null){
						if(!TextUtils.isEmpty(type) && TextUtils.isDigitsOnly(type)){
							mixDataInfo.setType(Integer.valueOf(type));
						}
						mixDataInfo.setClazz(classStr);
						mixDataInfos.add(mixDataInfo);
					}
				}
				is.close();
			} catch (SAXException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ParserConfigurationException e) {
				e.printStackTrace();
			}
		}
	}

	public static ArrayList<MixDataInfo> getMixDataInfos(String html){
		ArrayList<MixDataInfo> mixDataInfos = new ArrayList<MixDataInfo>();
		try {
			getMixDataInfos(html,mixDataInfos);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mixDataInfos;
	}
	
	public static ArrayList<MixDataInfo> getMixDataInfosNotCatch(String html){
		ArrayList<MixDataInfo> mixDataInfos = new ArrayList<MixDataInfo>();
		getMixDataInfos(html,mixDataInfos);
		return mixDataInfos;
	}
	
	public static String getHtml(ArrayList<MixDataInfo> mixDataInfos){
		if(mixDataInfos == null || mixDataInfos.isEmpty()){
			return null;
		}
		StringBuffer html = new StringBuffer();
		for (MixDataInfo mixDataInfo : mixDataInfos) {
			html.append(mixDataInfoToString(mixDataInfo));
		}
		return html.toString();
	}
	
	public static String photoVideoDataToString(String url,long duration,long size,String coverUrl,int w,int h){
		StringBuffer html = new StringBuffer();
		html.append("<p class='"+MixDataInfo.CLAZZ_VIDEO+"' dataUrl='"+formatAttribute(url)+"' duration='"+duration+"' size='"+size+"'>");
		html.append("<img src='"+formatAttribute(coverUrl)+"' imgWidth='"+w+"' imgHeight='"+h+"'/>");
		html.append("</p>");
		return html.toString();
	}
	
	public static String photoAlbumDataToString(String url,int w,int h){
		return photoAlbumDataToString(url, w, h,null);
	}
	
	public static String photoAlbumDataToString(String url,int w,int h,String foreground){
		StringBuffer html = new StringBuffer();
		html.append("<p class='"+MixDataInfo.CLAZZ_ALBUM+"'>");
		if(!TextUtils.isEmpty(foreground)){
			html.append("<img src='"+formatAttribute(url)+"' imgWidth='"+w+"' imgHeight='"+h+"' foreground='"+formatAttribute(foreground)+"'/>");
		}else{
			html.append("<img src='"+formatAttribute(url)+"' imgWidth='"+w+"' imgHeight='"+h+"'/>");
		}
		html.append("</p>");
		return html.toString();
	}
	
	public static String photoDataToString(String url,int w,int h){
		StringBuffer html = new StringBuffer();
		html.append("<p class='"+MixDataInfo.CLAZZ_IMG+"'>");
		html.append("<img src='"+formatAttribute(url)+"' imgWidth='"+w+"' imgHeight='"+h+"'/>");
		html.append("</p>");
		return html.toString();
	}
	
	public static String textDataToString(String text){
		StringBuffer html = new StringBuffer();
		html.append("<p class='"+MixDataInfo.CLAZZ_CONTENT+"'>");
		html.append(MixDataInfo.formatCDATA(text));
		html.append("</p>");
		return html.toString();
	}
	
	public static String mixDataInfoToString(MixDataInfo mixDataInfo){
		StringBuffer html = new StringBuffer();
		String clazz = mixDataInfo.getClazz();
		if(!TextUtils.isEmpty(mixDataInfo.getText())){
			if(TextUtils.isEmpty(clazz)){
				clazz = MixDataInfo.CLAZZ_CONTENT;
			}
			html.append("<p class='"+clazz+"'"+getType(mixDataInfo.getType())+">");
			html.append(formatCDATA(mixDataInfo.getText()));
			html.append("</p>");
		}
		if(!TextUtils.isEmpty(mixDataInfo.getImgUrl())){
			if(TextUtils.isEmpty(clazz)){
				clazz = MixDataInfo.CLAZZ_IMG;
			}
			if(MixDataInfo.CLAZZ_VIDEO.equals(clazz)){
				html.append("<p class='"+MixDataInfo.CLAZZ_VIDEO+"' dataUrl='"+formatAttribute(mixDataInfo.getDataUrl())+"' duration='"+mixDataInfo.getDuration()+"' size='"+mixDataInfo.getSize()+"'>");
			}else{
				html.append("<p class='"+clazz+"'"+getType(mixDataInfo.getType())+">");
			}
			if(!TextUtils.isEmpty(mixDataInfo.getForeground())){
				html.append("<img src='"+formatAttribute(mixDataInfo.getImgUrl())+"' imgWidth='"+mixDataInfo.getImgW()+"' imgHeight='"+mixDataInfo.getImgH()+"' foreground='"+formatAttribute(mixDataInfo.getForeground())+"'/>");
			}else{
				html.append("<img src='"+formatAttribute(mixDataInfo.getImgUrl())+"' imgWidth='"+mixDataInfo.getImgW()+"' imgHeight='"+mixDataInfo.getImgH()+"'/>");
			}
			html.append("</p>");
		}
		return html.toString();
	}
	
	public static String getType(int type){
		if(TYPE_STEP == type){
			return " type='"+TYPE_STEP+"' ";
		}else if(TYPE_STEP_TITLE == type){
			return " type='"+TYPE_STEP_TITLE+"' ";
		}else if(TYPE_STOCK == type){
			return " type='"+TYPE_STOCK+"' ";
		}else if(TYPE_STOCK_TITLE == type){
			return " type='"+TYPE_STOCK_TITLE+"' ";
		}else{
			return "";
		}
	}
	
	public static String filterPlaceholder(CharSequence text){
		StringBuffer sb = new StringBuffer();
		int serialSpaceSize = 0;
		for (int i = 0; i < text.length(); i++) {
			char c = text.charAt(i);
			if(c != '\uFFFC'){
				if(c == '&'){
					sb.append("＆");
				}else if(c == '<'){
					sb.append("《");
				}else if(c == '>'){
					sb.append("》");
				}else if(c == ' '){
					serialSpaceSize++;
					sb.append(c);
					if(serialSpaceSize == 2){
						sb.replace(sb.length() - 2, sb.length(), "　");
						serialSpaceSize = 0;
					}
				}else if(c == 9){
					sb.append("　　");
				}else{
					sb.append(c);
				}
			}
			if(c != ' '){
				serialSpaceSize = 0;
			}
		}
		return sb.toString();
	}
	
	public static String formatCDATA(CharSequence str){
//		StringBuffer sb = new StringBuffer();
//		sb.append("<![CDATA["+str+"]]>");
//		return sb.toString();
		return filterPlaceholder(str);
	}
	
	public static String formatAttribute(String str){
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			if(c == '&'){
				sb.append("&amp;");
			}else if(c == '<'){
				sb.append("&lt;");
			}else if(c == '>'){
				sb.append("&gt;");
			}else if(c == '"'){
				sb.append("&quot;");
			}else if(c == '\''){
				sb.append("&apos;");
			}else{
				sb.append(c);
			}
		}
		return sb.toString();
	}
	
	private static int getIntValue(String value){
		if(!TextUtils.isEmpty(value)){
			try {
				return Float.valueOf(value).intValue();
			} catch (Exception e) {}
		}
		return 0;
	}

	public static class MediaData implements Serializable{
		private static final long serialVersionUID = 134045712244515507L;
		public static final int TYPE_IMG = 1;//1图片，2音频，3视频)
		public static final int TYPE_AUDIO = 2;
		public static final int TYPE_VIDEO = 3;
		public static final int TYPE_TEXT_IMG = 4;

		public static final int SOURCE_DEFAULT = 1;//1系统默认，2云相册，3自定义
		public static final int SOURCE_PHOTO = 2;
		public static final int SOURCE_CUSTOM = 3;
		public static final int SOURCE_TEXT = 4;

		@SerializedName("src")
		private String src;
		@SerializedName("type")
		private int type;
		@SerializedName("source")
		private int source;
		@SerializedName("width")
		private float width;
		@SerializedName("height")
		private float height;
		@SerializedName("length")
		private long length;
		
		public boolean isCustomImg(){
			return type == TYPE_IMG && source == SOURCE_CUSTOM;
		}
		
		public boolean needUploadImg(){
			String scheme = "";
			if(src != null){
				scheme = Uri.parse(src).getScheme();
				if(scheme != null){
					scheme = scheme.toLowerCase();
				}
			}
			return src != null && !"http".equals(scheme);
		}
		/**
		 * @return the source
		 */
		public int getSource() {
			return source;
		}
		/**
		 * @param source the source to set
		 */
		public void setSource(int source) {
			this.source = source;
		}
		/**
		 * @return the src
		 */
		public String getSrc() {
			return src;
		}
		/**
		 * @param src the src to set
		 */
		public void setSrc(String src) {
			this.src = src;
		}
		/**
		 * @return the type
		 */
		public int getType() {
			return type;
		}
		/**
		 * @param type the type to set
		 */
		public void setType(int type) {
			this.type = type;
		}
		/**
		 * @return the width
		 */
		public int getWidth() {
			return (int) width;
		}
		/**
		 * @param width the width to set
		 */
		public void setWidth(int width) {
			this.width = width;
		}
		/**
		 * @return the height
		 */
		public int getHeight() {
			return (int) height;
		}
		/**
		 * @param height the height to set
		 */
		public void setHeight(int height) {
			this.height = height;
		}
		/**
		 * @return the length
		 */
		public long getLength() {
			return length;
		}
		/**
		 * @param length the length to set
		 */
		public void setLength(long length) {
			this.length = length;
		}
	}
	
	public static class LinkInfo implements Serializable{
		private static final long serialVersionUID = 7265565214333596972L;
		private int start;
		private int end;
		private String data;
		/**
		 * @return the start
		 */
		public int getStart() {
			return start;
		}
		/**
		 * @param start the start to set
		 */
		public void setStart(int start) {
			this.start = start;
		}
		/**
		 * @return the end
		 */
		public int getEnd() {
			return end;
		}
		/**
		 * @param end the end to set
		 */
		public void setEnd(int end) {
			this.end = end;
		}
		/**
		 * @return the data
		 */
		public String getData() {
			return data;
		}
		/**
		 * @param data the data to set
		 */
		public void setData(String data) {
			this.data = data;
		}
	}
}
