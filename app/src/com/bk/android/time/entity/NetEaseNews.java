package com.bk.android.time.entity;

import com.google.gson.annotations.SerializedName;

public class NetEaseNews extends BaseDataEntity {

    private static final long serialVersionUID = -5951596999193760557L;

    @SerializedName("title")
    private String title;

    @SerializedName("imgsrc")
    private String icon;

    @SerializedName("ptime")
    private String time;

    @SerializedName("postid")
    private String id;

    @SerializedName("body")
    private String body;

    public String getTitle() {
        return title;
    }

    public String getIcon() {
        return icon;
    }

    public String getTime() {
        return time;
    }

    public String getId() {
        return id;
    }

    public String getBody() {
        return body;
    }
}
