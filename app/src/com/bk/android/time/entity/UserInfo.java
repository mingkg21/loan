package com.bk.android.time.entity;

import com.google.gson.annotations.SerializedName;

public class UserInfo extends BaseDataEntity {
	
	private static final long serialVersionUID = 4374536384500774309L;

	public static final int RELATION_NOT = 0;//没任何关系
	public static final int RELATION_ONE = 1;//单边关系，我关注的，或关注我的
	public static final int RELATION_BOTH = 2;//互粉
	
	public static final int ROLE_USER = 1;//普通用户
	public static final int ROLE_SHOP = 2;//商家
	public static final int ROLE_EXPERT = 3;//专家
	public static final int ROLE_TARENTO = 4;//达人
	public static final int ROLE_OFFICIAL = 5;//官方
	
	@SerializedName("userId")
	private String id;
	@SerializedName("displayName")
	private String name;
	@SerializedName("icon")
	private String icon;
	@SerializedName("phone")
	private String phone;
	@SerializedName("experience")
	private int experience;
	@SerializedName("point")
	private int point;
	@SerializedName("currentLevel")
	private int currentLevel;
	@SerializedName("signFlag")
	private int signFlag;
	@SerializedName("defaultIconFlag")
	private int defaultIconFlag;

	//个人信息
	@SerializedName("realName")
	private String realName;
	@SerializedName("locationCity")
	private String locationCity;
	@SerializedName("idCard")
	private String idCard;
	@SerializedName("marryFlag")
	private String marryFlag;
	@SerializedName("educationLevel")
	private String educationLevel;

	//身份信息
	@SerializedName("profession")
	private String profession;
	@SerializedName("incomeType")
	private String incomeType;
	@SerializedName("incomeValue")
	private String incomeValue;
	@SerializedName("companyType")
	private String companyType;
	@SerializedName("companyWorkTime")
	private String companyWorkTime;
	@SerializedName("moreSixFundFlag")
	private String moreSixFundFlag;
	@SerializedName("moreSixSocialFlag")
	private String moreSixSocialFlag;

	//房产信息
	@SerializedName("hasCreditFlag")
	private String hasCreditFlag;
	@SerializedName("containHouseType")
	private String containHouseType;
	@SerializedName("containCarType")
	private String containCarType;
	@SerializedName("successLoanFlag")
	private String successLoanFlag;
	@SerializedName("creditStanding")
	private String creditStanding;

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the icon
	 */
	public String getIcon() {
		return icon;
	}
	/**
	 * @param icon the icon to set
	 */
	public void setIcon(String icon) {
		this.icon = icon;
	}
	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}
	/**
	 * @param phone the contact to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	public int getCurrentLevel() {
		return currentLevel;
	}

	public void setCurrentLevel(int currentLevel) {
		this.currentLevel = currentLevel;
	}

	public int getExperience() {
		return experience;
	}

	public void setExperience(int experience) {
		this.experience = experience;
	}

	public int getPoint() {
		return point;
	}

	public void setPoint(int point) {
		this.point = point;
	}

	public int getSignFlag() {
		return signFlag;
	}

	public void setSignFlag(int signFlag) {
		this.signFlag = signFlag;
	}

	public int getDefaultIconFlag() {
		return defaultIconFlag;
	}

	public void setDefaultIconFlag(int defaultIconFlag) {
		this.defaultIconFlag = defaultIconFlag;
	}

	public boolean isDefaultIconFlag() {
		return defaultIconFlag == 1;
	}

	public String getLocationCity() {
		return locationCity;
	}

	public void setLocationCity(String locationCity) {
		this.locationCity = locationCity;
	}

	public String getIdCard() {
		return idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}

	public String getMarryFlag() {
		return marryFlag;
	}

	public void setMarryFlag(String marryFlag) {
		this.marryFlag = marryFlag;
	}

	public String getEducationLevel() {
		return educationLevel;
	}

	public void setEducationLevel(String educationLevel) {
		this.educationLevel = educationLevel;
	}

	public String getProfession() {
		return profession;
	}

	public void setProfession(String profession) {
		this.profession = profession;
	}

	public String getIncomeType() {
		return incomeType;
	}

	public void setIncomeType(String incomeType) {
		this.incomeType = incomeType;
	}

	public String getIncomeValue() {
		return incomeValue;
	}

	public void setIncomeValue(String incomeValue) {
		this.incomeValue = incomeValue;
	}

	public String getCompanyType() {
		return companyType;
	}

	public void setCompanyType(String companyType) {
		this.companyType = companyType;
	}

	public String getCompanyWorkTime() {
		return companyWorkTime;
	}

	public void setCompanyWorkTime(String companyWorkTime) {
		this.companyWorkTime = companyWorkTime;
	}

	public String getMoreSixFundFlag() {
		return moreSixFundFlag;
	}

	public void setMoreSixFundFlag(String moreSixFundFlag) {
		this.moreSixFundFlag = moreSixFundFlag;
	}

	public String getMoreSixSocialFlag() {
		return moreSixSocialFlag;
	}

	public void setMoreSixSocialFlag(String moreSixSocialFlag) {
		this.moreSixSocialFlag = moreSixSocialFlag;
	}

	public String getHasCreditFlag() {
		return hasCreditFlag;
	}

	public void setHasCreditFlag(String hasCreditFlag) {
		this.hasCreditFlag = hasCreditFlag;
	}

	public String getContainHouseType() {
		return containHouseType;
	}

	public void setContainHouseType(String containHouseType) {
		this.containHouseType = containHouseType;
	}

	public String getContainCarType() {
		return containCarType;
	}

	public void setContainCarType(String containCarType) {
		this.containCarType = containCarType;
	}

	public String getSuccessLoanFlag() {
		return successLoanFlag;
	}

	public void setSuccessLoanFlag(String successLoanFlag) {
		this.successLoanFlag = successLoanFlag;
	}

	public String getCreditStanding() {
		return creditStanding;
	}

	public void setCreditStanding(String creditStanding) {
		this.creditStanding = creditStanding;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	@Override
	public boolean equals(Object o) {
		boolean isEquals = super.equals(o);
		if(!isEquals){
			if(o instanceof UserInfo){
				UserInfo temp = (UserInfo) o;
				if(equalsValue(this.icon, temp.icon)
						&& equalsValue(this.id, temp.id)
						&& equalsValue(this.name, temp.name)
						){
					isEquals = true;
				}
			}
		}
		return isEquals;
	}
	
	private boolean equalsValue(String value,String tempValue){
		return value != null ? value.equals(tempValue) : value == tempValue;
	}
	
}
