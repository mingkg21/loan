package com.bk.android.time.entity;

import com.google.gson.annotations.SerializedName;

public class MessageInfo extends BaseDataEntity {

    private static final long serialVersionUID = -1871336975290636219L;

    @SerializedName("msgTitle")
    private String title;

    @SerializedName("msgContent")
    private String content;

    @SerializedName("activityUrl")
    private String url;

    @SerializedName("activityCover")
    private String cover;

    @SerializedName("pushTime")
    private String time;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
