package com.bk.android.time.entity;

import com.google.gson.annotations.SerializedName;

public class ServerMessageList extends BaseListEntity<ServerMessageInfo> {
	private static final long serialVersionUID = -550694027705008665L;
	@SerializedName("unreadcount")
	private int unreadcount;
	/**
	 * @return the unreadcount
	 */
	public int getUnreadcount() {
		return unreadcount;
	}

	/**
	 * @param unreadcount the unreadcount to set
	 */
	public void setUnreadcount(int unreadcount) {
		this.unreadcount = unreadcount;
	}

}
