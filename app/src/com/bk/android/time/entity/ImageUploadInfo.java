package com.bk.android.time.entity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mingkg21 on 2016/12/16.
 */

public class ImageUploadInfo extends BaseDataEntity {

    private static final long serialVersionUID = 4737385208671155277L;

    @SerializedName("uploadToken")
    private String uploadToken;

    @SerializedName("uploadId")
    private String uploadId;

    @SerializedName("keyPrefix")
    private String keyPrefix;

    public String getKeyPrefix() {
        return keyPrefix;
    }

    public String getUploadId() {
        return uploadId;
    }

    public String getUploadToken() {
        return uploadToken;
    }
}
