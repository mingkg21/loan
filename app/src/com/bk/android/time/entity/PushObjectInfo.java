package com.bk.android.time.entity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mingkg21 on 2016/12/19.
 */

public class PushObjectInfo extends BaseDataEntity {

    private static final long serialVersionUID = 9217452533768169236L;

    @SerializedName("object_id")
    private String objectId;

    @SerializedName("object_title")
    private String objectTitle;

    public String getObjectId() {
        return objectId;
    }

    public String getObjectTitle() {
        return objectTitle;
    }
}
