package com.bk.android.time.entity;

import java.util.ArrayList;

import com.google.gson.annotations.SerializedName;

public class BaseListEntity<Data> extends BaseDataEntity{
	private static final long serialVersionUID = 5722884158764559532L;
	@SerializedName("list")
	private ArrayList<Data> dataList;
	@SerializedName("cur_offset")
	private int curOffset;
	@SerializedName("next")
	private int hasnext;
	@SerializedName("offset")
	private int nextstartpos;
	private int haspre;

	private int prestartpos;
	/**
	 * @return the dataList
	 */
	public ArrayList<Data> getDataList() {
		return dataList;
	}
	/**
	 * @param dataList the dataList to set
	 */
	public void setDataList(ArrayList<Data> dataList) {
		this.dataList = dataList;
	}
	/**
	 * @return the curOffset
	 */
	public int getCurOffset() {
		return curOffset;
	}
	/**
	 * @param curOffset the curOffset to set
	 */
	public void setCurOffset(int curOffset) {
		this.curOffset = curOffset;
	}
	/**
	 * @return the hasnext
	 */
	public boolean hasnext() {
		return hasnext == 1;
	}
	/**
	 * @param hasnext the hasnext to set
	 */
	public void setHasnext(boolean hasnext) {
		this.hasnext = hasnext ? 1 : 0;
	}
	/**
	 * @return the nextstartpos
	 */
	public int getNextstartpos() {
		return nextstartpos;
	}
	/**
	 * @param nextstartpos the nextstartpos to set
	 */
	public void setNextstartpos(int nextstartpos) {
		this.nextstartpos = nextstartpos;
	}
	/**
	 * @return the haspre
	 */
	public boolean haspre() {
		return haspre == 1;
	}
	/**
	 * @param haspre the haspre to set
	 */
	public void setHaspre(boolean haspre) {
		this.haspre = haspre ? 1 : 0;
	}
	/**
	 * @return the prestartpos
	 */
	public int getPrestartpos() {
		return prestartpos;
	}
	/**
	 * @param prestartpos the prestartpos to set
	 */
	public void setPrestartpos(int prestartpos) {
		this.prestartpos = prestartpos;
	}
}
