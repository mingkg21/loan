package com.bk.android.time.entity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mingkg21 on 2016/12/13.
 */

public class LoginInfo extends BaseDataEntity {

    private static final long serialVersionUID = 3696842654445789591L;

    @SerializedName("authorization_token")
    private String authorizationToken;

    @SerializedName("user_id")
    private String userId;

    @SerializedName("display_name")
    private String displayName;

    public String getAuthorizationToken() {
        return authorizationToken;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getUserId() {
        return userId;
    }

}
