package com.bk.android.time.entity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mingkg21 on 2016/12/22.
 */

public class Comment extends BaseDataEntity {

    private static final long serialVersionUID = 2452128135724381068L;

    @SerializedName("commentId")
    private long id;

    @SerializedName("atlasId")
    private int atlasId;

    @SerializedName("comment")
    private String comment;

    @SerializedName("floor")
    private int floor;

    @SerializedName("userName")
    private String userName;

    @SerializedName("userIcon")
    private String userIcon;

    @SerializedName("userLevel")
    private int userLevel;

    @SerializedName("submitDate")
    private long submitDate;

    @SerializedName("praiseCnt")
    private int praiseCnt;

    @SerializedName("isPraise")
    private boolean isPraise;

    public int getAtlasId() {
        return atlasId;
    }

    public String getComment() {
        return comment;
    }

    public int getFloor() {
        return floor;
    }

    public long getId() {
        return id;
    }

    public boolean isPraise() {
        return isPraise;
    }

    public int getPraiseCnt() {
        return praiseCnt;
    }

    public long getSubmitDate() {
        return submitDate;
    }

    public String getUserIcon() {
        return userIcon;
    }

    public int getUserLevel() {
        return userLevel;
    }

    public String getUserName() {
        return userName;
    }


    public void setAtlasId(int atlasId) {
        this.atlasId = atlasId;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public void setFloor(int floor) {
        this.floor = floor;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setPraise(boolean praise) {
        isPraise = praise;
    }

    public void setPraiseCnt(int praiseCnt) {
        this.praiseCnt = praiseCnt;
    }

    public void setSubmitDate(long submitDate) {
        this.submitDate = submitDate;
    }

    public void setUserIcon(String userIcon) {
        this.userIcon = userIcon;
    }

    public void setUserLevel(int userLevel) {
        this.userLevel = userLevel;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
