package com.bk.android.time.entity;

import com.google.gson.annotations.SerializedName;

public class FeedbackInfo extends BaseDataEntity {
	private static final long serialVersionUID = 3452223681112919214L;
	@SerializedName("f_createtime")
	private long createTime;
	@SerializedName("f_content")
	private String content;
	/** 1代表提交了。 2代表回复的 */
	@SerializedName("f_isadmin")
	private int isSelf;
	/** 1代表未读 2代表已读  */
	@SerializedName("f_status")
	private int isRead;
	/**
	 * @return the createTime
	 */
	public long getCreateTime() {
		return createTime * 1000;
	}
	/**
	 * @param createTime the createTime to set
	 */
	public void setCreateTime(long createTime) {
		this.createTime = createTime;
	}
	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}
	/**
	 * @param content the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}
	/**
	 * @return the isSelf
	 */
	public boolean isSelf() {
		return isSelf == 1;
	}
	/**
	 * @param isSelf the isSelf to set
	 */
	public void setIsSelf(boolean isSelf) {
		this.isSelf = isSelf ? 1: 0;
	}
	/**
	 * @return the isRead
	 */
	public boolean isRead() {
		return isRead == 2;
	}
	/**
	 * @param isRead the isRead to set
	 */
	public void setIsRead(boolean isRead) {
		this.isRead = isRead ? 2: 1;
	}
}
