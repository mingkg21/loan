package com.bk.android.time.entity;

import com.google.gson.annotations.SerializedName;

public class Config extends BaseDataEntity {

    private static final long serialVersionUID = -4476208804672165242L;

    @SerializedName("clientShowSettingFlag")
    private String clientShowSettingFlag;

    public boolean isClientShwow() {
        return "show".equals(clientShowSettingFlag);
    }
}
