package com.bk.android.time.entity;

import com.google.gson.annotations.SerializedName;

public class BankQuery extends BaseDataEntity {

    private static final long serialVersionUID = -5318207622380182104L;

    @SerializedName("bankQryName")
    private String queryName;

    @SerializedName("bankName")
    private String bankName;

    @SerializedName("bankLogo")
    private String bankLogo;

    @SerializedName("queryProUrl")
    private String queryUrl;

    public String getQueryName() {
        return queryName;
    }

    public String getBankName() {
        return bankName;
    }

    public String getBankLogo() {
        return bankLogo;
    }

    public String getQueryUrl() {
        return queryUrl;
    }
}
