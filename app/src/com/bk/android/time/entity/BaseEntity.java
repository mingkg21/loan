package com.bk.android.time.entity;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class BaseEntity<Data extends Serializable> implements Serializable {

	public static final String CODE_SUCCESS = "0000";
	public static final String CODE_FAIL = "0";
	public static final String CODE_PARSE_FAIL = "-1";

	public static final String CODE_ILLEGAL_WORDS = "13559";

	private static final long serialVersionUID = 5995549456960762303L;
	
	@SerializedName("code")
	private String resultCode;
	
	@SerializedName("httpcode")
	private String httpCode;
	
	@SerializedName("msg")
	private String errMsg;

	@SerializedName("functionCode")
	private String functionCode;

	@SerializedName("isSuccess")
	private boolean isSuccess;
	
	@SerializedName("results")
	private Data data;

	@SerializedName("next")
	private String next;

	@SerializedName("totalPages")
	private String totalPages;

	@SerializedName("curPage")
	private String curPage;

	@SerializedName("pageSize")
	private String pageSize;

	@SerializedName("bcoin")
	private int coin;

	/**
	 * @return the errMsg
	 */
	public String getErrMsg() {
		return errMsg;
	}

	/**
	 * @param errMsg the errMsg to set
	 */
	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

	/**
	 * @return the httpCode
	 */
	public String getHttpCode() {
		return httpCode;
	}

	/**
	 * @param httpCode the httpCode to set
	 */
	public void setHttpCode(String httpCode) {
		this.httpCode = httpCode;
	}

	/**
	 * @return the resultCode
	 */
	public String getResultCode() {
		return resultCode;
	}

	/**
	 * @param resultCode the resultCode to set
	 */
	public BaseEntity<Data> setResultCode(String resultCode) {
		this.resultCode = resultCode;
		return this;
	}

	/**
	 * @return the data
	 */
	public Data getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(Data data) {
		this.data = data;
	}
	
	public boolean isSucceed(){
//		return CODE_SUCCESS.equals(getResultCode());
		return isSuccess;
	}

	public int getCoin() {
		return coin;
	}

	private int stringToInt(String value) {
		if (!TextUtils.isEmpty(value) && TextUtils.isDigitsOnly(value)) {
			return Integer.valueOf(value);
		}
		return 0;
	}

	public int getCurPage() {
		return stringToInt(curPage);
	}

	public String getFunctionCode() {
		return functionCode;
	}

	public boolean isSuccess() {
		return isSuccess;
	}

	public String getNext() {
		return next;
	}

	public String getPageSize() {
		return pageSize;
	}

	public int getTotalPages() {
		return stringToInt(totalPages);
	}

	public int getNextPage() {
		return getCurPage() + 1;
	}

	public boolean haveNext() {
		return getCurPage() < getTotalPages();
	}
}
