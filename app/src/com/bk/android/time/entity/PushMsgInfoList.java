package com.bk.android.time.entity;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by mingkg21 on 2016/12/28.
 */

public class PushMsgInfoList extends BaseDataEntity {

    private static final long serialVersionUID = 7261844907394698459L;

    @SerializedName("pushMsgList")
    private ArrayList<PushMsgInfo> pushMsgList;

    @SerializedName("intervalTime")
    private String intervalTime;

    public String getIntervalTime() {
        return intervalTime;
    }

    public ArrayList<PushMsgInfo> getPushMsgList() {
        return pushMsgList;
    }
}
