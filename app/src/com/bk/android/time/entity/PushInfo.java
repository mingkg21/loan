package com.bk.android.time.entity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mingkg21 on 2016/12/19.
 */

public class PushInfo extends BaseDataEntity {

    private static final long serialVersionUID = 8077913078500405647L;

    public static final String ACTION_ATLAS = "image_atlas";
    public static final String ACTION_APP = "package";

    @SerializedName("click")
    private int click;

    @SerializedName("msg")
    private String msg;

    @SerializedName("action")
    private String action;

    @SerializedName("notice")
    private String notice;

    @SerializedName("user_id")
    private String userId;

    @SerializedName("results")
    private PushObjectInfo results;

    public String getAction() {
        return action;
    }

    public int getClick() {
        return click;
    }

    public String getMsg() {
        return msg;
    }

    public String getNotice() {
        return notice;
    }

    public PushObjectInfo getResults() {
        return results;
    }

    public String getUserId() {
        return userId;
    }
}
