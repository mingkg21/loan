package com.bk.android.time.entity;

import com.google.gson.annotations.SerializedName;


public class ServerMessageInfo extends BaseDataEntity {
	public static final int ACTION_POST_INFO = 1;//帖子
	public static final int ACTION_BOARD_INFO = 2;//帖子列表
	public static final int ACTION_NEWS = 3;//资讯
	public static final int ACTION_INTERIOR_URL = 4;//内部网页
	public static final int ACTION_EXTERNAL_URL = 5;//外部网页
	public static final int ACTION_BABY_NEW_RECORD = 7;//新宝宝记录
	public static final int ACTION_BABY_SPACE_NEW_MEMBER = 8;//新成员加入
	public static final int ACTION_BABY_NEW_COMMENT = 9;//新宝宝记录评论
	public static final int ACTION_BABY_NEW_PRAISE = 10;//新宝宝记录点赞
	public static final int ACTION_BABY_ALBUM_TEMPLET = 11;//打开宝宝影集模板
	public static final int ACTION_ORDER_DETAILS = 17;//跳转订单详情页面

	private static final long serialVersionUID = 5773070260499714904L;
	@SerializedName("um_id")
    private int id;
	@SerializedName("um_title")
	private String title;
	@SerializedName("um_content")
	private String content;
	@SerializedName("um_createtime")
	private long createtime;
	@SerializedName("um_to_uid")
	private String umToUid;
	@SerializedName("uname")
	private String userName;
	@SerializedName("uicon")
	private String userIcon;
	@SerializedName("um_status")
	private int status;
	@SerializedName("um_action")
	private int action;
	@SerializedName("um_action_data")
	private String actionData;
	@SerializedName("br_mix")
	private String mixContent;
	@SerializedName("b_id")
	private String babyId;
	@SerializedName("uid")
	private String userId;
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}
	/**
	 * @param content the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}
	/**
	 * @return the createtime
	 */
	public long getCreatetime() {
		return createtime * 1000;
	}
	/**
	 * @param createtime the createtime to set
	 */
	public void setCreatetime(long createtime) {
		this.createtime = createtime / 1000;
	}
	/**
	 * @return the umToUid
	 */
	public String getUmToUid() {
		return umToUid;
	}
	/**
	 * @param umToUid the umToUid to set
	 */
	public void setUmToUid(String umToUid) {
		this.umToUid = umToUid;
	}
	/**
	 * @return the status
	 */
	public boolean isReaded() {
		return status == 1;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(boolean isReaded) {
		this.status = isReaded ? 1 : 2;
	}
	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}
	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	/**
	 * @return the userIcon
	 */
	public String getUserIcon() {
		return userIcon;
	}
	/**
	 * @param userIcon the userIcon to set
	 */
	public void setUserIcon(String userIcon) {
		this.userIcon = userIcon;
	}
	
	/**
	 * @return the action
	 */
	public int getAction() {
		return action;
	}
	/**
	 * @param action the action to set
	 */
	public void setAction(int action) {
		this.action = action;
	}
	/**
	 * @return the actionData
	 */
	public String getActionData() {
		return actionData;
	}
	/**
	 * @param actionData the actionData to set
	 */
	public void setActionData(String actionData) {
		this.actionData = actionData;
	}
	public String getMixContent() {
		return mixContent;
	}
	public void setMixContent(String mixContent) {
		this.mixContent = mixContent;
	}
	public String getBabyId() {
		return babyId;
	}
	public void setBabyId(String babyId) {
		this.babyId = babyId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
}
