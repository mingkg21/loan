package com.bk.android.time.data.dao;

import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;

import com.bk.android.app.BaseApp;
import com.bk.android.assistant.BuildConfig;
import com.bk.android.dao.DBKeyValueProvider;
import com.bk.android.dao.DBKeyValueProviderProxy;

public class DBPreferencesProvider extends DBKeyValueProvider{
	private static final String DB_NAME = "KeyValueDB";
	private static final String DB_TABLE = "KeyValueTable";
	
	private static final String CONTENT_URI_AUTHORITIES = BuildConfig.APPLICATION_ID  + ".UserDataProvider";
	private static final String CONTENT_URI_PATH_RW = "rw";
	private static final int PATH_RW = 0;
	
	public static final Uri CONTENT_URI = Uri.parse("content://" + CONTENT_URI_AUTHORITIES + "/" + CONTENT_URI_PATH_RW);
	
    private static final UriMatcher sURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    
    static {
        sURIMatcher.addURI(CONTENT_URI_AUTHORITIES,CONTENT_URI_PATH_RW,PATH_RW);
	}
	
	private static DBKeyValueProviderProxy mProxy;

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		if (sURIMatcher.match(uri) != PATH_RW) {
            throw new IllegalArgumentException("Unknown/Invalid URI " + uri);
        }
		return super.query(uri, projection, selection, selectionArgs, sortOrder);
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		if (sURIMatcher.match(uri) != PATH_RW) {
            throw new IllegalArgumentException("Unknown/Invalid URI " + uri);
        }
		return super.insert(uri, values);
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		if (sURIMatcher.match(uri) != PATH_RW) {
            throw new IllegalArgumentException("Unknown/Invalid URI " + uri);
        }
		return super.delete(uri, selection, selectionArgs);
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,String[] selectionArgs) {
		if (sURIMatcher.match(uri) != PATH_RW) {
            throw new IllegalArgumentException("Unknown/Invalid URI " + uri);
        }
		return super.update(uri, values, selection, selectionArgs);
	}

	@Override
	protected String getTableName() {
		return DB_TABLE;
	}

	@Override
	protected String getDBName() {
		return DB_NAME;
	}
	
	public static synchronized DBKeyValueProviderProxy getProxy(){
		if(mProxy == null){
			mProxy = new DBKeyValueProviderProxy(BaseApp.getInstance(), CONTENT_URI);
		}
		return mProxy;
	}
}
