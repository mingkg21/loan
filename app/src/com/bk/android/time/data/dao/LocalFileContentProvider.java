package com.bk.android.time.data.dao;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.ParcelFileDescriptor;

/** 加载本地文件的provider
 * @author mingkg21
 * @email mingkg21@gmail.com
 * @date 2015年4月25日
 *
 */
public class LocalFileContentProvider extends ContentProvider {
	
    @Override  
    public ParcelFileDescriptor openFile(Uri uri, String mode) throws FileNotFoundException {  
        File file = new File(uri.getPath());  
        ParcelFileDescriptor parcel = ParcelFileDescriptor.open(file, ParcelFileDescriptor.MODE_READ_ONLY);  
        return parcel;  
    }  
      
    @Override  
    public AssetFileDescriptor openAssetFile (Uri uri, String mode) throws FileNotFoundException{  
        String path = uri.getPath();    
        File file = new File(path);  
        if (file.exists()) {  
            return super.openAssetFile(uri, mode);  
        }  
          
        try {
        	if(path.startsWith("/")) {
        		path = path.substring(1);
        	}
			AssetManager am = getContext().getAssets(); 
			return am.openFd(path);
		} catch (IOException e) {
			e.printStackTrace();
		}
        
        return super.openAssetFile(uri, mode);  
    }  
//    
//    private Uri getContentUri(String path) {
//    	StringBuilder sb = new StringBuilder();
//        sb.append("content://");
//        sb.append(getContext().getPackageName());
//        sb.append(path);
//        return Uri.parse(sb.toString());  
//	}
  
    @Override  
    public boolean onCreate() {  
        return true;  
    }  
  
    @Override  
    public int delete(Uri uri, String s, String[] as) {  
        throw new UnsupportedOperationException("Not supported by this LocalFileContentProvider; method: delete " + uri.toString());  
    }  
  
    @Override  
    public String getType(Uri uri) {  
    	return null;
    }  
  
    @Override  
    public Uri insert(Uri uri, ContentValues contentvalues) {  
    	throw new UnsupportedOperationException("Not supported by this LocalFileContentProvider; method: insert " + uri.toString());  
    }  
  
    @Override  
    public Cursor query(Uri uri, String[] as, String s, String[] as1, String s1) {  
    	throw new UnsupportedOperationException("Not supported by this LocalFileContentProvider; method: query " + uri.toString());    
    }  
  
    @Override  
    public int update(Uri uri, ContentValues contentvalues, String s, String[] as) {  
    	throw new UnsupportedOperationException("Not supported by this LocalFileContentProvider; method: update " + uri.toString());  
    }  
}
