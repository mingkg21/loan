package com.bk.android.time.data.net;

import java.io.IOException;
import java.io.InputStream;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.ByteArrayBuffer;

import android.content.Context;
import android.text.TextUtils;

import com.bk.android.app.BaseApp;
import com.bk.android.data.net.AbsConnect;
import com.bk.android.data.net.RequestData;
import com.bk.android.data.net.ResponseData;
import com.bk.android.util.LogUtil;
import com.google.gson.Gson;

public class WXHttpConnect extends AbsConnect {
	
	private static final String TAG = WXHttpConnect.class.getSimpleName();
	private static String URL = "";
	private static final int BUFFER_SIZE = 100 * 1024;

	private static WXHttpConnect mInstance;
	
	public static synchronized WXHttpConnect getInstance(){
		if(mInstance == null){
			mInstance = new WXHttpConnect();
		}
		return mInstance;
	}
	
	private WXHttpConnect(){
	} 
	
	private Context getContext(){
		return BaseApp.getInstance();
	}
	
	public <T> T requestData(String url, Class<T> clazz){
		T dataT = null;
		String json = requestData(url);
		if(TextUtils.isEmpty(json)){
			return null;
		}
		try {
			dataT = new Gson().fromJson(json, clazz);
		} catch (Exception e) {
			LogUtil.e(TAG, e);
		}
		return dataT;
	}
	
	public String requestData(final String url) {
		String reulst = null;
		DefaultHttpClient httpClient = null;
		InputStream is = null;
		LogUtil.i(TAG, "request pre URL=" + url);
		try {
			httpClient = getDefaultHttpClient(getContext());
			HttpResponse httpResponse = null;
			HttpUriRequest httpUriRequest = null;
			httpUriRequest = new HttpGet(url);
			
			LogUtil.i(TAG, "request pre URL: " + httpUriRequest.getURI());
			LogUtil.i(TAG, "request pre request header start");
			Header[] hs = httpUriRequest.getAllHeaders();
			for (Header header : hs) {
				LogUtil.i(TAG,"request pre header="+header.getName() + ":" + header.getValue());
			}
			LogUtil.i(TAG, "request pre request header end");
			
			httpResponse = httpClient.execute(httpUriRequest);
			int httpCode = httpResponse.getStatusLine().getStatusCode();
			LogUtil.i(TAG, "request post http code=" + httpCode);
			if (httpCode != HttpStatus.SC_OK) {
				httpUriRequest.abort();
				return null;
			}
			Header[] headers = httpResponse.getAllHeaders();
			for (Header head : headers) {
				LogUtil.i(TAG,"request post "+head.getName() + ":" + head.getValue());
			}

			HttpEntity entity = httpResponse.getEntity();
			if (entity == null) {
				return null;
			}
			is = entity.getContent();

			LogUtil.i(TAG, "request post in lenght=" + is.available());
			byte[] responseByteArray = new byte[BUFFER_SIZE];
			ByteArrayBuffer bab = new ByteArrayBuffer(BUFFER_SIZE);
			int line = -1;
			while ((line = is.read(responseByteArray)) != -1) {
				bab.append(responseByteArray, 0, line);
				responseByteArray = new byte[BUFFER_SIZE];
			}
			LogUtil.i(TAG, "request post bab length=" + bab.length());
			reulst = new String(bab.toByteArray());
			LogUtil.i(TAG, "reulst" + reulst);
		} catch (Exception e) {
			LogUtil.e(TAG,e);
		} finally {
			try {
				if (is != null) {
					is.close();
				}
			} catch (IOException e) {}
			if (httpClient != null) {
				httpClient.getConnectionManager().shutdown();
				httpClient = null;
			}
		}
		return reulst;
	}

	@Override
	public ResponseData connection(RequestData requestData) {
		return null;
	}
	
}
