package com.bk.android.time.data.net;

import com.bk.android.time.app.App;
import com.bk.android.time.data.UserData;

/**
 * @author mingkg21
 * @email mingkg21@gmail.com
 * @date 2014年9月1日
 *
 */
public class WebConfig {
	private static final String TEST_URL = "http://123.207.227.249:8080/";//测试服务器

	private static final String URL = "http://api.daikuanhua.com/";//正式服务器
//	private static final String URL = "http://192.168.2.190:8080/";//测试服务器
//	private static final String URL = "http://192.168.1.192/";//测试服务器
//	private static final String URL = "http://123.207.227.249:8080/";//测试服务器
	public static final String API_SECRET = "95cecbfa4fb491957186a5a609ec25fa";
	public static final String API_KEY    = "98dba382bc9de1579f2e84d575608eda";

	private static WebConfig instance;
	
	private WebConfig() {
		
	}
	
	public static WebConfig getInstance() {
		if(instance == null) {
			instance = new WebConfig();
		}
		return instance;
	}
	
	public String getHost() {
		if (App.getInstance().isNormalServer()) {
			return URL;
		} else {
			return TEST_URL;
		}
	}
	
	public String getAPIUrl() {
		return getHost() + "apicenter/";
	}
	
	public String getWebUrl() {
		return getHost() + "wap/";
	}

	public String getHomeUrl() {
		return getWebUrl();
	}

	public String getLoanUrl() {
		return getWebUrl() + "loan/home";
	}

	public String getCreditCardUrl() {
		return getWebUrl() + "card/home";
	}

	public String getShareIcondUrl() {
		return "http://resource.daikuanhua.com/media/static/logo/logo.png";
	}

	public String getBrowsingHistoryDeatilUrl(String id) {
		return getWebUrl() + "loan/" + id +"?userToken=" + UserData.getToken();
	}

	public String getStoreUrl(String uid) {
		return "";
	}

	/** FAG
	 * @return
	 */
	public String getFAQUrl() {
		return getWebUrl() + "view/faq";
	}
	

	public String getMyRMBUrl() {
		return "";
	}

	public String getRMBStoreUrl() {
		return "";
	}
	
}
