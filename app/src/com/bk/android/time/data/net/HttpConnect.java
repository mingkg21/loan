package com.bk.android.time.data.net;

import android.content.Context;
import android.os.SystemClock;
import android.text.TextUtils;

import com.bk.android.app.BaseApp;
import com.bk.android.data.net.AbsConnect;
import com.bk.android.data.net.ExtraFileEntity;
import com.bk.android.data.net.RequestData;
import com.bk.android.data.net.ResponseData;
import com.bk.android.os.AsyncTaskManage;
import com.bk.android.os.AsyncTaskManage.IAsyncTask;
import com.bk.android.time.app.App;
import com.bk.android.time.data.NullStringToEmptyAdapterFactory;
import com.bk.android.time.data.Preferences;
import com.bk.android.time.data.UserData;
import com.bk.android.time.entity.BaseEntity;
import com.bk.android.time.util.MD5Util;
import com.bk.android.time.util.MachineInfoUtil;
import com.bk.android.util.LogUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.AbstractHttpEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.ByteArrayBuffer;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

public class HttpConnect extends AbsConnect {
	public static final String X_ENTER_APP = "x-enter-app";

	public static final int URL_TYPE_NORMAL = 0;
	public static final int URL_TYPE_DEBUG = 1;

	private static final String TAG = "HttpConnect";
//	private static String URL = "";
	private static final int BUFFER_SIZE = 8 * 1024;
	
	private static Long sServerTime;
	private static long sClientElapsedRealtime;
	private static HttpConnect sInstance;
	public static boolean sEnterAppIsInit = false;
	private String mToken;

	public static void onEnterApp(){
		sEnterAppIsInit = false;
	}
	
	public static void onExitApp(){
	}

	public static synchronized HttpConnect getInstance(){
		if(sInstance == null){
			sInstance = new HttpConnect();
		}
		return sInstance;
	}
	
	public static int getHttpUrlDefaultType(){
		if(App.getInstance().isDebug()){
			return URL_TYPE_DEBUG;
		}
		return URL_TYPE_NORMAL;
	}
	
	private HttpConnect(){
//		if (App.getInstance().isNormalServer()) {
//			setHttpUrlString(URL_TYPE_NORMAL);
//		} else {
//			setHttpUrlString(URL_TYPE_DEBUG);
//		}
	} 
	
	public void setHttpUrl(int type){
		Preferences.getInstance().setHttpUrlType(type);
//		setHttpUrlString(type);
	}
	
	private String getUrl(){
		return WebConfig.getInstance().getAPIUrl();
	}
	
	private Context getContext(){
		return BaseApp.getInstance();
	}
	
	private synchronized boolean checkServerTime(){
//		if(sServerTime != null){
//			return true;
//		}
//		for (int i = 0; i < 3; i++) {
//			long clientElapsedRealtime = SystemClock.elapsedRealtime();
//			sServerTime = loadServerTime();
//			if(sServerTime != null){
//				sClientElapsedRealtime = SystemClock.elapsedRealtime() + (clientElapsedRealtime - SystemClock.elapsedRealtime()) / 2;
////				LogUtil.e(TAG,"sServerTime=" + FormatUtil.formatSimpleDate("yyyy-MM-dd HH:mm:ss", sServerTime) + " serverDate=" + FormatUtil.formatSimpleDate("yyyy-MM-dd HH:mm:ss", getServerTime()));
//				return true;
//			}
//			try {
//				Thread.sleep(1000);
//			} catch (InterruptedException e) {}
//		}
		return true;
	}
	
	@Override
	public ResponseData connection(RequestData requestData) {
		LogUtil.i(TAG, "request start action="+requestData.getAction());
		ResponseData responseData = null;
		if(checkServerTime()){
			responseData = requestData(getUrl(), requestData);
		}
		LogUtil.i(TAG, "request end action="+requestData.getAction()+" response="+responseData);
		return responseData;
	}
	
	public <T> T connection(RequestData requestData,Class<T> clazz) {
		ResponseData responseData = connection(requestData);
		return parse(responseData, clazz);
	}
	
	public <T> T parse(ResponseData responseData,Class<T> clazz){
		T dataT = null;
		String json = resultContent(responseData);
		if(TextUtils.isEmpty(json)){
			if(responseData != null){
				StringBuffer errStr = new StringBuffer();
				errStr.append("{'code':"+BaseEntity.CODE_FAIL+",'httpcode':"+responseData.resultCode);
				if(responseData.requestHeaders != null){
					errStr.append(",'requestHeaders':[");
					for (int i = 0;i < responseData.requestHeaders.length;i++) {
						Header header = responseData.requestHeaders[i];
						errStr.append("{'"+header.getName() + "':'" + header.getValue()+"'}");
						if(i != responseData.requestHeaders.length - 1){
							errStr.append(",");
						}
					}
					errStr.append("]");
				}
				if(responseData.responseHeaders != null){
					errStr.append(",'responseHeaders':[");
					for (int i = 0;i < responseData.responseHeaders.length;i++) {
						Header header = responseData.responseHeaders[i];
						errStr.append("{'"+header.getName() + "':'" + header.getValue()+"'}");
						if(i != responseData.responseHeaders.length - 1){
							errStr.append(",");
						}
					}
					errStr.append("]");
				}
				errStr.append("}");
				json = errStr.toString();
			}else{
				json = "{'code':"+BaseEntity.CODE_FAIL+"}";
			}
		} else {
			try {
				JSONObject responseObj = new JSONObject(json);
				json = responseObj.getJSONObject(responseData.action).toString();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		try {
			LogUtil.v("HttpConnect", "paser json: " + json);
			Gson gson = new GsonBuilder().registerTypeAdapterFactory(new NullStringToEmptyAdapterFactory()).create();
			dataT = gson.fromJson(json, clazz);
		} catch (Exception e) {
			LogUtil.e(TAG, e);
			try {
				dataT = new Gson().fromJson("{'code':"+BaseEntity.CODE_PARSE_FAIL+",'errmsg':'"+json+"'}", clazz);
			} catch (Exception e1) {
				LogUtil.e(TAG, e);
			}
		}
		return dataT;
	}
	
	public String resultContent(ResponseData responseData) {
		if (responseData == null || responseData.resultContent == null) {
			return null;
		}
		String content = "";
		try {
			content = new String(responseData.resultContent.toByteArray(), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			LogUtil.e(TAG, e);
		}
		LogUtil.v(TAG, "result content="+content);
		return content;
	}
	
	public ResponseData requestData(final String url,final RequestData requestData) {
		long startTime = System.currentTimeMillis();
		long diffTime = startTime;
		int httpCode = 0;
		long reqSize = 0;
		long resSzie = 0;
		Boolean isSuccess = null;
		ResponseData responseData = new ResponseData();
		DefaultHttpClient httpClient = null;
		InputStream is = null;
//		LogUtil.i(TAG, "request pre URL=" + url);
		LogUtil.i(TAG, "request pre action=" + requestData.getAction());
		LogUtil.i(TAG, "request pre sendData=" + requestData.getSendData());
		LogUtil.i(TAG, "request pre sendCompositeData=" + requestData.getSendCompositeData());
		boolean isEnterAppInit = false;
		try {
			httpClient = getDefaultHttpClient(getContext());
			HttpResponse httpResponse = null;
			HttpUriRequest httpUriRequest = null;
			if (requestData.getRequestMethod().equals(RequestData.REQUEST_METHOD_POST)) {
				HttpPost httpPost = postType(requestData,url);
				reqSize = httpPost.getEntity().getContentLength();
				httpUriRequest = httpPost;
			} else {
				httpUriRequest = getType(requestData,url);
			}
			LogUtil.i(TAG, "request pre URL: " + httpUriRequest.getURI());
//			LogUtil.i(TAG, "request pre request header start");
			Header[] hs = httpUriRequest.getAllHeaders();
			for (Header header : hs) {
				if(X_ENTER_APP.equals(header.getName())){
					isEnterAppInit = true;
				}
				LogUtil.i(TAG,"request pre header="+header.getName() + ":" + header.getValue());
			}
//			LogUtil.i(TAG, "request pre request header end");
			final DefaultHttpClient tempHttpClient = httpClient;
			int result = AsyncTaskManage.getInstance().registerHttpTask(new IAsyncTask() {
				@Override
				public void onCancel() {
					LogUtil.i(TAG, "cancel request action="+requestData.getAction());
					tempHttpClient.getConnectionManager().shutdown();
				}
			});
			LogUtil.i(TAG, "request pre request state="+ (result == AsyncTaskManage.RESULT_STOP ? "stop":"staring") +" action="+requestData.getAction());
			if(result != AsyncTaskManage.RESULT_STOP){
				httpResponse = httpClient.execute(httpUriRequest);
				httpCode = httpResponse.getStatusLine().getStatusCode();
				LogUtil.i(TAG, "request post http code=" + httpCode);
				LogUtil.i(TAG, "request post action=" + requestData.getAction());
				responseData.resultCode = httpCode+"";
				responseData.requestHeaders = hs;
				responseData.action = requestData.getAction();
				Header[] headers = httpResponse.getAllHeaders();
				if(headers != null){
					for (Header head : headers) {
						LogUtil.i(TAG,"request post header "+head.getName() + ":" + head.getValue());
					}
				}
				responseData.responseHeaders = headers;
				if (httpCode == HttpStatus.SC_OK) {
					HttpEntity entity = httpResponse.getEntity();
					if (entity != null) {
						is = entity.getContent();
						LogUtil.i(TAG, "request post in lenght=" + is.available());
						byte[] responseByteArray = new byte[BUFFER_SIZE];
						ByteArrayBuffer bab = new ByteArrayBuffer(BUFFER_SIZE);
						int line = -1;
						while ((line = is.read(responseByteArray)) != -1) {
							bab.append(responseByteArray, 0, line);
							responseByteArray = new byte[BUFFER_SIZE];
						}
						resSzie = bab.length();
						LogUtil.i(TAG, "request post bab length=" + bab.length());
						responseData.resultContent = bab;
						isSuccess = true;
					}else{
						isSuccess = false;
					}
				}else{
					isSuccess = false;
				}
			}
		} catch (Exception e) {
			LogUtil.e(TAG,e);
			responseData.resultCode = e.toString();
			isSuccess = false;
		} finally {
			try {
				if (is != null) {
					is.close();
				}
			} catch (IOException e) {}
			if (httpClient != null) {
				httpClient.getConnectionManager().shutdown();
				httpClient = null;
			}
			if(isEnterAppInit && !String.valueOf(HttpStatus.SC_OK).equals(responseData.resultCode)){
				sEnterAppIsInit = false;
			}
		}
		return responseData;
	}
	
	private HttpPost postType(RequestData requestData,String url) throws UnsupportedEncodingException {

		AbstractHttpEntity entity = null;
		if(requestData.getSendCompositeData() == null || requestData.getSendCompositeData().size() == 0){
			entity = new StringEntity("data=" + generateParams(requestData).toString(), "UTF-8");
			entity.setContentType("application/x-www-form-urlencoded");
		}else{
			entity = new ExtraFileEntity(requestData);
		}

		HttpPost httpPost = null;
		StringBuilder urlSb = new StringBuilder();
		urlSb.append(url);
		if (!TextUtils.isEmpty(requestData.getAction())) {
			urlSb.append(requestData.getAction());
		}
		urlSb.append("/");
		urlSb.append(mToken);
		url = urlSb.toString();
		httpPost = new HttpPost(url);

		setCommonHeader(httpPost, requestData);
		
		httpPost.setEntity(entity);
		return httpPost;
	}
	
	private HttpGet getType(RequestData requestData,String url) {
		StringBuilder sb = new StringBuilder();
		sb.append(url);
		if (!TextUtils.isEmpty(requestData.getAction())) {
			sb.append(requestData.getAction());
		}
		if (!TextUtils.isEmpty(requestData.getSendData())) {
			sb.append("?");
			sb.append(requestData.getSendData());
		}
		url = sb.toString();
		HttpGet httpGet = new HttpGet(url);
		setCommonHeader(httpGet, requestData);
		return httpGet;
	}

	private JSONObject generateParams(RequestData requestData) {
		JSONObject res = new JSONObject();
		JSONObject temp = new JSONObject();
		try {
			StringBuilder builder = new StringBuilder();
			TreeMap<String, Object> kvMap = new TreeMap<>();
			kvMap.put("api_key", WebConfig.API_KEY);
			kvMap.putAll(requestData.getSendDataMap());

			Iterator<Map.Entry<String, Object>> iter = kvMap.entrySet().iterator();
			while (iter.hasNext()) {
				Map.Entry<String, Object> entry = iter.next();
				String key = entry.getKey();
				Object value = entry.getValue();
				if (!(value instanceof String) || !TextUtils.isEmpty(value.toString())) {
					builder.append(entry.getKey());
					builder.append("=");
					builder.append(value);
					builder.append("&");
					temp.put(key, value);
				}
			}

			builder.append(requestData.getAction());
			builder.append("&");
			builder.append(WebConfig.API_SECRET);
			mToken = MD5Util.string2MD5(builder.toString());
			temp.put("api_sign", mToken);
			if (UserData.isLogin()) {
				temp.put("user_token", UserData.getToken());
			}
			res.put(requestData.getAction(), temp);

			LogUtil.v(TAG, "request: " + res.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return res;
	}
	
	/**
	 * 添加公共head参数
	 * @param httpUriRequest
	 * @param requestData
	 */
	private static void setCommonHeader(HttpUriRequest httpUriRequest, RequestData requestData){
//		if (UserData.isLogin()) {
//			JSONObject jsonObject = new JSONObject();
//			try {
//				jsonObject.put("authorization_token", UserData.getToken());
//			} catch (JSONException e) {
//				e.printStackTrace();
//			}
//			httpUriRequest.setHeader("X-Client-User", jsonObject.toString());
//		}

		JSONObject jsonObject = MachineInfoUtil.getInstance(App.getInstance()).getClientHeader();
		if (UserData.isLogin()) {
			try {
				jsonObject.put("user_token", UserData.getToken());
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		httpUriRequest.setHeader("X-Client-Event", jsonObject.toString());
	}

	public static Long getServerTime() {
		if(sServerTime == null){
			return sServerTime;
		}
		return sServerTime + (SystemClock.elapsedRealtime() - sClientElapsedRealtime);
	}
	
}
