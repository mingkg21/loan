package com.bk.android.time.data.net;

import android.content.Context;
import android.text.TextUtils;

import com.bk.android.app.BaseApp;
import com.bk.android.data.net.AbsConnect;
import com.bk.android.data.net.ExtraFileEntity;
import com.bk.android.data.net.RequestData;
import com.bk.android.data.net.ResponseData;
import com.bk.android.os.AsyncTaskManage;
import com.bk.android.os.AsyncTaskManage.IAsyncTask;
import com.bk.android.time.app.App;
import com.bk.android.time.entity.BaseEntity;
import com.bk.android.util.LogUtil;
import com.google.gson.Gson;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.AbstractHttpEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.ByteArrayBuffer;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

public class NetEaseConnect extends AbsConnect {

	public static final String X_ENTER_APP = "x-enter-app";

	public static final int URL_TYPE_NORMAL = 0;
	public static final int URL_TYPE_DEBUG = 1;

	private static final String TAG = NetEaseConnect.class.getSimpleName();
//	private static String URL = "";
	private static final int BUFFER_SIZE = 8 * 1024;

	private static NetEaseConnect sInstance;
	public static boolean sEnterAppIsInit = false;

	public static void onEnterApp(){
		sEnterAppIsInit = false;
	}

	public static void onExitApp(){
	}

	public static synchronized NetEaseConnect getInstance(){
		if(sInstance == null){
			sInstance = new NetEaseConnect();
		}
		return sInstance;
	}

	public static int getHttpUrlDefaultType(){
		if(App.getInstance().isDebug()){
			return URL_TYPE_DEBUG;
		}
		return URL_TYPE_NORMAL;
	}

	private NetEaseConnect(){
//		if (App.getInstance().isNormalServer()) {
//			setHttpUrlString(URL_TYPE_NORMAL);
//		} else {
//			setHttpUrlString(URL_TYPE_DEBUG);
//		}
	} 
	
	private String getUrl(){
		return "http://c.m.163.com/nc/subscribe/list/";
	}
	
	private Context getContext(){
		return BaseApp.getInstance();
	}
	
	@Override
	public ResponseData connection(RequestData requestData) {
		LogUtil.i(TAG, "request start action="+requestData.getAction());
		ResponseData responseData = null;
		responseData = requestData(requestData.getBaseUrl(), requestData);
		LogUtil.i(TAG, "request end action="+requestData.getAction()+" response="+responseData);
		return responseData;
	}
	
	public <T> T connection(RequestData requestData, Class<T> clazz) {
		ResponseData responseData = connection(requestData);
		return parse(requestData, responseData, clazz);
	}
	
	public <T> T parse(RequestData requestData, ResponseData responseData, Class<T> clazz){
		T dataT = null;
//		String json = resultContent(responseData);
		String json = null;
		if (TextUtils.isEmpty(requestData.getAction()) && TextUtils.isEmpty(json)) {
			json = "{\"subscribe_info\":{\"template\":\"normal1\",\"hasCover\":false,\"alias\":\"你身边有用的财经新闻。\",\"subnum\":\"37.3万\",\"ename\":\"T1414984365340\",\"tname\":\"央视财经\",\"hasIcon\":true,\"topic_icons\":\"http://dingyue.nosdn.127.net/Qa1ibKazk6huSBP1tsIJgrvNSCXFP6I=ZxH=Lx4BvYFRv1489038605067.png\",\"cid\":\"C1374477759029\",\"topic_background\":\"http://img2.cache.netease.com/m/newsapp/reading/cover1/C1374477759029.jpg\"},\"tab_list\":[{\"postid\":\"CPCSNG5U0519814N\",\"hasCover\":false,\"hasHead\":0,\"replyCount\":0,\"aheadBody\":\"中央电视台大型城市文化旅游品牌竞演节目《魅力中国城》首场竞演阿拉善对战延安于昨晚19：00在CCTV2播出，《魅力中国城》节目由城市主政者及城市形象代言人组成三人战队，由主政者演讲、城市味道、城市名片...\",\"pixel\":\"640*327\",\"hasImg\":0,\"digest\":\"中央电视台大型城市文化旅游品牌竞演节目《魅力中国城》首场竞演阿拉善对战延安于昨晚19：00在CCTV2播出，《魅力中国城》节目由城市主政者及城市形象代言人组成三\",\"hasIcon\":true,\"docid\":\"CPCSNG5U0519814N\",\"title\":\"央视《魅力中国城》首播口碑爆棚！观众表示精彩震撼\",\"order\":2,\"priority\":60,\"boardid\":\"dy_wemedia_bbs\",\"topic_background\":\"http://img2.cache.netease.com/m/newsapp/reading/cover1/C1374477759029.jpg\",\"url_3w\":\"\",\"template\":\"normal1\",\"votecount\":0,\"alias\":\"你身边有用的财经新闻。\",\"cid\":\"C1374477759029\",\"url\":\"\",\"hasAD\":0,\"source\":\"央视财经\",\"ename\":\"T1414984365340\",\"subtitle\":\"\",\"imgsrc\":\"http://dingyue.nosdn.127.net/c8zaSZJRRcGcqUb9aOfqx9VtWNRyNpQhRZLGrWRHKcbw11500093144164.jpg\",\"tname\":\"央视财经\",\"ptime\":\"2017-07-1512:33:10\"},{\"postid\":\"CPB9UDOD0519814N\",\"url_3w\":\"\",\"votecount\":25,\"replyCount\":34,\"aheadBody\":\"（央视财经《经济信息联播》）诱惑巨大的资本市场向来不缺趋之若鹜的上市公司，然而，在资本江湖上急于求成的造假企业也是层出不穷。在证监会近期的专项执法行动中，一家名叫“雅百特”的上市公司造假案件浮出水面，...\",\"pixel\":\"640*372\",\"digest\":\"（央视财经《经济信息联播》）诱惑巨大的资本市场向来不缺趋之若鹜的上市公司，然而，在资本江湖上急于求成的造假企业也是层出不穷。在证监会近期的专项执法行动中，一家名\",\"url\":\"\",\"docid\":\"CPB9UDOD0519814N\",\"title\":\"雅百特业绩造假：虚构跨境大单，10亿元假账出自20平米店铺\",\"source\":\"央视财经\",\"priority\":60,\"imgsrc\":\"http://dingyue.nosdn.127.net/AdH7n9rKR7EnR71IZQjQPeL26iB5rB5OQgtiSLkrWe9A91500039881687.jpg\",\"subtitle\":\"\",\"boardid\":\"dy_wemedia_bbs\",\"ptime\":\"2017-07-1421:45:39\"},{\"postid\":\"CPB9ELQ10519814N\",\"url_3w\":\"\",\"votecount\":0,\"replyCount\":0,\"aheadBody\":\"（央视财经《整点财经》）记者今天从中央国家机关住房资金管理中心了解到，国管住房公积金网上办理业务正式开通。基本信息完整的缴存职工，只需登录中央国家机关住房资金管理中心门户网站，就可以办理住房公积金提取...\",\"pixel\":\"582*323\",\"digest\":\"（央视财经《整点财经》）记者今天从中央国家机关住房资金管理中心了解到，国管住房公积金网上办理业务正式开通。基本信息完整的缴存职工，只需登录中央国家机关住房资金管\",\"url\":\"\",\"docid\":\"CPB9ELQ10519814N\",\"title\":\"国管住房公积金全面推行网上办理\",\"source\":\"央视财经\",\"priority\":60,\"imgsrc\":\"http://dingyue.nosdn.127.net/PZWJk9RgWHfafMQ1m=UiYoe3dIX9UX24ARGL8EL2KOf2K1500039371301.jpg\",\"subtitle\":\"\",\"boardid\":\"dy_wemedia_bbs\",\"ptime\":\"2017-07-1421:37:04\"},{\"postid\":\"CPB65DI50519814N\",\"url_3w\":\"\",\"votecount\":1,\"replyCount\":3,\"aheadBody\":\"（央视财经《交易时间》）国内基因测序的行业龙头华大基因，今天正式登陆深圳证券交易所创业板。那么，目前在中国基因检测的发展又是如何？临床方面的应用情况怎样？随着华大基因的上市，资本又将如何看待这个行业？...\",\"pixel\":\"563*303\",\"digest\":\"（央视财经《交易时间》）国内基因测序的行业龙头华大基因，今天正式登陆深圳证券交易所创业板。那么，目前在中国基因检测的发展又是如何？临床方面的应用情况怎样？随着华\",\"url\":\"\",\"docid\":\"CPB65DI50519814N\",\"title\":\"华大基因今日上市深市上市公司数量破2000家\",\"source\":\"央视财经\",\"priority\":60,\"imgsrc\":\"http://dingyue.nosdn.127.net/kUIxQfcVXAusAqO=Tkzs4he=R7YoZHHgvcS12xvGGUtUV1500035800555.jpg\",\"subtitle\":\"\",\"boardid\":\"dy_wemedia_bbs\",\"ptime\":\"2017-07-1420:39:35\"},{\"postid\":\"CPB2CM460519814N\",\"url_3w\":\"\",\"votecount\":2438,\"replyCount\":2472,\"aheadBody\":\"（央视财经讯）截至2017年4月30日，沪深两市2016年已上市的3,050家公司（其中A股3,032家），除*ST烯碳未按期披露年报外，其余3,049家均按时披露了2016年年度财务报告。为掌握上市...\",\"pixel\":\"640*419\",\"digest\":\"（央视财经讯）截至2017年4月30日，沪深两市2016年已上市的3,050家公司（其中A股3,032家），除*ST烯碳未按期披露年报外，其余3,049家均按时\",\"url\":\"\",\"docid\":\"CPB2CM460519814N\",\"title\":\"证监会：部分上市公司财务及内控信息披露不规范\",\"source\":\"央视财经\",\"priority\":60,\"imgsrc\":\"http://dingyue.nosdn.127.net/EtijQAiM4C5uCaTecHYaiU12cirPYYbrI8bmUI3hnLt6K1500031953114.jpg\",\"subtitle\":\"\",\"boardid\":\"dy_wemedia_bbs\",\"ptime\":\"2017-07-1419:33:38\"},{\"postid\":\"CPB1IQRC0519814N\",\"url_3w\":\"\",\"votecount\":0,\"replyCount\":0,\"aheadBody\":\"（央视财经讯）据中央气象台网站消息，目前，南海中部有热带扰动发展，将于今天下午到晚上加强为热带低压，并逐渐向海南东南部沿海靠近；强度逐渐增强，15日将加强为今年第4号台风(热带风暴级)，并于15日夜间...\",\"pixel\":\"640*519\",\"digest\":\"（央视财经讯）据中央气象台网站消息，目前，南海中部有热带扰动发展，将于今天下午到晚上加强为热带低压，并逐渐向海南东南部沿海靠近；强度逐渐增强，15日将加强为今年\",\"url\":\"\",\"docid\":\"CPB1IQRC0519814N\",\"title\":\"台风来了！今年第4号台风将于明晚至后天凌晨登陆海南！\",\"source\":\"央视财经\",\"priority\":60,\"imgsrc\":\"http://dingyue.nosdn.127.net/h7q6=TSBqDaBAgMxVYzq7DEjyfpU51RPYhV5ezXz41GaK1500030234519.jpg\",\"subtitle\":\"\",\"boardid\":\"dy_wemedia_bbs\",\"ptime\":\"2017-07-1419:19:31\"},{\"postid\":\"CPAPNMNP0519814N\",\"url_3w\":\"\",\"votecount\":0,\"replyCount\":0,\"aheadBody\":\"（央视财经《整点财经》）随着资金问题的逐步发酵，乐视的海外项目也在近日出现危机。据新华社报道，总部位于美国加利福尼亚州的电视机制造商Ｖｉｚｉｏ最近就对乐视系的两家公司提起诉讼，指控乐视违约，要求乐视依...\",\"pixel\":\"713*376\",\"digest\":\"（央视财经《整点财经》）随着资金问题的逐步发酵，乐视的海外项目也在近日出现危机。据新华社报道，总部位于美国加利福尼亚州的电视机制造商Ｖｉｚｉｏ最近就对乐视系的两\",\"url\":\"\",\"docid\":\"CPAPNMNP0519814N\",\"title\":\"乐视在美遭电视机制造商Vizio起诉，要求赔付1亿美元！\",\"source\":\"央视财经\",\"priority\":60,\"imgsrc\":\"http://dingyue.nosdn.127.net/MYH2rDU3uQ3yT5hpax0DY8ugxBGsjWj1INnDJGicbNnQY1500017534512compressflag.png\",\"subtitle\":\"\",\"boardid\":\"dy_wemedia_bbs\",\"ptime\":\"2017-07-1417:02:22\"},{\"postid\":\"SOCS7Q27050835RB\",\"votecount\":0,\"replyCount\":0,\"skipID\":\"VSOCS7Q27\",\"pixel\":\"480*270\",\"videosource\":\"新媒体\",\"digest\":\"\",\"skipType\":\"video\",\"docid\":\"9IG74V5H00963VRO_CPALTCEP0519814NupdateDoc\",\"title\":\"身材小智慧大！分拣机器人每小时完成12000单\",\"TAGS\":\"视频\",\"source\":\"央视财经\",\"videoTopic\":{\"alias\":\"你身边有用的财经新闻。\",\"tname\":\"央视财经\",\"ename\":\"T1414984365340\",\"tid\":\"T1414984365340\",\"topic_icons\":\"http://dingyue.nosdn.127.net/Qa1ibKazk6huSBP1tsIJgrvNSCXFP6I=ZxH=Lx4BvYFRv1489038605067.png\"},\"videoID\":\"VSOCS7Q27\",\"priority\":60,\"lmodify\":\"2017-07-1415:55:33\",\"imgsrc\":\"http://vimg3.ws.126.net/image/snapshot/2017/7/2/8/VSOCS7Q28.jpg\",\"length\":123,\"boardid\":\"video_bbs\",\"ptime\":\"2017-07-1415:55:33\"},{\"postid\":\"CPAJQIT60519814N\",\"url_3w\":\"\",\"votecount\":0,\"replyCount\":0,\"aheadBody\":\"（央视财经《老爸全知道》）孩子的小脑袋里到底有多少奇怪的问题？把爸爸问到抓狂不已？把妈妈问到难以招架？我的天哪！谁能回答所有的问题&hellip;&hellip;如果你还没体验过孩子们的提问“轰炸”，...\",\"pixel\":\"640*629\",\"digest\":\"（央视财经《老爸全知道》）孩子的小脑袋里到底有多少奇怪的问题？把爸爸问到抓狂不已？把妈妈问到难以招架？我的天哪！谁能回答所有的问题&hellip;&hellip\",\"url\":\"\",\"docid\":\"CPAJQIT60519814N\",\"title\":\"暑假到，《老爸全知道》陪你快乐学知识、开心度假期！\",\"source\":\"央视财经\",\"priority\":60,\"imgsrc\":\"http://dingyue.nosdn.127.net/h0DGtWByTFH7WrSdoWVFtok57z6bt3Cs8763lAoUevH5P1500016711364.jpg\",\"subtitle\":\"\",\"boardid\":\"dy_wemedia_bbs\",\"ptime\":\"2017-07-1415:19:05\"},{\"postid\":\"CPAJCL4B0519814N\",\"url_3w\":\"\",\"votecount\":207,\"replyCount\":219,\"aheadBody\":\"（央视财经《交易时间》）7月13日，中国人民银行首次发布《消费者金融素养调查分析报告》。这份报告，给全国消费者的金融素养指数，打出了63.71的平均分，属于中等偏上水平。排在前五位的金融知识，分别是股...\",\"pixel\":\"723*381\",\"digest\":\"（央视财经《交易时间》）7月13日，中国人民银行首次发布《消费者金融素养调查分析报告》。这份报告，给全国消费者的金融素养指数，打出了63.71的平均分，属于中等\",\"url\":\"\",\"docid\":\"CPAJCL4B0519814N\",\"title\":\"央行：全国消费者金融素养指数中等偏上\",\"source\":\"央视财经\",\"priority\":60,\"imgsrc\":\"http://dingyue.nosdn.127.net/Vfb1rQbeeJiBEIl0QCWQ1cU0YNT6Te4Bd20OEks=OoUen1500016092280compressflag.png\",\"subtitle\":\"\",\"boardid\":\"dy_wemedia_bbs\",\"ptime\":\"2017-07-1415:11:29\"},{\"postid\":\"CPA9FADN0519814N\",\"url_3w\":\"\",\"votecount\":44,\"replyCount\":52,\"aheadBody\":\"（央视财经《第一时间》）自共享单车大量出现后，深圳因为多个道路没有设置专用自行车道，导致涉非机动车交通违法行为增长不少。据深圳交警公布的数据显示，7月1日至7月9日期间，共查处涉非机动车交通违法136...\",\"pixel\":\"659*376\",\"digest\":\"（央视财经《第一时间》）自共享单车大量出现后，深圳因为多个道路没有设置专用自行车道，导致涉非机动车交通违法行为增长不少。据深圳交警公布的数据显示，7月1日至7月\",\"url\":\"\",\"docid\":\"CPA9FADN0519814N\",\"title\":\"深圳开出首批共享单车“禁骑令”！13615人违法被罚\",\"source\":\"央视财经\",\"priority\":60,\"imgsrc\":\"http://dingyue.nosdn.127.net/El5iHiBAhxiSrzjZzfTL=M22FBw=4FOx6rarYBg20jgLp1500005671081compressflag.png\",\"subtitle\":\"\",\"boardid\":\"dy_wemedia_bbs\",\"ptime\":\"2017-07-1412:18:11\"},{\"postid\":\"CPA95URP0519814N\",\"url_3w\":\"\",\"votecount\":0,\"replyCount\":0,\"aheadBody\":\"（央视财经《秘密大改造》）坐标：南京，榜样人物：王碟，改造面积：60平方米，施工周期：14天，设计师：何潇宁，设计团队：廖启雄，本周我们将迎来节目的第十二集，也是《秘密大改造》的第一季收官之作，榜样人...\",\"pixel\":\"554*312\",\"digest\":\"（央视财经《秘密大改造》）坐标：南京，榜样人物：王碟，改造面积：60平方米，施工周期：14天，设计师：何潇宁，设计团队：廖启雄，本周我们将迎来节目的第十二集，也\",\"url\":\"\",\"docid\":\"CPA95URP0519814N\",\"title\":\"《秘密大改造》收官：设计师颠覆房屋结构60平米打造四室\",\"source\":\"央视财经\",\"priority\":60,\"imgsrc\":\"http://dingyue.nosdn.127.net/kYX7V63c61iAIHAuT5N=V9b2pKBmxc1RjJLtEx19n=SzW1500005548847.jpg\",\"subtitle\":\"\",\"boardid\":\"dy_wemedia_bbs\",\"ptime\":\"2017-07-1412:13:03\"},{\"postid\":\"CPA8LJMI0519814N\",\"url_3w\":\"\",\"votecount\":0,\"replyCount\":0,\"aheadBody\":\"（央视财经《第一时间》）近日，国务院食品安全办、公安部等9部门联合下发《食品、保健食品欺诈和虚假宣传整治方案》。《方案》要求，经营食品、保健食品应当依法取得食品经营许可，没有实体店经营资格的企业，不得...\",\"pixel\":\"652*367\",\"digest\":\"（央视财经《第一时间》）近日，国务院食品安全办、公安部等9部门联合下发《食品、保健食品欺诈和虚假宣传整治方案》。《方案》要求，经营食品、保健食品应当依法取得食品\",\"url\":\"\",\"docid\":\"CPA8LJMI0519814N\",\"title\":\"9部门联合整治食品保健品欺诈：广告代言人也要担责\",\"source\":\"央视财经\",\"priority\":60,\"imgsrc\":\"http://dingyue.nosdn.127.net/NY8DDwbzIzjdBLuN1ht5BrTHVtEn9qD3JqkfkUzku6iHl1500005023619compressflag.png\",\"subtitle\":\"\",\"boardid\":\"dy_wemedia_bbs\",\"ptime\":\"2017-07-1412:04:08\"},{\"postid\":\"CPA6ED890519814N\",\"url_3w\":\"\",\"votecount\":26,\"replyCount\":38,\"aheadBody\":\"承载了观众近五个月的翘首期盼，三十二座魅力城市终于要揭开它们神秘的面纱。7月14日19:00，大型城市文化旅游品牌竞演节目《魅力中国城》将在中央电视台财经频道播出，正式与观众朋友们见面！明星、素人竞技...\",\"pixel\":\"480*2040\",\"digest\":\"承载了观众近五个月的翘首期盼，三十二座魅力城市终于要揭开它们神秘的面纱。7月14日19:00，大型城市文化旅游品牌竞演节目《魅力中国城》将在中央电视台财经频道播\",\"url\":\"\",\"docid\":\"CPA6ED890519814N\",\"title\":\"央视大片《魅力中国城》今日首播！看城市PK大战！\",\"source\":\"央视财经\",\"priority\":60,\"imgsrc\":\"http://dingyue.nosdn.127.net/=4BiA4HMIVv0RgT6m9njT2b9h7mdN7RWosUKKlrjXyzIY1500001912176compressflag.jpg\",\"subtitle\":\"\",\"boardid\":\"dy_wemedia_bbs\",\"ptime\":\"2017-07-1411:37:47\"},{\"postid\":\"CP8OO20M0519814N\",\"url_3w\":\"\",\"votecount\":0,\"replyCount\":0,\"aheadBody\":\"（央视财经讯）进入2017年，融创中国收购动作频频，先是花150亿元驰援乐视，然后斥资631.7亿元拿下万达13个文旅项目91%的股权和76个酒店项目，然而就在这起收购宣布后不久，国际三大评级机构中的...\",\"pixel\":\"479*257\",\"digest\":\"（央视财经讯）进入2017年，融创中国收购动作频频，先是花150亿元驰援乐视，然后斥资631.7亿元拿下万达13个文旅项目91%的股权和76个酒店项目，然而就在\",\"url\":\"\",\"docid\":\"CP8OO20M0519814N\",\"title\":\"150亿驰援乐视、631亿豪买万达...今年最火的公司被降级了\",\"source\":\"央视财经\",\"priority\":60,\"imgsrc\":\"http://dingyue.nosdn.127.net/lka2oeZ1WJ0hRzr=iL25rLzeNE=cMysRsop17bYRibW1t1499954414167.jpg\",\"subtitle\":\"\",\"boardid\":\"dy_wemedia_bbs\",\"ptime\":\"2017-07-1322:06:36\"},{\"postid\":\"CP8OBEED0519814N\",\"url_3w\":\"\",\"votecount\":125,\"replyCount\":135,\"aheadBody\":\"（央视财经《经济信息联播》）昨天，一段顺丰无人机在水上起飞的视频引发大家关注。顺丰控股证实，该公司测试的水陆两用无人机，已完成陆上、水上的载重科研试飞，这种无人机业务载荷量250公斤。此外，顺丰还在研...\",\"pixel\":\"726*306\",\"digest\":\"（央视财经《经济信息联播》）昨天，一段顺丰无人机在水上起飞的视频引发大家关注。顺丰控股证实，该公司测试的水陆两用无人机，已完成陆上、水上的载重科研试飞，这种无人\",\"url\":\"\",\"docid\":\"CP8OBEED0519814N\",\"title\":\"顺丰控股证实：水陆两栖无人机完成水陆试飞\",\"source\":\"央视财经\",\"priority\":60,\"imgsrc\":\"http://dingyue.nosdn.127.net/pfNH=ZHUcPePFosWlnOfeEWesOEwlIKGCcrSZ6NraPXKF1499954364834compressflag.png\",\"subtitle\":\"\",\"boardid\":\"dy_wemedia_bbs\",\"ptime\":\"2017-07-1321:59:43\"},{\"postid\":\"CP8NP3A30519814N\",\"url_3w\":\"\",\"votecount\":0,\"replyCount\":0,\"aheadBody\":\"（央视财经《央视财经评论》）在我国，农村地区居民们用来自建房屋的土地称之为宅基地。长期以来，对于宅基地我国实行的是“一户一宅”政策。但是在现实生活中，有许多农户出于自己的诉求或家庭的变故，使“一宅”变...\",\"pixel\":\"737*421\",\"digest\":\"（央视财经《央视财经评论》）在我国，农村地区居民们用来自建房屋的土地称之为宅基地。长期以来，对于宅基地我国实行的是“一户一宅”政策。但是在现实生活中，有许多农户\",\"url\":\"\",\"docid\":\"CP8NP3A30519814N\",\"title\":\"农村宅基地改革怎么改？应让宅基地变成资产“活”起来\",\"source\":\"央视财经\",\"priority\":60,\"imgsrc\":\"http://dingyue.nosdn.127.net/ttfqd3s9v0JzTHmw9e0QnOGK=JZqx9PFJ2GT9b99QNCll1499953749510compressflag.png\",\"subtitle\":\"\",\"boardid\":\"dy_wemedia_bbs\",\"ptime\":\"2017-07-1321:49:43\"},{\"postid\":\"CP8NF0L50519814N\",\"url_3w\":\"\",\"votecount\":0,\"replyCount\":0,\"aheadBody\":\"（央视财经《央视财经评论》）截至2016年底中国高铁的运营里程已经达到2.2万公里，堪称世界一流，但是高铁的餐饮服务水平一直难以满足旅客的需求，不少旅客对高铁盒饭的价格、质量等问题颇为不满。中国青年报...\",\"pixel\":\"753*404\",\"digest\":\"（央视财经《央视财经评论》）截至2016年底中国高铁的运营里程已经达到2.2万公里，堪称世界一流，但是高铁的餐饮服务水平一直难以满足旅客的需求，不少旅客对高铁盒\",\"url\":\"\",\"docid\":\"CP8NF0L50519814N\",\"title\":\"动车上可以点“外卖”啦！铁总如此贴心为了啥？\",\"source\":\"央视财经\",\"priority\":60,\"imgsrc\":\"http://dingyue.nosdn.127.net/zIZnVkemCRLPUR0Y1z30moYyNK8NLoHsxcVCrqeSmRacv1499953419720compressflag.png\",\"subtitle\":\"\",\"boardid\":\"dy_wemedia_bbs\",\"ptime\":\"2017-07-1321:44:12\"},{\"postid\":\"CP7L748T0519814N\",\"url_3w\":\"\",\"votecount\":8,\"replyCount\":12,\"aheadBody\":\"（央视财经《整点财经》）昨天，中国人民银行通报了今年上半年我国金融统计数据基本情况，其中社会融资规模保持合理增长，广义货币M2增速连续两个月处于10%以下。初步统计，2017年上半年社会融资规模增量累...\",\"pixel\":\"728*381\",\"digest\":\"（央视财经《整点财经》）昨天，中国人民银行通报了今年上半年我国金融统计数据基本情况，其中社会融资规模保持合理增长，广义货币M2增速连续两个月处于10%以下。初步\",\"url\":\"\",\"docid\":\"CP7L748T0519814N\",\"title\":\"中国6月M2增速再创历史新低新增贷款持续走高\",\"source\":\"央视财经\",\"priority\":60,\"imgsrc\":\"http://dingyue.nosdn.127.net/SHj6I8716V=IxxmlJC7QAP3no8w6ZpRNJzqmrF5=FOrsD1499917055658compressflag.png\",\"subtitle\":\"\",\"boardid\":\"dy_wemedia_bbs\",\"ptime\":\"2017-07-1321:30:42\"},{\"postid\":\"CP85V52H0519814N\",\"url_3w\":\"\",\"votecount\":0,\"replyCount\":0,\"aheadBody\":\"（央视财经《环球财经连线》）美国《纽约时报》9号爆料称，美国总统特朗普的长子“小特朗普”承认在去年美国总统大选期间，“跟一名与俄政府有联系的俄罗斯律师会面”，而他这么做的目的是为了获取希拉里的“黑料”...\",\"pixel\":\"754*371\",\"digest\":\"（央视财经《环球财经连线》）美国《纽约时报》9号爆料称，美国总统特朗普的长子“小特朗普”承认在去年美国总统大选期间，“跟一名与俄政府有联系的俄罗斯律师会面”，而\",\"url\":\"\",\"docid\":\"CP85V52H0519814N\",\"title\":\"“通俄门”事件持续发酵特朗普发文声援儿子\",\"source\":\"央视财经\",\"priority\":60,\"imgsrc\":\"http://dingyue.nosdn.127.net/YrD7lAyZ8n=PO2zod9lhSmBbSIu9bJJXsElfd41FxtXqB1499933884845compressflag.png\",\"subtitle\":\"\",\"boardid\":\"dy_wemedia_bbs\",\"ptime\":\"2017-07-1321:30:32\"}]}";
		}
		if(TextUtils.isEmpty(json)){
			if(responseData != null){
				StringBuffer errStr = new StringBuffer();
				errStr.append("{\"code\":"+ BaseEntity.CODE_FAIL+",\"httpcode\":"+responseData.resultCode);
				if(responseData.requestHeaders != null){
					errStr.append(",\"requestHeaders\":[");
					for (int i = 0;i < responseData.requestHeaders.length;i++) {
						Header header = responseData.requestHeaders[i];
						errStr.append("{\""+header.getName() + "\":\"" + header.getValue()+"\"}");
						if(i != responseData.requestHeaders.length - 1){
							errStr.append(",");
						}
					}
					errStr.append("]");
				}
				if(responseData.responseHeaders != null){
					errStr.append(",\"responseHeaders\":[");
					for (int i = 0;i < responseData.responseHeaders.length;i++) {
						Header header = responseData.responseHeaders[i];
						errStr.append("{\""+header.getName() + "\":\"" + header.getValue()+"\"}");
						if(i != responseData.responseHeaders.length - 1){
							errStr.append(",");
						}
					}
					errStr.append("]");
				}
				errStr.append("}");
				json = errStr.toString();
			}else{
				json = "{\"code\":"+ BaseEntity.CODE_FAIL+"}";
			}
		} else {//格式转换
			try {
				JSONObject jsonObject = new JSONObject(json);
				StringBuilder sb = new StringBuilder();
				sb.append("{\"code\":");
				sb.append("\"0000\"");
				sb.append(", \"isSuccess\": true");
				sb.append(", \"totalPages\": 1");
				sb.append(", \"curPage\": 1");
				sb.append(", \"results\": ");
				if (TextUtils.isEmpty(requestData.getAction())) {
					sb.append(jsonObject.optString("tab_list"));
				} else {
					sb.append(jsonObject.optString(requestData.getAction()));
				}
				sb.append("}");
				json = sb.toString();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		try {
			System.out.println("json: " + json);
			dataT = new Gson().fromJson(json, clazz);
			System.out.println("Gson");
		} catch (Exception e) {
			LogUtil.e(TAG, e);
			try {
				dataT = new Gson().fromJson("{\"code\":"+ BaseEntity.CODE_PARSE_FAIL+",\"errmsg\":\""+json+"\"}", clazz);
			} catch (Exception e1) {
				LogUtil.e(TAG, e);
			}
		}
		return dataT;
	}
	
	public String resultContent(ResponseData responseData) {
		if (responseData == null || responseData.resultContent == null) {
			return null;
		}
		String content = "";
		try {
			content = new String(responseData.resultContent.toByteArray(), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			LogUtil.e(TAG, e);
		}
		LogUtil.v(TAG, "result content="+content);
		return content;
	}
	
	public ResponseData requestData(final String url, final RequestData requestData) {
		long startTime = System.currentTimeMillis();
		long diffTime = startTime;
		int httpCode = 0;
		long reqSize = 0;
		long resSzie = 0;
		Boolean isSuccess = null;
		ResponseData responseData = new ResponseData();
		DefaultHttpClient httpClient = null;
		InputStream is = null;
		LogUtil.i(TAG, "request pre URL=" + url);
		LogUtil.i(TAG, "request pre action=" + requestData.getAction());
		LogUtil.i(TAG, "request pre sendData=" + requestData.getSendData());
		LogUtil.i(TAG, "request pre sendCompositeData=" + requestData.getSendCompositeData());
		boolean isEnterAppInit = false;
		try {
			httpClient = getDefaultHttpClient(getContext());
			HttpResponse httpResponse = null;
			HttpUriRequest httpUriRequest = null;
			if (requestData.getRequestMethod().equals(RequestData.REQUEST_METHOD_POST)) {
				HttpPost httpPost = postType(requestData,url);
				reqSize = httpPost.getEntity().getContentLength();
				httpUriRequest = httpPost;
			} else {
				httpUriRequest = getType(requestData,url);
			}
			LogUtil.i(TAG, "request pre URL: " + httpUriRequest.getURI());
			LogUtil.i(TAG, "request pre request header start");
			Header[] hs = httpUriRequest.getAllHeaders();
			for (Header header : hs) {
				if(X_ENTER_APP.equals(header.getName())){
					isEnterAppInit = true;
				}
				LogUtil.i(TAG,"request pre header="+header.getName() + ":" + header.getValue());
			}
			LogUtil.i(TAG, "request pre request header end");
			final DefaultHttpClient tempHttpClient = httpClient;
			int result = AsyncTaskManage.getInstance().registerHttpTask(new IAsyncTask() {
				@Override
				public void onCancel() {
					LogUtil.i(TAG, "cancel request action="+requestData.getAction());
					tempHttpClient.getConnectionManager().shutdown();
				}
			});
			LogUtil.i(TAG, "request pre request state="+ (result == AsyncTaskManage.RESULT_STOP ? "stop":"staring") +" action="+requestData.getAction());
			if(result != AsyncTaskManage.RESULT_STOP){
				httpResponse = httpClient.execute(httpUriRequest);
				httpCode = httpResponse.getStatusLine().getStatusCode();
				LogUtil.i(TAG, "request post http code=" + httpCode);
				LogUtil.i(TAG, "request post action=" + requestData.getAction());
				responseData.resultCode = httpCode+"";
				responseData.requestHeaders = hs;
				Header[] headers = httpResponse.getAllHeaders();
				if(headers != null){
					for (Header head : headers) {
						LogUtil.i(TAG,"request post header "+head.getName() + ":" + head.getValue());
					}
				}
				responseData.responseHeaders = headers;
				if (httpCode == HttpStatus.SC_OK) {
					HttpEntity entity = httpResponse.getEntity();
					if (entity != null) {
						is = entity.getContent();
						LogUtil.i(TAG, "request post in lenght=" + is.available());
						byte[] responseByteArray = new byte[BUFFER_SIZE];
						ByteArrayBuffer bab = new ByteArrayBuffer(BUFFER_SIZE);
						int line = -1;
						while ((line = is.read(responseByteArray)) != -1) {
							bab.append(responseByteArray, 0, line);
							responseByteArray = new byte[BUFFER_SIZE];
						}
						resSzie = bab.length();
						LogUtil.i(TAG, "request post bab length=" + bab.length());
						responseData.resultContent = bab;
						isSuccess = true;
					}else{
						isSuccess = false;
					}
				}else{
					isSuccess = false;
				}
			}
		} catch (Exception e) {
			LogUtil.e(TAG,e);
			responseData.resultCode = e.toString();
			isSuccess = false;
		} finally {
			try {
				if (is != null) {
					is.close();
				}
			} catch (IOException e) {}
			if (httpClient != null) {
				httpClient.getConnectionManager().shutdown();
				httpClient = null;
			}
			if(isEnterAppInit && !String.valueOf(HttpStatus.SC_OK).equals(responseData.resultCode)){
				sEnterAppIsInit = false;
			}
		}
		return responseData;
	}
	
	private HttpPost postType(RequestData requestData, String url) throws UnsupportedEncodingException {
		HttpPost httpPost = null;
		StringBuilder urlSb = new StringBuilder();
		urlSb.append(url);
		if (!TextUtils.isEmpty(requestData.getAction())) {
			urlSb.append(requestData.getAction());
		}
		url = urlSb.toString();
		httpPost = new HttpPost(url);

		AbstractHttpEntity entity = null;
		if(requestData.getSendCompositeData() == null || requestData.getSendCompositeData().size() == 0){
			entity = new StringEntity(requestData.getSendData(), "UTF-8");
			entity.setContentType("application/x-www-form-urlencoded");  
		}else{
			entity = new ExtraFileEntity(requestData);
		}
		httpPost.setEntity(entity);
		return httpPost;
	}
	
	private HttpGet getType(RequestData requestData, String url) {
		StringBuilder sb = new StringBuilder();
		sb.append(url);
//		if (!TextUtils.isEmpty(requestData.getAction())) {
//			sb.append(requestData.getAction());
//		}
		url = sb.toString();
		HttpGet httpGet = new HttpGet(url);
		return httpGet;
	}
	
}
