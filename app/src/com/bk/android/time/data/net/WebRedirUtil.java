package com.bk.android.time.data.net;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;

import com.bk.android.time.ui.activiy.ActivityChannels;
import com.bk.android.util.URLUtil;

import java.net.URLDecoder;
import java.util.HashMap;

public class WebRedirUtil {

	public static boolean dispatchUrl(String url, Context context) {
		if (TextUtils.isEmpty(url)) {
			return false;
		}
		if (url.indexOf("http://") == -1) {
			url = "http://" + url;
		}
		Intent intent = new Intent();
		intent.setAction(Intent.ACTION_VIEW);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		Uri content_url = Uri.parse(url);
		intent.setData(content_url);
		try{
			context.startActivity(intent);
		}catch (Exception e) {}
		return true;
	}

	public static boolean redirUrl(String url,Context context){
		if(url.indexOf("banketime=out") > -1){
		    try{
		    	Intent intent = new Intent();        
			    intent.setAction(Intent.ACTION_VIEW);
			    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			    Uri content_url = Uri.parse(url);   
			    intent.setData(content_url);  
			    context.startActivity(intent);
				return true;
		    }catch (Exception e) {}
		}
		if(dispatchBabyAction(url,context)) {
			return true;
		}
		if(dispatchTaobaoAction(url,context)) {
			return true;
		}
		return false;
	}
	
	private static boolean dispatchBabyAction(String url,Context context) {
		if (!URLUtil.getProtocol(url).equals("baby")) {
			return false;
		}
		String action = URLUtil.getHost(url);
		HashMap<String, String> requestValue = URLUtil.getURLRequest(url);
		if(!TextUtils.isEmpty(action)) {
			if(action.equals("edit_info")) {//编辑个人中心
				ActivityChannels.openPersonInfoEditActivity(context);
			} else if(action.equals("download_app")) {//下载应用
				if(requestValue != null) {
					String urlStr = requestValue.get("url");
					if(!TextUtils.isEmpty(urlStr)) {
						ActivityChannels.openCommonWebActivity(context, URLDecoder.decode(urlStr) + "1");
					}
				}
			}
		}
		return true;
	}
	
	private static boolean dispatchTaobaoAction(String url,Context context) {
		int index = url.indexOf("&istaobao=1");
		if(index > -1) {
			url = url.substring(0, index);
			Intent intent = new Intent();        
		    intent.setAction(Intent.ACTION_VIEW);
		    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		    Uri content_url = Uri.parse(url);   
		    intent.setData(content_url);
		    try{
		    	context.startActivity(intent);
		    }catch (Exception e) {}
			return true;
		}
		return false;
	}
}
