package com.bk.android.time.data;

import android.text.TextUtils;

import com.bk.android.dao.DBKeyValueProviderProxy;
import com.bk.android.time.app.App;
import com.bk.android.time.data.dao.DBPreferencesProvider;
import com.bk.android.time.entity.UserInfo;

/**
 * 用户数据
 * @author linyiwei
 */
public class UserData {

	public static final String DESIRE_USER_ID = "000000";
	private static final String PREFERENCE_TYPE_USER_DATA = "PREFERENCE_TYPE_USER_DATA";

	private static final String KEY_USER_ID = "KEY_USER_ID";
	private static final String KEY_TOKEN = "KEY_TOKEN";

	private static final String KEY_USER_ICON = "KEY_USER_ICON";
	private static final String KEY_NAME = "KEY_NAME";
	private static final String KEY_PHONE = "KEY_PHONE";
	
	private static final String KEY_UID = "KEY_UID";
	private static final String KEY_ROLE = "KEY_ROLE";
	private static final String KEY_LEVEL = "KEY_LEVEL";
	private static final String KEY_EXPERIENCE = "KEY_EXPERIENCE";
	private static final String KEY_POINT = "KEY_POINT";
	private static final String KEY_SIGN_FLAG = "KEY_SIGN_FLAG";
	private static final String KEY_DEFAULT_ICON_FLAG = "KEY_DEFAULT_ICON_FLAG";

	//个人信息
	private static final String KEY_LOCATION_CITY = "KEY_LOCATION_CITY";
	private static final String KEY_ID_CARD = "KEY_ID_CARD";
	private static final String KEY_MARRY_FLAG = "KEY_MARRY_FLAG";
	private static final String KEY_EDUCATION_LEVEL = "KEY_EDUCATION_LEVEL";
	//身份信息
	private static final String KEY_PROFESSION = "KEY_PROFESSION";
	private static final String KEY_INCOME_TYPE = "KEY_INCOME_TYPE";
	private static final String KEY_INCOME_VALUE = "KEY_INCOME_VALUE";
	private static final String KEY_COMPANY_TYPE = "KEY_COMPANY_TYPE";
	private static final String KEY_COMPANY_WORK_TIME = "KEY_COMPANY_WORK_TIME";
	private static final String KEY_MORE_SIX_FUND_FLAG = "KEY_MORE_SIX_FUND_FLAG";
	private static final String KEY_MORE_SIX_SOCIAL_FLAG = "KEY_MORE_SIX_SOCIAL_FLAG";
	//房产信息
	private static final String KEY_HAS_CREDIT_FLAG = "KEY_HAS_CREDIT_FLAG";
	private static final String KEY_CONTAIN_HOUSE_TYPE = "KEY_CONTAIN_HOUSE_TYPE";
	private static final String KEY_CONTAIN_CAR_TYPE = "KEY_CONTAIN_CAR_TYPE";
	private static final String KEY_SUCCESS_LOAN_FLAG = "KEY_SUCCESS_LOAN_FLAG";
	private static final String KEY_CREDIT_STANDING = "KEY_CREDIT_STANDING";

	private static final String KEY_SHOW_APP_LIST_FLAG = "KEY_SHOW_APP_LIST_FLAG";

	private static final String KEY_SHOW_IMAGE_TIP = "KEY_SHOW_IMAGE_TIP";

	private static Object sUserIdLock = new Object();

	private static String sUserId;
	private static String sToken;

	public static UserInfo mUserInfo;

	private static DBKeyValueProviderProxy getProxy(){
		return DBPreferencesProvider.getProxy();
	}
	
	public static String getUserId(){
		synchronized (sUserIdLock) {
			if(sUserId == null){
				sUserId = getProxy().getString(KEY_USER_ID, PREFERENCE_TYPE_USER_DATA,DESIRE_USER_ID);
			}
			if(TextUtils.isEmpty(sUserId)){
				sUserId = DESIRE_USER_ID;
			}
			return sUserId;
//			return "30876";
		}
	}
	
	public static void setUserId(String value){
		synchronized (sUserIdLock) {
			sUserId = value;
		}
		getProxy().putString(KEY_USER_ID, value, PREFERENCE_TYPE_USER_DATA);
	}

	public static void logout() {
		setUserId("");
		setToken("");
	}

	public static String getToken(){
		synchronized (sUserIdLock) {
			if(sToken == null){
				sToken = getProxy().getString(KEY_TOKEN, PREFERENCE_TYPE_USER_DATA,DESIRE_USER_ID);
			}
			return sToken;
		}
//		return "e0d05b3c7bc84d138d54c1e07edc3ac1";
	}

	public static void setToken(String value){
		synchronized (sUserIdLock) {
			sToken = value;
		}
		getProxy().putString(KEY_TOKEN, value, PREFERENCE_TYPE_USER_DATA);
	}
	
	public static void setUserIcon(String icon){
		getProxy().putString(KEY_USER_ICON, icon, PREFERENCE_TYPE_USER_DATA);
	}
	
	public static String getUserIcon(){
		return getProxy().getString(KEY_USER_ICON,PREFERENCE_TYPE_USER_DATA, "");
	}
	
	public static String getName(){
		return getProxy().getString(KEY_NAME,PREFERENCE_TYPE_USER_DATA, "");
	}
	
	public static void setName(String value){
		getProxy().putString(KEY_NAME, value, PREFERENCE_TYPE_USER_DATA);
	}
	
	public static void clear(){
		getProxy().clear(PREFERENCE_TYPE_USER_DATA);
	}
	
	public static boolean isLogin(){
		return !getUserId().equals(DESIRE_USER_ID);
	}
	
	private static boolean hasData(String key){
		return getProxy().hasData(key,PREFERENCE_TYPE_USER_DATA);
	}
	
	public static String getPhone() {
		return getProxy().getString(KEY_PHONE,PREFERENCE_TYPE_USER_DATA, "");
	}

	public static void setPhone(String string) {
		getProxy().putString(KEY_PHONE, string, PREFERENCE_TYPE_USER_DATA);
	}
	
	public static String getUUid(){
		return getProxy().getString(KEY_UID,PREFERENCE_TYPE_USER_DATA, "");
	}
	
	public static void setUUid(String value){
		getProxy().putString(KEY_UID, value, PREFERENCE_TYPE_USER_DATA);
	}
	
	public static int getRole(){
		return getProxy().getInt(KEY_ROLE,PREFERENCE_TYPE_USER_DATA, 0);
	}
	
	public static void setRole(int value){
		getProxy().putInt(KEY_ROLE, value, PREFERENCE_TYPE_USER_DATA);
	}
	
	public static int getLevel(){
		return getProxy().getInt(KEY_LEVEL,PREFERENCE_TYPE_USER_DATA, 0);
	}
	
	public static void setLevel(int value){
		getProxy().putInt(KEY_LEVEL, value, PREFERENCE_TYPE_USER_DATA);
	}

	public static int getExperience(){
		return getProxy().getInt(KEY_EXPERIENCE,PREFERENCE_TYPE_USER_DATA, 0);
	}

	public static void setExperience(int value){
		getProxy().putInt(KEY_EXPERIENCE, value, PREFERENCE_TYPE_USER_DATA);
	}

	public static int getPoint(){
		return getProxy().getInt(KEY_POINT,PREFERENCE_TYPE_USER_DATA, 0);
	}

	public static void setPoint(int value){
		getProxy().putInt(KEY_POINT, value, PREFERENCE_TYPE_USER_DATA);
	}

	public static boolean isSignIn() {
		return getSignFlag() == 1;
	}

	public static int getSignFlag(){
		return getProxy().getInt(KEY_SIGN_FLAG,PREFERENCE_TYPE_USER_DATA, 0);
	}

	public static void setSignFlag(int value){
		getProxy().putInt(KEY_SIGN_FLAG, value, PREFERENCE_TYPE_USER_DATA);
	}

	public static boolean isDefaultIconFlag() {
		return getDefaultIconFlag() == 1;
	}

	public static int getDefaultIconFlag(){
		return getProxy().getInt(KEY_DEFAULT_ICON_FLAG, PREFERENCE_TYPE_USER_DATA, 0);
	}

	public static void setDefaultIconFlag(int value){
		getProxy().putInt(KEY_DEFAULT_ICON_FLAG, value, PREFERENCE_TYPE_USER_DATA);
	}

	public static boolean isShowNews(){
		if (!App.getInstance().isMarketVersion()) {
			return false;
		}
		return getProxy().getBoolean(KEY_SHOW_APP_LIST_FLAG, PREFERENCE_TYPE_USER_DATA, true);
//		return true;
	}

	public static void setShowNews(boolean value){
		getProxy().putBoolean(KEY_SHOW_APP_LIST_FLAG, value, PREFERENCE_TYPE_USER_DATA);
	}

	public static boolean isShowImageTip(){
		return getProxy().getBoolean(KEY_SHOW_IMAGE_TIP, PREFERENCE_TYPE_USER_DATA, true);
	}

	public static void setShowImageTip(boolean value){
		getProxy().putBoolean(KEY_SHOW_IMAGE_TIP, value, PREFERENCE_TYPE_USER_DATA);
	}

	//个人信息
	public static String getLocationCity(){
		return getProxy().getString(KEY_LOCATION_CITY, PREFERENCE_TYPE_USER_DATA, "");
	}

	public static void setLocationCity(String value){
		getProxy().putString(KEY_LOCATION_CITY, value, PREFERENCE_TYPE_USER_DATA);
	}

	public static String getIdCard(){
		return getProxy().getString(KEY_ID_CARD, PREFERENCE_TYPE_USER_DATA, "");
	}

	public static void setIdCard(String value){
		getProxy().putString(KEY_ID_CARD, value, PREFERENCE_TYPE_USER_DATA);
	}

	public static String getMarryFlag(){
		return getProxy().getString(KEY_MARRY_FLAG, PREFERENCE_TYPE_USER_DATA, "");
	}

	public static void setMarryFlag(String value){
		getProxy().putString(KEY_MARRY_FLAG, value, PREFERENCE_TYPE_USER_DATA);
	}

	public static String getEducationLevel(){
		return getProxy().getString(KEY_EDUCATION_LEVEL, PREFERENCE_TYPE_USER_DATA, "");
	}

	public static void setEducationLevel(String value){
		getProxy().putString(KEY_EDUCATION_LEVEL, value, PREFERENCE_TYPE_USER_DATA);
	}

	//身份信息
	public static String getProfession(){
		return getProxy().getString(KEY_PROFESSION, PREFERENCE_TYPE_USER_DATA, "");
	}

	public static void setProfession(String value){
		getProxy().putString(KEY_PROFESSION, value, PREFERENCE_TYPE_USER_DATA);
	}

	public static String getIncomeType(){
		return getProxy().getString(KEY_INCOME_TYPE, PREFERENCE_TYPE_USER_DATA, "");
	}

	public static void setIncomeType(String value){
		getProxy().putString(KEY_INCOME_TYPE, value, PREFERENCE_TYPE_USER_DATA);
	}

	public static String getIncomeValue(){
		return getProxy().getString(KEY_INCOME_VALUE, PREFERENCE_TYPE_USER_DATA, "");
	}

	public static void setIncomeValue(String value){
		getProxy().putString(KEY_INCOME_VALUE, value, PREFERENCE_TYPE_USER_DATA);
	}

	public static String getCompanyType(){
		return getProxy().getString(KEY_COMPANY_TYPE, PREFERENCE_TYPE_USER_DATA, "");
	}

	public static void setCompanyType(String value){
		getProxy().putString(KEY_COMPANY_TYPE, value, PREFERENCE_TYPE_USER_DATA);
	}

	public static String getCompanyWorkTime(){
		return getProxy().getString(KEY_COMPANY_WORK_TIME, PREFERENCE_TYPE_USER_DATA, "");
	}

	public static void setCompanyWorkTime(String value){
		getProxy().putString(KEY_COMPANY_WORK_TIME, value, PREFERENCE_TYPE_USER_DATA);
	}

	public static String getMoreSixFundFlag(){
		return getProxy().getString(KEY_MORE_SIX_FUND_FLAG, PREFERENCE_TYPE_USER_DATA, "");
	}

	public static void setMoreSixFundFlag(String value){
		getProxy().putString(KEY_MORE_SIX_FUND_FLAG, value, PREFERENCE_TYPE_USER_DATA);
	}

	public static String getMoreSixSocialFlag(){
		return getProxy().getString(KEY_MORE_SIX_SOCIAL_FLAG, PREFERENCE_TYPE_USER_DATA, "");
	}

	public static void setMoreSixSocialFlag(String value){
		getProxy().putString(KEY_MORE_SIX_SOCIAL_FLAG, value, PREFERENCE_TYPE_USER_DATA);
	}

	//房产信息
	public static String getHasCreditFlag(){
		return getProxy().getString(KEY_HAS_CREDIT_FLAG, PREFERENCE_TYPE_USER_DATA, "");
	}

	public static void setHasCreditFlag(String value){
		getProxy().putString(KEY_HAS_CREDIT_FLAG, value, PREFERENCE_TYPE_USER_DATA);
	}

	public static String getContainHouseType(){
		return getProxy().getString(KEY_CONTAIN_HOUSE_TYPE, PREFERENCE_TYPE_USER_DATA, "");
	}

	public static void setContainHouseType(String value){
		getProxy().putString(KEY_CONTAIN_HOUSE_TYPE, value, PREFERENCE_TYPE_USER_DATA);
	}

	public static String getContainCarType(){
		return getProxy().getString(KEY_CONTAIN_CAR_TYPE, PREFERENCE_TYPE_USER_DATA, "");
	}

	public static void setContainCarType(String value){
		getProxy().putString(KEY_CONTAIN_CAR_TYPE, value, PREFERENCE_TYPE_USER_DATA);
	}

	public static String getSuccessLoanFlag(){
		return getProxy().getString(KEY_SUCCESS_LOAN_FLAG, PREFERENCE_TYPE_USER_DATA, "");
	}

	public static void setSuccessLoanFlag(String value){
		getProxy().putString(KEY_SUCCESS_LOAN_FLAG, value, PREFERENCE_TYPE_USER_DATA);
	}

	public static String getCreditStanding(){
		return getProxy().getString(KEY_CREDIT_STANDING, PREFERENCE_TYPE_USER_DATA, "");
	}

	public static void setCreditStanding(String value){
		getProxy().putString(KEY_CREDIT_STANDING, value, PREFERENCE_TYPE_USER_DATA);
	}


}
