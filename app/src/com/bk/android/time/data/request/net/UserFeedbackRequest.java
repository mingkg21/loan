package com.bk.android.time.data.request.net;

import android.content.Context;

import com.bk.android.data.net.RequestData;
import com.bk.android.time.data.UserData;
import com.bk.android.time.data.request.AbsNetDataRequest;
import com.bk.android.time.entity.SimpleData;

import java.io.Serializable;
import java.util.HashMap;

/**
 * @author mingkg21
 * @email mingkg21@gmail.com
 * @date 2016年12月15日
 *
 */
public class UserFeedbackRequest extends AbsNetDataRequest {


	private static final long serialVersionUID = -5693679703523589647L;

	private String mKindId;
	private String mContent;
	private String mContactType;
	private String mContact;

	public UserFeedbackRequest(String kindId, String content, String contactType, String contact) {
		mKindId = kindId;
		mContent = content;
		mContactType = contactType;
		mContact = contact;
	}

	@Override
	public int getCacheTime() {
		return 0;
	}

	@Override
	protected boolean isNeedCheckLogin() {
		return true;
	}

	@Override
	protected boolean isUserRelevance() {
		return true;
	}
	
	@Override
	protected String createDataKey() {
		return createKey(this, mKindId, mContent, mContactType, mContact);
	}

	@Override
	protected Serializable onRunTask(Context context) {
		HashMap<String, Object> sendData = new HashMap<>();
		sendData.put("kindId", mKindId);
		sendData.put("content", mContent);
		sendData.put("userId", UserData.getUserId());
		sendData.put("contactType", mContactType);
		sendData.put("contact", mContact);

		SimpleData data = connection(new RequestData("user.userFeedBack", sendData)
				,SimpleData.class);
		return data;
	}
}
