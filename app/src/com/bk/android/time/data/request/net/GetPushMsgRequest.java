package com.bk.android.time.data.request.net;

import android.content.Context;

import com.bk.android.data.net.RequestData;
import com.bk.android.time.data.request.AbsNetDataRequest;
import com.bk.android.time.entity.PushMsgInfoListData;

import java.io.Serializable;
import java.util.HashMap;

public class GetPushMsgRequest extends AbsNetDataRequest {

	private static final long serialVersionUID = -8454536801371236582L;

	public GetPushMsgRequest() {
	}

	@Override
	public int getCacheTime() {
		return DEFAULT_CACHE_TIME;
	}

	@Override
	public boolean isNeedSaveDB() {
		return true;
	}

	@Override
	protected boolean isNeedCheckLogin() {
		return false;
	}

	@Override
	protected boolean isUserRelevance() {
		return false;
	}

	@Override
	protected String createDataKey() {
		return createKey(this);
	}

	@Override
	protected String createDataGroupKey(){
		return createGroupKey(this);
	}

	@Override
	protected Serializable onRunTask(Context context) {
		HashMap<String, Object> sendData = new HashMap<>();

		return connection(
				new RequestData("content.dataPush.getCrackGamePushMsg", sendData)
				,PushMsgInfoListData.class);
	}
}
