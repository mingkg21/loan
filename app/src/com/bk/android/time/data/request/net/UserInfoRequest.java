package com.bk.android.time.data.request.net;

import android.content.Context;

import com.bk.android.data.net.RequestData;
import com.bk.android.time.data.request.AbsNetDataRequest;
import com.bk.android.time.entity.UserInfoData;

import java.io.Serializable;
import java.util.HashMap;

public class UserInfoRequest extends AbsNetDataRequest {

	private static final long serialVersionUID = -8884994922874799L;
	
	public UserInfoRequest() {

	}
	
	@Override
	protected boolean isNeedCheckLogin() {
		return true;
	}

	@Override
	protected boolean isUserRelevance() {
		return true;
	}

	@Override
	protected String createDataKey() {
		return createKey(this);
	}

	@Override
	public int getCacheTime() {
		return 0;
	}

	@Override
	protected Serializable onRunTask(Context context) {
		UserInfoData userData = null;

		HashMap<String, Object> sendData = new HashMap<>();

		userData = connection(new RequestData("user.getDetailProfile", sendData)
				,UserInfoData.class);
		return userData;
	}
}
