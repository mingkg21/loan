package com.bk.android.time.data.request.net;

import android.content.Context;

import com.bk.android.data.net.RequestData;
import com.bk.android.time.data.request.AbsNetDataRequest;
import com.bk.android.time.entity.SimpleData;

import java.io.Serializable;
import java.util.HashMap;

public class AddPushRequest extends AbsNetDataRequest {

	public static final String UPDATE_TYPE_DISPLAY = "display_amount_add_one";
	public static final String UPDATE_TYPE_CLICK = "click_amount_add_one";
	public static final String UPDATE_TYPE_DOWNLOAD = "download_amount_add_one";

	private static final long serialVersionUID = -3217618339753401789L;

	private int id;
	private String updateType;

	public AddPushRequest(int id, String updateType) {
		this.id = id;
		this.updateType = updateType;
	}
	
	@Override
	protected boolean isNeedCheckLogin() {
		return false;
	}

	@Override
	protected boolean isUserRelevance() {
		return false;
	}

	@Override
	protected String createDataKey() {
		return createKey(this, id, updateType);
	}

	@Override
	public int getCacheTime() {
		return 0;
	}

	@Override
	protected Serializable onRunTask(Context context) {
		HashMap<String, Object> sendData = new HashMap<>();
		sendData.put("id", id);
		sendData.put("update_type", updateType);

		return connection(new RequestData("content.dataPush.addCrackGamePushCount", sendData)
				,SimpleData.class);
	}
}
