package com.bk.android.time.data.request.net;

import android.content.Context;

import com.bk.android.data.net.RequestData;
import com.bk.android.time.data.net.NetEaseConnect;
import com.bk.android.time.data.request.AbsNetDataRequest;
import com.bk.android.time.entity.NetEaseNewsData;

import java.io.Serializable;

public class NetEaseDetailRequest extends AbsNetDataRequest {

	private static final long serialVersionUID = -6735110601471515877L;

	private String mId;

	public NetEaseDetailRequest(String id) {
		mId = id;
	}

	@Override
	public int getCacheTime() {
		return DEFAULT_CACHE_TIME;
	}

	@Override
	public boolean isNeedSaveDB() {
		return true;
	}
	
	@Override
	protected boolean isNeedCheckLogin() {
		return false;
	}

	@Override
	protected boolean isUserRelevance() {
		return false;
	}
	
	@Override
	protected String createDataKey() {
		return createKey(this, mId);
	}
	
	@Override
	protected String createDataGroupKey(){
		return createGroupKey(this, mId);
	}

	@Override
	protected Serializable onRunTask(Context context) {
		RequestData requestData = new RequestData(RequestData.REQUEST_METHOD_GET,
				"http://c.m.163.com/nc/article/" + mId + "/full.html");
		requestData.setAction(mId);

		return NetEaseConnect.getInstance().connection(requestData, NetEaseNewsData.class);

	}
}
