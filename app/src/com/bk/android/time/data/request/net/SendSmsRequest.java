package com.bk.android.time.data.request.net;

/** 发送验证码接口
 * Created by mingkg21 on 2017/4/14.
 */

import android.content.Context;

import com.bk.android.data.net.RequestData;
import com.bk.android.time.data.request.AbsNetDataRequest;
import com.bk.android.time.entity.SimpleData;

import java.io.Serializable;
import java.util.HashMap;

public class SendSmsRequest extends AbsNetDataRequest {

    private static final long serialVersionUID = 1502008111840866693L;

    private String mPhone;

    public SendSmsRequest(String phone){
        mPhone = phone;
    }

    @Override
    protected boolean isNeedCheckLogin() {
        return false;
    }

    @Override
    protected boolean isUserRelevance() {
        return false;
    }

    @Override
    protected String createDataKey() {
        return createKey(this, mPhone);
    }

    @Override
    public int getCacheTime() {
        return 0;
    }

    @Override
    protected Serializable onRunTask(Context context) {
        HashMap<String, Object> sendData = new HashMap<>();
        sendData.put("phone", mPhone);

        return connection(
                new RequestData("user.sendSmsCode", sendData)
                ,SimpleData.class);
    }
}