package com.bk.android.time.data.request.net;

import android.content.Context;

import com.bk.android.data.net.RequestData;
import com.bk.android.time.data.request.AbsNetDataRequest;
import com.bk.android.time.entity.ConfigData;

import java.io.Serializable;
import java.util.HashMap;

public class ConfigLoadRequest extends AbsNetDataRequest {

	private static final long serialVersionUID = -10724457353747405L;

	@Override
	protected boolean isNeedCheckLogin() {
		return false;
	}

	@Override
	protected boolean isUserRelevance() {
		return false;
	}

	@Override
	protected String createDataKey() {
		return createKey(this);
	}

	@Override
	public int getCacheTime() {
		return 0;
	}

	@Override
	protected Serializable onRunTask(Context context) {
		HashMap<String, Object> sendData = new HashMap<>();

		return connection(new RequestData("common.config.load", sendData)
				,ConfigData.class);
	}
}
