package com.bk.android.time.data.request.net;

import android.content.Context;

import com.bk.android.data.net.RequestData;
import com.bk.android.time.data.request.AbsNetDataRequest;
import com.bk.android.time.entity.LoanInfoListData;

import java.io.Serializable;
import java.util.HashMap;

public class UserBrowseRequest extends AbsNetDataRequest {

	private static final long serialVersionUID = -1946555971687294008L;

	private int pageSize;
	private int page;

	public UserBrowseRequest(int page) {
		this.pageSize = 20;
		this.page = page;
	}

	@Override
	public int getCacheTime() {
		return 0;
	}

	@Override
	public boolean isNeedSaveDB() {
		return true;
	}

	@Override
	protected boolean isNeedCheckLogin() {
		return true;
	}

	@Override
	protected boolean isUserRelevance() {
		return true;
	}

	@Override
	protected String createDataKey() {
		return createKey(this, page, pageSize);
	}

	@Override
	protected String createDataGroupKey(){
		return createGroupKey(this, page, pageSize);
	}

	@Override
	protected Serializable onRunTask(Context context) {
		HashMap<String, Object> sendData = new HashMap<>();
//		sendData.put("user_token", UserData.getToken());
//		sendData.put("pageSize", pageSize);
//		sendData.put("page", page);

		return connection(new RequestData("log.userBrowse.list", sendData), LoanInfoListData.class);

	}
}
