package com.bk.android.time.data.request.net;

import android.content.Context;

import com.bk.android.data.net.RequestData;
import com.bk.android.time.data.UserData;
import com.bk.android.time.data.request.AbsNetDataRequest;
import com.bk.android.time.entity.UserInfoData;

import java.io.Serializable;
import java.util.HashMap;

public class ModifyUserIconRequest extends AbsNetDataRequest {

	private static final long serialVersionUID = 2563087440090894117L;

	private String mIcon;

	public ModifyUserIconRequest(String icon) {
		this.mIcon = icon;
	}

	
	@Override
	protected boolean isNeedCheckLogin() {
		return true;
	}

	@Override
	protected boolean isUserRelevance() {
		return true;
	}

	@Override
	protected String createDataKey() {
		return createKey(this, mIcon);
	}

	@Override
	public int getCacheTime() {
		return 0;
	}

	@Override
	protected Serializable onRunTask(Context context) {
		HashMap<String, Object> sendData = new HashMap<>();
		sendData.put("user_token", UserData.getToken());
		sendData.put("icon", mIcon);

		UserInfoData data = connection(new RequestData("user.updateProfile", sendData)
				,UserInfoData.class);
		return data;
	}
}
