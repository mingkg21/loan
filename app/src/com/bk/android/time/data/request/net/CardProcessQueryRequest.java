package com.bk.android.time.data.request.net;

import android.content.Context;

import com.bk.android.data.net.RequestData;
import com.bk.android.time.data.request.AbsNetDataRequest;
import com.bk.android.time.entity.BankQueryListData;

import java.io.Serializable;
import java.util.HashMap;

public class CardProcessQueryRequest extends AbsNetDataRequest {

    private static final long serialVersionUID = 6715929856313043337L;

    @Override
    protected boolean isNeedCheckLogin() {
        return true;
    }

    @Override
    protected boolean isUserRelevance() {
        return true;
    }

    @Override
    protected String createDataKey() {
        return createKey(this);
    }

    @Override
    public int getCacheTime() {
        return 0;
    }

    @Override
    protected Serializable onRunTask(Context context) {
        HashMap<String, Object> sendData = new HashMap<>();

        return connection(
                new RequestData("content.bank.cardProcessQry", sendData)
                , BankQueryListData.class);
    }
}
