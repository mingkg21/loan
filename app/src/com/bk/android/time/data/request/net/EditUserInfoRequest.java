package com.bk.android.time.data.request.net;

import android.content.Context;

import com.bk.android.data.net.RequestData;
import com.bk.android.time.data.request.AbsNetDataRequest;
import com.bk.android.time.entity.SimpleData;

import java.io.Serializable;
import java.util.HashMap;

public class EditUserInfoRequest extends AbsNetDataRequest {

	private static final long serialVersionUID = -8884994922874799L;

	private HashMap<String, Object> parameterMap;

	public EditUserInfoRequest(HashMap<String, Object> parameterMap) {
		this.parameterMap = parameterMap;
	}
	
	@Override
	protected boolean isNeedCheckLogin() {
		return true;
	}

	@Override
	protected boolean isUserRelevance() {
		return true;
	}

	@Override
	protected String createDataKey() {
		return createKey(this, parameterMap.hashCode());
	}

	@Override
	public int getCacheTime() {
		return 0;
	}

	@Override
	protected Serializable onRunTask(Context context) {
		HashMap<String, Object> sendData = new HashMap<>();
		sendData.putAll(parameterMap);

		SimpleData data = connection(new RequestData("user.updateProfileDetail", sendData),SimpleData.class);
		return data;
	}
}
