package com.bk.android.time.data.request.net;

import android.content.Context;

import com.bk.android.data.net.RequestData;
import com.bk.android.time.data.request.AbsNetDataRequest;
import com.bk.android.time.entity.SimpleData;

import java.io.Serializable;
import java.util.HashMap;

public class GetValueFromCacheRequest extends AbsNetDataRequest {

	private static final long serialVersionUID = 7827569888529390235L;

	private String settingName;

	public GetValueFromCacheRequest(String settingName) {
		this.settingName = settingName;
	}
	
	@Override
	protected boolean isNeedCheckLogin() {
		return false;
	}

	@Override
	protected boolean isUserRelevance() {
		return false;
	}

	@Override
	protected String createDataKey() {
		return createKey(this, settingName);
	}

	@Override
	public int getCacheTime() {
		return 0;
	}

	@Override
	protected Serializable onRunTask(Context context) {
		HashMap<String, Object> sendData = new HashMap<>();
		return connection(new RequestData("common.config.getChannelGameShowFlag", sendData)
				,SimpleData.class);
	}
}
