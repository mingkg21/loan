package com.bk.android.time.data.request.net;

import android.content.Context;

import com.bk.android.data.net.RequestData;
import com.bk.android.time.data.UserData;
import com.bk.android.time.data.request.AbsNetDataRequest;
import com.bk.android.time.entity.SimpleData;

import java.io.Serializable;
import java.util.HashMap;

/**
 * @author mingkg21
 * @email mingkg21@gmail.com
 * @date 2016年12月15日
 *
 */
public class ModifyNicknameRequest extends AbsNetDataRequest {


	private static final long serialVersionUID = 8108109881323837151L;

	private String mNickname;

	public ModifyNicknameRequest(String nickname){
		mNickname = nickname;
	}
	
	@Override
	public int getCacheTime() {
		return 0;
	}

	@Override
	protected boolean isNeedCheckLogin() {
		return true;
	}

	@Override
	protected boolean isUserRelevance() {
		return true;
	}
	
	@Override
	protected String createDataKey() {
		return createKey(this, mNickname);
	}

	@Override
	protected Serializable onRunTask(Context context) {
		HashMap<String, Object> sendData = new HashMap<>();
		sendData.put("user_token", UserData.getToken());
		sendData.put("nickName", mNickname);

		SimpleData data = connection(new RequestData("user.modifyNickName", sendData)
				,SimpleData.class);
		return data;
	}
}
