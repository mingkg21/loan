package com.bk.android.time.data.request.net;

import android.content.Context;

import com.bk.android.data.net.RequestData;
import com.bk.android.time.data.request.AbsNetDataRequest;
import com.bk.android.time.entity.SimpleData;

import java.io.Serializable;
import java.util.HashMap;

public class ResetPassowrdRequest extends AbsNetDataRequest {

	private static final long serialVersionUID = 3104188753175221624L;

	private String mUserName;
	private String mPassword;
	private String mValidateCode;

	public ResetPassowrdRequest(String userName, String password, String validateCode) {
		mUserName = userName;
		mPassword = password;
		mValidateCode = validateCode;
	}
	
	@Override
	protected boolean isNeedCheckLogin() {
		return false;
	}

	@Override
	protected boolean isUserRelevance() {
		return false;
	}

	@Override
	protected String createDataKey() {
		return createKey(this, mUserName, mPassword, mValidateCode);
	}

	@Override
	public int getCacheTime() {
		return 0;
	}

	@Override
	protected Serializable onRunTask(Context context) {
		HashMap<String, Object> sendData = new HashMap<>();
		sendData.put("phone", mUserName);
		sendData.put("newPassword", mPassword);
		sendData.put("validateCode", mValidateCode);

		return connection(new RequestData("user.resetPasswordByPhone", sendData), SimpleData.class);
	}
}
