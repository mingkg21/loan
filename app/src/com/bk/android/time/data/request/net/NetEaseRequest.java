package com.bk.android.time.data.request.net;

import android.content.Context;

import com.bk.android.data.net.RequestData;
import com.bk.android.time.data.net.NetEaseConnect;
import com.bk.android.time.data.request.AbsNetDataRequest;
import com.bk.android.time.entity.NetEaseNewsListData;

import java.io.Serializable;

public class NetEaseRequest extends AbsNetDataRequest {

	private static final long serialVersionUID = -7981137695440056799L;

	private int mStart;

	public NetEaseRequest(int start) {
		mStart = start;
	}

	@Override
	public int getCacheTime() {
		return DEFAULT_CACHE_TIME;
	}

	@Override
	public boolean isNeedSaveDB() {
		return true;
	}
	
	@Override
	protected boolean isNeedCheckLogin() {
		return false;
	}

	@Override
	protected boolean isUserRelevance() {
		return false;
	}
	
	@Override
	protected String createDataKey() {
		return createKey(this, mStart);
	}
	
	@Override
	protected String createDataGroupKey(){
		return createGroupKey(this, mStart);
	}

	@Override
	protected Serializable onRunTask(Context context) {
		int pageSzie = 20;

		RequestData requestData = new RequestData(RequestData.REQUEST_METHOD_GET,
				"http://c.m.163.com/nc/subscribe/list/T1414984365340/all/" + mStart + "-" + (mStart + pageSzie) + ".html");

		return NetEaseConnect.getInstance().connection(requestData, NetEaseNewsListData.class);

	}
}
