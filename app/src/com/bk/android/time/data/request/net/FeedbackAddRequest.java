package com.bk.android.time.data.request.net;

import java.io.Serializable;

import android.content.Context;
import android.text.TextUtils;

import com.bk.android.data.net.RequestData;
import com.bk.android.time.data.request.AbsNetDataRequest;
import com.bk.android.time.entity.SimpleData;

public class FeedbackAddRequest extends AbsNetDataRequest {
	private static final long serialVersionUID = 7605814647608694645L;

	private String mContent;
	private String mQQ;
	private String mNumber;

	public FeedbackAddRequest(String content,String qq,String number) {
		mContent = content;
		mQQ = qq;
		mNumber = number;
	}

	@Override
	protected boolean isNeedCheckLogin() {
		return false;
	}

	@Override
	protected boolean isUserRelevance() {
		return false;
	}

	@Override
	protected String createDataKey() {
		return createKey(this, mContent,mQQ,mNumber);
	}

	@Override
	public int getCacheTime() {
		return 0;
	}

	@Override
	protected Serializable onRunTask(Context context) {
		StringBuffer sendData = new StringBuffer();
		sendData.append("content=");
		sendData.append(mContent);
		if(!TextUtils.isEmpty(mQQ)){
			sendData.append("&");
			sendData.append("qq=");
			sendData.append(mQQ);
		}
		if(!TextUtils.isEmpty(mNumber)){
			sendData.append("&");
			sendData.append("mobile=");
			sendData.append(mNumber);
		}
		SimpleData datas = connection(
				new RequestData(RequestData.REQUEST_METHOD_POST,sendData.toString(),"addfeedback")
				,SimpleData.class);
		return datas;
	}
}
