package com.bk.android.time.data.request.net;

import android.content.Context;
import android.text.TextUtils;

import com.bk.android.data.net.RequestData;
import com.bk.android.time.data.request.AbsNetDataRequest;
import com.bk.android.time.entity.LoginInfoData;

import java.io.Serializable;
import java.util.HashMap;

public class LoginRequest extends AbsNetDataRequest {
	/**
	 * 
	 */
	private static final long serialVersionUID = 399935034792021387L;
	private String mUserName;
	private String mPassword;
	private String mValidateCode;

	public LoginRequest(String userName, String password, String validateCode) {
		mUserName = userName;
		mPassword = password;
		mValidateCode = validateCode;
	}
	
	@Override
	protected boolean isNeedCheckLogin() {
		return false;
	}

	@Override
	protected boolean isUserRelevance() {
		return false;
	}

	@Override
	protected String createDataKey() {
		return createKey(this, mUserName, mPassword, mValidateCode);
	}

	@Override
	public int getCacheTime() {
		return 0;
	}

	@Override
	protected Serializable onRunTask(Context context) {
		String action = "user.login";
		HashMap<String, Object> sendData = new HashMap<>();
		sendData.put("username", mUserName);
		if (!TextUtils.isEmpty(mPassword)) {
			sendData.put("password", mPassword);
		} else {
			sendData.put("validateCode", mValidateCode);
			action = "user.loginByPhoneCode";
		}

		return connection(new RequestData(action, sendData), LoginInfoData.class);
	}
}
