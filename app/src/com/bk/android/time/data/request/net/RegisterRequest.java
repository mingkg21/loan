package com.bk.android.time.data.request.net;

import android.content.Context;

import com.bk.android.data.net.RequestData;
import com.bk.android.time.data.request.AbsNetDataRequest;
import com.bk.android.time.entity.LoginInfoData;

import java.io.Serializable;
import java.util.HashMap;

/**
 * 注册接口
 */
public class RegisterRequest extends AbsNetDataRequest {

	private static final long serialVersionUID = 399935034792021387L;

	private String mUsetName;
	private String mPassword;
	private String mSecurityCode;

	public RegisterRequest(String usetName, String password, String securityCode){
		mUsetName = usetName;
		mPassword = password;
		mSecurityCode = securityCode;
	}
	
	@Override
	protected boolean isNeedCheckLogin() {
		return false;
	}

	@Override
	protected boolean isUserRelevance() {
		return false;
	}

	@Override
	protected String createDataKey() {
		return createKey(this, mUsetName, mPassword, mSecurityCode);
	}

	@Override
	public int getCacheTime() {
		return 0;
	}

	@Override
	protected Serializable onRunTask(Context context) {
		HashMap<String, Object> sendData = new HashMap<>();
		sendData.put("phone", mUsetName);
		sendData.put("password", mPassword);
		sendData.put("smsCode", mSecurityCode);
		sendData.put("nickName", "");

		return connection(
				new RequestData("user.registerByPhone", sendData)
				,LoginInfoData.class);
	}
}
