package com.bk.android.time.data.request.net;

import android.content.Context;

import com.bk.android.data.net.RequestData;
import com.bk.android.time.data.request.AbsNetDataRequest;
import com.bk.android.time.entity.MessageInfoListData;

import java.io.Serializable;
import java.util.HashMap;

public class MsgListRequest extends AbsNetDataRequest {

	private static final long serialVersionUID = -4712194903838053064L;

	public static final String MSG_TYPE_SYSTEM = "systemeRecommend";
	public static final String MSG_TYPE_ACTIVITY = "loanRecommend";

	private int pageSize;
	private int page;
	private String msgType;

	public MsgListRequest(int page, String msgType) {
		this.pageSize = 20;
		this.page = page;
		this.msgType = msgType;
	}

	@Override
	public int getCacheTime() {
		return 0;
	}

	@Override
	public boolean isNeedSaveDB() {
		return true;
	}

	@Override
	protected boolean isNeedCheckLogin() {
		return true;
	}

	@Override
	protected boolean isUserRelevance() {
		return true;
	}

	@Override
	protected String createDataKey() {
		return createKey(this, page, pageSize, msgType);
	}

	@Override
	protected String createDataGroupKey(){
		return createGroupKey(this, page, pageSize, msgType);
	}

	@Override
	protected Serializable onRunTask(Context context) {
		HashMap<String, Object> sendData = new HashMap<>();
		sendData.put("pageSize", pageSize);
		sendData.put("page", page);
		sendData.put("msgType", msgType);

		return connection(new RequestData("user.userMsg.msgList", sendData), MessageInfoListData.class);

	}
}
