package com.bk.android.time.data.request;

import java.io.Serializable;
import java.util.HashMap;

import android.content.Context;
import android.text.TextUtils;

import com.bk.android.data.BaseNetDataRequest;
import com.bk.android.data.net.AbsConnect.ParamEncipher;
import com.bk.android.data.net.RequestData;
import com.bk.android.time.data.UserData;
import com.bk.android.time.data.net.HttpConnect;
import com.bk.android.time.entity.BaseEntity;
import com.bk.android.time.entity.SimpleData;

public abstract class AbsNetDataRequest extends BaseNetDataRequest {
	private static final long serialVersionUID = -9085174890593149806L;
	private static HashMap<Integer, Boolean> sSubmitErrMap = new HashMap<Integer, Boolean>();
	protected static final int DEFAULT_CACHE_TIME = 1000 * 60 * 5;
	protected String mUserId;
	
	public AbsNetDataRequest(){
		mUserId = UserData.getUserId();
	}
	
	@Override
	protected void onClientInit() {
	}
	
	public static HttpConnect getHttpConnect(){
		return HttpConnect.getInstance();
	}
	
	public static <T> T connection(RequestData requestData,Class<T> clazz){
		return getHttpConnect().connection(requestData, clazz);
	}
	
	public static String createSendData(String... keyValues){
		return HttpConnect.createSendData(keyValues);
	}
	
	public static String createSendData(ParamEncipher paramEncipher,String... keyValues){
		return HttpConnect.createSendData(paramEncipher, keyValues);
	}
	
	@Override
	protected final boolean checkLogin() {
		if(isNeedCheckLogin()){
			return isLogin();
		}
		return true;
	}

	@Override
	public final String getDataGroupKey() {
		String createDataGroupKey = createDataGroupKey();
		if(createDataGroupKey != null){
			StringBuffer key = new StringBuffer(createDataGroupKey);
			if(isUserRelevance()){
				key.append("_");
				key.append(mUserId);
			}
			return key.toString();
		}
		return super.getDataGroupKey();
	}
	
	@Override
	public final String getDataKey() {
		StringBuffer key = new StringBuffer(createDataKey());
		if(isUserRelevance()){
			key.append("_");
			key.append(mUserId);
		}
		return key.toString();
	}

	protected boolean isLogin(){
		return !UserData.DESIRE_USER_ID.equals(UserData.getUserId());
	}
	
	@Override
	public String[] getNeedNotifyGroupKey() {
		return null;
	}

	@Override
	protected boolean isDataNeedSave(Serializable data) {
		if(data instanceof BaseEntity && ((BaseEntity<?>)data).isSucceed()){
			return super.isDataNeedSave(data);
		}
		return false;
	}
	
	@Override
	protected boolean checkRequest(Context context) {
		return true;
	}

	@Override
	public int getCacheTime() {
		return 0;
	}

	protected void sleepThread(long time){
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {}
	}
	
	//
//	public boolean isUserChange(){
//		if(isUserRelevance()){
//			return !mUserId.equals(UserData.getUserId());
//		}
//		return false;
//	}
	/**
	 * 是否需要检查登录
	 * @return
	 */
	protected abstract boolean isNeedCheckLogin();
	/**
	 * 是否是用户关联数据
	 * @return
	 */
	protected abstract boolean isUserRelevance();
	/**
	 * 生成请求唯一标示
	 * @return
	 */
	protected abstract String createDataKey();
	
	/**
	 * 生成请求分类唯一标示
	 * @return
	 */
	protected String createDataGroupKey(){
		return null;
	}
	
	public static void submitErr(String params,String function){
		try {
			int key = (params + function).hashCode();
			synchronized (sSubmitErrMap) {
				if(sSubmitErrMap.containsKey(key)){
					return;
				}
				sSubmitErrMap.put(key, true);
			}
			HashMap<String, String> sendCompositeData = new HashMap<String, String>();
			sendCompositeData.put("params",params);
			sendCompositeData.put("function",function);
			SimpleData simpleData = connection(new RequestData(RequestData.REQUEST_METHOD_POST,sendCompositeData,"submiterror"),SimpleData.class);
			synchronized (sSubmitErrMap) {
				if(simpleData == null || !simpleData.isSucceed()){
					sSubmitErrMap.remove(key);
				}
			}
		} catch (Exception e) {}
	}
	
	/**
	 * 错误日志统计
	 * @param entity
	 * @param params
	 * @param function
	 */
	public static void submitErrInThread(BaseEntity<?> entity,String params,final String function){
		submitErrInThread(entity, params,null, function);
	}
	/**
	 * 错误日志统计
	 * @param entity
	 * @param params
	 * @param clientErr
	 * @param function
	 */
	public static void submitErrInThread(BaseEntity<?> entity,String params,String clientErr,final String function){
		try {
			if(entity != null && entity.isSucceed()){
				return;
			}
			final StringBuffer errStr = new StringBuffer();
			if(entity != null){
				if(BaseEntity.CODE_FAIL.equals(entity.getResultCode()) && !TextUtils.isEmpty(entity.getHttpCode())){
					errStr.append("httpCode:'"+entity.getHttpCode()+"';");
				}else{
					errStr.append("code:'"+entity.getResultCode()+"';");
				}
				if(!TextUtils.isEmpty(entity.getErrMsg())){
					errStr.append("errmsg:'"+entity.getErrMsg()+"';");
				}
			}
			errStr.append("data:'"+params+"';");
			if(clientErr != null){
				errStr.append("Exception:'"+clientErr+"';");
			}
			new Thread(){
				@Override
				public void run() {
					submitErr(errStr.toString(),function);
				}
			}.start();
		} catch (Exception e) {
		}
	}
}
