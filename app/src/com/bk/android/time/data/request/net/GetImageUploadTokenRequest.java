package com.bk.android.time.data.request.net;

import android.content.Context;

import com.bk.android.data.net.RequestData;
import com.bk.android.time.data.UserData;
import com.bk.android.time.data.request.AbsNetDataRequest;
import com.bk.android.time.entity.ImageUploadInfoData;
import com.bk.android.time.entity.UserInfoData;

import java.io.Serializable;
import java.util.HashMap;

public class GetImageUploadTokenRequest extends AbsNetDataRequest {

	private static final long serialVersionUID = -1028184693634868601L;

	public GetImageUploadTokenRequest() {

	}
	
	@Override
	protected boolean isNeedCheckLogin() {
		return true;
	}

	@Override
	protected boolean isUserRelevance() {
		return true;
	}

	@Override
	protected String createDataKey() {
		return createKey(this);
	}

	@Override
	public int getCacheTime() {
		return 0;
	}

	@Override
	protected Serializable onRunTask(Context context) {
		UserInfoData userData = null;

		HashMap<String, Object> sendData = new HashMap<>();
		sendData.put("user_token", UserData.getToken());

		return connection(new RequestData("common.cdn.getImageUploadToken", sendData)
				,ImageUploadInfoData.class);
	}
}
