package com.bk.android.time.data;

import android.content.Context;
import android.text.TextUtils;

import com.bk.android.dao.DBKeyValueProviderProxy;
import com.bk.android.data.net.RequestData;
import com.bk.android.data.net.RequestData.ProgressCallback;
import com.bk.android.os.AbsTerminableThread;
import com.bk.android.os.AbsThreadPool;
import com.bk.android.time.data.dao.DBPreferencesProvider;
import com.bk.android.time.data.request.AbsNetDataRequest;
import com.bk.android.time.entity.BaseDataEntity;
import com.bk.android.time.entity.BaseEntity;
import com.bk.android.time.util.AppFileUtil;
import com.bk.android.time.util.ImageCompressService;
import com.bk.android.time.widget.ImageLoader;
import com.bk.android.util.BitmapUtil;
import com.bk.android.util.FileUtil;
import com.bk.android.util.LockUtil;
import com.bk.android.util.LogUtil;
import com.google.gson.annotations.SerializedName;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

public class RecordFlieNetUrlData {
	private static final String TAG = "RecordFlieNetUrlData";
	
	private static final String KEY_PRE_NET_URL = "NEW_RECORD_FLIE_NET_URL_DATA_KEY_PRE_";
	private static final String KEY_PRE_LOCAL_URL = "NEW_RECORD_FLIE_LOCAL_URL_DATA_KEY_PRE_";
	private static HashMap<String, String> sImgNetUrlMap = new HashMap<String, String>();
	private static HashMap<String, String> sImgLocalUrlMap = new HashMap<String, String>();
	private static boolean sIsCacheInitFinish;
	
	public static void initCache(){
		Thread thread = new Thread(){
			@Override
			public void run() {
				ArrayList<String[]> keyValues = getProxy().getAllKeyValueByType(KEY_PRE_NET_URL);
				if(keyValues != null){
					for (String[] keyValue : keyValues) {
						synchronized (sImgNetUrlMap) {
							String key = keyValue[0];
							String value = keyValue[1];
							if(value == null){
								value = "";
							}
							sImgNetUrlMap.put(key, value);
						}
					}
				}
				sIsCacheInitFinish = true;
			}
		};
		thread.setPriority(Thread.MIN_PRIORITY);
		thread.start();
	}
	
	private static DBKeyValueProviderProxy getProxy(){
		return DBPreferencesProvider.getProxy();
	}
	
	public static String getImgLocalPath(String netUrl){
		synchronized (sImgLocalUrlMap) {
			String key = KEY_PRE_LOCAL_URL + netUrl;
			String localPath = sImgLocalUrlMap.get(key);
			if(localPath == null){
				if(sIsCacheInitFinish){
					localPath = "";
				}else{
					localPath = getProxy().getString(key,KEY_PRE_LOCAL_URL, "");
				}
				sImgLocalUrlMap.put(key, localPath);
			}
			return localPath;
		}
	}
	
	public static void setImgLocalUrl(String localPath,String netUrl){
		synchronized (sImgLocalUrlMap) {
			String key = KEY_PRE_LOCAL_URL + netUrl;
			localPath = localPath.replace("_source", "");
			sImgLocalUrlMap.put(key, localPath);
			getProxy().putString(key, localPath, KEY_PRE_LOCAL_URL);
		}
	}
	
	public static String getImgNetUrl(String localPath){
		synchronized (sImgNetUrlMap) {
			String key = KEY_PRE_NET_URL + localPath;
			String netUrl = sImgNetUrlMap.get(key);
			if(netUrl == null){
				if(sIsCacheInitFinish){
					netUrl = "";
				}else{
					netUrl = getProxy().getString(key,KEY_PRE_NET_URL, "");
				}
				sImgNetUrlMap.put(key, netUrl);
			}
			return netUrl;
		}
	}
	
	public static void setImgNetUrl(String localPath,String netUrl){
		synchronized (sImgNetUrlMap) {
			String key = KEY_PRE_NET_URL + localPath;
			sImgNetUrlMap.put(key, netUrl);
			getProxy().putString(key, netUrl, KEY_PRE_NET_URL);
			setImgLocalUrl(localPath, netUrl);
		}
	}
	
	public static boolean uploadFiles(ArrayList<UploadTask> uploadTaskList){
		boolean isSuccess = false;
		if(uploadTaskList != null){
			synchronized (uploadTaskList) {
				for (UploadTask taskInfo : uploadTaskList) {
					taskInfo.start();
				}
			}
			for (;true;) {
				synchronized (uploadTaskList) {
					int size = 0;
					int successSize = 0;
					for (UploadTask taskInfo : uploadTaskList) {
						if(taskInfo.isFinish()){
							size++;
						}
						if(taskInfo.isSuccess()){
							successSize++;
						}
					}
					isSuccess  = successSize == uploadTaskList.size();
					if(size == uploadTaskList.size()){
						break;
					}
				}
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {}
			}
		}
		return isSuccess;
	}
	
	private static String uploadFile(String filePath, Context context, ProgressCallback callback){
		if(TextUtils.isEmpty(filePath)){
			return null;
		}
		if(filePath.startsWith("http://")){
			return filePath;
		}
		if(filePath.startsWith("file://")){
			filePath = filePath.substring(7);
		}
		String netUrl = getImgNetUrl(filePath);
		if(!TextUtils.isEmpty(netUrl)){
			return netUrl;
		}
		synchronized (LockUtil.getLock(filePath)) {
			boolean isMp4 = BitmapUtil.isMp4Path(filePath);
			File file = new File(filePath);
			if(file.exists()){
				if(isMp4){
				}else{
					netUrl = uploadImg(filePath, context, callback);
				}
				if(!TextUtils.isEmpty(netUrl)){
					setImgNetUrl(filePath, netUrl);
				}
			}
			return netUrl;
		}
	}
	
	private static String uploadImg(String filePath, Context context, ProgressCallback callback){
		String netUrl = null;
		String format = BitmapUtil.getImgFormat(filePath);
		String cachePathPre = AppFileUtil.getRecordImgCachePath() + "temp_img";
		String cachePath = cachePathPre + filePath.hashCode();
		boolean isTemp = filePath.indexOf(cachePathPre) != -1;
		File cacheFile = new File(cachePath);
		if(!isTemp && !cacheFile.exists()){
			ImageCompressService.syncCompressionImg(filePath, cachePath, 1536, 2048, 90);
		}
//		if(cacheFile.exists() && (ApnUtil.isWifiWork(context) || isForce)){
		if(isTemp || cacheFile.exists()){
//			SystemConfig systemConfig = HttpConnect.getSystemConfig();
//			if(systemConfig != null){
////				systemConfig.setUploadType(1);
////				systemConfig.setUploadData("mBDD8A4WH2RhExFv,1REUUZjEMmUzBlyuBWwnuL7vyTUSYy,bk-forum,oss-cn-hangzhou.aliyuncs.com,http://osscdn.banketime.com/");
//				if(systemConfig.getUploadType() == 1 && OSSServiceUtil.initAccess(HttpConnect.getSystemConfig().getUploadData(), HttpConnect.getServerTime())){
//					netUrl = uploadFileToOss(isTemp ? filePath : cachePath, format, context, callback);
//				}else{
//					netUrl = uploadFileToSelf(isTemp ? filePath : cachePath, format, context, callback);
//				}
//			}
		}
		return netUrl;
	}
	
//	private static String uploadFileToOss(String filePath, String format, Context context, ProgressCallback callback){
//		String netUrl = null;
//		try {
//			String objKey = "upload/babyrecord/babyrecord_" + OSSToolKit.calMd5sumString(OSSToolKit.calFileMd5sum(filePath)) + "." + format;
//			File cacheFile = new File(filePath);
//			netUrl = OSSServiceUtil.upload(filePath ,objKey, format, callback);
//			if(!TextUtils.isEmpty(netUrl)){
//				String cachePath = AppFileUtil.getRecordImgCachePath() + "temp_";
//				if(!format.equals("mp4")){
//					if(format.equals("png")){
//						netUrl += "@!_min.stylePng";
//					}else{
//						netUrl += "@!_min.style";
//					}
//					ImageCompressService.syncCompressionImg(filePath, ImageLoader.getInstance().urlToPath(netUrl) ,620,1024, 90);
//					if(filePath.indexOf(cachePath) == -1){
//						ImageCompressService.syncCompressionImg(filePath, ImageLoader.getInstance().urlToPath(netUrl.replace("_min.", ".")), 1536, 2048, 90);
//					}else{
//						cacheFile.renameTo(new File(ImageLoader.getInstance().urlToPath(netUrl.replace("_min.", "."))));
//					}
//				}else{
//					if(filePath.indexOf(cachePath) == -1){
//						FileUtil.copyFile(filePath, ImageLoader.getInstance().urlToPath(netUrl, true));
//					}else{
//						cacheFile.renameTo(new File(ImageLoader.getInstance().urlToPath(netUrl, true)));
//					}
//				}
//			}else{
//				netUrl = null;
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//			netUrl = null;
//		}
//		return netUrl;
//	}
	
	private static String uploadFileToSelf(String filePath, String format, Context context, ProgressCallback callback){
		HashMap<String, String> sendCompositeData = new HashMap<String, String>();
		sendCompositeData.put("file",filePath);
		FileInfoData simpleData = null;
		File cacheFile = new File(filePath);
		try {
			simpleData = AbsNetDataRequest.connection(
					new RequestData(RequestData.REQUEST_METHOD_POST,sendCompositeData,"upfiletobabyrecord").setProgressCallback(callback)
					,FileInfoData.class);
		} catch (Exception e) {}
		String netUrl = null;
		if(simpleData != null && simpleData.isSucceed()){
			netUrl = simpleData.getData().file;
			if(!TextUtils.isEmpty(netUrl)){
				String cachePath = AppFileUtil.getRecordImgCachePath() + "temp_";
				if(!format.equals("mp4")){
					if(netUrl.indexOf("_min.") != -1){
						ImageCompressService.syncCompressionImg(filePath, ImageLoader.getInstance().urlToPath(netUrl) ,620,1024, 90);
						if(filePath.indexOf(cachePath) == -1){
							ImageCompressService.syncCompressionImg(filePath, ImageLoader.getInstance().urlToPath(netUrl.replace("_min.", ".")), 1536, 2048, 90);
						}else{
							cacheFile.renameTo(new File(ImageLoader.getInstance().urlToPath(netUrl.replace("_min.", "."))));
						}
					}else{
						if(filePath.indexOf(cachePath) == -1){
							ImageCompressService.syncCompressionImg(filePath, ImageLoader.getInstance().urlToPath(netUrl), 1536, 2048, 90);
						}else{
							cacheFile.renameTo(new File(ImageLoader.getInstance().urlToPath(netUrl)));
						}
					}
				}else{
					if(filePath.indexOf(cachePath) == -1){
						FileUtil.copyFile(filePath, ImageLoader.getInstance().urlToPath(netUrl, true));
					}else{
						cacheFile.renameTo(new File(ImageLoader.getInstance().urlToPath(netUrl, true)));
					}
				}
			}
		}
		return netUrl;
	}
	
	public static class UploadTask extends AbsTerminableThread implements ProgressCallback{
		private int mProgress;
		private boolean isFinish;
		private boolean isSuccess;
		private String mFileUrl;
		private Context mContext;
		private Thread mCallbackThread;
		private ArrayList<UploadTask> mUploadTaskList;
		private ProgressCallback mTotalCallback;
		private String mNetUrl;
		private Thread mRunThread;

		public UploadTask(String fileurl, Context context,ArrayList<UploadTask> uploadTaskList,ProgressCallback totalCallback){
			mFileUrl = fileurl;
			mContext = context;
			mCallbackThread = Thread.currentThread();
			mUploadTaskList = uploadTaskList;
			mTotalCallback = totalCallback;
		}

		public boolean isFinish() {
			return isFinish;
		}

		public boolean isSuccess() {
			return isSuccess;
		}
		
		public String getFileUrl() {
			return mFileUrl;
		}

		public String getNetUrl() {
			return mNetUrl;
		}
		
		@Override
		public void run() {
			mRunThread = Thread.currentThread();
			String netPath = null;
			try {
				netPath = uploadFile(mFileUrl, mContext,this);
				if(TextUtils.isEmpty(netPath)){
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {}
					if(!isCancel()){
						netPath = uploadFile(mFileUrl, mContext,this);
					}
				}
				LogUtil.e(TAG, "onFinish path=" + mFileUrl.hashCode() + " " + (!TextUtils.isEmpty(netPath) ? "succeed" : "failure"));
			} catch (Exception e) {}
			finish(netPath);
			mTotalCallback = null;
			mUploadTaskList = null;
			mRunThread = null;
		}
		
		@Override
		public void onUploadProgress(int p, int m) {
			if(m > 0){
				int progress = (int) (p * 1f / m * 100);
				if(progress > mProgress){
					mProgress = progress;
				}
			}
			if(mUploadTaskList != null){
				synchronized (mUploadTaskList) {
					int maxProgress = 0;
					for (UploadTask info : mUploadTaskList) {
						maxProgress += info.mProgress;
					}
					if(mTotalCallback != null){
						mTotalCallback.onUploadProgress(maxProgress, mUploadTaskList.size() * 100);
					}
				}
			}
		}
		
		@Override
		public final void onCancel() {
			if(mCallbackThread != null){
				mCallbackThread.interrupt();
				mCallbackThread = null;
			}
			if(mRunThread != null){
				mRunThread.interrupt();
				mRunThread = null;
			}
			LogUtil.e(TAG, "onCancel path=" + mFileUrl.hashCode());
		}
		
		private void finish(String netPath){
			isFinish = true;
			isSuccess = !TextUtils.isEmpty(netPath);
			onFinish(netPath);
			if(mCallbackThread != null){
				mCallbackThread.interrupt();
				mCallbackThread = null;
			}
			if(mRunThread != null){
				mRunThread.interrupt();
				mRunThread = null;
			}
		}
		
		protected void onFinish(String netPath){}

		@Override
		protected void runTask(Runnable runnable) {
			ThreadPool.getInstance().addTask(runnable);
		}
	}
	
	private static class ThreadPool extends AbsThreadPool{
	    private static ThreadPool instance = null;
	    public static synchronized ThreadPool getInstance() {
	        if (instance == null){
	        	instance = new ThreadPool();
	        }
	        return instance;
	    }
	    
	    private ThreadPool(){}

		@Override
		protected int getCorePoolSize() {
			return 3;
		}

		@Override
		protected int getMaximumPoolSize() {
			return 128;
		}

		@Override
		protected long getKeepAliveTime() {
			return 1;
		}

		@Override
		protected TimeUnit getTimeUnit() {
			return TimeUnit.SECONDS;
		}

		@Override
		protected BlockingQueue<Runnable> newQueue() {
			return new LinkedBlockingQueue<Runnable>();
		}

		@Override
		protected ThreadFactory newThreadFactory() {
			return new DefaultThreadFactory(Thread.MIN_PRIORITY);
		}
	}
	
	private class FileInfoData extends BaseEntity<FileData>{
		private static final long serialVersionUID = -1217089954958639352L;
	}
	
	private class FileData extends BaseDataEntity{
		private static final long serialVersionUID = 4877700187013461837L;
		@SerializedName("file")
		private String file;
	}
	
	private static class ProgressThread extends Thread{
		private boolean isStop;
		private ProgressCallback mCallback;
		private int time;
		
		public ProgressThread(ProgressCallback callback) {
			mCallback = callback;
		}

		@Override
		public void run() {
			while (!isStop) {
				if(mCallback != null){
					mCallback.onUploadProgress(time, 800);
					time++;
				}
				try {
					sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		
		public void stopThread(){
			isStop = true;
			if(!isInterrupted()){
				interrupt();
			}
		}
	}
}
