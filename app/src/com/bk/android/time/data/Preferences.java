package com.bk.android.time.data;

import com.bk.android.dao.DBKeyValueProviderProxy;
import com.bk.android.time.app.App;
import com.bk.android.time.data.dao.DBPreferencesProvider;
import com.bk.android.time.data.net.HttpConnect;
import com.bk.android.time.update.ClientInfo;
import com.bk.android.time.weixin.WXAPIManager;
import com.bk.android.util.AppUtil;
import com.google.gson.Gson;
import com.sina.weibo.sdk.auth.Oauth2AccessToken;

import java.util.Calendar;

/**
 * 应用程序设置
 * @author linyiwei
 */
public class Preferences {
	private static final String PREFERENCE_TYPE_NORMAL = "PREFERENCE_TYPE_NORMAL";
	private static final String PREFERENCE_TYPE_PERMANENT = "PREFERENCE_TYPE_PERMANENT";
	
	private static final String TAG_TRAFFIC_WARNING = "tag_traffic_warning";
	private static final String TAG_HTTP_URL_TYPE = "tag_http_url_type";
	private static final String TAG_APP_SHARE_URL = "tag_app_share_url";
	private static final String TAG_APP_SHARE_URL_TIME = "tag_app_share_url_time";
	private static final String TAG_MAIN_SYNC_OLD_WORK = "TAG_MAIN_SYNC_OLD_WORK";
	private static final String TAG_PLAY_MODE = "TAG_PLAY_MODE_";
	private static final String TAG_LAST_MARK_CATEGORY = "TAG_LAST_MARK_CATEGORY_";
	private static final String TAG_LAST_POST_CONTENT_ITEM = "TAG_LAST_POST_CONTENT_ITEM_";

	//消息提醒设置
	private static final String TAG_MESSAGE_PUSH = "TAG_MESSAGE_PUSH";
	private static final String TAG_MESSAGE_SYSTEM = "TAG_MESSAGE_SYSTEM";
	private static final String TAG_MESSAGE_MY_POST = "TAG_MESSAGE_MY_POST";
	private static final String TAG_MESSAGE_MY_COMMENT = "TAG_MESSAGE_MY_COMMENT";

	private static final String TAG_VACCIEN = "TAG_VACCIEN";
	private static final String TAG_RECORD_SYNC_TIME = "TAG_RECORD_SYNC_TIME_NEW_"  + App.getInstance().getRecordDataVersion();
	private static final String TAG_RECORD_PAGE_TIME = "TAG_RECORD_PAGE_TIME";
	private static final String TAG_RECORD_MIN_PAGE_TIME = "TAG_RECORD_MIN_PAGE_TIME";
	private static final String TAG_RECORD_SHOW_HELP = "TAG_RECORD_SHOW_HELP";
	private static final String TAG_LAST_POSTS_UNREAD_TIME = "TAG_LAST_POSTS_UNREAD_TIME";
	
	//提示去应用市场评论
	private static final String TAG_HAD_CLICK_GO_TO_MARKET = "TAG_HAD_CLICK_GO_TO_MARKET";
	private static final String TAG_CLICK_GO_TO_MARKET_IGNORE_DATE = "TAG_CLICK_GO_TO_MARKET_IGNORE_DATE";
	private static final String TAG_CLICK_GO_TO_MARKET_IGNORE = "TAG_CLICK_GO_TO_MARKET_IGNORE";
	
	//设置用户访问家庭的记录
	private static final String PREFERENCE_TYPE_FAMILY_VISIT = "PREFERENCE_TYPE_FAMILY_VISIT";
	private static final String TAG_FAMILY_VISIT_RECORD_OPEN_APP = "TAG_FAMILY_RECORD_OPEN_APP";
	private static final String TAG_FAMILY_VISIT_RECORD_DATE = "TAG_FAMILY_RECORD_DATE";
	
	//设置服务器：正式or测试，默认是正式服务器
	private static final String TAG_NORMAL_SERVER = "TAG_NORMAL_SERVER";
	private static final String TAG_SWITCH_NORMAL_SERVER_VISIBILITY = "TAG_SWITCH_NORMAL_SERVER_VISIBILITY";
	private static final String TAG_WEIBO_ACCESS_TOKEN = "TAG_WEIBO_ACCESS_TOKEN";
	private static final String TAG_NEW_PHOTO_TIP = "TAG_NEW_PHOTO_TIP";
	//升级提醒
	private static final String TAG_SHOW_UPDATE_AGAIN = "TAG_SHOW_UPDATE_AGAIN";
	private static final String TAG_SHOW_UPDATE_TIME = "TAG_SHOW_UPDATE_TIME";
	private static final String TAG_UPDATE_INFO = "TAG_UPDATE_INFO";
	//
	private static final String TAG_GUIDE_WELCOME = "TAG_GUIDE_WELCOME";
	private static final String TAG_GUIDE_FIRST_IMPORT_PHOTO = "TAG_GUIDE_FIRST_IMPORT_PHOTO";
	private static final String TAG_GUIDE_BABY_LIST = "TAG_GUIDE_BABY_LIST";
	private static final String TAG_GUIDE_MAGAZINE_BATCH = "TAG_GUIDE_MAGAZINE_BATCH";
	private static final String TAG_GUIDE_MAGAZINE_EDIT = "TAG_GUIDE_MAGAZINE_EDIT";

	private static final String TAG_RECORD_SHARE_QZONE = "TAG_RECORD_SHARE_QZONE";
	private static final String TAG_RECORD_SHARE_FRIEND = "TAG_RECORD_SHARE_FRIEND";
	private static final String TAG_RECORD_SHARE_WEIBO = "TAG_RECORD_SHARE_WEIBO";

	private static final String TAG_CHECK_RELOGIN_PRE = "TAG_CHECK_RELOGIN_PRE";
	private static final String TAG_ACTION_MESSAGE_LAST_TIME = "TAG_ACTION_MESSAGE_LAST_TIME";
	private static final String TAG_NEW_USER = "TAG_NEW_USER";
	private static final String TAG_QR_CODE_SCAN_TIP = "TAG_QR_CODE_SCAN_TIP";
	private static final String TAG_OPEN_WX_TIP = "TAG_OPEN_WX_TIP";
	private static final String TAG_OPEN_APP_DAYS = "TAG_OPEN_APP_DAYS";
	private static final String TAG_SPLASH_LAST_SHOW_AD_ID = "TAG_SPLASH_LAST_SHOW_AD_ID";
	private static final String TAG_GUARD_ACTIVITY_LAUNCH_LAST_TIME = "TAG_GUARD_ACTIVITY_LAUNCH_LAST_TIME";
	private static final String TAG_NEW_USER_GUIDE_RECORD = "TAG_NEW_USER_GUIDE_RECORD";
	private static final String TAG_MAGAZINE_MAKE_GUIDE_PREVIEW = "TAG_MAGAZINE_MAKE_GUIDE_PREVIEW";
	private static final String TAG_PRINT_EDIT_IMG_TIP = "TAG_PRINT_EDIT_IMG_TIP";

	private static Preferences instance;
	
	private Preferences() {}
	
	public static Preferences getInstance() {
		if(instance == null) {
			instance = new Preferences();
		}
		return instance;
	}
	
	private static DBKeyValueProviderProxy getProxy(){
		return DBPreferencesProvider.getProxy();
	}
	
	public static void clear(){
		getProxy().clear(PREFERENCE_TYPE_NORMAL);
	}
	
	public void setNormaServer(boolean value) {
		getProxy().putBoolean(TAG_NORMAL_SERVER, value, PREFERENCE_TYPE_NORMAL);
	}
	
	public boolean isNormalServer() {
		return getProxy().getBoolean(TAG_NORMAL_SERVER, PREFERENCE_TYPE_NORMAL, true);
	}
	
	public void setSwitchNormalServerVisibility() {
		getProxy().putBoolean(TAG_SWITCH_NORMAL_SERVER_VISIBILITY, true, PREFERENCE_TYPE_NORMAL);
	}
	
	public boolean isSwitchNormalServerVisibility() {
		return getProxy().getBoolean(TAG_SWITCH_NORMAL_SERVER_VISIBILITY, PREFERENCE_TYPE_NORMAL, false);
	}
	
	public void setHttpUrlType(int type) {
		getProxy().putInt(TAG_HTTP_URL_TYPE, type, PREFERENCE_TYPE_NORMAL);
	}
	
	public int getHttpUrlType() {
		return getProxy().getInt(TAG_HTTP_URL_TYPE, PREFERENCE_TYPE_NORMAL, HttpConnect.getHttpUrlDefaultType());
	}
	
	public void setTrafficWarning(boolean value) {
		getProxy().putBoolean(TAG_TRAFFIC_WARNING, value, PREFERENCE_TYPE_NORMAL);
	}
	
	public boolean isTrafficWarning() {
		return getProxy().getBoolean(TAG_TRAFFIC_WARNING, PREFERENCE_TYPE_NORMAL, true);
	}
	
	//============================================================================ 
	//================================== 消息提醒 ================================== 
	//============================================================================ 
	public boolean isMessagePush() {
		return getProxy().getBoolean(TAG_MESSAGE_PUSH,PREFERENCE_TYPE_NORMAL, true);
	}
	
	public void setMessagePush(boolean value) {
		getProxy().putBoolean(TAG_MESSAGE_PUSH, value, PREFERENCE_TYPE_NORMAL);
	}
	
	public boolean isMessageSystem() {
		return getProxy().getBoolean(TAG_MESSAGE_SYSTEM, PREFERENCE_TYPE_NORMAL, true);
	}
	
	public void setMessageSystem(boolean value) {
		getProxy().putBoolean(TAG_MESSAGE_SYSTEM, value, PREFERENCE_TYPE_NORMAL);
	}
	
	public boolean isMessageMyPost() {
		return getProxy().getBoolean(TAG_MESSAGE_MY_POST,PREFERENCE_TYPE_NORMAL, true);
	}
	
	public void setMessageMyPost(boolean value) {
		getProxy().putBoolean(TAG_MESSAGE_MY_POST, value, PREFERENCE_TYPE_NORMAL);
	}
	
	public boolean isMessageMyComment() {
		return getProxy().getBoolean(TAG_MESSAGE_MY_COMMENT,PREFERENCE_TYPE_NORMAL, true);
	}
	
	public void setMessageMyComment(boolean value) {
		getProxy().putBoolean(TAG_MESSAGE_MY_COMMENT, value, PREFERENCE_TYPE_NORMAL);
	}
	
	public void setVaccineTip(int value) {
		getProxy().putInt(TAG_VACCIEN + UserData.getUserId(), value, PREFERENCE_TYPE_NORMAL);
	}
	
	public int getVaccineTip() {
		return getProxy().getInt(TAG_VACCIEN + UserData.getUserId(),PREFERENCE_TYPE_NORMAL, -1);
	}
	
	public void setLastMarkCategory(int type,String value) {
		getProxy().putString(TAG_LAST_MARK_CATEGORY + type + "_" + UserData.getUserId(), value, PREFERENCE_TYPE_NORMAL);
	}
	
	public String getLastMarkCategory(int type) {
		return getProxy().getString(TAG_LAST_MARK_CATEGORY + type + "_" + UserData.getUserId(), PREFERENCE_TYPE_NORMAL, null);
	}
	
	public void setShowHelpRecord(boolean value) {
		getProxy().putBoolean(TAG_RECORD_SHOW_HELP + UserData.getUserId(), value, PREFERENCE_TYPE_NORMAL);
	}
	
	public boolean isShowHelpRecord() {
		return getProxy().getBoolean(TAG_RECORD_SHOW_HELP + UserData.getUserId(), PREFERENCE_TYPE_NORMAL, false);
	}
	
	public void setPlayMode(int value) {
		getProxy().putInt(TAG_PLAY_MODE + UserData.getUserId(), value, PREFERENCE_TYPE_NORMAL);
	}
	
	public String getShowUpdateAgain() {
		long saveMil = getProxy().getLong(TAG_SHOW_UPDATE_TIME,PREFERENCE_TYPE_NORMAL, 0);
		if (saveMil > 0) {
			long curMil = System.currentTimeMillis();
			long durationMil = curMil - saveMil;
			float durationDay = (float) durationMil / (1000 * 60 * 60 * 24);
			if (durationDay > 7 || durationDay < 0) {
				return "";
			}
		}
		return getProxy().getString(TAG_SHOW_UPDATE_AGAIN, PREFERENCE_TYPE_NORMAL, "");
	}

	public void setShowUpdateAgain(String value, boolean isTimeRecord) {
		if (isTimeRecord) {
			long curMil = System.currentTimeMillis();
			getProxy().putLong(TAG_SHOW_UPDATE_TIME, curMil,PREFERENCE_TYPE_NORMAL);
		} else {
			getProxy().putLong(TAG_SHOW_UPDATE_TIME, 0,PREFERENCE_TYPE_NORMAL);
		}
		getProxy().putString(TAG_SHOW_UPDATE_AGAIN,value,PREFERENCE_TYPE_NORMAL);
	}
	
	public ClientInfo getUpdateInfo(){
		ClientInfo info = null;
		String data = getProxy().getString(TAG_UPDATE_INFO + "_" + AppUtil.getApkVersionCode(App.getInstance()), PREFERENCE_TYPE_NORMAL, null);
		if(data != null){
			try {
				info = new Gson().fromJson(data, ClientInfo.class);
			} catch (Exception e) {}
		}
		return info;
	}
	
	public void setUpdateInfo(ClientInfo info) {
		String data = null;
		if(info != null){
			try {
				data = new Gson().toJson(info);
			} catch (Exception e) {}
		}
		getProxy().putString(TAG_UPDATE_INFO + "_" + AppUtil.getApkVersionCode(App.getInstance()), data, PREFERENCE_TYPE_NORMAL);
	}
	
	//XXX↓///////////////////////////////////////不可清除的/////////////////////////////////////////////////////////////↓
	public boolean isSyncOldWork(){
		return getProxy().getBoolean(TAG_MAIN_SYNC_OLD_WORK + UserData.getUserId(),PREFERENCE_TYPE_PERMANENT, false);
	}
	
	public void setSyncOldWork(boolean value) {
		getProxy().putBoolean(TAG_MAIN_SYNC_OLD_WORK + UserData.getUserId(), value, PREFERENCE_TYPE_PERMANENT);
	}
	
	public void setAppShareUrl(String value) {
		getProxy().putString(TAG_APP_SHARE_URL, value, PREFERENCE_TYPE_PERMANENT);
	}
	
	public String getAppShareUrl() {
		return getProxy().getString(TAG_APP_SHARE_URL, PREFERENCE_TYPE_PERMANENT,"");
	}
	
	public void setAppShareUrlTime(long value) {
		getProxy().putLong(TAG_APP_SHARE_URL_TIME, value, PREFERENCE_TYPE_PERMANENT);
	}
	
	public long getAppShareUrlTime() {
		return getProxy().getLong(TAG_APP_SHARE_URL_TIME, PREFERENCE_TYPE_PERMANENT, 0);
	}
	
	private String packageKey(String userId,String babyId,String type,String month){
		if(month == null){
			month = "";
		}
		return "_" + userId + "_" + babyId + "_" + type + "_" + month;
	}
	
	public void setRecordSyncTime(String userId,String babyId,String type,String month,long value) {
		getProxy().putLong(TAG_RECORD_SYNC_TIME + packageKey(userId, babyId, type, month), value, PREFERENCE_TYPE_PERMANENT);
	}
	
	public long getRecordSyncTime(String userId,String babyId,String type,String month) {
		return getProxy().getLong(TAG_RECORD_SYNC_TIME + packageKey(userId, babyId, type, month),PREFERENCE_TYPE_PERMANENT,0);
	}
	
	public void setRecordPageTime(String userId,String babyId,String type,String month,long value) {
		getProxy().putLong(TAG_RECORD_PAGE_TIME + packageKey(userId, babyId, type, month), value, PREFERENCE_TYPE_PERMANENT);
	}
	
	public long getRecordPageTime(String userId,String babyId,String type,String month){
		return getProxy().getLong(TAG_RECORD_PAGE_TIME + packageKey(userId, babyId, type, month),PREFERENCE_TYPE_PERMANENT,0);
	}
	
	public void setRecordMinPageTime(String userId,String babyId,String type,String month,long value) {
		long time = getRecordPageTime(userId, babyId, type, month);
		if(time != 0 && time < value){
			setRecordPageTime(userId, babyId, type, month, value);
		}
		getProxy().putLong(TAG_RECORD_MIN_PAGE_TIME + packageKey(userId, babyId, type, month), value, PREFERENCE_TYPE_PERMANENT);
	}
	
	public long getRecordMinPageTime(String userId,String babyId,String type,String month){
		return getProxy().getLong(TAG_RECORD_MIN_PAGE_TIME + packageKey(userId, babyId, type, month),PREFERENCE_TYPE_PERMANENT,-1);
	}
	
	public String getRecordPageTimeStr(String userId,String babyId,String type,String month){
		long time = getRecordPageTime(userId, babyId, type, month);
		return time == 0 ? "" : String.valueOf(time);
	}
	
	public void setLastPostsUnreadTime(long value) {
		getProxy().putLong(TAG_LAST_POSTS_UNREAD_TIME + UserData.getUserId(), value, PREFERENCE_TYPE_PERMANENT);
	}
	
	public long getLastPostsUnreadTime() {
		return getProxy().getLong(TAG_LAST_POSTS_UNREAD_TIME + UserData.getUserId(),PREFERENCE_TYPE_PERMANENT,0);
	}
	
	public void setHadClickGoToMarket() {
		getProxy().putBoolean(TAG_HAD_CLICK_GO_TO_MARKET, true, PREFERENCE_TYPE_PERMANENT);
	}
	
	public boolean isHadClickGoToMarket() {
		boolean result = getProxy().getBoolean(TAG_HAD_CLICK_GO_TO_MARKET, PREFERENCE_TYPE_PERMANENT, false);
		if(!result) {
			long time = getProxy().getLong(TAG_CLICK_GO_TO_MARKET_IGNORE_DATE, PREFERENCE_TYPE_PERMANENT, 0);
			int date = (int) ((System.currentTimeMillis() - time) / (3600 * 1000 * 24));
			if(date > 2) {
				result = false;
				setClickGoToMarketIgnoreDate();
			} else {
				result = true;
			}
		}
		return result;
	}
	
	public void setClickGoToMarketIgnoreDate() {
		getProxy().putLong(TAG_CLICK_GO_TO_MARKET_IGNORE_DATE, System.currentTimeMillis(), PREFERENCE_TYPE_PERMANENT);
	}
	
	public void initClickGoToMarketIgnoreDate() {
		long time = getProxy().getLong(TAG_CLICK_GO_TO_MARKET_IGNORE_DATE, PREFERENCE_TYPE_PERMANENT, 0);
		if(time == 0) {
			setClickGoToMarketIgnoreDate();
		}
	}
	
	public void setClickGoToMarketIgnoreCount() {
		int count = getClickGoToMarketIgnoreCount();
		count++;
		getProxy().putInt(TAG_CLICK_GO_TO_MARKET_IGNORE, count, PREFERENCE_TYPE_PERMANENT);
		if(count > 1) {
			setHadClickGoToMarket();
		}
	}
	
	public int getClickGoToMarketIgnoreCount() {
		return getProxy().getInt(TAG_CLICK_GO_TO_MARKET_IGNORE, PREFERENCE_TYPE_PERMANENT, 0);
	}
	
	public void setFamilyVisitDate() {
		Calendar todayStart = Calendar.getInstance();  
        todayStart.set(Calendar.HOUR_OF_DAY, 0);  
        todayStart.set(Calendar.MINUTE, 0);  
        todayStart.set(Calendar.SECOND, 0);  
        todayStart.set(Calendar.MILLISECOND, 0);  
        long currentDayTime = todayStart.getTime().getTime(); 
		getProxy().putLong(TAG_FAMILY_VISIT_RECORD_DATE + UserData.getUserId(), currentDayTime, PREFERENCE_TYPE_PERMANENT);
	}
	
	public long getFamilyVisitDate() {
		return getProxy().getLong(TAG_FAMILY_VISIT_RECORD_DATE + UserData.getUserId(), PREFERENCE_TYPE_PERMANENT, 0);
	}
	
	public boolean isTodayVisitFamily() {
		long time = getFamilyVisitDate();
		if(time == 0) {
			return false;
		}
		Calendar todayStart = Calendar.getInstance();  
        todayStart.set(Calendar.HOUR_OF_DAY, 0);  
        todayStart.set(Calendar.MINUTE, 0);  
        todayStart.set(Calendar.SECOND, 0);  
        todayStart.set(Calendar.MILLISECOND, 0);  
        long currentDayTime = todayStart.getTime().getTime(); 
        return time == currentDayTime;
	}
	
	public void setFamilyVisitOpenApp(boolean value) {
		getProxy().putBoolean(TAG_FAMILY_VISIT_RECORD_OPEN_APP + UserData.getUserId(), value, PREFERENCE_TYPE_FAMILY_VISIT);
	}
	
	public boolean isFamilyVisitOpenApp() {
		return getProxy().getBoolean(TAG_FAMILY_VISIT_RECORD_OPEN_APP + UserData.getUserId(), PREFERENCE_TYPE_FAMILY_VISIT, true);
	}
	
	public void resetFamilyVisitOpenApp() {
		getProxy().clear(PREFERENCE_TYPE_FAMILY_VISIT);
	}

	public void setWeiboAccessToken(Oauth2AccessToken token){
		if(token != null){
			String data = new Gson().toJson(token);
			getProxy().putString(TAG_WEIBO_ACCESS_TOKEN, data, PREFERENCE_TYPE_PERMANENT);
		}
	}
	
	public Oauth2AccessToken getWeiboAccessToken(){
		String data = getProxy().getString(TAG_WEIBO_ACCESS_TOKEN, PREFERENCE_TYPE_PERMANENT, null);
		if(data != null){
			try {
				return new Gson().fromJson(data, Oauth2AccessToken.class);
			} catch (Exception e) {}
		}
		return null;
	}
	
	public void setNewPhotoTipTime(long time){
		getProxy().putLong(TAG_NEW_PHOTO_TIP, time, PREFERENCE_TYPE_PERMANENT);
	}
	
	public long getNewPhotoTipTime(){
		return getProxy().getLong(TAG_NEW_PHOTO_TIP,PREFERENCE_TYPE_PERMANENT,0);
	}
	
	public boolean isShowGuideBabyList(){
		return getProxy().getBoolean(TAG_GUIDE_BABY_LIST, PREFERENCE_TYPE_PERMANENT, true);
	}
	
	public void setShowGuideBabyList(){
		getProxy().putBoolean(TAG_GUIDE_BABY_LIST, false, PREFERENCE_TYPE_PERMANENT);
	}
	
	public boolean isShowGuideWelcome(){
		return getProxy().getBoolean(TAG_GUIDE_WELCOME, PREFERENCE_TYPE_PERMANENT, true);
	}
	
	public void setShowGuideWelcome(){
		getProxy().putBoolean(TAG_GUIDE_WELCOME, false, PREFERENCE_TYPE_PERMANENT);
	}
	
	public boolean isShowGuideFirstImportPhoto(){
		return getProxy().getBoolean(TAG_GUIDE_FIRST_IMPORT_PHOTO, PREFERENCE_TYPE_PERMANENT, true);
	}
	
	public void setShowGuideFirstImportPhoto(){
		getProxy().putBoolean(TAG_GUIDE_FIRST_IMPORT_PHOTO, false, PREFERENCE_TYPE_PERMANENT);
	}
	
	public boolean isShowGuideMagazineBatch(){
		return getProxy().getBoolean(TAG_GUIDE_MAGAZINE_BATCH, PREFERENCE_TYPE_PERMANENT, true);
	}
	
	public void setShowGuideMagazineBatch(){
		getProxy().putBoolean(TAG_GUIDE_MAGAZINE_BATCH, false, PREFERENCE_TYPE_PERMANENT);
	}
	
	public boolean isShowGuideMagazineEdit(){
		return getProxy().getBoolean(TAG_GUIDE_MAGAZINE_EDIT, PREFERENCE_TYPE_PERMANENT, true);
	}
	
	public void setShowGuideMagazineEdit(){
		getProxy().putBoolean(TAG_GUIDE_MAGAZINE_EDIT, false, PREFERENCE_TYPE_PERMANENT);
	}
	
	public long getLastActionMessageTime(String key){
		return getProxy().getLong(TAG_ACTION_MESSAGE_LAST_TIME + "_" + key, PREFERENCE_TYPE_PERMANENT, 0);
	}
	
	public void setLastActionMessageTime(String key,long time){
		getProxy().putLong(TAG_ACTION_MESSAGE_LAST_TIME + "_" + key, time, PREFERENCE_TYPE_PERMANENT);
	}
	
	public void setRecordShareQzone(boolean value){
		getProxy().putBoolean(TAG_RECORD_SHARE_QZONE, value, PREFERENCE_TYPE_PERMANENT);
	}
	
	public boolean isRecordShareQzone(){
		return getProxy().getBoolean(TAG_RECORD_SHARE_QZONE, PREFERENCE_TYPE_PERMANENT, false);
	}
	
	public void setRecordShareFriend(boolean value){
		getProxy().putBoolean(TAG_RECORD_SHARE_FRIEND, value, PREFERENCE_TYPE_PERMANENT);
	}
	
	public boolean isRecordShareFriend(){
		return getProxy().getBoolean(TAG_RECORD_SHARE_FRIEND, PREFERENCE_TYPE_PERMANENT, WXAPIManager.getInstance(App.getInstance()).isWXAppInstalled());
	}
	
	public void setRecordShareWeibo(boolean value){
		getProxy().putBoolean(TAG_RECORD_SHARE_WEIBO, value, PREFERENCE_TYPE_PERMANENT);
	}
	
	public boolean isRecordShareWeibo(){
		return getProxy().getBoolean(TAG_RECORD_SHARE_WEIBO, PREFERENCE_TYPE_PERMANENT, false);
	}
	
	public void setCheckRelogin(String userId,String value){
		getProxy().putString(TAG_CHECK_RELOGIN_PRE + userId, value, PREFERENCE_TYPE_PERMANENT);
	}
	
	public String getCheckRelogin(String userId){
		return getProxy().getString(TAG_CHECK_RELOGIN_PRE + userId, PREFERENCE_TYPE_PERMANENT, null);
	}
	
	public String getLastPostContentItem(String postId,String type){
		return getProxy().getString(TAG_LAST_POST_CONTENT_ITEM + UserData.getUserId() + "_" + postId + "_" + type, PREFERENCE_TYPE_PERMANENT,null);
	}
	
	public void setLastPostContentItem(String postId,String type,String pcId){
		getProxy().putString(TAG_LAST_POST_CONTENT_ITEM + UserData.getUserId() + "_" + postId + "_" + type, pcId, PREFERENCE_TYPE_PERMANENT);
	}
	
	public boolean isNewUser(){
		return !getProxy().hasData(TAG_NEW_USER, PREFERENCE_TYPE_PERMANENT);
	}
	
	public void setNewUser(boolean value){
		getProxy().putBoolean(TAG_NEW_USER, value, PREFERENCE_TYPE_PERMANENT);
	}

	public boolean isShowQRCodeScanTip(){
		return getProxy().getBoolean(TAG_QR_CODE_SCAN_TIP, PREFERENCE_TYPE_PERMANENT, true);
	}

	public void setShowQRCodeScanTip(){
		getProxy().putBoolean(TAG_QR_CODE_SCAN_TIP, false, PREFERENCE_TYPE_PERMANENT);
	}

	public boolean isShowOpenWXTip(){
		return getProxy().getBoolean(TAG_OPEN_WX_TIP, PREFERENCE_TYPE_PERMANENT, true);
	}

	public void setShowOpenWXTip(){
		getProxy().putBoolean(TAG_OPEN_WX_TIP, false, PREFERENCE_TYPE_PERMANENT);
	}

	public boolean isShowPrintEditImgTip(){
		return getProxy().getBoolean(TAG_PRINT_EDIT_IMG_TIP, PREFERENCE_TYPE_PERMANENT, false);
	}

	public void setShowPrintEditImgTip(){
		getProxy().putBoolean(TAG_PRINT_EDIT_IMG_TIP, true, PREFERENCE_TYPE_PERMANENT);
	}
	
	public int getOpenAppDays(){
		return getProxy().getInt(TAG_OPEN_APP_DAYS, PREFERENCE_TYPE_PERMANENT, 0);
	}

	public void setOpenAppDays(int days){
		getProxy().putInt(TAG_OPEN_APP_DAYS, days, PREFERENCE_TYPE_PERMANENT);
	}
	
	public void setLastShowAdId(String id){
		getProxy().putString(TAG_SPLASH_LAST_SHOW_AD_ID, id, PREFERENCE_TYPE_PERMANENT);
	}
	
	public String getLastShowAdId(){
		return getProxy().getString(TAG_SPLASH_LAST_SHOW_AD_ID, PREFERENCE_TYPE_PERMANENT, null);
	}

	public long getGuardActivityLaunchLastTime(){
		return getProxy().getLong(TAG_GUARD_ACTIVITY_LAUNCH_LAST_TIME, PREFERENCE_TYPE_PERMANENT, 0);
	}

	public void setGuardActivityLaunchLastTime(long time){
		getProxy().putLong(TAG_GUARD_ACTIVITY_LAUNCH_LAST_TIME, time, PREFERENCE_TYPE_PERMANENT);
	}

	/** 获取新用户引导提示状态；
	 *
	 * @return
	 */
	public int getNewUserGuideRecordStatus(){
		return getProxy().getInt(TAG_NEW_USER_GUIDE_RECORD, PREFERENCE_TYPE_PERMANENT, 0);
	}

	/** 设置新用户引导提示状态；
	 *
	 * @param status 0为默认 -》1为新用户 -》 2为显示提示 -》 3为不需要提醒
	 */
	public void setNewUserGuideRecordStatus(int status){
		getProxy().putInt(TAG_NEW_USER_GUIDE_RECORD, status, PREFERENCE_TYPE_PERMANENT);
	}

	public boolean isShowMagazineMakePreViewTipp(){
		return getProxy().getBoolean(TAG_MAGAZINE_MAKE_GUIDE_PREVIEW, PREFERENCE_TYPE_PERMANENT, true);
	}

	public void setMagazineMakePreViewTip(){
		getProxy().putBoolean(TAG_MAGAZINE_MAKE_GUIDE_PREVIEW, false, PREFERENCE_TYPE_PERMANENT);
	}
}
