package com.bk.android.time.ui;

import android.webkit.WebViewClient;

public interface IWebView {
	public abstract void loadUrl(String url);
	public void release();
	public void setWebViewClient(WebViewClient client);
}
