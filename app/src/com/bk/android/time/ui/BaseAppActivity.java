package com.bk.android.time.ui;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewTreeObserver.OnPreDrawListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.TextView;

import com.bk.android.assistant.R;
import com.bk.android.time.anim.ViewAnimDecorator;
import com.bk.android.time.app.AbsAppActivity;
import com.bk.android.time.app.AppBroadcast;
import com.bk.android.time.model.BaseDialogViewModel;
import com.bk.android.time.ui.BaseAppActivity.ForwardPopwin.ForwardViewControl;
import com.bk.android.time.ui.ITitleBar.OnTitleListener;
import com.bk.android.time.ui.activiy.LoginActivity;
import com.bk.android.time.ui.widget.MainRootLayout;
import com.bk.android.time.util.DialogUtil;
import com.bk.android.time.util.ToastUtil;
import com.bk.android.util.AppUtil;

import java.lang.ref.WeakReference;
import java.lang.reflect.Method;
import java.util.ArrayList;

public abstract class BaseAppActivity extends AbsAppActivity implements INetLoadView,ITitleBar,OnTitleListener{
	protected static final int REQUEST_CODE_LOGIN = 17911;
	private TextView mLoadingTv;
	private View mNetRetryLay;
	private TextView mNetRetryTipTv;
	private Button mNetRetryBut;
	private ViewGroup mLoadingView;
	private BaseDialogViewModel mDialogViewModel;
	private TextView mTitleTv;
	private ImageView mLeftButIv;
	private TextView mLeftButTv;
	private ImageView mRightButIv;
	private TextView mRightButTv;
	private ViewGroup mTitleLay;
	private boolean isTransparentLoadingView;
	private ImageView mCloseView;
	private Runnable mLoginRetryTask;
	private ArrayList<WeakReference<OnTitleListener>> mOnTitleListeners = new ArrayList<WeakReference<OnTitleListener>>();
	private MainRootLayout mRootView;
	private Runnable mFailRetryTask;
	private InputMethodManager mInputMethodManager;
	private ForwardPopwin mForwardPopwin;
	private long mExitTime;
	private Runnable mForwardViewRunnable;
	
	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		mRootView = (MainRootLayout) getLayoutInflater().inflate(R.layout.com_activity_lay, null);
		getWindow().getDecorView().setBackgroundDrawable(mRootView.getWindowBGDrawable());
		super.setContentView(mRootView);
		mRootView.setInitFinish();
		mRootView.setTouchBackEnabled(!isMainActivity());
		mOnTitleListeners = new ArrayList<WeakReference<OnTitleListener>>();
		mTitleLay = (ViewGroup) findViewById(R.id.base_head_title_lay);
		mTitleTv = (TextView)findViewById(R.id.base_head_title_tv);
		mLeftButIv = (ImageView)findViewById(R.id.base_head_left_but_iv);
		mLeftButTv = (TextView)findViewById(R.id.base_head_left_but_tv);
		mRightButIv = (ImageView)findViewById(R.id.base_head_right_but_iv);
		mRightButTv = (TextView)findViewById(R.id.base_head_right_but_tv);
		findViewById(R.id.base_head_left_lay).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dispatchOnTitleButtonClick(true);
			}
		});
		
		mRightButTv.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dispatchOnTitleButtonClick(false);
			}
		});
		
		mTitleTv.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dispatchOnTitleClick();
			}
		});
		addTitleListener(this);
		resetTitleBar();
		
		mCloseView = (ImageView)findViewById(R.id.net_retry_tip_iv);
		mCloseView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				hideLoadView();
			}
		});
		mLoadingView = (ViewGroup) findViewById(R.id.loading_lay);
		mLoadingTv = (TextView)findViewById(R.id.loading_tv);
		mNetRetryLay = findViewById(R.id.net_retry_lay);
		mNetRetryTipTv = (TextView)findViewById(R.id.net_retry_tip_tv);
		mNetRetryBut = (Button)findViewById(R.id.net_retry_but);
		
		if(hasSmartBar()){
			setTheme(R.style.SmartBarTheme);
            getWindow().setUiOptions(ActivityInfo.UIOPTION_SPLIT_ACTION_BAR_WHEN_NARROW);
			if(isActionBarEnabled()){
				ActionBar actionBar = getActionBar();
				if(actionBar != null){
					ViewGroup viewParent = (ViewGroup) mTitleLay.getParent();
					viewParent.removeView(mTitleLay);
					actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
					actionBar.setCustomView(mTitleLay, new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT));
				}
			}else{
				LayoutParams lp = mTitleLay.getLayoutParams();
				if(lp != null){
					lp.height = getResources().getDimensionPixelSize(R.dimen.smart_bar_height_max);
				}
				mTitleLay.setLayoutParams(lp);
			}
		}
		mInputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
	}

	public void setRootViewBackground(int alpha, int color) {
		mRootView.setBGColor(color);
		mRootView.setBGAlpha(alpha);
		mRootView.invalidateBG();
	}
	
	public void setRootViewShow(boolean isShow){
		mRootView.setVisibility(isShow ? View.VISIBLE : View.GONE);
	}
	
	public void setTouchBackEnabled(boolean b) {
		mRootView.setTouchBackEnabled(b);
	}
	
	@Override
	protected int getStatusBarTintResource() {
		return R.color.com_color_1;
	}
	
	@Override
	protected boolean isActionBarEnabled(){
		return hasSmartBar();
	}
	
	@Override
	protected boolean isSetTranslucentStatus() {
//		return super.isSetTranslucentStatus() && !isMeiZu();
		return false;
	}
	
	private boolean isMeiZu(){
		if(Build.VERSION.SDK_INT < Build.VERSION_CODES.ICE_CREAM_SANDWICH){
	        return false;
		}
        try {
            // 新型号可用反射调用Build.hasSmartBar()
            Method method = Class.forName("android.os.Build").getMethod("hasSmartBar");
            return ((Boolean) method.invoke(null)).booleanValue();
        } catch (Exception e) {}
        // 反射不到Build.hasSmartBar()，则用Build.DEVICE判断
        if (Build.DEVICE.equals("mx2")) {
            return true;
        } else if (Build.DEVICE.equals("mx") || Build.DEVICE.equals("m9")) {
            return false;
        }
        return false;
	}
	
	protected boolean hasSmartBar() {
        return false;
    }
	
	private void dispatchOnTitleButtonClick(boolean isLeft){
		boolean isHandle = false;
		ArrayList<WeakReference<OnTitleListener>> temp = new ArrayList<WeakReference<OnTitleListener>>(mOnTitleListeners);
		for (int i = temp.size() - 1; i >= 0; i--) {
			WeakReference<OnTitleListener> l = temp.get(i);
			if(l.get() != null){
				if(!isHandle){
					isHandle = l.get().onTitleButtonClick(isLeft);
				}
			}else{
				mOnTitleListeners.remove(l);
			}
		}
	}
	
	private void dispatchOnTitleClick() {
		boolean isHandle = false;
		ArrayList<WeakReference<OnTitleListener>> temp = new ArrayList<WeakReference<OnTitleListener>>(mOnTitleListeners);
		for (int i = temp.size() - 1; i >= 0; i--) {
			WeakReference<OnTitleListener> l = temp.get(i);
			if(l.get() != null){
				if(!isHandle){
					isHandle = l.get().onTitleClick();
				}
			}else{
				mOnTitleListeners.remove(l);
			}
		}
	}
	
	@Override
	public void finish() {
		forceCloseForwardView();
		super.finish();
	}
	
	@Override
	public boolean isFinishing() {
		return super.isFinishing() || (mRootView != null && mRootView.isTouchBackEnabled() && mRootView.isMenuOpen());
	}
	
	public void closeForwardView(){
		if(isFinishing()){
			return;
		}
		if(mForwardPopwin != null && mForwardPopwin.isShowing()){
			mForwardPopwin.dismiss();
		}
	}
	
	private void forceCloseForwardView(){
		if(isFinishing()){
			return;
		}
		if(mForwardPopwin != null && mForwardPopwin.isShowing()){
			mForwardPopwin.forceDismiss();
		}
	}
	
	public void setForwardView(View view){
		setForwardView(view,true,null);
	}
	
	public void setForwardView(View view,boolean canCancel){
		setForwardView(view,canCancel,null);
	}
	
	public void setForwardView(final View view,final boolean canCancel,final ForwardViewControl c){
		if(isFinishing()){
			return;
		}
		final Runnable runnable = new Runnable() {
			@Override
			public void run() {
				if(!this.equals(mForwardViewRunnable)){
					return;
				}
				forceCloseForwardView();
				if(view != null){
					FrameLayout contentView = new FrameLayout(this_){
						private boolean isInit;
						@Override
						public boolean dispatchTouchEvent(MotionEvent ev) {
							try {
								return super.dispatchTouchEvent(ev);
							} catch (Exception e) {}
							return true;
						}
						
						@Override
						protected void dispatchDraw(Canvas canvas) {
							super.dispatchDraw(canvas);
							if(isInit){
								return;
							}
							isInit = true;
							postInvalidateDelayed(500);
						}
					};
					mForwardPopwin = new ForwardPopwin(contentView,LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT);
					mForwardPopwin.setForwardViewControl(c);
					mForwardPopwin.setAnimationStyle(R.style.DropDownPopWin);
					if(canCancel){
						mForwardPopwin.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
					}
					mForwardPopwin.setTouchable(true);
					mForwardPopwin.setOnDismissListener(new OnDismissListener() {
						@Override
						public void onDismiss() {
							if(mForwardPopwin != null){
								mForwardPopwin.setFocusable(false);
								mForwardPopwin.setForwardViewControl(null);
								mForwardPopwin.setOnDismissListener(null);
								mForwardPopwin = null;
							}
						}
					});
					contentView.addView(view,LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT);
					showForwardView();
				}
			}
		};
		mForwardViewRunnable = runnable;
		if(!isNeedPostRunnable()){
			mRootView.post(runnable);
		}else{
			mRootView.getViewTreeObserver().addOnPreDrawListener(new OnPreDrawListener() {
				@Override
				public boolean onPreDraw() {
					if(!isFinishing() && runnable.equals(mForwardViewRunnable)){
						if(isNeedPostRunnable()){
							mRootView.invalidate();
						}else{
							mRootView.getViewTreeObserver().removeOnPreDrawListener(this);
							mRootView.post(runnable);
						}
					}else{
						mRootView.getViewTreeObserver().removeOnPreDrawListener(this);
					}
					return true;
				}
			});
			mRootView.invalidate();
		}
	}
	
	private boolean isNeedPostRunnable(){
		if(mRootView.getWindowToken() == null){
			return true;
		}
		if(mRootView.isLayoutRequested() && (mRootView.getWidth() == 0 || mRootView.getHeight() == 0)){
			return true;
		}
		boolean isFullScreen = (getWindow().getAttributes().flags & WindowManager.LayoutParams.FLAG_FULLSCREEN) == WindowManager.LayoutParams.FLAG_FULLSCREEN;
		int[] location = new int[2];
		mRootView.getLocationInWindow(location);
		return isFullScreen ? location[1] != 0 : location[1] <= 0;
	}
	
	public void showForwardView(){
		if(isFinishing()){
			return;
		}
		if(mForwardPopwin != null && !mForwardPopwin.isShowing()){
			mForwardPopwin.setFocusable(true);
			int[] location = new int[2];
			mRootView.getLocationInWindow(location);
			mForwardPopwin.setWidth(mRootView.getWidth());
			mForwardPopwin.setHeight(mRootView.getHeight());
			int gravity = Gravity.LEFT | Gravity.TOP;
			mForwardPopwin.showAtLocation(getWindow().getDecorView(), gravity ,location[0],location[1]);
		}
	}
	
	@Override
	public void setContentView(int layoutResID) {
		View view = getLayoutInflater().inflate(layoutResID, null);
		setContentView(view,view.getLayoutParams());
	}
	
	@Override
	public void setContentView(View view) {
		setContentView(view,view.getLayoutParams());
	}

	protected int getContentLayId(){
		return mRootView.getId();
	}
	
	@Override
	public void onNetworkChange(boolean isAvailable) {
		super.onNetworkChange(isAvailable);
		if(isAvailable){
			hideNetSettingView();
		}
	}

	@Override
	public void setContentView(View view,LayoutParams params) {
		removeContentView();
		addContentView(view, params);
	}

	@Override
	public void addContentView(View view,LayoutParams params) {
		view.setTag(R.id.base_activity_content, mRootView.hashCode());
		if(params != null){
			mRootView.addView(view,params);
		}else{
			mRootView.addView(view);
		}
    }
	
	private void removeContentView(){
		int size  = mRootView.getChildCount();
		ArrayList<View> findViews = new ArrayList<View>();
		for (int i = 0;i < size;i++) {
			View viewOld = mRootView.getChildAt(i);
			if(Integer.valueOf(mRootView.hashCode()).equals(viewOld.getTag(R.id.base_activity_content))){
				findViews.add(viewOld);
			}
		}
		for (View view : findViews) {
			mRootView.removeView(view);
		}
	}
	
	protected int getDefaultLoadingViewBGColor(){
		return R.color.com_color_5;
	}
	
	private boolean isTransparentLoadingView() {
		return isTransparentLoadingView;
	}
	
	@Override
	public void setLoadingViewTransparent(boolean isTransparent){
		isTransparentLoadingView = isTransparent;
	}
	
	protected boolean isViewAnimEnabled() {
		return true;
	}
	
	@SuppressWarnings("deprecation")
	private void setViewBGColor(View view,boolean isLoadingView){
		if(isTransparentLoadingView()){
			if(isLoadingView){
				view.setBackgroundColor(getResources().getColor(getDefaultLoadingViewBGColor()) + 0x99000000);
				view.setClickable(true);
			}else{
				view.setBackgroundDrawable(null);
				view.setClickable(false);
			}
		}else{
			view.setBackgroundColor(getResources().getColor(getDefaultLoadingViewBGColor()));
			view.setClickable(true);
		}
	}
	
	protected String getLoadingTip(){
		return getString(R.string.tip_loading);
	}
	
	private void cancelFailRetryTask(){
		if(mFailRetryTask instanceof LoginCallback){
			((LoginCallback) mFailRetryTask).cancel();
		}
		mFailRetryTask = null;
	}
	
	protected void onHideLoadAndRetryView() {
		onHideLoadAndRetryView(true);
	}
	
	protected void onHideLoadAndRetryView(boolean isCancelFailRetryTask) {
		if(isCancelFailRetryTask){
			cancelFailRetryTask();
		}
		mNetRetryBut.setOnClickListener(null);
		if (isFinishing()) {
			return;
		}
		hideNetSettingDialog();
		ViewAnimDecorator.hideView(mLoadingView, isViewAnimEnabled());
		ViewAnimDecorator.hideView(mNetRetryLay, isViewAnimEnabled());
	}
	
	private void showAllChild(ViewGroup view){
		for (int i = 0; i < view.getChildCount(); i++) {
			view.getChildAt(i).setVisibility(View.VISIBLE);
		}
	}
	
	private void hideAllChild(ViewGroup view){
		for (int i = 0; i < view.getChildCount(); i++) {
			view.getChildAt(i).setVisibility(View.INVISIBLE);
		}
	}
	
	@Override
	public void showLoadView() {
		cancelFailRetryTask();
		
		mLoadingTv.setText(getLoadingTip());
		mNetRetryLay.setVisibility(View.GONE);
		setViewBGColor(mLoadingView, true);
		showAllChild(mLoadingView);
		ViewAnimDecorator.showView(mLoadingView, false);
	}

	@Override
	public void updateProgress(int progress, int maxProgress, int msgFormat) {
		if(mLoadingTv.isShown()){
			int per = (int) (progress * 1f / maxProgress * 100);
			String str = per + "%";
			if(msgFormat > 0){
				mLoadingTv.setText(getString(msgFormat, str));
			}else{
				mLoadingTv.setText(getLoadingTip() + str);
			}
		}
	}

	@Override
	public void hideLoadView() {
		onHideLoadAndRetryView();
	}

	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK && event.isTracking() && !event.isCanceled()) {
			if(mInputMethodManager.isActive()){
				mInputMethodManager.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(),0); //强制隐藏键盘 
			}
		}
		return super.onKeyUp(keyCode, event);
	}
	
	@Override
	public boolean onTitleButtonClick(boolean isLeft) {
		if(isLeft && Integer.valueOf(R.drawable.btn_back).equals(mLeftButIv.getTag())){
			onBackPressed();
			return true;
		}
		return false;
	}
	
	@Override
	public boolean onTitleClick() {
		setAdapterViewGoToTop(mRootView);
		return false;
	}

	public void setAdapterViewGoToTop(View view){
		if(!view.isShown()){
			return;
		}
		if(view instanceof AdapterView){
			((AdapterView<?>) view).setSelection(0);
		}else if(view instanceof ViewGroup){
			ViewGroup viewGroup = (ViewGroup) view;
			for (int i = 0; i < viewGroup.getChildCount(); i++) {
				setAdapterViewGoToTop(viewGroup.getChildAt(i));
			}
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode == REQUEST_CODE_LOGIN){
			if(mLoginRetryTask != null){
				if(resultCode == RESULT_OK){
					mLoginRetryTask.run();
				}
				if(mLoginRetryTask instanceof LoginCallback){
					((LoginCallback) mLoginRetryTask).cancel();
				}
				mLoginRetryTask = null;
			}
		}
	}

	@Override
	public void showLoginView(Runnable retryTask) {
		if(mLoginRetryTask == null){
			startActivityForResult(LoginActivity.getIntent(this, LoginActivity.ACTION_TYPE_ONLY_QUICK), REQUEST_CODE_LOGIN);
		}
		mLoginRetryTask = retryTask;
	}

	@Override
	public void showNetSettingView(boolean canClean) {
		cancelFailRetryTask();
		mNetRetryLay.setVisibility(View.VISIBLE);
		mNetRetryTipTv.setText(R.string.tip_on_net);
		mNetRetryBut.setText(R.string.btn_text_setting);
		if(canClean){
			mCloseView.setImageResource(R.drawable.load_and_retry_close_icon);
		}else{
			mCloseView.setImageResource(R.drawable.loading_tip_icon);
		}
		mCloseView.setClickable(canClean);
		mNetRetryBut.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				AppUtil.gotoSettingActivity(this_);
			}
		});
		hideAllChild(mLoadingView);
		setViewBGColor(mLoadingView,false);
		ViewAnimDecorator.showView(mLoadingView,false);
		ViewAnimDecorator.showView(mNetRetryLay,false);
	}

	@Override
	public void showNetSettingDialog(boolean canClean) {
		if (isFinishing()) {
			return;
		}
		cancelFailRetryTask();
		if(mDialogViewModel != null && mDialogViewModel.isShowing()){
			return;
		}
		mDialogViewModel = DialogUtil.createNotNetDialog(this_);
		mDialogViewModel.setCancelable(canClean);
		mDialogViewModel.show();
	}

	@Override
	public void hideNetSettingView() {
		onHideLoadAndRetryView();
	}

	@Override
	public void hideNetSettingDialog() {
		if(mDialogViewModel != null && mDialogViewModel.isShowing()){
			mDialogViewModel.finish();
		}
	}
	
	@Override
	public void showRetryView(final Runnable retryTask,boolean canClean) {
		mNetRetryLay.setVisibility(View.VISIBLE);
		mNetRetryTipTv.setText(R.string.tip_err_server);
		mNetRetryBut.setText(R.string.btn_text_retry);
		if(canClean){
			mCloseView.setImageResource(R.drawable.load_and_retry_close_icon);
		}else{
			mCloseView.setImageResource(R.drawable.loading_tip_icon);
		}
		mCloseView.setClickable(canClean);
		mFailRetryTask = retryTask;
		mNetRetryBut.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				onHideLoadAndRetryView(false);
				if(mFailRetryTask != null){
					mFailRetryTask.run();
				}
				cancelFailRetryTask();
			}
		});
		hideAllChild(mLoadingView);
		setViewBGColor(mLoadingView,false);
		ViewAnimDecorator.showView(mLoadingView,false);
		ViewAnimDecorator.showView(mNetRetryLay,false);
	}

	@Override
	public void hideRetryView() {
		onHideLoadAndRetryView();
	}

	protected boolean isTitleBackBtnEnabled(){
		return !hasSmartBar() || !isActionBarEnabled();
	}
		
	@Override
	public void removeTitleView(View view) {
		View oldView = mTitleLay.findViewById(mTitleTv.getId());
		if(view != null && view.getParent() == oldView){
			((ViewGroup) oldView).removeView(view);
		}
	}

	public void cleartitleView() {
		mTitleLay.removeAllViews();
	}
	
	@Override
	public void setTitleView(View view) {
		if(view != null && view.getParent() == null){
			View oldView = mTitleLay.findViewById(mTitleTv.getId());
			LinearLayout layView = null;
			if(oldView instanceof LinearLayout){
				layView = (LinearLayout) oldView;
			}else{
				if(oldView != null){
					mTitleLay.removeView(oldView);
				}
				layView = new LinearLayout(this);
				layView.setGravity(Gravity.CENTER);
				layView.setId(mTitleTv.getId());
				mTitleLay.addView(layView,mTitleTv.getLayoutParams());
			}
			layView.removeAllViews();
			layView.addView(view);
		}
	}

	@Override
	public void setTitle(CharSequence title) {
		if(title != null) {
			mTitleTv.setText(title);
			if(!mTitleLay.equals(mTitleTv.getParent())){
				View oldView = mTitleLay.findViewById(mTitleTv.getId());
				if(oldView != null){
					mTitleLay.removeView(oldView);
					if(oldView instanceof ViewGroup){
						((ViewGroup) oldView).removeAllViews();
					}
				}
				mTitleLay.addView(mTitleTv,mTitleTv.getLayoutParams());
			}
		}
	}

	@Override
	public void setTitle(int titleId) {
		setTitle(getString(titleId));
	}
	
	@Override
	public void resetTitleBar() {
		if(!mTitleLay.equals(mTitleTv.getParent())){
			View oldView = mTitleLay.findViewById(mTitleTv.getId());
			if(oldView != null){
				mTitleLay.removeView(oldView);
				if(oldView instanceof ViewGroup){
					((ViewGroup) oldView).removeAllViews();
				}
			}
			mTitleLay.addView(mTitleTv,mTitleTv.getLayoutParams());
		}
		setTitleBarEnabled(true);
		mTitleTv.setText(R.string.app_name);
		setRightButtonEnabled(false);
		setRightButton("", 0, 0);
		if(isTitleBackBtnEnabled()){
			setLeftButtonEnabled(true);
			setLeftButton(null, R.drawable.btn_back, 0);
			mLeftButIv.setTag(R.drawable.btn_back);
		}else{
			setLeftButton("", 0, 0);
			setLeftButtonEnabled(false);
			mLeftButIv.setTag(null);
		}
		
		View leftView = findViewById(R.id.base_head_left_but_new_iv);
		if(leftView != null){
			leftView.setVisibility(View.INVISIBLE);
		}
		
		View rightView = findViewById(R.id.base_head_right_but_new_iv);
		if(rightView != null){
			rightView.setVisibility(View.INVISIBLE);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(item != null){
			if(item.getItemId() == mRightButTv.getId()){
				dispatchOnTitleButtonClick(false);
			}else if(item.getItemId() == mLeftButTv.getId()){
				dispatchOnTitleButtonClick(true);
			}
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public View getLeftButtonView() {
		return mLeftButTv;
	}
	
	@Override
	public View getRightButtonView() {
		return mRightButIv;
	}

	private void setButton(boolean isLeft, String tip, int icon, Drawable drawable, int bgRes) {
		if(TextUtils.isEmpty(tip)){
			tip = "";
		}
		TextView butTV = mLeftButTv;
		ImageView butIV = mLeftButIv;

		if (!isLeft) {
			butTV = mRightButTv;
			butIV = mRightButIv;
		}

		butTV.setBackgroundColor(Color.TRANSPARENT);
		butIV.setBackgroundColor(Color.TRANSPARENT);
		if(icon <= 0 && drawable == null){
			butTV.setText(tip);
			butIV.setImageBitmap(null);
		}else{
			tip = "";
			butTV.setText("");
			if (icon > 0) {
				butIV.setImageResource(icon);
			}
			if (drawable != null) {
				butIV.setImageDrawable(drawable);
			}
		}
		int w = getResources().getDimensionPixelSize(R.dimen.head_bar_but_width);
		if(tip.length() > 2){
			w = getResources().getDimensionPixelSize(R.dimen.head_bar_but_width_long);
		}
		LayoutParams lp = mLeftButTv.getLayoutParams();
		lp.width = w;
		mLeftButTv.setLayoutParams(lp);
		
		lp = mRightButTv.getLayoutParams();
		lp.width = w;
		mRightButTv.setLayoutParams(lp);
		if(bgRes <= 0){
			butTV.setBackgroundResource(R.drawable.title_bar_but_selector);
		}else{
			butTV.setBackgroundResource(bgRes);
		}
		butIV.setTag(null);

		if (isLeft) {
			setLeftButtonEnabled(true);
		} else {
			setRightButtonEnabled(true);
		}
	}
	
	@Override
	public void setLeftButton(String tip, int icon, int bgRes) {
		setButton(true, tip, icon, null, bgRes);
	}

	@Override
	public void setRightButton(String tip, int icon, int bgRes) {
		setButton(false, tip, icon, null, bgRes);
	}

	@Override
	public void setLeftButtonImageView(Drawable drawable) {
		if (drawable != null) {
			setButton(true, null, 0, drawable, 0);
		}
	}

	@Override
	public void setRightButtonImageView(Drawable drawable) {
		if (drawable != null) {
			setButton(false, null, 0, drawable, 0);
		}
	}

	@Override
	public void setLeftButtonEnabled(boolean isEnabled) {
//		mLeftButIv.setVisibility(isEnabled ? View.VISIBLE : View.GONE);
//		mLeftButTv.setVisibility(isEnabled ? View.VISIBLE : View.GONE);
		if (isEnabled) {
			if (TextUtils.isEmpty(mLeftButTv.getText())) {
				mLeftButTv.setVisibility(View.GONE);
			} else {
				mLeftButTv.setVisibility(View.VISIBLE);
			}
			mLeftButIv.setVisibility(View.VISIBLE);
		} else {
			mLeftButIv.setVisibility(View.GONE);
			mLeftButTv.setVisibility(View.GONE);
		}
	}

	@Override
	public void setRightButtonEnabled(boolean isEnabled) {
		mRightButIv.setVisibility(isEnabled ? View.VISIBLE : View.INVISIBLE);
		mRightButTv.setVisibility(isEnabled ? View.VISIBLE : View.INVISIBLE);
	}
	
	@Override
	public void setTitleBarEnabled(boolean isEnabled) {
		mTitleLay.setVisibility(isEnabled ? View.VISIBLE : View.GONE);
	}
	
	@Override
	public void addTitleListener(OnTitleListener l) {
		mOnTitleListeners.add(new WeakReference<ITitleBar.OnTitleListener>(l));
	}

	@Override
	public void onBackPressed() {
		if(isMainActivity()){
			//退出应用的提示
			if((System.currentTimeMillis() - mExitTime) > 2000L) {
				ToastUtil.showToast(getApplicationContext(), R.string.tip_exit_app);
				mExitTime = System.currentTimeMillis();
				return;
			}
			sendBroadcast(new Intent(AppBroadcast.ACTION_CLOSE_APP));
		}
		super.onBackPressed();
	}
	
	public static class ForwardPopwin extends PopupWindow implements OnDismissListener{
		private ForwardViewControl mForwardViewControl;
		private OnDismissListener mOnDismissListener;
		
		public ForwardPopwin(View contentView, int width, int height) {
			super(contentView, width, height);
			super.setOnDismissListener(this);
		}

		public final void forceDismiss(){
			super.dismiss();
		}
		
		@Override
		public void dismiss() {
			if(mForwardViewControl == null || mForwardViewControl.onPreDismiss()){
				super.dismiss();
			}
		}
		
		public void setForwardViewControl(ForwardViewControl c){
			mForwardViewControl = c;
		}

		@Override
		public void setOnDismissListener(OnDismissListener onDismissListener) {
			mOnDismissListener = onDismissListener;
		}
		
		@Override
		public void onDismiss() {
			if(mForwardViewControl != null){
				mForwardViewControl.onDismiss();
			}
			if(mOnDismissListener != null){
				mOnDismissListener.onDismiss();
			}
		}
		
		public interface ForwardViewControl{
			public boolean onPreDismiss();
			public void onDismiss();
		}
	}
}
