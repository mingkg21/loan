package com.bk.android.time.ui.photo;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import com.bk.android.assistant.R;
import com.bk.android.binding.command.OnClickCommand;
import com.bk.android.time.model.BaseDataViewModel;
import com.bk.android.time.model.lightweight.AddImgModel.BitmapInfo;
import com.bk.android.time.ui.ILoadView;
import com.bk.android.time.ui.photo.ImageHandler.RemarkInfo;
import com.bk.android.time.ui.widget.binding.command.OnSuspensionGroupCommand;
import com.bk.android.time.util.ToastUtil;
import com.bk.android.util.FormatUtil;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import gueei.binding.collections.ArrayListObservable;
import gueei.binding.observables.BooleanObservable;
import gueei.binding.observables.IntegerObservable;
import gueei.binding.observables.StringObservable;

public class PhotoChoiceListViewModel extends BaseDataViewModel {
	public final IntegerObservable bSelectedPosition = new IntegerObservable(0);
	public final ArrayListObservable<GroupItemViewModel> bGroupItems = new ArrayListObservable<GroupItemViewModel>(GroupItemViewModel.class){
		public void onLoad(int position) {
			bGroupItems.get(position).onLoadItem(position);
		}
	};
	
	public final OnSuspensionGroupCommand bOnSuspensionGroupCommand = new OnSuspensionGroupCommand() {
		@Override
		public void onSuspensionGroupChange(int newPosition, int oldPosition) {
			if(oldPosition >= 0 && oldPosition < bGroupItems.size()){
				GroupItemViewModel item = bGroupItems.get(oldPosition);
				if(item.mRemarkInfo != null && !item.mRemarkInfo.isMustShow()){
					item.bDateRemark.set(null);
				}
			}
			if(newPosition >= 0 && newPosition < bGroupItems.size()){
				GroupItemViewModel item = bGroupItems.get(newPosition);
				if(item.mRemarkInfo != null && !item.mRemarkInfo.isMustShow()){
					item.bDateRemark.set(item.mRemarkInfo.getContent());
				}
			}
		}
	};
	
	private SimpleDateFormat mDayFormat = new SimpleDateFormat("MM-dd");
	private SimpleDateFormat mYearFormat = new SimpleDateFormat("yyyy-");
	private String mCurYearStr = mYearFormat.format(new Date());
	private ImageHandler mImageHandler;
	private ArrayList<BitmapInfo> mPhotolist;
	
	public PhotoChoiceListViewModel(Context context, ILoadView loadView,ArrayList<BitmapInfo> photolist,ImageHandler imageHandler) {
		super(context, loadView);
		setData(imageHandler, photolist);
	}

	public void setData(ImageHandler imageHandler, ArrayList<BitmapInfo> photolist) {
		mPhotolist = photolist;
		mImageHandler = imageHandler;
	}
	
	public int getDataSize() {
		return mPhotolist != null ? mPhotolist.size() : 0;
	}
	
	public ArrayList<BitmapInfo> getPhotolist() {
		return mPhotolist;
	}
	
	private void fillData(){
		Runnable runnable = new Runnable() {
			ArrayList<GroupItemViewModel> mTempItems = new ArrayList<GroupItemViewModel>();
			int mIndex = 0;
			@Override
			public void run() {
				Calendar calendar = Calendar.getInstance();
				int dayTime = 24 * 60 * 60 * 1000;
				long lastTime = 0;
				int lastDay = 0;
				String lastImagePath = mImageHandler.getLastImagePath();
				int itemIndex = 0;
				GroupItemViewModel groupItem = null;
				for (int i = 0; i < mPhotolist.size(); i++) {
					BitmapInfo info = mPhotolist.get(i);
					calendar.setTimeInMillis(info.mLastModified);
					int day = calendar.get(Calendar.DAY_OF_MONTH);
					if(lastTime == 0 || (day != lastDay) || (day == lastDay && Math.abs(lastTime - info.mLastModified) > dayTime)){
						if(groupItem != null){
							groupItem.finish();
						}
						groupItem = new GroupItemViewModel();
						groupItem.mLastModified = info.mLastModified;
						groupItem.mPreLastModified = lastTime;
						groupItem.bCanSelectAll.set(mImageHandler != null ? mImageHandler.canSelectAllByDay() : false);
						mTempItems.add(groupItem);
						lastTime = info.mLastModified;
						lastDay = day;
						itemIndex++;
					}
					if(addImgItemViewModel(info, groupItem)){
						itemIndex++;
					}
					if(mIndex == 0 && !TextUtils.isEmpty(lastImagePath) && lastImagePath.equals(info.mPath)){
						mIndex = itemIndex - 2;
					}
				}
				if(groupItem != null){
					groupItem.finish();
				}
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						bGroupItems.setAll(mTempItems);
						bSelectedPosition.set(mIndex);
						hideLoadView();
					}
				});
			}
		};
		showLoadView();
		new Thread(runnable).start();
	}
	
	private boolean addImgItemViewModel(BitmapInfo info,GroupItemViewModel groupItem){
		boolean isNewChildItem = false;
		ChildItemViewModel item = null;
		if(!groupItem.bChildItems.isEmpty()){
			item = groupItem.bChildItems.get(groupItem.bChildItems.size() - 1);
		}
		if(item == null || item.mImgSize == 3){
			item = new ChildItemViewModel();
			item.bImgItems.add(new ImgItemViewModel(groupItem));
			item.bImgItems.add(new ImgItemViewModel(groupItem));
			item.bImgItems.add(new ImgItemViewModel(groupItem));
			groupItem.bChildItems.add(item);
			isNewChildItem = true;
		}
		item.bImgItems.get(item.mImgSize).set(info);
		item.mImgSize ++;
		return isNewChildItem;
	}
	
	@Override
	public void onStart() {
		fillData();
	}

	@Override
	public void onRelease() {}

	public void notifyDataSetChanged(){
		if(mImageHandler == null){
			return;
		}
		for (GroupItemViewModel groupItem : bGroupItems) {
			for (ChildItemViewModel childItem : groupItem.bChildItems) {
				for (ImgItemViewModel imgItem : childItem.bImgItems) {
					imgItem.fillSelectState();
				}
			}
			groupItem.fillSelectState();
		}
	}
	
	public class GroupItemViewModel{
		public final BooleanObservable bCanSelectAll = new BooleanObservable(false);
		public final StringObservable bDate = new StringObservable();
		public final StringObservable bDateRemark = new StringObservable();
		public final ArrayListObservable<ChildItemViewModel> bChildItems = new ArrayListObservable<ChildItemViewModel>(ChildItemViewModel.class);
		public final BooleanObservable bIsSelectAll = new BooleanObservable(false);
		private long mLastModified;
		private long mPreLastModified;
		private RemarkInfo mRemarkInfo;
		public final OnClickCommand bOnClickCommand = new OnClickCommand() {
			@Override
			public void onClick(View v) {
				if(bIsSelectAll.get()){
					cancelAll();
				}else{
					selectAll();
				}
			}
		};
		
		public void onLoadItem(int position){
			if(mLastModified > 0){
				mRemarkInfo = mImageHandler.getDateRemark(mLastModified,mPreLastModified);
				bDateRemark.set(mRemarkInfo != null && mRemarkInfo.isMustShow() ? mRemarkInfo.getContent() : null);
				String yearStr = mYearFormat.format(new Date(mLastModified));
				String newDayStr = null;
				if(mCurYearStr.equals(yearStr)){
					newDayStr = mDayFormat.format(new Date(mLastModified));
				}else{
					newDayStr = yearStr + mDayFormat.format(new Date(mLastModified));
				}
				bDate.set(newDayStr);
				mLastModified = 0;
			}
		}
		
		public void finish() {
			if(!bChildItems.isEmpty()){
				bChildItems.get(0).bIsTop.set(true);
				bChildItems.get(bChildItems.size() - 1).bIsBottom.set(true);
			}
			fillSelectState();
		}

		public void fillSelectState() {
			if(!bCanSelectAll.get()){
				return;
			}
			for (ChildItemViewModel childItem : bChildItems) {
				for (ImgItemViewModel imgItem : childItem.bImgItems) {
					if(imgItem.mBitmapInfo != null && !imgItem.bIsSelect.get()){
						bIsSelectAll.set(false);
						return;
					}
				}
			}
			bIsSelectAll.set(true);
		}
		
		public void selectAll(){
			boolean isSuccess = true;
			boolean isBreak = false;
			for (ChildItemViewModel childItem : bChildItems) {
				for (ImgItemViewModel imgItem : childItem.bImgItems) {
					if(imgItem.mBitmapInfo != null && !imgItem.bIsSelect.get()){
						if(!imgItem.handSelect(false)){
							isSuccess = false;
							if(mImageHandler.getSelectItemMap().size() == mImageHandler.getPhotoNumberLimit()){
								isBreak = true;
								break;
							}
						}
					}
				}
				if(isBreak){
					break;
				}
			}
			bIsSelectAll.set(isSuccess);
		}
		
		public void cancelAll(){
			boolean isSuccess = true;
			for (ChildItemViewModel childItem : bChildItems) {
				for (ImgItemViewModel imgItem : childItem.bImgItems) {
					if(imgItem.mBitmapInfo != null && imgItem.bIsSelect.get()){
						if(!imgItem.handSelect(false)){
							isSuccess = false;
						}
					}
				}
			}
			bIsSelectAll.set(!isSuccess);
		}
	}

	public class ChildItemViewModel{
		public final BooleanObservable bIsTop = new BooleanObservable(false);
		public final BooleanObservable bIsBottom = new BooleanObservable(false);
		public int mImgSize;
		public final ArrayListObservable<ImgItemViewModel> bImgItems = new ArrayListObservable<ImgItemViewModel>(ImgItemViewModel.class);
	}
	
	public class ImgItemViewModel{
		public final BooleanObservable bIsSelect = new BooleanObservable(false);
		public final BooleanObservable bIsVideo = new BooleanObservable(false);
		public final BooleanObservable bIsPixelNotEnough = new BooleanObservable(false);
		public final StringObservable bVideoDuration = new StringObservable();
		public final StringObservable bIconUrl = new StringObservable();
		public final IntegerObservable bUploadTipRes = new IntegerObservable(0);
		public final OnClickCommand bOnClickCommand = new OnClickCommand() {
			@Override
			public void onClick(View v) {
				if(mBitmapInfo == null){
					return;
				}
				int index = 0;
				ArrayList<BitmapInfo> imgs = new ArrayList<BitmapInfo>();
				for (GroupItemViewModel groupItem : bGroupItems) {
					for (ChildItemViewModel childItem : groupItem.bChildItems) {
						for (ImgItemViewModel imgItem : childItem.bImgItems) {
							if(imgItem.mBitmapInfo != null){
								if(imgItem.mBitmapInfo.equals(mBitmapInfo)){
									index = imgs.size();
								}
								imgs.add(imgItem.mBitmapInfo);
							}
						}
					}
				}
//				ActivityChannels.openActivityWithSelect((Activity) getContext(), imgs, index,mImageHandler,10001);
			}
		};
		
		public final OnClickCommand bOnCheckedChangeCommand = new OnClickCommand(){
			@Override
			public void onClick(View v) {
				if(handSelect(true)) {
				}
			}
		};
		
		public final OnClickCommand bOnPixelNotEnoughCommand = new OnClickCommand(){
			@Override
			public void onClick(View v) {
				ToastUtil.showToast(getContext(), R.string.pixel_not_enough);
			}
		};
		
		public BitmapInfo mBitmapInfo;
		private GroupItemViewModel mGroupItem;
		
		public ImgItemViewModel(GroupItemViewModel groupItem){
			mGroupItem = groupItem;
		}
		
		public void set(BitmapInfo info){
			mBitmapInfo = info;
			bVideoDuration.set(FormatUtil.getMillisecondTimeStr(mBitmapInfo.mDuration, false));
			bIsVideo.set(mBitmapInfo.isVideo());
			bIconUrl.set(setItemImgPath(mBitmapInfo.mPath));
			bIsPixelNotEnough.set(mImageHandler.isPixelNotEnough(mBitmapInfo.mWidth, mBitmapInfo.mHeight));
			bUploadTipRes.set(mBitmapInfo.isUpload && !bIsPixelNotEnough.get() ? mImageHandler.getUploadTipRes() : 0);
			fillSelectState();
		}
		
		public void fillSelectState(){
			if(mBitmapInfo != null){
				bIsSelect.set(mImageHandler.hasSelect(mBitmapInfo));
			}
		}
		
		public boolean handSelect(boolean needFillGroup){
			boolean isSuccess = false;
			if(mBitmapInfo != null){
				boolean oldSelect = bIsSelect.get();
				bIsSelect.set(mImageHandler.onHandSelect(mBitmapInfo));
				isSuccess = oldSelect != bIsSelect.get();
				if(needFillGroup){
					mGroupItem.fillSelectState();
				}
			}
			return isSuccess;
		}
		
		private String setItemImgPath(String path){
			if(path == null){
				return path;
			}
			if(path.startsWith("file://") || path.startsWith("http://") || path.startsWith("android.resource://")) {
				return path;
			} else {
				return "file://" + path;
			}
		}
	}
}
