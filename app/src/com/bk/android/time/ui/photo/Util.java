package com.bk.android.time.ui.photo;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.AbstractMap.SimpleEntry;
import java.util.Map.Entry;

import com.bk.android.time.model.lightweight.AddImgModel.BitmapInfo;

public class Util {
	@SuppressWarnings("unchecked")
	public static ArrayList<SimpleEntry<String, BitmapInfo>> castEntryList(Object data){
		LinkedHashMap<String, BitmapInfo> map = null;
		try {
			map = (LinkedHashMap<String, BitmapInfo>) data;
		} catch (Exception e) {}
		return castEntryList(map);
	}
	
	public static ArrayList<SimpleEntry<String, BitmapInfo>> castEntryList(LinkedHashMap<String, BitmapInfo> map){
		if(map == null){
			return null;
		}
		ArrayList<SimpleEntry<String, BitmapInfo>> entrys = new ArrayList<SimpleEntry<String, BitmapInfo>>();
		for (Entry<String, BitmapInfo> entry : map.entrySet()) {
			entrys.add(new SimpleEntry<String, BitmapInfo>(entry));
		}
		return entrys;
	}
	
	@SuppressWarnings("unchecked")
	public static LinkedHashMap<String, BitmapInfo> castLinkedHashMap(Object data){
		ArrayList<SimpleEntry<String, BitmapInfo>> list = null;
		try {
			list = (ArrayList<SimpleEntry<String, BitmapInfo>>) data;
		} catch (Exception e) {}
		return castLinkedHashMap(list);
	}
	
	public static LinkedHashMap<String, BitmapInfo> castLinkedHashMap(ArrayList<SimpleEntry<String, BitmapInfo>> list){
		if(list == null){
			return null;
		}
		LinkedHashMap<String, BitmapInfo> map = new LinkedHashMap<String,BitmapInfo>();
		for (SimpleEntry<String, BitmapInfo> entry : list) {
			map.put(entry.getKey(), entry.getValue());
		}
		return map;
	}
}
