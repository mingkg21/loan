package com.bk.android.time.ui.photo;

import android.content.ContentResolver;
import android.database.ContentObserver;
import android.database.Cursor;
import android.os.AsyncTask;
import android.provider.MediaStore.Images;
import android.provider.MediaStore.Video;
import android.text.TextUtils;

import com.bk.android.time.app.App;
import com.bk.android.time.model.lightweight.AddImgModel.BitmapInfo;
import com.bk.android.time.util.AppFileUtil;
import com.bk.android.util.BitmapUtil;
import com.bk.android.util.LogUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;

public class PhotoLoadAsyncTask extends AsyncTask<String, String, LinkedHashMap<String, ArrayList<BitmapInfo>>>{
	public static final String FOLDER_NAME_ALL_IMG = "所有图片";

	private static ArrayList<BitmapInfo> sPhotoList = new ArrayList<BitmapInfo>();
	private static HashMap<String, Integer> sCameraSizeMap = new HashMap<String, Integer>();
	private static HashMap<String, Boolean> sWXPathMap = new HashMap<String, Boolean>();
	private static final Object LOAD_DATA_LOCK = new Object();
	private static boolean isInit;
	private static boolean hasLoadData;
	private static boolean hasNewData;
	private static Thread mLoadDataThread;

	public static void init(){
		if(isInit){
			return;
		}
		isInit = true;
		loadData();
		hasNewData = true;
		App.getInstance().getContentResolver().registerContentObserver(Images.Media.EXTERNAL_CONTENT_URI, true, new ContentObserver(null) {
			@Override
			public void onChange(boolean selfChange) {
				hasNewData = true;
				loadData();
			}
		});
		App.getInstance().getContentResolver().registerContentObserver(Video.Media.EXTERNAL_CONTENT_URI, true, new ContentObserver(null) {
			@Override
			public void onChange(boolean selfChange) {
				hasNewData = true;
				loadData();
			}
		});
	}
	
	public static String getCameraFolder(){
		String path = null;
		int size = 0;
		synchronized (sCameraSizeMap) {
			for (Entry<String, Integer> entry : sCameraSizeMap.entrySet()) {
				if(entry.getValue() != null && entry.getValue() > size){
					path = entry.getKey();
					size = entry.getValue();
				}
			}
		}
		return path;
	}

	private static void loadData(){
		synchronized (LOAD_DATA_LOCK) {
			if(mLoadDataThread != null){
				return;
			}
			boolean oldHasNewData = hasNewData;
			hasNewData = false;
			mLoadDataThread = new Thread(){
				@Override
				public void run() {
					try {
						long time = System.currentTimeMillis();
						ArrayList<BitmapInfo> tempList = getImages();
						LogUtil.v("PhotoLoadAsyncTask", "all time: " + (System.currentTimeMillis() - time));
						synchronized (sPhotoList) {
							if(!tempList.isEmpty()){
								sPhotoList.clear();
								for (BitmapInfo newInfo : tempList) {
									int index = -1;
									for (int i = 0; i < sPhotoList.size(); i++) {
										BitmapInfo oldInfo = sPhotoList.get(i);
										if(index == -1 && oldInfo.mLastModified < newInfo.mLastModified){
											index = i;
										}
										if(oldInfo.mPath.equals(newInfo.mPath)){
											index = -2;
											break;
										}
									}
									if(index == -1){
										index = sPhotoList.size();
									}
									if(index == -2){
										continue;
									}
									sPhotoList.add(index, newInfo);
								}
							}
							hasLoadData = true;
						}
					} catch (Exception e) {
						e.printStackTrace();
						hasLoadData = false;
					}
					synchronized (LOAD_DATA_LOCK) {
						mLoadDataThread = null;
						if(hasNewData){
							loadData();
						}
					}
				}
			};
			mLoadDataThread.setPriority(oldHasNewData ? Thread.MIN_PRIORITY : Thread.NORM_PRIORITY);
			mLoadDataThread.start();
		}
	}
	

	private static ArrayList<BitmapInfo> getImages(){
		ArrayList<BitmapInfo> tempList = new ArrayList<BitmapInfo>();
		try {
			ContentResolver contentResolver = App.getInstance().getContentResolver();
			Cursor cursor = contentResolver.query(Images.Media.EXTERNAL_CONTENT_URI,
					new String[]{Images.Media.DATA, Images.Media.DATE_TAKEN, Images.Media.LONGITUDE, Images.Media.LATITUDE, Images.Media.WIDTH, Images.Media.HEIGHT, Images.Media.SIZE},
					Images.Media.DATA + " NOT LIKE '%" + AppFileUtil.getAppPath()+ "%' AND (" + Images.Media.MIME_TYPE + "=? OR " + Images.Media.MIME_TYPE + "=?)",
					new String[] {"image/jpeg", "image/png"}, Images.Media.DATE_TAKEN + " desc");
			if(cursor != null){
				while (cursor.moveToNext()) {
					try {
						String path = cursor.getString(cursor.getColumnIndex(Images.Media.DATA));
						if(!TextUtils.isEmpty(path)){
							File file = new File(path);
							if(file.exists()){
//								saveCameraSize(file);
								
								BitmapInfo info = new BitmapInfo(path, 0, 0);
								
//								info.mLastModified = file.lastModified();
//								info.mLastModified = cursor.getLong(cursor.getColumnIndex(Images.Media.DATE_TAKEN));
//								info.mSize = cursor.getLong(cursor.getColumnIndex(Images.Media.SIZE));

//								info.mLongitude = new String[2];
//								info.mLongitude[0] = cursor.getString(cursor.getColumnIndex(Images.Media.LONGITUDE));
//								info.mLongitude[1] = cursor.getString(cursor.getColumnIndex(Images.Media.LATITUDE));

								int degree = BitmapUtil.readPictureDegree(path);
								if(degree != 0 && degree != 180){
									info.mWidth = cursor.getInt(cursor.getColumnIndex(Images.Media.HEIGHT));
									info.mHeight = cursor.getInt(cursor.getColumnIndex(Images.Media.WIDTH));
								}else{
									info.mWidth = cursor.getInt(cursor.getColumnIndex(Images.Media.WIDTH));
									info.mHeight = cursor.getInt(cursor.getColumnIndex(Images.Media.HEIGHT));
								}
								if(info.mWidth == 0 || info.mHeight == 0){
									int[] wh = BitmapUtil.getLocalWH(path,null,true);
									if(wh != null){
										info.mWidth = wh[0];
										info.mHeight = wh[1];
									}
								}else{
									BitmapUtil.saveLocalWH(path, info.mWidth, info.mHeight);
								}
								
								tempList.add(info);
							}
						}
					} catch (Exception e) {e.printStackTrace();}
				}
				cursor.close();
			}
		} catch (Exception e) {e.printStackTrace();}
		return tempList;
	}
	
	private static void saveCameraSize(File file){
		if(file.getParent().toLowerCase().indexOf("camera") != -1 
				 || file.getParent().toLowerCase().indexOf("相机") != -1
				 || file.getParent().toLowerCase().indexOf("相册") != -1
				 || file.getParent().toLowerCase().indexOf("media") != -1){
			synchronized (sCameraSizeMap) {
				Integer size = sCameraSizeMap.get(file.getParent());
				if(size == null){
					size = 0;
				}
				sCameraSizeMap.put(file.getParent(), size + 1);
			}
		}
	}
	
	private boolean mHadCamera;
	private Callback mCallback;
	private ImageHandler mImageHandler;

	public PhotoLoadAsyncTask(boolean hadCamera, Callback callback, ImageHandler imageHandler){
		mHadCamera = hadCamera;
		mCallback = callback;
		mImageHandler = imageHandler;
	}

	public void start(){
		synchronized (sPhotoList) {
			if(!sPhotoList.isEmpty() || hasLoadData){
				LinkedHashMap<String, ArrayList<BitmapInfo>> result = loadPhotoMap();
				mCallback.onLoadFinish(photoInAblum(result), result);
			}else{
				execute();
			}
		}
	}
	
	private LinkedHashMap<String, ArrayList<BitmapInfo>> loadPhotoMap(){
		LinkedHashMap<String, ArrayList<BitmapInfo>> photoDirInfoMap = new LinkedHashMap<String, ArrayList<BitmapInfo>>();
		ArrayList<BitmapInfo> allImgList = new ArrayList<BitmapInfo>();
		ArrayList<BitmapInfo> allVideoList = new ArrayList<BitmapInfo>();
		photoDirInfoMap.put(FOLDER_NAME_ALL_IMG, allImgList);
		synchronized (sPhotoList) {
			for (BitmapInfo info : sPhotoList) {
				if(!ImageHandler.isFilter(info.mPath,info.mWidth,info.mHeight)){
					continue;
				}
				File file = new File(info.mPath);
				if(file.exists()){
					info.isUpload = mImageHandler.isUpload(info.mPath);
					String parentName = "";
					Boolean isWXVideo = sWXPathMap.get(file.getAbsolutePath());
					if(isWXVideo != null){
						parentName = isWXVideo ? "微信视频" : "微信照片";
					}else if(file.getParentFile() != null){
						parentName = file.getParent();
					}
					if(mImageHandler.getDataType() == ImageHandler.DATA_TYPE_IMG){
						if(info.isVideo()){
							continue;
						}else{
							allImgList.add(info);
						}
					}else if(mImageHandler.getDataType() == ImageHandler.DATA_TYPE_VIDEO){
						if(info.isVideo()){
							allVideoList.add(info);
						}else{
							continue;
						}
					}else{
						if(info.isVideo()){
							allVideoList.add(info);
						}else{
							allImgList.add(info);
						}
					}
					if (!photoDirInfoMap.containsKey(parentName)) {
						ArrayList<BitmapInfo> chileList = new ArrayList<BitmapInfo>();
						chileList.add(info);
						photoDirInfoMap.put(parentName, chileList);
					} else {
						photoDirInfoMap.get(parentName).add(info);
					}
				}
			}
		}
		return photoDirInfoMap;
	}
	
	@Override
	protected void onPostExecute(LinkedHashMap<String, ArrayList<BitmapInfo>> result) {
		super.onPostExecute(result);
		mCallback.onLoadFinish(photoInAblum(result), result);
	}

	@Override
	protected LinkedHashMap<String, ArrayList<BitmapInfo>> doInBackground(String... arg0) {
		init();
		for(;true;){
			synchronized (sPhotoList) {
				if(!sPhotoList.isEmpty() || hasLoadData){
					break;
				}
			}
			loadData();
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {}
		}
		return loadPhotoMap();
	}

	private ArrayList<PhotoDirInfo> photoInAblum(LinkedHashMap<String, ArrayList<BitmapInfo>> gruopMap){
		ArrayList<PhotoDirInfo> list = new ArrayList<PhotoDirInfo>();
		if(gruopMap == null || gruopMap.size() == 0){
			return list;
		}

		Iterator<Entry<String, ArrayList<BitmapInfo>>> it = gruopMap.entrySet().iterator();
		while (it.hasNext()) {
			Entry<String, ArrayList<BitmapInfo>> entry = it.next();
			PhotoDirInfo photoDirInfo = new PhotoDirInfo();
			String key = entry.getKey();
			List<BitmapInfo> value = entry.getValue();

			if(value.size() > 0) {
				String name = key;
				int index = key.lastIndexOf("/") + 1;
				if(index != 0 && index < key.length()){
					name = key.substring(index, key.length());
				}
				photoDirInfo.setFolderName(name);
				photoDirInfo.setFolderPath(key);
				int imageCounts = 0;
				int videoCounts = 0;
				for (BitmapInfo bitmapInfo : value) {
					if(bitmapInfo.isVideo()){
						videoCounts++;
					}else{
						imageCounts++;
					}
				}
				photoDirInfo.setImageCounts(imageCounts);
				photoDirInfo.setVideoCounts(videoCounts);

				if(FOLDER_NAME_ALL_IMG.equals(photoDirInfo.getFolderPath())) {
					if(mHadCamera){
						photoDirInfo.setTopImagePath(value.size() > 1 ? value.get(1).mPath : "");
					}else{
						photoDirInfo.setTopImagePath(value.get(0).mPath);
					}
					list.add(photoDirInfo);
				}
			}
		}
		return list;
	}

	public interface Callback{
		public void onLoadFinish(ArrayList<PhotoDirInfo> list, LinkedHashMap<String, ArrayList<BitmapInfo>> result);
	}
}
