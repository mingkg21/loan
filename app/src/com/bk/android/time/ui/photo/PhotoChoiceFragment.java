package com.bk.android.time.ui.photo;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bk.android.assistant.R;
import com.bk.android.time.model.lightweight.AddImgModel.BitmapInfo;
import com.bk.android.time.ui.BaseFragment;

import java.util.ArrayList;

public class PhotoChoiceFragment extends BaseFragment {
	private ImageHandler mImageHandler;
	private ArrayList<BitmapInfo> mPhotolist;
	private PhotoChoiceListViewModel mPhotoChoiceListViewModel;
	
	@Override
	public View onCreateContentView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mPhotoChoiceListViewModel = new PhotoChoiceListViewModel(getActivity(), this, mPhotolist, mImageHandler);
		View contentView = bindView(R.layout.uniq_photo_list_lay, null, mPhotoChoiceListViewModel);
		mPhotoChoiceListViewModel.onStart();
		return contentView;
	}

	@Override
	public void onDestroyContentView() {
		mPhotoChoiceListViewModel.onRelease();
	}

	public boolean isDataChange(ImageHandler imageHandler, ArrayList<BitmapInfo> photolist){
		return !imageHandler.equals(mImageHandler) || !photolist.equals(mPhotolist);
	}
	
	public void setData(ImageHandler imageHandler, ArrayList<BitmapInfo> photolist){
		mImageHandler = imageHandler;
		mPhotolist = photolist;
		if(mPhotoChoiceListViewModel != null){
			mPhotoChoiceListViewModel.setData(imageHandler, photolist);
			mPhotoChoiceListViewModel.onStart();
		}
	}
	
	public void notifyDataSetChanged(){
		if(mPhotoChoiceListViewModel != null){
			mPhotoChoiceListViewModel.notifyDataSetChanged();
		}
	}
}
