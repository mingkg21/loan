//package com.bk.android.time.ui.photo;
//
//import android.app.Activity;
//import android.content.Context;
//import android.content.Intent;
//import android.os.Bundle;
//import android.support.v4.app.Fragment;
//import android.support.v4.app.FragmentTransaction;
//import android.text.TextUtils;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.widget.Button;
//
//import com.bk.android.assistant.R;
//import com.bk.android.time.model.lightweight.AddImgModel.BitmapInfo;
//import com.bk.android.time.model.lightweight.UserTrackModel;
//import com.bk.android.time.ui.BaseAppActivity;
//import com.bk.android.time.ui.activiy.ActivityChannels;
//import com.bk.android.time.ui.photo.ImageHandler.SelectLinstener;
//import com.bk.android.time.ui.photo.PhotoDirListViewModel.OnSelectDirListener;
//import com.bk.android.time.ui.photo.PhotoLoadAsyncTask.Callback;
//import com.bk.android.time.util.MobclickUtil;
//
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.LinkedHashMap;
//
//public class PhotoSelectActivity extends BaseAppActivity implements OnSelectDirListener {
//	public static final String EXTRA_NAME_PHOTO_SELECT_LIST = "extra_name_photo_info_select_list";
//
//	public static final int VIEW_TYPE_DIR = 1;
//	public static final int VIEW_TYPE_LOCAL = 2;
//
//	public static void openActivity(Context context, int requestCode,ImageHandler imageHandler) {
//		Intent intent = new Intent(context, PhotoSelectActivity.class);
//		intent.putExtra("imageHandler", imageHandler);
//		if(context instanceof Activity){
//			((Activity) context).startActivityForResult(intent, requestCode);
//		}else{
//			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//			context.startActivity(intent);
//		}
//	}
//
//	public static ArrayList<String> getResultStringList(Intent data){
//		ArrayList<String> strList = new ArrayList<String>();
//		if(data != null){
//			@SuppressWarnings("unchecked")
//			ArrayList<BitmapInfo> resultList = (ArrayList<BitmapInfo>) data.getSerializableExtra(EXTRA_NAME_PHOTO_SELECT_LIST);
//			if(resultList != null){
//				for (BitmapInfo info : resultList) {
//					strList.add(info.mPath);
//				}
//			}
//		}
//		return strList;
//	}
//
//	protected ImageHandler mImageHandler;
//	private HashMap<String, ArrayList<BitmapInfo>> mPhotoAlbumMap;
//	private ArrayList<PhotoDirInfo> mDirList;
//	private String mSelectDirName;
//	private ArrayList<BitmapInfo> mSelectDirList;
//	private int mMaxSelectSize;
//	private Button mPreviewBtn;
//	private Button mConfirmBtn;
//	private boolean isSubmit;
//	private Button mFullConfirmBtn;
//	private int mViewType;
//
//	private Fragment mShowFragment;
//	private PhotoDirListFragment mPhotoDirListFragment;
//	private PhotoChoiceFragment mPhotoChoiceFragment;
//	private NetPhotoChoiceFragment mNetPhotoChoiceFragment;
//
//	protected ImageHandler getImageHandler(Bundle arg0){
//		ImageHandler imageHandler;
//		if(arg0 != null){
//			imageHandler = (ImageHandler) arg0.getSerializable("mImageHandler");
//		}else{
//			imageHandler = (ImageHandler) getIntent().getSerializableExtra("imageHandler");
//		}
//		return imageHandler;
//	}
//
//	@Override
//	protected void onCreate(Bundle arg0) {
//		super.onCreate(arg0);
//		mImageHandler = getImageHandler(arg0);
//		mMaxSelectSize = mImageHandler.getPhotoNumberLimit();
//		setContentView(R.layout.uniq_photo_select_lay);
//		mPreviewBtn = (Button) findViewById(R.id.preview_btn);
//		mConfirmBtn = (Button) findViewById(R.id.confirm_btn);
//		mFullConfirmBtn = (Button) findViewById(R.id.confirm_btn_full);
//		OnClickListener onClickListener = new OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				isSubmit = true;
//				finish();
//			}
//		};
//		mConfirmBtn.setOnClickListener(onClickListener);
//		mFullConfirmBtn.setOnClickListener(onClickListener);
//		mPreviewBtn.setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				ArrayList<BitmapInfo> imgs = new ArrayList<BitmapInfo>();
//				LinkedHashMap<String, BitmapInfo> selectList = mImageHandler.getSelectItemMap();
//				if(selectList != null){
//					for (BitmapInfo bitmapInfo : selectList.values()) {
//						imgs.add(bitmapInfo);
//					}
//				}
//				ActivityChannels.openActivityWithSelect(this_, imgs, 0, mImageHandler, 10001);
//			}
//		});
//		mPreviewBtn.setVisibility(mImageHandler.canPreviewSelect() ? View.VISIBLE : View.GONE);
//		mConfirmBtn.setVisibility(mImageHandler.canPreviewSelect() ? View.VISIBLE : View.GONE);
//		mFullConfirmBtn.setVisibility(mImageHandler.canPreviewSelect() ? View.GONE : View.VISIBLE);
//
//		showLoadView();
//		PhotoLoadAsyncTask photoLoadAsyncTask = new PhotoLoadAsyncTask(false, new Callback() {
//			@Override
//			public void onLoadFinish(final ArrayList<PhotoDirInfo> list, final LinkedHashMap<String, ArrayList<BitmapInfo>> result) {
//				if(!isFinishing()){
//					mDirList = list;
//					mPhotoAlbumMap = result;
//					String firstDir = null;
//					int type = VIEW_TYPE_DIR;
//					String lastPathDir = mImageHandler.getLastImagePathDir();
//					if((lastPathDir != null && lastPathDir.startsWith("http://")) || (lastPathDir == null && mImageHandler.isSelectPrint())){
//						type = VIEW_TYPE_NET;
//					}else{
//						if(!TextUtils.isEmpty(lastPathDir) && mPhotoAlbumMap.containsKey(lastPathDir)){
//							firstDir = lastPathDir;
//						}
//						if(firstDir == null){
//							if (!mImageHandler.isDirMain()) {
//								if (mImageHandler.isGotoCamera()) {
//									firstDir = PhotoLoadAsyncTask.getCameraFolder();
//								} else {
//									if(mImageHandler.getDataType() == ImageHandler.DATA_TYPE_IMG){
//										firstDir = PhotoLoadAsyncTask.FOLDER_NAME_ALL_IMG;
//									}else if(mImageHandler.getDataType() == ImageHandler.DATA_TYPE_VIDEO){
//										firstDir = PhotoLoadAsyncTask.FOLDER_NAME_ALL_VIDEO;
//									}else{
//										firstDir = PhotoLoadAsyncTask.getCameraFolder();
//									}
//								}
//							} else {
//								if (mImageHandler.isGotoCamera()) {
//									firstDir = PhotoLoadAsyncTask.getCameraFolder();
//								}
//							}
//						}
//						if (firstDir != null) {
//							ArrayList<BitmapInfo> infos = mPhotoAlbumMap.get(firstDir);
//							if(infos != null){
//								String firstDirName = firstDir;
//								for (PhotoDirInfo info : list) {
//									if(info.getFolderPath().equals(firstDir)){
//										firstDirName = info.getFolderName();
//									}
//								}
//								mSelectDirName = firstDirName;
//								mSelectDirList = infos;
//								type = VIEW_TYPE_LOCAL;
//							}
//						}
//					}
//					showView(type);
//					hideLoadView();
//				}
//			}
//		},mImageHandler);
//		photoLoadAsyncTask.start();
//
//		mImageHandler.setSelectLinstener(new SelectLinstener() {
//			@Override
//			public void onCheckedChanged(int number) {
//				setBtnState();
//				if(number == mImageHandler.getPhotoNumberLimit() && number == 1){
//					mConfirmBtn.performClick();
//				}
//			}
//		});
//	}
//
//	protected void onShowViewChange(int type){}
//
//	private void showView(int type){
//		if(mViewType == type){
//			return;
//		}
//		FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
//		if(mShowFragment != null){
//			fragmentTransaction.hide(mShowFragment);
//		}
//		mShowFragment = null;
//		mViewType = type;
//		boolean isNew = false;
//		if(mViewType == VIEW_TYPE_DIR){
//			if(mPhotoDirListFragment == null){
//				mPhotoDirListFragment = new PhotoDirListFragment();
//				mPhotoDirListFragment.init(mDirList, this);
//				fragmentTransaction.add(R.id.content_view, mPhotoDirListFragment);
//				isNew = true;
//			}
//			mShowFragment = mPhotoDirListFragment;
//		}else if(mViewType == VIEW_TYPE_LOCAL){
//			if(mPhotoChoiceFragment == null || mPhotoChoiceFragment.isDataChange(mImageHandler, mSelectDirList)){
//				if(mPhotoChoiceFragment != null){
//					fragmentTransaction.remove(mPhotoChoiceFragment);
//				}
//				mPhotoChoiceFragment = new PhotoChoiceFragment();
//				mPhotoChoiceFragment.setData(mImageHandler, mSelectDirList);
//				fragmentTransaction.add(R.id.content_view, mPhotoChoiceFragment);
//				isNew = true;
//			}
//			mShowFragment = mPhotoChoiceFragment;
//		}
//		if(mShowFragment != null && !isNew){
//			fragmentTransaction.show(mShowFragment);
//		}
//		setBtnState();
//		fragmentTransaction.commit();
//		onShowViewChange(mViewType);
//	}
//
//	private void setBtnState(){
//		int number = mImageHandler.getSelectItemMap().size();
//		mConfirmBtn.setText(getString(R.string.btn_text_confirm) + "("+number+"/"+mMaxSelectSize+")");
//		mPreviewBtn.setEnabled(number > 0);
//		mConfirmBtn.setEnabled(number > 0);
//		if(mViewType == VIEW_TYPE_DIR){
//			setTitle(R.string.photo_dir_list_choice_tip);
//			setRightButtonEnabled(false);
//		}else if(mViewType == VIEW_TYPE_LOCAL){
//			setTitle(mSelectDirName);
//			if(mImageHandler.canSelectAll()){
//				setRightButtonEnabled(true);
//				setRightButton(mImageHandler.isSelectAll(mSelectDirList.size()) ? getString(R.string.btn_text_cancel_check_all) : getString(R.string.btn_text_check_all), 0, 0);
//			}else if(!mImageHandler.isDirMain()){
//				setRightButtonEnabled(true);
//				setRightButton(getString(R.string.btn_text_dir), 0, 0);
//			}
//		}
//	}
//
//	private void notifyDataSetChanged(){
//		if(mViewType == VIEW_TYPE_DIR){
//		}else if(mViewType == VIEW_TYPE_LOCAL){
//			if(mPhotoChoiceFragment != null){
//				mPhotoChoiceFragment.notifyDataSetChanged();
//			}
//		}
//	}
//
//	@Override
//	public boolean onTitleButtonClick(boolean isLeft) {
//		if(!isLeft){
//			if(mViewType == VIEW_TYPE_DIR){
//			}else if(mViewType == VIEW_TYPE_LOCAL){
//				if(mImageHandler.canSelectAll()){
//					mImageHandler.onSwitchSelectAll(mSelectDirList);
//					notifyDataSetChanged();
//				}else if(!mImageHandler.isDirMain()){
//					showView(VIEW_TYPE_DIR);
//				}
//			}
//		}
//		return super.onTitleButtonClick(isLeft);
//	}
//	@Override
//	public void onBackPressed() {
//		if(mViewType != VIEW_TYPE_DIR && !mImageHandler.isDirMain()){
//			showView(VIEW_TYPE_DIR);
//		}else{
//			super.onBackPressed();
//		}
//	}
//
//	@Override
//	protected void onSaveInstanceState(Bundle outState) {
//		super.onSaveInstanceState(outState);
//		outState.putSerializable("mImageHandler", mImageHandler);
//	}
//
//	/**
//	 *
//	 * @param selectMap
//	 * @return canFinishs
//	 */
//	public boolean onSubmitSelect(LinkedHashMap<String, BitmapInfo> selectMap){
//		Intent intent = new Intent();
//		intent.putExtra(EXTRA_NAME_PHOTO_SELECT_LIST,new ArrayList<BitmapInfo>(selectMap.values()));
//		setResult(RESULT_OK, intent);
//		return true;
//	}
//
//	@Override
//	public void finish() {
//		if(isSubmit){
//			isSubmit = false;
//			LinkedHashMap<String, BitmapInfo> selectMap = mImageHandler.getSelectItemMap();
//			if(selectMap != null){
//				if(!onSubmitSelect(selectMap)){
//					return;
//				}
//			}
//		}else{
//		}
//		super.finish();
//	}
//
//	@Override
//	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//		super.onActivityResult(requestCode, resultCode, data);
//		LinkedHashMap<String, BitmapInfo> selectMap = null;
//		if(data != null){
//			if(requestCode == 10001){
//				selectMap = Util.castLinkedHashMap(data.getSerializableExtra(PhotoSelectActivity.EXTRA_NAME_PHOTO_SELECT_LIST));
//			}
//			if(selectMap != null){
//				mImageHandler.setSelectMap(selectMap);
//				setBtnState();
//				notifyDataSetChanged();
//			}
//		}
//		if(resultCode == Activity.RESULT_OK){
//			isSubmit = true;
//			finish();
//		}
//	}
//
//	@Override
//	public void onSelectDir(ArrayList<PhotoDirInfo> dirList, int position) {
//		PhotoDirInfo photoDirInfo = dirList.get(position);
//		if(mImageHandler.hadCloudPhoto() && position == 0){
//			showView(VIEW_TYPE_NET);
//		}else{
//			mSelectDirName = photoDirInfo.getFolderName();
//			mSelectDirList = mPhotoAlbumMap.get(photoDirInfo.getFolderPath());
//			showView(VIEW_TYPE_LOCAL);
//		}
//	}
//}
