package com.bk.android.time.ui.photo;

import android.content.Context;
import android.text.TextUtils;

import com.bk.android.time.app.App;
import com.bk.android.time.data.RecordFlieNetUrlData;
import com.bk.android.time.model.lightweight.AddImgModel.BitmapInfo;
import com.bk.android.time.widget.ImageLoader;
import com.bk.android.util.FileUtil;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

public class ImageHandler implements Serializable {
	public static final int DATA_TYPE_IMG = 0;
	public static final int DATA_TYPE_VIDEO = 1;
	public static final int DATA_TYPE_IMG_VIDEO = 2;

	private static final long serialVersionUID = 6615162825841441919L;
	private transient LinkedHashMap<String, BitmapInfo> mSelectMap = new LinkedHashMap<String, BitmapInfo>();
	private int mPhotoNumberLimit = 0;
	private boolean hadCamera;
	private boolean hadCloudPhoto;
	private boolean isDirMain;
	private transient ArrayList<SelectLinstener> mSelectLinsteners;
	private boolean mCanPreviewSelect = true;
	private boolean mCanSelectAll;
	private boolean isGotoCamera;
	private boolean isSelectPrint;
	private String mLastImagePath;
	private int mDataType;
	private boolean isRelease = true;
	
	public ImageHandler(int photoNumberLimit,boolean hadCamera,boolean hadCloudPhoto){
		mPhotoNumberLimit = photoNumberLimit;
		this.hadCamera = hadCamera;
		this.hadCloudPhoto = hadCloudPhoto;
		isDirMain = hadCloudPhoto;
	}
	
	public void setSelectMap(LinkedHashMap<String, BitmapInfo> selectMap){
		mSelectMap = selectMap;
	}
	
	private LinkedHashMap<String, BitmapInfo> getSelectMap(){
		if(mSelectMap == null){
			mSelectMap = new LinkedHashMap<String, BitmapInfo>();
		}
		return mSelectMap;
	}
	
	private ArrayList<SelectLinstener> getSelectLinsteners(){
		if(mSelectLinsteners == null){
			mSelectLinsteners = new ArrayList<SelectLinstener>();
		}
		return mSelectLinsteners;
	}
	
	private Context getContext(){
		return App.getInstance();
	}
	
	public void setLastImagePath(String lastImagePath){
		mLastImagePath = lastImagePath;
	}
	
	public String getLastImagePath(){
		return mLastImagePath;
	}
	
	public String getLastImagePathDir(){
		if(!TextUtils.isEmpty(mLastImagePath) && FileUtil.isFileExists(mLastImagePath)){
			return new File(mLastImagePath).getParent();
		}
		return null;
	}
	
	public int getDataType() {
		return mDataType;
	}
	
	public void setDataType(int dataType) {
		this.mDataType = dataType;
	}
	
	public void setRelease(boolean isRelease) {
		this.isRelease = isRelease;
	}

	public boolean isRelease() {
		return isRelease;
	}
	
	public void setSelectPrint(boolean selectPrint){
		isSelectPrint = selectPrint;
	}
	
	public boolean isSelectPrint(){
		return isSelectPrint;
	}
	
	public boolean isPixelNotEnough(int w, int h){
		if(!isSelectPrint || w == 0 || h == 0){
			return false;
		}
		return isPixelNotEnoughNotCheck(w, h);
	}
	
	public static boolean isPixelNotEnoughNotCheck(int w, int h){
		if(w < h){
			return w < 480 || h < 480;
		}else{
			return w < 480 || h < 480;
		}
	}
	
	public boolean isDirMain() {
		return isDirMain;
	}
	
	public void setDirMain(boolean isDirMain) {
		this.isDirMain = isDirMain;
	}

	public boolean hadCloudPhoto() {
		return hadCloudPhoto;
	}

	public void setCloudPhoto(boolean hadCloudPhoto) {
		this.hadCloudPhoto = hadCloudPhoto;
	}

	public boolean isGotoCamera() {
		return isGotoCamera;
	}

	public void setIsGotoCamera(boolean isGotoCamera) {
		this.isGotoCamera = isGotoCamera;
	}

	//出生、满月、2个月、3个月、100天、4个月…
	public RemarkInfo getDateRemark(long relativeTime,long preRelativeTime) {
		return null;
	}
	
	public String getAge(long relativeTime) {
		return null;
	}
	
	public boolean canPreviewSelect() {
		return mCanPreviewSelect;
	}
	
	public void setCanPreviewSelect(boolean canPreviewSelect){
		mCanPreviewSelect = canPreviewSelect;
	}

	public void onSwitchSelectAll(ArrayList<BitmapInfo> bitmapInfos) {
		if(bitmapInfos == null){
			return;
		}
		if(isSelectAll(bitmapInfos.size())){
			ArrayList<BitmapInfo> selectList = getSelectItems();
			for (BitmapInfo bitmapInfo : selectList) {
				if(hasSelect(bitmapInfo)){
					onHandSelect(bitmapInfo);
				}
			}
		}else{
			handSelectAll(bitmapInfos);
		}
	}
	
	public void handSelectAll(ArrayList<BitmapInfo> bitmapInfos) {
		for (BitmapInfo bitmapInfo : bitmapInfos) {
			if(!hasSelect(bitmapInfo) && !isPixelNotEnough(bitmapInfo.mWidth, bitmapInfo.mHeight)){
				onHandSelect(bitmapInfo);
			}
			if(isSelectAll(bitmapInfos.size())){
				break;
			}
		}
	}
	
	public boolean canDefaultSelectAll(){
		return false;
	}
	
	public boolean isSelectAll(int listSize){
		return getSelectMap().size() == mPhotoNumberLimit || getSelectMap().size() == listSize;
	}
	
	public boolean canSelectAll() {
		return mCanSelectAll;
	}
	
	public void setCanSelectAll(boolean canSelectAll){
		mCanSelectAll = canSelectAll;
	}
	
	public boolean canSelectAllByDay() {
		return mPhotoNumberLimit >= 20;
	}
	
	public boolean isUpload(String path) {
		return false;
	}

	public static boolean isFilter(String path, int width, int height) {
		return (width <= 0 && height <= 0) || (width > 200 && height > 200);
	}

	public int getUploadTipRes() {
		return 0;
	}

	public void onCheckUploadImg(BitmapInfo bitmapInfo) {
	}
	
	public int getPhotoNumberLimit() {
		return mPhotoNumberLimit;
	}

	public boolean hadCamera() {
		return hadCamera;
	}

	public boolean hasSelect(BitmapInfo bitmapInfo) {
		if(bitmapInfo == null){
			return false;
		}
		return getSelectMap().containsKey(bitmapInfo.mId);
	}
	
	public boolean onHandSelect(BitmapInfo bitmapInfo) {
		if(bitmapInfo == null){
			return false;
		}
		if(bitmapInfo.isVideo() && bitmapInfo.mDuration > 121000){
			return false;
		}
		if(bitmapInfo.mPath.startsWith("http://")){
			String path = ImageLoader.getInstance().urlToPath(bitmapInfo.mPath);
			if(!FileUtil.isFileExists(path)){
				return false;
			}
		}
		if(!hasSelect(bitmapInfo)) {
			if(mPhotoNumberLimit < 0 || getSelectMap().size() < mPhotoNumberLimit) {
				getSelectMap().put(bitmapInfo.mId,bitmapInfo);
				if(bitmapInfo.isUpload){
					onCheckUploadImg(bitmapInfo);
				}
			} else {
			}
		} else {
			getSelectMap().remove(bitmapInfo.mId);
		}
		for (SelectLinstener selectLinstener : getSelectLinsteners()) {
			selectLinstener.onCheckedChanged(getSelectMap().size());
		}
		return hasSelect(bitmapInfo);
	}

	public void setSelectLinstener(SelectLinstener l){
		if(!getSelectLinsteners().contains(l)){
			getSelectLinsteners().add(l);
		}
	}
	
	public interface SelectLinstener {
		public void onCheckedChanged(int number);
	}
	
	public LinkedHashMap<String, BitmapInfo> getSelectItemMap(){
		return getSelectMap();
	}
	
	public ArrayList<String> getSelectItemPaths(){
		ArrayList<String> list = new ArrayList<String>();
		for(Iterator<Map.Entry<String, BitmapInfo>> it = getSelectMap().entrySet().iterator(); it.hasNext();){
			Map.Entry<String, BitmapInfo> entry = it.next();
			list.add(entry.getValue().mPath);
		}
		return list;
	}
	
	public ArrayList<BitmapInfo> getSelectItems(){
		ArrayList<BitmapInfo> list = new ArrayList<BitmapInfo>();
		for(Iterator<Map.Entry<String, BitmapInfo>> it = getSelectMap().entrySet().iterator(); it.hasNext();){
			Map.Entry<String, BitmapInfo> entry = it.next();
			list.add(entry.getValue());
		}
		return list;
	}
	
	private void writeObject(java.io.ObjectOutputStream out) throws IOException {
		out.defaultWriteObject();  //序列化所有非transient字段,必须是该方法的第一个操作 
		out.writeObject(Util.castEntryList(getSelectMap())); //序列化transient字段 
	}

	private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {
		in.defaultReadObject();             //反序列化所有非transient字段,必须是该方法的第一个操作 
		setSelectMap(Util.castLinkedHashMap(in.readObject()));//反序列化transient字段 
	}
	
	public static ImageHandler create(int photoNumberLimit,boolean hadCamera){
		return create(photoNumberLimit, hadCamera,false,false);
	}
	
	public static ImageHandler create(int photoNumberLimit,boolean hadCamera,boolean needCheckRecordUpload){
		return create(photoNumberLimit, hadCamera,needCheckRecordUpload,false);
	}
	
	public static ImageHandler create(int photoNumberLimit,boolean hadCamera,boolean needCheckRecordUpload,boolean hadCloudPhoto){
		if(!needCheckRecordUpload){
			return new ImageHandler(photoNumberLimit, hadCamera,hadCloudPhoto);
		}
		return new ImageRecordUploadHandler(photoNumberLimit, hadCamera, hadCloudPhoto);
	}
	
	public static class ImageRecordUploadHandler extends ImageHandler{
		private static final long serialVersionUID = -7692875961138476515L;

		public ImageRecordUploadHandler(int photoNumberLimit,boolean hadCamera, boolean hadCloudPhoto) {
			super(photoNumberLimit, hadCamera, hadCloudPhoto);
		}
		
		@Override
		public boolean isUpload(String path) {
			String netUrl = RecordFlieNetUrlData.getImgNetUrl(path);
			return !TextUtils.isEmpty(netUrl);
		}

		@Override
		public int getUploadTipRes() {
			return 0;
		}

		@Override
		public void onCheckUploadImg(BitmapInfo bitmapInfo) {
		}
	}
	
	public class RemarkInfo{
		private String mContent;
		private boolean isMustShow;
		public RemarkInfo(String content, boolean mustShow) {
			this.mContent = content;
			this.isMustShow = mustShow;
		}
		
		public String getContent() {
			return mContent;
		}
		
		public boolean isMustShow() {
			return isMustShow;
		}
	}
}
