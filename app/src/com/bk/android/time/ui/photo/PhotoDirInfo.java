package com.bk.android.time.ui.photo;

public class PhotoDirInfo {
	private String topImagePath;
	private String folderName;
	private String folderPath;
	private int imageCounts;
	private int videoCounts;
	private boolean isTopBoundary;

	public String getTopImagePath() {
		return topImagePath;
	}

	public void setTopImagePath(String topImagePath) {
		this.topImagePath = topImagePath;
	}
	
	public String getFolderPath() {
		return folderPath;
	}

	public void setFolderPath(String folderPath) {
		this.folderPath = folderPath;
	}

	public String getFolderName() {
		return folderName;
	}

	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}

	public int getImageCounts() {
		return imageCounts;
	}

	public void setImageCounts(int imageCounts) {
		this.imageCounts = imageCounts;
	}
	
	public int getVideoCounts() {
		return videoCounts;
	}

	public void setVideoCounts(int videoCounts) {
		this.videoCounts = videoCounts;
	}
	
	public void setTopBoundary(boolean isTopBoundary) {
		this.isTopBoundary = isTopBoundary;
	}

	public boolean isTopBoundary() {
		return isTopBoundary;
	}
}
