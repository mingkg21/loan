package com.bk.android.time.ui.photo;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

import android.text.TextUtils;

import com.bk.android.os.AbsThreadPool;
import com.bk.android.time.model.lightweight.AddImgModel.BitmapInfo;
import com.bk.android.time.util.FaceDetectUtil;
import com.bk.android.util.LogUtil;

public class PhotoDetect {
	public static final String FOLDER_NAME_ALL = "所有图片";
	private static boolean isInit;
	private long originalTimeMillis;

	private HashMap<Integer, ArrayList<BitmapInfo>> mDayMap = new HashMap<Integer, ArrayList<BitmapInfo>>();
	private HashMap<String, ArrayList<BitmapInfo>> mMapMap = new HashMap<String, ArrayList<BitmapInfo>>();
	private HashMap<Integer, ArrayList<BitmapInfo>> mMaxMapDayMap = new HashMap<Integer, ArrayList<BitmapInfo>>();
	private static ArrayList<BitmapInfo> mMaxMapList;
	private SimpleDateFormat mDaySdf = new SimpleDateFormat("yyyyMMdd");

	private static PhotoDetect mInstance;

	private ThreadPool mThreadPool;
	private int mDetectNum;
	private Integer mDetectDoneNum = 0;
	private Long mDetectTime = 0L;

	private PhotoDetect() {
		Calendar cal = Calendar.getInstance();
		cal.set(1970, Calendar.JANUARY, 1);
		originalTimeMillis = cal.getTimeInMillis();
		mThreadPool = new ThreadPool();
	}

	public static PhotoDetect getInstance() {
		if (mInstance == null) {
			mInstance = new PhotoDetect();
		}
		return mInstance;
	}

	private static class ThreadPool extends AbsThreadPool {
		private ThreadPool(){}

		@Override
		protected int getCorePoolSize() {
			return 3;
		}

		@Override
		protected int getMaximumPoolSize() {
			return 10;
		}

		@Override
		protected long getKeepAliveTime() {
			return 1;
		}

		@Override
		protected TimeUnit getTimeUnit() {
			return TimeUnit.SECONDS;
		}

		@Override
		protected BlockingQueue<Runnable> newQueue() {
			return new LinkedBlockingQueue<Runnable>();
		}

		@Override
		protected ThreadFactory newThreadFactory() {
			return new DefaultThreadFactory(Thread.NORM_PRIORITY);
		}
	}

	public void detect(File file, final BitmapInfo bitmapInfo) {
		if (isInit) {
			return;
		}
		ArrayList<BitmapInfo> imgList = null;
		if(file.length() > 102400 && file.getParentFile().getName().equals("Camera")){//照片文件要大于100K，而且是相机相册目录下
			if (bitmapInfo.mWidth >= 200 && bitmapInfo.mHeight >= 200) {//照片尺寸要大于等于200*200
				int dayKey = (int)((bitmapInfo.mLastModified - originalTimeMillis) / (3600000 * 24));
				imgList = mDayMap.get(dayKey);
				if (imgList == null) {
					imgList = new ArrayList<BitmapInfo>();
				}
				imgList.add(bitmapInfo);
				if (!mDayMap.containsKey(dayKey)) {
					mDayMap.put(dayKey, imgList);
				}
//				LogUtil.v("PhotoLoadAsyncTask", "path: " + bitmapInfo.mPath);

				mDetectNum++;
				Runnable runnable = new Runnable() {
					@Override
					public void run() {
						synchronized (mDetectTime) {
							if (mDetectTime == 0) {
								mDetectTime = System.currentTimeMillis();
							}
						}
						LogUtil.v("PhotoLoadAsyncTask", "path: " + bitmapInfo.mPath);
						long time = System.currentTimeMillis();
						LogUtil.v("PhotoLoadAsyncTask", "time: " + mDaySdf.format(new Date(bitmapInfo.mLastModified)));
						if (FaceDetectUtil.detectFace(bitmapInfo.mPath)) {
							LogUtil.v("PhotoLoadAsyncTask", "have face");
						} else {
							LogUtil.v("PhotoLoadAsyncTask", "no face");
						}
						time = (System.currentTimeMillis() - time);
						LogUtil.v("PhotoLoadAsyncTask", "total time: " + time);
						LogUtil.v("PhotoLoadAsyncTask", "current thread: " + Thread.currentThread());
						LogUtil.v("PhotoLoadAsyncTask", "===========================================================");
						synchronized (mDetectDoneNum) {
							mDetectDoneNum++;
							if (mDetectDoneNum == mDetectNum) {
								LogUtil.v("PhotoLoadAsyncTask", "detect face done, detect number: " + mDetectDoneNum + " space time : " + (System.currentTimeMillis() - mDetectTime));
							}
						}
					}
				};
				mThreadPool.addTask(runnable);

				String longitude = bitmapInfo.mLongitude[0];
				String latitude = bitmapInfo.mLongitude[1];
				LogUtil.v("PhotoLoadAsyncTask", "LONGITUDE: " + longitude + " LATITUDE: " + latitude);
				if (!TextUtils.isEmpty(longitude) && !TextUtils.isEmpty(latitude)
						&& !"0".equals(longitude) && !"0".equals(latitude)) {
					int index = longitude.indexOf(".");
					if (index > -1 && (index + 2 <= longitude.length() - 1)) {//longitude经度取小数点后一位
						longitude = longitude.substring(0, index + 2);
					}
					index = latitude.indexOf(".");
					if (index > -1 && (index + 3 <= latitude.length() - 1)) {//latitude纬度取小数点后两位
						latitude = latitude.substring(0, index + 3);
					}
					LogUtil.v("PhotoLoadAsyncTask", "after LONGITUDE: " + longitude + " LATITUDE: " + latitude);
					String mapKey = longitude + "_" + latitude;

					imgList = mMapMap.get(mapKey);
					if (imgList == null) {
						imgList = new ArrayList<BitmapInfo>();
					}
					imgList.add(bitmapInfo);
					if (!mMapMap.containsKey(mapKey)) {
						mMapMap.put(mapKey, imgList);
					}
				}
			}
		}
	}

	public long getDetectTime() {
		return mDetectTime;
	}

	public void initData(File file, BitmapInfo bitmapInfo) {
		if (isInit) {
			return;
		}
		ArrayList<BitmapInfo> imgList = null;
		if(file.length() > 102400 && file.getParentFile().getName().equals("Camera")){//照片文件要大于100K，而且是相机相册目录下
			if (bitmapInfo.mWidth >= 200 && bitmapInfo.mHeight >= 200) {//照片尺寸要大于等于200*200
				int dayKey = (int)((bitmapInfo.mLastModified - originalTimeMillis) / (3600000 * 24));
				imgList = mDayMap.get(dayKey);
				if (imgList == null) {
					imgList = new ArrayList<BitmapInfo>();
				}
				imgList.add(bitmapInfo);
				if (!mDayMap.containsKey(dayKey)) {
					mDayMap.put(dayKey, imgList);
				}
				LogUtil.v("PhotoLoadAsyncTask", "path: " + bitmapInfo.mPath);

				String longitude = bitmapInfo.mLongitude[0];
				String latitude = bitmapInfo.mLongitude[1];
				LogUtil.v("PhotoLoadAsyncTask", "LONGITUDE: " + longitude + " LATITUDE: " + latitude);
				if (!TextUtils.isEmpty(longitude) && !TextUtils.isEmpty(latitude)
						&& !"0".equals(longitude) && !"0".equals(latitude)) {
					int index = longitude.indexOf(".");
					if (index > -1 && (index + 2 <= longitude.length() - 1)) {//longitude经度取小数点后一位
						longitude = longitude.substring(0, index + 2);
					}
					index = latitude.indexOf(".");
					if (index > -1 && (index + 3 <= latitude.length() - 1)) {//latitude纬度取小数点后两位
						latitude = latitude.substring(0, index + 3);
					}
					LogUtil.v("PhotoLoadAsyncTask", "after LONGITUDE: " + longitude + " LATITUDE: " + latitude);
					String mapKey = longitude + "_" + latitude;

					imgList = mMapMap.get(mapKey);
					if (imgList == null) {
						imgList = new ArrayList<BitmapInfo>();
					}
					imgList.add(bitmapInfo);
					if (!mMapMap.containsKey(mapKey)) {
						mMapMap.put(mapKey, imgList);
					}
				}
			}
		}
	}

	public void organize() {
		if (isInit) {
			return;
		}
//		ArrayList<Integer> keys = new ArrayList<>(mDayMap.keySet());
//		Collections.sort(keys, new Comparator<Integer>() {
//			@Override
//			public int compare(Integer lhs, Integer rhs) {
//				return lhs > rhs ? 1 : -1;
//			}
//		});
//		for (Integer key : keys) {
//			LogUtil.v("PhotoLoadAsyncTask", "key: " + key + "; size: " + mDayMap.get(key).size());
//		}

		int maxSize = 0;
		String maxSizeKey = null;
		for (String key : mMapMap.keySet()) {
			LogUtil.v("PhotoLoadAsyncTask", "map key: " + key + "; size: " + mMapMap.get(key).size());
			int size = mMapMap.get(key).size();
			if (size > maxSize) {
				maxSize = size;
				maxSizeKey = key;
			}
			for (BitmapInfo bitmapInfo : mMapMap.get(key)) {
				LogUtil.v("PhotoLoadAsyncTask", "map: " + bitmapInfo.mPath);
			}
		}
		if (!TextUtils.isEmpty(maxSizeKey)) {
			mMaxMapList = mMapMap.get(maxSizeKey);
			Collections.sort(mMaxMapList, new Comparator<BitmapInfo>() {
				@Override
				public int compare(BitmapInfo lhs, BitmapInfo rhs) {
					return lhs.mLastModified > rhs.mLastModified ? 1 : -1;
				}
			});
			LogUtil.v("PhotoLoadAsyncTask", "max map key: " + maxSizeKey + "; size: " + mMapMap.get(maxSizeKey).size());

			ArrayList<BitmapInfo> imgList = null;
			for (BitmapInfo bitmapInfo : mMapMap.get(maxSizeKey)) {
				LogUtil.v("PhotoLoadAsyncTask", "max map path: " + bitmapInfo.mPath + "; time: " + new SimpleDateFormat("yyyy-MM-dd mm:ss").format(new Date(bitmapInfo.mLastModified)));

//				int dayKey = Integer.valueOf(mDaySdf.format(new Date(bitmapInfo.mLastModified)));
//				int dayKey = (int)((bitmapInfo.mLastModified - originalTimeMillis) / (3600000 * 24));
				int dayKey = getDayKey(bitmapInfo.mLastModified);
				imgList = mMaxMapDayMap.get(dayKey);
				if (imgList == null) {
					imgList = new ArrayList<BitmapInfo>();
				}
				imgList.add(bitmapInfo);
				if (!mMaxMapDayMap.containsKey(dayKey)) {
					mMaxMapDayMap.put(dayKey, imgList);
				}

//				int monthKey = Integer.valueOf(mMonthSdf.format(new Date(bitmapInfo.mLastModified)));
//				imgList = mMaxMapMonthMap.get(dayKey);
//				if (imgList == null) {
//					imgList = new ArrayList<BitmapInfo>();
//				}
//				imgList.add(bitmapInfo);
//				if (!mMaxMapMonthMap.containsKey(dayKey)) {
//					mMaxMapMonthMap.put(dayKey, imgList);
//				}
			}
		}
		isInit = true;

	}

	private int getDayKey(long millisecond) {
		int dayKey = 0;
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(millisecond);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return (int)((cal.getTimeInMillis() - originalTimeMillis) / (3600000 * 24));
	}

	public HashMap<Integer, ArrayList<BitmapInfo>> getImageList(long birthday) {
		if (mMaxMapList == null || mMaxMapList.isEmpty()) {
			return null;
		}
		//宝宝年龄（0-4个月）----新生儿纪念册
		//（优先分析第一个月）
		//（宝宝当前年龄下（没有超过10天的照片），支持展现1天多页最多不超过4页）
		//当用户已存在超过10天的照片，每天一页
		//
		//宝宝年龄（大于4个月小于1岁），每天一页----成长轨迹纪念册
		//（100天内每10天，100天后，按每月）优先分析100天内
		//
		//宝宝年龄（大于1岁----生日集纪念册
		//展现宝宝每年的生日纪念册集合（1个生日最多显示10页）优先分析距离当前最近的宝宝生日照片

		HashMap<Integer, ArrayList<BitmapInfo>> map = new HashMap<Integer, ArrayList<BitmapInfo>>();

		int days = (int)((System.currentTimeMillis() - birthday) / (3600000 * 24));
		int page = 1;

		int birthdayDay = (int)((birthday - originalTimeMillis) / (3600000 * 24));

		ArrayList<Integer> dayKeys = new ArrayList<Integer>(mMaxMapDayMap.keySet());
		Collections.sort(dayKeys, new Comparator<Integer>() {
			@Override
			public int compare(Integer lhs, Integer rhs) {
				return lhs > rhs ? 1 : -1;
			}
		});

//		if (days <= 120) {
//			for (Integer dayKey : dayKeys) {
//				if (dayKey >= birthdayDay && page <= 10) {
//					map.put(page, mMaxMapDayMap.get(dayKey));
//					page++;
//					if (page > 10) {
//						break;
//					}
//				}
//			}
//		} else if (days < 365) {
//			for (Integer dayKey : dayKeys) {
//				if (dayKey >= birthdayDay && page <= 10) {
//					if ((dayKey - birthdayDay <= 100) && (dayKey - birthdayDay) % 10 == 0) {
//						map.put(page, mMaxMapDayMap.get(dayKey));
//						page++;
//						if (page > 10) {
//							break;
//						}
//					} else {
//						int months = getMonths(birthday, dayKey);
//						if (months > 3) {
//							map.put(page, mMaxMapDayMap.get(dayKey));
//							page++;
//							if (page > 10) {
//								break;
//							}
//						}
//					}
//				}
//			}
//		} else {
//			int birthdayDays = Integer.valueOf(mDaySdf.format(new Date(birthday)));
//			int years = (Integer.valueOf(mDaySdf.format(new Date(System.currentTimeMillis())))
//					- birthdayDays) / 10000;
//			if (years > 0) {
//				for (int i = years; i > 0; i--) {
//					int y = birthdayDays + i * 10000;
//					try {
//						Date date = mDaySdf.parse(String.valueOf(y));
//						int dayKey = (int)((date.getTime() - originalTimeMillis) / (3600000 * 24));
//						if (mMaxMapDayMap.containsKey(dayKey)) {
//							mMaxMapDayMap.get(dayKey);
//							break;
//						}
//					} catch (ParseException e) {
//						e.printStackTrace();
//					}
//				}
//			}
//
//		}
		if (days >= 365) {
			int birthdayDays = Integer.valueOf(mDaySdf.format(new Date(birthday)));
			int years = (Integer.valueOf(mDaySdf.format(new Date(System.currentTimeMillis())))
					- birthdayDays) / 10000;
			if (years > 0) {
				for (int i = years; i > 0; i--) {
					int y = birthdayDays + i * 10000;
					try {
						Date date = mDaySdf.parse(String.valueOf(y));
						int dayKey = (int)((date.getTime() - originalTimeMillis) / (3600000 * 24));
						if (mMaxMapDayMap.containsKey(dayKey)) {
							if (mMaxMapDayMap.get(dayKey).size() > 5) {
								map.put(page, mMaxMapDayMap.get(dayKey));
								break;
							}
						}
					} catch (ParseException e) {
						e.printStackTrace();
					}
				}
			}
		}
		if (map.size() == 0) {
			for (Integer dayKey : dayKeys) {
				if (dayKey >= birthdayDay && page <= 10) {
					map.put(page, mMaxMapDayMap.get(dayKey));
					page++;
					if (page > 10) {
						break;
					}
				}
			}
		}
		return map;
	}

	public void test() {
		try {
			String birthday = "20120229";
			Date brithdayTimeMilis = mDaySdf.parse(birthday);

			String day = "20120315";
			Date relativeDayTimeMilis = mDaySdf.parse(day);
			LogUtil.v("PhotoLoadAsyncTask", "birdthday: " + birthday + " day: " + day + " ; getDifference: " + getDifference(brithdayTimeMilis, relativeDayTimeMilis));
			day = "20120328";
			relativeDayTimeMilis = mDaySdf.parse(day);
			LogUtil.v("PhotoLoadAsyncTask", "birdthday: " + birthday + " day: " + day + " ; getDifference: " + getDifference(brithdayTimeMilis, relativeDayTimeMilis));
			day = "20120329";
			relativeDayTimeMilis = mDaySdf.parse(day);
			LogUtil.v("PhotoLoadAsyncTask", "birdthday: " + birthday + " day: " + day + " ; getDifference: " + getDifference(brithdayTimeMilis, relativeDayTimeMilis));
			day = "20120628";
			relativeDayTimeMilis = mDaySdf.parse(day);
			LogUtil.v("PhotoLoadAsyncTask", "birdthday: " + birthday + " day: " + day + " ; getDifference: " + getDifference(brithdayTimeMilis, relativeDayTimeMilis));
			day = "20120629";
			relativeDayTimeMilis = mDaySdf.parse(day);
			LogUtil.v("PhotoLoadAsyncTask", "birdthday: " + birthday + " day: " + day + " ; getDifference: " + getDifference(brithdayTimeMilis, relativeDayTimeMilis));

		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	private int getMonths(long birthday, int relativeDay) {
		try {
			Date brithdayDate = new Date(birthday);
			Date relativeDayDate = mDaySdf.parse(String.valueOf(relativeDay));
			return getDifference(brithdayDate, relativeDayDate);

		} catch (ParseException e) {
			e.printStackTrace();
		}
		return -1;
	}

	private int getDifference(Date birthday, Date relativeDay) {
		Calendar birthdayCal = Calendar.getInstance();
		birthdayCal.setTime(birthday);

		Calendar relativeDayyCal = Calendar.getInstance();
		relativeDayyCal.setTime(relativeDay);

		int birthdayYear = birthdayCal.get(Calendar.YEAR);
		int birthdayMonth = birthdayCal.get(Calendar.MONTH) + 1;
		int birthdayDay = birthdayCal.get(Calendar.DAY_OF_MONTH);

		int relativeDayYear = relativeDayyCal.get(Calendar.YEAR);
		int relativeDayMonth = relativeDayyCal.get(Calendar.MONTH) + 1;
		int relativeDayDay = relativeDayyCal.get(Calendar.DAY_OF_MONTH);

		int years = relativeDayYear - birthdayYear;
		int months = relativeDayMonth - birthdayMonth;
		int days = relativeDayDay - birthdayDay;
		days += 1;

		if(days < 0) {
			relativeDayyCal.set(Calendar.DATE, 1);
			relativeDayyCal.add(Calendar.DATE, -1);
			days += relativeDayyCal.get(Calendar.DATE);
			months--;
		}
		if(months < 0) {
			months += 12;
			years--;
		}

		if (years == 0) {
			if (days == 1) {
				if (months > 0) {
					return months;
				}
			}
		}

		return -1;

	}

}
