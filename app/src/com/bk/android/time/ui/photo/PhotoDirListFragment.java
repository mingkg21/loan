package com.bk.android.time.ui.photo;

import java.util.ArrayList;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bk.android.assistant.R;
import com.bk.android.time.ui.BaseFragment;
import com.bk.android.time.ui.photo.PhotoDirListViewModel.OnSelectDirListener;

public class PhotoDirListFragment extends BaseFragment implements OnSelectDirListener {
	private PhotoDirListViewModel mPhotoDirListViewModel;
	private ArrayList<PhotoDirInfo> mDirList;
	private OnSelectDirListener mOnSelectDirListener;
	
	@Override
	public View onCreateContentView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState) {
		mPhotoDirListViewModel = new PhotoDirListViewModel(getActivity(), this, mDirList, this);
		View contentView = bindView(R.layout.uniq_photo_dir_list_lay, null, mPhotoDirListViewModel);
		mPhotoDirListViewModel.onStart();
		return contentView;
	}

	@Override
	public void onDestroyContentView() {
		mPhotoDirListViewModel.onRelease();
	}
	
	public void init(ArrayList<PhotoDirInfo> dirList, OnSelectDirListener l){
		mDirList = dirList;
		mOnSelectDirListener = l;
	}

	@Override
	public void onSelectDir(ArrayList<PhotoDirInfo> dirList, int position) {
		if(mOnSelectDirListener != null){
			mOnSelectDirListener.onSelectDir(dirList, position);
		}
	}
}
