package com.bk.android.time.ui.photo;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;

import com.bk.android.assistant.R;
import com.bk.android.binding.command.OnItemClickCommand;
import com.bk.android.time.model.BaseDataViewModel;
import com.bk.android.time.ui.ILoadView;
import com.bk.android.util.BitmapUtil;

import java.util.ArrayList;

import gueei.binding.collections.ArrayListObservable;
import gueei.binding.observables.BooleanObservable;
import gueei.binding.observables.StringObservable;

public class PhotoDirListViewModel extends BaseDataViewModel {
	public final ArrayListObservable<ItemViewModel> bItems = new ArrayListObservable<ItemViewModel>(ItemViewModel.class);
	public final OnItemClickCommand bOnItemClickCommand = new OnItemClickCommand() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,long id) {
			mOnSelectDirListener.onSelectDir(mDirList, position);
		}
	};
	
	private OnSelectDirListener mOnSelectDirListener;
	private ArrayList<PhotoDirInfo> mDirList;
	
	public PhotoDirListViewModel(Context context, ILoadView loadView, ArrayList<PhotoDirInfo> dirList,OnSelectDirListener listener) {
		super(context, loadView);
		mDirList = dirList;
		mOnSelectDirListener = listener;
	}

	@Override
	public void onStart() {
		if(mDirList != null){
			ArrayList<ItemViewModel> items = new ArrayList<ItemViewModel>();
			for (PhotoDirInfo info : mDirList) {
				ItemViewModel item = new ItemViewModel();
				item.bIsShowLine.set(info.isTopBoundary());
				if(info.getTopImagePath() != null && TextUtils.isEmpty(Uri.parse(info.getTopImagePath()).getScheme())){
					item.bCoverUrl.set("file://" + info.getTopImagePath());
				}else{
					item.bCoverUrl.set(TextUtils.isEmpty(info.getTopImagePath()) ? BitmapUtil.getResourcesUri(R.drawable.default_icon_head) : info.getTopImagePath());
				}
				item.bName.set(info.getFolderName());
				if(info.getImageCounts() <= 0 && info.getVideoCounts() <= 0){
					item.bDetails.set(null);
				}else{
					String sizeInfo = "共";
//					if(info.getImageCounts() > 0){
//						sizeInfo += getContext().getString(R.string.record_phone_size,info.getImageCounts()) + (info.getVideoCounts() > 0 ? " , " : "");
//					}
//					if(info.getVideoCounts() > 0){
//						sizeInfo += getContext().getString(R.string.record_video_size,info.getVideoCounts());
//					}
					item.bDetails.set(sizeInfo);
				}
				items.add(item);
			}
			bItems.setAll(items);
		}
	}

	@Override
	public void onRelease() {}
	
	public class ItemViewModel{
		public final StringObservable bCoverUrl = new StringObservable();
		public final StringObservable bName = new StringObservable();
		public final StringObservable bDetails = new StringObservable();
		public final BooleanObservable bIsShowLine = new BooleanObservable(false);
	}
	
	public interface OnSelectDirListener{
		public void onSelectDir(ArrayList<PhotoDirInfo> dirList, int position);
	}
}
