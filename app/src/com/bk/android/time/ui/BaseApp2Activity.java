package com.bk.android.time.ui;

import com.bk.android.assistant.R;
import com.bk.android.time.app.AbsApp2Activity;

/**
 * Created by mingkg21 on 2016/12/14.
 */

public class BaseApp2Activity extends AbsApp2Activity {

    @Override
    protected int getStatusBarTintResource() {
        return R.color.com_color_1;
    }

}
