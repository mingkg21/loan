package com.bk.android.time.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bk.android.assistant.R;
import com.bk.android.time.model.lightweight.MyInfoViewModel;
import com.bk.android.time.ui.BaseFragment;
import com.bk.android.ui.widget.viewpager.ViewPagerItem;

/**
 * Created by mingkg21 on 2017/4/8.
 */

public class MyInfoFragment extends BaseFragment implements ViewPagerItem {

    private MyInfoViewModel mMyInfoViewModel;

    public MyInfoFragment(){}

    @Override
    public View onCreateContentView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mMyInfoViewModel = new MyInfoViewModel(getActivity(), this);
        View contentView = bindView(R.layout.uniq_my_info_lay, null, mMyInfoViewModel);
        setContentView(contentView);
        mMyInfoViewModel.onStart();
        return contentView;
    }

    @Override
    public void onDestroyContentView() {
    }

    @Override
    public void setLifeCycleEnabled(boolean enabled) {
    }

    @Override
    public boolean onCreateItem() {
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onResumeItem() {
    }

    @Override
    public void onPauseItem() {
    }

    @Override
    public void onDestroyItem() {
        mMyInfoViewModel.onRelease();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mMyInfoViewModel.onActivityResult(requestCode, resultCode, data);
    }
}