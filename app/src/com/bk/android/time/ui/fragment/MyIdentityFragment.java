package com.bk.android.time.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bk.android.assistant.R;
import com.bk.android.time.model.lightweight.MyIdentityViewModel;
import com.bk.android.time.ui.BaseFragment;
import com.bk.android.ui.widget.viewpager.ViewPagerItem;

/**
 * Created by mingkg21 on 2017/4/8.
 */

public class MyIdentityFragment extends BaseFragment implements ViewPagerItem {

    private MyIdentityViewModel mMyIdentityViewModel;

    public MyIdentityFragment(){}

    @Override
    public View onCreateContentView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mMyIdentityViewModel = new MyIdentityViewModel(getActivity(), this);
        View contentView = bindView(R.layout.uniq_my_identity_lay, null, mMyIdentityViewModel);
        setContentView(contentView);
        mMyIdentityViewModel.onStart();
        return contentView;
    }

    @Override
    public void onDestroyContentView() {
    }

    @Override
    public void setLifeCycleEnabled(boolean enabled) {
    }

    @Override
    public boolean onCreateItem() {
        return false;
    }

    @Override
    public void onResumeItem() {
    }

    @Override
    public void onPauseItem() {
    }

    @Override
    public void onDestroyItem() {
        mMyIdentityViewModel.onRelease();
    }

}