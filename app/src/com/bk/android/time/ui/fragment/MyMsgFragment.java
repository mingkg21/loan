package com.bk.android.time.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bk.android.assistant.R;
import com.bk.android.time.model.lightweight.MyMsgListViewModel;
import com.bk.android.time.ui.BaseFragment;
import com.bk.android.ui.widget.viewpager.ViewPagerItem;

/**
 * Created by mingkg21 on 2017/4/8.
 */

public class MyMsgFragment extends BaseFragment implements ViewPagerItem {

    private MyMsgListViewModel mMyMsgListViewModel;

    public MyMsgFragment(){}

    @Override
    public View onCreateContentView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mMyMsgListViewModel = new MyMsgListViewModel(getActivity(), this);
        View contentView = bindView(R.layout.com_list_lay, null, mMyMsgListViewModel);
        setContentView(contentView);
        mMyMsgListViewModel.onStart();
        return contentView;
    }

    @Override
    public void onDestroyContentView() {
    }

    @Override
    public void setLifeCycleEnabled(boolean enabled) {
    }

    @Override
    public boolean onCreateItem() {
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onResumeItem() {
    }

    @Override
    public void onPauseItem() {
    }

    @Override
    public void onDestroyItem() {
        mMyMsgListViewModel.onRelease();
    }

}