package com.bk.android.time.ui.fragment;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.JsResult;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bk.android.assistant.R;
import com.bk.android.time.app.App;
import com.bk.android.time.data.UserData;
import com.bk.android.time.data.net.WebConfig;
import com.bk.android.time.model.BaseDialogViewModel;
import com.bk.android.time.model.BaseDialogViewModel.OnBtnClickCallBack;
import com.bk.android.time.model.common.CommonDialogViewModel;
import com.bk.android.time.ui.BaseFragment;
import com.bk.android.time.ui.activiy.ActivityChannels;
import com.bk.android.time.ui.activiy.CommonWebActivity;
import com.bk.android.time.util.DialogUtil;
import com.bk.android.ui.widget.ScrollWebView;
import com.bk.android.util.ApnUtil;
import com.bk.android.util.AppUtil;
import com.zaaach.citypicker.CityPickerActivity;

import static android.app.Activity.RESULT_OK;

/** 通用的内嵌webview的Fragment
 * @author mingkg21
 * @email mingkg21@gmail.com
 * @date 2014年4月28日
 *
 */
public class CommonWebFragment extends BaseFragment {

	public static final String EXTRA_NAME_URL = "extra_name_url";
	public static final String EXTRA_NAME_HTML_DATA = "extra_name_html_data";

	private ValueCallback<Uri> mUploadMessage;
	private static final int REQUEST_CODE_FILECHOOSER = 1001;
	private static final int REQUEST_CODE_MAGAZINE_MAKE = 2001;
	private static final int REQUEST_CODE_ADDRESS_MAKE = 3001;

	protected ScrollWebView mWebView;
	private ScrollWebView.ScrollChangedListener mScrollChangedListener;
	private ProgressBar mWebProgress;
	private View mErrLay;
	private TextView mErrTv;
	private TextView mErrBtn;
	
	private String mCurrentUrl;
	private String mHtmlData;
	
	private boolean needSetWebTitel;
	
	private AdLayoutInterface mAdLayoutInterface;

	@Override
	public View onCreateContentView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View contentView = inflater.inflate(R.layout.uniq_common_web_lay, null);

		ViewGroup adLayout = (ViewGroup) contentView.findViewById(R.id.adLayout);
		if (mAdLayoutInterface != null) {
			if (mAdLayoutInterface.setAdLayout(adLayout)) {
				adLayout.setVisibility(View.VISIBLE);
			}
		}
		
		mWebProgress = (ProgressBar) contentView.findViewById(R.id.web_progress);
		mErrLay = contentView.findViewById(R.id.err_tip_lay);
		mErrTv = (TextView) contentView.findViewById(R.id.err_tip_tv);
		mErrBtn = (TextView) contentView.findViewById(R.id.err_tip_btn);
		mErrBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				loadData(mCurrentUrl, mHtmlData);
			}
		});
		
		mWebView = (ScrollWebView)contentView.findViewById(R.id.common_web_wv);

		mWebView.setScrollChangedListener(mScrollChangedListener);
		mWebView.getSettings().setSupportZoom(false);
		mWebView.getSettings().setNeedInitialFocus(false);
		mWebView.getSettings().setBuiltInZoomControls(false);
		mWebView.getSettings().setJavaScriptEnabled(true);
		mWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
//		mWebView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);//视频缓存有问题
		mWebView.getSettings().setAllowFileAccess(true);
		mWebView.getSettings().setDomStorageEnabled(true);
		if(Build.VERSION.SDK_INT >= 11) {
			if (Build.MANUFACTURER.equals("Xiaomi") && Build.MODEL.indexOf("HM") > -1) {
				mWebView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);//据说和AwContents: nativeOnDraw failed; clearing to background color.这个报错有关
			}
		}
		
		mWebView.setWebViewClient(new WebViewClient() {
			@Override
			public void onPageFinished(WebView view, String url) {
				super.onPageFinished(view, url);
				mWebProgress.setVisibility(View.INVISIBLE);
				try {
					mWebView.getSettings().setBlockNetworkImage(false);
				} catch (Exception e) {
				}
			}

			@Override
			public void onPageStarted(WebView view, String url, Bitmap favicon) {
				mCurrentUrl = url;
				super.onPageStarted(view, url, favicon);
				mWebProgress.setVisibility(View.VISIBLE);
				mWebProgress.setProgress(0);
				try {
					mWebView.getSettings().setBlockNetworkImage(true);
				} catch (Exception e) {
				}
			}

			@Override
			public void onReceivedError(WebView view, int errorCode,
										String description, final String failingUrl) {
				if (!isAdded()) {
					return;
				}
				mErrLay.setVisibility(View.VISIBLE);
				if (ApnUtil.isNetAvailable(getActivity())) {
					mErrTv.setText(R.string.tip_err_server);
				} else {
					mErrTv.setText(R.string.tip_on_net);
				}
				super.onReceivedError(view, errorCode, description, failingUrl);
			}

			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				if (!isAdded()) {
					return true;
				}
				if (url.equals(mCurrentUrl)) {
					return false;
				}
				if (url.indexOf("#openOut") > -1) {
					try {
						Intent intent = new Intent();
						intent.setAction(Intent.ACTION_VIEW);
						intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						Uri content_url = Uri.parse(url);
						intent.setData(content_url);
						getActivity().startActivity(intent);
						return true;
					} catch (Exception e) {
					}
				}
				CommonWebActivity.openActivity(getActivity(), url);
				return true;
			}
		});
		mWebView.setWebChromeClient(new WebChromeClient() {
			@Override
			public void onProgressChanged(WebView view, int newProgress) {
				super.onProgressChanged(view, newProgress);
				mWebProgress.setProgress(newProgress);
			}

			@Override
			public void onReceivedTitle(WebView view, String title) {
				super.onReceivedTitle(view, title);
				if (needSetWebTitel && getUserVisibleHint()) {
					setTitle(title);
				}
			}

			public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType) {
				mUploadMessage = uploadMsg;
				Intent i = new Intent(Intent.ACTION_GET_CONTENT);
				i.addCategory(Intent.CATEGORY_OPENABLE);
				i.setType("image/*");
				getActivity().startActivityForResult(
						Intent.createChooser(i, "File Chooser"),
						REQUEST_CODE_FILECHOOSER);
			}

			// For Android < 3.0
			public void openFileChooser(ValueCallback<Uri> uploadMsg) {
				mUploadMessage = uploadMsg;
				Intent i = new Intent(Intent.ACTION_GET_CONTENT);
				i.addCategory(Intent.CATEGORY_OPENABLE);
				i.setType("image/*");
				getActivity().startActivityForResult(
						Intent.createChooser(i, "File Chooser"),
						REQUEST_CODE_FILECHOOSER);
			}

			// For Android 4.1
			public void openFileChooser(ValueCallback uploadMsg, String acceptType, String capture) {
				mUploadMessage = uploadMsg;
				Intent i = new Intent(Intent.ACTION_GET_CONTENT);
				i.addCategory(Intent.CATEGORY_OPENABLE);
				i.setType("image/*");
				getActivity().startActivityForResult(
						Intent.createChooser(i, "File Chooser"),
						REQUEST_CODE_FILECHOOSER);

			}

			@Override
			public boolean onJsAlert(WebView view, String url, String message, final JsResult result) {
				if (getActivity() != null && !getActivity().isFinishing()) {
					CommonDialogViewModel dialogViewModel = DialogUtil.createConfirmDialogNotT(getActivity(), message, null);
					dialogViewModel.setLeftBtnClickCallBack(new OnBtnClickCallBack() {
						@Override
						public void onClick(View v, BaseDialogViewModel viewModel) {
							viewModel.finish();
							result.confirm();
						}
					});
					dialogViewModel.setRightBtnClickCallBack(null);
					dialogViewModel.setOnCancelListener(new OnCancelListener() {
						@Override
						public void onCancel(DialogInterface dialog) {
							result.cancel();
						}
					});
					dialogViewModel.show();
				} else {
					result.confirm();
				}
				return true;
			}
		});

		mWebView.addJavascriptInterface(new JSObject(), "client");

		mCurrentUrl = getArguments().getString(EXTRA_NAME_URL);
		mHtmlData = getArguments().getString(EXTRA_NAME_HTML_DATA);
		if(mCurrentUrl != null && mCurrentUrl.indexOf("banketime=out") > -1){
			try{
		    	Intent intent = new Intent();        
			    intent.setAction(Intent.ACTION_VIEW);
			    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			    Uri content_url = Uri.parse(mCurrentUrl);   
			    intent.setData(content_url);  
			    getActivity().startActivity(intent);
			    finish();
				return contentView;
		    }catch (Exception e) {}
		}
		loadData(mCurrentUrl, mHtmlData);

		return contentView;
	}

	public void setScrollChangedListener(ScrollWebView.ScrollChangedListener l){
		mScrollChangedListener = l;
		if (mWebView != null) {
			mWebView.setScrollChangedListener(l);
		}
	}

	public WebView getWebView() {
		return mWebView;
	}
	
	public void reLoad(){
		if(mWebView != null){
			mWebView.reload();
		}
	}

	public class JSObject {

		@android.webkit.JavascriptInterface
		public boolean isLogin() {
			return UserData.isLogin();
		}

		@android.webkit.JavascriptInterface
		public void login() {
			ActivityChannels.openLoginActivity(getContext());
		}

		@android.webkit.JavascriptInterface
		public void register() {
			ActivityChannels.openRegisterActivity(getContext());
		}

		@android.webkit.JavascriptInterface
		public String getUserToken() {
			return UserData.getToken();
		}

		@android.webkit.JavascriptInterface
		public void gotoCreditCheck() {
			ActivityChannels.openPersonalCreditReportyActivity(getContext());
		}

		@android.webkit.JavascriptInterface
		public void getLocation() {
			CityPickerActivity.openActivity((Activity) getContext());
		}

		@android.webkit.JavascriptInterface
		public String getClientVersion() {
			return "android:" + App.getInstance().getApkVersionCode();
		}

		@android.webkit.JavascriptInterface
		public String getMarketName() {
			return App.getInstance().getUMengChannel();
		}

		@android.webkit.JavascriptInterface
		public String getChannelId() {
			return AppUtil.getChannelId(App.getInstance());
		}

		@android.webkit.JavascriptInterface
		public String getPlatformName() {
			return "android";
		}

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUEST_CODE_FILECHOOSER) {
			if (null == mUploadMessage) {
				return;
			}
			Uri result = data == null || resultCode != RESULT_OK ? null: data.getData();
			mUploadMessage.onReceiveValue(result);
			mUploadMessage = null;
			return;
		}else if (requestCode == REQUEST_CODE_ADDRESS_MAKE) {
			loadData(mCurrentUrl, mHtmlData);
		} else if (requestCode == CityPickerActivity.REQUEST_CODE_PICK_CITY){
			if (resultCode == RESULT_OK && data != null){
				String city = data.getStringExtra(CityPickerActivity.KEY_PICKED_CITY);
				javascript("locationCity(\"" + city + "\")");
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	public void javascript(String javascript) {
		if (mWebView != null) {
			mWebView.loadUrl("javascript:" + javascript);
		}
	}

	@Override
	public boolean onBackPressed() {
		if(mWebView != null && mWebView.canGoBack()) {
			mWebView.goBack();
			return true;
		}
		return super.onBackPressed();
	}
	
	public void loadData(String url, String htmlData){
		mWebView.stopLoading();
		mWebView.clearView();
		if(url != null) {
//			mWebView.loadUrl(url, getHeaders());
			if (url.startsWith(WebConfig.getInstance().getHost())) {
				if (url.indexOf("?") > 0) {
					url += "&";
				} else {
					url += "?";
				}
				url += "channelId=" + AppUtil.getChannelId(App.getInstance());
			}
			mWebView.loadUrl(url);
		} else {
			mWebView.loadData(htmlData, "text/html; charset=UTF-8", null);//这种写法可以正确解码
		}
		mErrLay.setVisibility(View.GONE);
	}
	
	@Override
	public void onDestroyContentView() {
		destroy();
	}
	
	public String getCurrentUrl(){
		return mCurrentUrl;
	}

	public void setWebTitle(boolean b) {
		needSetWebTitel = b;
	}
	
	public void destroy() {
		mWebView.stopLoading();
		mWebView.removeAllViews();
		mWebView.setVisibility(View.GONE);
		mWebView.destroy();
	}

	public void clearView() {
		mWebView.stopLoading();  
		mWebView.loadData("<a></a>", "text/html", "utf-8"); 
	}
	
	public void setAdLayoutInterface(AdLayoutInterface adLayoutInterface) {
		mAdLayoutInterface = adLayoutInterface;
	}

	public interface AdLayoutInterface {
		public boolean setAdLayout(ViewGroup adLayout);
	}
}
