package com.bk.android.time.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bk.android.assistant.R;
import com.bk.android.time.model.lightweight.PersonalInfoViewModel;
import com.bk.android.time.ui.BaseFragment;
import com.bk.android.ui.widget.viewpager.ViewPagerItem;

/** 个人中心
 *
 */
public class PersonalInfoFragment extends BaseFragment implements ViewPagerItem{
	private PersonalInfoViewModel mPersonalInfoViewModel;

	@Override
	public View onCreateContentView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState) {
		return null;
	}

	@Override
	public void onDestroyContentView() {
	}

	@Override
	public boolean onCreateItem() {
		mPersonalInfoViewModel = new PersonalInfoViewModel(getActivity(),this);
		
		View contentView = bindView(R.layout.uniq_personal_info, null,mPersonalInfoViewModel);
		setContentView(contentView);

		mPersonalInfoViewModel.onStart();
		return false;
	}

	@Override
	public void resetTitleBar() {
		super.resetTitleBar();
		setTitleBarEnabled(false);
	}

	@Override
	public void onUserLoginStateChange(boolean isLogin) {
		if(getUserVisibleHint()){
			resetTitleBar();
		}
	}
	
	@Override
	public boolean onActivityResume() {
		return super.onActivityResume();
	}
	
	@Override
	public void onResumeItem() {
		mPersonalInfoViewModel.onResume();
		resetTitleBar();
	}
	
	@Override
	public void onPauseItem() {
		
	}

	@Override
	public void onDestroyItem() {
		mPersonalInfoViewModel.onRelease();
	}

	@Override
	public void onResume() {
		super.onResume();
		if(mPersonalInfoViewModel != null){
			mPersonalInfoViewModel.onStart();
		}
	}
	
	@Override
	public void setLifeCycleEnabled(boolean enabled) {
	}

}
