package com.bk.android.time.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bk.android.assistant.R;
import com.bk.android.time.model.lightweight.LoginViewModel;
import com.bk.android.time.ui.BaseFragment;
import com.bk.android.ui.widget.viewpager.ViewPagerItem;

/**
 * Created by mingkg21 on 2017/4/8.
 */

public class LoginFragment extends BaseFragment implements ViewPagerItem {

    private boolean mIsBySmsLogin;

    private LoginViewModel mLoginViewModel;

    public LoginFragment(){}

    public void setBySmsLogin(boolean bySmsLogin) {
        mIsBySmsLogin = bySmsLogin;
    }

    @Override
    public View onCreateContentView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mLoginViewModel = new LoginViewModel(getActivity(), this, false);
        mLoginViewModel.setBySMSLogin(mIsBySmsLogin);
        View contentView = bindView(R.layout.uniq_login_lay, null, mLoginViewModel);
        setContentView(contentView);
        mLoginViewModel.onStart();
        return contentView;
    }

    @Override
    public void onDestroyContentView() {
    }

    @Override
    public void setLifeCycleEnabled(boolean enabled) {
    }

    @Override
    public boolean onCreateItem() {
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onResumeItem() {
    }

    @Override
    public void onPauseItem() {
    }

    @Override
    public void onDestroyItem() {
        mLoginViewModel.onRelease();
    }

}