package com.bk.android.time.ui.fragment;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.bk.android.assistant.R;
import com.bk.android.time.data.UserData;
import com.bk.android.time.data.net.WebConfig;
import com.bk.android.time.ui.BaseFragment;
import com.bk.android.time.ui.activiy.ActivityChannels;
import com.bk.android.time.util.ToastUtil;
import com.bk.android.ui.widget.ScrollWebView;
import com.bk.android.ui.widget.viewpager.ViewPagerItem;
import com.zaaach.citypicker.CityPickerActivity;
import com.zaaach.citypicker.utils.StringUtils;

import static android.app.Activity.RESULT_OK;

public class CreditCardFragment extends BaseFragment implements ViewPagerItem {

	private TextView mCityTV;
	private CommonWebFragment mCommonWebFragment;
	private AMapLocationClient mLocationClient;

	@Override
	public View onCreateContentView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return null;
	}

	@Override
	public void onDestroyContentView() {
	}

	@Override
	public void setLifeCycleEnabled(boolean enabled) {
	}

	@Override
	public boolean onActivityResume() {
		return super.onActivityResume();
	}
	
	@Override
	public boolean onCreateItem() {
		View contentView = LayoutInflater.from(getContext()).inflate(R.layout.uniq_credit_card_layout, null);
		setContentView(contentView);

		final View titleBarView = contentView.findViewById(R.id.credit_card_titlebar);
		final ImageView cityIV = (ImageView) contentView.findViewById(R.id.credit_card_city_iv);
		mCityTV = (TextView) contentView.findViewById(R.id.credit_card_city_tv);
		final TextView recordView = (TextView) contentView.findViewById(R.id.credit_card_record_tv);

		contentView.findViewById(R.id.credit_card_city_lay).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				CityPickerActivity.openActivity((Activity) getContext());
			}
		});

		recordView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				checkLogin(new Runnable() {
					@Override
					public void run() {
						ActivityChannels.openCardProccessQueryActivity(getContext());
					}
				});
			}
		});

		titleBarView.setBackgroundDrawable(new ColorDrawable(Color.argb(0, 255, 255, 255)));

		int textColor = getResources().getColor(R.color.com_color_2);
		final int textColorR = Color.red(textColor);
		final int textColorG = Color.green(textColor);
		final int textColorB = Color.blue(textColor);

		mCommonWebFragment = new CommonWebFragment();
		Bundle args = new Bundle();
		args.putString(CommonWebFragment.EXTRA_NAME_URL, WebConfig.getInstance().getCreditCardUrl());
		mCommonWebFragment.setArguments(args);
		mCommonWebFragment.setScrollChangedListener(new ScrollWebView.ScrollChangedListener() {
			@Override
			public void onScrollChanged(WebView scrollView, int l, int t, int oldl, int oldt) {
				float progress = 0;
				int max = (int) (getResources().getDimensionPixelSize(R.dimen.head_bar_height) * 5);
				progress = Math.abs(t) * 1f / max;
				if(progress > 1){
					progress = 1;
				}

				int alpha = (int) (255 * progress);
				int color = Color.argb(255, changeColor(progress, textColorR),  changeColor(progress, textColorG), changeColor(progress, textColorB));
//				int color = Color.argb(255, (int)((1 - progress) * 255),  (int)((1 - progress) * 255), (int)((1 - progress) * 255));
//				int color = Color.argb(255, 255,  255 - (int)(progress * 102) ,255 - (int)(progress * 255));
//				int color = Color.argb(255, (int)((1 - progress) * 255),  (int)((1 - progress) * 153) ,(int)((1 - progress) * 0));
				mCityTV.setTextColor(color);
				recordView.setTextColor(color);

				titleBarView.setBackgroundDrawable(new ColorDrawable(Color.argb(alpha, 255, 255, 255)));

				if(progress != 0){
					ColorMatrix cm = new ColorMatrix();
					cm.set(new float[] { Color.red(color) / 255f, 0, 0, 0, 0,// 红色值
							0, Color.green(color) / 255f, 0, 0, 0,// 绿色值
							0, 0, Color.blue(color) / 255f, 0, 0,// 蓝色值
							0, 0, 0, 1, 0 // 透明度
					});
                    cityIV.setColorFilter(new ColorMatrixColorFilter(cm));
				}else{
                    cityIV.setColorFilter(null);
				}
			}
		});
		getFragmentManager().beginTransaction().add(R.id.web_layout, mCommonWebFragment).commit();

		initLocation();
		return false;
	}

	private int changeColor(float progress, int color) {
		return 255 - (int)(progress * (255 - color));
	}

	private void selectCity(String city) {
		if (mCommonWebFragment != null && !TextUtils.isEmpty(city)) {
			mCommonWebFragment.javascript("locationCity(\"" + city + "\")");
		}
	}

	private void initLocation() {
		mLocationClient = new AMapLocationClient(getContext());
		AMapLocationClientOption option = new AMapLocationClientOption();
		option.setLocationMode(AMapLocationClientOption.AMapLocationMode.Hight_Accuracy);
		option.setOnceLocation(true);
		mLocationClient.setLocationOption(option);
		mLocationClient.setLocationListener(new AMapLocationListener() {
			@Override
			public void onLocationChanged(AMapLocation aMapLocation) {
				if (aMapLocation != null) {
					if (aMapLocation.getErrorCode() == 0) {
						String city = aMapLocation.getCity();
						String district = aMapLocation.getDistrict();
						String location = StringUtils.extractLocation(city, district);
						setCity(city);
					} else {
						//定位失败
					}
				}
			}
		});
		mLocationClient.startLocation();
	}

	private void setCity(String city) {
		mCityTV.setText(city);
		selectCity(city);
	}

	public void checkLogin(Runnable runnable) {
		if(runnable == null) {
			return;
		}
		if(!UserData.isLogin()){
			ToastUtil.showToast(getContext(), R.string.tip_not_login_doing);
			showLoginView(runnable);
		}else{
			runnable.run();
		}
	}

	@Override
	public void onResumeItem() {
		resetTitleBar();
		setTitleBarEnabled(false);
	}

	@Override
	public void onPauseItem() {
	}

	@Override
	public void onDestroyItem() {
		mLocationClient.stopLocation();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == CityPickerActivity.REQUEST_CODE_PICK_CITY && resultCode == RESULT_OK){
			if (data != null){
				String city = data.getStringExtra(CityPickerActivity.KEY_PICKED_CITY);
				setCity(city);
			}
		}
	}
}
