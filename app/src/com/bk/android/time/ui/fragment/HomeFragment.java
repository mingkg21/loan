package com.bk.android.time.ui.fragment;

import android.os.Bundle;

import com.bk.android.time.data.net.WebConfig;
import com.bk.android.ui.widget.viewpager.ViewPagerItem;

public class HomeFragment extends CommonWebFragment implements ViewPagerItem {

	public HomeFragment(){
		Bundle args = new Bundle();
		args.putString(CommonWebFragment.EXTRA_NAME_URL, WebConfig.getInstance().getHomeUrl());
		setArguments(args);
		setWebTitle(false);
	}

	@Override
	public void onDestroyContentView() {
	}

	@Override
	public void setLifeCycleEnabled(boolean enabled) {
	}

	@Override
	public boolean onActivityResume() {
		return super.onActivityResume();
	}
	
	@Override
	public boolean onCreateItem() {
		return false;
	}

	@Override
	public void onResumeItem() {
		resetTitleBar();
		setTitleBarEnabled(false);
	}

	@Override
	public void onPauseItem() {
	}

	@Override
	public void onDestroyItem() {
	}
	
}
