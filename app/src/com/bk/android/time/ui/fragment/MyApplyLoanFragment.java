package com.bk.android.time.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bk.android.assistant.R;
import com.bk.android.time.model.lightweight.MyApplyLoanListViewModel;
import com.bk.android.time.ui.BaseFragment;
import com.bk.android.ui.widget.viewpager.ViewPagerItem;

/**
 * Created by mingkg21 on 2017/4/8.
 */

public class MyApplyLoanFragment extends BaseFragment implements ViewPagerItem {

    private MyApplyLoanListViewModel mMyApplyLoanListViewModel;

    public MyApplyLoanFragment(){}

    @Override
    public View onCreateContentView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mMyApplyLoanListViewModel = new MyApplyLoanListViewModel(getActivity(), this);
        View contentView = bindView(R.layout.com_list_lay, null, mMyApplyLoanListViewModel);
        setContentView(contentView);
        mMyApplyLoanListViewModel.onStart();
        return contentView;
    }

    @Override
    public void onDestroyContentView() {
    }

    @Override
    public void setLifeCycleEnabled(boolean enabled) {
    }

    @Override
    public boolean onCreateItem() {
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onResumeItem() {
    }

    @Override
    public void onPauseItem() {
    }

    @Override
    public void onDestroyItem() {
        mMyApplyLoanListViewModel.onRelease();
    }

}