package com.bk.android.time.ui.widget;

import uk.co.senab.photoview.PhotoView;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.animation.Animation;

import com.bk.android.assistant.R;
import com.bk.android.time.ui.widget.AsyncImageViewManager.IImageView;
import com.bk.android.time.ui.widget.AsyncImageViewManager.ImageInfo;
import com.bk.android.util.BitmapUtil;
/**
 * 异步图片加载ImageView
 * @author linyiwei
 */
public class AsyncPhotoView extends PhotoView implements IImageView{
	private AsyncImageViewManager mAsyncImageViewManager;

	public AsyncPhotoView(Context context) {
		super(context);
		init();
	}
	
	public AsyncPhotoView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
		TypedArray a = context.obtainStyledAttributes(attrs,R.styleable.AsyncImage);
		int quality = a.getInt(R.styleable.AsyncImage_quality, BitmapUtil.IMG_QUALITY_0);
		a.recycle();
		setQuality(quality);
	}
	
	private void init(){
		mAsyncImageViewManager = new AsyncImageViewManager(this);
		mAsyncImageViewManager.setDefaultHeavyDraw(true);
		mAsyncImageViewManager.setQuality(BitmapUtil.IMG_QUALITY_0);
	}

	@Override
	protected void onAttachedToWindow() {
		super.onAttachedToWindow();
		mAsyncImageViewManager.onAttachedToWindow(this);
	}
	
	@Override
	protected void onDetachedFromWindow() {
		super.onDetachedFromWindow();
		mAsyncImageViewManager.onDetachedFromWindow(this);
	}
	
	public void setDefaultHeavyDraw(boolean defaultHeavyDraw){
		mAsyncImageViewManager.setDefaultHeavyDraw(defaultHeavyDraw);
	}
	
	public void setNeedBuildThumbUrl(boolean needBuildThumbUrl){
		mAsyncImageViewManager.setNeedBuildThumbUrl(needBuildThumbUrl);
	}
	
	public void setQuality(int quality){
		mAsyncImageViewManager.setQuality(quality);
	}
	
	public void setFinishPreAnimation(int anim){
		mAsyncImageViewManager.setFinishPreAnimation(anim);
	}

	public void setFinishPreAnimation(Animation animation){
		mAsyncImageViewManager.setFinishPreAnimation(animation);
	}
	
	public void setFinishPostAnimation(int anim){
		mAsyncImageViewManager.setFinishPostAnimation(anim);
	}

	public void setFinishPostAnimation(Animation animation){
		mAsyncImageViewManager.setFinishPostAnimation(animation);
	}
	
	public void setDefaultImgRes(int defaultImgRes){
		mAsyncImageViewManager.setDefaultImgRes(defaultImgRes);
	}
	
	public void setImageUrl(String url){
		mAsyncImageViewManager.setImageUrl(url);
	}
	
	public void setImageInfo(ImageInfo info){
		mAsyncImageViewManager.setImageInfo(info);
	}
	
	public String getImgUrl(){
		return mAsyncImageViewManager.getUrl();
	}
	
	public boolean hasLoadImg(){
		return getDrawable() != null && mAsyncImageViewManager.hasLoadNetImg();
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		mAsyncImageViewManager.onDraw(canvas,getWidth(),getHeight());
	}
	
	@Override
	public void onLoadedImg(Bitmap bitmap, String imageUrl, String cacheId) {
	}
	
	@Override
	public void onFinishTemporaryDetach() {
		clearAnimation();
		setAnimation(null);
	}
	
	@Override
	public void onSuperDraw(Canvas canvas) {
		super.onDraw(canvas);
	}
}
