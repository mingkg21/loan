package com.bk.android.time.ui.widget.span;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.FontMetricsInt;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.text.style.ReplacementSpan;

import com.bk.android.assistant.R;
import com.bk.android.time.app.App;

public class TextBackgroundSpan extends ReplacementSpan{
    private ISpanDrawableView mSpanDrawableView;
    private String mText;
    private Drawable mDrawable;
	private Rect mPadding;

    public TextBackgroundSpan(ISpanDrawableView spanDrawableView,String text,int bgRes){
    	mPadding = new Rect();
    	mText = text;
    	mSpanDrawableView = spanDrawableView;
    	mDrawable = mSpanDrawableView.getResources().getDrawable(bgRes);
    	mDrawable.getPadding(mPadding);
    }
    
	@Override
	public int getSize(Paint paint, CharSequence text, int start, int end,FontMetricsInt fm) {
		if (fm != null) {
	        paint.getFontMetricsInt(fm);
	        fm.bottom += mPadding.bottom + mPadding.top;
	        mDrawable.setBounds(0, 0, (int) (paint.measureText(mText) + mPadding.right + mPadding.left), fm.bottom - fm.top);
		}
		return mSpanDrawableView.getMaxViewWidth();
	}

	@Override
	public void draw(Canvas canvas, CharSequence text, int start, int end,float x, int top, int y, int bottom, Paint paint) {
		canvas.save();
		canvas.translate(x, top);
		mDrawable.draw(canvas);
		canvas.restore();
		paint.setColor(App.getInstance().getResources().getColor(R.color.com_color_4));
		canvas.drawLine(x, top, x + mSpanDrawableView.getMaxViewWidth(), top, paint);
		paint.setTextSize(paint.getTextSize());
		paint.setColor(App.getInstance().getResources().getColor(R.color.com_color_5));
		canvas.drawText(mText, x + mPadding.left, y + mPadding.top, paint);
	}
}
