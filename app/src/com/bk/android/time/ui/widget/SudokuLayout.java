package com.bk.android.time.ui.widget;

import java.util.HashMap;


public class SudokuLayout {
	/**
	 *	int type = key / 1000 % 10;</br>
	 *	int sideSize = key / 100 % 10;</br>
	 *	int horizontalSize = key / 10 % 10;</br>
	 *	int verticalSize = key % 10;
	 */
	private static HashMap<Integer, SudokuLayout> SUDOKU_ITEM_MAP = new HashMap<Integer, SudokuLayout>();
	/**---------------------------- 一张图片 --------------------------------------*/
	static{
		SudokuLayout root = new SudokuLayout(100, 100);//一方
		SUDOKU_ITEM_MAP.put(1100, root);
		
		root = new SudokuLayout(400, 300);//一横
		SUDOKU_ITEM_MAP.put(1010, root);
		
		root = new SudokuLayout(300, 400);//一竖
		SUDOKU_ITEM_MAP.put(1001, root);
	}
	/**---------------------------- 两张图片 --------------------------------------*/
	static{
		SudokuLayout root = new SudokuLayout(100, 100);//两方
		SUDOKU_ITEM_MAP.put(1200, root);
		root.createRightItem(100, 100);
		
		root = new SudokuLayout(400, 300);//两横
		SUDOKU_ITEM_MAP.put(1020, root);
		root.createBottomItem(400, 300);
		
		root = new SudokuLayout(300, 400);//两竖
		SUDOKU_ITEM_MAP.put(1002, root);
		root.createRightItem(300, 400);
		
		root = new SudokuLayout(400, 300);//一横一竖
		SUDOKU_ITEM_MAP.put(1011, root);
		root.createRightItem(getHeightToWidth(300, 3, 4), 300);
		
		root = new SudokuLayout(300, 400);//一竖一方
		SUDOKU_ITEM_MAP.put(1101, root);
		root.createRightItem(400, 400);
		
		root = new SudokuLayout(400, 300);//一横一方
		SUDOKU_ITEM_MAP.put(1110, root);
		root.createRightItem(300, 300);
	}
	/**---------------------------- 三张图片 --------------------------------------*/
	static{
		SudokuLayout root = new SudokuLayout(100, 100);//三方
		SUDOKU_ITEM_MAP.put(1300, root);
		root.createRightItem(100, 100)
		.createBottomItem(200, 200, root);
		
		root = new SudokuLayout(800, 600);//三横
		SUDOKU_ITEM_MAP.put(1030, root);
		root.createBottomItem(400, 300)
		.createRightItem(400, 300);
		
		root = new SudokuLayout(600, 800);//三竖
		SUDOKU_ITEM_MAP.put(1003, root);
		root.createRightItem(300, 400)
		.createBottomItem(300, 400);
		
		root = new SudokuLayout(400, 300);//一横两竖
		SUDOKU_ITEM_MAP.put(1012, root);
		root.createBottomItem(200, getWidthToHeight(200, 3, 4))
		.createRightItem(200, getWidthToHeight(200, 3, 4));
		
		root = new SudokuLayout(400, 300);//两横一竖
		SUDOKU_ITEM_MAP.put(1021, root);
		int height = getTotalWidthToHeight(400, 4, 3, 3, 4);
		root.createBottomItem(getHeightToWidth(height, 4, 3), height)
		.createRightItem(getHeightToWidth(height, 3, 4), height);
		
		root = new SudokuLayout(400, 300);//一横一竖一方
		SUDOKU_ITEM_MAP.put(1111, root);
		height = getTotalWidthToHeight(400, 3, 3, 3, 4);
		root.createBottomItem(getHeightToWidth(height, 3, 3), height)
		.createRightItem(getHeightToWidth(height, 3, 4), height);
		
		root = new SudokuLayout(400, 300);//一横两方
		SUDOKU_ITEM_MAP.put(1210, root);
		root.createBottomItem(200, 200)
		.createRightItem(200, 200);
		
		root = new SudokuLayout(600, 600);//两方一竖
		SUDOKU_ITEM_MAP.put(1201, root);
		height = getTotalWidthToHeight(600, 3, 4, 3, 3);
		root.createBottomItem(getHeightToWidth(height, 3, 4), height)
		.createRightItem(height, height);
		
		root = new SudokuLayout(600, 600);//一方两竖
		SUDOKU_ITEM_MAP.put(1102, root);
		root.createBottomItem(300, 400)
		.createRightItem(300, 400);
		
		root = new SudokuLayout(400, 300);//两横一方
		SUDOKU_ITEM_MAP.put(1120, root);
		height = getTotalWidthToHeight(400, 4, 3, 3, 3);
		root.createBottomItem(getHeightToWidth(height, 4, 3), height)
		.createRightItem(height, height);
	}
	/**---------------------------- 四张图片 --------------------------------------*/
	static{
		SudokuLayout root = new SudokuLayout(100, 100);//四方
		SUDOKU_ITEM_MAP.put(1400, root);
		root.createRightItem(100, 100)
		.createBottomItem(100, 100, root)
		.createRightItem(100, 100);
		
		root = new SudokuLayout(400, 300);//四横
		SUDOKU_ITEM_MAP.put(1040, root);
		root.createRightItem(400, 300)
		.createBottomItem(400, 300, root)
		.createRightItem(400, 300);
		
		root = new SudokuLayout(300, 400);//四竖
		SUDOKU_ITEM_MAP.put(1004, root);
		root.createRightItem(300, 400)
		.createBottomItem(300, 400, root)
		.createRightItem(300, 400);
		
		root = new SudokuLayout(400, 300);//一横三竖
		SUDOKU_ITEM_MAP.put(1013, root);
		int height = getTotalWidthToHeight(400, 3, 8, 3, 4);
		SudokuLayout temp = root.createBottomItem(getHeightToWidth(height / 2, 3, 4), height / 2);
		temp.createBottomItem(getHeightToWidth(height / 2, 3, 4), height / 2)
		.createRightItem(getHeightToWidth(height, 3, 4), height, temp);
		
		root = new SudokuLayout(400, 300);//一横两方一竖
		SUDOKU_ITEM_MAP.put(1211, root);
		height = getTotalWidthToHeight(400, 1, 2, 3, 4);
		temp = root.createBottomItem(height / 2, height / 2);
		temp.createBottomItem(height / 2, height / 2)
		.createRightItem(getHeightToWidth(height, 3, 4), height, temp);
		
		root = new SudokuLayout(400, 300);//两横两方
		SUDOKU_ITEM_MAP.put(1220, root);
		height = getTotalWidthToHeight(400, 4, 7, 1, 1);
		temp = root.createBottomItem(400 - height, 400 - height);
		temp.createBottomItem(getHeightToWidth((int)(height * 3f / 7), 4, 3), (int)(height * 3f / 7))
		.createRightItem(height, height, temp);
		
		root = new SudokuLayout(400, 300);//三横一竖
		SUDOKU_ITEM_MAP.put(1031, root);
		height = getTotalWidthToHeight(400, 3, 4, 4, 6);
		int width = getHeightToWidth(height, 3, 4);
		root.createBottomItem(width, height)
		.createRightItem(400 - width, getWidthToHeight(400 - width, 4, 3))
		.createBottomItem(400 - width, getWidthToHeight(400 - width, 4, 3));
		
		root = new SudokuLayout(400, 300);//三横一方
		SUDOKU_ITEM_MAP.put(1130, root);
		height = getTotalWidthToHeight(400, 1, 1, 4, 6);
		root.createBottomItem(height, height)
		.createRightItem(400 - height, getWidthToHeight(400 - height, 4, 3))
		.createBottomItem(400 - height, getWidthToHeight(400 - height, 4, 3));
		
		root = new SudokuLayout(400, 400);//三方一竖
		SUDOKU_ITEM_MAP.put(1301, root);
		width = getHeightToWidth(400, 3, 4);
		root.createRightItem(width, 400)
		.createBottomItem((width + 400) / 2, (width + 400) / 2, root)
		.createRightItem((width + 400) / 2, (width + 400) / 2);
		
		root = new SudokuLayout(300, 400);//三竖一方
		SUDOKU_ITEM_MAP.put(1103, root);
		height = getTotalWidthToHeight(600, 1, 1, 3, 4);
		root.createRightItem(300, 400)
		.createBottomItem(height, height, root)
		.createRightItem(getHeightToWidth(height, 3, 4), height);
		
		root = new SudokuLayout(1200, 900);//三方一横
		SUDOKU_ITEM_MAP.put(1310, root);
		root.createBottomItem(400, 400)
		.createRightItem(400, 400)
		.createRightItem(400, 400);
		
		root = new SudokuLayout(300, 400);//一横一方两竖
		SUDOKU_ITEM_MAP.put(1112, root);
		height = getTotalWidthToHeight(600, 1, 1, 4, 3);
		root.createRightItem(300, 400)
		.createBottomItem(getHeightToWidth(height, 4, 3), height, root)
		.createRightItem(height, height);
		
		root = new SudokuLayout(400, 300);//两横两竖
		SUDOKU_ITEM_MAP.put(1022, root);
		height = getTotalWidthToHeight(400, 12, 25, 3, 4);
		width = getHeightToWidth(height, 12, 25);
		temp = root.createBottomItem(width, getWidthToHeight(width, 3, 4));
		temp.createBottomItem(width, getWidthToHeight(width, 4, 3))
		.createRightItem((400 - width), getHeightToWidth(400 - width, 3, 4), temp);
	
		root = new SudokuLayout(400, 400);//两方两竖
		SUDOKU_ITEM_MAP.put(1202, root);
		root.createBottomItem(400, getWidthToHeight(400, 3, 4))
		.createRightItem(400, getWidthToHeight(400, 3, 4), root)
		.createBottomItem(400, 400);
		
		root = new SudokuLayout(300, 400);//两横一方一竖
		SUDOKU_ITEM_MAP.put(1121, root);
		width = getHeightToWidth(400, 4, 3);
		height = getTotalWidthToHeight(width + 300, 12, 25, 3, 4);
		root.createRightItem(width, 400)
		.createBottomItem(getHeightToWidth(height, 4, 3), height, root)
		.createRightItem(height, height);
	}
	
	private static int getTotalWidthToHeight(int totalWidth, int sW1, int sH1, int sW2, int sH2){
		float s1 = sW1 * 1f / sH1;
		float s2 = sW2 * 1f / sH2;
		return (int) (totalWidth / (s1 + s2));
	}

	private static int getHeightToWidth(int height, int sW, int sH){
		return (int) (height * 1f * sW / sH);
	}
	
	private static int getWidthToHeight(int width, int sW, int sH){
		return (int) (width * 1f * sH / sW);
	}
	
	public static SudokuLayout getSudokuLayout(int key){
		int type = key / 1000 % 10;
		int sideSize = key / 100 % 10;
		int horizontalSize = key / 10 % 10;
		int verticalSize = key % 10;
		int size = sideSize + horizontalSize + verticalSize;
		SudokuLayout root = null;
		if(size == 0){
			return root;
		}else{
			root = SUDOKU_ITEM_MAP.get(key);
			if(root == null){
				root = SUDOKU_ITEM_MAP.get(type * 1000 + size * 100);
			}
		}
		return root;
	}
	
	private SudokuLayout leftItem;
	private SudokuLayout topItem;
	private SudokuLayout nextItem;
	private int width;
	private int height;
	
	private SudokuLayout(int w, int h){
		width = w;
		height = h;
	}

	/**
	 * @return the leftItem
	 */
	public SudokuLayout getLeftItem() {
		return leftItem;
	}

	/**
	 * @param leftItem the leftItem to set
	 */
	private void setLeftItem(SudokuLayout leftItem) {
		this.leftItem = leftItem;
	}

	/**
	 * @return the topItem
	 */
	public SudokuLayout getTopItem() {
		return topItem;
	}

	/**
	 * @param topItem the topItem to set
	 */
	private void setTopItem(SudokuLayout topItem) {
		this.topItem = topItem;
	}

	/**
	 * @return the nextItem
	 */
	public SudokuLayout getNextItem() {
		return nextItem;
	}

	/**
	 * @param nextItem the nextItem to set
	 */
	private void setNextItem(SudokuLayout nextItem) {
		this.nextItem = nextItem;
	}
	/**
	 * @return the width
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * @return the height
	 */
	public int getHeight() {
		return height;
	}
	
	private SudokuLayout createRightItem(int w, int h){
		return createRightItem(w, h, this);
	}
	
	private SudokuLayout createRightItem(int w, int h, SudokuLayout p){
		SudokuLayout item = new SudokuLayout(w, h);
		this.setNextItem(item);
		item.setLeftItem(p);
		return item;
	}
	
	private SudokuLayout createBottomItem(int w, int h){
		return createBottomItem(w, h, this);
	}
	
	private SudokuLayout createBottomItem(int w, int h, SudokuLayout p){
		SudokuLayout item = new SudokuLayout(w, h);
		this.setNextItem(item);
		item.setTopItem(p);
		return item;
	}
}
