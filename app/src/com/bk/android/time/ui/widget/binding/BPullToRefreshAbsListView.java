package com.bk.android.time.ui.widget.binding;

import gueei.binding.Binder;
import gueei.binding.IBindableView;
import gueei.binding.ViewAttribute;
import gueei.binding.listeners.ViewMulticastListener;
import gueei.binding.viewAttributes.ViewEventAttribute;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AbsListView;

import com.bk.android.ui.widget.pulltorefresh.PullToRefreshAbsListView;
import com.bk.android.ui.widget.pulltorefresh.PullToRefreshBase;

public abstract class BPullToRefreshAbsListView<T extends AbsListView> extends PullToRefreshAbsListView<T> implements IBindableView<BPullToRefreshAbsListView<T>>{
	public BPullToRefreshAbsListView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setScrollingWhileRefreshingEnabled(true);
	}

	@Override
	public ViewAttribute<? extends View, ?> createViewAttribute(String attributeId) {
		ViewAttribute<? extends View, ?> viewAttribute = null;
		if(attributeId.equals("onRefresh")){
			viewAttribute = new OnRefreshListenerAttribute(this, attributeId);
		}else if(attributeId.equals("refreshComplete")){
			viewAttribute = new RefreshComplete(this, attributeId);
		}else{
			try {
				viewAttribute = Binder.getAttributeForView(getRefreshableView(),attributeId);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return viewAttribute;
	}
	
	public class RefreshComplete extends ViewAttribute<BPullToRefreshAbsListView<T>, Boolean>{
		public RefreshComplete(BPullToRefreshAbsListView<T> view,String attributeId){
			super(Boolean.class, view, attributeId);
		}
		
		@Override
		public Boolean get() {
			return null;
		}

		@Override
		protected void doSetAttributeValue(Object newValue) {
			if (newValue instanceof Boolean) {
				if((Boolean) newValue){
					getView().onRefreshComplete();
				}
			}
		}
	}
	
	public class OnRefreshListenerAttribute  extends ViewEventAttribute<BPullToRefreshAbsListView<T>> implements OnRefreshListener<T>{
		public OnRefreshListenerAttribute(BPullToRefreshAbsListView<T> view,String attributeName) {
			super(view, attributeName);
		}

		@SuppressWarnings("unchecked")
		@Override
		protected void registerToListener(BPullToRefreshAbsListView<T> view) {
			Binder.getMulticastListenerForView(view, OnRefreshListenerMulticast.class).register(this);
		}

		@Override
		public void onRefresh(PullToRefreshBase<T> refreshView) {
			this.invokeCommand(refreshView);
		}
	}
	
	public static class OnRefreshListenerMulticast<T extends AbsListView> extends ViewMulticastListener<OnRefreshListener<T>> implements OnRefreshListener<T>{
		@SuppressWarnings("unchecked")
		@Override
		public void registerToView(View v) {
			if (!(v instanceof BPullToRefreshAbsListView)) return;
			((BPullToRefreshAbsListView<T>)v).setOnRefreshListener(this);
		}

		@Override
		public void onRefresh(PullToRefreshBase<T> refreshView) {
			for(OnRefreshListener<T> l:listeners){
				l.onRefresh(refreshView);
			}
		}
	}
}
