package com.bk.android.time.ui.widget.binding;

import gueei.binding.ViewAttribute;
import android.content.Context;
import android.graphics.Canvas;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.View;

import com.bk.android.time.widget.media.StreamPlayer;
import com.bk.android.time.widget.media.StreamPlayerListener;

public class BAudioQuickImageView extends BQuickImageView implements StreamPlayerListener,Runnable{
	private String mVoiceSrc;
	private float mDegree;
	private Handler mHandler = new Handler(Looper.getMainLooper());
	private boolean isStart;
	long lastTime;

	public BAudioQuickImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
		StreamPlayer.getInstance().addPlayerListener(this);
	}
	
	@Override
	public ViewAttribute<? extends View, ?> createViewAttribute(
			String attributeId) {
		if (attributeId.equals("voiceSrc")) {//设置对于的属性值
			return new VoiceSrc(this,attributeId);
		}
		return super.createViewAttribute(attributeId);
	}

	public static class VoiceSrc extends ViewAttribute<BAudioQuickImageView, String>{
		public VoiceSrc(BAudioQuickImageView view,String attributeId){
			super(String.class, view, attributeId);
		}
		
		@Override
		public String get() {//获取值
			return null;
		}

		@Override
		protected void doSetAttributeValue(Object newValue) {//设置值
			if (newValue instanceof String) {
				getView().setVoiceSrc((String) newValue);
			}else{
				getView().setVoiceSrc(null);
			}
		}
	}
	
	public void setVoiceSrc(String voiceSrc){
		if(mVoiceSrc != voiceSrc){
			mVoiceSrc = voiceSrc;
			mDegree = 0;
			onPlayStateChange(StreamPlayer.getInstance().getState(mVoiceSrc), voiceSrc);
		}
	}
	
	public void start(){
		if(isStart){
			return;
		}
		lastTime = (long) (SystemClock.uptimeMillis() - mDegree * 50f);
		isStart = true;
		mHandler.post(this);
	}
	
	public void stop(){
		if(!isStart){
			return;
		}
		isStart = false;
		mHandler.removeCallbacks(this);
	}
	
	@Override
	public void draw(Canvas canvas) {
		try {
			canvas.save();
			canvas.rotate(mDegree,getWidth() / 2,getHeight() / 2);
			super.draw(canvas);
			canvas.restore();
		} catch (Exception e) {
			super.draw(canvas);
		}
	}
	
	@Override
	public void onPlayStateChange(byte state, String voiceSrc) {
		if(voiceSrc != null && voiceSrc.equals(mVoiceSrc)){
			if(state == StreamPlayerListener.STATE_STOP || state == StreamPlayerListener.STATE_PAUSE
					|| state == StreamPlayerListener.STATE_ERR || state == StreamPlayerListener.STATE_COMPLETION){
				stop();
			}else{
				start();
			}
		}
	}

	@Override
	public void onError(byte type, String voiceSrc) {}

	@Override
	public void onLoadProgressChange(float progress, String voiceSrc) {}

	@Override
	public void onPlayProgressChange(float progress, int maxSeconds,
			String voiceSrc) {
		if(StreamPlayer.getInstance().isPlaying(mVoiceSrc)){
			start();
		}
	}

	@Override
	public void onBuffering(float progress, String voiceSrc) {}

	@Override
	public void run() {
		if(!isStart){
			return;
		}
		mDegree = (SystemClock.uptimeMillis() - lastTime) / 50f;
		if(mDegree > Integer.MAX_VALUE / 2){
			mDegree = 0;
		}
		post(this);
		invalidate();
	}
}
