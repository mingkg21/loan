package com.bk.android.time.ui.widget.binding;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.SystemClock;
import android.util.AttributeSet;

import com.bk.android.util.DimensionsUtil;

public class BHomeCoverImageView extends BAsyncImageView {
	private boolean isInit;
	private boolean isStop;
	private boolean isInvertedOrder;
	private int mInitLeft = 0;
	private int mInitTop = 0;
	private int mCurLeft = 0;
	private int mCurTop = 0;
	private long mLastDrawTime = 0;
	private Rect bounds = new Rect();

	public BHomeCoverImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setDefaultHeavyDraw(false);
		setScaleType(ScaleType.CENTER_CROP);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		setMeasuredDimension(getMeasuredWidth(), (int) (getMeasuredWidth() * 3f / 5));
	}
	
	@Override
	public void setImageDrawable(Drawable drawable) {
		super.setImageDrawable(drawable);
		isInit = false;
		isStop = false;
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		Drawable drawable = getDrawable();
		if(drawable != null){
			if(!isStop && hasLoadImg()){
				if(!isInit){
					int vWidth = getWidth();
					int vHeight = getHeight();
					int imgWidth = drawable.getIntrinsicWidth();
					int imgHeight = drawable.getIntrinsicHeight();
					int gapW = imgHeight * (imgWidth - vWidth) / imgWidth;
					int gapH = imgHeight - vHeight;
					if(gapW <= gapH){
						imgHeight = imgHeight * vWidth / imgWidth;
						imgWidth = vWidth;
						mInitLeft = getLeft();
						mInitTop = getTop() - (imgHeight - vHeight) / 2;
					}else{
						imgWidth = imgWidth * vHeight / imgHeight;
						imgHeight = vHeight;
						mInitLeft = getLeft() - (imgWidth - vWidth) / 2;
						mInitTop = getTop();
					}
					isInit = true;
					isInvertedOrder = true;
					mCurLeft = -mInitLeft;
					mCurTop = -mInitTop;
					mLastDrawTime = SystemClock.uptimeMillis();
				}else{
					long timeDifference = SystemClock.uptimeMillis() - mLastDrawTime;
					int unitX = (int) ((isInvertedOrder ? -1 : 1) * timeDifference * DimensionsUtil.DIPToPXF(0.01f));
					int unitY = (int) ((isInvertedOrder ? -1 : 1) * timeDifference * DimensionsUtil.DIPToPXF(0.01f));
					if(unitX != 0 || unitY != 0){
						if(mInitLeft != 0){
							mCurLeft += unitX;
						}
						if(mInitTop != 0){
							mCurTop += unitY;
						}
						
						if(!isInvertedOrder && mCurTop > 0){
							mCurTop = 0;
							isStop = true;
						}
						if(!isInvertedOrder && mCurLeft > 0){
							mCurLeft = 0;
							isStop = true;
						}
						
						if(mCurLeft > -mInitLeft){
							mCurLeft = -mInitLeft;
							isInvertedOrder = true;
						}
						if(mCurLeft < mInitLeft){
							mCurLeft = mInitLeft;
							isInvertedOrder = false;
						}
						if(mCurTop > -mInitTop){
							mCurTop = -mInitTop;
							isInvertedOrder = true;
						}
						if(mCurTop < mInitTop){
							mCurTop = mInitTop;
							isInvertedOrder = false;
						}
						mLastDrawTime = SystemClock.uptimeMillis();
					}
				}
				canvas.save();
				canvas.translate(mCurLeft, mCurTop);
				super.onDraw(canvas);
				canvas.restore();
				invalidate();
			}else{
				isInit = false;
				if(hasLoadImg()){
					super.onDraw(canvas);
				}else{
					drawable.copyBounds(bounds);
					drawable.setBounds(0, 0, getWidth() ,getHeight());
					canvas.save();
					drawable.draw(canvas);
					canvas.restore();
					drawable.setBounds(bounds);
				}
			}
		}
	}
}
