package com.bk.android.time.ui.widget.span;

import android.content.res.Resources;

public interface ISpanDrawableView{
	public void invalidate();
	public int getViewPaddingLeft();
	public int getViewPaddingRight();
	public int getViewPaddingTop();
	public int getViewPaddingBottom();
	public int getMaxViewWidth();
	public int getMaxViewHeight();
	public void requestViewLayout(Object span);
    public Resources getResources();
}