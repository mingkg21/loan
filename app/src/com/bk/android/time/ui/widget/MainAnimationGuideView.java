//package com.bk.android.time.ui.widget;
//
//import java.util.ArrayList;
//
//import android.content.Context;
//import android.view.KeyEvent;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.animation.Animation;
//import android.view.animation.Animation.AnimationListener;
//import android.view.animation.AnimationUtils;
//import android.widget.FrameLayout;
//
//import com.bk.android.assistant.R;
//
///** 引导动画
// * @author mingkg21
// * @email mingkg21@gmail.com
// * @date 2015年2月7日
// *
// */
//public class MainAnimationGuideView extends FrameLayout {
//	
//	private CallBack mCallBack;
//	private int currentAnimIndex;
//	
//	public MainAnimationGuideView(Context context,CallBack callBack) {
//		super(context);
//		LayoutInflater.from(getContext()).inflate(R.layout.uniq_main_animation_guide_lay, this);
//		setFocusable(true);
//		
//		animationRun(0);
//	}
//	
//	private void animationRun(final int index) {
////		int currentViewId = 0;
////		if(index == 0) {
////			currentViewId = R.id.guide_1_lay;
////		} else if(index == 1) {
////			currentViewId = R.id.guide_2_lay;
////		} else if(index == 2) {
////			currentViewId = R.id.guide_3_lay;
////		} else if(index == 3) {
////			currentViewId = R.id.guide_4_lay;
////		} else {
////			return;
////		}
////		Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.alpha_in);
////		final View view = findViewById(currentViewId);
////		view.startAnimation(animation);
////		animation.setAnimationListener(new AnimationListener() {
////			
////			@Override
////			public void onAnimationStart(Animation arg0) {
////				view.setVisibility(View.VISIBLE);
////			}
////			
////			@Override
////			public void onAnimationRepeat(Animation arg0) {
////				
////			}
////			
////			@Override
////			public void onAnimationEnd(Animation arg0) {
////				if(index != 3) {
////					view.setVisibility(View.GONE);
////					animationRun(index + 1);
////				}
////			}
////		});
//		
//		final ArrayList<AnimationUnit> units = new ArrayList<MainAnimationGuideView.AnimationUnit>();
//		
//		AnimationUnit unit1 = null;
//		AnimationUnit unit2 = null;
//		AnimationUnit unit3 = null;
//		AnimationUnit unit4 = null;
//		AnimationUnit unit5 = null;
//		Runnable runnable = new Runnable() {
//			
//			@Override
//			public void run() {
//				if(currentAnimIndex < units.size()) {
//					units.get(currentAnimIndex).animationRun();
////					ToastUtil.showLongToast(getContext(), "show anim index: " + currentAnimIndex);
//					currentAnimIndex++;
//				}
//			}
//		};
//		
//		unit1 = new AnimationUnit(runnable);
//		unit1.layoutRedsId = R.id.guide_1_lay;
//		unit1.animResId = R.anim.alpha_in;
//		unit1.addItem(new AnimationItem(R.id.ic_main_guide_1_text, R.anim.alpha_in));
//		units.add(unit1);
//		
//		unit2 = new AnimationUnit(runnable);
//		unit2.layoutRedsId = R.id.guide_2_lay;
//		unit2.lastLayoutRedsId = R.id.guide_1_lay;
//		unit2.animResId = R.anim.alpha_in;
//		unit2.addItem(new AnimationItem(R.id.ic_main_guide_2_text, R.anim.alpha_in));
//		units.add(unit2);
//		
//		unit3 = new AnimationUnit(runnable);
//		unit3.layoutRedsId = R.id.guide_3_lay;
//		unit3.lastLayoutRedsId = R.id.guide_2_lay;
//		unit3.animResId = R.anim.alpha_in;
//		unit3.addItem(new AnimationItem(R.id.ic_main_guide_3_text, R.anim.alpha_in));
//		units.add(unit3);
//		
//		unit4 = new AnimationUnit(runnable);
//		unit4.layoutRedsId = R.id.guide_4_lay;
//		unit4.lastLayoutRedsId = R.id.guide_3_lay;
//		unit4.animResId = R.anim.alpha_in;
//		unit4.addItem(new AnimationItem(R.id.ic_main_guide_4_text, R.anim.alpha_in));
//		units.add(unit4);
//		
//		unit5 = new AnimationUnit(runnable);
//		unit5.layoutRedsId = R.id.guide_5_lay;
//		unit5.lastLayoutRedsId = R.id.guide_4_lay;
//		unit5.animResId = R.anim.alpha_in;
//		unit5.isNeedGone = false;
//		unit5.addItem(new AnimationItem(R.id.ic_main_guide_5_text, R.anim.alpha_in));
//		unit5.addItem(new AnimationItem(R.id.ic_main_guide_5_login, R.anim.alpha_in));
//		units.add(unit5);
//		
//		runnable.run();
//	}
//	
//	private class AnimationUnit {
//		
//		public int layoutRedsId;
//		public int lastLayoutRedsId;
//		public int animResId;
//		public boolean isNeedGone = true;
//		public boolean isLayoutShow;
//		public ArrayList<AnimationItem> itemResIds;
//		private int currentItemIndex;
//		private Runnable mRunnable;
//		
//		public AnimationUnit(Runnable runnable) {
//			mRunnable = runnable;
//		}
//		
//		public void addItem(AnimationItem item) {
//			if(item == null) {
//				return;
//			}
//			if(itemResIds == null) {
//				itemResIds = new ArrayList<MainAnimationGuideView.AnimationItem>();
//			}
//			itemResIds.add(item);
//		}
//	
//		
//		private boolean isRunDone() {
//			if(itemResIds == null || itemResIds.size() <= currentItemIndex) {
//				return true;
//			}
//			return false;
//		}
//		
//		public void animationRun() {
//			int currentViewId = layoutRedsId;
//			int currentAnimResId = animResId;
//			if(isLayoutShow) {
//				if(isRunDone()) {
//					mRunnable.run();
//					return;
//				}
//				AnimationItem item = itemResIds.get(currentItemIndex);
//				if(item == null) {
//					mRunnable.run();
//					return;
//				}
//				currentViewId = item.resId;
//				currentAnimResId = item.animResId;
//				currentItemIndex++;
//			}
//			Animation animation = AnimationUtils.loadAnimation(getContext(), currentAnimResId);
//			final View view = findViewById(currentViewId);
//			view.startAnimation(animation);
//			animation.setAnimationListener(new AnimationListener() {
//				
//				@Override
//				public void onAnimationStart(Animation arg0) {
//					view.setVisibility(View.VISIBLE);
//					if(lastLayoutRedsId != 0) {
//						findViewById(lastLayoutRedsId).setVisibility(View.GONE);
//					}
//				}
//				
//				@Override
//				public void onAnimationRepeat(Animation arg0) {
//					
//				}
//				
//				@Override
//				public void onAnimationEnd(Animation arg0) {
//					if(!isLayoutShow) {
//						isLayoutShow = true;
//					}
//					if(isRunDone()) {
//						if(isNeedGone) {
////							findViewById(layoutRedsId).setVisibility(View.GONE);
//						}
//						mRunnable.run();
//					} else {
//						animationRun();
//					}
//				}
//			});
//		}
//		
//	}
//	
//	private class AnimationItem {
//		public int resId;
//		public int animResId;
//		
//		public AnimationItem(int resId, int animResId) {
//			this.resId = resId;
//			this.animResId = animResId;
//		}
//	}
//	
//	@Override
//    public boolean dispatchKeyEvent(KeyEvent event) {
//        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
//        	if (event.getAction() == KeyEvent.ACTION_UP){
//    			mCallBack.onExit(false);
//                return true;
//        	}
//            return super.dispatchKeyEvent(event);
//        } else {
//            return super.dispatchKeyEvent(event);
//        }
//    }
//	
//	public interface CallBack{
//		public void onExit(boolean isGotoLogin);
//		public boolean isLogin();
//	}
//}
