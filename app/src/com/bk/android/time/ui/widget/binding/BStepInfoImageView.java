package com.bk.android.time.ui.widget.binding;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

public class BStepInfoImageView extends BAsyncImageView{
	public BStepInfoImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec,heightMeasureSpec);
		Drawable drawable = getDrawable();
		if(drawable != null){
			int w = getMeasuredWidth() - getPaddingLeft() - getPaddingRight();
			int mexW = (int) (getResources().getDisplayMetrics().widthPixels * 0.8f);
			if(getMeasuredWidth() > mexW){
				w = mexW - getPaddingLeft() - getPaddingRight();
			}
			int imgW = drawable.getIntrinsicWidth();
	        int imgH = drawable.getIntrinsicHeight();
	        if(imgW > 0){
	        	imgH = imgH * w / imgW;
				imgW = w;
				setMeasuredDimension(w + getPaddingLeft() + getPaddingRight(), imgH + getPaddingTop() + getPaddingBottom());
	        }
		}
	}
}
