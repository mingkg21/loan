package com.bk.android.time.ui.widget.span;

import com.bk.android.assistant.R;
import com.bk.android.time.app.App;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.FontMetricsInt;
import android.text.style.ReplacementSpan;

public class TextFillSpan extends ReplacementSpan{
    private ISpanDrawableView mSpanDrawableView;
    private String mLeftText;
    private String mRightText;

    public TextFillSpan(ISpanDrawableView spanDrawableView,String leftText,String rightText){
    	mLeftText = leftText;
    	mRightText = rightText;
    	mSpanDrawableView = spanDrawableView;
    }
    
	@Override
	public int getSize(Paint paint, CharSequence text, int start, int end,FontMetricsInt fm) {
		if (fm != null) {
	        paint.getFontMetricsInt(fm);
		}
		return mSpanDrawableView.getMaxViewWidth();
	}

	@Override
	public void draw(Canvas canvas, CharSequence text, int start, int end,float x, int top, int y, int bottom, Paint paint) {
		paint.setTextSize(paint.getTextSize() * 0.9f);
		paint.setColor(App.getInstance().getResources().getColor(R.color.com_color_3));
		canvas.drawText(mRightText, x + mSpanDrawableView.getMaxViewWidth() - paint.measureText("　　　　　　"), y , paint);
		canvas.drawText(mLeftText, x , y , paint);
	}
}
