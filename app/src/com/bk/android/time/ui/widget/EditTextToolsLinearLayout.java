package com.bk.android.time.ui.widget;

import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.LinearLayout;

public class EditTextToolsLinearLayout extends LinearLayout {
	private int mInitActivityHeight;
	private View mDecorView;
	private OnGlobalLayoutListener mOnGlobalLayoutListener;

	public EditTextToolsLinearLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	@Override
	protected void onAttachedToWindow() {
		super.onAttachedToWindow();
		if(mOnGlobalLayoutListener == null){
			mOnGlobalLayoutListener = new OnGlobalLayoutListener() {
				@Override
				public void onGlobalLayout() {
					if(mInitActivityHeight == 0){
						mInitActivityHeight = mDecorView.getHeight();
					}
					Rect frame = new Rect();  
					mDecorView.getWindowVisibleDisplayFrame(frame);
					boolean isShow = mInitActivityHeight > mDecorView.getHeight();
					if(isShow){
						setVisibility(VISIBLE);
					}else{
						setVisibility(GONE);
					}
				}
			};
		}
		if(getContext() instanceof Activity){
			mDecorView = ((Activity) getContext()).getWindow().getDecorView().findViewById(android.R.id.edit);
		}
		if(mDecorView != null){
			mDecorView.getViewTreeObserver().addOnGlobalLayoutListener(mOnGlobalLayoutListener);
		}
	}
	
	@SuppressWarnings("deprecation")
	@Override
	protected void onDetachedFromWindow() {
		super.onDetachedFromWindow();
		if(mOnGlobalLayoutListener != null){
			if (android.os.Build.VERSION.SDK_INT >= 17) {
				mDecorView.getViewTreeObserver().removeOnGlobalLayoutListener(mOnGlobalLayoutListener);
			}else{
				mDecorView.getViewTreeObserver().removeGlobalOnLayoutListener(mOnGlobalLayoutListener);
			}
		}
	}
}
