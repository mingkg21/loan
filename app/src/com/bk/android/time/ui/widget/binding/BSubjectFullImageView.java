package com.bk.android.time.ui.widget.binding;

import android.content.Context;
import android.util.AttributeSet;

public class BSubjectFullImageView extends BFullImageView{
	public BSubjectFullImageView(Context context) {
		super(context);
	}
	
	public BSubjectFullImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		if(!hasLoadImg()){
			setMeasuredDimension(getMeasuredWidth(), (int) (getMeasuredWidth() * 1f / 482 * 207));
		}
	}
}
