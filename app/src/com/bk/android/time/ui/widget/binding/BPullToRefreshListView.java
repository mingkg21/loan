package com.bk.android.time.ui.widget.binding;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.util.AttributeSet;
import android.widget.ListView;

import com.bk.android.time.ui.widget.pulltorefresh.FlipLoadingLayout;
import com.bk.android.time.ui.widget.pulltorefresh.RotateLoadingLayout;
import com.bk.android.ui.widget.pulltorefresh.internal.LoadingLayout;

public class BPullToRefreshListView extends BPullToRefreshAbsListView<ListView>{
	public BPullToRefreshListView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setScrollingWhileRefreshingEnabled(true);
	}
	
	@Override
	protected ListView createRefreshableView(Context context, AttributeSet attrs) {
		ListView lv = (ListView) createListView(context, attrs);
		lv.setCacheColorHint(Color.TRANSPARENT);
		lv.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		lv.setId(android.R.id.list);
		return lv;
	}

	@Override
	protected LoadingLayout createLoadingLayout(
			Context context,AnimationStyle style,Mode mode,Orientation scrollDirection,TypedArray attrs) {
		switch (style) {
			case ROTATE:
			default:
				return new RotateLoadingLayout(context, mode, scrollDirection, attrs);
			case FLIP:
				return new FlipLoadingLayout(context, mode, scrollDirection, attrs);
		}
	}
}
