package com.bk.android.time.ui.widget;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ListAdapter;

import com.bk.android.ui.widget.DecoratorListAdapter;

public class FullHorizontalGridView extends GridView{
	private int mHorizontalSpacing;
	private int mColumnWidth;
	private int mContentHeight;

	public FullHorizontalGridView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	@Override
	public void setAdapter(ListAdapter adapter) {
		if(adapter != null){
			super.setAdapter(new InteriorAdapter(adapter));
		}else{
			super.setAdapter(adapter);
		}
	}

	@Override
	public void setHorizontalSpacing(int horizontalSpacing) {
		mHorizontalSpacing = horizontalSpacing; 
		super.setHorizontalSpacing(horizontalSpacing);
	}

	@Override
	public void setColumnWidth(int columnWidth) {
		super.setColumnWidth(columnWidth);
		mColumnWidth = columnWidth;
	}
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		if(getCount() > 0){
			setNumColumns(getCount());
			int padingHorizontal = getPaddingLeft() + getPaddingRight();
			int padingVertical = getPaddingTop() + getPaddingBottom();
			int horizontalSpacing = mHorizontalSpacing;
			Rect padding = new Rect();
			if(getSelector() != null){
				getSelector().getPadding(padding);
				padingVertical += padding.top + padding.bottom;
				horizontalSpacing += padding.left + padding.right;
			}
			int wCount = mColumnWidth * getCount() + horizontalSpacing * (getCount() - 1) + padingHorizontal;
			if(wCount != getLayoutParams().width){
				getLayoutParams().width = wCount;
				post(new Runnable() {
					@Override
					public void run() {
						requestLayout();
					}
				});
			}
			int measureHeight = MeasureSpec.getSize(heightMeasureSpec);
			if(measureHeight > 0){
				mContentHeight = measureHeight - padingVertical;
			}else{
				mContentHeight = 0;
			}
		}
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	}
	
	private class InteriorAdapter extends DecoratorListAdapter{
		public InteriorAdapter(ListAdapter adapter) {
			super(adapter);
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			convertView = super.getView(position, convertView, parent);
			if(convertView != null && mContentHeight > 0){
				LayoutParams lp = (LayoutParams) convertView.getLayoutParams();
				if(lp == null){
					lp = new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT);
					convertView.setLayoutParams(lp);
				}
				lp.height = mContentHeight;
			}
			return convertView;
		}
	}
}
