package com.bk.android.time.ui.widget;

import java.io.IOException;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Drawable.Callback;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.util.AttributeSet;

public class DecoratorDrawable extends Drawable implements Callback{
	private Drawable mDrawable;
	private Integer mAlpha = null;
	private ColorFilter mColorFilter = null;
	private Boolean isDither = null;
	private Boolean isFilter = null;
	public DecoratorDrawable(Drawable drawable){
		mDrawable = drawable;
		mDrawable.setCallback(this);
	}
	
	@Override
	public void draw(Canvas canvas) {
		getSourceDrawable().draw(canvas);
	}

	@Override
	public void setAlpha(int alpha) {
		mAlpha = alpha;
		getSourceDrawable().setAlpha(alpha);
	}

//	@Override
//	public void setColorFilter(int color, Mode mode) {
//		getDrawable().setColorFilter(color, mode);
//	}
//
//	@Override
//	public void clearColorFilter() {
//		getDrawable().clearColorFilter();
//	}
	
	@Override
	public void setColorFilter(ColorFilter cf) {
		mColorFilter = cf;
		getSourceDrawable().setColorFilter(cf);
	}

	@Override
	public int getOpacity() {
		return getSourceDrawable().getOpacity();
	}
	
	@Override
	public void setBounds(int left, int top, int right, int bottom) {
		getSourceDrawable().setBounds(left, top, right, bottom);
		super.setBounds(left, top, right, bottom);
	}
	
	@Override
	public void setBounds(Rect bounds) {
		getSourceDrawable().setBounds(bounds);
		super.setBounds(bounds);
	}

	@Override
	public void setChangingConfigurations(int configs) {
		getSourceDrawable().setChangingConfigurations(configs);
	}

	@Override
	public int getChangingConfigurations() {
		return getSourceDrawable().getChangingConfigurations();
	}

	@Override
	public void setDither(boolean dither) {
		isDither = dither;
		getSourceDrawable().setDither(dither);
	}

	@Override
	public void setFilterBitmap(boolean filter) {
		isFilter = filter;
		getSourceDrawable().setFilterBitmap(filter);
	}

	@Override
	public boolean isStateful() {
		return getSourceDrawable().isStateful();
	}

	@Override
	public boolean setState(int[] stateSet) {
		return getSourceDrawable().setState(stateSet);
	}

	@Override
	public int[] getState() {
		return getSourceDrawable().getState();
	}

	@Override
	public Drawable getCurrent() {
		return getSourceDrawable().getCurrent();
	}

	@Override
	public boolean setVisible(boolean visible, boolean restart) {
		return getSourceDrawable().setVisible(visible, restart);
	}

	@Override
	public Region getTransparentRegion() {
		return getSourceDrawable().getTransparentRegion();
	}

	@Override
	public int getIntrinsicWidth() {
		return getSourceDrawable().getIntrinsicWidth();
	}

	@Override
	public int getIntrinsicHeight() {
		return getSourceDrawable().getIntrinsicHeight();
	}

	@Override
	public int getMinimumWidth() {
		return getSourceDrawable().getMinimumWidth();
	}

	@Override
	public int getMinimumHeight() {
		return getSourceDrawable().getMinimumHeight();
	}

	@Override
	public boolean getPadding(Rect padding) {
		return getSourceDrawable().getPadding(padding);
	}

	@Override
	public Drawable mutate() {
		return this;
	}

	@Override
	public void inflate(Resources r, XmlPullParser parser,
			AttributeSet attrs) throws XmlPullParserException, IOException {
		getSourceDrawable().inflate(r, parser, attrs);
	}

	@Override
	public ConstantState getConstantState() {
		return getSourceDrawable().getConstantState();
	}
	
	@Override
	public void invalidateSelf() {
		getSourceDrawable().invalidateSelf();
	}
	 
	@Override
	public void scheduleSelf(Runnable what, long when) {
		getSourceDrawable().scheduleSelf(what, when);
	}

	@Override
	public void unscheduleSelf(Runnable what) {
		getSourceDrawable().unscheduleSelf(what);
	}
	
	@Override
	public void invalidateDrawable(Drawable who) {
		super.invalidateSelf();
	}

	@Override
	public void scheduleDrawable(Drawable who, Runnable what, long when) {
		super.scheduleSelf(what, when);
	}

	@Override
	public void unscheduleDrawable(Drawable who, Runnable what) {
		super.unscheduleSelf(what);
	}

	public Drawable getSourceDrawable(){
		return mDrawable;
	}
	
    public Callback getCallback() {
    	if(VERSION.SDK_INT >= VERSION_CODES.HONEYCOMB){
            return super.getCallback();
    	}
        return null;
    }
    
	public void replaceDrawable(Drawable newDrawable){
		if(newDrawable.equals(mDrawable)){
			return;
		}
		Rect rect = mDrawable.copyBounds();
		int configs = mDrawable.getChangingConfigurations();
		int level = mDrawable.getLevel();
		int[] mStateSet = mDrawable.getState();
		boolean isVisible = mDrawable.isVisible();
		mDrawable.setCallback(null);
		mDrawable = newDrawable;
		mDrawable.setCallback(this);
		if(mAlpha != null){
			mDrawable.setAlpha(mAlpha);
		}
		if(isDither != null){
			mDrawable.setDither(isDither);
		}
		if(isFilter != null){
			mDrawable.setFilterBitmap(isFilter);
		}
		if(mColorFilter != null){
			mDrawable.setColorFilter(mColorFilter);
		}
		mDrawable.setBounds(rect);
		mDrawable.setChangingConfigurations(configs);
		mDrawable.setLevel(level);
		mDrawable.setState(mStateSet);
		mDrawable.setVisible(isVisible, false);
		invalidateSelf();
	}
}
