package com.bk.android.time.ui.widget.binding;

import gueei.binding.IBindableView;
import gueei.binding.ViewAttribute;
import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ProgressBar;

import com.bk.android.time.widget.media.StreamPlayer;
import com.bk.android.time.widget.media.StreamPlayerListener;
import com.bk.android.ui.widget.ScrollChildView;

/** 播放进度条
 * @author mingkg21
 * @email mingkg21@gmail.com
 * @date 2014年5月26日
 *
 */
public class BAudioProgressBar extends ProgressBar implements IBindableView<BAudioProgressBar>, StreamPlayerListener,ScrollChildView {//实现这个接口才可以被识别到
	private StreamPlayer mAudioPlayer;
	private String mVoiceSrc;

	public BAudioProgressBar(Context context, AttributeSet attrs) {
		super(context, attrs);
		mAudioPlayer = StreamPlayer.getInstance();
		mAudioPlayer.addPlayerListener(this);
		setProgress(0);
		setMax(Integer.MAX_VALUE);
	}

	@Override
	public ViewAttribute<? extends View, ?> createViewAttribute(
			String attributeId) {
		if (attributeId.equals("voiceSrc")) {//设置对于的属性值
			return new VoiceSrc(this,attributeId);
		}
		return null;
	}
	
	public static class VoiceSrc extends ViewAttribute<BAudioProgressBar, String>{//定义View的类型 和 数据类型 ，如果是多个值要定义成 Object 在去类型转换
		public VoiceSrc(BAudioProgressBar view,String attributeId){
			super(String.class, view, attributeId);
		}
		
		@Override
		public String get() {//获取值
			return null;
		}

		@Override
		protected void doSetAttributeValue(Object newValue) {//设置值
			if (newValue instanceof String) {
				getView().setVoiceSrc((String) newValue);
			}else{
				getView().setVoiceSrc(null);
			}
		}
	}

	public void setVoiceSrc(String voiceSrc) {
		mVoiceSrc = voiceSrc;
	}

	@Override
	public boolean canScroll(View view, int dx, int dy, int x, int y) {
		return true;
	}

	@Override
	public void onPlayStateChange(byte state, String voiceSrc) {
		if(mAudioPlayer.getState(mVoiceSrc) != StreamPlayerListener.STATE_START
				&& mAudioPlayer.getState(mVoiceSrc) != StreamPlayerListener.STATE_PAUSE){
			setProgress(0);
		}
	}

	@Override
	public void onError(byte type, String voiceSrc) {}

	@Override
	public void onLoadProgressChange(float progress, String voiceSrc) {}

	@Override
	public void onPlayProgressChange(float progress, int maxSeconds,
			String voiceSrc) {
		if(!TextUtils.isEmpty(mVoiceSrc) && voiceSrc.equals(mVoiceSrc) && maxSeconds != 0){
			setProgress((int) (maxSeconds * progress));
			setMax(maxSeconds);
		}
	}

	@Override
	public void onBuffering(float progress, String voiceSrc) {}
}
