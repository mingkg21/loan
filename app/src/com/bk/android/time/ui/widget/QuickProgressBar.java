package com.bk.android.time.ui.widget;

import android.R;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.util.AttributeSet;
import android.view.View;

public class QuickProgressBar extends View {
    private static final int MAX_LEVEL = 10000;
	private int mProgress;
	private int mMax;
	private Drawable mProgressDrawable;

	public QuickProgressBar(Context context, AttributeSet attrs) {
		super(context, attrs);
		setProgress(0);
	}
	
    public void setProgress(int progress) {
    	mProgress = progress;
    	doRefreshProgress(R.id.progress, progress);
    }

    public void setMax(int max) {
    	mMax = max;
    	doRefreshProgress(R.id.progress, mProgress);
    }
    
    public void setProgressDrawable(Drawable d) {
        mProgressDrawable = d;
        invalidate();
    }
    
    @Override
    protected void dispatchDraw(Canvas canvas) {
    	super.dispatchDraw(canvas);
    	if(mProgressDrawable != null){
    		mProgressDrawable.setBounds(0,0,getWidth(),getHeight());
    		mProgressDrawable.draw(canvas);
    	}
    }
    
    private void doRefreshProgress(int id, int progress) {
        final Drawable d = mProgressDrawable;
        if (d != null) {
            float scale = mMax > 0 ? (float) progress / (float) mMax : 0;
            Drawable progressDrawable = null;

            if (d instanceof LayerDrawable) {
                progressDrawable = ((LayerDrawable) d).findDrawableByLayerId(id);
            }

            final int level = (int) (scale * MAX_LEVEL);
            (progressDrawable != null ? progressDrawable : d).setLevel(level);
            invalidate();
        }
    }
}
