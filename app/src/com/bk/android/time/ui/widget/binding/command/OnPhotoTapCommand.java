package com.bk.android.time.ui.widget.binding.command;

import android.view.View;

import com.bk.android.binding.command.BaseCommand;

import uk.co.senab.photoview.PhotoView;

public abstract class OnPhotoTapCommand extends BaseCommand {
	@Override
	protected void onInvoke(View view, Object... args) {
		if(args == null || args.length < 2
				|| !(view instanceof PhotoView)
				|| !(args[0] instanceof Float)
				|| !(args[1] instanceof Float)){
			return;
		}
		onPhotoTap((PhotoView) view, (Float) args[0], (Float) args[1]);
	}
	
	public abstract void onPhotoTap(View view, float x, float y);
}
