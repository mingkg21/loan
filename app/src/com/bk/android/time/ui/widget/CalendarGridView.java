package com.bk.android.time.ui.widget;

import gueei.binding.CollectionChangedEventArg;
import gueei.binding.CollectionObserver;
import gueei.binding.IObservableCollection;
import gueei.binding.collections.ArrayListObservable;

import java.lang.ref.WeakReference;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint.FontMetrics;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;

import com.bk.android.time.ui.widget.DrawerViewFlow.ItemSeletedView;
import com.bk.android.util.DimensionsUtil;

public class CalendarGridView extends View implements ItemSeletedView,CollectionObserver, OnClickListener{
	private int mSelection;
	private FontMetrics mFontMetrics;
	private TextPaint mPaint;
	private ArrayListObservable<AbsDayItemData<?>> mItems;
	private LinkedList<DayItem> mDayItems = new LinkedList<DayItem>();
	private LinkedList<DayItem> mDayItemCache = new LinkedList<DayItem>();
	private int mItemWidth;
	private int mHorizontalSpacing;
	private int mVerticalSpacing;
	private PointF mPoint;
	private RectF mItemRect;
	private OnCalendarItemClickListener mOnCalendarItemClickListener;
	private boolean isReLoadData;
	
	public CalendarGridView(Context context, AttributeSet attrs) {
		super(context, attrs);
		mFontMetrics = new FontMetrics();
		mItemRect = new RectF();
		mPoint = new PointF();
		mPaint = new TextPaint();
		mPaint.setColor(Color.BLACK);
		mPaint.setTextSize(DimensionsUtil.DIPToPXF(20));
		mPaint.setAntiAlias(true);
		mPaint.getFontMetrics(mFontMetrics);
		setClickable(true);
		setOnClickListener(this);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if(event.getAction() == MotionEvent.ACTION_UP){
			mPoint.set(event.getX(), event.getY());
		}
		return super.onTouchEvent(event);
	}

	@Override
	public void onClick(View v) {
		for (int i = 0; i < mDayItems.size(); i++) {
			DayItem dayItem = mDayItems.get(i);
			mItemRect.set(dayItem.mX ,dayItem.mY , dayItem.mX + mItemWidth,dayItem.mY + mItemWidth);
			if(mItemRect.contains(mPoint.x, mPoint.y)){
				if(mOnCalendarItemClickListener != null){
					mOnCalendarItemClickListener.onCalendarItemClick(this,i);
				}
				setSelection(i);
				break;
			}
		}
	}
	
	public void setSelection(int selection){
		mSelection = selection;
	}
	
	public void setData(ArrayListObservable<AbsDayItemData<?>> items){
		if(mItems != null){
			mItems.unsubscribe(this);
		}
		mItems = items;
		if(mItems != null){
			mItems.subscribe(this);
		}
		isReLoadData = true;
		requestLayout();
	}

	public void setVerticalSpacing(int horizontalSpacing) {
		int oldHorizontalSpacing = horizontalSpacing;
		mHorizontalSpacing = horizontalSpacing;
		if(mHorizontalSpacing != oldHorizontalSpacing){
			requestLayout();
		}
	}

	public void setHorizontalSpacing(int verticalSpacing) {
		int oldVerticalSpacing = verticalSpacing;
		mVerticalSpacing = verticalSpacing;
		if(mVerticalSpacing != oldVerticalSpacing){
			requestLayout();
		}
	}

	@Override
	public void onCollectionChanged(IObservableCollection<?> collection,CollectionChangedEventArg args, Collection<Object> initiators) {
		isReLoadData = true;
		requestLayout();
	}
	
	@Override
	public void draw(Canvas canvas) {
		super.draw(canvas);
		for (int i = 0; i < mDayItems.size(); i++) {
			DayItem dayItem = mDayItems.get(i);
			canvas.save();
			canvas.translate(dayItem.mX, dayItem.mY);
			dayItem.onDraw(canvas);
			canvas.restore();
		}
	}

	@Override
	protected void onLayout(boolean changed, int left, int top, int right,
			int bottom) {
		super.onLayout(changed, left, top, right, bottom);
		int x = getPaddingLeft();
		int y = getPaddingTop();
		for (int i = 0; i < mDayItems.size(); i++) {
			DayItem dayItem = mDayItems.get(i);
			dayItem.mX = x;
			dayItem.mY = y;
			if((i + 1) % 7 == 0){
				x = getPaddingLeft();
				y += mItemWidth + mVerticalSpacing;
			}else{
				x += mItemWidth + mHorizontalSpacing;
			}
		}
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		if(isReLoadData){
			fillDayItem();
			isReLoadData = false;
		}
		int rowSize = 7;
		int horizontalSpacingSize = rowSize == 0 ? 0 : mHorizontalSpacing * (rowSize - 1);
		int contentW = getMeasuredWidth() - getPaddingLeft() - getPaddingRight() - horizontalSpacingSize;
		mItemWidth = contentW / rowSize;
		int lineSize = mDayItems.isEmpty() ? 5 : (mDayItems.size() - 1) / 7 + 1;
		int verticalSpacingSize = lineSize == 0 ? 0 : mVerticalSpacing * (lineSize - 1);
		int contentH = lineSize * mItemWidth + getPaddingTop() + getPaddingBottom() + verticalSpacingSize;
		setMeasuredDimension(getMeasuredWidth(), contentH);
	}

	private void fillDayItem(){
		recycleDayItemAll(mDayItems);
		if(mItems != null){
			for (AbsDayItemData<?> dayItemData : mItems) {
				DayItem dayItem = newDayItem();
				dayItem.dayItemData = dayItemData;
				dayItemData.setCallback(dayItem);
				mDayItems.add(dayItem);
			}
		}
	}

	private void recycleDayItemAll(List<DayItem> dayItems){
		if(dayItems == null){
			return;
		}
		for (DayItem dayItem : dayItems) {
			recycleDayItem(dayItem);
		}
		dayItems.clear();
	}
	
	private void recycleDayItem(DayItem dayItem){
		if(dayItem == null){
			return;
		}
		dayItem.dayItemData = null;
		dayItem.mX = 0;
		dayItem.mY = 0;
		mDayItemCache.add(dayItem);
	}
	
	private DayItem newDayItem(){
		DayItem dayItem = null;
		if(!mDayItemCache.isEmpty()){
			dayItem = mDayItemCache.removeLast();
		}
		if(dayItem == null){
			dayItem = new DayItem();
		}
		return dayItem;
	}
	
	public void setOnCalendarItemClickListener(OnCalendarItemClickListener l) {
		mOnCalendarItemClickListener = l;
	}
	
	@Override
	public Rect getSelectedViewRect() {
		Rect rect = null;
        if (!mDayItems.isEmpty() && mSelection >= 0) {
        	DayItem dayItem = mDayItems.get(mSelection);
        	rect = new Rect((int)(dayItem.mX - mHorizontalSpacing) , (int)(dayItem.mY - mVerticalSpacing)
					, (int)(dayItem.mX + mItemWidth + mHorizontalSpacing),  (int)(dayItem.mY + mItemWidth + mVerticalSpacing));
        }
		return rect;
	}
	
	public static abstract class AbsDayItemData<Data>{
		private Data dateSource;
		private boolean isCurrentMonth;
		private boolean isToday;
		private boolean isSelected;
		private String dateStr;
		private int bgDrawableRes;
		private int textColor;
		private WeakReference<Callback> callback;
		private int tipBgDrawableRes;
		private int tipTextColor;
	    
		public Data getDateSource() {
			return dateSource;
		}
		 
		public void setDateSource(Data dateSource) {
			this.dateSource = dateSource;
			noticeDataChange();
		}
		 
		public boolean isCurrentMonth() {
			return isCurrentMonth;
		}
		 
		public void setCurrentMonth(boolean isCurrentMonth) {
			this.isCurrentMonth = isCurrentMonth;
			noticeDataChange();
		}
		 
		public boolean isToday() {
			return isToday;
		}
		 
		public void setToday(boolean isToday) {
			this.isToday = isToday;
			noticeDataChange();
		}
		 
		public boolean isSelected() {
			return isSelected;
		}
		 
		public void setSelected(boolean isSelected) {
			this.isSelected = isSelected;
			noticeDataChange();
		}
		 
		public String getDateStr() {
			return dateStr;
		}

		public void setDateStr(String dateStr) {
			this.dateStr = dateStr;
			noticeDataChange();
		}
		 
		public int getBgDrawableRes() {
			return bgDrawableRes;
		}
 
		public void setBgDrawableRes(int bgDrawable) {
			this.bgDrawableRes = bgDrawable;
			noticeDataChange();
		}

		private void setCallback(Callback callback){
			this.callback = new WeakReference<Callback>(callback);
		}
		
		public int getTextColor() {
			return textColor;
		}

		public void setTextColor(int textColor) {
			this.textColor = textColor;
			noticeDataChange();
		}

		public void setTipBgDrawableRes(int bgDrawable) {
			this.tipBgDrawableRes = bgDrawable;
			noticeDataChange();
		}
		
		public void setTipTextColor(int textColor) {
			this.tipTextColor = textColor;
			noticeDataChange();
		}
		
		public int getTipBgDrawableRes() {
			return tipBgDrawableRes;
		}
		
		public int getTipTextColor() {
			return tipTextColor;
		}

		public void noticeDataChange(){
			if(callback != null && callback.get() != null){
				callback.get().noticeDataChange();
			}
		}
	}
	
	private Drawable getDrawable(int res){
		if(res > 0){
			return getResources().getDrawable(res);
		}
		return null;
	}
	
	public class DayItem implements Callback{
		private AbsDayItemData<?> dayItemData;
		private int mX;
		private int mY;
		@Override
		public void noticeDataChange() {
			postInvalidate();
		}
		
		protected void onDraw(Canvas canvas) {
			Drawable drawable = getDrawable(dayItemData.getBgDrawableRes());
			if(drawable != null){
				drawable.setBounds(0, 0, mItemWidth, mItemWidth);
				drawable.draw(canvas);
			}
			if(!dayItemData.isCurrentMonth){
				return;
			}
			Drawable tipBgDrawable = getDrawable(dayItemData.getTipBgDrawableRes());
			if(tipBgDrawable != null){
				tipBgDrawable.setBounds(0, 0, mItemWidth, mItemWidth);
				tipBgDrawable.draw(canvas);
			}
			int startX = 0;
			int startY = 0;
			startX += (mItemWidth - mPaint.measureText(dayItemData.dateStr)) / 2;
			if(startX < 0){
				startX = 0;
			}
			startY += (mItemWidth - mPaint.getFontSpacing()) / 2;
			if(startY < 0){
				startY = 0;
			}
			startY += -mFontMetrics.ascent;
			if(tipBgDrawable != null){
				mPaint.setColor(dayItemData.getTipTextColor());
			}else{
				mPaint.setColor(dayItemData.getTextColor());
			}
			canvas.drawText(dayItemData.dateStr, startX, startY, mPaint);
//			if(dayItemData.getTipDrawables() != null){
//				int tipDrawableSize = (mItemWidth - dayItemData.paddingLeft - dayItemData.paddingRight - mDrawablePadding * 2) / 3;
//				int left = dayItemData.paddingLeft;
//				int top = mItemWidth - dayItemData.paddingBottom - tipDrawableSize;
//				for (int i = 0,j = 0; i < dayItemData.getTipDrawables().length; i++) {
//					drawable = getDrawable(dayItemData.getTipDrawables()[i]);
//					if(drawable != null){
//						drawable.setBounds(left, top, left + tipDrawableSize, top + tipDrawableSize);
//						drawable.draw(canvas);
//						if(j < 2){
//							left += tipDrawableSize + mDrawablePadding;
//						}else{
//							top -= tipDrawableSize + mDrawablePadding;
//						}
//						j++;
//					}
//				}
//			}
		}
	}
	
	public interface Callback{
		public void noticeDataChange();
	}
	
	public interface OnCalendarItemClickListener {
		public void onCalendarItemClick(CalendarGridView view, int position);
	}
}
