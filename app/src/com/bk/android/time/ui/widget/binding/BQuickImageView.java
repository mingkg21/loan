package com.bk.android.time.ui.widget.binding;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import com.bk.android.time.ui.widget.QuickImageView;
import com.bk.android.util.DimensionsUtil;

import gueei.binding.IBindableView;
import gueei.binding.ViewAttribute;

public class BQuickImageView extends QuickImageView implements IBindableView<BQuickImageView>{
	public BQuickImageView(Context context) {
		super(context);
	}

	public BQuickImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	@Override
	public ViewAttribute<? extends View, ?> createViewAttribute(
			String attributeId) {
		if (attributeId.equals("circle")) {
			return new Circle(this,attributeId);
		} else if(attributeId.equals("profileColor")){
			return new ProfileColor(this,attributeId);
		} else if(attributeId.equals("strokeWidth")){
			return new StrokeWidth(this,attributeId);
		}
		return BAsyncImageViewUtil.createViewAttribute(attributeId, this);
	}
	
	public static class Circle extends ViewAttribute<BQuickImageView, Object>{
		public Circle(BQuickImageView view,String attributeId){
			super(Object.class, view, attributeId);
		}
		
		@Override
		public Object get() {
			return null;
		}

		@Override
		protected void doSetAttributeValue(Object newValue) {
			if (newValue instanceof Boolean) {
				getView().setCircle((Boolean) newValue);
			}else if (newValue instanceof Integer) {
				getView().setCircle((Integer) newValue == 1 ? true : false);
			}
		}
	}
	
	public static class ProfileColor extends ViewAttribute<BQuickImageView, Integer>{
		public ProfileColor(BQuickImageView view,String attributeId){
			super(Integer.class, view, attributeId);
		}
		
		@Override
		public Integer get() {
			return null;
		}

		@Override
		protected void doSetAttributeValue(Object newValue) {
			if (newValue instanceof Integer) {
				getView().setProfileColor((Integer) newValue);
			}
		}
	}

	public static class StrokeWidth extends ViewAttribute<BQuickImageView, Integer>{
		public StrokeWidth(BQuickImageView view, String attributeId){
			super(Integer.class, view, attributeId);
		}

		@Override
		public Integer get() {
			return null;
		}

		@Override
		protected void doSetAttributeValue(Object newValue) {
			if (newValue instanceof Number) {
				getView().setStrokeWidth(DimensionsUtil.DIPToPX(((Number)newValue).intValue()));
			}
		}
	}
}
