package com.bk.android.time.ui.widget.binding;

import gueei.binding.BindingType;
import gueei.binding.IBindableView;
import gueei.binding.ViewAttribute;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;

import com.bk.android.time.ui.widget.QuickProgressBar;

public class BQuickProgressBar extends QuickProgressBar implements IBindableView<BQuickProgressBar>{
	public BQuickProgressBar(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	@Override
	public ViewAttribute<? extends View, ?> createViewAttribute(
			String attributeId) {
		if (attributeId.equals("progress")) {//设置对于的属性值
			return new ProgressViewAttribute(this,attributeId);
		}else if (attributeId.equals("max")) {
			return new MaxViewAttribute(this,attributeId);
		}else if (attributeId.equals("progressDrawable")) {
			return new ProgressDrawableViewAttribute(this,attributeId);
		}
		return null;
	}
	

	public class ProgressDrawableViewAttribute extends ViewAttribute<BQuickProgressBar, Object> {
		public ProgressDrawableViewAttribute(BQuickProgressBar view,String attributeId) {
			super(Object.class, view, attributeId);
		}
	
		@Override
		protected void doSetAttributeValue(Object newValue) {
			if(getView()==null) return;
			if (newValue==null){
				getView().setProgressDrawable(null);
				return;
			}
			if (newValue instanceof Integer){
				getView().setProgressDrawable(getResources().getDrawable((Integer) newValue));
			}
			if (newValue instanceof Drawable){
				getView().setProgressDrawable((Drawable)newValue);
				return;
			}
		}
	
		@Override
		protected BindingType AcceptThisTypeAs(Class<?> type) {
			return BindingType.OneWay;
		}
	
		@Override
		public Object get() {
			return null;
		}
	}
	
	public static class ProgressViewAttribute extends ViewAttribute<BQuickProgressBar, Integer>{
		public ProgressViewAttribute(BQuickProgressBar view,String attributeId){
			super(Integer.class, view, attributeId);
		}
		
		@Override
		public Integer get() {//获取值
			return null;
		}

		@Override
		protected void doSetAttributeValue(Object newValue) {//设置值
			if (newValue instanceof Integer) {
				getView().setProgress((Integer) newValue);
			}
		}
	}
	
	public static class MaxViewAttribute extends ViewAttribute<BQuickProgressBar, Integer>{
		public MaxViewAttribute(BQuickProgressBar view,String attributeId){
			super(Integer.class, view, attributeId);
		}
		
		@Override
		public Integer get() {//获取值
			return null;
		}

		@Override
		protected void doSetAttributeValue(Object newValue) {//设置值
			if (newValue instanceof Integer) {
				getView().setMax((Integer) newValue);
			}
		}
	}
}
