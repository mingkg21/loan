package com.bk.android.time.ui.widget.binding.command;

import android.view.View;

import com.bk.android.binding.command.BaseCommand;
import com.bk.android.time.ui.widget.SudokuImageView_new;

public abstract class OnSudokuItemClickCommand extends BaseCommand {
	@Override
	protected void onInvoke(View view, Object... args) {
		if(args == null || args.length < 1
				|| !(view instanceof SudokuImageView_new)
				|| !(args[0] instanceof Integer)){
			return;
		}
		onSudokuItemClick((SudokuImageView_new) view,(Integer) args[0]);
	}
	
	public abstract void onSudokuItemClick(SudokuImageView_new view, int position);
}
