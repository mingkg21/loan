package com.bk.android.time.ui.widget.spinner;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

import com.bk.android.assistant.R;
import com.bk.android.ui.widget.AbsDropDownListView;
import com.bk.android.ui.widget.AbsSpinner;
import com.bk.android.util.DimensionsUtil;

public class StaticSpinner extends AbsSpinner {
	public StaticSpinner(Context context) {
		super(context);
	}
	
	public StaticSpinner(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	public StaticSpinner(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}
	
	@Override
	protected void onInit() {
	}

	@Override
	protected View getDefaultEmptyView() {
		return null;
	}
	
	@Override
	protected Drawable getDefaultBackgroundDrawable() {
		return null;
	}
	
	@Override
	protected AbsDropDownListView newDropDownListView() {
		return new DropDownListView(this);
	}

	@Override
	protected boolean isShowSelectionView() {
		return false;
	}
}
