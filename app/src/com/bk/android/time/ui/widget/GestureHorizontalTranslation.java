package com.bk.android.time.ui.widget;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.VelocityTrackerCompat;
import android.support.v4.view.ViewConfigurationCompat;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Scroller;

public class GestureHorizontalTranslation {
	private static final int MAX_SETTLE_DURATION = 500; // ms
	private static final int MIN_DISTANCE_FOR_FLING = 25; // dips
	
	private static final boolean USE_CACHE = true;

	private static final int INVALID_POINTER = -1;

	private boolean mTouchBackEnabled = true;
	private boolean mIsUnableToDrag;
	private int mActivePointerId;
	private float mLastMotionX;
	private float mInitialMotionX;
	private float mLastMotionY;
	private boolean mIsBeingDragged;
	private VelocityTracker mVelocityTracker;
	private int mTouchSlop;
	private View mContentView;
	private boolean mScrollingCacheEnabled;
	private float mScrollX = 0.0f;
	private boolean mScrolling;
	private Scroller mScroller;
	private int mMinimumVelocity;
	private int mMaximumVelocity;
	private boolean mQuickReturn;
	private int mCurItem;
	private int mFlingDistance;
	private OpenedClosedListener mOpenedClosedListener;
	private boolean mFadeEnabled = true;
	private float mFadeDegree = 0.35f;
	private final Paint mFadePaint = new Paint();
	private Drawable mShadowDrawable;
	private int mShadowWidth;
	private int mMarginThreshold;

	public GestureHorizontalTranslation(View view){
		mContentView = view;
		final ViewConfiguration configuration = ViewConfiguration.get(view.getContext());
		mTouchSlop = ViewConfigurationCompat.getScaledPagingTouchSlop(configuration);
		mMinimumVelocity = configuration.getScaledMinimumFlingVelocity();
		mMaximumVelocity = configuration.getScaledMaximumFlingVelocity();
		final float density = view.getContext().getResources().getDisplayMetrics().density;
		mFlingDistance = (int) (MIN_DISTANCE_FOR_FLING * density);
		setCurrentItem(1, false);
		mScroller = new Scroller(view.getContext(),AnimationUtils.loadInterpolator(view.getContext(), android.R.anim.accelerate_interpolator));
	}
	
	public void setMarginThreshold(int marginThreshold) {
		mMarginThreshold = marginThreshold;
	}
	
	public void setOpenedClosedListener(OpenedClosedListener l){
		mOpenedClosedListener = l;
	}
	
	public void setTouchBackEnabled(boolean b) {
		mTouchBackEnabled = b;
	}
	
	public boolean isTouchBackEnabled(){
		return mTouchBackEnabled;
	}
	
	public void setShadowDrawable(Drawable shadow) {
		mShadowDrawable = shadow;
		mContentView.invalidate();
	}

	public void setShadowWidth(int width) {
		mShadowWidth = width;
		mContentView.invalidate();
	}
	
	public void setFadeEnabled(boolean b) {
		mFadeEnabled = b;
	}
	
	public void setFadeDegree(float degree) {
		if (degree > 1.0f || degree < 0.0f)
			throw new IllegalStateException("The BehindFadeDegree must be between 0.0f and 1.0f");
		mFadeDegree = degree;
	}
	
	public boolean onInterceptTouchEvent(MotionEvent ev) {
		if (!mTouchBackEnabled)
			return false;

		final int action = ev.getAction() & MotionEventCompat.ACTION_MASK;
		if (action == MotionEvent.ACTION_CANCEL
				|| action == MotionEvent.ACTION_UP
				|| (action != MotionEvent.ACTION_DOWN && mIsUnableToDrag)) {
			endDrag();
			return false;
		}

		switch (action) {
		case MotionEvent.ACTION_MOVE:
			determineDrag(ev);
			break;
		case MotionEvent.ACTION_DOWN:
			int index = MotionEventCompat.getActionIndex(ev);
			mActivePointerId = MotionEventCompat.getPointerId(ev, index);
			if (mActivePointerId == INVALID_POINTER)
				break;
			mLastMotionX = mInitialMotionX = MotionEventCompat.getX(ev, index);
			mLastMotionY = MotionEventCompat.getY(ev, index);
			if (thisTouchAllowed(ev)) {
				mIsBeingDragged = false;
				mIsUnableToDrag = false;
				if (isMenuOpen() && menuTouchInQuickReturn(mCurItem, ev.getX() + mScrollX)) {
					mQuickReturn = true;
				}
			} else {
				mIsUnableToDrag = true;
			}
			break;
		case MotionEventCompat.ACTION_POINTER_UP:
			onSecondaryPointerUp(ev);
			break;
		}

		if (!mIsBeingDragged) {
			if (mVelocityTracker == null) {
				mVelocityTracker = VelocityTracker.obtain();
			}
			mVelocityTracker.addMovement(ev);
		}
		return mIsBeingDragged || mQuickReturn;
	}
	
	public boolean onTouchEvent(MotionEvent ev) {
		if (!mTouchBackEnabled)
			return false;

		if (!mIsBeingDragged)
			return false;

		// if (!mIsBeingDragged && !mQuickReturn)
		// return false;

		final int action = ev.getAction();

		if (mVelocityTracker == null) {
			mVelocityTracker = VelocityTracker.obtain();
		}
		mVelocityTracker.addMovement(ev);

		switch (action & MotionEventCompat.ACTION_MASK) {
		case MotionEvent.ACTION_DOWN:
			/*
			 * If being flinged and user touches, stop the fling. isFinished
			 * will be false if being flinged.
			 */
			completeScroll();
			// Remember where the motion event started
			int index = MotionEventCompat.getActionIndex(ev);
			mActivePointerId = MotionEventCompat.getPointerId(ev, index);
			mLastMotionX = mInitialMotionX = ev.getX();
			break;
		case MotionEvent.ACTION_MOVE:
			if (!mIsBeingDragged) {
				determineDrag(ev);
				if (mIsUnableToDrag)
					return false;
			}
			if (mIsBeingDragged) {
				// Scroll to follow the motion event
				final int activePointerIndex = getPointerIndex(ev,
						mActivePointerId);
				if (mActivePointerId == INVALID_POINTER)
					break;
				final float x = MotionEventCompat.getX(ev, activePointerIndex);
				final float deltaX = mLastMotionX - x;
				mLastMotionX = x;
				float oldScrollX = getScrollX();
				float scrollX = oldScrollX + deltaX;
				final float leftBound = getLeftBound();
				final float rightBound = getRightBound();
				if (scrollX < leftBound) {
					scrollX = leftBound;
				} else if (scrollX > rightBound) {
					scrollX = rightBound;
				}
				// Don't lose the rounded component
				mLastMotionX += scrollX - (int) scrollX;
				scrollTo((int) scrollX, getScrollY());
			}
			break;
		case MotionEvent.ACTION_UP:
			if (mIsBeingDragged) {
				final VelocityTracker velocityTracker = mVelocityTracker;
				velocityTracker.computeCurrentVelocity(1000, mMaximumVelocity);
				int initialVelocity = (int) VelocityTrackerCompat.getXVelocity(
						velocityTracker, mActivePointerId);
				final int scrollX = getScrollX();
				final float pageOffset = (float) (scrollX - getDestScrollX(mCurItem)) / getBehindWidth();
				final int activePointerIndex = getPointerIndex(ev,
						mActivePointerId);
				if (mActivePointerId != INVALID_POINTER) {
					final float x = MotionEventCompat.getX(ev,
							activePointerIndex);
					final int totalDelta = (int) (x - mInitialMotionX);
					int nextPage = determineTargetPage(pageOffset, initialVelocity, totalDelta);
					setCurrentItemInternal(nextPage, true, true,initialVelocity);
				} else {
					setCurrentItemInternal(mCurItem, true, true,initialVelocity);
				}
				mActivePointerId = INVALID_POINTER;
				endDrag();
			} else if (mQuickReturn && menuTouchInQuickReturn(mCurItem,ev.getX() + mScrollX)) {
				// close the menu
				setCurrentItem(1);
				endDrag();
			}
			break;
		case MotionEvent.ACTION_CANCEL:
			if (mIsBeingDragged) {
				setCurrentItemInternal(mCurItem, true, true);
				mActivePointerId = INVALID_POINTER;
				endDrag();
			}
			break;
		case MotionEventCompat.ACTION_POINTER_DOWN: {
			final int indexx = MotionEventCompat.getActionIndex(ev);
			mLastMotionX = MotionEventCompat.getX(ev, indexx);
			mActivePointerId = MotionEventCompat.getPointerId(ev, indexx);
			break;
		}
		case MotionEventCompat.ACTION_POINTER_UP:
			onSecondaryPointerUp(ev);
			int pointerIndex = getPointerIndex(ev, mActivePointerId);
			if (mActivePointerId == INVALID_POINTER)
				break;
			mLastMotionX = MotionEventCompat.getX(ev, pointerIndex);
			break;
		}
		return true;
	}
	
	public void dispatchDraw(Canvas canvas) {
		drawShadow(canvas);
		drawFade(canvas,getPercentOpen());
	}
	
	public void computeScroll() {
		if (!mScroller.isFinished()) {
			if (mScroller.computeScrollOffset()) {
				int oldX = getScrollX();
				int oldY = getScrollY();
				int x = mScroller.getCurrX();
				int y = mScroller.getCurrY();

				if (oldX != x || oldY != y) {
					scrollTo(x, y);
				}

				// Keep on drawing until the animation has finished.
				mContentView.invalidate();
				return;
			}
		}
		// Done with scroll, clean up state.
		completeScroll();
	}
	
	private boolean thisTouchAllowed(MotionEvent ev) {
		int x = (int) (ev.getX() + mScrollX);
		if (isMenuOpen()) {
			return menuTouchInQuickReturn(mCurItem, x);
		} else {
			return marginTouchAllowed(x);
		}
	}

	private void drawShadow(Canvas canvas) {
		if (mShadowDrawable == null || mShadowWidth <= 0) return;
		int left = mContentView.getLeft() - mShadowWidth;
		mShadowDrawable.setBounds(left, 0, left + mShadowWidth, mContentView.getHeight());
		mShadowDrawable.draw(canvas);
	}

	private void drawFade(Canvas canvas, float openPercent) {
		if (!mFadeEnabled) return;
		final int alpha = (int) (mFadeDegree * 255 * Math.abs(1-openPercent));
		mFadePaint.setColor(Color.argb(alpha, 0, 0, 0));
		int left = mContentView.getLeft() - getBehindWidth();
		int right = mContentView.getLeft();
		canvas.drawRect(left, 0, right, mContentView.getHeight(), mFadePaint);
	}
	
	private float getRightBound() {
		return mContentView.getLeft();
	}

	private float getLeftBound() {
		return mContentView.getLeft() - getBehindWidth();
	}

	private int determineTargetPage(float pageOffset, int velocity, int deltaX) {
		int targetPage = mCurItem;
		if (Math.abs(deltaX) > mFlingDistance
				&& Math.abs(velocity) > mMinimumVelocity) {
			if (velocity > 0 && deltaX > 0) {
				targetPage -= 1;
			} else if (velocity < 0 && deltaX < 0) {
				targetPage += 1;
			}
		} else {
			targetPage = (int) Math.round(mCurItem + pageOffset);
		}
		return targetPage;
	}
	
	private int getBehindWidth() {
		return mContentView.getWidth();
	}
	
	private int getDestScrollX(int page) {
		switch (page) {
		case 0:
			return mContentView.getLeft() - getBehindWidth();
		case 2:
			return mContentView.getLeft();
		case 1://1:前景
			return mContentView.getLeft();
		}
		return 0;
	}
	
	private void scrollTo(int x, int y) {
		mScrollX = x;
		mContentView.scrollTo(x, y);
		mContentView.invalidate();
	}

	public int getScrollX() {
		return mContentView.getScrollX();
	}
	
	private int getScrollY() {
		return mContentView.getScrollY();
	}

	private boolean menuTouchInQuickReturn(int currPage, float x) {
		return x >= mContentView.getLeft();
	}
	
	private boolean marginTouchAllowed(int x) {
		if(mMarginThreshold < 0){
			return true;
		}
		if(mMarginThreshold == 0){
			return false;
		}
		int left = mContentView.getLeft();
		return (x >= left && x <= mMarginThreshold + left);
	}
	
	/**
	 * Set the currently selected page. If the CustomViewPager has already been
	 * through its first layout there will be a smooth animated transition
	 * between the current item and the specified item.
	 * 
	 * @param item
	 *            Item index to select
	 */
	public void setCurrentItem(int item) {
		setCurrentItemInternal(item, true, false);
	}

	/**
	 * Set the currently selected page.
	 * 
	 * @param item
	 *            Item index to select
	 * @param smoothScroll
	 *            True to smoothly scroll to the new item, false to transition
	 *            immediately
	 */
	public void setCurrentItem(int item, boolean smoothScroll) {
		setCurrentItemInternal(item, smoothScroll, false);
	}
	
	public void setCurrentItem(int item, boolean smoothScroll, boolean always) {
		setCurrentItemInternal(item, smoothScroll, always);
	}


	public int getCurrentItem() {
		return mCurItem;
	}

	void setCurrentItemInternal(int item, boolean smoothScroll, boolean always) {
		setCurrentItemInternal(item, smoothScroll, always, 0);
	}

	void setCurrentItemInternal(int item, boolean smoothScroll, boolean always,
			int velocity) {
		if (!always && mCurItem == item) {
			setScrollingCacheEnabled(false);
			return;
		}

		item = getMenuPage(item);

		mCurItem = item;
		final int destX = getDestScrollX(mCurItem);
		if (smoothScroll) {
			smoothScrollTo(destX, 0, velocity);
		} else {
			completeScroll();
			scrollTo(destX, 0);
		}
	}
	
	public int getMenuPage(int page) {
		page = (page > 1) ? 2 : ((page < 1) ? 0 : page);
		if (page > 1) {
			return 0;
		} else {
			return page;
		}
	}
	
	void smoothScrollTo(int x, int y, int velocity) {
		int sx = getScrollX();
		int sy = getScrollY();
		int dx = x - sx;
		int dy = y - sy;
		if (dx == 0 && dy == 0) {
			completeScroll();
			if (mOpenedClosedListener != null){
				mOpenedClosedListener.onOpenedClosedChange(isMenuOpen());
			}
			return;
		}

		setScrollingCacheEnabled(true);
		mScrolling = true;

		final int width = getBehindWidth();
		final int halfWidth = width / 2;
		final float distanceRatio = Math.min(1f, 1.0f * Math.abs(dx) / width);
		final float distance = halfWidth + halfWidth * distanceInfluenceForSnapDuration(distanceRatio);

		int duration = 0;
		velocity = Math.abs(velocity);
		if (velocity > 0) {
			duration = 4 * Math.round(1000 * Math.abs(distance / velocity));
		} else {
			final float pageDelta = (float) Math.abs(dx) / width;
			duration = (int) ((pageDelta + 1) * 100);
			duration = MAX_SETTLE_DURATION;
		}
		duration = Math.min(duration, MAX_SETTLE_DURATION);

		mScroller.startScroll(sx, sy, dx, dy, duration);
		mContentView.invalidate();
	}
	
	float distanceInfluenceForSnapDuration(float f) {
		f -= 0.5f; // center the values about 0.
		f *= 0.3f * Math.PI / 2.0f;
		return (float) Math.sin(f);
	}
	
	private void completeScroll() {
		boolean needPopulate = mScrolling;
		if (needPopulate) {
			// Done with scroll, no longer want to cache view drawing.
			setScrollingCacheEnabled(false);
			mScroller.abortAnimation();
			int oldX = getScrollX();
			int oldY = getScrollY();
			int x = mScroller.getCurrX();
			int y = mScroller.getCurrY();
			if (oldX != x || oldY != y) {
				scrollTo(x, y);
			}
			if (mOpenedClosedListener != null){
				mOpenedClosedListener.onOpenedClosedChange(isMenuOpen());
			}
		}
		mScrolling = false;
	}
	
	private boolean thisSlideAllowed(float dx) {
		boolean allowed = false;
		if (isMenuOpen()) {
			allowed = dx < 0;
		} else {
			allowed = dx > 0;
		}
		return allowed;
	}
	
	private void onSecondaryPointerUp(MotionEvent ev) {
		final int pointerIndex = MotionEventCompat.getActionIndex(ev);
		final int pointerId = MotionEventCompat.getPointerId(ev, pointerIndex);
		if (pointerId == mActivePointerId) {
			// This was our active pointer going up. Choose a new
			// active pointer and adjust accordingly.
			final int newPointerIndex = pointerIndex == 0 ? 1 : 0;
			mLastMotionX = MotionEventCompat.getX(ev, newPointerIndex);
			mActivePointerId = MotionEventCompat.getPointerId(ev,
					newPointerIndex);
			if (mVelocityTracker != null) {
				mVelocityTracker.clear();
			}
		}
	}
	
	private void startDrag() {
		mIsBeingDragged = true;
		mQuickReturn = false;
	}

	private void endDrag() {
		mQuickReturn = false;
		mIsBeingDragged = false;
		mIsUnableToDrag = false;
		mActivePointerId = INVALID_POINTER;
		if (mVelocityTracker != null) {
			mVelocityTracker.recycle();
			mVelocityTracker = null;
		}
	}
	
	private void determineDrag(MotionEvent ev) {
		final int activePointerId = mActivePointerId;
		final int pointerIndex = getPointerIndex(ev, activePointerId);
		if (activePointerId == INVALID_POINTER
				|| pointerIndex == INVALID_POINTER)
			return;
		final float x = MotionEventCompat.getX(ev, pointerIndex);
		final float dx = x - mLastMotionX;
		final float xDiff = Math.abs(dx);
		final float y = MotionEventCompat.getY(ev, pointerIndex);
		final float dy = y - mLastMotionY;
		final float yDiff = Math.abs(dy);
		if (xDiff > (isMenuOpen() ? mTouchSlop / 2 : mTouchSlop)
				&& xDiff > yDiff && thisSlideAllowed(dx)) {
			startDrag();
			mLastMotionX = x;
			mLastMotionY = y;
			setScrollingCacheEnabled(true);
			// TODO add back in touch slop check
		} else if (xDiff > mTouchSlop) {
			mIsUnableToDrag = true;
		}
	}

	private int getPointerIndex(MotionEvent ev, int id) {
		int activePointerIndex = MotionEventCompat.findPointerIndex(ev, id);
		if (activePointerIndex == -1)
			mActivePointerId = INVALID_POINTER;
		return activePointerIndex;
	}
	
	public boolean isMenuOpen() {
		return mCurItem == 0 || mCurItem == 2;
	}
	
	private void setScrollingCacheEnabled(boolean enabled) {
		if (mScrollingCacheEnabled != enabled) {
			mScrollingCacheEnabled = enabled;
			if (USE_CACHE) {
				if(mContentView instanceof ViewGroup){
					ViewGroup viewGroup = (ViewGroup) mContentView;
					final int size = viewGroup.getChildCount();
					for (int i = 0; i < size; ++i) {
						final View child = viewGroup.getChildAt(i);
						if (child.getVisibility() != View.GONE) {
							child.setDrawingCacheEnabled(enabled);
						}
					}
				}else{
					mContentView.setDrawingCacheEnabled(enabled);
				}
			}
		}
	}
	
	protected float getPercentOpen() {
		return Math.abs(mScrollX - mContentView.getLeft()) / getBehindWidth();
	}
	
	public interface OpenedClosedListener{
		public void onOpenedClosedChange(boolean isOpen);
	}
}
