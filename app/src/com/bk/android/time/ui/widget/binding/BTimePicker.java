package com.bk.android.time.ui.widget.binding;

import gueei.binding.BindingType;
import gueei.binding.IBindableView;
import gueei.binding.ViewAttribute;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TimePicker;
import android.widget.TimePicker.OnTimeChangedListener;

/**
 * @author mingkg21
 * @email mingkg21@gmail.com
 * @date 2015年2月5日
 *
 */
public class BTimePicker extends TimePicker implements IBindableView<BTimePicker>,
		OnTimeChangedListener {
	
	private GetHour mGetHour;
	private GetMinute mGetMinute;

	public BTimePicker(Context context, AttributeSet attrs) {
		super(context, attrs);
		setIs24HourView(true);
		setOnTimeChangedListener(this);
	}

	@Override
	public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
		if(mGetHour != null){
			mGetHour.notifyChanged();
		}
		if(mGetMinute != null){
			mGetMinute.notifyChanged();
		}
	}
	
	@Override
	public ViewAttribute<? extends View, ?> createViewAttribute(
			String attributeId) {
		if (attributeId.equals("getHour")) {
			if(mGetHour == null){
				mGetHour = new GetHour(this, attributeId);
			}
			return mGetHour;
		} else if (attributeId.equals("getMinute")) {
			if(mGetMinute == null){
				mGetMinute = new GetMinute(this, attributeId);
			}
			return mGetMinute;
		}
		return null;
	}

	public static class GetHour extends ViewAttribute<BTimePicker, Integer> {
		
		private boolean isInit;
		
		public GetHour(BTimePicker view, String attributeId) {
			super(Integer.class, view, attributeId);
		}

		@Override
		public Integer get() {
			return getView().getCurrentHour();
		}

		@Override
		protected void doSetAttributeValue(Object newValue) {
			if(isInit) {
				return;
			}
			if(newValue instanceof Integer) {
				isInit = true;
				getView().setCurrentHour((Integer)newValue);
			}
		}

		@Override
		protected BindingType AcceptThisTypeAs(Class<?> type) {
			return BindingType.TwoWay;
		}
	}

	public static class GetMinute extends ViewAttribute<BTimePicker, Integer> {
		
		private boolean isInit;
		
		public GetMinute(BTimePicker view, String attributeId) {
			super(Integer.class, view, attributeId);
		}

		@Override
		public Integer get() {
			return getView().getCurrentMinute();
		}

		@Override
		protected void doSetAttributeValue(Object newValue) {
			if(isInit) {
				return;
			}
			if(newValue instanceof Integer) {
				isInit = true;
				getView().setCurrentMinute((Integer)newValue);
			}
		}

		@Override
		protected BindingType AcceptThisTypeAs(Class<?> type) {
			return BindingType.TwoWay;
		}
	}


}
