package com.bk.android.time.ui.widget;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.os.Parcelable;
import android.text.Editable;
import android.text.Selection;
import android.text.SpanWatcher;
import android.text.Spannable;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.ArrowKeyMovementMethod;
import android.text.method.LinkMovementMethod;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver.OnScrollChangedListener;
import android.widget.AbsListView;
import android.widget.ScrollView;

import com.bk.android.app.BaseActivity;
import com.bk.android.assistant.R;
import com.bk.android.time.entity.MixDataInfo;
import com.bk.android.time.entity.MixDataInfo.LinkInfo;
import com.bk.android.time.model.lightweight.AddImgModel;
import com.bk.android.time.model.lightweight.AddImgModel.BitmapInfo;
import com.bk.android.time.ui.widget.span.AsyncDrawableSpan;
import com.bk.android.time.ui.widget.span.ISpanDrawableView;
import com.bk.android.time.ui.widget.span.LinkSpan;
import com.bk.android.time.ui.widget.span.TextDrawableSpan;
import com.bk.android.time.ui.widget.span.TextFillSpan;
import com.bk.android.time.util.FaceUtil;
import com.bk.android.ui.widget.PatchedEditText;
import com.bk.android.util.DimensionsUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

public class HtmlTextView extends PatchedEditText implements ISpanDrawableView, OnGestureListener, OnScrollChangedListener{
	private ChangeWatcher mChangeWatcher;
	private ArrayList<AsyncDrawableSpan> mDrawableSpans;
	private HashMap<AsyncDrawableSpan, Integer> mDrawableHeights;
	private View mScrollView;
	private View mAbsListViewItem;
	private int mAbsListViewItemTop;
	private GestureDetector mGestureDetector;
	private boolean isFill;
	private Integer mLostHtmlHashCode;
	private Object mHeadHintSpan;
	private int mStepSize;
	private int mImgPadding = 5;
	private int mPaddingLeft;
	private int mPaddingRight;
	private int mPaddingTop;
	private int mPaddingBottom;
	private OnSpanClickListener mOnSpanClickListener;
	private OnKeyEventPreImeListener mOnKeyEventPreImeListener;
	private boolean isRequestViewLayout;
	private int mLastWidthMeasure;
	private boolean mUnEnabledCanTouch = true;
	private boolean mTouching = false;
	
	public HtmlTextView(Context context) {
		super(context);
		init();
	}
	
	public HtmlTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}
	
	@Override
	public void setEnabled(boolean enabled) {
		if(!enabled){
			setMovementMethod(LinkMovementMethod.getInstance());
		}else{
			setMovementMethod(ArrowKeyMovementMethod.getInstance());
		}
		super.setEnabled(enabled);
		setFocusable(enabled);
		if(enabled){
			setFocusableInTouchMode(true);
			requestFocus();
			int index = getText().length() - 1;
			if(index < 0){
				index = 0;
			}
			Selection.setSelection(getText(),index);
		}
	}
	
	private void init(){
		setEnabled(false);
		setFill(true);
		mGestureDetector = new GestureDetector(getContext(),this);
		mChangeWatcher = new ChangeWatcher();
		mDrawableSpans = new ArrayList<AsyncDrawableSpan>();
		mDrawableHeights = new HashMap<AsyncDrawableSpan, Integer>();
		getText().setSpan(mChangeWatcher, 0, 0, Spanned.SPAN_INCLUSIVE_INCLUSIVE | (100 << Spanned.SPAN_PRIORITY_SHIFT));
		addTextChangedListener(new TextWatcher() {
			int needAddIndex = -2;
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,int after) {
				if(count > 0){
					Object[] spans = getText().getSpans(start, start + count, Object.class);
					if(spans != null){
						for (Object span : spans) {
							if(span instanceof AsyncDrawableSpan){
								getText().removeSpan(span);
							}else if(span.equals(mHeadHintSpan)){
								removeHeadHintSpan();
							}
						}
						invalidate();
					}
				}
				if(needAddIndex == -1){
					needAddIndex = -2;
				}else{
					if(start < s.length() && after > 0 && s.charAt(start) == '\uFFFC'){
						needAddIndex = start + after;
					}
				}
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				if(needAddIndex > 0){
					int index = needAddIndex;
					needAddIndex = -1;
					s.insert(index, "\n");
					setSelection(index);
				}
			}
		});
	}
	
	@Override
	protected void onSelectionChanged(int selStart, int selEnd) {
		super.onSelectionChanged(selStart, selEnd);
		if(mHeadHintSpan != null && selStart == 0){
			setSelection(1);
		}
	}
	
	@Override
	protected void onAttachedToWindow() {
		super.onAttachedToWindow();
		getViewTreeObserver().addOnScrollChangedListener(this);
		for (AsyncDrawableSpan span : mDrawableSpans) {
			span.onAttachedToWindow();
		}
	}
	
	@Override
	protected void onDetachedFromWindow() {
		super.onDetachedFromWindow();
		getViewTreeObserver().removeOnScrollChangedListener(this);
		for (AsyncDrawableSpan span : mDrawableSpans) {
			span.onDetachedFromWindow();
		}
	}
	
	private String setItemImgPath(String path){
		if(path.startsWith("file://") || path.startsWith("http://") || path.startsWith("android.resource://")) {
			return path;
		} else {
			return "file://" + path;
		}
	}
	
	public void setHtml(String html){
		if(TextUtils.isEmpty(html)){
			mLostHtmlHashCode = null;
			getText().clear();
			return;
		}
		if(html != null && mLostHtmlHashCode != null && mLostHtmlHashCode == html.hashCode()){
			return;
		}
		mLostHtmlHashCode = html != null ? html.hashCode() : null;
		mStepSize = 0;
		getText().clear();
		ArrayList<MixDataInfo> mixDataInfos = MixDataInfo.getMixDataInfos(html);
		if(!mixDataInfos.isEmpty()){
			int i = 0;
			boolean isImg = true;
			for (MixDataInfo mixDataInfo : mixDataInfos) {
				if(!TextUtils.isEmpty(mixDataInfo.getText())){
					Editable faceText = FaceUtil.loadFace(mixDataInfo.getText());
					if(faceText != null){
						if(!isImg){
							getText().append("\n");
						}
						int start = getText().length();
						if(mixDataInfo.getType() == MixDataInfo.TYPE_STEP){
							mStepSize++;
							getText().append(mStepSize+".");
						}else if(mixDataInfo.getType() == MixDataInfo.TYPE_STEP_TITLE
								|| mixDataInfo.getType() == MixDataInfo.TYPE_STOCK_TITLE){
							getText().append("\n");
							start = getText().length();
						}
						getText().append(faceText);
						if(mixDataInfo.getType() == MixDataInfo.TYPE_STOCK){
							String[] textArr = mixDataInfo.getText().split("-");
							if(textArr.length == 2){
								TextFillSpan textFillSpan = new TextFillSpan(this, textArr[0], textArr[1]);
								getText().setSpan(textFillSpan, start,getText().length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
							}
						}else if(mixDataInfo.getType() == MixDataInfo.TYPE_STEP_TITLE
								|| mixDataInfo.getType() == MixDataInfo.TYPE_STOCK_TITLE){
//							TextBackgroundSpan textBackgroundSpan = new TextBackgroundSpan(this, faceText.toString() ,R.drawable.ic_step_title_bg);
//							getText().setSpan(textBackgroundSpan, start,getText().length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
							getText().append("\n");
						}
						ArrayList<LinkInfo> linkInfos = mixDataInfo.getLinkInfos();
						if(linkInfos != null){
							for (LinkInfo linkInfo : linkInfos) {
								LinkSpan linkSpan = new LinkSpan(linkInfo.getData());
								getText().setSpan(linkSpan, linkInfo.getStart(), linkInfo.getEnd(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
							}
						}
					}
					isImg = false;
				}else if(!TextUtils.isEmpty(mixDataInfo.getImgUrl())){
					addImage(mixDataInfo,mixDataInfo.getImgUrl(),mixDataInfo.getImgW(),mixDataInfo.getImgH(),true);
					if(i == mixDataInfos.size() - 1 && isEnabled()){
						getText().append("\n");
					}
					isImg = true;
				}
				i++;
			}
		}else{
			getText().append(html != null ? html : "");
		}
		requestLayout();
	}
	
	public String getHtml(){
		StringBuffer html = new StringBuffer();
		AsyncDrawableSpan[] spans = getText().getSpans(0, getText().length(), AsyncDrawableSpan.class);
		LinkedList<DrawableSpanInfo> spanList = new LinkedList<DrawableSpanInfo>();
		if(spans != null){
			for (AsyncDrawableSpan span : spans) {
				int star = getText().getSpanStart(span);
				int end = getText().getSpanEnd(span);
				DrawableSpanInfo spanInfo = new DrawableSpanInfo(span,star,end);
				int location = spanList.size();
				for (int i = 0;i < spanList.size();i++) {
					DrawableSpanInfo drawableSpanInfo = spanList.get(i);
					if(star < drawableSpanInfo.mStart){
						location = i;
						break;
					}
				}
				spanList.add(location, spanInfo);
			}
		}
		int j = 0;
		for (DrawableSpanInfo spanInfo : spanList) {
			AsyncDrawableSpan span = spanInfo.mSpan;
			int star = spanInfo.mStart;
			int end = spanInfo.mEnd;
			if(j < star){
				CharSequence text = getText().subSequence(j, star);
				if(text.length() > 0 && text.charAt(text.length() - 1) == '\n'){
					text = text.subSequence(0, text.length() - 1);
				}
				if(text.length() > 0){
					html.append(MixDataInfo.textDataToString(text.toString()));
				}
			}
			html.append(MixDataInfo.photoDataToString(span.getUrl(), span.getImgWidth(), span.getImgHeight()));
			j = end;
		}
		if(j < getText().length()){
			CharSequence text = getText().subSequence(j, getText().length());
			if(text.length() > 0){
				if(text.charAt(0) == '\n'){
					text = text.subSequence(1, text.length());
				}
				if(text.length() > 0){
					html.append(MixDataInfo.textDataToString(text.toString()));
				}
			}
		}
		html.trimToSize();
		return html.toString();
	}
	
	
	public void addFace(String key){
		FaceUtil.addFace(key, getText(), getSelectionStart());
	}
	
	public void setHeadHintSpan(Object span){
		if(span instanceof String){
			span = new TextDrawableSpan((String) span);
		}
		if(mHeadHintSpan != null && mHeadHintSpan.equals(span)){
			return;
		}
		removeHeadHintSpan();
		mHeadHintSpan = span;
		if(mHeadHintSpan != null){
			if(mHeadHintSpan instanceof TextDrawableSpan){
				((TextDrawableSpan) mHeadHintSpan).setSpanDrawableView(this);
			}
			String addStr = "\uFFFC";
			getText().insert(0 , addStr);
			getText().setSpan(span, 0, addStr.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		}
	}
	
	private void removeHeadHintSpan(){
		if(mHeadHintSpan != null){
			getText().removeSpan(mHeadHintSpan);
			if(mHeadHintSpan instanceof TextDrawableSpan){
				((TextDrawableSpan) mHeadHintSpan).setSpanDrawableView(null);
			}
			mHeadHintSpan = null;
		}
	}
	
	public void addImage(String url,int width,int height){
		addImage(null,url,width,height,false);
	}
	
	private void addImage(MixDataInfo mixDataInfo,String url,int width,int height,boolean isInit){
		int start = 0;
		String newUrl = setItemImgPath(url);
		if(isInit){
			start = getText().length();
		}else{
			start = getSelectionStart();
		}
		boolean isEnd = start == getText().length();
		if(start > 0 && getText().charAt(start - 1) != '\n' && getText().charAt(start - 1) != '\uFFFC' && isFill){
			getText().insert(start , "\n");
			start ++;
		}
		if(start < 0){
			start = 0;
		}
		String addStr = "\uFFFC";
		getText().insert(start , addStr);
		AsyncDrawableSpan span = new AsyncDrawableSpan(mixDataInfo,newUrl, width,height,R.drawable.ic_launcher, HtmlTextView.this);
		if(!isFill){
			int padding = DimensionsUtil.DIPToPX(1);
			span.setPadding(padding, padding, padding, padding);
		}else {
			int padding = DimensionsUtil.DIPToPX(mImgPadding);
			span.setPadding(padding, DimensionsUtil.DIPToPX(5), padding, DimensionsUtil.DIPToPX(5));
		}
		getText().setSpan(span, start,start + addStr.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		if(!isInit && isEnd && isFill){
			getText().insert(start + addStr.length() , "\n");
		}
		if(!isInit){
			requestLayout();
		}
	}
	
	public void setFill(boolean fill){
		isFill = fill;
	}
	
	public void setImgPadding(int padding){
		mImgPadding = padding;
	}
	
	@Override
	public int getMaxViewHeight() {
		if(isFill){
			return Integer.MAX_VALUE;
		}else{
			return 0;
		}
	}

	@Override
	public int getMaxViewWidth() {
		if(getLayout() == null){
			if(getMeasuredWidth() == 0){
				return mLastWidthMeasure;
			}else{
				return getMeasuredWidth();
			}
		}else{
			return getLayout().getWidth();
		}
	}

	@Override
	public boolean isEnabled() {
		boolean isEnabled = super.isEnabled();
		if((mUnEnabledCanTouch && mTouching) || isEnabled){
			return true;
		}
		return isEnabled;
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		boolean hand = false;
		mTouching = true;
		if(!hand){
			hand = mGestureDetector.onTouchEvent(event);
		}
		if(!hand){
			hand = super.onTouchEvent(event);
		}
		mTouching = false;
		return hand;
	}
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		mPaddingLeft = getPaddingLeft();
		mPaddingRight = getPaddingRight();
		mPaddingTop = getPaddingTop();
		mPaddingBottom = getPaddingBottom();
		int newLastWidthMeasure = getDefaultSize(getSuggestedMinimumWidth(), widthMeasureSpec) - getPaddingLeft() - getPaddingRight();
		if(mLastWidthMeasure != newLastWidthMeasure){
			mLastWidthMeasure = newLastWidthMeasure;
			if(!mDrawableSpans.isEmpty()){
				requestViewLayout(mDrawableSpans.get(0));
			}
		}
		try {
			super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		} catch (Exception e) {}
	}
	
	@Override
	protected void onLayout(boolean changed, int left, int top, int right,int bottom) {
		super.onLayout(changed, left, top, right, bottom);
		findScrollView(this);
	}
	
	private void findScrollView(View view){
		if(view instanceof ScrollView){
			mScrollView = (ScrollView) view;
			return;
		}else if(view.getParent() instanceof AbsListView){
			mAbsListViewItem = view;
			return;
		}else if(view.getParent() != null && view.getParent() instanceof View){
			findScrollView((View) view.getParent());
		}
		if(view.equals(this)){
			mAbsListViewItemTop = 0;
		}
		mAbsListViewItemTop += view.getTop();
	}
	
	private int findScrollY(View view){
		if(mScrollView != null){
			return mScrollView.getScrollY();
		}else if(mAbsListViewItem != null){
			return -mAbsListViewItemTop - mAbsListViewItem.getTop();
		}
		return getScrollY();
	}
	
	private int getViewHeight(){
		if(mDrawableSpans.size() <= 2){//XXX listView Item View 不是第一项的时候计算释放图片有误，临时解决方案
			return Integer.MAX_VALUE / 2;
		}
		if(mScrollView != null){
			if(mScrollView.getHeight() < getHeight()){
				return mScrollView.getHeight();
			}
		}
		if(mAbsListViewItem != null){
			View parentView = (View) mAbsListViewItem.getParent();
			if(parentView.getHeight() < getHeight()){
				return parentView.getHeight();
			}
		}
		return getHeight();
	}
	
	@Override
	protected void dispatchDraw(Canvas canvas) {
		int scrollY = findScrollY(this);
		for (AsyncDrawableSpan span : mDrawableSpans) {
			span.drawLastLocal(canvas,scrollY,getViewHeight());
		}
		super.dispatchDraw(canvas);
	}
	
	@Override
	public void requestViewLayout(final Object span) {
		post(new Runnable() {
			@Override
			public void run() {
				if(mDrawableSpans.contains(span)){
					isRequestViewLayout = true;
					int start = getText().getSpanStart(span);
					int end = getText().getSpanEnd(span);
					int flags = getText().getSpanFlags(span);
					if(start >= 0){
						char charAt = getText().charAt(start);
						getText().delete(start, start + 1);
						getText().insert(start,String.valueOf(charAt));
						getText().setSpan(span, start, end, flags);
					}
					isRequestViewLayout = false;
				}
			}
		});
	}
	
	@Override
	protected void dispatchSaveInstanceState(SparseArray<Parcelable> container) {
//		super.dispatchSaveInstanceState(container);
	}
	
	@Override
	protected void dispatchRestoreInstanceState(SparseArray<Parcelable> container) {
//		super.dispatchRestoreInstanceState(container);
	}
	
	@Override
	public boolean onSingleTapUp(MotionEvent e) {
		int position = -1;
		AsyncDrawableSpan asyncDrawableSpan = null;
		for (AsyncDrawableSpan span : mDrawableSpans) {
			if(span.containPoint((int)e.getX(), (int)e.getY(),!isEnabled())){
				asyncDrawableSpan = span;
				break;
			}
		}
		AsyncDrawableSpan singleTapUpSpan = null;
		if(asyncDrawableSpan != null){
			ArrayList<BitmapInfo> infos = new ArrayList<BitmapInfo>();
			int i = 0;
			for (AsyncDrawableSpan span : mDrawableSpans) {
				BitmapInfo info = new BitmapInfo(span.getUrl(), span.getImgWidth(), span.getImgWidth());
				info.setVideo(span.isVideo());
				infos.add(info);
				if(asyncDrawableSpan.equals(span)){
					singleTapUpSpan = span;
					position = i ;
				}
				i ++;
			}
			if(position != -1 && !infos.isEmpty()){
				if(mOnSpanClickListener != null){
					mOnSpanClickListener.onSpanClick(isEnabled(), infos, position,singleTapUpSpan);
				}else if(!singleTapUpSpan.handLink()){
//					if(isEnabled()){
//						ActivityChannels.openImagesActivityWithEditModel((Activity) getContext(), infos, position, AddImgModel.REQUEST_CODE_PHOTO_SELECT_RESULT );
//					}else{
//						ActivityChannels.openImagesActivityWithSave((Activity) getContext(), infos, position);
//					}
				}
			}
			setSelection(getText().getSpanStart(asyncDrawableSpan));
		}
		return position != -1;
	}

	private void handleSelectImgs(ArrayList<BitmapInfo> resultList){
		HashMap<String, AsyncDrawableSpan> spanMap = new HashMap<String, AsyncDrawableSpan>();
		ArrayList<AsyncDrawableSpan> tempSpans = new ArrayList<AsyncDrawableSpan>();
		for (AsyncDrawableSpan span : mDrawableSpans) {
			tempSpans.add(span);
			spanMap.put(span.getUrl(), span);
		}
		for (int i = 0;i < resultList.size() || i < tempSpans.size();i++) {
			String string = null;
			AsyncDrawableSpan span = null;
			if(i < resultList.size()){
				string = resultList.get(i).mPath;
			}
			if(i < tempSpans.size()){
				span = tempSpans.get(i);
			}
			if(string != null){
				span.setSource(spanMap.get(string));
			}else{
				int star = getText().getSpanStart(span);
				int end = getText().getSpanEnd(span);
				getText().delete(star,end);
			}
		}
		invalidate();
	}
	
	public void onActivityResult(BaseActivity this_,int requestCode, int resultCode, Intent data) {
		if(requestCode == AddImgModel.REQUEST_CODE_PHOTO_SELECT_RESULT) {
			if(resultCode == Activity.RESULT_OK) {
				if(data != null) {
//					@SuppressWarnings("unchecked")
//					ArrayList<BitmapInfo> resultList = (ArrayList<BitmapInfo>) data.getSerializableExtra(PhotoSelectActivity.EXTRA_NAME_PHOTO_SELECT_LIST);
//					boolean editMode = data.getBooleanExtra("edit_mode", false);
//					if(resultList != null && editMode){
//						handleSelectImgs(resultList);
//					}
				}
			}
		}
	}

	@Override
	public void onScrollChanged() {
		invalidate();
	}
	
	@Override
	public boolean onDown(MotionEvent e) {
		return false;
	}
	
	@Override
	public void onShowPress(MotionEvent e) {}
	
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,float distanceY) {
		return false;
	}
	
	@Override
	public void onLongPress(MotionEvent e) {
		performLongClick();
	}
	
	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,float velocityY) {
		return false;
	}
	
	public void setOnSpanClickListener(OnSpanClickListener l){
		mOnSpanClickListener = l;
	}
	
	public interface OnSpanClickListener{
		public void onSpanClick(boolean isEnabled, ArrayList<BitmapInfo> imgs, int position, AsyncDrawableSpan span);
	}
	
	private class ChangeWatcher implements SpanWatcher{
		@Override
		public void onSpanAdded(Spannable text, Object what, int start, int end) {
			if(isRequestViewLayout){
				return;
			}
			if(what instanceof AsyncDrawableSpan && !mDrawableSpans.contains(what)){
				AsyncDrawableSpan span = (AsyncDrawableSpan) what;
				span.onAttachedToWindow();
				mDrawableSpans.add(span);
				mDrawableHeights.put(span, span.getHeight());
			}
		}

		@Override
		public void onSpanRemoved(Spannable text, Object what, int start, int end) {
			if(isRequestViewLayout){
				return;
			}
			if(what instanceof AsyncDrawableSpan && mDrawableSpans.contains(what)){
				((AsyncDrawableSpan) what).onDetachedFromWindow();
				mDrawableSpans.remove(what);
				mDrawableHeights.remove(what);
			}
		}

		@Override
		public void onSpanChanged(Spannable text, Object what, int ostart,
				int oend, int nstart, int nend) {
		}
	}
	
	private class DrawableSpanInfo{
		private AsyncDrawableSpan mSpan;
		private int mStart;
		private int mEnd;
		
		public DrawableSpanInfo(AsyncDrawableSpan span, int star, int end) {
			mSpan = span;
			mStart = star;
			mEnd = end;
		}
	}

	@Override
	public int getViewPaddingLeft() {
		return mPaddingLeft;
	}

	@Override
	public int getViewPaddingRight() {
		return mPaddingRight;
	}

	@Override
	public int getViewPaddingTop() {
		return mPaddingTop;
	}

	@Override
	public int getViewPaddingBottom() {
		return mPaddingBottom;
	}

	public interface OnKeyEventPreImeListener {
		public boolean onKeyEventPreIme(KeyEvent event);
	}

	public void setOnKeyEventPreImeListener(OnKeyEventPreImeListener l) {
		mOnKeyEventPreImeListener = l;
	}

	@Override
	public boolean dispatchKeyEventPreIme(KeyEvent event) {
		if (mOnKeyEventPreImeListener != null) {
			if (mOnKeyEventPreImeListener.onKeyEventPreIme(event)) {
				return true;
			}
		}
		return super.dispatchKeyEventPreIme(event);
	}
}
