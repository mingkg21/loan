package com.bk.android.time.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;

import com.bk.android.ui.widget.viewpager.ViewPagerItem;

public class ScrollViewViewPagerItem extends ScrollView implements ViewPagerItem{
	public ScrollViewViewPagerItem(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	public boolean onCreateItem() {
		dispatchCreateItem(this);
		return false;
	}

	private void dispatchCreateItem(ViewGroup viewGroup){
		for (int i = 0; i < viewGroup.getChildCount(); i++) {
			View view = viewGroup.getChildAt(i);
			if(view instanceof ViewPagerItem){
				((ViewPagerItem) view).onCreateItem();
			}
			if(view instanceof ViewGroup){
				dispatchCreateItem((ViewGroup) view);
			}
		}
	}
	
	@Override
	public void onResumeItem() {
		dispatchResumeItem(this);
	}

	private void dispatchResumeItem(ViewGroup viewGroup){
		for (int i = 0; i < viewGroup.getChildCount(); i++) {
			View view = viewGroup.getChildAt(i);
			if(view instanceof ViewPagerItem){
				((ViewPagerItem) view).onResumeItem();
			}
			if(view instanceof ViewGroup){
				dispatchResumeItem((ViewGroup) view);
			}
		}
	}
	
	@Override
	public void onPauseItem() {
		dispatchPauseItem(this);
	}

	private void dispatchPauseItem(ViewGroup viewGroup){
		for (int i = 0; i < viewGroup.getChildCount(); i++) {
			View view = viewGroup.getChildAt(i);
			if(view instanceof ViewPagerItem){
				((ViewPagerItem) view).onPauseItem();
			}
			if(view instanceof ViewGroup){
				dispatchPauseItem((ViewGroup) view);
			}
		}
	}
	
	@Override
	public void onDestroyItem() {
		dispatchDestroyItem(this);
	}
	
	private void dispatchDestroyItem(ViewGroup viewGroup){
		for (int i = 0; i < viewGroup.getChildCount(); i++) {
			View view = viewGroup.getChildAt(i);
			if(view instanceof ViewPagerItem){
				((ViewPagerItem) view).onDestroyItem();
			}
			if(view instanceof ViewGroup){
				dispatchDestroyItem((ViewGroup) view);
			}
		}
	}

	@Override
	public void setLifeCycleEnabled(boolean enabled) {
		dispatchSetLifeCycleEnabled(this, enabled);
	}
	
	private void dispatchSetLifeCycleEnabled(ViewGroup viewGroup,boolean enabled){
		for (int i = 0; i < viewGroup.getChildCount(); i++) {
			View view = viewGroup.getChildAt(i);
			if(view instanceof ViewPagerItem){
				((ViewPagerItem) view).setLifeCycleEnabled(enabled);
			}
			if(view instanceof ViewGroup){
				dispatchSetLifeCycleEnabled((ViewGroup) view,enabled);
			}
		}
	}

}
