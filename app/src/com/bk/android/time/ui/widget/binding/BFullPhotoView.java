package com.bk.android.time.ui.widget.binding;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

public class BFullPhotoView extends BAsyncPhotoView{
	public BFullPhotoView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec,heightMeasureSpec);
		Drawable drawable = getDrawable();
		if(drawable != null){
			int w = getMeasuredWidth() - getPaddingLeft() - getPaddingRight();
			int imgW = drawable.getIntrinsicWidth();
	        int imgH = drawable.getIntrinsicHeight();
			imgH = imgH * w / imgW;
			imgW = w;
			setMeasuredDimension(getMeasuredWidth(), imgH + getPaddingTop() + getPaddingBottom());
		}
	}
}
