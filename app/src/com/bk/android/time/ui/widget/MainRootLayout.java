package com.bk.android.time.ui.widget;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.bk.android.assistant.R;
import com.bk.android.time.ui.widget.GestureHorizontalTranslation.OpenedClosedListener;

public class MainRootLayout extends RelativeLayout implements OpenedClosedListener {
	private boolean isInit = false;
	private GestureHorizontalTranslation mGesture;
	private Paint mBGPaint;
	private boolean isSelfCheckFinish;
	private WindowBGDrawable mWindowBGDrawable;
	
	public MainRootLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		mBGPaint = new Paint();
		mBGPaint.setColor(getContext().getResources().getColor(R.color.page_background));
		mGesture = new GestureHorizontalTranslation(this);
		mGesture.setOpenedClosedListener(this);
		mGesture.setShadowWidth((int) context.getResources().getDimension(R.dimen.menu_shadow_width));
		mGesture.setShadowDrawable(context.getResources().getDrawable(R.drawable.menu_shadow));
		mGesture.setMarginThreshold((int) (context.getResources().getDisplayMetrics().widthPixels * 0.1f));
		mWindowBGDrawable = new WindowBGDrawable();
	}

	public void setBGAlpha(int alpha) {
		mBGPaint.setAlpha(alpha);
	}

	public void setBGColor(int color) {
		mBGPaint.setColor(color);
	}

	public void invalidateBG() {
		if(mWindowBGDrawable != null){
			mWindowBGDrawable.invalidateSelf();
		}
	}
	
	public Drawable getWindowBGDrawable(){
		return mWindowBGDrawable;
	}
	
	@Override
	public void onWindowFocusChanged(boolean hasWindowFocus) {
		super.onWindowFocusChanged(hasWindowFocus);
		if(!hasWindowFocus){
			mGesture.setCurrentItem(1, false, true);
		}
	}
	
	@Override
	public void addView(View child, int index, ViewGroup.LayoutParams params) {
		if(!isInit){
			super.addView(child, index, params);
			return;
		}
		params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		((LayoutParams)params).addRule(RelativeLayout.BELOW, R.id.base_head_title_lay);
		child.setClickable(true);
		super.addView(child, 0, params);
	}
	
	public void setInitFinish(){
		isInit = true;
	}
	
	public void setTouchBackEnabled(boolean b) {
		mGesture.setTouchBackEnabled(b);
	}
	
	public boolean isTouchBackEnabled(){
		return mGesture.isTouchBackEnabled();
	}
	
	@Override
	public void scrollTo(int x, int y) {
		if(mWindowBGDrawable != null){
			mWindowBGDrawable.invalidateSelf();
		}
		super.scrollTo(x, y);
	}
	
	@Override
	protected void dispatchDraw(Canvas canvas) {
		if(mGesture.isTouchBackEnabled()){
			mGesture.computeScroll();
			mGesture.dispatchDraw(canvas);
		}
		super.dispatchDraw(canvas);
	}
	
	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		try {
			return super.dispatchTouchEvent(ev);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}
	
	@Override
	public boolean onInterceptTouchEvent(MotionEvent event) {
		if(mGesture.isTouchBackEnabled()){
			return mGesture.onInterceptTouchEvent(event);
		}else{
			return super.onInterceptTouchEvent(event);
		}
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if(mGesture.isTouchBackEnabled()){
			return mGesture.onTouchEvent(event);
		}else{
			return super.onTouchEvent(event);
		}
	}

	@Override
	public void onOpenedClosedChange(boolean isOpen) {
		if(getContext() instanceof Activity && isOpen){
			Activity activity = (Activity) getContext();
			isSelfCheckFinish = true;
			activity.onBackPressed();
			if(!activity.isFinishing()){
				mGesture.setCurrentItem(1, true);
			}
			isSelfCheckFinish = false;
		}
	}
	
	@Override
	public void setVisibility(int visibility) {
		if(mWindowBGDrawable != null){
			mWindowBGDrawable.invalidateSelf();
		}
		super.setVisibility(visibility);
	}
	
	public boolean isMenuOpen(){
		return mGesture.isMenuOpen() && !isSelfCheckFinish;
	}
	
	public class WindowBGDrawable extends Drawable{
		@Override
		public void draw(Canvas canvas) {
			Rect rect = getBounds();
			if(rect != null && getVisibility() == View.VISIBLE){
				int left = rect.left;
				int right = rect.right;
				if(getWidth() > 0){
					left = -mGesture.getScrollX();
					right = left + getWidth();
				}
				canvas.drawRect(left, rect.top , right, rect.bottom , mBGPaint);
			}
		}

		@Override
		public void setAlpha(int alpha) {}

		@Override
		public void setColorFilter(ColorFilter cf) {}

		@Override
		public int getOpacity() {
			return 0;
		}
	}
}
