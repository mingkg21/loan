package com.bk.android.time.ui.widget.span;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.FontMetricsInt;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.text.style.ReplacementSpan;
import android.view.View;
import android.view.animation.Animation;

import com.bk.android.time.entity.MixDataInfo;
import com.bk.android.time.ui.widget.AsyncImageViewManager;
import com.bk.android.time.ui.widget.AsyncImageViewManager.IImageView;
import com.bk.android.time.widget.ImageLoader;
import com.bk.android.util.BitmapUtil;
import com.bk.android.util.DimensionsUtil;

public class AsyncDrawableSpan extends ReplacementSpan implements IImageView {
	private AsyncImageViewManager mAsyncImageViewManager;
	private String mSrc;
	private int mImgWidth;
	private int mImgHeight;
	private int mWidth;
	private int mHeight;
	private Drawable mDrawable;
	private Drawable mPlayDrawable;
	private Drawable mTipDrawable;
	private ISpanDrawableView mSpanDrawableView;
	private float mLastDrawX;
	private int mLastDrawY;
	private int mLastDrawTop;
	private int mLastDrawBottom;
	boolean isNeedDraw = true;
	private int paddingL;
	private int paddingT;
	private int paddingB;
	private int paddingR;
	private MixDataInfo mMixDataInfo;
	private Paint mPaint;

	public AsyncDrawableSpan(MixDataInfo mixDataInfo, String src, int width,
			int height, int defaultImgRes, ISpanDrawableView spanDrawableView) {
		mPaint = new Paint();
		mMixDataInfo = mixDataInfo;
		mSrc = src;
		mAsyncImageViewManager = new AsyncImageViewManager(this);
		mAsyncImageViewManager.setDefaultHeavyDraw(true);
		mImgWidth = width;
		mImgHeight = height;
		mSpanDrawableView = spanDrawableView;
		if(mImgWidth == 0 || mImgHeight == 0){
			String url = mAsyncImageViewManager.handImageUrl(mSrc);
			int[] wh = BitmapUtil.getLocalWH(ImageLoader.getInstance().urlToPath(url));
			if(wh != null){
				mImgWidth = wh[0];
				mImgHeight = wh[1];
			}
		}
		if (mImgWidth * 1.1f >= mImgHeight) {
			setQuality(BitmapUtil.IMG_QUALITY_1);
		} else {
			setQuality(BitmapUtil.IMG_QUALITY_0);
		}
		mAsyncImageViewManager.setDefaultImgRes(defaultImgRes);
		paddingL = paddingR = DimensionsUtil.DIPToPX(5);
		paddingT = paddingB = DimensionsUtil.DIPToPX(15);
	}

	public void onAttachedToWindow(){
		mAsyncImageViewManager.onAttachedToWindow(null);
	}
	
	public void onDetachedFromWindow(){
		mAsyncImageViewManager.onDetachedFromWindow(null);
	}
	
	public void setPadding(int left, int top, int right, int bottom) {
		paddingL = left;
		paddingR = right;
		paddingT = top;
		paddingB = bottom;
	}

	public void setSource(AsyncDrawableSpan source) {
		if (source == null || this.equals(source)) {
			return;
		}
		mSrc = source.mSrc;
		mDrawable = source.mDrawable;
		mImgWidth = source.mImgWidth;
		mImgHeight = source.mImgHeight;
		if (mImgWidth * 1.1f >= mImgHeight) {
			setQuality(BitmapUtil.IMG_QUALITY_1);
		} else {
			setQuality(BitmapUtil.IMG_QUALITY_0);
		}
		mSpanDrawableView.requestViewLayout(this);
		if (mDrawable == null) {
			mAsyncImageViewManager.setImageUrl(mSrc, mImgWidth, mImgHeight);
		}
	}

	public MixDataInfo getMixDataInfo(){
		return mMixDataInfo;
	}
	
	public String getUrl() {
		return mSrc;
	}

	public int getImgWidth() {
		return mImgWidth;
	}

	public int getImgHeight() {
		return mImgHeight;
	}

	public int getWidth() {
		return mWidth;
	}

	public int getHeight() {
		return mHeight;
	}

	public void setQuality(int quality) {
		mAsyncImageViewManager.setQuality(quality);
	}

	@Override
	public void setImageDrawable(Drawable bitmapDrawable) {
		mDrawable = bitmapDrawable;
		if (mDrawable != null) {
			if (mAsyncImageViewManager.hasShowImg()) {
				if(mImgHeight == 0 || mImgWidth == 0 || Math.abs(mImgWidth * 1f / mImgHeight - mDrawable.getIntrinsicWidth() * 1f  / mDrawable.getIntrinsicHeight()) > 0.01f){
					mImgWidth = mDrawable.getIntrinsicWidth();
					mImgHeight = mDrawable.getIntrinsicHeight();
					mSpanDrawableView.requestViewLayout(this);
				}
			}
			invalidate();
		}
	}

	@Override
	public void invalidate() {
		isNeedDraw = true;
		mSpanDrawableView.invalidate();
	}
	
	@Override
	public int getSize(Paint paint, CharSequence text, int start, int end, FontMetricsInt fm) {
		int imgWidth = mImgWidth;
		int imgHeight = mImgHeight;
		int vWidth = mSpanDrawableView.getMaxViewWidth();
		if (paddingL == 0 && paddingR == 0) {
			vWidth += mSpanDrawableView.getViewPaddingLeft()
					+ mSpanDrawableView.getViewPaddingRight();
		}
		int vHeight = mSpanDrawableView.getMaxViewHeight();
		if(vHeight == 0){
			FontMetricsInt fontMetricsInt = paint.getFontMetricsInt();
			vHeight = fontMetricsInt.bottom - fontMetricsInt.top;
		}
		vWidth -= paddingL + paddingR;
		vHeight -= paddingT + paddingB;
		if (imgWidth > 0 && imgHeight > 0) {
			int gapW = imgHeight * (imgWidth - vWidth) / imgWidth;
			int gapH = imgHeight - vHeight;
			if (gapW > gapH) {
				imgHeight = imgHeight * vWidth / imgWidth;
				imgWidth = vWidth;
			} else {
				imgWidth = imgWidth * vHeight / imgHeight;
				imgHeight = vHeight;
			}
			if (fm != null) {
				fm.ascent = -(imgHeight + paddingT + paddingB);
				fm.descent = 0;

				fm.top = fm.ascent;
				fm.bottom = 0;
			}
		}
		mWidth = imgWidth;
		mHeight = imgHeight;
		if (paddingL == 0 && paddingR == 0) {
			return mSpanDrawableView.getMaxViewWidth();
		} else {
			return imgWidth + paddingL + paddingR;
		}
	}

	@Override
	public void draw(Canvas canvas, CharSequence text, int start, int end,
			float x, int top, int y, int bottom, Paint paint) {
		mLastDrawX = x;
		mLastDrawY = y;
		mLastDrawTop = top;
		mLastDrawBottom = bottom;
		mSpanDrawableView.invalidate();
	}

	public boolean containPoint(int x, int y) {
		return containPoint(x, y, false);
	}

	public boolean containPoint(int x, int y, boolean isLoose) {
		if (mLastDrawBottom - mLastDrawTop > 0) {
			int offsetW = mWidth / 4;
			int offsetH = mHeight / 4;
			int min = DimensionsUtil.DIPToPX(100);
			if (mWidth < min || isLoose) {
				offsetW = 0;
			}
			if (mHeight < min || isLoose) {
				offsetH = 0;
			}
			int t = mLastDrawTop + paddingT;
			int b = t + mHeight - paddingB - offsetH;
			t += offsetH;
			int l = (int) (mLastDrawX + paddingL);
			int r = l + mWidth - paddingR - offsetW;
			l += offsetW;
			if (x > l && x < r && y > t && y < b) {
				return true;
			}
		}
		return false;
	}

	public boolean drawLastLocal(Canvas canvas, int scrollTop, int viewHeight) {
		boolean isBeyond = false;
		if (scrollTop - mLastDrawTop - viewHeight / 2 > mHeight) {
			isBeyond = true;
			mDrawable = null;
			mAsyncImageViewManager.setImageUrl(null);
		} else if (scrollTop + viewHeight + viewHeight / 2 < mLastDrawTop) {
			isBeyond = true;
			mDrawable = null;
			mAsyncImageViewManager.setImageUrl(null);
		}
		if (mLastDrawBottom - mLastDrawTop > 0 && !isBeyond) {
			draw(canvas, mLastDrawX, mLastDrawTop, mLastDrawY, mLastDrawBottom);
			isNeedDraw = false;
		}
		return isBeyond;
	}

	public Drawable getDrawable() {
		return mDrawable;
	}

	private void draw(Canvas canvas, float x, int top, int y, int bottom) {
		if (mAsyncImageViewManager.getUrl() == null) {
			mAsyncImageViewManager.setImageUrl(mSrc, mImgWidth, mImgHeight);
		}
		int pb = (bottom - top - mHeight) / 2;
		int translateX = (int) (x + paddingL);
		int translateY = bottom - pb - mHeight;
		translateY += mSpanDrawableView.getViewPaddingTop();
		if (paddingL != 0 || paddingR != 0) {
			translateX += mSpanDrawableView.getViewPaddingLeft();
		}
		canvas.save();
		canvas.translate(translateX, translateY);
		mAsyncImageViewManager.onDraw(canvas, mWidth, mHeight);
		canvas.restore();
	}

	@Override
	public void setAnimation(Animation animation) {
	}

	@Override
	public void startAnimation(Animation animation) {
	}

	@Override
	public void clearAnimation() {
	}

	@Override
	public void onLoadedImg(Bitmap bitmap, String imageUrl, String cacheId) {
	}

	@Override
	public int getVisibility() {
		return View.VISIBLE;
	}

	@Override
	public void onSuperDraw(Canvas canvas) {
		if (mDrawable != null) {
			mDrawable.setBounds(0, 0, mWidth, mHeight);
			mDrawable.draw(canvas);
		}
		if (mPlayDrawable != null) {
			mPaint.setColor(0x77000000);
			canvas.drawRect(0, 0, mWidth, mHeight, mPaint);
			int playWidth = mWidth / 2;
			int playHeight = playWidth * mPlayDrawable.getIntrinsicHeight() / mPlayDrawable.getIntrinsicWidth();
			if(playWidth > mPlayDrawable.getIntrinsicWidth()){
				playWidth = mPlayDrawable.getIntrinsicWidth();
				playHeight = mPlayDrawable.getIntrinsicHeight();
			}
			int pLeft = (mWidth - playWidth) / 2;
			int pTop = (mHeight - playHeight) / 2;
			mPlayDrawable.setBounds(pLeft,pTop,pLeft + playWidth,pTop + playHeight);
			mPlayDrawable.draw(canvas);
		}
		if(mTipDrawable != null){
			mTipDrawable.setBounds(mWidth - mTipDrawable.getIntrinsicWidth() , 0 , mWidth , mTipDrawable.getIntrinsicHeight());
			mTipDrawable.draw(canvas);
		}
	}

	public boolean isVideo() {
		return mMixDataInfo != null && MixDataInfo.CLAZZ_VIDEO.equals(mMixDataInfo.getClazz());
	}

	public boolean handLink() {
		if(mMixDataInfo != null && !TextUtils.isEmpty(mMixDataInfo.getLinkData())){
//			XGPushActivity.openActivity(App.getInstance(), mMixDataInfo.getLinkData());
			return true;
		}
		return false;
	}
}
