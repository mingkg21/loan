package com.bk.android.time.ui.widget.spinner;

import android.graphics.drawable.Drawable;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView.LayoutParams;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bk.android.assistant.R;
import com.bk.android.util.DimensionsUtil;

import java.util.ArrayList;

public class SimpleDropDownListView extends DropDownListView {

	private ArrayList<DataEntity> dataList = new ArrayList<DataEntity>();

	private int mDefaultSelected;

	public SimpleDropDownListView(View anchor) {
		super(anchor);
		super.setAdapter(mAdapter);
		setDefaultSelected(mDefaultSelected);
	}

	public void setDefaultSelected(int defaultSelected) {
		mDefaultSelected = defaultSelected;
//		setSelection(mDefaultSelected);
		mAdapter.notifyDataSetChanged();
	}

	public void setData(ArrayList<DataEntity> list){
		dataList.clear();
		dataList.addAll(list);
		mAdapter.notifyDataSetChanged();
	}

	@Override
	public Drawable getDefaultBackgroundDrawable() {
		return getContext().getResources().getDrawable(R.drawable.ic_drop_down_bg);
	}

	@Override
	protected void onDispatchItemClick(AdapterView<?> parent, View view, int position, long id) {
		super.onDispatchItemClick(parent, view, position, id);

		setDefaultSelected(position);
	}

	public static class DataEntity {

		private String mContent;
		private int mResId;

		public DataEntity(String content, int resId) {
			mContent = content;
			mResId = resId;
		}

		public String getContent() {
			return mContent;
		}

		public void setContent(String content) {
			mContent = content;
		}

		public int getResId() {
			return mResId;
		}

		public void setResId(int resId) {
			mResId = resId;
		}
	}

	private BaseAdapter mAdapter = new BaseAdapter(){
		@Override
		public int getCount() {
			return dataList.size();
		}

		@Override
		public DataEntity getItem(int position) {
			return dataList.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			TextView textView = null;
			if(convertView == null){
				convertView = new TextView(getContext());
				convertView.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
				textView = (TextView) convertView;
				textView.setCompoundDrawablePadding(DimensionsUtil.DIPToPX(3));
//				textView.setTextColor(getContext().getColorStateList(R.color.drop_down_filter_text));
				textView.setTextSize(TypedValue.COMPLEX_UNIT_PX,getContext().getResources().getDimensionPixelSize(R.dimen.font_common_2));
				int padding = DimensionsUtil.DIPToPX(15);
				textView.setPadding((int)(padding * 1.5f), padding, (int)(padding * 1.5f), padding);
			}else{
				textView = (TextView) convertView;
			}
			if (textView != null) {
				DataEntity entity = dataList.get(position);
				textView.setCompoundDrawablesWithIntrinsicBounds(entity.getResId(), 0, 0, 0);
				textView.setText(entity.getContent());
				if (mDefaultSelected == position) {
					textView.setTextColor(R.color.com_color_1);
				} else {
					textView.setTextColor(R.color.com_color_2);
				}
			}
			return convertView;
		}
	};
}
