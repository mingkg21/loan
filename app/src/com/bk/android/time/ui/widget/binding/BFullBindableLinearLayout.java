package com.bk.android.time.ui.widget.binding;

import gueei.binding.widgets.BindableLinearLayout;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

public class BFullBindableLinearLayout extends BindableLinearLayout {
	public BFullBindableLinearLayout(Context context) {
		super(context);
	}
	
	public BFullBindableLinearLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		if(getChildCount() > 0 && getParent() != null){
			int width = (((View)getParent()).getMeasuredWidth() - getPaddingLeft() - getPaddingRight()) / getChildCount();
			for (int i = 0; i < getChildCount(); i++) {
				View view = getChildAt(i);
				view.setLayoutParams(new LayoutParams(width, width));
			}
		}
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	}
}
