package com.bk.android.time.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class OnlyDisplayFullGridView extends FullGridView{
	public OnlyDisplayFullGridView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		return false;
	}
}
