package com.bk.android.time.ui.widget;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.FontMetrics;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;

import com.bk.android.util.DimensionsUtil;
import com.bk.android.util.LogUtil;

/** 睡眠的统计
 * @author mingkg21
 * @email mingkg21@gmail.com
 * @date 2015年1月6日
 *
 */
public class SleepDataView extends View {
	/*
	 * Constants used to save/restore the instance state.
	 */
	private static final String STATE_PARENT = "parent";

	private Paint mScoreBgPaint;
	private Paint mScoreFgPaint;
	
	private int mWidth;
	private int mHeight;
	private float fontHeight;
	private float fontWidth;
	
	private ArrayList<DrawRect> mDrawRects;
	
	public SleepDataView(Context context) {
		super(context);
		init(null, 0);
	}

	public SleepDataView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(attrs, 0);
	}

	public SleepDataView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(attrs, defStyle);
	}

	private void init(AttributeSet attrs, int defStyle) {

		mWidth = 0;
		mHeight = 0;

		mScoreBgPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		mScoreBgPaint.setColor(Color.parseColor("#7f7f7f"));
		mScoreBgPaint.setStyle(Paint.Style.FILL);
		mScoreBgPaint.setTextAlign(Align.CENTER);
		mScoreBgPaint.setTextSize(DimensionsUtil.DIPToPXF(14));
		
		FontMetrics fm = mScoreBgPaint.getFontMetrics();  
		fontHeight = (float) Math.ceil(fm.descent - fm.top) + 2;  
		fontWidth = mScoreBgPaint.measureText("24h");
		
		mScoreFgPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		mScoreFgPaint.setColor(Color.parseColor("#2e2d33"));
		mScoreFgPaint.setStyle(Paint.Style.FILL);

	}

	@Override
	protected void onDraw(Canvas canvas) {
		float offX = fontWidth / 2;
		float offHeight = (float) (1.5 * fontHeight);
		float width = getWidth() - getPaddingLeft() - getPaddingRight() - offX * 2;
		float height = getHeight() - getPaddingTop() - getPaddingBottom() - offHeight;
		float offW = width / 12.0f;
//		LogUtil.i("SleepDataView", "width: " + width + "; height: " + height + "; offW: " + offW);
		
		float x = getPaddingLeft();
		float xStart = getPaddingLeft() + offX;
		float yStart = getPaddingTop() + offHeight;
		float yEnd = height + yStart;
		
//		mDrawRects = new ArrayList<SleepDataView.DrawRect>();
//		DrawRect temp = new DrawRect();
//		temp.startMin = 0;
//		temp.endMin = 60;
//		mDrawRects.add(temp);
//		
//		temp = new DrawRect();
//		temp.startMin = 65;
//		temp.endMin = 160;
//		mDrawRects.add(temp);
		
		if(mDrawRects != null) {
			int totalMins = 24 * 60;
			float x1 = 0;
			float x2 = 0;
			for(DrawRect drawRect : mDrawRects) {
				x1 = xStart + width * drawRect.startMin / totalMins;
				x2 = xStart + width * drawRect.endMin / totalMins;
				canvas.drawRect(x1, yStart, x2, yEnd, mScoreFgPaint);
			}
		}
		
		for(int i = 0; i <= 12; i++) {
			x = i * offW + xStart;
			if((i % 3) == 0) {
				canvas.drawText("" + i * 2 + "h", x, getPaddingTop() + fontHeight, mScoreBgPaint);
			}
			canvas.drawLine(x, yStart, x, yEnd, mScoreBgPaint);
		}
		
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int widthMode = MeasureSpec.getMode(widthMeasureSpec);
		int widthSize = MeasureSpec.getSize(widthMeasureSpec);
		int heightMode = MeasureSpec.getMode(heightMeasureSpec);
		int heightSize = MeasureSpec.getSize(heightMeasureSpec);

		int width;
		int height;

		if (widthMode == MeasureSpec.EXACTLY) {
			width = widthSize;
		} else if (widthMode == MeasureSpec.AT_MOST) {
			width = Math.min(mWidth, widthSize);
		} else {
			width = mWidth;
		}

		if (heightMode == MeasureSpec.EXACTLY) {
			height = heightSize;
		} else if (heightMode == MeasureSpec.AT_MOST) {
			height = Math.min(mHeight, heightSize);
		} else {
			height = mHeight;
		}

		int min = Math.min(mWidth, mHeight);
		setMeasuredDimension(widthSize, heightSize);
	}

	@Override
	protected Parcelable onSaveInstanceState() {
		Parcelable superState = super.onSaveInstanceState();

		Bundle state = new Bundle();
		state.putParcelable(STATE_PARENT, superState);

		return state;
	}

	@Override
	protected void onRestoreInstanceState(Parcelable state) {
		Bundle savedState = (Bundle) state;

		Parcelable superState = savedState.getParcelable(STATE_PARENT);
		super.onRestoreInstanceState(superState);
	}
	
	public void setDrawRects(ArrayList<DrawRect> drawRects) {
		mDrawRects = drawRects;
		invalidate();
	}

	
	public static class DrawRect {
		public int startMin;
		public int endMin;
	}
	
}
