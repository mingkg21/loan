package com.bk.android.time.ui.widget.binding;

import android.content.Context;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;

import com.bk.android.time.ui.widget.AsyncImageViewManager.ImageInfo;
import com.bk.android.time.ui.widget.AsyncPhotoView;
import com.bk.android.ui.widget.ScrollChildView;
import com.bk.android.ui.widget.viewpager.ViewPagerItem;

import gueei.binding.Binder;
import gueei.binding.BindingType;
import gueei.binding.IBindableView;
import gueei.binding.ViewAttribute;
import gueei.binding.listeners.ViewMulticastListener;
import gueei.binding.viewAttributes.ViewEventAttribute;
import uk.co.senab.photoview.PhotoView;
import uk.co.senab.photoview.PhotoViewAttacher;

public class BAsyncPhotoView extends AsyncPhotoView implements
		IBindableView<BAsyncPhotoView> ,ViewPagerItem,ScrollChildView{
	public BAsyncPhotoView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	@Override
	public ViewAttribute<? extends View, ?> createViewAttribute(
			String attributeId) {
		if (attributeId.equals("defaultImgRes")) {
			return new DefaultImgRes(this,attributeId);
		} else if (attributeId.equals("imageUrl")) {
			return new ImageUrl(this,attributeId);
		} else if (attributeId.equals("zoomable")) {
			return new Zoomable(this,attributeId);
		} else if (attributeId.equals("finishPostAnimation")) {
			return new FinishPostAnimation(this,attributeId);
		} else if (attributeId.equals("finishPreAnimation")) {
			return new FinishPreAnimation(this,attributeId);
		} else if (attributeId.equals("quality")) {
			return new Quality(this,attributeId);
		} else if (attributeId.equals("buildThumbUrl")) {
			return new BuildThumbUrl(this,attributeId);
		} else if (attributeId.equals("onPhotoTap")) {
			return new OnPhotoTapEvent(this, attributeId);
		}
		return null;
	}

	public static class OnClickListenerMulticast extends ViewMulticastListener<PhotoViewAttacher.OnPhotoTapListener>
			implements PhotoViewAttacher.OnPhotoTapListener {

		@Override
		public void registerToView(View v) {
			if (!(v instanceof PhotoView)) return;
			((PhotoView)v).setOnPhotoTapListener(this);
		}

		@Override
		public void onPhotoTap(View view, float x, float y) {
			for(PhotoViewAttacher.OnPhotoTapListener l:listeners){
				l.onPhotoTap(view, x, y);
			}
		}
	}

	public static class OnPhotoTapEvent extends ViewEventAttribute<PhotoView> implements PhotoViewAttacher.OnPhotoTapListener {

		public OnPhotoTapEvent(PhotoView view, String attributeId) {
			super(view, attributeId);
		}

		@Override
		protected void registerToListener(PhotoView view) {
			Binder.getMulticastListenerForView(view, OnClickListenerMulticast.class).register(this);
		}

		@Override
		public void onPhotoTap(View view, float x, float y) {
			invokeCommand(view, x, y);
		}
	}

	public static class BuildThumbUrl extends ViewAttribute<BAsyncPhotoView, Object> {
		public BuildThumbUrl(BAsyncPhotoView view,String attributeId) {
			super(Object.class, view, attributeId);
		}
		
		@Override
		public Object get() {
			return null;
		}
		
		@Override
		protected void doSetAttributeValue(Object newValue) {
			if (newValue instanceof Boolean) {
				getView().setNeedBuildThumbUrl((Boolean) newValue);
			}else if (newValue instanceof String) {
				if(newValue.equals("true")){
					getView().setNeedBuildThumbUrl(true);
				}else if(newValue.equals("false")){
					getView().setNeedBuildThumbUrl(false);
				}
			}
		}
		
		@Override
		protected BindingType AcceptThisTypeAs(Class<?> type) {
			return BindingType.OneWay;
		}
	}
	
	public static class Quality extends ViewAttribute<AsyncPhotoView, Integer>{
		public Quality(AsyncPhotoView view,String attributeId){
			super(Integer.class, view, attributeId);
		}
		
		@Override
		public Integer get() {
			return null;
		}

		@Override
		protected void doSetAttributeValue(Object newValue) {
			if (newValue instanceof Integer) {
				getView().setQuality((Integer) newValue);
			}
		}
	}
	
	public static class Zoomable extends ViewAttribute<BAsyncPhotoView, Object> {
		public Zoomable(BAsyncPhotoView view,String attributeId) {
			super(Object.class, view, attributeId);
		}
		
		@Override
		public Object get() {
			return null;
		}
		
		@Override
		protected void doSetAttributeValue(Object newValue) {
			if (newValue instanceof Boolean) {
				getView().setZoomable((Boolean) newValue);
			}else if (newValue instanceof String) {
				if(newValue.equals("true")){
					getView().setZoomable(true);
				}else if(newValue.equals("false")){
					getView().setZoomable(false);
				}
			}
		}
		
		@Override
		protected BindingType AcceptThisTypeAs(Class<?> type) {
			return BindingType.OneWay;
		}
	}
			
	public static class DefaultImgRes extends
			ViewAttribute<BAsyncPhotoView, Integer> {
		public DefaultImgRes(BAsyncPhotoView view,String attributeId) {
			super(Integer.class, view, attributeId);
		}

		@Override
		public Integer get() {
			return null;
		}

		@Override
		protected void doSetAttributeValue(Object newValue) {
			if (newValue instanceof Integer) {
				getView().setDefaultImgRes((Integer) newValue);
			}
		}
	}

	public static class ImageUrl extends ViewAttribute<BAsyncPhotoView, Object>{
		public ImageUrl(BAsyncPhotoView view,String attributeId){
			super(Object.class, view, attributeId);
		}
		
		@Override
		public Object get() {
			return null;
		}

		@Override
		protected void doSetAttributeValue(Object newValue) {
			if (newValue instanceof String) {
				getView().setImageUrl((String) newValue);
			}else if (newValue instanceof ImageInfo){
				getView().setImageInfo((ImageInfo) newValue);
			}else{
				getView().setImageUrl(null);
			}
		}
	}
	
	public static class FinishPreAnimation extends ViewAttribute<BAsyncPhotoView, Object>{
		public FinishPreAnimation(BAsyncPhotoView view,String attributeId){
			super(Object.class, view, attributeId);
		}
		
		@Override
		public Object get() {
			return null;
		}

		@Override
		protected void doSetAttributeValue(Object newValue) {
			if (newValue instanceof Integer) {
				getView().setFinishPreAnimation((Integer) newValue);
			}else if (newValue instanceof Animation){
				getView().setFinishPreAnimation((Animation) newValue);
			}
		}
	}
	
	public static class FinishPostAnimation extends ViewAttribute<BAsyncPhotoView, Object>{
		public FinishPostAnimation(BAsyncPhotoView view,String attributeId){
			super(Object.class, view, attributeId);
		}
		
		@Override
		public Object get() {
			return null;
		}

		@Override
		protected void doSetAttributeValue(Object newValue) {
			if (newValue instanceof Integer) {
				getView().setFinishPostAnimation((Integer) newValue);
			}else if (newValue instanceof Animation){
				getView().setFinishPostAnimation((Animation) newValue);
			}
		}
	}
	
	@Override
	public boolean canScroll(View view, int dx,int dy, int x, int y) {
		if(getTop() <= y && getBottom() >= y){
			RectF rect = getDisplayRect();
			if(dx > 0){
				if(rect.left < 0){
					return true;
				}
			}else{
				if(rect.right > getRight() - getLeft()){
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public boolean onCreateItem() {
		return false;
	}

	@Override
	public void onResumeItem() {
	}

	@Override
	public void onDestroyItem() {
	}

	@Override
	public void onPauseItem() {
		setScale(1);
	}

	@Override
	public void setLifeCycleEnabled(boolean enabled) {
	}
}
