package com.bk.android.time.ui.widget.binding;

import gueei.binding.Binder;
import gueei.binding.IBindableView;
import gueei.binding.ViewAttribute;
import gueei.binding.listeners.ViewMulticastListener;
import gueei.binding.viewAttributes.ViewEventAttribute;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import com.bk.android.ui.widget.FacilityExpandableListView;

public class BFacilityExpandableListView extends FacilityExpandableListView implements IBindableView<BFacilityExpandableListView> {
	public BFacilityExpandableListView(Context context) {
		super(context);
	}
	
	public BFacilityExpandableListView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	@Override
	public ViewAttribute<? extends View, ?> createViewAttribute(String attributeId) {
		if (attributeId.equals("onSuspensionGroupChange")) {
			return new OnSuspensionGroupChangeViewEvent(this,attributeId);
		}
		return null;
	}
	
	public static class OnSuspensionGroupChangeViewEvent extends ViewEventAttribute<FacilityExpandableListView> implements OnSuspensionGroupListener{
		public OnSuspensionGroupChangeViewEvent(FacilityExpandableListView view,String attributeId) {
			super(view, attributeId);
		}
	
		@Override
		protected void registerToListener(FacilityExpandableListView view) {
			Binder.getMulticastListenerForView(view, OnSuspensionGroupListenerMulticast.class).register(this);
		}

		@Override
		public void onSuspensionGroupChange(int newPosition, int oldPosition) {
			this.invokeCommand(getView(),newPosition,oldPosition);
		}
	}
	
	public static class OnSuspensionGroupListenerMulticast extends ViewMulticastListener<OnSuspensionGroupListener> implements OnSuspensionGroupListener {
		@Override
		public void registerToView(View v) {
			if (!(v instanceof FacilityExpandableListView)) return;
			((FacilityExpandableListView)v).setOnSuspensionGroupListener(this);
		}

		@Override
		public void onSuspensionGroupChange(int newPosition, int oldPosition) {
			for(OnSuspensionGroupListener l : listeners){
				l.onSuspensionGroupChange(newPosition, oldPosition);
			}
		}
	}
}
