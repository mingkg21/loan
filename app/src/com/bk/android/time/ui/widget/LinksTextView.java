package com.bk.android.time.ui.widget;

import android.content.Context;
import android.text.method.LinkMovementMethod;
import android.util.AttributeSet;

import com.bk.android.ui.widget.PatchedTextView;

public class LinksTextView extends PatchedTextView{
	public LinksTextView(Context context) {
		super(context);
		init();
	}
	
	public LinksTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}
	
	public LinksTextView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	private void init(){
		setMovementMethod(LinkMovementMethod.getInstance());
		setFocusable(false);
	}
}
