/*
 * Copyright (C) 2006 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bk.android.time.ui.widget.span;

import android.os.Parcel;
import android.text.ParcelableSpan;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;
import android.view.View.OnClickListener;

public class ClickColorSpan extends ClickableSpan implements ParcelableSpan {
	private OnClickListener mListener;
    private int mColor;
    
	public ClickColorSpan(int color, OnClickListener l) {
		mColor = color;
		mListener = l;
    }

    public ClickColorSpan(Parcel src) {
    	mColor = src.readInt();
    }
    
    public int getSpanTypeId() {
        return 0;
    }
    
    public int describeContents() {
        return 0;
    }
    
    @Override
    public void updateDrawState(TextPaint ds) {
        ds.setColor(mColor);
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mColor);
    }

    @Override
    public void onClick(View widget) {
    	if(mListener != null){
    		mListener.onClick(widget);
    	}
    }
}
