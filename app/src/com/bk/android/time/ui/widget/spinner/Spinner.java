package com.bk.android.time.ui.widget.spinner;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;

import com.bk.android.ui.widget.AbsDropDownListView;
import com.bk.android.ui.widget.AbsSpinner;

public class Spinner extends AbsSpinner {
	public Spinner(Context context) {
		super(context);
	}
	
	public Spinner(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	public Spinner(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}
	
	@Override
	protected void onInit() {
	}

	@Override
	protected View getDefaultEmptyView() {
		return null;
	}
	
	@Override
	protected Drawable getDefaultBackgroundDrawable() {
		return null;
	}
	
	@Override
	protected AbsDropDownListView newDropDownListView() {
		return new DropDownListView(this);
	}
}
