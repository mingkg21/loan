package com.bk.android.time.ui.widget.binding;

import gueei.binding.BindingType;
import gueei.binding.IBindableView;
import gueei.binding.ViewAttribute;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import com.bk.android.time.ui.widget.FaceLinearLayout;

public class BFaceLinearLayout extends FaceLinearLayout implements IBindableView<BFaceLinearLayout>{
	private ViewAttribute<? extends View, ?> mVisibilityViewAttribute;
	public BFaceLinearLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	public ViewAttribute<? extends View, ?> createViewAttribute(
			String attributeId) {
		if (attributeId.equals("visibility")){
			if(mVisibilityViewAttribute == null){
				mVisibilityViewAttribute = new VisibilityViewAttribute(this, "visibility");
			}
			return mVisibilityViewAttribute;
		}
		return null;
	}
	
	@Override
	protected void onVisibilityChanged(View changedView, int visibility) {
		super.onVisibilityChanged(changedView, visibility);
		if(mVisibilityViewAttribute != null){
			mVisibilityViewAttribute.notifyChanged();
		}
	}
	
	public class VisibilityViewAttribute extends ViewAttribute<View, Boolean> {
		public VisibilityViewAttribute(View view, String attributeName) {
			super(Boolean.class, view, attributeName);
		}

		@Override
		protected void doSetAttributeValue(Object newValue) {
			if(getView()==null) return;
			if (newValue==null){
				getView().setVisibility(View.GONE);
				return;
			}
			if (newValue instanceof Boolean){
				if ((Boolean)newValue)
					getView().setVisibility(View.VISIBLE);
				else
					getView().setVisibility(View.GONE);
				return;
			}else
			if (newValue instanceof Integer){
				getView().setVisibility((Integer)newValue);
				return;
			}
		}

		@Override
		public Boolean get() {
			if(getView()==null) return null;
			return getView().getVisibility() == VISIBLE;
		}

		@Override
		protected BindingType AcceptThisTypeAs(Class<?> type) {
			return BindingType.TwoWay;
		}
	}
}
