package com.bk.android.time.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.animation.Animation;
import android.widget.ImageView;

import com.bk.android.assistant.R;
import com.bk.android.time.entity.MixDataInfo;
import com.bk.android.time.ui.widget.AsyncImageViewManager.IImageView;
import com.bk.android.time.ui.widget.AsyncImageViewManager.ImageInfo;
import com.bk.android.util.BitmapUtil;
import com.bk.android.util.DimensionsUtil;
/**
 * 异步图片加载ImageView
 * @author linyiwei
 */
public class AsyncImageView extends ImageView implements IImageView{
	private AsyncImageViewManager mAsyncImageViewManager;
	private ForegroundInfo mForegroundInfo;
	private AsyncImageDrawable mAsyncImageDrawable;
	private String mLastSrc;
	private OnImageLoadedCallback mOnImageLoadedCallback;
	
	public AsyncImageView(Context context) {
		super(context);
		init();
	}
	
	public AsyncImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
		TypedArray a = context.obtainStyledAttributes(attrs,R.styleable.AsyncImage);
		int quality = a.getInt(R.styleable.AsyncImage_quality,BitmapUtil.IMG_QUALITY_1);
		a.recycle();
		setQuality(quality);
	}
	
	private void init(){
		mAsyncImageViewManager = new AsyncImageViewManager(this);
		mAsyncImageViewManager.setDefaultHeavyDraw(true);
	}
	
	@Override
	protected void onAttachedToWindow() {
		super.onAttachedToWindow();
		mAsyncImageViewManager.onAttachedToWindow(this);
	}
	
	@Override
	protected void onDetachedFromWindow() {
		super.onDetachedFromWindow();
		mAsyncImageViewManager.onDetachedFromWindow(this);
	}

	public void setDefaultHeavyDraw(boolean defaultHeavyDraw){
		mAsyncImageViewManager.setDefaultHeavyDraw(defaultHeavyDraw);
	}
	
	public void setNeedBuildThumbUrl(boolean needBuildThumbUrl){
		mAsyncImageViewManager.setNeedBuildThumbUrl(needBuildThumbUrl);
	}
	
	public void setQuality(int quality){
		mAsyncImageViewManager.setQuality(quality);
	}
	
	public void setFinishPreAnimation(int anim){
		mAsyncImageViewManager.setFinishPreAnimation(anim);
	}

	public void setFinishPreAnimation(Animation animation){
		mAsyncImageViewManager.setFinishPreAnimation(animation);
	}
	
	public void setFinishPostAnimation(int anim){
		mAsyncImageViewManager.setFinishPostAnimation(anim);
	}

	public void setFinishPostAnimation(Animation animation){
		mAsyncImageViewManager.setFinishPostAnimation(animation);
	}
	
	public void setDefaultImgRes(int defaultImgRes){
		mAsyncImageViewManager.setDefaultImgRes(defaultImgRes);
	}
	
	public String getImgUrl(){
		return mAsyncImageViewManager.getUrl();
	}
	
	public boolean hasLoadImg(){
		return getDrawable() != null && mAsyncImageViewManager.hasLoadNetImg();
	}
	
	public void setImageUrl(String url){
		setImageInfo(new ImageInfo(url, 0, 0));
	}
	
	public void setImageUrl(String url, int width, int height){
		setImageInfo(new ImageInfo(url, width, height));
	}
	
	public void setImageInfo(ImageInfo info){
		String src = info != null ? info.mSrc : null;
		if(src == null){
			src = "";
		}
		if(!src.equals(mLastSrc)){
			mLastSrc = src;
			mForegroundInfo = null;
			if(mAsyncImageDrawable != null){
				mAsyncImageDrawable.onDetachedFromWindow();
				mAsyncImageDrawable.setCallback(null);
				mAsyncImageDrawable = null;
			}
		}
		if(!TextUtils.isEmpty(src)){
			int index = info.mSrc.indexOf(MixDataInfo.COMPOSITE_IMG_SEPARATOR);
			if(index != -1){
				String bSrc = null;
				try {
					bSrc = info.mSrc.substring(0, index);
					
					String uriStr = info.mSrc.substring(index + MixDataInfo.COMPOSITE_IMG_SEPARATOR.length(), info.mSrc.length());
					Uri uri = Uri.parse(uriStr);
					mForegroundInfo = new ForegroundInfo(uriStr.substring(0, uriStr.indexOf("?")),
							uri.getQueryParameter("viewX"), 
							uri.getQueryParameter("viewY"), 
							uri.getQueryParameter("viewW"), 
							uri.getQueryParameter("viewH"), 
							uri.getQueryParameter("imgW"), 
							uri.getQueryParameter("imgH"));
					
					mAsyncImageDrawable = new AsyncImageDrawable();
					mAsyncImageDrawable.setClipRect(false);
					mAsyncImageDrawable.setCallback(this);
					mAsyncImageDrawable.setQuality(BitmapUtil.IMG_QUALITY_2);
					mAsyncImageDrawable.onAttachedToWindow();
					mAsyncImageDrawable.setImageUrl(new ImageInfo(bSrc, info.mWidth, info.mHeight));
					
					info.mSrc = mForegroundInfo.mSrc;
					info.mWidth = mForegroundInfo.mImgW;
					info.mHeight = mForegroundInfo.mImgH;
				} catch (Exception e) {
					if(!TextUtils.isEmpty(bSrc)){
						info.mSrc = bSrc;
					}
				}
			}
		}
		mAsyncImageViewManager.setImageInfo(info);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		if(mForegroundInfo != null && mAsyncImageDrawable != null && mAsyncImageViewManager.hasLoadNetImg() && mForegroundInfo.mImgW > 0){
			float s = getWidth() * 1f / mForegroundInfo.mImgW;
			int x = (int) (mForegroundInfo.mViewX * s);
			int y = (int) (mForegroundInfo.mViewY * s);
			int w = (int) (mForegroundInfo.mViewW * s);
			int h = (int) (mForegroundInfo.mViewH * s);
			int cw = w;
			int ch = h;
			if(x + w > getWidth()){
				cw -= x + w - getWidth();
				cw -= DimensionsUtil.DIPToPX(3);
			}
			if(y + h > getHeight()){
				ch -= y + h - getHeight();
				ch -= DimensionsUtil.DIPToPX(3);
			}
			canvas.save();
			canvas.clipRect(x, y, cw + x , ch + y);
			mAsyncImageDrawable.setBounds(x, y, w + x, h + y);
			mAsyncImageDrawable.draw(canvas);
			canvas.restore();
		}
		canvas.save();
		mAsyncImageViewManager.onDraw(canvas, getWidth(), getHeight());
		canvas.restore();
	}

	public void drawProgress(Canvas canvas,int w,int h){
		mAsyncImageViewManager.drawProgress(canvas, w, h);
	}
	
	@Override
	public void onLoadedImg(Bitmap bitmap, String imageUrl, String cacheId) {
		if (mOnImageLoadedCallback != null) {
			mOnImageLoadedCallback.onLoaded(bitmap);
		}
	}
	
	@Override
	public void onFinishTemporaryDetach() {
		clearAnimation();
		setAnimation(null);
	}

	@Override
	public void onSuperDraw(Canvas canvas) {
		super.onDraw(canvas);
	}
	
	@Override
	public void invalidateDrawable(Drawable dr) {
		if(dr != null && dr.equals(mAsyncImageDrawable)){
			invalidate();
		}else{
			super.invalidateDrawable(dr);
		}
	}
    
	private class ForegroundInfo{
		private String mSrc;
		private int mViewX;
		private int mViewY;
		private int mImgW;
		private int mImgH;
		private int mViewW;
		private int mViewH;
		public ForegroundInfo(String src, String viewX, String viewY, String viewW, String viewH, String imgW,String imgH) {
			this.mSrc = src;
			this.mViewX = Integer.valueOf(viewX);
			this.mViewY = Integer.valueOf(viewY);
			this.mViewW = Integer.valueOf(viewW);
			this.mViewH = Integer.valueOf(viewH);
			this.mImgW = Integer.valueOf(imgW);
			this.mImgH = Integer.valueOf(imgH);
		}
	}

	public void setOnImageLoadedCallback(OnImageLoadedCallback onImageLoadedCallback) {
		mOnImageLoadedCallback = onImageLoadedCallback;
	}

	public interface OnImageLoadedCallback {
		public void onLoaded(Bitmap bitmap);
	}
}
