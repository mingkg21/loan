package com.bk.android.time.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.SlidingDrawer;

import com.bk.android.ui.widget.FingerDrawingView;

public class FingerDrawingDrawer extends SlidingDrawer  {
	private FingerDrawingView mFingerDrawingView;

	public FingerDrawingDrawer(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	private void init(){
		mFingerDrawingView = new FingerDrawingView(getContext());
		addView(mFingerDrawingView, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
	}
}
