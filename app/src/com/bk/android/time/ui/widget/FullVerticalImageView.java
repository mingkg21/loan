package com.bk.android.time.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

public class FullVerticalImageView extends ImageView {
	public FullVerticalImageView(Context context) {
		super(context);
	}

	public FullVerticalImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		if(getDrawable() != null){
			int imgW = getDrawable().getIntrinsicWidth() * getMeasuredHeight() / getDrawable().getIntrinsicHeight();
			setMeasuredDimension(imgW, getMeasuredHeight());
		}
	}
}
