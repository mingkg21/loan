package com.bk.android.time.ui.widget.binding;

import gueei.binding.Binder;
import gueei.binding.IBindableView;
import gueei.binding.ViewAttribute;
import gueei.binding.listeners.ViewMulticastListener;
import gueei.binding.viewAttributes.ViewEventAttribute;

import java.util.Collection;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import com.bk.android.time.ui.widget.SudokuImageView;

public class BSudokuImageView extends SudokuImageView implements IBindableView<BSudokuImageView>{
	public BSudokuImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	@Override
	public ViewAttribute<? extends View, ?> createViewAttribute(
			String attributeId) {
		if (attributeId.equals("dataSource")) {
			return new DataSourceAttribute(this,attributeId);
		}else if (attributeId.equals("spacing")) {
			return new SpacingAttribute(this,attributeId);
		}else if (attributeId.equals("defaultImgRes")) {
			return new DefaultImgResAttribute(this,attributeId);
		}else if (attributeId.equals("quality")) {
			return new QualityAttribute(this,attributeId);
		}else if (attributeId.equals("onItemClicked")) {
			return new OnItemClickedViewEvent(this,attributeId);
		}
		return null;
	}
	
	public class OnItemClickedViewEvent extends ViewEventAttribute<SudokuImageView> implements OnSudokuItemClickListener{
		public OnItemClickedViewEvent(SudokuImageView view,String attributeId) {
			super(view, attributeId);
		}
	
		@Override
		protected void registerToListener(SudokuImageView view) {
			Binder.getMulticastListenerForView(view, OnCalendarItemClickListenerMulticast.class).register(this);
		}

		@Override
		public void onSudokuItemClick(SudokuImageView view, int position) {
			this.invokeCommand(view,position);
		}
	}
	
	public static class OnCalendarItemClickListenerMulticast extends ViewMulticastListener<OnSudokuItemClickListener> implements OnSudokuItemClickListener {
		@Override
		public void registerToView(View v) {
			if (!(v instanceof SudokuImageView)) return;
			((SudokuImageView)v).setOnSudokuItemClickListener(this);
		}

		@Override
		public void onSudokuItemClick(SudokuImageView view, int position) {
			for(OnSudokuItemClickListener l : listeners){
				l.onSudokuItemClick(view, position);
			}
		}
	}
	
	public static class QualityAttribute extends ViewAttribute<BSudokuImageView, Integer>{
		public QualityAttribute(BSudokuImageView view,String attributeId){
			super(Integer.class, view, attributeId);
		}
		
		@Override
		public Integer get() {//获取值
			return null;
		}

		@Override
		protected void doSetAttributeValue(Object newValue) {//设置值
			if (newValue instanceof Integer) {
				getView().setQuality((Integer) newValue);
			}
		}
	}
	
	public static class DefaultImgResAttribute extends ViewAttribute<BSudokuImageView, Object>{
		public DefaultImgResAttribute(BSudokuImageView view,String attributeId){
			super(Object.class, view, attributeId);
		}
		
		@Override
		public Object get() {//获取值
			return null;
		}

		@Override
		protected void doSetAttributeValue(Object newValue) {//设置值
			if (newValue instanceof Number) {
				getView().setDefaultImgRes(((Number) newValue).intValue());
			}else{
				getView().setDefaultImgRes(0);
			}
		}
	}
	
	public static class SpacingAttribute extends ViewAttribute<BSudokuImageView, Object>{
		public SpacingAttribute(BSudokuImageView view,String attributeId){
			super(Object.class, view, attributeId);
		}
		
		@Override
		public Object get() {//获取值
			return null;
		}

		@Override
		protected void doSetAttributeValue(Object newValue) {//设置值
			if (newValue instanceof Number) {
				getView().setSpacing(((Number) newValue).intValue());
			}else{
				getView().setSpacing(0);
			}
		}
	}
	
	public static class DataSourceAttribute extends ViewAttribute<BSudokuImageView, Object>{
		public DataSourceAttribute(BSudokuImageView view,String attributeId){
			super(Object.class, view, attributeId);
		}
		
		@Override
		public Object get() {//获取值
			return null;
		}

		@SuppressWarnings("unchecked")
		@Override
		protected void doSetAttributeValue(Object newValue) {//设置值
			if (newValue instanceof Collection) {
				getView().setData((Collection<Object>) newValue);
			}else{
				getView().setData(null);
			}
		}
	}
}
