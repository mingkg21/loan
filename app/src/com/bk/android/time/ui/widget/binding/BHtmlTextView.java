package com.bk.android.time.ui.widget.binding;

import gueei.binding.Binder;
import gueei.binding.IBindableView;
import gueei.binding.ViewAttribute;
import gueei.binding.listeners.ViewMulticastListener;
import gueei.binding.viewAttributes.ViewEventAttribute;

import java.util.ArrayList;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import com.bk.android.time.model.lightweight.AddImgModel.BitmapInfo;
import com.bk.android.time.ui.widget.HtmlTextView;
import com.bk.android.time.ui.widget.span.AsyncDrawableSpan;

public class BHtmlTextView extends HtmlTextView implements IBindableView<BHtmlTextView>{
	public BHtmlTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	@Override
	public ViewAttribute<? extends View, ?> createViewAttribute(
			String attributeId) {
		if (attributeId.equals("html")) {//设置对于的属性值
			return new HtmlViewAttribute(this,attributeId);
		}else if (attributeId.equals("headHint")) {//设置对于的属性值
			return new HeadHintAttribute(this,attributeId);
		}else if (attributeId.equals("imgPadding")) {//设置对于的属性值
			return new ImgPaddingAttribute(this,attributeId);
		}else if (attributeId.equals("onSpanClicked")) {
			return new OnSpanClickViewEvent(this,attributeId);
		}
		return null;
	}
	
	public class OnSpanClickViewEvent extends ViewEventAttribute<HtmlTextView> implements OnSpanClickListener{
		public OnSpanClickViewEvent(HtmlTextView view,String attributeId) {
			super(view, attributeId);
		}
	
		@Override
		protected void registerToListener(HtmlTextView view) {
			Binder.getMulticastListenerForView(view, OnSpanClickListenerMulticast.class).register(this);
		}

		@Override
		public void onSpanClick(boolean isEnabled, ArrayList<BitmapInfo> imgs,int position, AsyncDrawableSpan span) {
			this.invokeCommand(getView(),isEnabled,imgs,position,span);
		}

	}
	
	public static class OnSpanClickListenerMulticast extends ViewMulticastListener<OnSpanClickListener> implements OnSpanClickListener {
		@Override
		public void registerToView(View v) {
			if (!(v instanceof HtmlTextView)) return;
			((HtmlTextView)v).setOnSpanClickListener(this);
		}

		@Override
		public void onSpanClick(boolean isEnabled, ArrayList<BitmapInfo> imgs,int position, AsyncDrawableSpan span) {
			for(OnSpanClickListener l : listeners){
				l.onSpanClick(isEnabled, imgs, position, span);
			}
		}
	}
	
	public static class ImgPaddingAttribute extends ViewAttribute<BHtmlTextView, Integer>{
		public ImgPaddingAttribute(BHtmlTextView view,String attributeId){
			super(Integer.class, view, attributeId);
		}
		
		@Override
		public Integer get() {//获取值
			return null;
		}

		@Override
		protected void doSetAttributeValue(Object newValue) {//设置值
			if (newValue instanceof Integer) {
				getView().setImgPadding((Integer) newValue);
			}else{
				getView().setImgPadding(0);
			}
		}
	}
	
	public static class HtmlViewAttribute extends ViewAttribute<BHtmlTextView, String>{
		public HtmlViewAttribute(BHtmlTextView view,String attributeId){
			super(String.class, view, attributeId);
		}
		
		@Override
		public String get() {//获取值
			return null;
		}

		@Override
		protected void doSetAttributeValue(Object newValue) {//设置值
			if (newValue instanceof String) {
				getView().setHtml((String) newValue);
			}else{
				getView().setHtml(null);
			}
		}
	}
	
	public static class HeadHintAttribute extends ViewAttribute<BHtmlTextView, Object>{
		public HeadHintAttribute(BHtmlTextView view,String attributeId){
			super(Object.class, view, attributeId);
		}
		
		@Override
		public Object get() {//获取值
			return null;
		}

		@Override
		protected void doSetAttributeValue(Object newValue) {
			getView().setHeadHintSpan(newValue);
		}
	}
}
