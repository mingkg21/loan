package com.bk.android.time.ui.widget;

import java.util.LinkedList;
import java.util.List;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.view.MotionEvent;

public class BladeGesture {
    private static final int ALPHA = 180;
    private static final int ALPHA_FULL = 255;
    private static final int GESTURECOLOR = Color.rgb(153, 153, 153);
    private static final long CREATE_DISTANCE = 0;
    private static final long RECYCLE_DISTANCE = 25;
    private static final long MAX_POINT_SIZE = 10;
    private static final long MAX_HEIGHT = 15;

	private LinkedList<PointInfo> mPointList;
	private LinkedList<PointInfo> mPointRecycleList;
	private Paint mPaint;
	private Path mPath;
	private Path mBackPath;
	private Long mLastRecyclePointTime;
	private Long mLastCreatePointTime;

	public BladeGesture(){
		mPointList = new LinkedList<PointInfo>();
		mPointRecycleList = new LinkedList<PointInfo>();
		mPath = new Path();
		mBackPath = new Path();
		mPaint = new Paint();
		mPaint.setColor(Color.WHITE);
		mPaint.setAntiAlias(true);
		mPaint.setStrokeWidth(5);
		mPaint.setStyle(Paint.Style.FILL);
	}
	
	public boolean onTouchEvent(MotionEvent event) {
		if(event.getAction() == MotionEvent.ACTION_DOWN){
			if(!mPointList.isEmpty()){
				mPointRecycleList.addAll(mPointList);
				mPointList.clear();
			}
			mLastCreatePointTime = System.currentTimeMillis() - CREATE_DISTANCE;
		}
		mLastRecyclePointTime = System.currentTimeMillis();
		long time = System.currentTimeMillis() - mLastCreatePointTime;
		if(mLastCreatePointTime != null && time >= CREATE_DISTANCE){
			PointInfo point = null;
			if(mPointList.size() > MAX_POINT_SIZE){
				point = mPointList.removeFirst();
			}
			if(point == null){
				if(mPointRecycleList.isEmpty()){
					point = new PointInfo();
				}else{
					point = mPointRecycleList.removeFirst();
				}
			}
			point.x = event.getX();
			point.y = event.getY();
			mPointList.add(point);
			mLastCreatePointTime = System.currentTimeMillis();
		}
		return true;
	}
	
	public boolean drawPoint(Canvas canvas) {
		if(mPointList.size() < 3){
			return false;
		}
		
		mPath.reset();
		mBackPath.reset();
		computePoint(mPointList);
		int maxIndex = mPointList.size() - 2;
		int size = mPointList.size();
		for (int i = 0; i < size; i++) {
			PointInfo point = mPointList.get(i);
			if(i == 0){
				mPath.moveTo(point.x, point.y);
			}else if(i >= maxIndex){
				mPath.lineTo(point.x, point.y);
			}else{
				mPath.lineTo(point.tx, point.ty);
			}
			if(i == 0){
				mBackPath.moveTo(point.x, point.y);
			}else if(i >= maxIndex){
				mBackPath.lineTo(point.x, point.y);
			}else{
				mBackPath.lineTo(point.btx, point.bty);
			}
		}
		
		for (int i = mPointList.size() - 1; i >= 0; i--) {
			PointInfo point = mPointList.get(i);
			if(i == 0){
				mPath.lineTo(point.x, point.y);
			}else if(i >= maxIndex){
				mPath.lineTo(point.x, point.y);
			}else{
				mPath.lineTo(point.bx, point.by);
			}
			if(i == 0){
				mBackPath.lineTo(point.x, point.y);
			}else if(i >= maxIndex){
				mBackPath.lineTo(point.x, point.y);
			}else{
				mBackPath.lineTo(point.bbx, point.bby);
			}
		}
		
		mPaint.setColor(GESTURECOLOR);
		mPaint.setAlpha(ALPHA);
		canvas.drawPath(mBackPath, mPaint);
		
		mPaint.setColor(Color.WHITE);
	    mPaint.setAlpha(ALPHA_FULL);
		canvas.drawPath(mPath, mPaint);
		
		if(recyclePoint()){
			return true;
		}
		return false;
	}

	
	private boolean recyclePoint(){
		if(mLastRecyclePointTime != null){
			if(!mPointList.isEmpty()){
				if(System.currentTimeMillis() - mLastRecyclePointTime > RECYCLE_DISTANCE){
					mPointRecycleList.add(mPointList.removeFirst());
					mLastRecyclePointTime = System.currentTimeMillis();
				}
			}else{
				mLastRecyclePointTime = null;
			}
			return true;
		}
		return false;
	}
	
	private float getGap(float x0, float y0, float x1, float y1) {
		return (float) Math.pow(
				Math.pow((x0 - x1), 2) + Math.pow((y0 - y1), 2), 0.5);
	}

	private void computePoint(List<PointInfo> mPointList) {
		int size = mPointList.size() - 1;
		PointInfo point = mPointList.get(0);
		point.tx = point.x;
		point.ty = point.y;
		point.bx = point.x;
		point.by = point.y;
		point.btx = point.x;
		point.bty = point.y;
		point.bbx = point.x;
		point.bby = point.y;
		for (int i = 0; i < size; i++) {
			point = mPointList.get(i);
			PointInfo pointNext = mPointList.get(i + 1);
			float startX = point.x;
			float startY = point.y;
			float endX = pointNext.x;
			float endY = pointNext.y;
			float gap = getGap(startX, startY, endX, endY);
			float w = gap / 10;
			// 背景shape外侧点高度
			float h = MAX_HEIGHT * (i + 1) / size;
			// 填充shape外侧点高度
			float h2 = h * 2 / 3;
			double length = Math.pow(Math.pow(w, 2) + Math.pow((h), 2), 0.5);
			double length2 = Math.pow(Math.pow(w, 2) + Math.pow((h2), 2), 0.5);

			double ang1_1 = Math.atan((endY - startY) / (endX - startX));
			double ang1_2 = Math.atan(h / w);
			double angle1_1 = ang1_1 + ang1_2;
			double angle1_2 = ang1_1 - ang1_2;

			double ang2_2 = Math.atan(h2 / w);
			double angle2_1 = ang1_1 + ang2_2;
			double angle2_2 = ang1_1 - ang2_2;
			if (endX > startX) {
				float xx1 = endX - (float) (length * Math.cos(angle1_1));
				float yy1 = endY - (float) (length * Math.sin(angle1_1));
				float xx2 = endX - (float) (length * Math.cos(angle1_2));
				float yy2 = endY - (float) (length * Math.sin(angle1_2));

				float xx12 = endX - (float) (length2 * Math.cos(angle2_1));
				float yy12 = endY - (float) (length2 * Math.sin(angle2_1));
				float xx22 = endX - (float) (length2 * Math.cos(angle2_2));
				float yy22 = endY - (float) (length2 * Math.sin(angle2_2));

				pointNext.tx = xx12;
				pointNext.ty = yy12;
				pointNext.bx = xx22;
				pointNext.by = yy22;

				pointNext.btx = xx1;
				pointNext.bty = yy1;
				pointNext.bbx = xx2;
				pointNext.bby = yy2;
			} else if(endX < startX){
				float xx1 = endX + (float) (length * Math.cos(angle1_1));
				float yy1 = endY + (float) (length * Math.sin(angle1_1));
				float xx2 = endX + (float) (length * Math.cos(angle1_2));
				float yy2 = endY + (float) (length * Math.sin(angle1_2));

				float xx12 = endX + (float) (length2 * Math.cos(angle2_1));
				float yy12 = endY + (float) (length2 * Math.sin(angle2_1));
				float xx22 = endX + (float) (length2 * Math.cos(angle2_2));
				float yy22 = endY + (float) (length2 * Math.sin(angle2_2));

				pointNext.tx = xx12;
				pointNext.ty = yy12;
				pointNext.bx = xx22;
				pointNext.by = yy22;

				pointNext.btx = xx1;
				pointNext.bty = yy1;
				pointNext.bbx = xx2;
				pointNext.bby = yy2;
			}else{
				pointNext.tx = point.tx;
				pointNext.ty = point.ty;
				pointNext.bx = point.bx;
				pointNext.by = point.by;

				pointNext.btx = point.btx;
				pointNext.bty = point.bty;
				pointNext.bbx = point.bbx;
				pointNext.bby = point.bby;
			}
		}
	}
	
	public class PointInfo {
		public float x;
		public float y;
		public float tx;
		public float ty;
		public float bx;
		public float by;
		public float btx;
		public float bty;
		public float bbx;
		public float bby;
	}
}
