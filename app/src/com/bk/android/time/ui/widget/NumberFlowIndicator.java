package com.bk.android.time.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import com.bk.android.ui.widget.viewflow.FlowIndicator;
import com.bk.android.ui.widget.viewflow.IViewFlow;

public class NumberFlowIndicator extends TextView implements FlowIndicator{
	private IViewFlow mIViewFlow;
	
	public NumberFlowIndicator(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	public void onSwitched(int position) {
		if(mIViewFlow != null){
			if(mIViewFlow.getCount() < 2){
				setVisibility(View.GONE);
			}else{
				setText((position + 1) + "/" + mIViewFlow.getCount());
				setVisibility(View.VISIBLE);
			}
		}
	}

	@Override
	public void setViewFlow(IViewFlow view) {
		mIViewFlow = view;	
		if(mIViewFlow != null){
			setText((mIViewFlow.getSelectedItemPosition() + 1) + "/" + mIViewFlow.getCount());
		}
	}

	@Override
	public void onScrolled(int h, int v, int oldh, int oldv) {
	}
}
