package com.bk.android.time.ui.widget.binding.command;

import android.view.View;

import com.bk.android.binding.command.BaseCommand;

public abstract class OnDrawerOpenCommand extends BaseCommand {
	
	@Override
	protected void onInvoke(View view, Object... args) {
		onOpenDrawer(view);
	}
	
	public abstract void onOpenDrawer(View view);
}
