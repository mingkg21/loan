package com.bk.android.time.ui.widget.binding;

import android.content.Context;
import android.util.AttributeSet;

public class BAsyncImageSquareView extends BAsyncImageView{
	public BAsyncImageSquareView(Context context) {
		super(context);
	}

	public BAsyncImageSquareView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		setMeasuredDimension(getMeasuredWidth(), getMeasuredWidth());
	}
}
