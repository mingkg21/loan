package com.bk.android.time.ui.widget;

import android.content.ContentResolver;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.AttributeSet;
import android.util.Log;

import com.bk.android.assistant.R;
import com.bk.android.time.app.App;
import com.bk.android.util.DimensionsUtil;

/**
 * 一旦设置了图片之后出发设置null清空图片否则不会再重新计算布局，只会直接按照原来计算出来的大小来绘制新图片
 * @author linyiwei
 *
 */
public class QuickImageView extends AsyncImageView {
	private static final Paint DST_IN_PAINT = new Paint();
	private static final Bitmap CIRCLE_MASK_BITMAP = BitmapFactory.decodeResource(App.getInstance().getResources(), R.drawable.ic_circle_mask);
	static{
		DST_IN_PAINT.setFilterBitmap(false);
		DST_IN_PAINT.setXfermode(new PorterDuffXfermode(Mode.DST_IN));
	}
	
	private int mResource;
	private Uri mUri;
	private boolean isCircle;
	private float mRadius;
	private Paint mPaint;
	private int mStrokeWidth;
	private Integer mProfileColor;
	
	public QuickImageView(Context context) {
		super(context);
		init();
	}
	
	public QuickImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}
	
	private void init(){
		mStrokeWidth = DimensionsUtil.DIPToPX(1f);
		mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setStrokeWidth(mStrokeWidth);
        setProfileColor(Color.WHITE);
	}

	public void setStrokeWidth(int width) {
		mStrokeWidth = width;
		mPaint.setStrokeWidth(mStrokeWidth);
		invalidate();
	}
	
	public void setCircle(boolean circle){
		isCircle = circle;
		invalidate();
	}
	
	public void setProfileColor(int color){
		mProfileColor = color;
		invalidate();
	}
	
	@Override
	public void setImageResource(int resId) {
		if (mUri != null || mResource != resId) {
			 mResource = resId;
	         mUri = null;
	         resolveResource();
		}
	}

	@Override
	public void setImageURI(Uri uri) {
		if (mResource != 0 ||
                (mUri != uri &&
                 (uri == null || mUri == null || !uri.equals(mUri)))) {
            mResource = 0;
            mUri = uri;
            resolveResource();
        }
	}

	private void setImageDrawable(Drawable drawable,boolean isResource) {//2130837558
		if(!isResource){
			mResource = 0;
	        mUri = null;
		}
		if(drawable != null){
			Drawable odlDrawable = super.getDrawable();
			if(odlDrawable instanceof DecoratorDrawable){
				DecoratorDrawable quickDrawable = (DecoratorDrawable) odlDrawable;
				quickDrawable.replaceDrawable(drawable);
				invalidate();
			}else{
				drawable = new DecoratorDrawable(drawable);
				super.setImageDrawable(drawable);
			}
		}else{
			super.setImageDrawable(null);
		}
	}
	
	@Override
	public void setImageDrawable(Drawable drawable) {//2130837558
		setImageDrawable(drawable, false);
	}
	
	@Override
	public Drawable getDrawable() {
		Drawable drawable = super.getDrawable();
		if(drawable instanceof DecoratorDrawable){
			return ((DecoratorDrawable) drawable).getSourceDrawable();
		}
		return drawable;
	}

	private void resolveResource() {
        Resources rsrc = getResources();
        if (rsrc == null) {
            return;
        }
        Drawable d = null;
        if (mResource != 0) {
            try {
                d = rsrc.getDrawable(mResource);
            } catch (Exception e) {
                Log.w("ImageView", "Unable to find resource: " + mResource, e);
                // Don't try again.
                mUri = null;
            }
        } else if (mUri != null) {
            String scheme = mUri.getScheme();
            if (ContentResolver.SCHEME_ANDROID_RESOURCE.equals(scheme)
            		||ContentResolver.SCHEME_CONTENT.equals(scheme)
                    || ContentResolver.SCHEME_FILE.equals(scheme)) {
                try {
                    d = Drawable.createFromStream(
                    		getContext().getContentResolver().openInputStream(mUri),
                        null);
                } catch (Exception e) {
                    Log.w("ImageView", "Unable to open content: " + mUri, e);
                }
            } else {
                d = Drawable.createFromPath(mUri.toString());
            }
    
            if (d == null) {
                System.out.println("resolveUri failed on bad bitmap uri: "
                                   + mUri);
                // Don't try again.
                mUri = null;
            }
        } else {
            return;
        }
        setImageDrawable(d,true);
    }
	
	@Override
	protected void onLayout(boolean changed, int left, int top, int right,int bottom) {
		super.onLayout(changed, left, top, right, bottom);
		mRadius = getWidth() > getHeight() ? getHeight() / 2 : getWidth() / 2;
	}
	
	@Override
	public void draw(Canvas canvas) {
		try {
			Integer sc = null;
			RectF rectF = null;
			if(isCircle){
				sc = canvas.saveLayer(0, 0, getWidth(), getHeight(), null, Canvas.HAS_ALPHA_LAYER_SAVE_FLAG | Canvas.CLIP_SAVE_FLAG);
				float w = getWidth() > getHeight() ? getHeight() : getWidth();
				float left = (getWidth() - w) / 2f;
				float top = (getHeight() - w) / 2f;
				rectF = new RectF(left, top, left + w, top + w);
				rectF.inset(1, 1);
				canvas.clipRect(rectF);
			}
			
			super.draw(canvas);

			if(rectF != null){
				canvas.drawBitmap(CIRCLE_MASK_BITMAP, new Rect(0, 0, CIRCLE_MASK_BITMAP.getWidth(), CIRCLE_MASK_BITMAP.getHeight()), rectF, DST_IN_PAINT);
			}
			
			if(sc != null){
				canvas.restoreToCount(sc);
			}
			
			if(isCircle && mProfileColor != null){
	        	float radius = mRadius - mStrokeWidth / 2f;
	        	mPaint.setColor(mProfileColor);
				canvas.drawCircle(getWidth() / 2f, getHeight() / 2f, radius, mPaint);
	        }
		} catch (Exception e) {
			super.draw(canvas);
		}
	}
}
