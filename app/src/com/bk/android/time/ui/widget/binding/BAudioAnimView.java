package com.bk.android.time.ui.widget.binding;

import gueei.binding.ViewAttribute;
import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Interpolator;
import android.widget.Scroller;

import com.bk.android.time.widget.media.StreamPlayer;
import com.bk.android.time.widget.media.StreamPlayerListener;

public class BAudioAnimView extends BAsyncImageView implements StreamPlayerListener{
	private static final float SCALE_MAX = 50f;
	
	private String mVoiceSrc;
	private boolean isNeedStartAnim;
	private Scroller mScroller;
	public BAudioAnimView(Context context, AttributeSet attrs) {
		super(context, attrs);
		StreamPlayer.getInstance().addPlayerListener(this);
		setFinishPreAnimation(null);
		setFinishPostAnimation(null);
		
		mScroller = new Scroller(getContext(),new Interpolator() {
			@Override
			public float getInterpolation(float input) {
				return input;
			}
		});
		initAnim();
	}

	@Override
	public ViewAttribute<? extends View, ?> createViewAttribute(
			String attributeId) {
		if (attributeId.equals("voiceSrc")) {//设置对于的属性值
			return new VoiceSrc(this,attributeId);
		}
		return super.createViewAttribute(attributeId);
	}

	public void setVoiceSrc(String newValue) {
		mVoiceSrc = newValue;
		initAnim();
	}

	public void startAnim(int duration){
		mScroller.startScroll(1000, 0, -1000, 0, duration);
		invalidate();
	}
	
	public void initAnim(){
		mScroller.startScroll(mScroller.getCurrX(), 0,1000 - mScroller.getCurrX(), 0, 0);
		mScroller.abortAnimation();
		invalidate();
	}
	
	@Override
	public void draw(Canvas canvas) {
		canvas.save();
		canvas.clipRect(getLeft(), getTop(), getRight(), getBottom());
		float scale = (mScroller.getCurrX() * 1f / 1000 * SCALE_MAX) / 100 + 1f;
		canvas.scale(scale, scale, getWidth() / 2, getHeight() / 2);
		super.draw(canvas);
		canvas.restore();
		if(!mScroller.isFinished() && mScroller.computeScrollOffset()){
			invalidate();
		}
	}
	
	public static class VoiceSrc extends ViewAttribute<BAudioAnimView, String>{//定义View的类型 和 数据类型 ，如果是多个值要定义成 Object 在去类型转换
		public VoiceSrc(BAudioAnimView view,String attributeId){
			super(String.class, view, attributeId);
		}
		
		@Override
		public String get() {//获取值
			return null;
		}

		@Override
		protected void doSetAttributeValue(Object newValue) {//设置值
			if (newValue instanceof String) {
				getView().setVoiceSrc((String) newValue);
			}
		}
	}

	@Override
	public void onPlayStateChange(byte state, String voiceSrc) {
		if(!voiceSrc.equals(mVoiceSrc)){
			return;
		}
		if(state == StreamPlayerListener.STATE_START){
			isNeedStartAnim = true;
		}else{
			isNeedStartAnim = false;
		}
	}

	@Override
	public void onError(byte type, String voiceSrc) {}

	@Override
	public void onLoadProgressChange(float progress, String voiceSrc) {}

	@Override
	public void onPlayProgressChange(float progress, int maxSeconds,
			String voiceSrc) {
		if(!voiceSrc.equals(mVoiceSrc)){
			return;
		}
		if(isNeedStartAnim){
			startAnim(maxSeconds * 1000);
			isNeedStartAnim = false;
		}
	}

	@Override
	public void onBuffering(float progress, String voiceSrc) {}
}
