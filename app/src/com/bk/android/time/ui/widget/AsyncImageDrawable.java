package com.bk.android.time.ui.widget;

import pl.droidsonroids.gif.GifDrawable;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Drawable.Callback;
import android.view.View;
import android.view.animation.Animation;

import com.bk.android.assistant.R;
import com.bk.android.time.app.App;
import com.bk.android.time.ui.widget.AsyncImageViewManager.IImageView;
import com.bk.android.time.ui.widget.AsyncImageViewManager.ImageInfo;
import com.bk.android.time.widget.ImageLoader;
import com.bk.android.util.FileUtil;

public class AsyncImageDrawable extends Drawable implements IImageView,Callback{
	public static final int SCALE_NON = 0;
	public static final int SCALE_TYPE_FIT_CENTER = 1;
	public static final int SCALE_TYPE_CENTER_CROP = 2;

	private Drawable mDrawable;
	private Rect mDstRect;
	private AsyncImageViewManager mManager;
	private ImageInfo mImageInfo;
	private int mScaleType;
	private boolean isClipRect;
//	private boolean isPress;
	private Drawable mPlayDrawable;
	
	public AsyncImageDrawable(){
		mDstRect = new Rect();
		mManager = new AsyncImageViewManager(this);
		mManager.setDefaultHeavyDraw(true);
		setScaleType(SCALE_TYPE_CENTER_CROP);
		isClipRect = true;
	}
	
	public void setClipRect(boolean clipRect){
		isClipRect = clipRect;
	}
	
	public void onAttachedToWindow(){
		mManager.onAttachedToWindow(null);
	}
	
	public void onDetachedFromWindow(){
		mManager.onDetachedFromWindow(null);
	}
	
	public long getCurrentProgress(){
		return mManager.getCurrentProgress();
	}
	
	public long getMaxProgress(){
		return mManager.getMaxProgress();
	}
	
//	public void setPress(boolean press){
//		isPress = press;
//		invalidate();
//	}
	
	public void setScaleType(int scaleType){
		mScaleType = scaleType;
	}
	
	public void setDefaultImgRes(int defaultImgRes){
		mManager.setDefaultImgRes(defaultImgRes);
	}
	
	public void setDefaultHeavyDraw(boolean defaultHeavyDraw){
		mManager.setDefaultHeavyDraw(defaultHeavyDraw);
	}
	
	public void setNeedBuildThumbUrl(boolean needBuildThumbUrl){
		mManager.setNeedBuildThumbUrl(needBuildThumbUrl);
	}
	
	public String getUrl(){
		return mImageInfo != null ? mImageInfo.mSrc : null;
	}
	
	public Drawable getDrawable(){
		return mDrawable;
	}

	public boolean hasShowImg(){
		return mManager.hasShowImg();
	}
	
	public boolean hasLoadNetImg(){
		return mManager.hasLoadNetImg();
	}
	
	public boolean hasLoadNetImgFail(){
		return mManager.hasLoadNetImgFail();
	}
	
	public void setQuality(int quality){
		mManager.setQuality(quality);
	}
	
	public void reSetImageUrl(){
		if(mImageInfo != null){
			mManager.setImageUrl(mImageInfo.mSrc,mImageInfo.mWidth,mImageInfo.mHeight);
			invalidate();
		}
	}
	
	public void setImageUrl(ImageInfo imageInfo){
		if(mImageInfo == null && mImageInfo == imageInfo){
			return;
		}
		if(imageInfo != null && mImageInfo != null && mImageInfo.mSrc.equals(imageInfo.mSrc)){
			return;
		}
		mImageInfo = imageInfo;
		mManager.setImageUrl(mImageInfo.mSrc,mImageInfo.mWidth,mImageInfo.mHeight);
		invalidate();
	}
	
	public void release(){
		mManager.setImageUrl(null);
		if(mDrawable != null){
			mDrawable.setCallback(null);
		}
		mDrawable = null;
		mImageInfo = null;
	}
	
	@Override
	public void setImageDrawable(Drawable drawable) {
		if(mDrawable != null){
			mDrawable.setCallback(null);
		}
		mDrawable = drawable;
		if(mDrawable != null){
			mDrawable.setCallback(this);
		}
		invalidate();
	}

	@Override
	public void setAnimation(Animation animation) {
	}

	@Override
	public void startAnimation(Animation animation) {
	}

	@Override
	public void clearAnimation() {
	}

	@Override
	public void onLoadedImg(Bitmap bitmap,final String imageUrl, String imageId) {
		if(imageUrl != null && imageUrl.lastIndexOf(".gif") != -1){
			new Thread(){
				GifDrawable gifDrawable = null;
				@Override
				public void run() {
					String path = ImageLoader.getInstance().urlToPath(imageUrl);
					try {
						if(FileUtil.isFileExists(path)){
							gifDrawable = new GifDrawable(path);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
					if(gifDrawable != null){
						App.getHandler().post(new Runnable() {
							@Override
							public void run() {
								if(mImageInfo != null && imageUrl.equals(mImageInfo.mSrc)){
									setImageDrawable(gifDrawable);
								}
							}
						});
					}
				}
			}.start();
		}
	}

	@Override
	public void invalidate() {
		invalidateSelf();
	}

	@Override
	public int getVisibility() {
		return mDrawable == null ? View.GONE : View.VISIBLE;
	}

	@Override
	public int getIntrinsicWidth() {
		if(mImageInfo != null && hasLoadNetImg()){
			return mDrawable.getIntrinsicWidth();
		}else if(mDrawable != null){
			return mDrawable.getIntrinsicWidth();
		}
		return super.getIntrinsicWidth();
	}
	
	@Override
	public int getIntrinsicHeight() {
		if(mImageInfo != null && hasLoadNetImg()){
			return mDrawable.getIntrinsicHeight();
		}else if(mDrawable != null){
			return mDrawable.getIntrinsicHeight();
		}
		return super.getIntrinsicHeight();
	}
	
	@Override
	public void draw(Canvas canvas) {
		if(mDrawable != null && mImageInfo != null){
			copyBounds(mDstRect);
			Integer sc = null;
			if(isClipRect){
				sc = canvas.save();
				canvas.clipRect(mDstRect);
			}
			int left = mDstRect.left;
			int top = mDstRect.top;
			if(hasShowImg()){
				if(mScaleType != SCALE_NON){
					int vWidth = mDstRect.width();
		    		int vHeight = mDstRect.height();
					int imgWidth = mDrawable.getIntrinsicWidth();
					int imgHeight = mDrawable.getIntrinsicHeight();
					if(imgWidth > 0 && imgHeight > 0){
						int gapW = imgHeight * (imgWidth - vWidth) / imgWidth;
						int gapH = imgHeight - vHeight;
						if(mScaleType == SCALE_TYPE_CENTER_CROP ? gapW <= gapH : gapW > gapH){
			    			imgHeight = imgHeight * vWidth / imgWidth;
			    			imgWidth = vWidth;
			    			left = mDstRect.left;
			    			top = mDstRect.top - (imgHeight - vHeight) / 2;
			    		}else{
			    			imgWidth = imgWidth * vHeight / imgHeight;
			    			imgHeight = vHeight;
			    			left = mDstRect.left - (imgWidth - vWidth) / 2;
			    			top = mDstRect.top;
			    		}
			    		mDrawable.setBounds(0, 0, imgWidth,imgHeight);
					}
				}else{
					mDrawable.setBounds(0, 0, mDstRect.width(), mDstRect.height());
				}
			}else{
				mDrawable.setBounds(0, 0, mDstRect.width(), mDstRect.height());
			}
			canvas.translate(left,top);
			mManager.onDraw(canvas, mDstRect.width(), mDstRect.height());
			if(sc != null){
				canvas.restoreToCount(sc);
			}else{
				canvas.translate(-left,-top);
			}
		}
	}

	@Override
	public void setAlpha(int alpha) {
	}

	@Override
	public void setColorFilter(ColorFilter cf) {
	}

	@Override
	public int getOpacity() {
		return 0;
	}

	@Override
	public void onSuperDraw(Canvas canvas) {
		if(mDrawable != null){
			mDrawable.draw(canvas);
		}
	}

	@Override
	public void invalidateDrawable(Drawable who) {
		super.invalidateSelf();
	}

	@Override
	public void scheduleDrawable(Drawable who, Runnable what, long when) {
		super.scheduleSelf(what, when);
	}

	@Override
	public void unscheduleDrawable(Drawable who, Runnable what) {
		super.unscheduleSelf(what);
	}
}
