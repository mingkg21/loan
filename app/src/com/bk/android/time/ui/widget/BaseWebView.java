package com.bk.android.time.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebSettings.LayoutAlgorithm;
import android.webkit.WebView;

import com.bk.android.ui.widget.ScrollChildView;

public class BaseWebView extends WebView implements ScrollChildView{
	public BaseWebView(Context context) {
		super(context);
		init();
	}
	
	public BaseWebView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}
	
	public BaseWebView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}
	
	private void init(){
		setScrollBarStyle(SCROLLBARS_INSIDE_OVERLAY);
		DisplayMetrics dm = getResources().getDisplayMetrics();
		WebSettings.ZoomDensity zoomDensity = WebSettings.ZoomDensity.MEDIUM;
		switch (dm.densityDpi) {
		case DisplayMetrics.DENSITY_LOW:
			zoomDensity = WebSettings.ZoomDensity.CLOSE;
			break;
		case DisplayMetrics.DENSITY_MEDIUM:
			zoomDensity = WebSettings.ZoomDensity.MEDIUM;
			break;
		case DisplayMetrics.DENSITY_HIGH:
			zoomDensity = WebSettings.ZoomDensity.FAR;
			break;
		}
		getSettings().setDefaultZoom(zoomDensity);
		getSettings().setJavaScriptEnabled(true);
		
		getSettings().setNeedInitialFocus(false);
		getSettings().setBuiltInZoomControls(true);
		getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
		getSettings().setSupportZoom(false);
		getSettings().setLayoutAlgorithm(LayoutAlgorithm.SINGLE_COLUMN);
		getSettings().setAllowFileAccess(true);
	}

	@Override
	public boolean canScroll(View view, int dx, int dy, int x, int y) {
		return true;
	}
}
