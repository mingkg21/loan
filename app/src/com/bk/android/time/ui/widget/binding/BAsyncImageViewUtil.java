package com.bk.android.time.ui.widget.binding;

import android.view.View;
import android.view.animation.Animation;
import android.widget.ImageView;

import com.bk.android.time.ui.widget.AsyncImageView;
import com.bk.android.time.ui.widget.AsyncImageViewManager.ImageInfo;

import gueei.binding.BindingType;
import gueei.binding.ViewAttribute;

public class BAsyncImageViewUtil{
	public static ViewAttribute<? extends View, ?> createViewAttribute(String attributeId,AsyncImageView asyncImageView) {
		if (attributeId.equals("defaultImgRes")) {
			return new DefaultImgRes(asyncImageView,attributeId);
		} else if (attributeId.equals("imageUrl")) {
			return new ImageUrl(asyncImageView,attributeId);
		} else if (attributeId.equals("finishPostAnimation")) {
			return new FinishPostAnimation(asyncImageView,attributeId);
		} else if (attributeId.equals("finishPreAnimation")) {
			return new FinishPreAnimation(asyncImageView,attributeId);
		} else if (attributeId.equals("quality")) {
			return new Quality(asyncImageView,attributeId);
		} else if (attributeId.equals("buildThumbUrl")) {
			return new BuildThumbUrl(asyncImageView,attributeId);
		} else if (attributeId.equals("scaleType")) {
			return new ScaleType(asyncImageView,attributeId);
		}
		return null;
	}

	public static class BuildThumbUrl extends ViewAttribute<AsyncImageView, Object> {
		public BuildThumbUrl(AsyncImageView view,String attributeId) {
			super(Object.class, view, attributeId);
		}
		
		@Override
		public Object get() {
			return null;
		}
		
		@Override
		protected void doSetAttributeValue(Object newValue) {
			if (newValue instanceof Boolean) {
				getView().setNeedBuildThumbUrl((Boolean) newValue);
			}else if (newValue instanceof String) {
				if(newValue.equals("true")){
					getView().setNeedBuildThumbUrl(true);
				}else if(newValue.equals("false")){
					getView().setNeedBuildThumbUrl(false);
				}
			}
		}
		
		@Override
		protected BindingType AcceptThisTypeAs(Class<?> type) {
			return BindingType.OneWay;
		}
	}
	
	public static class Quality extends ViewAttribute<AsyncImageView, Integer>{
		public Quality(AsyncImageView view,String attributeId){
			super(Integer.class, view, attributeId);
		}
		
		@Override
		public Integer get() {
			return null;
		}

		@Override
		protected void doSetAttributeValue(Object newValue) {
			if (newValue instanceof Integer) {
				getView().setQuality((Integer) newValue);
			}
		}
	}
	
	public static class DefaultImgRes extends ViewAttribute<AsyncImageView, Integer>{
		public DefaultImgRes(AsyncImageView view,String attributeId){
			super(Integer.class, view, attributeId);
		}
		
		@Override
		public Integer get() {
			return null;
		}

		@Override
		protected void doSetAttributeValue(Object newValue) {
			if (newValue instanceof Integer) {
				getView().setDefaultImgRes((Integer) newValue);
			}
		}
	}

	public static class ScaleType extends ViewAttribute<AsyncImageView, Object>{
		public ScaleType(AsyncImageView view,String attributeId){
			super(Object.class, view, attributeId);
		}

		@Override
		public Object get() {
			return null;
		}

		@Override
		protected void doSetAttributeValue(Object newValue) {
			if (newValue instanceof String) {
				ImageView.ScaleType scaleType = ImageView.ScaleType.MATRIX;
				if (newValue.equals("matrix")) {
					scaleType = ImageView.ScaleType.MATRIX;
				} else if (newValue.equals("fitXY")) {
					scaleType = ImageView.ScaleType.FIT_XY;
				} else if (newValue.equals("fitStart")) {
					scaleType = ImageView.ScaleType.FIT_START;
				} else if (newValue.equals("fitCenter")) {
					scaleType = ImageView.ScaleType.FIT_CENTER;
				} else if (newValue.equals("fitEnd")) {
					scaleType = ImageView.ScaleType.FIT_END;
				} else if (newValue.equals("center")) {
					scaleType = ImageView.ScaleType.CENTER;
				} else if (newValue.equals("centerCrop")) {
					scaleType = ImageView.ScaleType.CENTER_CROP;
				} else if (newValue.equals("centerInside")) {
					scaleType = ImageView.ScaleType.CENTER_INSIDE;
				}
				getView().setScaleType(scaleType);
			}
		}
	}
	
	public static class ImageUrl extends ViewAttribute<AsyncImageView, Object>{
		public ImageUrl(AsyncImageView view,String attributeId){
			super(Object.class, view, attributeId);
		}
		
		@Override
		public Object get() {
			return null;
		}

		@Override
		protected void doSetAttributeValue(Object newValue) {
			if (newValue instanceof String) {
				getView().setImageUrl((String) newValue);
			}else if (newValue instanceof ImageInfo){
				getView().setImageInfo((ImageInfo) newValue);
			}else{
				getView().setImageUrl(null);
			}
		}
	}
	
	public static class FinishPostAnimation extends ViewAttribute<AsyncImageView, Object>{
		public FinishPostAnimation(AsyncImageView view,String attributeId){
			super(Object.class, view, attributeId);
		}
		
		@Override
		public Object get() {
			return null;
		}

		@Override
		protected void doSetAttributeValue(Object newValue) {
			if (newValue instanceof Integer) {
				getView().setFinishPostAnimation((Integer) newValue);
			}else if (newValue instanceof Animation){
				getView().setFinishPostAnimation((Animation) newValue);
			}
		}
	}
	
	public static class FinishPreAnimation extends ViewAttribute<AsyncImageView, Object>{
		public FinishPreAnimation(AsyncImageView view,String attributeId){
			super(Object.class, view, attributeId);
		}
		
		@Override
		public Object get() {
			return null;
		}

		@Override
		protected void doSetAttributeValue(Object newValue) {
			if (newValue instanceof Integer) {
				getView().setFinishPreAnimation((Integer) newValue);
			}else if (newValue instanceof Animation){
				getView().setFinishPreAnimation((Animation) newValue);
			}
		}
	}
}
