package com.bk.android.time.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.bk.android.assistant.R;

/**
 * Created by mingkg21 on 15/8/26.
 */
public class ListViewToTopImageView extends ImageView {

    private Animation.AnimationListener mAnimationListener;

    private int mVisibility;

    public ListViewToTopImageView(Context context) {
        super(context);
        init();
    }

    public ListViewToTopImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ListViewToTopImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {

        mAnimationListener = new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                if (mVisibility == VISIBLE) {
                    ListViewToTopImageView.super.setVisibility(mVisibility);
                }
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (mVisibility == GONE) {
                    ListViewToTopImageView.super.setVisibility(mVisibility);
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        };

    }

    @Override
    public void setVisibility(int visibility) {
        mVisibility = visibility;
//        if (mVisibility != getVisibility()) {
            if (mVisibility == VISIBLE) {
                Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.push_up_in);
                animation.setAnimationListener(mAnimationListener);
                animation.start();
                return;
            } else if (mVisibility == GONE) {
                Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.push_down_out);
                animation.setAnimationListener(mAnimationListener);
                animation.start();
                return;
            }
//        }
//        super.setVisibility(visibility);
    }
}
