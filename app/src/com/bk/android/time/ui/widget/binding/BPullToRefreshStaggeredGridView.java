package com.bk.android.time.ui.widget.binding;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.AbsListView;
import android.widget.ListAdapter;

import com.bk.android.time.ui.widget.pulltorefresh.FlipLoadingLayout;
import com.bk.android.time.ui.widget.pulltorefresh.RotateLoadingLayout;
import com.bk.android.ui.widget.binding.BStaggeredGridView;
import com.bk.android.ui.widget.pulltorefresh.OverscrollHelper;
import com.bk.android.ui.widget.pulltorefresh.internal.LoadingLayout;

public class BPullToRefreshStaggeredGridView extends BPullToRefreshAbsListView<AbsListView> {
	public BPullToRefreshStaggeredGridView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected AbsListView createRefreshableView(Context context, AttributeSet attrs) {
		AbsListView lv = createListView(context, attrs);
		lv.setCacheColorHint(Color.TRANSPARENT);
		lv.setId(android.R.id.list);
		return lv;
	}

	@Override
	protected LoadingLayout createLoadingLayout(
			Context context,AnimationStyle style,Mode mode,Orientation scrollDirection,TypedArray attrs) {
		switch (style) {
			case ROTATE:
			default:
				return new RotateLoadingLayout(context, mode, scrollDirection, attrs);
			case FLIP:
				return new FlipLoadingLayout(context, mode, scrollDirection, attrs);
		}
	}

	@Override
	protected AbsListView createListView(Context context, AttributeSet attrs) {
		final AbsListView lv;
		if (VERSION.SDK_INT >= VERSION_CODES.GINGERBREAD) {
			lv = new InternalStaggeredGridViewSDK9(context, attrs);
		} else {
			lv = new InternalStaggeredGridView(context, attrs);
		}
		return lv;
	}
	
	@TargetApi(9)
	final class InternalStaggeredGridViewSDK9 extends InternalStaggeredGridView {
		public InternalStaggeredGridViewSDK9(Context context, AttributeSet attrs) {
			super(context, attrs);
		}
		
		@Override
		protected boolean overScrollBy(int deltaX, int deltaY, int scrollX, int scrollY, int scrollRangeX,
				int scrollRangeY, int maxOverScrollX, int maxOverScrollY, boolean isTouchEvent) {

			final boolean returnValue = super.overScrollBy(deltaX, deltaY, scrollX, scrollY, scrollRangeX,
					scrollRangeY, maxOverScrollX, maxOverScrollY, isTouchEvent);

			// Does all of the hard work...
			OverscrollHelper.overScrollBy(BPullToRefreshStaggeredGridView.this, deltaX, scrollX, deltaY, scrollY, isTouchEvent);

			return returnValue;
		}
	}
	
	protected class InternalStaggeredGridView extends BStaggeredGridView{
		private PullHeadViewHelper<ListAdapter> mPullHeadViewHelper;

		public InternalStaggeredGridView(Context context, AttributeSet attrs) {
			super(context, attrs);
			mPullHeadViewHelper = new PullHeadViewHelper<ListAdapter>();
		}
		
		@Override
		public void setAdapter(ListAdapter adapter) {
			super.setAdapter(mPullHeadViewHelper.setAdapter(adapter));
		}
		
		@Override
		protected void dispatchDraw(Canvas canvas) {
			try {
				super.dispatchDraw(canvas);
			} catch (IndexOutOfBoundsException e) {
				e.printStackTrace();
			}
			mPullHeadViewHelper.handPullHead(this);
		}

		@Override
		public boolean dispatchTouchEvent(MotionEvent ev) {
			try {
				return super.dispatchTouchEvent(ev);
			} catch (IndexOutOfBoundsException e) {
				e.printStackTrace();
				return false;
			}
		}
	}
}
