//package com.lectek.android.sfreader.widgets;
//
//import android.graphics.Color;
//import android.graphics.drawable.ColorDrawable;
//import android.graphics.drawable.Drawable;
//import android.view.View;
//
//import com.lectek.android.sfreader.R;
//import com.lectek.android.sfreader.R.color;
//import com.lectek.android.widget.AbsDropDownListView;
//
//public class DropDownListView extends AbsDropDownListView {
//
//	public DropDownListView(View anchor) {
//		super(anchor);
//	}
//
//	@Override
//	public Drawable getDefaultContentBackgroundDrawable() {
////		return new ColorDrawable(Color.WHITE);
//		return getContext().getResources().getDrawable(R.drawable.pop_window_bg);
//	}
//
//	@Override
//	public Drawable getDefaultBackgroundDrawable() {
////		return new ColorDrawable(Color.BLACK);
//		return null;
//	}
//
//	@Override
//	public int getDefaultAnimationStyle() {
////		return R.style.DropDownPopWin;
//		return -1;
//	}
//
//	@Override
//	public Drawable getListViewSelector() {
////		return getContext().getResources().getDrawable(R.drawable.enter_btn_selector);
//		return null;
//	}
//
//	@Override
//	public Drawable getListViewDivider() {
//		return getContext().getResources().getDrawable(R.drawable.divider);
//	}
//}

package com.bk.android.time.ui.widget.spinner;

import android.graphics.drawable.Drawable;
import android.view.View;

import com.bk.android.assistant.R;
import com.bk.android.ui.widget.AbsDropDownListView;

public class DropDownListView extends AbsDropDownListView {
	public DropDownListView(View anchor) {
		super(anchor);
	}

	@Override
	public Drawable getDefaultContentBackgroundDrawable() {
		return null;
	}

	@Override
	public Drawable getDefaultBackgroundDrawable() {
		return getContext().getResources().getDrawable(R.drawable.spinner_bg);
	}

	@Override
	public int getDefaultAnimationStyle() {
		return R.style.DropDownPopWin;
	}

	@Override
	public Drawable getListViewSelector() {
		return null;
//		return getContext().getResources().getDrawable(R.drawable.list_selector_background);
	}

	@Override
	public Drawable getListViewDivider() {
		return getContext().getResources().getDrawable(R.drawable.line);
	}
}
