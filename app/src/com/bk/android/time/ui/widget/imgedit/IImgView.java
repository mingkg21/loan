package com.bk.android.time.ui.widget.imgedit;

import android.graphics.Canvas;
import android.graphics.Rect;

public interface IImgView {
	public Rect getViewBoundary();
	public Rect getImgBoundary();
	public void drawImgContent(Canvas canvas);
    public void invalidate();
}
