package com.bk.android.time.ui.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.bk.android.widget.AudioVolumeReader;
import com.bk.android.widget.AudioVolumeReader.VolumeListeners;

public class WindmillView extends ImageView implements Runnable,VolumeListeners{
	private static final int MAX_SPEED = 500;
	private AudioVolumeReader mAudioVolumeReader;
	private Handler mHandler;
	private Long mLastComputeTime;
	private float mPower;
	private float mSpeed;
	private int mDegrees;
	private boolean isBlow;
//	private Integer mLastVolume;
//	private Integer mStartVolume;
	
	public WindmillView(Context context, AttributeSet attrs) {
		super(context, attrs);
		mAudioVolumeReader = new AudioVolumeReader();
		mAudioVolumeReader.setVolumeListeners(this);
		mHandler = new Handler(Looper.getMainLooper());
	}

	public void start(){
		mAudioVolumeReader.start();
		isBlow = false;
		mDegrees = 0;
		mSpeed = MAX_SPEED / 3;
		mHandler.post(this);
//		mLastVolume = null;
//		mStartVolume = null;
	}
	
	public void stop(){
		mAudioVolumeReader.stop();
		isBlow = false;
//		mLastVolume = null;
//		mStartVolume = null;
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		canvas.save();
		int maxTranslate = (int) (getHeight() * 0.7f);
		if(mSpeed > 0){
			if(mSpeed < 100){
				canvas.translate(0, (100 - mSpeed) / 100f * maxTranslate);
			}
		}else{
			canvas.translate(0,maxTranslate);
		}
		canvas.save();
		canvas.rotate(mDegrees,getWidth() / 2,getHeight() / 2);
		super.onDraw(canvas);
		canvas.restore();
		canvas.restore();
	}

	@Override
	public void onVolumeChange(int volume) {
//		if(mLastVolume == null){
//			mLastVolume = volume;
//		}
//		
//		int variation1 = volume - mLastVolume;
//		if(Math.abs(variation1) > 20){
//			Log.e("消息", "切换1s="+variation1+" volume="+volume);
//			if(variation1 < 0){
//				isBlow = false;
//				mStartVolume = null;
//			}else if(variation1 > 0){
//				mStartVolume = volume;
//				isBlow = true;
//				mHandler.post(this);
//			}
//		}else if(mStartVolume != null){
//			int variation2 = mStartVolume - volume;
//			if(variation2 > 15){
//				Log.e("消息", "切换2="+variation2);
//				isBlow = false;
//				mStartVolume = null;
//			}
//		}
//		mLastVolume = volume;
//		Log.e("消息", "volume="+volume);
		if(volume > 35){
			isBlow = true;
			mHandler.post(this);
		}else{
			isBlow = false;
		}
	}

	@Override
	public void run() {
		if(mLastComputeTime == null){
			mLastComputeTime = System.currentTimeMillis();
		}
		long time = System.currentTimeMillis() - mLastComputeTime;
		if(isBlow){
			if(mPower < 0){
				mPower = 0;
			}
			mPower += time / 100f;
		}else{
			mPower -= time / 100f;
		}
		if(Math.abs(mPower) > 10){
			mPower = Math.abs(mPower) / mPower * 10;
		}
		mSpeed += mPower * time / 100f;
		if(mSpeed < 0){
			mSpeed = 0;
		}
		if(mSpeed > MAX_SPEED){
			mSpeed = MAX_SPEED;
		}
		mDegrees += mSpeed * time / 100f;
		mDegrees %= 360;
		mLastComputeTime = System.currentTimeMillis();
		invalidate();
		if(mSpeed != 0 || isBlow){
			mHandler.post(this);
		}else{
			mLastComputeTime = null;
		}
	}
}
