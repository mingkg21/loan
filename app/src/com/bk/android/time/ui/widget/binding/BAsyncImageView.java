package com.bk.android.time.ui.widget.binding;

import gueei.binding.IBindableView;
import gueei.binding.ViewAttribute;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import com.bk.android.time.ui.widget.AsyncImageView;

public class BAsyncImageView extends AsyncImageView implements IBindableView<BAsyncImageView>{
	public BAsyncImageView(Context context) {
		super(context);
	}

	public BAsyncImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	@Override
	public ViewAttribute<? extends View, ?> createViewAttribute(
			String attributeId) {
		return BAsyncImageViewUtil.createViewAttribute(attributeId, this);
	}
}
