package com.bk.android.time.ui.widget.binding;

import gueei.binding.Binder;
import gueei.binding.IBindableView;
import gueei.binding.ViewAttribute;
import gueei.binding.collections.ArrayListObservable;
import gueei.binding.listeners.ViewMulticastListener;
import gueei.binding.viewAttributes.ViewEventAttribute;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import com.bk.android.time.ui.widget.CalendarGridView;

public class BCalendarGridView extends CalendarGridView implements IBindableView<BCalendarGridView>{
	public BCalendarGridView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	public ViewAttribute<? extends View, ?> createViewAttribute(
			String attributeId) {
		if (attributeId.equals("itemSource")) {
			return new ItemSourceAttribute(this,attributeId);
		}else if (attributeId.equals("horizontalSpacing")) {
			return new HorizontalSpacingAttribute(this,attributeId);
		}else if (attributeId.equals("verticalSpacing")) {
			return new VerticalSpacingAttribute(this,attributeId);
		}else if (attributeId.equals("selectedPosition")) {
			return new SelectedPositionAttribute(this,attributeId);
		}else if (attributeId.equals("onItemClicked")) {
			return new OnItemClickedViewEvent(this,attributeId);
		}
		return null;
	}
	
	public class OnItemClickedViewEvent extends ViewEventAttribute<CalendarGridView>implements OnCalendarItemClickListener{
		public OnItemClickedViewEvent(CalendarGridView view,String attributeId) {
			super(view, attributeId);
		}
	
		@Override
		protected void registerToListener(CalendarGridView view) {
			Binder.getMulticastListenerForView(view, OnCalendarItemClickListenerMulticast.class).register(this);
		}
	
		@Override
		public void onCalendarItemClick(CalendarGridView view,int position) {
			this.invokeCommand(view,position);
		}
	}
	
	public static class OnCalendarItemClickListenerMulticast extends ViewMulticastListener<OnCalendarItemClickListener>implements OnCalendarItemClickListener {
		@Override
		public void onCalendarItemClick(CalendarGridView view,int position) {
			for(OnCalendarItemClickListener l:listeners){
				l.onCalendarItemClick(view,position);
			}
		}
		
		@Override
		public void registerToView(View v) {
			if (!(v instanceof CalendarGridView)) return;
			((CalendarGridView)v).setOnCalendarItemClickListener(this);
		}
	}

	
	public static class ItemSourceAttribute extends ViewAttribute<CalendarGridView, Object>{
		public ItemSourceAttribute(CalendarGridView view,String attributeId){
			super(Object.class, view, attributeId);
		}
		
		@Override
		public Object get() {//获取值
			return null;
		}

		@SuppressWarnings("unchecked")
		@Override
		protected void doSetAttributeValue(Object newValue) {//设置值
			if (newValue instanceof ArrayListObservable<?>) {
				getView().setData((ArrayListObservable<AbsDayItemData<?>>) newValue);
			}else{
				getView().setData(null);
			}
		}
	}
	
	public static class SelectedPositionAttribute extends ViewAttribute<CalendarGridView, Integer>{
		public SelectedPositionAttribute(CalendarGridView view,String attributeId){
			super(Integer.class, view, attributeId);
		}
		
		@Override
		public Integer get() {//获取值
			return null;
		}

		@Override
		protected void doSetAttributeValue(Object newValue) {//设置值
			if (newValue instanceof Integer) {
				getView().setSelection((Integer) newValue);
			}else{
				getView().setSelection(0);
			}
		}
	}
	
	public static class HorizontalSpacingAttribute extends ViewAttribute<CalendarGridView, Number>{
		public HorizontalSpacingAttribute(CalendarGridView view,String attributeId){
			super(Number.class, view, attributeId);
		}
		
		@Override
		public Number get() {//获取值
			return null;
		}

		@Override
		protected void doSetAttributeValue(Object newValue) {//设置值
			if (newValue instanceof Number) {
				getView().setHorizontalSpacing(((Number) newValue).intValue());
			}else{
				getView().setHorizontalSpacing(0);
			}
		}
	}
	
	public static class VerticalSpacingAttribute extends ViewAttribute<CalendarGridView, Number>{
		public VerticalSpacingAttribute(CalendarGridView view,String attributeId){
			super(Number.class, view, attributeId);
		}
		
		@Override
		public Number get() {//获取值
			return null;
		}

		@Override
		protected void doSetAttributeValue(Object newValue) {//设置值
			if (newValue instanceof Number) {
				getView().setVerticalSpacing(((Number) newValue).intValue());
			}else{
				getView().setVerticalSpacing(0);
			}
		}
	}
}
