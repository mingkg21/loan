package com.bk.android.time.ui.widget;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ListAdapter;

import com.bk.android.ui.widget.DecoratorListAdapter;

public class SudokuGridView extends GridView {
	private int mColumnWidth;
	private InteriorAdapter mInteriorAdapter;
	private int mHorizontalSpacing;
	private int mNumColumns;

	public SudokuGridView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}
	
	private void init(){
		setFocusable(false);
		mNumColumns = -1;
		mInteriorAdapter = new InteriorAdapter(null);
		super.setAdapter(mInteriorAdapter);
	}

	@Override
	public void setAdapter(ListAdapter adapter) {
		mNumColumns = -1;
		ListAdapter oldAdapter = mInteriorAdapter.getAdapter();
		mInteriorAdapter.setAdapter(adapter);
		if(oldAdapter == null || adapter == null || !oldAdapter.getClass().equals(adapter.getClass())){
			super.setAdapter(mInteriorAdapter);
		}
	}
	
	@Override
	public void setHorizontalSpacing(int horizontalSpacing) {
		mHorizontalSpacing = horizontalSpacing; 
		super.setHorizontalSpacing(horizontalSpacing);
	}
	
	@Override
	public void setNumColumns(int numColumns) {
		super.setNumColumns(numColumns);
		mNumColumns = numColumns;
	}
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int count = getCount();
		if(count > 0){
			if(count > 3){
				count = count == 4 ? 2 : 3;
			}
			if(mNumColumns != count){
				setNumColumns(count);
				requestLayout();
			}
		}
		int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2, MeasureSpec.AT_MOST);
		super.onMeasure(widthMeasureSpec, expandSpec);
		int w = getMeasuredWidth() - getPaddingLeft() - getPaddingRight();
		if(w > 0 && count > 0){
			Rect padding = new Rect();
			if(getSelector() != null){
				getSelector().getPadding(padding);
			}
			int tempW = w - mHorizontalSpacing * (count - 1) - padding.left - padding.right;
			mColumnWidth = tempW / count;
		}
	}
	
	private class InteriorAdapter extends DecoratorListAdapter{
		public InteriorAdapter(ListAdapter adapter) {
			super(adapter);
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			convertView = super.getView(position, convertView, parent);
			if(convertView != null){
				LayoutParams lp = (LayoutParams) convertView.getLayoutParams();
				int w = mColumnWidth;
				int h = mNumColumns == 2 && getCount() != mNumColumns ? w * 3 / 4 : w;
				if(lp == null){
					lp = new LayoutParams(w,h);
					convertView.setLayoutParams(lp);
				}else{
					lp.width = w;
					lp.height = h;
				}
			}
			return convertView;
		}
	}
}
