package com.bk.android.time.ui.widget.binding.command;

import java.util.ArrayList;

import android.view.View;

import com.bk.android.binding.command.BaseCommand;
import com.bk.android.time.model.lightweight.AddImgModel.BitmapInfo;
import com.bk.android.time.ui.widget.span.AsyncDrawableSpan;

public abstract class OnSpanClickCommand extends BaseCommand {
	@SuppressWarnings("unchecked")
	@Override
	protected void onInvoke(View view, Object... args) {
		if(args == null || args.length < 4
				|| !(args[0] instanceof Boolean)
				|| !(args[1] instanceof ArrayList)
				|| !(args[2] instanceof Integer)
				|| !(args[3] instanceof AsyncDrawableSpan)){
			return;
		}
		onSpanClick((Boolean) args[0],(ArrayList<BitmapInfo>)args[1],(Integer)args[2],(AsyncDrawableSpan)args[3]);
	}
	
	public abstract void onSpanClick(boolean isEnabled, ArrayList<BitmapInfo> imgs,int position, AsyncDrawableSpan span);
}
