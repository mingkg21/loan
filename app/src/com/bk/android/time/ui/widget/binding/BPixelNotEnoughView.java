package com.bk.android.time.ui.widget.binding;

import gueei.binding.IBindableView;
import gueei.binding.ViewAttribute;

import java.util.HashMap;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

import com.bk.android.assistant.R;
import com.bk.android.dao.DBKeyValueProviderProxy;
import com.bk.android.data.net.RequestData;
import com.bk.android.data.net.ResponseData;
import com.bk.android.os.TerminableThread;
import com.bk.android.time.data.dao.DBPreferencesProvider;
import com.bk.android.time.data.net.HttpConnect;
import com.bk.android.time.entity.BaseDataEntity;
import com.bk.android.time.ui.photo.ImageHandler;
import com.bk.android.time.util.ToastUtil;
import com.bk.android.util.ApnUtil;
import com.bk.android.util.LogUtil;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

public class BPixelNotEnoughView extends ImageView implements IBindableView<BPixelNotEnoughView>, OnClickListener{
	private static final String TAG = "BPixelNotEnoughView";
	private static final String TYPE_PIXEL_NOT_ENOUGH_VIEW = "TYPE_PIXEL_NOT_ENOUGH_VIEW";
	private static final HashMap<String, String> SIZE_INFO_MAP = new HashMap<String, String>();

	private String mSrc;
	private Drawable mTipDrawable;
	private TerminableThread mTerminableThread;
	private Boolean isPixelNotEnough;
	private Paint mPaint;
	private boolean isAttachedToWin;
	
	public BPixelNotEnoughView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}
	
	private void init(){
		mPaint = new Paint();
		mTipDrawable = getResources().getDrawable(R.drawable.ic_pixel_not_enough_tip);
		setOnClickListener(this);
	}
	
	public void setSrc(String src){
		if(mSrc == src){
			return;
		}
		cancelTask();
		LogUtil.v(TAG, "setSrc src=" + src);
		mSrc = src;
		isPixelNotEnough = null;
		doTask();
	}
	
	private void cancelTask(){
		if(mTerminableThread != null){
			LogUtil.i(TAG, "cancelTask src=" + mSrc);
			mTerminableThread.cancel();
			mTerminableThread = null;
		}
	}
	
	public boolean isAttachedToWin() {
		return isAttachedToWin;
	}
	
	private void doTask(){
		final String url = getParseSrc();
		if(!TextUtils.isEmpty(url)){
			if(getVisibility() == View.VISIBLE && isAttachedToWin()){
				LogUtil.v(TAG, "doTask src=" + mSrc);
				SizeInfo sizeInfo = getSizeInfo(url);
				if(sizeInfo != null){
					LogUtil.i(TAG, "doTask hasData w=" + sizeInfo.getWidth() + " h=" + sizeInfo.getHeight());
					fillData(sizeInfo);
				}else{
					fillData(null);
					LogUtil.v(TAG, "doTask notData");
					if(mTerminableThread == null && isPixelNotEnough == null && ApnUtil.isNetAvailable(getContext())){
						LogUtil.i(TAG, "doTask do get Data");
						mTerminableThread = new TerminableThread(){
							boolean isSucceed = false;
							@Override
							public void run() {
								try {
									RequestData requestData = new RequestData(RequestData.REQUEST_METHOD_GET, "", "");
									ResponseData responseData = HttpConnect.getInstance().requestData(url, requestData);
									if(responseData.resultContent != null){
										String content = new String(responseData.resultContent.toByteArray(), "UTF-8");
										if(!TextUtils.isEmpty(content)){
											saveSizeInfo(url, content);
											isSucceed = true;
										}
									}
									post(new Runnable() {
										@Override
										public void run() {
											mTerminableThread = null;
											SizeInfo info = null;
											if(isSucceed){
												info = getSizeInfo(url);
											}
											LogUtil.i(TAG, "doTask do get Data isSucceed=" + isSucceed + " info=" + info);
											if(info == null){
												info = new SizeInfo();
											}
											fillData(info);
										}
									});
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						};
						mTerminableThread.start();
					}
				}
			}else{
				fillData(null);
			}
		}else{
			fillData(new SizeInfo());
		}
	}
	
	private void fillData(SizeInfo sizeInfo){
		if(sizeInfo == null){
			isPixelNotEnough = null;
		}else{
			isPixelNotEnough = ImageHandler.isPixelNotEnoughNotCheck(sizeInfo.getWidth(), sizeInfo.getHeight());
		}
		setClickable(isPixelNotEnough == null || isPixelNotEnough);
		invalidate();
	}
	
	private SizeInfo getSizeInfo(String src){
		String key = String.valueOf(src.hashCode());
		String json = SIZE_INFO_MAP.get(key);
		if(json == null){
			json = getProxy().getString(key, TYPE_PIXEL_NOT_ENOUGH_VIEW, null);
			SIZE_INFO_MAP.put(key, json);
		}
		SizeInfo sizeInfo = null;
		if(!TextUtils.isEmpty(json)){
			try {
				sizeInfo = new Gson().fromJson(json, SizeInfo.class);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return sizeInfo ;
	}
	
	private void saveSizeInfo(String src, String sizeInfo){
		String key = String.valueOf(src.hashCode());
		getProxy().putString(key, sizeInfo, TYPE_PIXEL_NOT_ENOUGH_VIEW);
		SIZE_INFO_MAP.put(key, sizeInfo);
	}
	
	private String getParseSrc(){
		String src = mSrc;
		if(!TextUtils.isEmpty(src)){
			int index = src.indexOf("http://osscdn.banketime.com");
			if(index != -1){
				src = src.replace("http://osscdn.banketime.com", "http://osscdn2.banketime.com");
			}else{
				index = src.indexOf("http://osscdn2.banketime.com");
			}
			if(index != -1){
				index = src.lastIndexOf("@");
				if(index == -1){
					src = src + "@info";
				}else{
					src = src.substring(0, index) + "@info";
				}
			}else{
				src = null;
			}
		}
		return src;
	}
	
	@Override
	public void setVisibility(int visibility) {
		super.setVisibility(visibility);
		if(visibility == View.VISIBLE){
			doTask();
		}else{
			cancelTask();
		}
	}
	
	@Override
	protected void onAttachedToWindow() {
		isAttachedToWin = true;
		super.onAttachedToWindow();
		doTask();
	}
	
	@Override
	protected void onDetachedFromWindow() {
		isAttachedToWin = false;
		super.onDetachedFromWindow();
		cancelTask();
	}
	
	@Override
	public void draw(Canvas canvas) {
		if(isPixelNotEnough == null){
			super.draw(canvas);
		}else if(isPixelNotEnough){
			int color = Color.parseColor("#77ffffff");
			mPaint.setColor(color);
			canvas.drawRect(0, 0, getWidth(), getHeight(), mPaint);
			mTipDrawable.setBounds(0 , getBottom() - mTipDrawable.getIntrinsicHeight() , mTipDrawable.getIntrinsicWidth(), getBottom());
			mTipDrawable.draw(canvas);
		}
	}

	@Override
	public void onClick(View v) {
		ToastUtil.showToast(getContext(), R.string.pixel_not_enough);
	}
	
	private static DBKeyValueProviderProxy getProxy() {
		return DBPreferencesProvider.getProxy();
	}
	
	@Override
	public ViewAttribute<? extends View, ?> createViewAttribute(String attributeId) {
		if (attributeId.equals("imageUrl")) {//设置对于的属性值
			return new ImageUrlViewAttribute(this,attributeId);
		}
		return null;
	}
	
	public static class ImageUrlViewAttribute extends ViewAttribute<BPixelNotEnoughView, String>{
		public ImageUrlViewAttribute(BPixelNotEnoughView view,String attributeId){
			super(String.class, view, attributeId);
		}
		
		@Override
		public String get() {//获取值
			return null;
		}

		@Override
		protected void doSetAttributeValue(Object newValue) {//设置值
			if (newValue instanceof String) {
				getView().setSrc((String) newValue);
			}else{
				getView().setSrc(null);
			}
		}
	}
	
	public static class SizeInfo extends BaseDataEntity{
		private static final long serialVersionUID = -7052838834627586927L;
		@SerializedName("width")
		private int width;
		@SerializedName("height")
		private int height;
		@SerializedName("size")
		private int size;
		/**
		 * @return the width
		 */
		public int getWidth() {
			return width;
		}
		/**
		 * @param width the width to set
		 */
		public void setWidth(int width) {
			this.width = width;
		}
		/**
		 * @return the height
		 */
		public int getHeight() {
			return height;
		}
		/**
		 * @param height the height to set
		 */
		public void setHeight(int height) {
			this.height = height;
		}
		/**
		 * @return the size
		 */
		public int getSize() {
			return size;
		}
		/**
		 * @param size the size to set
		 */
		public void setSize(int size) {
			this.size = size;
		}
	}
}
