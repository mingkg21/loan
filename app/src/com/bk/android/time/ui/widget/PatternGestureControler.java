package com.bk.android.time.ui.widget;

import android.graphics.PointF;
import android.view.MotionEvent;
import android.view.ViewConfiguration;

import com.bk.android.time.app.App;

public class PatternGestureControler {
	private IPatternInfo mPattern;
	private PointF mDownPoint = new PointF();
	private Float mDownX;
	private Float mDownY;
	private Float mDownDoubleDistance;
	private double mDownDoubleDegree;
	private float mInitPatternDegree;
	private float mInitPatternScale;
	private boolean mCanTouchPointDegree;
	private boolean mCanTouchDegree;
	private boolean isTouchPointDegree;
	private int mScreenWidth;
	private int mTouchSlop;

	public PatternGestureControler(IPatternInfo viewInfo){
		mPattern = viewInfo;
		mScreenWidth = App.getInstance().getResources().getDisplayMetrics().widthPixels;
        mTouchSlop = ViewConfiguration.get(App.getInstance()).getScaledTouchSlop();
	}
	
	public void setCanTouchPointDegree(boolean canTouchPointDegree){
		mCanTouchPointDegree = canTouchPointDegree;
	}
	
	public void setCanTouchDegree(boolean canTouchDegree){
		mCanTouchDegree = canTouchDegree;
	}
	
	public void onTouchEvent(MotionEvent event) {
		PointF point1 = new PointF(event.getX(0), event.getY(0));
		PointF point2 = null;
		float x = mPattern.getX();
		float y = mPattern.getY();
		float scale = mPattern.getScale();
		float degree = mPattern.getDegree();
		if (event.getPointerCount() == 1) {
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				if(mCanTouchPointDegree){
					float[] point = rotatePoint(mPattern.getX(), mPattern.getY() , -mPattern.getDegree() + 180);
					if(containPoint(point, mPattern.getTouchPointWidth() , point1.x, point1.y)){
						point = rotatePoint(point1.x, point1.y , 180);
						point2 = new PointF(point[0],point[1]);
						isTouchPointDegree = true;
					}
				}
				mDownPoint.set(point1);
				mDownX = x;
				mDownY = y;
			} else if (mDownX != null) {
				if (event.getAction() == MotionEvent.ACTION_MOVE) {
					if(isTouchPointDegree){
						float[] point = rotatePoint(point1.x, point1.y , 180);
						point2 = new PointF(point[0],point[1]);
					}else{
						x = mDownX - mDownPoint.x + point1.x;
						y = mDownY - mDownPoint.y + point1.y;
					}
				} else if (event.getAction() == MotionEvent.ACTION_UP) {
					float distance = PointF.length(mDownX - x, mDownY - y);
					float[] point = rotatePoint(mPattern.getX(), mPattern.getY(), -mPattern.getDegree());
					if(distance < mTouchSlop){
						if (containPoint(point, mPattern.getTouchPointWidth() , point1.x, point1.y)) {
							mPattern.onClose();
						}
					}
				}
			}
		}else if(mCanTouchDegree && event.getPointerCount() == 2){
			point2 = new PointF(event.getX(1), event.getY(1));
			if(event.getActionMasked() == MotionEvent.ACTION_POINTER_DOWN){
				initTouch();
			}
		}
		
		if (point2 != null) {
			if (event.getAction() == MotionEvent.ACTION_MOVE) {
				float relativeX = point1.x - point2.x;
				float relativeY = point1.y - point2.y;
				float distance = PointF.length(relativeX, relativeY);
				if (mDownDoubleDistance == null) {
					mDownDoubleDegree = computeDegree(relativeX, relativeY);
					mInitPatternDegree = mPattern.getDegree();
					mDownDoubleDistance = distance;
					mInitPatternScale = scale;
				} else {
					double degreeTemp = computeDegree(relativeX, relativeY);
					degree = (float) (mInitPatternDegree + mDownDoubleDegree - degreeTemp);
					scale = mInitPatternScale + (distance - mDownDoubleDistance) / mScreenWidth * 2;
					if (scale > mPattern.maxScale()) {
						scale = mPattern.maxScale();
					}
					if (scale < mPattern.minScale()) {
						scale = mPattern.minScale();
					}
					x += (mPattern.getScale() - scale) * mPattern.getWidth() / 2;
					y += (mPattern.getScale() - scale) * mPattern.getHeight() / 2;
				}
			}
		}
		if (event.getAction() == MotionEvent.ACTION_UP
				|| event.getAction() == MotionEvent.ACTION_CANCEL
				|| event.getAction() == MotionEvent.ACTION_OUTSIDE
				|| event.getActionMasked() == MotionEvent.ACTION_POINTER_UP) {
			initTouch();
		} else if (event.getAction() == MotionEvent.ACTION_MOVE) {
			mPattern.setX(x);
			mPattern.setY(y);
			if (point2 == null) {
				if (mDownX != null && mPattern.checkPatternBound()) {
					mDownX = mPattern.getX();
					mDownY = mPattern.getY();
					mDownPoint.x = event.getX();
					mDownPoint.y = event.getY();
				}
			} else {
				mPattern.setScale(scale);
				mPattern.checkPatternBound();
				mPattern.setDegree(degree);
			}
		}
	}
	
	private void initTouch(){
		mDownX = null;
		mDownY = null;
		mDownDoubleDistance = null;
		isTouchPointDegree = false;
		mDownDoubleDistance = null;
		isTouchPointDegree = false;
	}
	
	private double computeDegree(float relativeX, float relativeY){
		double degreeTemp = Math.atan(relativeX / relativeY) / Math.PI * 180;
		if(relativeX < 0 && relativeY < 0 || relativeX > 0 && relativeY < 0){
			degreeTemp = 180 + degreeTemp;
		}
		return degreeTemp;
	}
	
	private boolean containPoint(float[] point,int w,float x,float y){
		return point[0] - w < x
				&& point[0] + w > x
				&& point[1] - w < y
				&& point[1] + w > y;
	}
	
	public float[] rotatePoint(float x, float y, float degree) {
		// A(x,y) 绕 O(x0,y0) 旋转 角度 k 之后的点 B(x2,y2)
		float k = new Float(Math.toRadians(degree));
		float x0 = mPattern.getX() + mPattern.getWidth() * mPattern.getScale() / 2;
		float y0 = mPattern.getY() + mPattern.getHeight() * mPattern.getScale() / 2;
		float x2 = new Float((x - x0) * Math.cos(k) + (y - y0) * Math.sin(k) + x0);
		float y2 = new Float(-(x - x0) * Math.sin(k) + (y - y0) * Math.cos(k) + y0);
		return new float[] { x2, y2 };
	}
	
	public interface IPatternInfo{
		public float getX();
		public float getY();
		public void setX(float x);
		public void setY(float y);
		public void onClose();
		public void setDegree(float degree);
		public void setScale(float scale);
		public boolean checkPatternBound();
		public float maxScale();
		public float minScale();
		public float getWidth();
		public float getHeight();
		public float getScale();
		public float getDegree();
		public int getTouchPointWidth();
	}
}
