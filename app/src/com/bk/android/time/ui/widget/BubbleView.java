package com.bk.android.time.ui.widget;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PointF;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.bk.android.assistant.R;
import com.bk.android.widget.AudioVolumeReader;
import com.bk.android.widget.AudioVolumeReader.VolumeListeners;

public class BubbleView extends View implements Runnable,VolumeListeners{
	private AudioVolumeReader mAudioVolumeReader;
	private Drawable mBubbleDrawable;
	private LinkedList<Particle> mParticleList;
	private LinkedList<Particle> mParticleRecycleList;
	private Handler mHandler;
	private Long mLastComputeTime;
	private Long mLastCreateTime;
	private boolean isBlow;
	private Random mRandom;
	private BubbleAnimListener mBubbleAnimListener;
	private PointF mLastPointF;
	private BladeGesture mBladeGesture;
	
	public BubbleView(Context context, AttributeSet attrs) {
		super(context, attrs);
		mAudioVolumeReader = new AudioVolumeReader();
		mAudioVolumeReader.setVolumeListeners(this);
		mBubbleDrawable = getResources().getDrawable(R.drawable.bubble);
		mParticleList = new LinkedList<Particle>();
		mParticleRecycleList = new LinkedList<Particle>();
		mHandler = new Handler(Looper.getMainLooper());
		mRandom = new Random();
		mBladeGesture = new BladeGesture();
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if(event.getAction() == MotionEvent.ACTION_DOWN){
			mLastPointF = new PointF();
		}else if(event.getAction() == MotionEvent.ACTION_UP){
			mLastPointF = null;
		}else if(mLastPointF != null){
			mLastPointF.set(event.getX(), event.getY());
		}
		mBladeGesture.onTouchEvent(event);
		invalidate();
		return true;
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		if(mBladeGesture.drawPoint(canvas)){
			invalidate();
		}
		recycleParticle();
		for (Particle particle : mParticleList) {
			particle.draw(canvas, mBubbleDrawable);
		}
	}
	
	private void recycleParticle(){
		for (Iterator<Particle> iterator = mParticleList.iterator(); iterator.hasNext();) {
			Particle particle = iterator.next();
			boolean isDelete = false;
			if(particle.isBeyondBoundary(getLeft(), getTop(), getRight(), getBottom())){
				isDelete = true;
			}else if(mLastPointF != null && particle.isContainPoint(mLastPointF.x,mLastPointF.y)){
				isDelete = true;
			}
			if(isDelete){
				iterator.remove();
				mParticleRecycleList.add(particle);
				performBubbleSizeChange(mParticleList.size());
			}
		}
	}
	
	public void start(){
		mAudioVolumeReader.start();
		isBlow = false;
	}
	
	public void stop(){
		mAudioVolumeReader.stop();
		isBlow = false;
	}
	
	@Override
	public void run() {
		if(mLastCreateTime == null){
			mLastCreateTime = System.currentTimeMillis();
		}
		long createTime = System.currentTimeMillis() - mLastCreateTime;
		if(isBlow && mParticleList.size() < 50 && createTime > 100){
			Particle particle = createParticle();
			mParticleList.add(particle);
			mLastCreateTime = System.currentTimeMillis();
			performBubbleSizeChange(mParticleList.size());
		}
		
		if(mLastComputeTime == null){
			mLastComputeTime = System.currentTimeMillis();
		}
		long time = System.currentTimeMillis() - mLastComputeTime;
		if(time > 200){
			for (Particle particle : mParticleList) {
				float powerX = getFrictionX(particle.mSpeedX,particle.mPowerX);
				float powerY = getFrictionY(particle.mSpeedY,particle.mPowerY);
				particle.addPower(powerX, powerY);
			}
			mLastComputeTime = System.currentTimeMillis();
		}
		invalidate();
		if(!mParticleList.isEmpty() || isBlow){
			mHandler.post(this);
		}else{
			mLastComputeTime = null;
			mLastCreateTime = null;
		}
	} 
	
	private float getFrictionX(float speed,float power){
		if(Math.abs(speed) < 20){
			float p = getRandomOrientation() * getRandomValue(1,0);
			if(p + power == 0){
				p = -p; 
			}
			return p;
		}
		return -speed * 0.1f;
	}
	
	private float getFrictionY(float speed,float power){
		if(Math.abs(speed) < 20){
			return -power - 0.05f;
		}
		return -speed * 0.05f;
	}
	
	private int getRandomValue(int max,int min){
		return mRandom.nextInt(max + 1 - min) + min;
	}
	
	private int getRandomOrientation(){
		double random = Math.random();
		if(random > 0.5f){
			return 1;
		}else{
			return -1;
		}
	}
	
	private Particle createParticle(){
		int type = getRandomValue(Particle.TYPE_NEAR, Particle.TYPE_FAR);
		int x = getWidth() / 2;
		int y = getHeight() - mBubbleDrawable.getIntrinsicHeight() / 2;
		int speedX = getRandomOrientation() * getRandomValue(20, 5);
		int speedY = -getRandomValue(100,10);
		Particle particle = null;
		if(mParticleRecycleList.isEmpty()){
			particle = new Particle(type,mBubbleDrawable.getIntrinsicWidth(), mBubbleDrawable.getIntrinsicHeight());
		}else{
			particle = mParticleRecycleList.removeFirst();
			particle.init(type,mBubbleDrawable.getIntrinsicWidth(), mBubbleDrawable.getIntrinsicHeight());
		}
		particle.addPower(0, 0);
		particle.addLocal(x, y);
		particle.addSpeed(speedX, speedY);
		return particle;
	}
	
	@Override
	public void onVolumeChange(int volume) {
		if(volume > 35){
			isBlow = true;
			mHandler.post(this);
		}else{
			isBlow = false;
		}
	}
	
	public int getBubbleSize(){
		return mParticleList.size();
	}
	
	public void performBubbleSizeChange(int size){
		if(mBubbleAnimListener != null){
			mBubbleAnimListener.onBubbleSizeChange(size);
		}
	}
	
	public void setBubbleAnimListener(BubbleAnimListener l){
		mBubbleAnimListener = l;
	}
	
	public class Particle{
		/** 远 */
		private static final int TYPE_FAR = 0;
		/** 正常 */
		private static final int TYPE_NORMAL = 1;
		/** 近 */
		private static final int TYPE_NEAR = 2;

		private int mSize;
		private float mX;
		private float mY;
		private float mSpeedX;
		private float mSpeedY;
		private float mPowerX;
		private float mPowerY;
		private long mLastComputeTime;
		private int mWidth;
		private int mHeight;
		private int mColor;

		public Particle(int type,int initcWidth,int initcHeight){
			init(type,initcWidth,initcHeight);
		}
		
		public void init(int type,int initcWidth,int initcHeight){
			mSize = type;
			mX = 0;
			mY = 0;
			mSpeedX = 0;
			mSpeedY = 0;
			mPowerX = 0;
			mPowerY = 0;
			mLastComputeTime = System.currentTimeMillis();
			switch (mSize) {
				case TYPE_FAR:
					mWidth = (int) (initcWidth * 0.8f);
					mHeight = (int) (initcHeight * 0.8f);
					mColor = getColor();
					break;
				case TYPE_NORMAL:
					mWidth = initcWidth;
					mHeight = initcHeight;
					mColor = getColor();
					break;
				case TYPE_NEAR:
					mWidth = (int) (initcWidth * 1.2f);
					mHeight = (int) (initcHeight * 1.2f);
					mColor = getColor();
					break;
			}
			
		}
		
		private int getColor(){
			int type = getRandomValue(3, 1);
			switch (type) {
			case 1:
				return Color.parseColor("#F5A5C8");
			case 2:
				return Color.TRANSPARENT;
			case 3:
				return Color.parseColor("#67BBEA");
			}
			return Color.TRANSPARENT;
		}
		
		private void computeSpeed(long time){
			mSpeedX += mPowerX * time / 100;
			mSpeedY += mPowerY * time / 100;
		}
		
		private void computeLocation(long time){
			float moveX = mSpeedX * time / 100;
			float moveY = mSpeedY * time / 100;
			mX += moveX;
			mY += moveY;
		}
		
		public void draw(Canvas canvas,Drawable bubbleDrawable){
			long time = System.currentTimeMillis() - mLastComputeTime;
			computeSpeed(time);
			computeLocation(time);
			mLastComputeTime = System.currentTimeMillis();
			bubbleDrawable.setBounds((int)mX,(int)mY,(int)(mX + mWidth),(int)(mY + mHeight));
			bubbleDrawable.setColorFilter(mColor, Mode.SRC_ATOP);
			bubbleDrawable.draw(canvas);
		}
		
		public void addPower(float powerX,float powerY){
			mPowerX += powerX;
			mPowerY += powerY;
		}
		
		public void addSpeed(int speedX,int speedY){
			mSpeedX += speedX;
			mSpeedY += speedY;
		}
		
		public void addLocal(int x,int y){
			mX += x;
			mY += y;
		}
		
		public boolean isContainPoint(float x,float y){
			if(mX + mWidth >= x
					&& mX <= x
					&& mY + mHeight >= y
					&& mY <= y){
				return true;
			}
			return false;
		}
		
		public boolean isBeyondBoundary(int left, int top, int right, int bottom){
			if(mX + mWidth <= left
					|| mX >= right
					|| mY + mHeight <= top
					|| mY >= bottom){
				return true;
			}
			return false;
		}
	}
	
	public interface BubbleAnimListener{
		public void onBubbleSizeChange(int size);
	}
}
