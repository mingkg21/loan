package com.bk.android.time.ui.widget.binding.command;

import android.view.View;

import com.bk.android.binding.command.BaseCommand;
import com.bk.android.time.ui.widget.CalendarGridView;

public abstract class OnCalendarItemClickCommand extends BaseCommand {
	@Override
	protected void onInvoke(View view, Object... args) {
		if(args == null || args.length < 1
				|| !(view instanceof CalendarGridView)
				|| !(args[0] instanceof Integer)){
			return;
		}
		onCalendarItemClick((CalendarGridView) view,(Integer) args[0]);
	}
	
	public abstract void onCalendarItemClick(CalendarGridView view, int position);
}
