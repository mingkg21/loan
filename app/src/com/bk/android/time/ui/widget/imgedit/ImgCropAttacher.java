package com.bk.android.time.ui.widget.imgedit;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;

import com.bk.android.util.DimensionsUtil;

public class ImgCropAttacher {
	private IImgView mImgView;
	private Paint mTempPaint;
	private Paint mDstOutMaskPaint;
	private Rect mCropRect;
	private int[] mCropScale;
	private boolean isCroping;
	private CropCallback mCallback;
	private boolean isConfirmCropDraw;
	
	public ImgCropAttacher(IImgView imgView,CropCallback callback){
		mImgView = imgView;
		mCropScale = new int[]{1,1};
		mCallback = callback;
		mTempPaint = new Paint();
		mDstOutMaskPaint = new Paint();
		mDstOutMaskPaint.setFilterBitmap(false);
		mDstOutMaskPaint.setXfermode(new PorterDuffXfermode(Mode.DST_OUT));
	}
	
	private void init(boolean isCrop,boolean isCropScaleChange){
		mCropRect = null;
		mCallback.onCropInit(isCrop,isCropScaleChange);
		mImgView.invalidate();
	}
	
	public boolean isCroping() {
		return isCroping;
	}

	public void setCroping(boolean croping){
		if(croping != isCroping){
			isCroping = croping;
			init(croping,false);
		}
	}
	
	public void setCropScale(int wScale,int hScale){
		if(mCropScale[0] != wScale || mCropScale[1] != hScale){
			mCropScale = new int[]{wScale,hScale};
			if(isCroping){
				init(isCroping,true);
			}
		}
	}
	
	public Bitmap confirmCrop(){
		if(mCropRect != null && isCroping){
			Rect imgRect = mImgView.getImgBoundary();
			int left,right,top,bottom;
			int width = imgRect.width();
			int height = imgRect.height();
			if(imgRect.left <= mCropRect.left){
				left = mCropRect.left;
			}else{
				left = (int) imgRect.left;
			}
			if(imgRect.top <= mCropRect.top){
				top = mCropRect.top;
			}else{
				top = (int) imgRect.top;
			}
			if(imgRect.left + width >= mCropRect.right){
				right = mCropRect.right;
			}else{
				right = (int) (imgRect.left + width);
			}
			if(imgRect.top + height >= mCropRect.bottom){
				bottom = mCropRect.bottom;
			}else{
				bottom = (int) (imgRect.top + height);
			}
			Bitmap bitmap = Bitmap.createBitmap(right - left,bottom - top, Config.ARGB_8888);
			Canvas canvas = new Canvas(bitmap);
			canvas.translate(-left, -top);
			canvas.clipRect(left, top, right, bottom);
			isConfirmCropDraw = true;
			mImgView.drawImgContent(canvas);
			isConfirmCropDraw = false;
			return bitmap;
		}
		return null;
	}
	
	boolean initCropPatternLocation(RectF imgRect,Rect cropRect,Rect viewRect) {
		float scaleWH = mCropScale[0] * 1f / mCropScale[1];
		int width = (int) imgRect.width();
		int height = (int) imgRect.height();
		boolean isHorizontal = width / scaleWH < height;
		if(mCropRect == null){
			int cropInitWidth;
			int cropInitHeight;
			if(isHorizontal){
				cropInitWidth = width;
				if(viewRect.width() < cropInitWidth){
					cropInitWidth = viewRect.width();
				}
				cropInitHeight = (int) (cropInitWidth / scaleWH);
			}else{
				cropInitHeight = height;
				if(viewRect.height() < cropInitHeight){
					cropInitHeight = viewRect.height();
				}
				cropInitWidth = (int) (scaleWH * cropInitHeight);
			}
			int cropInitLeft = (int) imgRect.left + (width - cropInitWidth) / 2;
			int cropInitTop = (int) imgRect.top + (height - cropInitHeight) / 2;
			mCropRect = new Rect(cropInitLeft,cropInitTop,cropInitLeft + cropInitWidth,cropInitTop + cropInitHeight);
		}
		cropRect.set(mCropRect);
		return isHorizontal;
	}
	
	public void drawCropContent(Canvas canvas){
		if(!isCroping || !mCallback.isCropInitFinish() || mCropRect == null){
			return;
		}
		Rect viewRect = mImgView.getViewBoundary();
		int sc = canvas.saveLayer(viewRect.left,viewRect.top,viewRect.right,viewRect.bottom, null,  
                Canvas.MATRIX_SAVE_FLAG | Canvas.CLIP_SAVE_FLAG  
                        | Canvas.HAS_ALPHA_LAYER_SAVE_FLAG  
                        | Canvas.FULL_COLOR_LAYER_SAVE_FLAG  
                        | Canvas.CLIP_TO_LAYER_SAVE_FLAG);
		mTempPaint.setColor(0xBB000000);
		canvas.drawRect(viewRect, mTempPaint);
		mTempPaint.setColor(Color.WHITE);
		int borderW = DimensionsUtil.DIPToPX(1);
		mTempPaint.setStrokeWidth(borderW);
		int borderBorderW = borderW + DimensionsUtil.DIPToPX(2);
		int borderBorderLength = (mCropRect.width() < mCropRect.height() ? mCropRect.width() : mCropRect.height()) / 5;
		int left = mCropRect.left - borderBorderW;
		int right = mCropRect.left + borderBorderLength;
		int top = mCropRect.top - borderBorderW;
		int bottom = mCropRect.top + borderBorderLength;
		canvas.drawRect(left,top,right,bottom,mTempPaint);
		bottom = mCropRect.bottom + borderBorderW;
		top = mCropRect.bottom - borderBorderLength;
		canvas.drawRect(left,top,right,bottom,mTempPaint);
		
		top = mCropRect.top - borderBorderW;
		bottom = mCropRect.top + borderBorderLength;
		right = mCropRect.right + borderBorderW;
		left = mCropRect.right - borderBorderLength;
		canvas.drawRect(left,top,right,bottom,mTempPaint);
		bottom = mCropRect.bottom + borderBorderW;
		top = mCropRect.bottom - borderBorderLength;
		canvas.drawRect(left,top,right,bottom,mTempPaint);

		canvas.drawRect(mCropRect.left - borderW, mCropRect.top - borderW, mCropRect.right + borderW, mCropRect.bottom + borderW, mTempPaint);
		canvas.drawRect(mCropRect, mDstOutMaskPaint);
		canvas.restoreToCount(sc); 
		
		if(!isConfirmCropDraw){
			top = mCropRect.top + mCropRect.height() / 3;
			canvas.drawLine(mCropRect.left, top, mCropRect.right, top, mTempPaint);
			top += mCropRect.height() / 3;
			canvas.drawLine(mCropRect.left, top, mCropRect.right, top, mTempPaint);
			top += mCropRect.height() / 3;
			canvas.drawLine(mCropRect.left, top, mCropRect.right, top, mTempPaint);
			
			left = mCropRect.left + mCropRect.width() / 3;
			canvas.drawLine(left, mCropRect.top,left, mCropRect.bottom, mTempPaint);
			left += mCropRect.width() / 3;
			canvas.drawLine(left, mCropRect.top,left, mCropRect.bottom, mTempPaint);
			left += mCropRect.width() / 3;
			canvas.drawLine(left, mCropRect.top,left, mCropRect.bottom, mTempPaint);
		}
	}
	
	public interface CropCallback{
		public void onCropInit(boolean isCrop,boolean isCropScaleChange);
		public boolean isCropInitFinish(); 
	}
}
