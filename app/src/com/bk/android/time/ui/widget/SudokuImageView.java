package com.bk.android.time.ui.widget;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.FrameLayout;

import com.bk.android.time.ui.widget.AsyncImageViewManager.ImageInfo;
import com.bk.android.util.BitmapUtil;

public class SudokuImageView extends FrameLayout {
	public final int TYPE_SUDOKU = 0;
	public final int TYPE_FANCY = 1;
    private ArrayList<AsyncImageDrawable> mDrawableList = new ArrayList<AsyncImageDrawable>();
    private LinkedList<AsyncImageDrawable> mCacheDrawableList = new LinkedList<AsyncImageDrawable>();
    private ArrayList<ImageInfo> mImageInfoList = new ArrayList<ImageInfo>();
    
    private int mSpacing = 0;
	private int mDefaultImgRes = 0;
	private int mItemWidth = 0;
	private int mItemHeight = 0;
	private int mColumn = 0;
	private int mQuality = BitmapUtil.IMG_QUALITY_1;
	private OnSudokuItemClickListener mOnSudokuItemClickListener;
	private int mClickIndex;
	
	public SudokuImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setFocusable(false);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if(event.getAction() == MotionEvent.ACTION_DOWN){
			mClickIndex = -1;
			int i = 0;
			for (AsyncImageDrawable drawable : mDrawableList) {
				if(drawable.getBounds().contains((int)event.getX(),(int)event.getY())){
//					drawable.setPress(true);
					mClickIndex = i;
					break;
				}
				i++;
			}
		}else if(event.getAction() == MotionEvent.ACTION_UP){
			if(mOnSudokuItemClickListener != null && mClickIndex != -1){
				mOnSudokuItemClickListener.onSudokuItemClick(this, mClickIndex);
			}
		}
		if(event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL || event.getAction() == MotionEvent.ACTION_OUTSIDE){
//			for (AsyncImageDrawable drawable : mDrawableList) {
//				drawable.setPress(false);
//			}
		}
		return mClickIndex != -1;
	}
	
	public void setDefaultImgRes(int defaultImgRes){
		mDefaultImgRes = defaultImgRes;
		for (AsyncImageDrawable drawable : mDrawableList) {
			drawable.setDefaultImgRes(defaultImgRes);
		}
	}
	
	public void setQuality(int quality){
		mQuality = quality;
		for (AsyncImageDrawable drawable : mDrawableList) {
			drawable.setQuality(quality);
		}
	}
	
	public void setData(Collection<Object> imgInfoList){
		mImageInfoList.clear();
		if(imgInfoList != null){
			for (Object object : imgInfoList) {
				if(object instanceof ImageInfo){
					mImageInfoList.add((ImageInfo) object);
				}else if(object instanceof String){
					mImageInfoList.add(new ImageInfo((String) object, 0, 0));
				}
			}
		}
		requestLayout();
	}
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		int column = mImageInfoList.size();
		if(column > 0){
			if(column > 2){
				column = column == 4 ? 2 : 3;
			}
			int vWidth = getMeasuredWidth();
			int vHeight = 0;
			if(vWidth > 0){
				int tempW = vWidth - mSpacing * (column - 1) - getPaddingLeft() - getPaddingRight();
				mItemHeight = mItemWidth = tempW / column;
				int line = mImageInfoList.size() / column;
				if(mImageInfoList.size() == 3){
					line = mImageInfoList.size() - 1;
					mItemHeight = mItemHeight * 2 / (mImageInfoList.size() - 1);
				}else{
					line = mImageInfoList.size() % column > 0 ? line + 1 : line;
				}
				vHeight = mItemHeight * line + mSpacing * (line - 1) + getPaddingTop() + getPaddingBottom();
				if(vHeight > getMeasuredHeight()){
					setMeasuredDimension(vWidth, vHeight);
				}
			}
		}
		mColumn = column;
	}
	
	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		super.onLayout(changed, l, t, r, b);
		int left = getPaddingLeft();
		int top = getPaddingTop();
		boolean isHandBounds = false;
		int itemWidth = mItemWidth;
		int itemHeight = mItemHeight;
		int column = mColumn;
		for (int i = 0;(i < mImageInfoList.size() || i < mDrawableList.size()) && i >= 0; i++) {
			isHandBounds = false;
			if(i < mImageInfoList.size()){
				AsyncImageDrawable drawable = null;
				if(i < mDrawableList.size()){
					drawable = mDrawableList.get(i);
				}else{
					drawable = newDrawable();
					mDrawableList.add(drawable);
				}
//				drawable.setPress(false);
				if(mImageInfoList.size() > 1){
					if(mImageInfoList.size() <= 4){
						drawable.setQuality(BitmapUtil.IMG_QUALITY_2);
					}else{
						drawable.setQuality(BitmapUtil.IMG_QUALITY_3);
					}
				}else{
					drawable.setQuality(BitmapUtil.IMG_QUALITY_1);
				}
				
				if(mImageInfoList.size() == 3){
					if(i == 0){
						drawable.setBounds(left, top , left + itemWidth * 2, top + getWidth() - getPaddingTop() - getPaddingBottom());
						isHandBounds = true;
						drawable.setQuality(BitmapUtil.IMG_QUALITY_2);
					}else{
						left = getPaddingLeft() + itemWidth * 2 + mSpacing;
						top = getPaddingTop() + (itemHeight + mSpacing) * (i - 1);
					}
				}
				
				drawable.setImageUrl(mImageInfoList.get(i));

				if(!isHandBounds){
					drawable.setBounds(left, top , left + itemWidth, top + itemHeight);
				}
				if((i + 1) % column == 0){
					left = getPaddingLeft();
					top += itemHeight + mSpacing;
				}else{
					left += itemWidth + mSpacing;
				}
			}else{
				removeDrawable();
				i--;
			}
		}
		invalidate();
	}
	
	@Override
	protected void dispatchDraw(Canvas canvas) {
		super.dispatchDraw(canvas);
		for (AsyncImageDrawable drawable : mDrawableList) {
			drawable.draw(canvas);
		}
	}
	
	@Override
	protected boolean verifyDrawable(Drawable who) {
		if(who instanceof AsyncImageDrawable){
			return true;
		}
		return super.verifyDrawable(who);
	}
	
	private void removeDrawable(){
		if(mDrawableList.isEmpty()){
			return;
		}
		AsyncImageDrawable drawable = mDrawableList.remove(mDrawableList.size() - 1);
		if(drawable != null){
			drawable.release();
			if(mCacheDrawableList.size() + mDrawableList.size() < 9){
				mCacheDrawableList.add(drawable);
			}else{
				drawable.setCallback(null);
			}
			drawable.onDetachedFromWindow();
		}
	}
	
	private AsyncImageDrawable newDrawable(){
		AsyncImageDrawable drawable = null;
		if(!mCacheDrawableList.isEmpty()){
			drawable = mCacheDrawableList.removeFirst();
		}
		if(drawable == null){
			drawable = new AsyncImageDrawable();
			drawable.setCallback(this);
		}
		drawable.setNeedBuildThumbUrl(true);
		drawable.setDefaultImgRes(mDefaultImgRes);
		drawable.setQuality(mQuality);
		drawable.onAttachedToWindow();
		return drawable;
	}
	
	@Override
	protected void onDetachedFromWindow() {
		super.onDetachedFromWindow();
		for (;!mDrawableList.isEmpty();) {
			removeDrawable();
		}
	}
	/**
	 * @return the mHorizontalSpacing
	 */
	public int getSpacing() {
		return mSpacing;
	}
	/**
	 * @param mHorizontalSpacing the mHorizontalSpacing to set
	 */
	public void setSpacing(int horizontalSpacing) {
		if(mSpacing != horizontalSpacing){
			this.mSpacing = horizontalSpacing;
			requestLayout();
		}
	}
	
	public void setOnSudokuItemClickListener(OnSudokuItemClickListener l){
		mOnSudokuItemClickListener = l;
	}
	
	public interface OnSudokuItemClickListener {
		public void onSudokuItemClick(SudokuImageView view, int position);
	}
}
