package com.bk.android.time.ui.widget.binding;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

public class BFullImageView extends BAsyncImageView{
	private int mMaxHeight;
	public BFullImageView(Context context) {
		super(context);
	}
	
	public BFullImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	public void setMaxHeight(int maxHeight) {
		super.setMaxHeight(maxHeight);
		mMaxHeight = maxHeight;
    }
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec,heightMeasureSpec);
		Drawable drawable = getDrawable();
		if(drawable != null){
			int w = getMeasuredWidth() - getPaddingLeft() - getPaddingRight();
			int imgW = drawable.getIntrinsicWidth();
	        int imgH = drawable.getIntrinsicHeight();
	        int maxHeight = mMaxHeight;
	        if(imgW > 0){
	        	imgH = imgH * w / imgW;
				if(maxHeight > 0 && imgH > maxHeight) {
					imgH = maxHeight;
				}
				imgW = w;
				setMeasuredDimension(getMeasuredWidth(), imgH + getPaddingTop() + getPaddingBottom());
	        }
		}
	}
}
