package com.bk.android.time.ui.widget.binding;

import gueei.binding.BindingType;
import gueei.binding.IBindableView;
import gueei.binding.ViewAttribute;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.NumberPicker;
import android.widget.NumberPicker.OnValueChangeListener;

/**
 * @author mingkg21
 * @email mingkg21@gmail.com
 * @date 2015年2月5日
 *
 */
public class BNumberPicker extends NumberPicker implements IBindableView<BNumberPicker>,
		OnValueChangeListener {
	
	private GetValue mGetValue;

	public BNumberPicker(Context context, AttributeSet attrs) {
		super(context, attrs);
		setMaxValue(30);
		setMinValue(0);
		
		String[] displayedValues = new String[31]; 
		for(int i = 0; i < 31; i++) {
			displayedValues[i] = "" + i;
		}
		
		setDisplayedValues(displayedValues);
		setOnValueChangedListener(this);
	}
	
	@Override
	public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
		if(mGetValue != null){
			mGetValue.notifyChanged();
		}
	}
	
	@Override
	public ViewAttribute<? extends View, ?> createViewAttribute(
			String attributeId) {
		if (attributeId.equals("getValue")) {
			if(mGetValue == null){
				mGetValue = new GetValue(this, attributeId);
			}
			return mGetValue;
		}
		return null;
	}

	public static class GetValue extends ViewAttribute<BNumberPicker, Integer> {
		
		private boolean isInit;
		
		public GetValue(BNumberPicker view, String attributeId) {
			super(Integer.class, view, attributeId);
		}

		@Override
		public Integer get() {
			return getView().getValue();
		}

		@Override
		protected void doSetAttributeValue(Object newValue) {
			if(isInit) {
				return;
			}
			if(newValue instanceof Integer) {
				isInit = true;
				getView().setValue((Integer)newValue);
			}
		}

		@Override
		protected BindingType AcceptThisTypeAs(Class<?> type) {
			return BindingType.TwoWay;
		}
	}

}
