package com.bk.android.time.ui.widget.calendar;

import static java.util.Calendar.DATE;
import static java.util.Calendar.DAY_OF_MONTH;
import static java.util.Calendar.DAY_OF_WEEK;
import static java.util.Calendar.HOUR_OF_DAY;
import static java.util.Calendar.MILLISECOND;
import static java.util.Calendar.MINUTE;
import static java.util.Calendar.MONTH;
import static java.util.Calendar.SECOND;
import static java.util.Calendar.YEAR;

import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

public class CalendarDataProvider {
	private final List<List<List<MonthCellDescriptor>>> cells = new LinkedList<List<List<MonthCellDescriptor>>>();
	private final List<MonthDescriptor> months = new LinkedList<MonthDescriptor>();
	private final List<MonthCellDescriptor> selectedCells = new LinkedList<MonthCellDescriptor>();
	private final List<MonthCellDescriptor> highlightedCells = new LinkedList<MonthCellDescriptor>();
	private final List<Calendar> selectedCals = new LinkedList<Calendar>();
	private final List<Calendar> highlightedCals = new LinkedList<Calendar>();
	private Locale locale;
	private Calendar minCal;
	private Calendar maxCal;
	private Calendar monthCounter;
	private Calendar today;
	
	public CalendarDataProvider() {
		locale = Locale.getDefault();
		today = Calendar.getInstance(locale);
		minCal = Calendar.getInstance(locale);
		maxCal = Calendar.getInstance(locale);
		monthCounter = Calendar.getInstance(locale);
	}

	public void init(Date minDate, Date maxDate) {
		init(minDate, maxDate, Locale.getDefault(),false);
	}
	
	public void init(Date minDate, Date maxDate,boolean isOnlyMonth) {
		init(minDate, maxDate, Locale.getDefault(),isOnlyMonth);
	}

	public void init(Date minDate, Date maxDate, Locale locale,boolean isOnlyMonth) {
		if (minDate == null || maxDate == null) {
			throw new IllegalArgumentException(
					"minDate and maxDate must be non-null.  ");
		}
		if (minDate.after(maxDate)) {
			throw new IllegalArgumentException(
					"minDate must be before maxDate.  ");
		}
		if (minDate.getTime() == 0 || maxDate.getTime() == 0) {
			throw new IllegalArgumentException(
					"minDate and maxDate must be non-zero.  ");
		}
		if (locale == null) {
			throw new IllegalArgumentException("Locale is null.");
		}

		// Make sure that all calendar instances use the same locale.
		this.locale = locale;
		today = Calendar.getInstance(locale);
		minCal = Calendar.getInstance(locale);
		maxCal = Calendar.getInstance(locale);
		monthCounter = Calendar.getInstance(locale);

		// Clear out any previously-selected dates/cells.
		selectedCals.clear();
		selectedCells.clear();
		highlightedCells.clear();

		// Clear previous state.
		cells.clear();
		months.clear();
		minCal.setTime(minDate);
		maxCal.setTime(maxDate);
		setMidnight(minCal);
		setMidnight(maxCal);

		// maxDate is exclusive: bump back to the previous day so if maxDate is
		// the first of a month,
		// we don't accidentally include that month in the view.
		maxCal.add(MINUTE, -1);

		// Now iterate between minCal and maxCal and build up our list of months
		// to show.
		monthCounter.setTime(minCal.getTime());
		final int maxMonth = maxCal.get(MONTH);
		final int maxYear = maxCal.get(YEAR);
		while ((monthCounter.get(MONTH) <= maxMonth // Up to, including the
													// month.
				|| monthCounter.get(YEAR) < maxYear) // Up to the year.
				&& monthCounter.get(YEAR) < maxYear + 1) { // But not > next yr.
			Date date = monthCounter.getTime();
			MonthDescriptor month = new MonthDescriptor(
					monthCounter.get(MONTH), monthCounter.get(YEAR), date);
			if(!isOnlyMonth){
				cells.add(getMonthCells(month));
			}
			months.add(month);
			monthCounter.add(MONTH, 1);
		}
	}

	public List<List<List<MonthCellDescriptor>>> getData(){
		return cells;
	}
	
	public List<MonthDescriptor> getMonthsData(){
		return months;
	}
	
	private boolean isDateSelectable(Date date) {
		// TODO
		return false;
	}

	public List<List<MonthCellDescriptor>> getMonthCells(MonthDescriptor month) {
		Calendar cal = Calendar.getInstance(locale);
		cal.setTime(month.getDate());
		List<List<MonthCellDescriptor>> cells = new LinkedList<List<MonthCellDescriptor>>();
		cal.set(DAY_OF_MONTH, 1);
		int firstDayOfWeek = cal.get(DAY_OF_WEEK);
		int offset = cal.getFirstDayOfWeek() - firstDayOfWeek;
		if (offset > 0) {
			offset -= 7;
		}
		cal.add(Calendar.DATE, offset);

		Calendar minSelectedCal = minDate(selectedCals);
		Calendar maxSelectedCal = maxDate(selectedCals);

		while ((cal.get(MONTH) < month.getMonth() + 1 || cal.get(YEAR) < month
				.getYear()) //
				&& cal.get(YEAR) <= month.getYear()) {
			List<MonthCellDescriptor> weekCells = new LinkedList<MonthCellDescriptor>();
			cells.add(weekCells);
			for (int c = 0; c < 7; c++) {
				Date date = cal.getTime();
				boolean isCurrentMonth = cal.get(MONTH) == month.getMonth();
				boolean isSelected = isCurrentMonth
						&& containsDate(selectedCals, cal);
				boolean isSelectable = isCurrentMonth
						&& betweenDates(cal, minCal, maxCal)
						&& isDateSelectable(date);
				boolean isToday = sameDate(cal, today);
				boolean isHighlighted = containsDate(highlightedCals, cal);
				int value = cal.get(DAY_OF_MONTH);

				MonthCellDescriptor.RangeState rangeState = MonthCellDescriptor.RangeState.NONE;
				if (selectedCals.size() > 1) {
					if (sameDate(minSelectedCal, cal)) {
						rangeState = MonthCellDescriptor.RangeState.FIRST;
					} else if (sameDate(maxDate(selectedCals), cal)) {
						rangeState = MonthCellDescriptor.RangeState.LAST;
					} else if (betweenDates(cal, minSelectedCal, maxSelectedCal)) {
						rangeState = MonthCellDescriptor.RangeState.MIDDLE;
					}
				}

				weekCells.add(new MonthCellDescriptor(date, isCurrentMonth,
						isSelectable, isSelected, isToday, isHighlighted,
						value, rangeState));
				cal.add(DATE, 1);
			}
		}
		return cells;
	}

	private static void setMidnight(Calendar cal) {
		cal.set(HOUR_OF_DAY, 0);
		cal.set(MINUTE, 0);
		cal.set(SECOND, 0);
		cal.set(MILLISECOND, 0);
	}

	private static boolean containsDate(List<Calendar> selectedCals,
			Calendar cal) {
		for (Calendar selectedCal : selectedCals) {
			if (sameDate(cal, selectedCal)) {
				return true;
			}
		}
		return false;
	}

	private static Calendar minDate(List<Calendar> selectedCals) {
		if (selectedCals == null || selectedCals.size() == 0) {
			return null;
		}
		Collections.sort(selectedCals);
		return selectedCals.get(0);
	}

	private static Calendar maxDate(List<Calendar> selectedCals) {
		if (selectedCals == null || selectedCals.size() == 0) {
			return null;
		}
		Collections.sort(selectedCals);
		return selectedCals.get(selectedCals.size() - 1);
	}

	private static boolean sameDate(Calendar cal, Calendar selectedDate) {
		return cal.get(MONTH) == selectedDate.get(MONTH)
				&& cal.get(YEAR) == selectedDate.get(YEAR)
				&& cal.get(DAY_OF_MONTH) == selectedDate.get(DAY_OF_MONTH);
	}

	private static boolean betweenDates(Calendar cal, Calendar minCal,
			Calendar maxCal) {
		final Date date = cal.getTime();
		return betweenDates(date, minCal, maxCal);
	}

	static boolean betweenDates(Date date, Calendar minCal, Calendar maxCal) {
		final Date min = minCal.getTime();
		return (date.equals(min) || date.after(min)) // >= minCal
				&& date.before(maxCal.getTime()); // && < maxCal
	}

	static boolean sameMonth(Calendar cal, MonthDescriptor month) {
		return (cal.get(MONTH) == month.getMonth() && cal.get(YEAR) == month
				.getYear());
	}
}
