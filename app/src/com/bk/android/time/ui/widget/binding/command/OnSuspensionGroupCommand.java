package com.bk.android.time.ui.widget.binding.command;

import android.view.View;

import com.bk.android.binding.command.BaseCommand;
import com.bk.android.ui.widget.FacilityExpandableListView;

public abstract class OnSuspensionGroupCommand extends BaseCommand {
	@Override
	protected void onInvoke(View view, Object... args) {
		if(args == null || args.length < 2
				|| !(view instanceof FacilityExpandableListView)
				|| !(args[0] instanceof Integer)
				|| !(args[1] instanceof Integer)){
			return;
		}
		onSuspensionGroupChange((Integer) args[0],(Integer) args[1]);
	}

	public abstract void onSuspensionGroupChange(int newPosition,int oldPosition);
}
