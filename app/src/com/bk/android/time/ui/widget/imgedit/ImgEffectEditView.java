package com.bk.android.time.ui.widget.imgedit;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;

import com.bk.android.time.ui.widget.AsyncImageView;

/** 图片效果编辑
 * @author mingkg21
 * @email mingkg21@gmail.com
 * @date 2015年4月2日
 *
 */
public class ImgEffectEditView extends AsyncImageView {
	
	private static final int TOUCH_ORIENTATION_NONE = 0;
	private static final int TOUCH_ORIENTATION_VERTICAL = 1;
	private static final int TOUCH_ORIENTATION_HORIZONTAL = 2;
	
	private ImgLayout mImgLayout;
	private int mOffx;
	private int mOffy;
	private int mDrawableWidth;
	private int mDrawableHight;
	private int mDrawableVisiableWidth;
	private int mDrawableVisiableHight;
	private int mTouchOrientation = TOUCH_ORIENTATION_VERTICAL;
	private float mTouchDownX;
	private float mTouchDownY;
	private int mMoveOffX;
	private int mMoveOffY;
	private boolean mInit;
	
	private int mSetOffx;
	private int mSetOffy;

	public ImgEffectEditView(Context context, AttributeSet attrs) {
		super(context, attrs);
		mImgLayout = new ImgLayout();
		setFinishPreAnimation(null);
		setFinishPostAnimation(null);
//		mImgLayout.addDrawItem(new DrawTextItem(50, 20, "AAAAA", Color.BLACK));
//		mImgLayout.addDrawItem(new DrawTextItem(50, 70, "BBBBB", Color.RED));
//		mImgLayout.addDrawItem(new DrawTextItem(50, 120, "CCCCC", Color.BLUE));
//		mImgLayout.addDrawItem(new DrawTextItem(50, 170, "DDDDD", Color.CYAN));
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
//		super.onDraw(canvas);
		
		int w = getMeasuredWidth() - getPaddingLeft() - getPaddingRight();
		int h = getMeasuredHeight() - getPaddingTop() - getPaddingBottom();
		Paint paint = new Paint();
		paint.setColor(Color.WHITE);
		canvas.drawRect(getPaddingLeft(), getPaddingTop(), w + getPaddingLeft(), h + getPaddingTop(), paint);
		
		mDrawableVisiableWidth = w;
		mDrawableVisiableHight = h;

		canvas.save();
		Drawable drawable = getDrawable();
		if(drawable != null){
			int x = getPaddingLeft();
			int y = getPaddingTop();
			int imgW = drawable.getIntrinsicWidth();
	        int imgH = drawable.getIntrinsicHeight();
	        
	        if(!mInit) {
	        	mInit = true;
	        	mOffx = mSetOffx;
	        	mOffy = mSetOffy;
	        	mMoveOffY = mOffy;
				mMoveOffX = mOffx;
	        	if(imgW > imgH) {
	        		mTouchOrientation = TOUCH_ORIENTATION_HORIZONTAL;
	        	} else {
	        		mTouchOrientation = TOUCH_ORIENTATION_VERTICAL;
	        	}
	        }
	        
	        Rect bounds = new Rect();
	        
	        int vWidth = w;
    		int vHeight = h;
			int gapW = imgH * (imgW - vWidth) / imgW;
			int gapH = imgH - vHeight;
    		if(gapW <= gapH){
    			imgH = imgH * vWidth / imgW;
    			imgW = vWidth;
    			bounds.left = x;
    	        bounds.top = y - (imgH - vHeight) / 2;
    		}else{
    			imgW = imgW * vHeight / imgH;
    			imgH = vHeight;
    			bounds.left = x  - (imgW - vWidth) / 2;
    	        bounds.top = y;
    		}
    		bounds.left = x;
    		bounds.top = y;
    		bounds.left = bounds.left + mOffx;
    		bounds.top = bounds.top + mOffy;
    		bounds.right =  bounds.left + imgW; 
    		bounds.bottom = bounds.top + imgH;
    		
    		mDrawableWidth = imgW;
    		mDrawableHight = imgH;
    		
    		Rect rect = new Rect(x, y, x + w, y + h);
    		canvas.clipRect(rect);
	        drawable.setBounds(bounds);
	        drawable.draw(canvas);
	        
		}
		drawProgress(canvas, getWidth(), getHeight());
		canvas.restore();
		
		if(mImgLayout != null) {
			mImgLayout.onDraw(canvas);
		}
	}
	
	@Override
	public void setImageUrl(String url, int width, int height) {
		super.setImageUrl(url, width, height);
		mInit = false;
	}
	
	public void setMoveXY(int x, int y) {
		mInit = false;
		mSetOffx = x;
		mSetOffy = y;
		invalidate();
	}
	
	public int getMoveX() {
		return mOffx;
	}
	
	public int getMoveY() {
		return mOffy;
	}
	
	public boolean hadMove() {
		if(mOffx != 0 || mOffy != 0) {
			return true;
		}
		return false;
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
//		if(mImgLayout != null) {
//			return mImgLayout.onTouchEvent(event);
//		}
		
		if(MotionEvent.ACTION_MOVE == event.getAction()) {
			if(mTouchOrientation == TOUCH_ORIENTATION_VERTICAL) {
				if((mOffy + mDrawableVisiableHight) <= mDrawableHight) {
					mOffy = (int) (mMoveOffY + (event.getY() - mTouchDownY));
//					if((mOffy + mDrawableVisiableHight) > mDrawableHight) {
//						mOffy = mDrawableHight - mDrawableVisiableHight;
//					}
					int min = (mDrawableVisiableHight - mDrawableHight);
					if(mOffy > 0) {
						mOffy = 0;
					}
					if(mOffy < min) {
						mOffy = min;
					}
					invalidate();
				}
			} else if(mTouchOrientation == TOUCH_ORIENTATION_HORIZONTAL){
				if((mOffx + mDrawableVisiableWidth) <= mDrawableWidth) {
					mOffx = (int) (mMoveOffX + (event.getX() - mTouchDownX));
//					if((mOffy + mDrawableVisiableHight) > mDrawableHight) {
//						mOffy = mDrawableHight - mDrawableVisiableHight;
//					}
					int min = (mDrawableVisiableWidth - mDrawableWidth);
					if(mOffx > 0) {
						mOffx = 0;
					}
					if(mOffx < min) {
						mOffx = min;
					}
					invalidate();
				}
			}
			return true;
		} else if(MotionEvent.ACTION_DOWN == event.getAction()) {
			mTouchDownX = event.getX();
			mTouchDownY = event.getY();
			mMoveOffY = mOffy;
			mMoveOffX = mOffx;
			return true;
		}
		
		return super.onTouchEvent(event);
	}
	
//	@Override
//	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//		super.onMeasure(widthMeasureSpec,heightMeasureSpec);
//		int w = getMeasuredWidth() - getPaddingLeft() - getPaddingRight();
//		setMeasuredDimension(getMeasuredWidth(), (4 * w / 3)+ getPaddingTop() + getPaddingBottom());
//	}
	
	public class ImgLayout extends DrawItem {
		
		private ArrayList<DrawItem> mChild = new ArrayList<ImgEffectEditView.DrawItem>();
		
		private DrawItem mDragItem;
		private int mLayoutGravity;
		
		public void addDrawItem(DrawItem item) {
			if(item == null) {
				return;
			}
			mChild.add(item);
		}
		
		public void onDraw(Canvas canvas) {
			for(DrawItem imgItem : mChild) {
				imgItem.onDraw(canvas);
			}
		}
		
		public void onMeasure() {
			int width = 0;
			int height = 0;
			//计算子控件的宽高
			for(DrawItem item : mChild) {
				item.onMeasure();
				width = item.getLeft() + item.getWidth();
				height = item.getTop() + item.getHeight();
				if(width > mWidth) {
					mWidth = width;
				}
				if(height > mHeight) {
					mHeight = height;
				}
			}
		}
		
		public boolean onTouchEvent(MotionEvent event) {
			
			switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN:
				
				break;
			case MotionEvent.ACTION_MOVE:
				if(mDragItem != null) {
					
				}
				break;
			case MotionEvent.ACTION_UP:
				mDragItem = null;
				break;

			default:
				break;
			}
			return true;
		}

	}
	
	public class DrawItem {
		
		public static final int MATCH_PARENT = -1;
		
		protected int mLeft;
		protected int mTop;
		protected int mWidth;
		protected int mHeight;
		protected Paint mPaint = new Paint();
		
		protected boolean canDrag;
		
		public boolean isCanDrag() {
			return canDrag;
		}
		
		public int getLeft() {
			return mLeft;
		}
		
		public int getTop() {
			return mTop;
		}
		
		public int getWidth() {
			return mWidth;
		}
		
		public int getHeight() {
			return mHeight;
		}
		
		public void onDraw(Canvas canvas) {
			
		}
		
		protected void onMeasure() {
			
		}
		
	}
	
	public class DrawTextItem extends DrawItem {
		
		private String mContent;
		private int mContentColor;
		
		public DrawTextItem(int x, int y, String content, int contentColor) {
			this.mLeft = x;
			this.mTop = y;
			mContent = content;
			mContentColor = contentColor;
			mPaint.setColor(contentColor);
			mPaint.setTextSize(50);
		}
		
		@Override
		public void onDraw(Canvas canvas) {
			super.onDraw(canvas);
			canvas.drawText(mContent, mLeft, mTop, mPaint);
		}
		
	}

}
