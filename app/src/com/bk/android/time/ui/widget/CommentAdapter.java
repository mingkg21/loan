package com.bk.android.time.ui.widget;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bk.android.assistant.R;
import com.bk.android.time.entity.Comment;
import com.bk.android.util.FormatUtil;

import java.util.ArrayList;

/**
 * Created by mingkg21 on 2016/12/22.
 */

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.ViewHolder>  {

    private Context mContext;
    private LayoutInflater mInflater;

    private ArrayList<Comment> mDatas;

    public CommentAdapter(Context context, ArrayList<Comment> datas) {
        mContext = context;
        mInflater = LayoutInflater.from(mContext);

        mDatas = datas;
    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(mInflater.inflate(R.layout.uniq_comment_item_lay, null));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bindView(mDatas.get(position));
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private QuickImageView mImageView;
        private TextView mNameTV;
        private TextView mFloorTV;
        private TextView mContentTV;
        private TextView mTimeTV;

        public ViewHolder(View itemView) {
            super(itemView);

            mImageView = (QuickImageView) itemView.findViewById(R.id.icon_qiv);
            mImageView.setCircle(true);
            mNameTV = (TextView) itemView.findViewById(R.id.name_tv);
            mFloorTV = (TextView) itemView.findViewById(R.id.floor_tv);
            mContentTV = (TextView) itemView.findViewById(R.id.content_iv);
            mTimeTV = (TextView) itemView.findViewById(R.id.time_tv);
        }

        public void bindView(Comment comment) {
            mImageView.setImageUrl(comment.getUserIcon());
            mNameTV.setText(comment.getUserName());
            mFloorTV.setText(comment.getFloor() + "楼");
            mContentTV.setText(comment.getComment());
            mTimeTV.setText(FormatUtil.getDayTime(comment.getSubmitDate()));
        }
    }
}
