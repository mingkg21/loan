package com.bk.android.time.ui.widget.imgedit;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

import com.bk.android.time.ui.widget.AsyncImageViewManager.InteriorDrawable;
import com.bk.android.time.ui.widget.AsyncPhotoView;
import com.bk.android.time.ui.widget.imgedit.ImgCropAttacher.CropCallback;

public class ImgEditView extends AsyncPhotoView implements IImgView,CropCallback{
	private float mInitCropScale = 0.95f;
	private float mInitScale = 1f;
	private float mMinCropScale = 1f;
	private float mMaxCropScale = 3f;
	private ImgCropAttacher mImgCropAttacher;
	private Rect mTempViewBoundary;
	private Rect mTempImgBoundary;
	private boolean isCropInitFinish = true;
	private boolean mRequestCrop;
	private boolean mRequestCropScaleChange;
	private boolean isCropInited;
	private float mLastScale = -1;

	public ImgEditView(Context context, AttributeSet attrs) {
		super(context, attrs);
		mImgCropAttacher = new ImgCropAttacher(this, this);
	}

	public boolean confirmCroping() {
		if(!hasLoadImg()){
			return false;
		}
		//TODO 分辨下降
		if(mImgCropAttacher.isCroping()){
			Bitmap bitmap = mImgCropAttacher.confirmCrop();
			if(bitmap != null){
				setImageBitmap(bitmap);
			}
		}
		return mImgCropAttacher.isCroping();
	}

	public boolean setReset() {
		if(!hasLoadImg()){
			return false;
		}
		if(isCropInitFinish){
			RectF displayRect = getDisplayRect();
			setScale(getMinimumScale(), displayRect.width() / 2 , displayRect.height() / 2, true);
		}
		return isCropInitFinish;
	}
	
	public boolean switchRotate() {
		if(!hasLoadImg()){
			return false;
		}
		Drawable drawable = getDrawable();
		int w = drawable.getIntrinsicHeight();
		int h = drawable.getIntrinsicWidth();
		int left = Math.abs(w - drawable.getIntrinsicWidth()) / 2;
		int top = Math.abs(h - drawable.getIntrinsicHeight()) / 2;
		Bitmap bitmap = Bitmap.createBitmap(w,h, Config.ARGB_4444);
		Canvas canvas = new Canvas(bitmap);
		if(w > h){
			canvas.translate(left, -top);
		}else{
			canvas.translate(-left, top);
		}
		canvas.rotate(90,drawable.getIntrinsicWidth() / 2,drawable.getIntrinsicHeight() / 2);
		drawable.setBounds(0, 0,drawable.getIntrinsicWidth(),drawable.getIntrinsicHeight());
		drawable.draw(canvas);
		setImageBitmap(bitmap);
		return true;
	}
	
	public boolean isCroping(){
		return mImgCropAttacher.isCroping();
	}
	
	public boolean setCroping(boolean croping){
		mImgCropAttacher.setCroping(croping);
		return true;
	}
	
	public boolean setCropScale(int wScale,int hScale){
		mImgCropAttacher.setCropScale(wScale, hScale);
		return true;
	}
	
	public Bitmap confirmBitmap(boolean isForce){
		if(!hasLoadImg()){
			return null;
		}
		setCroping(false);
		update();
		Rect imgRect = getImgBoundary();
		float densityScaled = 1;
		if(isForce){
			long imgSize = imgRect.width() * imgRect.height();
	        float targetSize = 500 * 500;
	        if(imgSize > targetSize){
	        	densityScaled = (float) Math.sqrt(targetSize / imgSize);
	        }
		}
		Bitmap bitmap = Bitmap.createBitmap((int)(imgRect.width() * densityScaled),(int)(imgRect.height() * densityScaled), Config.RGB_565);
		Canvas canvas = new Canvas(bitmap);
		canvas.translate(-imgRect.left, -imgRect.top);
		canvas.clipRect(imgRect);
		if(densityScaled != 1){
			canvas.scale(densityScaled,densityScaled, imgRect.left, imgRect.top);
		}
		draw(canvas);
		return bitmap;
	}
	
	private void checkCropInitFinish(){
		if(!isCropInitFinish){
			Float scale = null;
			Float minCropScale = null;
			if(mImgCropAttacher.isCroping()){
				if(getScale() == mInitCropScale){
					isCropInitFinish = true;
					Rect rect = new Rect();
					Rect viewRect = new Rect(getLeft(),getTop(),getRight(),getBottom());
					boolean isHorizontal = mImgCropAttacher.initCropPatternLocation(getDisplayRect(),rect,viewRect);
					setPadding(rect.left,rect.top,getWidth() - rect.right,getHeight() - rect.bottom);
					update();
					RectF displayRect = getDisplayRect();
					if(isHorizontal){
						scale = minCropScale = rect.width() * 1f / displayRect.width();
					}else{
						scale = minCropScale = rect.height() * 1f / displayRect.height();
					}
				}
			}else{
				if(getScale() == mInitScale){
					isCropInitFinish = true;
				}
			}
			if(isCropInitFinish){
				if(minCropScale == null){
					minCropScale = mMinCropScale;
				}
				setScaleBoundary(mMaxCropScale, mMaxCropScale - (mMaxCropScale - minCropScale) / 2, minCropScale);
				if(scale != null){
					RectF displayRect = getDisplayRect();
					setScale(scale, displayRect.width() / 2 , displayRect.height() / 2, false);
				}
			}
		}
	}
	
	
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		try {
			if(hasLoadImg()){
				if(!isCropInited || !checkScaleChange()){
					onCropInit(mRequestCrop, mRequestCropScaleChange);
				}else{
					checkCropInitFinish();
					mImgCropAttacher.drawCropContent(canvas);
					if(!isCropInitFinish){
						invalidate();
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public float getScale() {
		mLastScale = super.getScale();
		return mLastScale;
	}
	
	private boolean checkScaleChange(){
		boolean isChange = true;
		if(!isCropInitFinish && mLastScale == super.getScale()){
			isChange = false;
		}
		return isChange;
	}
	
	@Override
	public void onCropInit(boolean isCrop,boolean isCropScaleChange) {
		mRequestCrop = isCrop;
		mRequestCropScaleChange = isCropScaleChange;
		isCropInited = false;
		mLastScale = -1;
		if(hasLoadImg()){
			setPadding(0, 0, 0, 0);
			update();
			setScaleBoundary(mMaxCropScale, mMaxCropScale - (mMaxCropScale - mInitCropScale) / 2, mInitCropScale);
			if(isCrop){
				setScale(mInitCropScale, !isCropScaleChange);
			}else{
				setScale(mInitCropScale);
				setScale(mInitScale, true);
			}
			isCropInitFinish = false;
			isCropInited = true;
		}
	}

	@Override
	public boolean hasLoadImg() {
		if(getDrawable() instanceof InteriorDrawable){
			return super.hasLoadImg() && ((InteriorDrawable) getDrawable()).isDrawable();
		}
		return super.hasLoadImg();
	}
	
	@Override
	public boolean isCropInitFinish() {
		return isCropInitFinish;
	}

	@Override
	public Rect getViewBoundary() {
		if(mTempViewBoundary == null){
			mTempViewBoundary = new Rect();
		}
		mTempViewBoundary.set(getLeft(), getTop(), getRight(), getBottom());
		return mTempViewBoundary;
	}

	@Override
	public Rect getImgBoundary() {
		if(mTempImgBoundary == null){
			mTempImgBoundary = new Rect();
		}
		RectF rectF = getDisplayRect();
		mTempImgBoundary.set((int)rectF.left + getPaddingLeft(),(int)rectF.top + getPaddingTop(),(int)rectF.right + getPaddingLeft(),(int)rectF.bottom + getPaddingTop());
		return mTempImgBoundary;
	}

	@Override
	public void drawImgContent(Canvas canvas) {
		draw(canvas);
	}
}
