package com.bk.android.time.ui.widget.binding;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;

import gueei.binding.IBindableView;
import gueei.binding.ViewAttribute;

public class BEditText extends EditText implements IBindableView<BEditText>{

	public BEditText(Context context) {
		super(context);
	}

	public BEditText(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	@Override
	public ViewAttribute<? extends View, ?> createViewAttribute(
			String attributeId) {
		if (attributeId.equals("getContent")) {
			return new GetContent(this,attributeId);
		}
		return null;
	}
	
	public static class GetContent extends ViewAttribute<BEditText, String>{

		public GetContent(BEditText view, String attributeId){
			super(String.class, view, attributeId);
		}
		
		@Override
		public String get() {
			return getView().getText().toString();
		}

		@Override
		protected void doSetAttributeValue(Object newValue) {

		}
	}
	
}
