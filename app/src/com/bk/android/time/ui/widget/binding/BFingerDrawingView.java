package com.bk.android.time.ui.widget.binding;

import gueei.binding.BindingType;
import gueei.binding.IBindableView;
import gueei.binding.ViewAttribute;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import com.bk.android.ui.widget.FingerDrawingView;

public class BFingerDrawingView extends FingerDrawingView  implements IBindableView<BFingerDrawingView>{
	public BFingerDrawingView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	public ViewAttribute<? extends View, ?> createViewAttribute(
			String attributeId) {
		if (attributeId.equals("paintSize")) {
			return new PaintSize(this,attributeId);
		}else if(attributeId.equals("paintColor")){
			return new PaintColor(this,attributeId);
		}else if(attributeId.equals("eraser")){
			return new Eraser(this,attributeId);
		}else if(attributeId.equals("clear")){
			return new Clear(this,attributeId);
		}
		return null;
	}
	
	public static class Clear extends ViewAttribute<BFingerDrawingView, Boolean>{
		public Clear(BFingerDrawingView view,String attributeId){
			super(Boolean.class, view, attributeId);
		}
		
		@Override
		public Boolean get() {
			return null;
		}

		@Override
		protected void doSetAttributeValue(Object newValue) {
			if (newValue instanceof Boolean && (Boolean)newValue) {
				getView().clear();
			}
		}
	}
	
	public static class Eraser extends ViewAttribute<BFingerDrawingView, Boolean>{
		public Eraser(BFingerDrawingView view,String attributeId){
			super(Boolean.class, view, attributeId);
		}
		
		@Override
		public Boolean get() {
			return getView().isEraser();
		}

		@Override
		protected void doSetAttributeValue(Object newValue) {
			if (newValue instanceof Boolean) {
				getView().setEraser((Boolean) newValue);
			}
		}
		
		@Override
		protected BindingType AcceptThisTypeAs(Class<?> type) {
			return BindingType.TwoWay;
		}
	}
	
	
	public static class PaintColor extends ViewAttribute<BFingerDrawingView, Integer>{
		public PaintColor(BFingerDrawingView view,String attributeId){
			super(Integer.class, view, attributeId);
		}
		
		@Override
		public Integer get() {
			return null;
		}

		@Override
		protected void doSetAttributeValue(Object newValue) {
			if (newValue instanceof Integer) {
				getView().setPaintColor((Integer) newValue);
			}
		}
	}
	
	public static class PaintSize extends ViewAttribute<BFingerDrawingView, Integer>{
		public PaintSize(BFingerDrawingView view,String attributeId){
			super(Integer.class, view, attributeId);
		}
		
		@Override
		public Integer get() {
			return null;
		}

		@Override
		protected void doSetAttributeValue(Object newValue) {
			if (newValue instanceof Integer) {
				getView().setPaintSize((Integer) newValue);
			}
		}
	}
}
