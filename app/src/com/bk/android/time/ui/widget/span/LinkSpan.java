package com.bk.android.time.ui.widget.span;

import android.os.Parcel;
import android.text.ParcelableSpan;
import android.text.TextUtils;
import android.text.style.ClickableSpan;
import android.view.View;

public class LinkSpan extends ClickableSpan implements ParcelableSpan {
    private final String mData;

    public LinkSpan(String data) {
        mData = data;
    }

    public LinkSpan(Parcel src) {
        mData = src.readString();
    }
    
    public int getSpanTypeId() {
        return 0;
    }
    
    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mData);
    }

    public String getURL() {
        return mData;
    }

    @Override
    public void onClick(View widget) {
    	if(!TextUtils.isEmpty(mData)){
//    		XGPushActivity.openActivity(App.getInstance(), mData);
    	}
    }
}
