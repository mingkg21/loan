package com.bk.android.time.ui.widget.binding;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import com.bk.android.time.ui.widget.SecurityCodeButton;

import gueei.binding.IBindableView;
import gueei.binding.ViewAttribute;

public class BSecurityCodeButton extends SecurityCodeButton implements IBindableView<BSecurityCodeButton>{
	public BSecurityCodeButton(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	@Override
	public ViewAttribute<? extends View, ?> createViewAttribute(
			String attributeId) {
		if (attributeId.equals("phone")) {//设置对于的属性值
			return new PhoneAttribute(this, attributeId);
		}
		return null;
	}
	
	public static class PhoneAttribute extends ViewAttribute<BSecurityCodeButton, String>{

		public PhoneAttribute(BSecurityCodeButton view, String attributeId){
			super(String.class, view, attributeId);
		}
		
		@Override
		public String get() {//获取值
			return null;
		}

		@Override
		protected void doSetAttributeValue(Object newValue) {//设置值
			if (newValue instanceof String) {
				getView().setPhone((String) newValue);
			}else{
				getView().setPhone("");
			}
		}
	}
	
}
