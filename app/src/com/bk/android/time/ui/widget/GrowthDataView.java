package com.bk.android.time.ui.widget;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.FontMetrics;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;

/** 身高体重的统计
 * @author mingkg21
 * @email mingkg21@gmail.com
 * @date 2015年1月20日
 *
 */
public class GrowthDataView extends View {
	
	private static final String STATE_PARENT = "parent";

	private Paint mScoreBgPaint;
	private Paint mScoreFgPaint;
	private Paint mCoordinatePaint;
	private Paint mWordPaint;
	
	private int mWidth;
	private int mHeight;
	private float fontHeight;
	private float fontWidth;
	private float halfFontHeight;
	
	private float weightFontWidth;
	private float weightFontHeight;
	
	private ArrayList<DrawRect> mDrawRects;
	
	public GrowthDataView(Context context) {
		super(context);
		init(null, 0);
	}

	public GrowthDataView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(attrs, 0);
	}

	public GrowthDataView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(attrs, defStyle);
	}

	private void init(AttributeSet attrs, int defStyle) {

		mWidth = 0;
		mHeight = 0;

		mScoreBgPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		mScoreBgPaint.setColor(Color.parseColor("#7f7f7f"));
		mScoreBgPaint.setStyle(Paint.Style.FILL);
		mScoreBgPaint.setTextAlign(Align.CENTER);
		
		mWordPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		mWordPaint.setColor(Color.parseColor("#7f7f7f"));
		mWordPaint.setTextAlign(Align.RIGHT);
		mWordPaint.setTextSize(14);
		
		FontMetrics fm = mWordPaint.getFontMetrics();  
		fontHeight = (float) Math.ceil(fm.descent - fm.top) + 2;  
		fontWidth = mWordPaint.measureText("24h");
		halfFontHeight = fontHeight / 2.0f;
		weightFontWidth = mWordPaint.measureText("25.0");
		
		mScoreFgPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		mScoreFgPaint.setColor(Color.parseColor("#2e2d33"));
		mScoreFgPaint.setStyle(Paint.Style.FILL);
		
		mCoordinatePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		mCoordinatePaint.setColor(Color.parseColor("#2e2d33"));
		mCoordinatePaint.setStyle(Paint.Style.FILL);
		mCoordinatePaint.setStrokeWidth(2);

	}

	@Override
	protected void onDraw(Canvas canvas) {
		float offX = fontWidth / 2;
		float offHeight = (float) (1.5 * fontHeight);
		float width = getWidth() - getPaddingLeft() - getPaddingRight() - offX * 2;
		float height = getHeight() - getPaddingTop() - getPaddingBottom() - offHeight;
		float offW = width / 12.0f;
		float offH = 0;
		
		float x = getPaddingLeft();
		float xStart = getPaddingLeft() + offX;
		float yStart = getPaddingTop() + offHeight;
		float xEnd = 0;
		float yEnd = height + yStart;
		
		if(mDrawRects != null) {
			int totalMins = 24 * 60;
			float x1 = 0;
			float x2 = 0;
			for(DrawRect drawRect : mDrawRects) {
				x1 = xStart + width * drawRect.startMin / totalMins;
				x2 = xStart + width * drawRect.endMin / totalMins;
				canvas.drawRect(x1, yStart, x2, yEnd, mScoreFgPaint);
			}
		}
		
//		for(int i = 0; i <= 12; i++) {
//			x = i * offW + xStart;
//			if((i % 3) == 0) {
//				canvas.drawText("" + i * 2 + "h", x, getPaddingTop() + fontHeight, mScoreBgPaint);
//			}
//			canvas.drawLine(x, yStart, x, yEnd, mScoreBgPaint);
//		}
		
		//y轴坐标
		float posYStartX = weightFontWidth * 1.3f; 
		float posYStartY = 0; 
		float posYEndX = posYStartX; 
		float posYEndY = height; 
		canvas.drawLine(posYStartX, posYStartY, posYEndX, posYEndY, mCoordinatePaint);
		//x轴坐标
		float posXStartX = posYStartX; 
		float posXStartY = posYEndY; 
		float posXEndX = width; 
		float posXEndY = posXStartY; 
		canvas.drawLine(posXStartX, posXStartY, posXEndX, posXEndY, mCoordinatePaint);
		//
		float startX = 0;
		float startY = 0;
		float endX = 0;
		float endY = 0;
		
		offW = posXEndX / 11.0f;
		offH = posYEndY / 25.0f;
		
		startY = posYStartY;
		endY = posYEndY;
		mWordPaint.setTextAlign(Align.CENTER);
		for (int i = 0; i <= 10; i++) {
			startX = i * offW + posYStartX;
			endX = startX;
			if(i != 0) {
				canvas.drawLine(startX, startY, endX, endY, mScoreBgPaint);
			}
			canvas.drawText("" + i + "", startX, endY + fontHeight, mWordPaint);
		}
		
		startX = posXStartX;
		endX = posXEndX;
		mWordPaint.setTextAlign(Align.RIGHT);
		for (int i = 1; i <= 25; i++) {
			startY = posYEndY - i * offH;
			endY = startY;
			canvas.drawLine(startX, startY, endX, endY, mScoreBgPaint);
			if(i % 5 == 0) {
				canvas.drawText("" + i + ".0", startX - weightFontWidth * 0.3f, startY + halfFontHeight, mWordPaint);
			}
		}
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int widthMode = MeasureSpec.getMode(widthMeasureSpec);
		int widthSize = MeasureSpec.getSize(widthMeasureSpec);
		int heightMode = MeasureSpec.getMode(heightMeasureSpec);
		int heightSize = MeasureSpec.getSize(heightMeasureSpec);

		int width;
		int height;

		if (widthMode == MeasureSpec.EXACTLY) {
			width = widthSize;
		} else if (widthMode == MeasureSpec.AT_MOST) {
			width = Math.min(mWidth, widthSize);
		} else {
			width = mWidth;
		}

		if (heightMode == MeasureSpec.EXACTLY) {
			height = heightSize;
		} else if (heightMode == MeasureSpec.AT_MOST) {
			height = Math.min(mHeight, heightSize);
		} else {
			height = mHeight;
		}

		int min = Math.min(mWidth, mHeight);
		setMeasuredDimension(widthSize, heightSize);
	}

	@Override
	protected Parcelable onSaveInstanceState() {
		Parcelable superState = super.onSaveInstanceState();

		Bundle state = new Bundle();
		state.putParcelable(STATE_PARENT, superState);

		return state;
	}

	@Override
	protected void onRestoreInstanceState(Parcelable state) {
		Bundle savedState = (Bundle) state;

		Parcelable superState = savedState.getParcelable(STATE_PARENT);
		super.onRestoreInstanceState(superState);
	}
	
	public void setDrawRects(ArrayList<DrawRect> drawRects) {
		mDrawRects = drawRects;
		invalidate();
	}

	
	public static class DrawRect {
		public int startMin;
		public int endMin;
	}
	
}
