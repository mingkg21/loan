package com.bk.android.time.ui.widget;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ListAdapter;

import com.bk.android.ui.widget.DecoratorListAdapter;

public class FullGridView extends GridView{
	private int mHorizontalSpacing;
	private int mColumnWidth;
	private int mNumColumns;
	private InteriorAdapter mInteriorAdapter;

	public FullGridView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}
	
	private void init(){
		mInteriorAdapter = new InteriorAdapter(null);
		super.setAdapter(mInteriorAdapter);
	}

	@Override
	public void setAdapter(ListAdapter adapter) {
		ListAdapter oldAdapter = mInteriorAdapter.getAdapter();
		mInteriorAdapter.setAdapter(adapter);
		if(oldAdapter == null || adapter == null || !oldAdapter.getClass().equals(adapter.getClass())){
			super.setAdapter(mInteriorAdapter);
		}
	}
	
	@Override
	public void setHorizontalSpacing(int horizontalSpacing) {
		mHorizontalSpacing = horizontalSpacing; 
		super.setHorizontalSpacing(horizontalSpacing);
	}

	@Override
	public void setNumColumns(int numColumns) {
		mNumColumns = numColumns;
		super.setNumColumns(numColumns);
	}
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		int w = getMeasuredWidth() - getPaddingLeft() - getPaddingRight();
		if(w > 0){
			Rect padding = new Rect();
			if(getSelector() != null){
				getSelector().getPadding(padding);
			}
			int tempW = w - mHorizontalSpacing * (mNumColumns - 1) - padding.left - padding.right;
			mColumnWidth = tempW / mNumColumns;
		}
	}
	
	private class InteriorAdapter extends DecoratorListAdapter{
		public InteriorAdapter(ListAdapter adapter) {
			super(adapter);
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			convertView = super.getView(position, convertView, parent);
			if(convertView != null){
				LayoutParams lp = (LayoutParams) convertView.getLayoutParams();
				int w = mColumnWidth;
				if(lp == null){
					lp = new LayoutParams(w,w);
					convertView.setLayoutParams(lp);
				}else{
					lp.width = w;
					lp.height = w;
				}
			}
			return convertView;
		}
	}
}
