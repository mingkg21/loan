package com.bk.android.time.ui.widget;

import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;

public class FaceLinearLayout extends LinearLayout{
	private int mInitActivityHeight;
	private boolean isShowSoftInput;
	private boolean isSelfShow;
	private InputMethodManager mInputMethodManager;
	private View mDecorView;
	private ViewGroup mRootView;
	private OnGlobalLayoutListener mOnGlobalLayoutListener;

	public FaceLinearLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		mInputMethodManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
		if(getContext() instanceof Activity){
			mDecorView = ((Activity) getContext()).getWindow().getDecorView();
			mRootView = (ViewGroup) ((Activity) getContext()).findViewById(android.R.id.content);
		}
	}
	
	@Override
	protected void onAttachedToWindow() {
		super.onAttachedToWindow();
		if(mRootView != null){
			if(mOnGlobalLayoutListener == null){
				mOnGlobalLayoutListener = new OnGlobalLayoutListener() {
					@Override
					public void onGlobalLayout() {
						post(new Runnable() {
							@Override
							public void run() {
								if(mInitActivityHeight == 0){
									mInitActivityHeight = getContentHeight();
								}
								Rect frame = new Rect();  
								mDecorView.getWindowVisibleDisplayFrame(frame); 
								boolean isShow = mInitActivityHeight > getContentHeight() + frame.top;
								if(isShowSoftInput != isShow){
									isShowSoftInput = isShow;
									if(!isSelfShow){
										FaceLinearLayout.super.setVisibility(GONE);
									}
									isSelfShow = false;
								}
							}
						});
					}
				};
			}
			mRootView.getViewTreeObserver().addOnGlobalLayoutListener(mOnGlobalLayoutListener);
		}
	}
	
	public int getContentHeight(){
		if(mRootView == null){
			return mDecorView.getHeight();
		}
		if(mRootView.getChildCount() > 0){
			View child = mRootView.getChildAt(0);
			if(child != null){
				if(((ViewGroup) child).getChildCount() > 0){
					child = ((ViewGroup) child).getChildAt(0);
					if(child != null){
						return child.getHeight();
					}
				}
			}
		}
		return mRootView.getHeight();
	}
	
	@SuppressWarnings("deprecation")
	@Override
	protected void onDetachedFromWindow() {
		super.onDetachedFromWindow();
		if(mOnGlobalLayoutListener != null){
			if (android.os.Build.VERSION.SDK_INT >= 17) {
				mRootView.getViewTreeObserver().removeOnGlobalLayoutListener(mOnGlobalLayoutListener);
			}else{
				mRootView.getViewTreeObserver().removeGlobalOnLayoutListener(mOnGlobalLayoutListener);
			}
		}
	}
	
	@Override
	public void setVisibility(final int visibility) {
		super.setVisibility(visibility);
		if(visibility == VISIBLE){
			if(isShowSoftInput){
				isSelfShow = true;
				mInputMethodManager.hideSoftInputFromWindow(getWindowToken(),0); //强制隐藏键盘 
			}
		}
	}
}
