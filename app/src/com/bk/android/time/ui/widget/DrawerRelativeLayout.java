package com.bk.android.time.ui.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.RelativeLayout;
import android.widget.Scroller;

public class DrawerRelativeLayout extends RelativeLayout {
	private DrawerBackgroundView mDrawerBgView;
	private DrawerForegroundView mDrawerFgView;
	private Point mLastPoint;
	private Point mDownPoint;
	private boolean isStartDrag;
	private Rect mInitBgRect;
	private Rect mInitFgRect;
	private int mBgFinishDifference;
	private int mFgFinishDifference;
	private boolean isOpen;
	private boolean isTouch;
	private boolean isCheck;
	private boolean isClick;
	private int mCurrentDifferenceY;
	private Scroller mScroller;
	private OnDrawerListener mDrawerListener;
	private VelocityTracker mVelocityTracker;
	private float mMaximumVelocity;
	private int mMinimumVelocity;
	private int mTouchSlop;

	public DrawerRelativeLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		mLastPoint = new Point();
		mDownPoint = new Point();
		mInitBgRect = new Rect();
		mInitFgRect = new Rect();
		mScroller = new Scroller(getContext());
        mVelocityTracker = VelocityTracker.obtain();
        final ViewConfiguration configuration = ViewConfiguration.get(getContext());
        mTouchSlop = configuration.getScaledTouchSlop();
        mMinimumVelocity = configuration.getScaledMinimumFlingVelocity();
        mMaximumVelocity = configuration.getScaledMaximumFlingVelocity();
	}

	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();
		findDrawerBackgroundView();
		findDrawerForegroundView();
	}

	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		super.onLayout(changed, l, t, r, b);
		if (isOpen) {
			Rect selectedRect = mDrawerBgView.getSelectedRect();
			if (selectedRect != null) {
				int oldTop = mInitFgRect.top;
				initDrawer(selectedRect);
				if(oldTop != mInitFgRect.top){
					mCurrentDifferenceY -= oldTop - mInitFgRect.top;
				}
			} else {
				View fgView = mDrawerFgView.getView();
				fgView.layout(fgView.getLeft(), fgView.getTop(),
						fgView.getRight(), fgView.getBottom()
								+ mFgFinishDifference);
			}
			offsetDrag(-mCurrentDifferenceY);
		}
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		if (!mScroller.isFinished()) {
			return true;
		}
		if (isDragEnabled()) {
			int differenceY = 0;
			View fgView = mDrawerFgView.getView();
			switch (ev.getAction()) {
			case MotionEvent.ACTION_DOWN:
				mLastPoint.set((int) ev.getX(), (int) ev.getY());
				mDownPoint.set((int) ev.getX(), (int) ev.getY());
				isStartDrag = false;
				isTouch = true;
				isCheck = false;
				isClick = true;
	            mVelocityTracker.clear();
				break;
			case MotionEvent.ACTION_MOVE:
				if (!isTouch) {
					return true;
				}
		        mVelocityTracker.addMovement(ev);
				differenceY = (int) (ev.getY() - mLastPoint.y);
				if(isClick && Math.abs(differenceY) > mTouchSlop){
					isClick = false;
				}
				if (!isStartDrag && (!isCheck || !isOpen) && differenceY != 0) {
					isCheck = true;
					checkCanDrag(differenceY, ev,!isOpen);
				}
				if (isStartDrag) {
					int newFgDifference = mInitFgRect.top - fgView.getTop()
							- differenceY;
					if (newFgDifference < 0) {
						differenceY = mInitFgRect.top - fgView.getTop();
					} else if (newFgDifference > mFgFinishDifference) {
						differenceY = mInitFgRect.top - fgView.getTop()
								- mFgFinishDifference;
					}
					offsetDrag(differenceY);
				}
				mLastPoint.set((int) ev.getX(), (int) ev.getY());
				break;
			case MotionEvent.ACTION_UP:
			case MotionEvent.ACTION_CANCEL:
			case MotionEvent.ACTION_OUTSIDE:
				if (!isTouch) {
					return true;
				}
		        mVelocityTracker.addMovement(ev);
				if (isStartDrag) {
					final VelocityTracker velocityTracker = mVelocityTracker;
					velocityTracker.computeCurrentVelocity(1000,mMaximumVelocity);
					int initialVelocity = (int) velocityTracker.getYVelocity(0);
                    if ((Math.abs(initialVelocity) > mMinimumVelocity)) {
						if (initialVelocity > 0) {// 向下
							startCloseAnim();
						} else {
							startOpenAnim();
						}
					} else if(isClick){
						int newFgDifference = mInitFgRect.top - fgView.getTop();
						if (newFgDifference > mFgFinishDifference / 2) {
							startCloseAnim();
						} else {
							startOpenAnim();
						}
					} else {
						int newFgDifference = mInitFgRect.top - fgView.getTop();
						if (newFgDifference > mFgFinishDifference / 2) {
							startOpenAnim();
						} else {
							startCloseAnim();
						}
					}
				}else if(isClick){
					checkCanDrag(0, ev);
					if (isStartDrag) {
						int newFgDifference = mInitFgRect.top - fgView.getTop();
						if (newFgDifference > mFgFinishDifference / 2) {
							startCloseAnim();
						} else {
							startOpenAnim();
						}
					}
				}
				isTouch = false;
				break;
			}
			if (isOpen) {
				ev.setLocation(mDownPoint.x, ev.getY());
			}else{
				ev.setLocation(ev.getX(),mDownPoint.y);
			}
			if (isStartDrag) {
				return true;
			}
		}
		return super.dispatchTouchEvent(ev);
	}

	public boolean isOpenDrawer(){
		return isOpen;
	}
	
	private void checkCanDrag(int differenceY, MotionEvent ev){
		checkCanDrag(differenceY, ev,false);
	}
	
	private void checkCanDrag(int differenceY, MotionEvent ev,boolean force){
		if(!isOpen && differenceY > 0){
			return;
		}else if(isOpen && differenceY < 0){
			return;
		}
		ev = MotionEvent.obtain(ev);
		View fgView = mDrawerFgView.getView();
		if (mDrawerFgView.canDrag(differenceY, (int) ev.getX()
				- fgView.getLeft(),
				(int) ev.getY() - fgView.getTop(),force)) {
			Rect selectedRect = mDrawerBgView.getSelectedRect();
			if (selectedRect != null) {
				isStartDrag = true;
				ev.setAction(MotionEvent.ACTION_CANCEL);
				super.dispatchTouchEvent(ev);
				openDrawer(selectedRect);
			}
		}
	}
	
	public void open(){
		Rect selectedRect = mDrawerBgView.getSelectedRect();
		if (selectedRect != null) {
			openDrawer(selectedRect);
			startOpenAnim();
		}
	}
	
	public void close(){
		startCloseAnim();
	}
	
	private void startOpenAnim() {
		View fgView = mDrawerFgView.getView();
		int difference = mInitFgRect.top - fgView.getTop();
		mScroller.startScroll(0, fgView.getTop(), 1, difference
				- mFgFinishDifference, 500);
		invalidate();
	}

	private void startCloseAnim() {
		View fgView = mDrawerFgView.getView();
		int difference = mInitFgRect.top - fgView.getTop();
		mScroller.startScroll(0, fgView.getTop(), 1, difference, 500);
		invalidate();
	}

	@Override
	protected void dispatchDraw(Canvas canvas) {
		super.dispatchDraw(canvas);
		if (!mScroller.isFinished() && mScroller.computeScrollOffset()) {
			handleScroller();
		} else if (!isTouch && mCurrentDifferenceY == 0) {
			closeDrawer();
		}
	}

	private void handleScroller() {
		View fgView = mDrawerFgView.getView();
		int difference = mScroller.getCurrY() - fgView.getTop();
		offsetDrag(difference);
		invalidate();
	}

	private void initDrawer(Rect selectedRect) {
		View fgView = mDrawerFgView.getView();
		View bgView = mDrawerBgView.getView();
		mInitBgRect.set(bgView.getLeft(), bgView.getTop(), bgView.getRight(),
				bgView.getBottom());
		mInitFgRect.set(fgView.getLeft(), fgView.getTop(), fgView.getRight(),
				fgView.getBottom());
		mBgFinishDifference = selectedRect.top;
		mFgFinishDifference = fgView.getTop() - selectedRect.bottom
				+ mBgFinishDifference;
		int scrollY = fgView.getScrollY();
		fgView.layout(fgView.getLeft(), fgView.getTop(), fgView.getRight(),
				fgView.getBottom() + mFgFinishDifference);
		fgView.scrollTo(0, scrollY);
	}

	private void openDrawer(Rect selectedRect) {
		if (isOpen) {
			return;
		}
		isOpen = true;
		mCurrentDifferenceY = 0;
		initDrawer(selectedRect);
		if (mDrawerListener != null) {
			mDrawerListener.onOpenDrawer();
		}
		mDrawerFgView.setOpen(isOpen);
	}

	private void closeDrawer() {
		if (!isOpen) {
			return;
		}
		mCurrentDifferenceY = 0;
		isOpen = false;
		View fgView = mDrawerFgView.getView();
		int scrollY = fgView.getScrollY();
		fgView.layout(mInitFgRect.left, mInitFgRect.top, mInitFgRect.right,
				mInitFgRect.bottom);
		fgView.scrollTo(0, scrollY);
		if (mDrawerListener != null) {
			mDrawerListener.onColseDrawer();
		}
		mDrawerFgView.setOpen(isOpen);
	}

	private void offsetDrag(int differenceY) {
		View fgView = mDrawerFgView.getView();
		View bgView = mDrawerBgView.getView();
		fgView.offsetTopAndBottom(differenceY);
		mCurrentDifferenceY = mInitFgRect.top - fgView.getTop();
		float scale = 0;
		if (mFgFinishDifference != 0) {
			scale = (mInitFgRect.top - fgView.getTop() - 0f)
					/ mFgFinishDifference * mBgFinishDifference;
		}
		differenceY = (int) (mInitBgRect.top - scale - bgView.getTop());
		bgView.offsetTopAndBottom(differenceY);
		invalidate();
	}

	private boolean isDragEnabled() {
		return mDrawerBgView != null && mDrawerFgView != null;
	}

	private void findDrawerBackgroundView() {
		for (int i = 0; i < getChildCount(); i++) {
			View view = getChildAt(i);
			if (view instanceof DrawerBackgroundView) {
				setDrawerBackgroundView((DrawerBackgroundView) view);
				return;
			}
		}
	}

	private void setDrawerBackgroundView(DrawerBackgroundView drawerview) {
		mDrawerBgView = drawerview;
	}

	private boolean findDrawerForegroundView() {
		for (int i = 0; i < getChildCount(); i++) {
			View view = getChildAt(i);
			if (view instanceof DrawerForegroundView) {
				setDrawerForegroundView((DrawerForegroundView) view);
				return true;
			}
		}
		return false;
	}

	private void setDrawerForegroundView(DrawerForegroundView drawerview) {
		mDrawerFgView = drawerview;
	}

	public void setOnDrawerListener(OnDrawerListener l) {
		mDrawerListener = l;
	}

	public interface DrawerBackgroundView {
		public Rect getSelectedRect();

		public View getView();
	}

	public interface DrawerForegroundView {
		public View getView();

		public boolean canDrag(int differenceY, int x, int y,boolean force);
		
		public void setOpen(boolean isOpen);
	}

	public interface OnDrawerListener {
		public void onOpenDrawer();

		public void onColseDrawer();
	}
}
