package com.bk.android.time.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;

public class QuickSmoothListView extends ListView {
	public QuickSmoothListView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	public void smoothScrollToPosition(int position) {
		if(android.os.Build.VERSION.SDK_INT >= 14){
			super.smoothScrollToPositionFromTop(position, 0, 0);
		}else{
			super.smoothScrollToPosition(position);
		}
	}
}
