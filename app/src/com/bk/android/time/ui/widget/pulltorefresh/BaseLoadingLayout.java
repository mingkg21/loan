package com.bk.android.time.ui.widget.pulltorefresh;

import android.content.Context;
import android.content.res.TypedArray;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bk.android.assistant.R;
import com.bk.android.ui.widget.pulltorefresh.PullToRefreshBase.Mode;
import com.bk.android.ui.widget.pulltorefresh.PullToRefreshBase.Orientation;
import com.bk.android.ui.widget.pulltorefresh.internal.LoadingLayout;

public abstract class BaseLoadingLayout extends LoadingLayout {
	public BaseLoadingLayout(Context context, Mode mode,Orientation scrollDirection, TypedArray attrs) {
		super(context, mode, scrollDirection, attrs);
	}

	@Override
	protected View onCreateContentView(Orientation scrollDirection,Mode mode) {
		View contentView = null;
		switch (scrollDirection) {
			case HORIZONTAL:
				contentView = LayoutInflater.from(getContext()).inflate(R.layout.uniq_pull_to_refresh_header_horizontal,null);
				break;
			case VERTICAL:
			default:
				contentView = LayoutInflater.from(getContext()).inflate(R.layout.uniq_pull_to_refresh_header_vertical,null);
				break;
		}
		FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT);
		switch (mode) {
			case PULL_FROM_END:
				lp.gravity = scrollDirection == Orientation.VERTICAL ? Gravity.TOP : Gravity.LEFT;
				break;
			case PULL_FROM_START:
			default:
				lp.gravity = scrollDirection == Orientation.VERTICAL ? Gravity.BOTTOM : Gravity.RIGHT;
				break;
		}
		contentView.setLayoutParams(lp);
		return contentView;
	}

	@Override
	protected TextView findHeaderText(View contentView) {
		return (TextView)contentView.findViewById(R.id.pull_to_refresh_text);
	}

	@Override
	protected TextView findSubHeader(View contentView) {
		return (TextView)contentView.findViewById(R.id.pull_to_refresh_sub_text);
	}

	@Override
	protected ProgressBar findHeaderProgress(View contentView) {
		return (ProgressBar)contentView.findViewById(R.id.pull_to_refresh_progress);
	}

	@Override
	protected ImageView findImageView(View contentView) {
		return (ImageView)contentView.findViewById(R.id.pull_to_refresh_image);
	}

	@Override
	protected String getPullLabel(Mode mode) {
		switch (mode) {
			case PULL_FROM_END:
				return getContext().getResources().getString(R.string.pull_to_refresh_from_bottom_pull_label);
			case PULL_FROM_START:
			default:
				return getContext().getResources().getString(R.string.pull_to_refresh_pull_label);
		}
	}

	@Override
	protected String getRefreshingLabel(Mode mode) {
		switch (mode) {
			case PULL_FROM_END:
				return getContext().getResources().getString(R.string.pull_to_refresh_from_bottom_refreshing_label);
			case PULL_FROM_START:
			default:
				return getContext().getResources().getString(R.string.pull_to_refresh_refreshing_label);
		}
	}

	@Override
	protected String getReleaseLabel(Mode mode) {
		switch (mode) {
			case PULL_FROM_END:
				return getContext().getResources().getString(R.string.pull_to_refresh_from_bottom_release_label);
			case PULL_FROM_START:
			default:
				return getContext().getResources().getString(R.string.pull_to_refresh_release_label);
		}
	}
}
