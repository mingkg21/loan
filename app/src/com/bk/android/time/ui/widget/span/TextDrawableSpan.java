package com.bk.android.time.ui.widget.span;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.FontMetricsInt;
import android.text.TextPaint;
import android.text.style.ReplacementSpan;

public class TextDrawableSpan extends ReplacementSpan{
	private String mContent;
	private String mResultContent;
    private ISpanDrawableView mSpanDrawableView;

	public TextDrawableSpan(String content){
		mContent = content;
	}
	
	public void setSpanDrawableView(ISpanDrawableView spanDrawableView){
		mSpanDrawableView = spanDrawableView;
	}
	
	@Override
	public void updateDrawState(TextPaint ds) {
		ds.setColor(0xff999999);
		super.updateDrawState(ds);
	}
	
	@Override
	public int getSize(Paint paint, CharSequence text, int start, int end,FontMetricsInt fm) {
		if(mSpanDrawableView == null){
			return 0;
		}
		if(fm != null){
			paint.getFontMetricsInt(fm);
		}
		mResultContent = mContent;
		if(mResultContent == null){
			mResultContent = "";
		}
		int nextPos = paint.breakText(mResultContent, true , mSpanDrawableView.getMaxViewWidth() - paint.measureText("…:"), null);
		if(nextPos > 0 && nextPos != mResultContent.length()){
			mResultContent = mResultContent.substring(0, nextPos);
			mResultContent += "…:";
		}else{
			mResultContent += ":";
		}
		return (int) paint.measureText(mResultContent);
	}

	@Override
	public void draw(Canvas canvas, CharSequence text, int start, int end,float x, int top, int y, int bottom, Paint paint) {
		if(mSpanDrawableView == null){
			return;
		}
		updateDrawState((TextPaint) paint);
		canvas.drawText(mResultContent, x, y, paint);
	}

	@Override
	public boolean equals(Object o) {
		if(o instanceof TextDrawableSpan){
			return ((TextDrawableSpan) o).mContent.equals(mContent);
		}
		return super.equals(o);
	}
}
