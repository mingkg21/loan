package com.bk.android.time.ui.widget.binding;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

/**
 * @author mingkg21
 * @email mingkg21@gmail.com
 * @date 2014年10月28日
 *
 */
public class BFullImageAutoView extends BAsyncImageView{
	private int mLastMeasuredWidth;

	public BFullImageAutoView(Context context) {
		super(context);
	}
	
	public BFullImageAutoView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		Drawable drawable = getDrawable();
		if(mLastMeasuredWidth <= 0){
			mLastMeasuredWidth = MeasureSpec.getSize(widthMeasureSpec);
		}
		if(drawable != null){
			int imgW = drawable.getIntrinsicWidth();
	        int imgH = drawable.getIntrinsicHeight();
			int w = imgW;
		    w += getPaddingLeft() + getPaddingRight();
	             
	        w = Math.max(w, getSuggestedMinimumWidth());
	        
	        widthMeasureSpec = MeasureSpec.makeMeasureSpec(mLastMeasuredWidth, MeasureSpec.getMode(widthMeasureSpec));
	        
	        w = resolveSizeAndState(w, widthMeasureSpec , 0);
	        
	        w -= getPaddingLeft() + getPaddingRight();
	        
	        int tW = 0;
 	        int tH = 0;
 	        if(imgW >= imgH) {//横的图片
 	        	tW = (int)(w * 2.0f / 3);
 	        } else {//竖的图片
 	        	float tmp = tH * 1.0f / tW;
 	        	if(tmp > 2.0f) {
 	        		tW = (int)(w * 1.0f / 4);
 	        	} else if(tmp > 1.5f) {
 	        		tW = (int)(w * 1.0f / 3);
 	        	} else {
 	        		tW = (int)(w * 1.0f / 2);
 	        	}
 	        }
 	        tH = (int)(tW * imgH * 1.0f / imgW);
 	        tW += getPaddingLeft() + getPaddingRight();
 			tH += getPaddingTop() + getPaddingBottom();
 			setMeasuredDimension(tW, tH);
		}else{
			super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		}
	}
	
	public static int resolveSizeAndState(int size, int measureSpec, int childMeasuredState) {
        int result = size;
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize =  MeasureSpec.getSize(measureSpec);
        switch (specMode) {
        case MeasureSpec.UNSPECIFIED:
            result = size;
            break;
        case MeasureSpec.AT_MOST:
            if (specSize < size) {
                result = specSize | MEASURED_STATE_TOO_SMALL;
            } else {
                result = size;
            }
            break;
        case MeasureSpec.EXACTLY:
            result = specSize;
            break;
        }
        return result | (childMeasuredState&MEASURED_STATE_MASK);
    }
}
