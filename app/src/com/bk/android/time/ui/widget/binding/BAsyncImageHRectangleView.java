package com.bk.android.time.ui.widget.binding;

import android.content.Context;
import android.util.AttributeSet;

public class BAsyncImageHRectangleView extends BAsyncImageView{
	public BAsyncImageHRectangleView(Context context) {
		super(context);
	}

	public BAsyncImageHRectangleView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		setMeasuredDimension(getMeasuredWidth(), getMeasuredWidth() * 5 / 4);
	}
}
