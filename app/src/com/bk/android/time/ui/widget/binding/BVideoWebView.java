package com.bk.android.time.ui.widget.binding;

import java.security.MessageDigest;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bk.android.assistant.R;
import com.bk.android.time.widget.media.FullScreenChromeClient;
import com.bk.android.ui.widget.binding.BWebView;
import com.bk.android.util.ApnUtil;
import com.bk.android.util.DimensionsUtil;

public class BVideoWebView extends BWebView implements Runnable{
	private FullScreenChromeClient mFullScreenChromeClient;
	private String mInitData;
	private View mErrLay;
	private TextView mErrTv;
	private TextView mErrBtn;
	private String mCurrentUrl;
	private ProgressBar mWebProgress;

	public BVideoWebView(Context context, AttributeSet attrs) {
		super(context, attrs);
		View contentView = ((Activity) context).getLayoutInflater().inflate(R.layout.com_web_load_lay, null);
		mWebProgress = (ProgressBar) contentView.findViewById(R.id.web_progress);
		mErrLay = contentView.findViewById(R.id.err_tip_lay);
		mErrTv = (TextView) contentView.findViewById(R.id.err_tip_tv);
		mErrBtn = (TextView) contentView.findViewById(R.id.err_tip_btn);
		mErrBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				stopLoading();
				clearView();
				loadUrl(mCurrentUrl);
			}
		});
		LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT);
		lp.addRule(RelativeLayout.ALIGN_TOP, WEB_VIEW_ID);
		lp.addRule(RelativeLayout.ALIGN_BOTTOM, WEB_VIEW_ID);
		addView(contentView,lp);
		if (android.os.Build.VERSION.SDK_INT >= 14) {// 4.0 需打开硬件加速
			((Activity) context).getWindow().setFlags(
				    WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED,
				    WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);
		}
	}

	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();
		init((Activity) getContext());
	}
	
	public void init(Activity activity) {
		mWebView.setFocusable(true);
		mWebView.setFocusableInTouchMode(true);
		mWebView.requestFocus();
		mWebView.setVerticalScrollBarEnabled(false);
		mWebView.setHorizontalScrollBarEnabled(false);
		
		getSettings().setNeedInitialFocus(false);
		getSettings().setBuiltInZoomControls(false);
		getSettings().setJavaScriptEnabled(true);
		getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
		getSettings().setAllowFileAccess(true);
		getSettings().setLoadWithOverviewMode(true);

		addJavascriptInterface(new JSObject(), "android");
		setWebViewClient(new WebViewClient(){
			@Override
			public void onPageFinished(WebView view, String url) {
				super.onPageFinished(view, url);
				mWebProgress.setVisibility(View.INVISIBLE);
				try {
					mWebView.getSettings().setBlockNetworkImage(false);
				} catch (Exception e) {}
			}
			
			@Override
			public void onPageStarted(WebView view, String url, Bitmap favicon) {
				mWebProgress.setVisibility(View.VISIBLE);
				removeCallbacks(BVideoWebView.this);
				post(BVideoWebView.this);
				mCurrentUrl = url;
				if(mInitData == null && url != null){
					url = Uri.decode(url);
					int start = url.indexOf("<html>");
					int end = url.indexOf("</html>");
					if(start != -1 && end != -1){
						mInitData = url.substring(start ,end + "</html>".length());
					}else{
						mInitData = url;
					}
				}
				super.onPageStarted(view, url, favicon);
				try {
					mWebView.getSettings().setBlockNetworkImage(true);
				} catch (Exception e) {}
			}

			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				boolean isOut = false;
				Uri uri = null;
				try {
					uri = Uri.parse(url);
				} catch (Exception e) {}
				if(uri != null && "http".equals(uri.getScheme())){
					if (android.os.Build.VERSION.SDK_INT > 10) {
						reLoadInitData();
						return true;
					}else{
						isOut = true;
					}
				}
				if(isOut || url.indexOf("banketime=out") != -1){
				    try{
				    	Intent intent = new Intent();
					    intent.setAction(Intent.ACTION_VIEW);
					    Uri content_url = Uri.parse(url);
					    intent.setData(content_url);
					    getContext().startActivity(intent);
						return true;
				    }catch (Exception e) {}
				}
				return false;
			}
			
			@Override
			public void onReceivedError(WebView view, int errorCode,
					String description,final String failingUrl) {
				mErrLay.setVisibility(View.VISIBLE);
				if(ApnUtil.isNetAvailable(view.getContext())){
					mErrTv.setText(R.string.tip_err_server);
				}else{
					mErrTv.setText(R.string.tip_on_net);
				}
				mWebView.setVisibility(View.GONE);
				super.onReceivedError(view, errorCode, description, failingUrl);
			}
		});
		mFullScreenChromeClient = new FullScreenChromeClient(activity){
			@Override
			public void onProgressChanged(WebView view, int newProgress) {
				super.onProgressChanged(view, newProgress);
				if(mWebProgress.getProgress() < newProgress){
					mWebProgress.setProgress(newProgress);
				}
			}
		};
		setWebChromeClient(mFullScreenChromeClient);
	}
	
	public void setYouKuVideo(String clientSecret, String clientId, String vid) {
		String html = getYouKuHtml(getContext(),vid, clientId, clientSecret);
		loadData(html, "text/html; charset=UTF-8", null);
	}
	
	@Override
	public void loadData(String data, String mimeType, String encoding) {
		super.loadData(data, mimeType, encoding);
		mErrLay.setVisibility(View.GONE);
		mWebView.setVisibility(View.VISIBLE);
	}

	@Override
	public void loadUrl(String url) {
		super.loadUrl(url);
		mErrLay.setVisibility(View.GONE);
		mWebView.setVisibility(View.VISIBLE);
	}

	@Override
	protected void onDetachedFromWindow() {
		super.onDetachedFromWindow();
		destroy();
	}
	
	@Override
	public void destroy() {
		if(mFullScreenChromeClient != null){
			mFullScreenChromeClient.hideWindow();
		}
		super.destroy();
	}
	
	@Override
	public void onPause() {
		if(mFullScreenChromeClient != null){
			mFullScreenChromeClient.hideWindow();
		}
		super.onPause();
	}
	
	private void reLoadInitData(){
		post(new Runnable() {
			@Override
			public void run() {
				if(mInitData != null){
					Uri uri = null;
					try {
						uri = Uri.parse(mInitData);
					} catch (Exception e) {}
					if(uri != null && "http".equals(uri.getScheme())){
						loadUrl(mInitData);
					}else{
						loadData(mInitData, "text/html; charset=UTF-8", null);
					}
				}
			}
		});
	}
	
	public class JSObject {
		@android.webkit.JavascriptInterface
        public void onPlayerReady() {
        }
		@android.webkit.JavascriptInterface
        public void onPlayStart() {
        }
		@android.webkit.JavascriptInterface
        public void onPlayEnd() {
			reLoadInitData();
        }
    }

	public boolean onBackPressed() {
		if(mFullScreenChromeClient.isShowWindow()){
			mFullScreenChromeClient.hideWindow();
			return true;
		}
		return false;
	}
	
	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		try {
			boolean handled = super.dispatchTouchEvent(ev);
			if(android.os.Build.VERSION.SDK_INT >= 19 && ev.getAction() == MotionEvent.ACTION_UP){
				int size = DimensionsUtil.DIPToPX(40);
				if(ev.getX() > getWidth() - size && ev.getY() > getHeight() - size){
					loadUrl("javascript:fullScreen()");
				}
			}
			return handled;
		} catch (Exception e) {}
		return false;
	}
	
	public static String getYouKuHtml(Context context, String vid,
			String clientId, String clientSecret) {
		DisplayMetrics dm = context.getResources().getDisplayMetrics();
		int width = (int) (dm.widthPixels / (dm.densityDpi * 1f / DisplayMetrics.DENSITY_MEDIUM));
		int height = (int) (dm.heightPixels / (dm.densityDpi * 1f / DisplayMetrics.DENSITY_MEDIUM));
		int videoWidth = width;
		int videoHeight = (int) (videoWidth / 1.333333f);
		int gapW = videoHeight * (videoWidth - width) / videoWidth;
		int gapH = videoHeight - height;
		if(gapW > gapH){
			videoHeight = videoHeight * width / videoWidth;
			videoWidth = width;
		}else{
			videoWidth = videoWidth * height / videoHeight;
			videoHeight = height;
		}
		StringBuffer result = new StringBuffer();
		result.append("<html>");
		
		result.append("<script type='text/javascript'>");
			result.append("function fullScreen(){");
				result.append("video = document.getElementsByTagName('video')[0];");
				result.append("if (!document.webkitFullScreen && video.webkitEnterFullscreen) {");
					result.append("video.webkitEnterFullscreen();");
				result.append("}");
			result.append("}");
		result.append("</script>");
		
		result.append("<style type='text/css'>*{margin:0;padding:0;-webkit-transform:translate3d(0, 0, 0);}</style>");
		result.append("<div id='youkuplayer' style='background-color: black;margin:0 auto;width:" + videoWidth
				+ "px;height:" + videoHeight + "px;'></div>");
		result.append("<script type='text/javascript' src='http://player.youku.com/jsapi'>");
		result.append("player = new YKU.Player('youkuplayer',{");
		result.append("styleid: '6',");
		result.append("client_id: '" + clientId + "',");
		result.append("vid: '" + vid + "',");
		result.append(createEmbsig(vid, clientSecret) + ",");
//		result.append("password:'" + MD5("woshi3201663" + clientId) + "',");
		result.append("show_related: false ,");
		
		result.append("events:{");
		
			result.append("onPlayEnd:function(){");
				result.append("javascript:android.onPlayEnd();");
			result.append("},");
			
			result.append("onPlayStart:function(){");
				result.append("javascript:android.onPlayStart();");
			result.append("},");
			
			result.append("onPlayerReady:function(){");
				result.append("javascript:android.onPlayerReady();");
			result.append("}");
			
		result.append("}");
		
		result.append("});");
		
		result.append("function playVideo(){");
			result.append("player.playVideo();");
		result.append("}");
		
		result.append("function pauseVideo(){");
			result.append("player.pauseVideo();");
		result.append("}");
		
		result.append("function seekTo(s){");
			result.append("player.seekTo(s);");
		result.append("}");
		
		result.append("function currentTime(){");
			result.append("return player.currentTime();");
		result.append("}");
		
		result.append("</script>");
		result.append("</html>");
		return result.toString();
	}
	
	public void playYouKuVideo(){
		mWebView.loadUrl("javascript:playVideo()");
	}
	
	public void pauseYouKuVideo(){
		mWebView.loadUrl("javascript:pauseVideo()");
	}
	
	public static String MD5(String password) {
		StringBuffer result = new StringBuffer();
		try {
			MessageDigest md5 = MessageDigest.getInstance("MD5");
			byte[] bytes = md5.digest(password.getBytes());
			for (byte b : bytes) {
				String temp = Integer.toHexString(b & 0xff);
				if (temp.length() == 1) {
					result.append("0");
				}
				result.append(temp);
			}
		} catch (Exception e) {
		}
		return result.toString();
	}

	public static String createEmbsig(String vid, String clientSecret) {
		String timestamp = String.valueOf(System.currentTimeMillis() / 1000);
		StringBuffer result = new StringBuffer();
		result.append("embsig:'");
		result.append("1");
		result.append("_");
		result.append(timestamp);
		result.append("_");
		result.append(MD5(vid + "_" + timestamp + "_" + clientSecret));
		result.append("'");
		return result.toString();
	}

	@Override
	public void run() {
		if(mWebProgress.isShown() && mWebProgress.getProgress() < 99){
			mWebProgress.setProgress(mWebProgress.getProgress() + 1);
			if(mWebProgress.getProgress() < 30){
				postDelayed(this, 150);
			}else if(mWebProgress.getProgress() < 60){
				postDelayed(this, 300);
			}else if(mWebProgress.getProgress() < 80){
				postDelayed(this, 500);
			}else{
				postDelayed(this, 1000);
			}
		}
	}
}
