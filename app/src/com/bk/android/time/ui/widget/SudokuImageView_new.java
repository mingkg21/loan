package com.bk.android.time.ui.widget;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.FrameLayout;

import com.bk.android.time.ui.widget.AsyncImageViewManager.ImageInfo;
import com.bk.android.util.BitmapUtil;

public class SudokuImageView_new extends FrameLayout {
	public final int TYPE_SUDOKU = 0;
	public final int TYPE_FANCY = 1;
    private ArrayList<AsyncImageDrawable> mDrawableList = new ArrayList<AsyncImageDrawable>();
    private LinkedList<AsyncImageDrawable> mCacheDrawableList = new LinkedList<AsyncImageDrawable>();
    private ArrayList<ImageInfo> mImageInfoList = new ArrayList<ImageInfo>();
    
    private int mSpacing = 0;
	private int mDefaultImgRes = 0;
	private int mQuality = BitmapUtil.IMG_QUALITY_1;
	private OnSudokuItemClickListener mOnSudokuItemClickListener;
	private int mClickIndex;
	private float mZoom;
	private SudokuLayout mSudokuLayout;
	
	public SudokuImageView_new(Context context, AttributeSet attrs) {
		super(context, attrs);
		setFocusable(false);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if(event.getAction() == MotionEvent.ACTION_DOWN){
			mClickIndex = -1;
			int i = 0;
			for (AsyncImageDrawable drawable : mDrawableList) {
				if(drawable.getBounds().contains((int)event.getX(),(int)event.getY())){
//					drawable.setPress(true);
					mClickIndex = i;
					break;
				}
				i++;
			}
		}else if(event.getAction() == MotionEvent.ACTION_UP){
			if(mOnSudokuItemClickListener != null && mClickIndex != -1){
				mOnSudokuItemClickListener.onSudokuItemClick(this, mClickIndex);
			}
		}
		if(event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL || event.getAction() == MotionEvent.ACTION_OUTSIDE){
//			for (AsyncImageDrawable drawable : mDrawableList) {
//				drawable.setPress(false);
//			}
		}
		return mClickIndex != -1;
	}
	
	public void setDefaultImgRes(int defaultImgRes){
		mDefaultImgRes = defaultImgRes;
		for (AsyncImageDrawable drawable : mDrawableList) {
			drawable.setDefaultImgRes(defaultImgRes);
		}
	}
	
	public void setQuality(int quality){
		mQuality = quality;
		for (AsyncImageDrawable drawable : mDrawableList) {
			drawable.setQuality(quality);
		}
	}
	
	public void setData(Collection<Object> imgInfoList){
		int sideSize = 0;
		int horizontalSize = 0;
		int verticalSize = 0;
		int size = 0;
		mImageInfoList.clear();
		if(imgInfoList != null){
			for (Object object : imgInfoList) {
				if(object instanceof ImageInfo){
					ImageInfo imageInfo = (ImageInfo) object;
					mImageInfoList.add(imageInfo);
					float proportion = imageInfo.mWidth * 1f / imageInfo.mHeight;
					if(0.875f < proportion){
						verticalSize ++;
					}else if(1.165f > proportion){
						horizontalSize ++;
					}else{
						sideSize ++;
					}
				}else if(object instanceof String){
					mImageInfoList.add(new ImageInfo((String) object, 0, 0));
					sideSize++;
				}
				size++;
				if(size == 4){
					return;
				}
			}
		}
		mSudokuLayout = SudokuLayout.getSudokuLayout(1000 + sideSize * 100 + horizontalSize * 10 + verticalSize);
		requestLayout();
	}
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		SudokuLayout sudokuLayout = mSudokuLayout;
		int curR = sudokuLayout.getWidth();
		int curB = sudokuLayout.getHeight();
		SudokuLayout leftItem = null;
		SudokuLayout topItem = null;
		for (;true;) {
			if(topItem != null){
				
			}else if(leftItem != null){
				
			}else{
				curR += sudokuLayout.getWidth();
				curB += sudokuLayout.getHeight();
			}
			sudokuLayout = sudokuLayout.getNextItem();
			if(sudokuLayout == null){
				return;
			}else{
				leftItem = sudokuLayout.getLeftItem();
				topItem = sudokuLayout.getTopItem();
			}
		}
	}
	
	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		super.onLayout(changed, l, t, r, b);
		int left = getPaddingLeft();
		int top = getPaddingTop();
		for (int i = 0;(i < mImageInfoList.size() || i < mDrawableList.size()) && i >= 0; i++) {
			if(i < mImageInfoList.size()){
				AsyncImageDrawable drawable = null;
				if(i < mDrawableList.size()){
					drawable = mDrawableList.get(i);
				}else{
					drawable = newDrawable();
					mDrawableList.add(drawable);
				}
				
				
				//TODO 布局
				drawable.setImageUrl(mImageInfoList.get(i));
			}else{
				removeDrawable();
				i--;
			}
		}
		invalidate();
	}
	
	@Override
	protected void dispatchDraw(Canvas canvas) {
		super.dispatchDraw(canvas);
		for (AsyncImageDrawable drawable : mDrawableList) {
			drawable.draw(canvas);
		}
	}
	
	@Override
	protected boolean verifyDrawable(Drawable who) {
		if(who instanceof AsyncImageDrawable){
			return true;
		}
		return super.verifyDrawable(who);
	}
	
	private void removeDrawable(){
		if(mDrawableList.isEmpty()){
			return;
		}
		AsyncImageDrawable drawable = mDrawableList.remove(mDrawableList.size() - 1);
		if(drawable != null){
			drawable.release();
			if(mCacheDrawableList.size() + mDrawableList.size() < 9){
				mCacheDrawableList.add(drawable);
			}else{
				drawable.setCallback(null);
			}
			drawable.onDetachedFromWindow();
		}
	}
	
	private AsyncImageDrawable newDrawable(){
		AsyncImageDrawable drawable = null;
		if(!mCacheDrawableList.isEmpty()){
			drawable = mCacheDrawableList.removeFirst();
		}
		if(drawable == null){
			drawable = new AsyncImageDrawable();
			drawable.setCallback(this);
		}
		drawable.setNeedBuildThumbUrl(true);
		drawable.setDefaultImgRes(mDefaultImgRes);
		drawable.setQuality(mQuality);
		drawable.onAttachedToWindow();
		return drawable;
	}
	
	@Override
	protected void onDetachedFromWindow() {
		super.onDetachedFromWindow();
		for (;!mDrawableList.isEmpty();) {
			removeDrawable();
		}
	}
	/**
	 * @return the mHorizontalSpacing
	 */
	public int getSpacing() {
		return mSpacing;
	}
	/**
	 * @param mHorizontalSpacing the mHorizontalSpacing to set
	 */
	public void setSpacing(int horizontalSpacing) {
		if(mSpacing != horizontalSpacing){
			this.mSpacing = horizontalSpacing;
			requestLayout();
		}
	}
	
	public void setOnSudokuItemClickListener(OnSudokuItemClickListener l){
		mOnSudokuItemClickListener = l;
	}
	
	public interface OnSudokuItemClickListener {
		public void onSudokuItemClick(SudokuImageView_new view, int position);
	}
}
