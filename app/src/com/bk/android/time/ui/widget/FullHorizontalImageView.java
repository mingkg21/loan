package com.bk.android.time.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * @author mingkg21
 * @email mingkg21@gmail.com
 * @date 2015年2月8日
 *
 */
public class FullHorizontalImageView extends ImageView {
	
	public FullHorizontalImageView(Context context) {
		super(context);
	}
	
	public FullHorizontalImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		if(getDrawable() != null){
			int imgH = getDrawable().getIntrinsicHeight() * getMeasuredWidth() / getDrawable().getIntrinsicWidth();
			setMeasuredDimension(getMeasuredWidth(), imgH);
		}
	}

}
