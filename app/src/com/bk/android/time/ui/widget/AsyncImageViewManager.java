package com.bk.android.time.ui.widget;

import java.lang.ref.WeakReference;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.os.SystemClock;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnScrollChangedListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;

import com.bk.android.app.BaseApp;
import com.bk.android.time.app.App;
import com.bk.android.time.widget.ImageLoader;
import com.bk.android.util.ApnUtil;
import com.bk.android.util.BitmapUtil;
import com.bk.android.util.DimensionsUtil;
import com.bk.android.util.LogUtil;
import com.bk.android.widget.imageload.BaseImageLoader.ImageBitmapInfo;
import com.bk.android.widget.imageload.BaseImageLoader.ImageCallback;
/**
 * 异步图片加载ImageView
 * @author linyiwei
 */
public class AsyncImageViewManager implements AnimationListener{
	private static final int LOAD_STATE_NON = 0;
	private static final int LOAD_STATE_FAIL = 1;
	private static final int LOAD_STATE_SUCCESS = 2;
	private static final int LOAD_STATE_PRE_THUMB = 3;
	private static final int LOAD_STATE_SUCCESS_THUMB = 4;
	private String mUrl;
	private Drawable mDefaultDrawable;
	private ImageLoader mImageLoader;
	private InteriorImageCallback mInteriorImageCallback;
	private int hasLoadNetImg = LOAD_STATE_NON;
	private IImageView mImageView;
	private Animation mFinishPostAnimation;
	private Animation mFinishPreAnimation;
	private boolean isRepeat;
	private Bitmap mBitmapTemp;
	private int mQuality;
	private InteriorDrawable mInteriorDrawable;
	private boolean isDefaultHeavyDraw;
	private int mLastY = -1;
	private long mLastTime = -1;
	private int mCheckSpeedDelayed = 100;
	private int mSpeed = 0;
	private int mPostSetSpeed = DimensionsUtil.DIPToPX(80);
	private String mRequestUrl;
	private Runnable mImageUrlRunnable;
	private OnScrollChangedListener mScrollChangedListener;
	private NetWorkListener mNetWorkListener;
	private int mWidth;
	private int mHeight;
	private boolean isNeedBuildThumbUrl;
	private boolean hasShowImg;
	
	public AsyncImageViewManager(IImageView imageView){
		mImageView = imageView;
		mImageLoader = ImageLoader.getInstance();
		mInteriorImageCallback = new InteriorImageCallback(this);
//		Animation preAnimation = AnimationUtils.loadAnimation(getContext(), android.R.anim.fade_out);
//		preAnimation.setDuration(300);
//		setFinishPreAnimation(preAnimation);
//		Animation postAnimation = AnimationUtils.loadAnimation(getContext(), android.R.anim.fade_in);
//		postAnimation.setDuration(300);
//		setFinishPostAnimation(postAnimation);
		mQuality = BitmapUtil.IMG_QUALITY_1;
		mDefaultDrawable = getDefaultImg(0);
	}
	
	public void onAttachedToWindow(View view){
		if(view != null && mScrollChangedListener == null){
			mScrollChangedListener = new ScrollChangedListener(view);
			view.getViewTreeObserver().addOnScrollChangedListener(mScrollChangedListener);
		}
		if(mNetWorkListener == null){
			mNetWorkListener = new NetWorkListener();
			App.getInstance().registerReceiver(mNetWorkListener, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
		}
	}
	
	public void onDetachedFromWindow(View view){
		if(view != null && mScrollChangedListener != null){
			view.getViewTreeObserver().removeOnScrollChangedListener(mScrollChangedListener);
			mScrollChangedListener = null;
		}
		if(mNetWorkListener != null){
			App.getInstance().unregisterReceiver(mNetWorkListener);
			mNetWorkListener = null;
		}
	}
	
	public void setDefaultHeavyDraw(boolean defaultHeavyDraw){
		isDefaultHeavyDraw = defaultHeavyDraw;
	}
	
	public void setNeedBuildThumbUrl(boolean needBuildThumbUrl){
		isNeedBuildThumbUrl = needBuildThumbUrl;
	}
	
	public void setQuality(int quality){
		if(quality >= BitmapUtil.IMG_QUALITY_0
				&& quality <= BitmapUtil.IMG_QUALITY_5){
			mQuality = quality;
		}
	}
	
	public int getQuality(){
		return mQuality;
	}
	
	public void setFinishAnimationRepeat(boolean repeat){
		isRepeat = repeat;
	}
	
	public void setFinishPreAnimation(int anim){
		Animation animation = null;
		if(anim > 0){
			animation = AnimationUtils.loadAnimation(getContext(), anim);
		}
		setFinishPreAnimation(animation);
	}

	public void setFinishPreAnimation(Animation animation){
		if(mFinishPreAnimation != null){
			mFinishPreAnimation.setAnimationListener(null);
		}
		mFinishPreAnimation = animation;
		if(mFinishPreAnimation != null){
			mFinishPreAnimation.setAnimationListener(this);
			mFinishPreAnimation.setFillAfter(true);
		}
	}
	
	public void setFinishPostAnimation(int anim){
		Animation animation = null;
		if(anim > 0){
			animation = AnimationUtils.loadAnimation(getContext(), anim);
		}
		setFinishPostAnimation(animation);
	}

	public boolean hasShowImg(){
		return hasShowImg;
	}
	
	public boolean hasLoadNetImg(){
		return hasLoadNetImg == LOAD_STATE_SUCCESS;
	}
	
	public boolean hasLoadNetImgFail(){
		return hasLoadNetImg == LOAD_STATE_FAIL;
	}
	
	public void setFinishPostAnimation(Animation animation){
		if(mFinishPostAnimation != null){
			mFinishPostAnimation.setAnimationListener(null);
		}
		mFinishPostAnimation = animation;
		if(mFinishPostAnimation != null){
			mFinishPostAnimation.setAnimationListener(this);
			mFinishPostAnimation.setFillAfter(true);
		}
	}
	
	public void setDefaultImgRes(int defaultImgRes){
		mDefaultDrawable = getDefaultImg(defaultImgRes);
		if(!hasLoadNetImg()){
			if(mInteriorDrawable != null){
				mInteriorDrawable.replaceDrawable(mDefaultDrawable);
				mImageView.setImageDrawable(mInteriorDrawable);
				mImageView.invalidate();
			}else{
				mImageView.setImageDrawable(mDefaultDrawable);
			}
		}
	}
	
	public boolean isSelfDrawable(Drawable drawable){
		return drawable != null && (drawable.equals(mInteriorDrawable) || drawable.equals(mDefaultDrawable));
	}
	
	public boolean isDefaultDrawable(Drawable drawable){
		if(drawable instanceof InteriorDrawable){
			return mDefaultDrawable.equals(((InteriorDrawable)drawable).getSourceDrawable());
		}
		return mDefaultDrawable.equals(drawable);
	}
	
	public Drawable getDefaultDrawable(){
		return mDefaultDrawable;
	}
	
	private Drawable getDefaultImg(int defaultImgRes){
		if(defaultImgRes > 0){
			return getContext().getResources().getDrawable(defaultImgRes);
		}else{
			return new ColorDrawable(Color.TRANSPARENT);
		}
	}
	
	private Context getContext() {
		return App.getInstance();
	}

	public String getUrl(){
		return mUrl != null ? mUrl : mRequestUrl;
	}
	
	public void setImageUrl(String url){
		setImageUrl(url,0,0);
	}
	
	public void setImageInfo(ImageInfo info){
		if(info == null){
			setImageUrl(null);
			return;
		}
		setImageUrl(info.mSrc,info.mWidth,info.mHeight);
	}
	
	public String handImageUrl(String url){
		if(isNeedBuildThumbUrl){
			url = mImageLoader.handImageUrl(url, mQuality);
		}
		return url;
	}
	
	public void setImageUrl(String url,final int width,final int height){
		final String finalUrl = handImageUrl(url);
		if(mRequestUrl != null && mRequestUrl.equals(finalUrl)){
			return;
		}
		if(mUrl == null && mUrl == finalUrl){
			return;
		}
		if(!TextUtils.isEmpty(finalUrl) && hasLoadNetImg != LOAD_STATE_FAIL && finalUrl.equals(mUrl)){
			return;
		}
		if(mImageUrlRunnable != null){
			App.getHandler().removeCallbacks(mImageUrlRunnable);
			mImageUrlRunnable = null;
		}
		if(mSpeed > mPostSetSpeed){
			mRequestUrl = finalUrl;
			handImageUrl(null, 0, 0);
			if(!TextUtils.isEmpty(finalUrl)){
				mImageUrlRunnable = new Runnable() {
					@Override
					public void run() {
						if(!finalUrl.equals(mRequestUrl)){
							return;
						}
						if(mSpeed < mPostSetSpeed / 5){
							handImageUrl(finalUrl, width, height);
							mRequestUrl = null;
							mImageUrlRunnable = null;
						}else{
							App.getHandler().postDelayed(this, mCheckSpeedDelayed / 2);
						}
					}
				};
				App.getHandler().postDelayed(mImageUrlRunnable,mCheckSpeedDelayed / 2);
			}
		}else{
			mRequestUrl = null;
			handImageUrl(finalUrl, width, height);
		}
	}

	private void handImageUrl(String url,int width,int height){
		if(mUrl == null && mUrl == url){
			return;
		}
		if(!TextUtils.isEmpty(url) && hasLoadNetImg != LOAD_STATE_FAIL && url.equals(mUrl)){
			return;
		}
		if(mImageView instanceof View){
			ViewGroup parent = (ViewGroup) ((View) mImageView).getParent();
			if(parent != null){
				parent.clearDisappearingChildren();
			}
		}
		mMaxProgress = 0;
		mBitmapTemp = null;
		hasShowImg = false;
		mInteriorDrawable = null;
		mImageView.clearAnimation();
		mImageView.setAnimation(null);
		BaseApp.getHandler().removeCallbacks(mFinishPreRunnable);
		BaseApp.getHandler().removeCallbacks(mFinishPostRunnable);
		mUrl = url;
		mWidth = width;
		mHeight = height;
		ImageBitmapInfo bitmapInfo = null;
		hasLoadNetImg = LOAD_STATE_NON;
		if(mUrl != null){
			bitmapInfo = mImageLoader.loadImage(mUrl,mInteriorImageCallback,mQuality,width,height);
		}
		if(bitmapInfo == null){
			bitmapInfo = new ImageBitmapInfo();
			if(width > 0){
				bitmapInfo.mWidth = width;
			}else{
				bitmapInfo.mWidth = mDefaultDrawable.getIntrinsicWidth();
			}
			if(height > 0){
				bitmapInfo.mHeight = height;
			}else{
				bitmapInfo.mHeight = mDefaultDrawable.getIntrinsicHeight();
			}
			if(height > 0 && width > 0){
				mInteriorDrawable = new InteriorDrawable(mDefaultDrawable, bitmapInfo);
				mImageView.setImageDrawable(mInteriorDrawable);
			}else{
				mImageView.setImageDrawable(new InteriorDrawable(mDefaultDrawable, bitmapInfo));
			}
		}else{
			if(bitmapInfo.mBitmap != null){
				hasLoadNetImg = LOAD_STATE_SUCCESS;
				hasShowImg = true;
				if(isRepeat){
					mInteriorDrawable = new InteriorDrawable(mDefaultDrawable, bitmapInfo);
					startPreAnim(bitmapInfo.mBitmap);
				}else{
					mInteriorDrawable = new InteriorDrawable(new BitmapDrawable(getContext().getResources(),bitmapInfo.mBitmap), bitmapInfo);
				}
			}else{
				mInteriorDrawable = new InteriorDrawable(mDefaultDrawable, bitmapInfo);
			}
			mImageView.setImageDrawable(mInteriorDrawable);
		}
		mImageView.invalidate();
	}
	
	private void setImageBitmap(Bitmap bitmap) {
		hasShowImg = bitmap != null;
		BitmapDrawable ditmapDrawable = new BitmapDrawable(getContext().getResources(), bitmap);
		if(mInteriorDrawable != null){
			mInteriorDrawable.replaceDrawable(ditmapDrawable);
			mImageView.setImageDrawable(mInteriorDrawable);
			mImageView.invalidate();
		}else{
			ImageBitmapInfo bitmapInfo = new ImageBitmapInfo();
			bitmapInfo.mWidth = bitmap.getWidth();
			bitmapInfo.mHeight = bitmap.getHeight();
			mInteriorDrawable = new InteriorDrawable(ditmapDrawable, bitmapInfo);
			mImageView.setImageDrawable(mInteriorDrawable);
			mImageView.invalidate();
		}
	}

	private boolean canStartAnim(boolean isPre){
		if(mImageView instanceof View && ((View) mImageView).getWindowToken() == null){
			return false;
		}
		if(mImageView.getVisibility() != View.VISIBLE){
			return false;
		}
		if(mInteriorDrawable != null){
			if(!mDefaultDrawable.equals(mInteriorDrawable.getSourceDrawable())){
				return false;
			}
		}
		if(hasLoadNetImg == LOAD_STATE_SUCCESS_THUMB){
			return false;
		}
		if(isPre){
			return mFinishPreAnimation != null;
		}else{
			return mFinishPostAnimation != null;
		}
	}
	
	private Runnable mFinishPreRunnable = new Runnable() {
		@Override
		public void run() {
			onAnimationEnd(mFinishPreAnimation);
		}
	};
	
	private Runnable mFinishPostRunnable = new Runnable() {
		@Override
		public void run() {
			onAnimationEnd(mFinishPostAnimation);
		}
	};
	
	private void startPreAnim(Bitmap bitmap){
		mBitmapTemp = bitmap;
		if(canStartAnim(true)){
			mImageView.startAnimation(mFinishPreAnimation);
			BaseApp.getHandler().postDelayed(mFinishPreRunnable, mFinishPreAnimation.getDuration() + 100);
		}else{
			startPostAnim();
		}
	}
	
	private void startPostAnim(){
		if(mBitmapTemp != null){
			boolean canStartAnim = canStartAnim(false);
			if(hasLoadNetImg == LOAD_STATE_PRE_THUMB){
				hasLoadNetImg = LOAD_STATE_SUCCESS_THUMB;
			}else{
				hasLoadNetImg = LOAD_STATE_SUCCESS;
			}
			setImageBitmap(mBitmapTemp);
			if(canStartAnim){
				mImageView.startAnimation(mFinishPostAnimation);
				BaseApp.getHandler().postDelayed(mFinishPostRunnable, mFinishPostAnimation.getDuration() + 100);
			}
			mBitmapTemp = null;
		}
	}
	
	private void updateProgress(final long p,final long m){
		BaseApp.getHandler().post(new Runnable() {
			@Override
			public void run() {
				mCurrentProgress = p;
				mMaxProgress = m;
				mImageView.invalidate();
			}
		});
	}
	
	private long mCurrentProgress;
	private long mMaxProgress;
	private Paint mPaint = new Paint();
	private Rect bounds = new Rect();

	public long getCurrentProgress(){
		return mCurrentProgress;
	}
	
	public long getMaxProgress(){
		return mMaxProgress;
	}

	public void drawProgress(Canvas canvas,int w,int h){
		if(mMaxProgress != 0){
//			int centreX = w / 2; //获取圆心的x坐标  
//			int centreY = h / 2; //获取圆心的x坐标  
//	        int radius = (centreX < centreY ? centreX : centreY) / 2; //圆环的半径  
//	        mPaint.setAntiAlias(true);  //消除锯齿 
//	        mPaint.setColor(0xBBDCDEDD); //设置圆环的颜色  
//	        mPaint.setStyle(Paint.Style.FILL_AND_STROKE); //设置空心  
//	        canvas.drawCircle(centreX, centreY, radius, mPaint); //画出圆环  
//	        
//	        mPaint.setColor(0xBBB5B7B6);  //设置进度的颜色 
//	        mPaint.setStyle(Paint.Style.FILL_AND_STROKE);  
//
//	        RectF oval = new RectF(centreX - radius, centreY - radius, centreX  
//	                + radius, centreY + radius);  //用于定义的圆弧的形状和大小的界限
//            canvas.drawArc(oval, 0, 360 * mCurrentProgress / mMaxProgress, true, mPaint);  //根据进度画圆弧  
			
			canvas.save();
			mPaint.setColor(0x20000000);
			int top = (int) (mCurrentProgress * 1f / mMaxProgress * h);
			canvas.drawRect(0, top, w, h, mPaint);
			canvas.restore();
		}
	}
	
	public boolean onDraw(Canvas canvas,int w,int h){
		boolean isHand = false;
		if(isDefaultHeavyDraw  && mDefaultDrawable != null && !hasShowImg()){
			if(mInteriorDrawable != null){
				isHand = mDefaultDrawable.equals(mInteriorDrawable.getSourceDrawable());
			}else{
				isHand = true;
			}
			if(isHand){
				mDefaultDrawable.copyBounds(bounds);
				mDefaultDrawable.setBounds(0, 0, w,h);
				canvas.save();
				mDefaultDrawable.draw(canvas);
				canvas.restore();
				mDefaultDrawable.setBounds(bounds);
			}
		}
		if(!isHand){
			mImageView.onSuperDraw(canvas);
		}
		if(isDefaultHeavyDraw){
			drawProgress(canvas, w, h);
		}
		return isHand;
	}
	
	@Override
	public void onAnimationStart(Animation animation) {
	}

	public String getCacheId() {
		return mImageLoader.getCacheId(mImageLoader.getId(mUrl), mQuality);
	}

	public String getThumbCacheId() {
		return mImageLoader.getCacheId(mImageLoader.getId(mImageLoader.getThumbImageUrl(mUrl)), mQuality);
	}
	
	@Override
	public void onAnimationEnd(Animation animation) {
		if(animation.equals(mFinishPreAnimation)){
			BaseApp.getHandler().removeCallbacks(mFinishPreRunnable);
			startPostAnim();
		}else{
			BaseApp.getHandler().removeCallbacks(mFinishPostRunnable);
			mImageView.setAnimation(null);
		}
	}

	@Override
	public void onAnimationRepeat(Animation animation) {
	}
	
	private class ScrollChangedListener implements OnScrollChangedListener{
		private View mView;
		private Runnable mScrollTimeOut = new Runnable() {
			@Override
			public void run() {
				mSpeed = 0;
				mLastY = -1;
				mLastTime = -1;
				mCheckSpeedDelayed = 100;
				mPostSetSpeed = DimensionsUtil.DIPToPX(80);
			}
		};
		
		private ScrollChangedListener(View view){
			mView = view;
		}
		
		@Override
		public void onScrollChanged() {
			int[] location = new int[2];
			mView.getLocationInWindow(location);
			mCheckSpeedDelayed = 100;
			if(mLastY == -1){
				mLastY = location[1];
				mLastTime = SystemClock.uptimeMillis();
				mSpeed = 0;
			}else{
				long timeDifference = SystemClock.uptimeMillis() - mLastTime;
				int differenceY = Math.abs(mLastY - location[1]);
				mLastTime = SystemClock.uptimeMillis();
				mLastY = location[1];
				if(differenceY > 0){
					mSpeed = differenceY;
					mCheckSpeedDelayed = (int) (timeDifference * 1.5f);
					if(timeDifference > 200){
						mPostSetSpeed = DimensionsUtil.DIPToPX(10);
					}else if(timeDifference > 150){
						mPostSetSpeed = DimensionsUtil.DIPToPX(20);
					}else if(timeDifference > 100){
						mPostSetSpeed = DimensionsUtil.DIPToPX(40);
					}else if(timeDifference > 50){
						mPostSetSpeed = DimensionsUtil.DIPToPX(60);
					}else{
						mPostSetSpeed = DimensionsUtil.DIPToPX(80);
					}
					if(mCheckSpeedDelayed > 500){
						mCheckSpeedDelayed = 500;
					}else if(mCheckSpeedDelayed < 100){
						mCheckSpeedDelayed = 100;
					}
				}
			}
			mView.removeCallbacks(mScrollTimeOut);
			mView.postDelayed(mScrollTimeOut, mCheckSpeedDelayed);
		}
	}
	
	private static class InteriorImageCallback implements ImageCallback{
		private WeakReference<AsyncImageViewManager> mImageViewManager;
		private InteriorImageCallback(AsyncImageViewManager imageViewManager){
			mImageViewManager = new WeakReference<AsyncImageViewManager>(imageViewManager);
		}
		
		@Override
		public void imageLoaded(Bitmap bitmap, String imageUrl, String cacheId) {
			AsyncImageViewManager imageViewManager = mImageViewManager.get();
			LogUtil.v("BaseImageLoader", "url="+imageUrl.hashCode()+" imageLoaded");
			if(imageViewManager == null || cacheId == null 
					|| (!cacheId.equals(imageViewManager.getCacheId()) && !cacheId.equals(imageViewManager.getThumbCacheId()))){
				return;
			}
			LogUtil.d("BaseImageLoader", "url="+imageUrl.hashCode()+" showImage=" + bitmap);
			if(bitmap != null){
				if(cacheId.equals(imageViewManager.getThumbCacheId())){
					imageViewManager.hasLoadNetImg = LOAD_STATE_PRE_THUMB;
				}
				imageViewManager.startPreAnim(bitmap);
			}else{
				imageViewManager.hasLoadNetImg = LOAD_STATE_FAIL;
			}
			imageViewManager.mImageView.onLoadedImg(bitmap, imageUrl, cacheId);
			imageViewManager.updateProgress(0, 0);
		}

		@Override
		public void onDownloadProgress(String imageUrl, long p, long m) {
			AsyncImageViewManager imageViewManager = mImageViewManager.get();
			if(imageViewManager == null || imageUrl == null || !imageUrl.equals(imageViewManager.mUrl)){
				return;
			}
			imageViewManager.updateProgress(p, m);
		}
	}
	
	public interface IImageView{
		public void setImageDrawable(Drawable bitmapDrawable);
	    public void setAnimation(Animation animation);
	    public void startAnimation(Animation animation);
	    public void clearAnimation();
	    public void onLoadedImg(Bitmap bitmap, String imageUrl, String imageId);
	    public void invalidate();
	    public void onSuperDraw(Canvas canvas);
	    public int getVisibility();
	}

	public class InteriorDrawable extends DecoratorDrawable{
		private ImageBitmapInfo mImageBitmapInfo;
		private boolean isDrawable;
		
		public InteriorDrawable(Drawable drawable,ImageBitmapInfo bitmapInfo) {
			super(drawable);
			mImageBitmapInfo = bitmapInfo;
			isDrawable = false;
		}

		@Override
		public void replaceDrawable(Drawable newDrawable) {
			super.replaceDrawable(newDrawable);
			isDrawable = false;
		}
		
		public boolean isDrawable() {
			return isDrawable;
		}
		
		@Override
		public int getIntrinsicWidth() {
			if(mImageBitmapInfo.mWidth > 0){
				return mImageBitmapInfo.mWidth;
			}
			return super.getIntrinsicWidth();
		}

		@Override
		public int getIntrinsicHeight() {
			if(mImageBitmapInfo.mHeight > 0){
				return mImageBitmapInfo.mHeight;
			}
			return super.getIntrinsicHeight();
		}

		@Override
		public int getMinimumWidth() {
			if(mImageBitmapInfo.mWidth > 0){
				return mImageBitmapInfo.mWidth;
			}
			return super.getMinimumWidth();
		}

		@Override
		public int getMinimumHeight() {
			if(mImageBitmapInfo.mHeight > 0){
				return mImageBitmapInfo.mHeight;
			}
			return super.getMinimumHeight();
		}
		
		@Override
		public void draw(Canvas canvas) {
			super.draw(canvas);
			isDrawable = true;
		}
	}
	
	public static class ImageInfo{
		public int mWidth;
		public int mHeight;
		public String mSrc;
		public boolean isVideo;
		
		public ImageInfo(String src,int width, int height) {
			this.mWidth = width;
			this.mHeight = height;
			this.mSrc = src;
		}
	}
	
	private class NetWorkListener extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			if(ApnUtil.isNetAvailable(context) && hasLoadNetImg == LOAD_STATE_FAIL){
				if(!TextUtils.isEmpty(mUrl)){
					setImageUrl(mUrl, mWidth, mHeight);
				}
			}
		}  
	}
}
