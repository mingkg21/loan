// Copyright 2012 Square, Inc.
package com.bk.android.time.ui.widget.calendar;

import java.util.Calendar;
import java.util.Date;

public class MonthDescriptor {
  private final int month;
  private final int year;
  private final Date date;

  public MonthDescriptor(int month, int year, Date date) {
    this.month = month;
    this.year = year;
    this.date = date;
  }

  public int getMonth() {
    return month;
  }

  public int getYear() {
    return year;
  }

  public Date getDate() {
    return date;
  }

  public boolean isCurrentMonth(Calendar today){
	return today.get(Calendar.YEAR) == year && today.get(Calendar.MONTH) == month;
  }
  
  @Override public String toString() {
    return "MonthDescriptor{"
        + "label='"
        + '\''
        + ", month="
        + month
        + ", year="
        + year
        + '}';
  }
}
