package com.bk.android.time.ui.widget;

import android.content.Context;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.bk.android.time.model.lightweight.UserInfoModel;
import com.bk.android.util.CommonUtil;

/** 验证码按钮
 * Created by mingkg21 on 2017/4/14.
 */

public class SecurityCodeButton extends Button implements View.OnClickListener{

    private String mPhone;
    private CountDownTimer mCountDownTimer;
    private String mDefaultText;
    private int mCountTime;
    private UserInfoModel mUserInfoModel;

    public SecurityCodeButton(Context context, AttributeSet attrs) {
        super(context, attrs);

        mDefaultText = getText().toString();

        mCountTime = 60;
        mCountDownTimer = new CountDownTimer(60 * 1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                mCountTime--;
                setText("剩余" + mCountTime + "秒");
            }

            @Override
            public void onFinish() {
                setText(mDefaultText);
                setEnabled(true);
                mCountTime = 60;
            }
        };

        mUserInfoModel = new UserInfoModel();

        setOnClickListener(this);
    }

    public void setPhone(String phone) {
        mPhone = phone;
    }

    @Override
    public void onClick(View v) {
        if (TextUtils.isEmpty(mPhone)) {
            Toast.makeText(getContext(), "请输入手机号码！", Toast.LENGTH_SHORT).show();
            return;
        }
        if (!CommonUtil.isPhoneNumber(mPhone)) {
            Toast.makeText(getContext(), "请输入正确的手机号码格式！", Toast.LENGTH_SHORT).show();
            return;
        }
        setEnabled(false);
        mUserInfoModel.getSecurityCode(mPhone);
        mCountDownTimer.start();
    }
}
