package com.bk.android.time.ui.widget;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

import com.bk.android.time.ui.widget.DrawerRelativeLayout.DrawerBackgroundView;
import com.bk.android.ui.widget.viewflow.ViewFlow;

public class DrawerViewFlow extends ViewFlow implements DrawerBackgroundView{
	public DrawerViewFlow(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	public Rect getSelectedRect() {
		View selectedView = getSelectedView();
		ItemSeletedView itemSeletedView = null;
		if(selectedView instanceof ItemSeletedView){
			itemSeletedView = (ItemSeletedView) selectedView;
		}else if(selectedView instanceof ViewGroup){
			itemSeletedView = findSeletedView((ViewGroup) selectedView);
		}
		if(itemSeletedView != null){
			return itemSeletedView.getSelectedViewRect();
		}
		return null;
	}

	private ItemSeletedView findSeletedView(ViewGroup viewGroup){
		for (int i = 0; i < viewGroup.getChildCount(); i++) {
			View view = viewGroup.getChildAt(i);
			if(view instanceof ItemSeletedView){
				return (ItemSeletedView) view;
			}else if(view instanceof ViewGroup){
				findSeletedView((ViewGroup) view);
			}
		}
		return null;
	}
	
	@Override
	public View getView() {
		return this;
	}
	
	public interface ItemSeletedView{
		public Rect getSelectedViewRect();
	}
}
