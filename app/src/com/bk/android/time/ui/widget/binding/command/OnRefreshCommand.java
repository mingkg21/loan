package com.bk.android.time.ui.widget.binding.command;

import android.view.View;

import com.bk.android.binding.command.BaseCommand;
import com.bk.android.ui.widget.pulltorefresh.PullToRefreshBase;

public abstract class OnRefreshCommand extends BaseCommand {
	
	@Override
	protected void onInvoke(View view, Object... args) {
		onRefresh((PullToRefreshBase<?>) view);
	}
	
	public abstract void onRefresh(final PullToRefreshBase<?> refreshView);
}
