package com.bk.android.time.ui.activiy;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;

import com.bk.android.time.app.AbsAppActivity;

import java.util.ArrayList;

import info.kuaicha.personalcreditreportengine.PersonalCreditReportEngine;
import info.kuaicha.personalcreditreportengine.PersonalCreditReportListener;

public class ActivityChannels {
	public static Intent getMainActivityIntent(Context context, ArrayList<Parcelable> parcelables) {
		Intent intent = ActivityChannels.getMainActivityIntent(context);
		if(parcelables != null) {
			intent.putExtra(AbsAppActivity.EXTRA_INTENT_ARR, parcelables);
		}
		return intent;
	}
	
	public static void openMainActivity(Context context, ArrayList<Parcelable> parcelables) {
		Intent intent = ActivityChannels.getMainActivityIntent(context);
		if(parcelables != null) {
			intent.putExtra(AbsAppActivity.EXTRA_INTENT_ARR, parcelables);
		}
		context.startActivity(intent);
	}
	
	public static Intent getMainActivityIntent(Context context) {
		Intent intent = new Intent(context,MainActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		return intent;
	}

	/** 打开反馈Activity
	 * @param context
	 */
	public static void openFeedbackActivity(Context context){
		FeedbackActivity.openActivity(context);
	}
	
	/** 打开关于Activity
	 * @param context
	 */
	public static void openAboutActivity(Context context){
		context.startActivity(new Intent(context, AboutActivity.class));
	}
	
	/** 打开设置Activity
	 * @param context
	 */
	public static void openSettingActivity(Context context){
		context.startActivity(new Intent(context, SettingActivity.class));
	}
	
	/** 打开注册Activity
	 * @param context
	 */
	public static void openRegisterActivity(Context context){
		RegisterActivity.openActivity(context);
	}
	/** 打开登录Activity
	 * @param context
	 */
	public static void openLoginActivity(Context context){
		LoginActivity.openActivity(context, LoginActivity.ACTION_TYPE_CAN_BACK);
	}

	/** 打开忘记密码Activity
	 * @param context
	 */
	public static void openForgetPasswordActivity(Context context){
		ForgetPasswordActivity.openActivity(context);
	}
	
	/** 打开编辑个人信息Activity
	 * @param context
	 */
	public static void openPersonInfoEditActivity(Context context){
		Intent intent = new Intent(context, PersonInfoEditActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(intent);
	}

	/** 打开通用webActivity
	 * @param context
	 */
	public static void openCommonWebActivity(Context context,String url){
		CommonWebActivity.openActivity(context, url);
	}

	public static void openWelcomeActiviy(Context context,ArrayList<Intent> intents) {
		WelcomeActiviy.openActivity(context, intents);
	}

	public static void openContactUsActivity(Context context){
		ContactUsActivity.openActivity(context);
	}

	public static void openCardProccessQueryActivity(Context context){
		CardProccessQueryActivity.openActivity(context);
	}


	public static void openMyApplyActivity(Context context){
		MyApplyActivity.openActivity(context);
	}

	public static void openMyInfoActivity(Context context){
		MyInfoActivity.openActivity(context);
	}

	public static void openMyMsgActivity(Context context){
		MyMsgActivity.openActivity(context);
	}

	public static void openBrowsingHistoryActivity(Context context){
		BrowsingHistoryActivity.openActivity(context);
	}

	public static void openPersonalCreditReportyActivity(Context context){
//		PersonalCreditReportEngine.getInstance().setCustomColor("#123125");
		PersonalCreditReportEngine.getInstance().getPersonalCreditReport(context, "", -1, new PersonalCreditReportListener() {
			@Override
			public void onSucceed(String s) {

			}

			@Override
			public void onFailed(BackType backType) {

			}
		});
	}
}
