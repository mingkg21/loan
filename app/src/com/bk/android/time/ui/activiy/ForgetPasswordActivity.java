package com.bk.android.time.ui.activiy;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.bk.android.assistant.R;
import com.bk.android.time.model.lightweight.ForgetPasswordViewModel;
import com.bk.android.time.ui.BaseAppActivity;

import gueei.binding.Binder.InflateResult;

/** 忘记界面
 * @author mingkg21
 * @date 2017年4月13日
 */
public class ForgetPasswordActivity extends BaseAppActivity {
	
	private ForgetPasswordViewModel mForgetPasswordViewModel;

	public static void openActivity(Context context) {
		context.startActivity(new Intent(context, ForgetPasswordActivity.class));
	}
	
	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		InflateResult result = inflateView(R.layout.uniq_forget_password_lay);
		mForgetPasswordViewModel = new ForgetPasswordViewModel(this, this);
		bindView(result, mForgetPasswordViewModel);
		setContentView(result.rootView);
		mForgetPasswordViewModel.onStart();
		setTitle(R.string.tag_forget_password);
	}

	@Override
	protected void onResume() {
		super.onResume();
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		mForgetPasswordViewModel.onRelease();
	}
}
