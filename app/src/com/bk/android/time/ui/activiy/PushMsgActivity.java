package com.bk.android.time.ui.activiy;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.bk.android.time.model.lightweight.PushMsgModel;
import com.bk.android.time.util.NoticeUtils;

/** 接收事件中转
 * @author mingkg21
 * @email mingkg21@gmail.com
 * @date 2016年12月28日
 */
public class PushMsgActivity extends Activity {

	public static Intent getPushMsgIntent(Context context, String resourceType, String resourceId) {
		Intent intent = new Intent(context, PushMsgActivity.class);
		intent.putExtra("resource_type", resourceType);
		intent.putExtra("resource_id", resourceId);
		return intent;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		String resourceType = getIntent().getStringExtra("resource_type");
		String resourceId = getIntent().getStringExtra("resource_id");

		Intent intent = null;
		if (intent != null) {
			try {
				int pushId = Integer.valueOf(resourceId);
				new PushMsgModel().addPushClick(pushId);
			} catch (Exception e){

			}
			startActivity(intent);
			NoticeUtils.cancel(this);
		}

		finish();

	}

}
