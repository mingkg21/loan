package com.bk.android.time.ui.activiy;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;

import com.bk.android.app.BaseActivity;
import com.bk.android.app.BaseApp;
import com.bk.android.assistant.R;
import com.bk.android.time.app.AbsAppActivity;
import com.bk.android.time.app.App;
import com.bk.android.time.app.observer.IAppContextObserver;
import com.bk.android.time.data.UserData;
import com.bk.android.time.model.BaseDataModel;
import com.bk.android.time.model.BaseDialogViewModel;
import com.bk.android.time.model.lightweight.SystemConfigModel;
import com.bk.android.time.ui.INetLoadView;
import com.bk.android.time.util.DialogUtil;

import java.util.ArrayList;

public class WelcomeActiviy extends BaseActivity implements INetLoadView{
	public static void openActivity(Context context, ArrayList<Intent> intents) {
		Intent intent = new Intent(context,WelcomeActiviy.class);
		intent.putExtra(AbsAppActivity.EXTRA_INTENT_ARR, intents);
		intent.addCategory(Intent.CATEGORY_LAUNCHER);
		intent.setAction(Intent.ACTION_MAIN);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(intent);
	}

	private Runnable mGotoMainRunnable = new Runnable() {
		@Override
		public void run() {
			if(!isFinishing() && !isGotoMain){
				try {
					ArrayList<Intent> intents = getIntent().getParcelableArrayListExtra(AbsAppActivity.EXTRA_INTENT_ARR);
					if(intents != null && !intents.isEmpty()){
						Intent intent = intents.remove(0);
						intent.putParcelableArrayListExtra(AbsAppActivity.EXTRA_INTENT_ARR, intents);
						startActivity(intent);
					}else{
						ActivityChannels.openMainActivity(this_, getIntent().getParcelableArrayListExtra(AbsAppActivity.EXTRA_INTENT_ARR));
					}
				} catch (Exception e) {
					ActivityChannels.openMainActivity(this_,null);
				}
				finish();
			}
			isGotoMain = true;
		}
	};

	private boolean isGotoMain;

	private BaseDataModel.SimpleLoadCallBack mSimpleLoadCallBack;
	private long mStartTime;

	@Override
	protected void onCreate(Bundle arg0) {
		boolean isEnterApp = App.getInstance().isEnterApp();
		super.onCreate(arg0);

		if (isEnterApp && getIntent() != null && (getIntent().getFlags() & Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT) != 0) {
			finish();
			return;
		}

//		setContentView(R.layout.uniq_welcome_lay);
//		boolean isNeedCheck = UserData.isShowNews() && "market_ezloan".equals(AppUtil.getUMengChannel(App.getInstance()));
//		if (isNeedCheck) {
//			mSimpleLoadCallBack = new BaseDataModel.SimpleLoadCallBack() {
//				@Override
//				public boolean onPostLoad(String key, int state) {
//					long time = System.currentTimeMillis() - mStartTime;
//					if (time > 1000) {
//						time = 0;
//					}
//					BaseApp.getHandler().postDelayed(mGotoMainRunnable, time);
//					return super.onPostLoad(key, state);
//				}
//			};
//			SystemConfigModel systemConfigModel = new SystemConfigModel();
//			systemConfigModel.addCallBack(mSimpleLoadCallBack);
//			systemConfigModel.getSystemConfig();
//			mStartTime = System.currentTimeMillis();
//		} else {
//			sleepGotoMain();
//		}

		if (App.getInstance().isMarketVersion()) {
			SystemConfigModel systemConfigModel = new SystemConfigModel();
			systemConfigModel.getSystemConfig();
		}
		setContentView(R.layout.uniq_welcome_lay);
		sleepGotoMain();

	}

	private void sleepGotoMain(){
		//设置动画保证界面显示之后才开始计时延时
		Animation animation = AnimationUtils.loadAnimation(this, android.R.anim.fade_in);
		animation.setDuration(0);
		animation.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {}
			@Override
			public void onAnimationRepeat(Animation animation) {}
			@Override
			public void onAnimationEnd(Animation animation) {
				BaseApp.getHandler().removeCallbacks(mGotoMainRunnable);

				int delayTime = 1000;
				if (UserData.isShowNews()) {
					delayTime = 2000;
				}

				BaseApp.getHandler().postDelayed(mGotoMainRunnable, delayTime);
			}
		});
		findViewById(R.id.welcome_root_lay).setAnimation(animation);
	}

	@Override
	public void finish() {
		super.finish();
		BaseApp.getHandler().removeCallbacks(mGotoMainRunnable);
	}

	@Override
	protected void dispatchNetworkChange(boolean isAvailable) {}

	@Override
	protected void onInitBroadcastReceiver() {}

	@Override
	public void showLoadView() {}

	@Override
	public void hideLoadView() {}

	@Override
	public void updateProgress(int progress, int maxProgress, int msgFormat) {}

	@Override
	public BaseDialogViewModel bindDialogViewModel(String type,Object viewModel, Object... extraParams) {
		return DialogUtil.bindDialogViewModel(this_, type, viewModel);
	}

	@Override
	public void addViewContextObservable(IAppContextObserver contextCallBack) {}

	@Override
	public void setLoadingViewTransparent(boolean isTransparent) {}

	@Override
	public void showNetSettingView(boolean canClean) {}

	@Override
	public void showNetSettingDialog(boolean canClean) {}

	@Override
	public void hideNetSettingDialog() {}

	@Override
	public void hideNetSettingView() {}

	@Override
	public void showRetryView(Runnable retryTask, boolean canClean) {}

	@Override
	public void hideRetryView() {}

	@Override
	public void showLoginView(Runnable retryTask) {}
}
