package com.bk.android.time.ui.activiy;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.bk.android.assistant.R;
import com.bk.android.time.ui.BaseAppActivity;
import com.bk.android.ui.widget.TabWidget;
import com.bk.android.ui.widget.viewpager.AbsFragmentPagerTabAdapter;
import com.bk.android.ui.widget.viewpager.SlideTabWidget;
import com.bk.android.ui.widget.viewpager.ViewPagerTabHost;
import com.bk.android.util.DimensionsUtil;

import java.util.ArrayList;

/**
 * Created by mingkg21 on 2017/4/8.
 */

public abstract class BaseTabActivity extends BaseAppActivity {

    protected TabWidget mTabWidget;
    protected ViewPagerTabHost mViewPagerTabHost;

    private ArrayList<String> mTabTitles = new ArrayList<>();

    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);

        View contentView = LayoutInflater.from(this).inflate(getLayoutResId(), null);
        setContentView(contentView);

        ArrayList<String> tabTitles = initTabTitles();
        if (tabTitles != null) {
            mTabTitles.addAll(tabTitles);
        }

        mViewPagerTabHost = (ViewPagerTabHost) contentView.findViewById(R.id.view_pager_tab);
        mViewPagerTabHost.setOffscreenPageLimit(1);
        mViewPagerTabHost.setAdapter(new TabPageIndicatorAdapter(this.getSupportFragmentManager()));

        mTabWidget = (TabWidget) contentView.findViewById(android.R.id.tabs);
        if (mTabWidget instanceof SlideTabWidget) {
            initSlideTabWidget((SlideTabWidget) mTabWidget);
        }
    }

    public void setCurrentTab(int index) {
        mViewPagerTabHost.setCurrentTab(index);
    }

    protected int getLayoutResId() {
        return R.layout.com_pager_tab_slide_lay;
    }

    protected void initSlideTabWidget(SlideTabWidget slideTabWidget) {
        slideTabWidget.initialize(false, new ColorDrawable(getResources().getColor(R.color.com_color_1)));
        slideTabWidget.setWidthMultiple(0.4f);
        slideTabWidget.setDividerDrawable(R.drawable.line);
        slideTabWidget.setShowDividers(SlideTabWidget.SHOW_DIVIDER_MIDDLE);
        slideTabWidget.setDividerPadding(DimensionsUtil.DIPToPX(12));
    }

    protected abstract ArrayList<String> initTabTitles();

    protected View initLeftTabView(String title) {
        View view = initTabView(title);
        return view;
    }

    protected View initRightTabView(String title) {
        View view = initTabView(title);
        return view;
    }

    protected View initTabView(String title) {
        TextView textView = new TextView(BaseTabActivity.this);
        textView.setText(title);
        textView.setGravity(Gravity.CENTER);
        textView.setTextSize(16);
        textView.setPadding(DimensionsUtil.DIPToPX(10), DimensionsUtil.DIPToPX(10), DimensionsUtil.DIPToPX(10), DimensionsUtil.DIPToPX(10));
        textView.setTextColor(getResources().getColorStateList(R.color.tab_text_common));
        return textView;
    }

    protected abstract Fragment getItemFragment(int position);

    private class TabPageIndicatorAdapter extends AbsFragmentPagerTabAdapter {

        public TabPageIndicatorAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return getItemFragment(position);
        }

        @Override
        public int getCount() {
            return mTabTitles.size();
        }

        @Override
        public View getIndicator(int position) {
            int count = getCount();
            if (count > 1) {
                if (position == 0) {
                    return initLeftTabView(mTabTitles.get(position));
                } else if (position == count - 1) {
                    return initRightTabView(mTabTitles.get(position));
                } else {
                    return initTabView(mTabTitles.get(position));
                }
            }  else {
                return initTabView(mTabTitles.get(position));
            }
        }

        @Override
        public String getTab(int position) {
            return "tag_" + position;
        }

        @Override
        public String getFragmentTag(int position) {
            return getTab(position);
        }
    }

}
