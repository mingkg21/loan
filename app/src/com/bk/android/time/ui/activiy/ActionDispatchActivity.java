package com.bk.android.time.ui.activiy;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;

import com.bk.android.time.app.AbsAppActivity;
import com.bk.android.time.app.App;
import com.bk.android.time.data.UserData;
import com.bk.android.util.LogUtil;
import com.bk.android.util.URLUtil;

import java.util.HashMap;
import java.util.Map;

/** 接收事件中转
 * @author mingkg21
 * @email mingkg21@gmail.com
 * @date 2015年8月6日
 */
public class ActionDispatchActivity extends AbsAppActivity {
	protected static final int REQUEST_CODE_LOGIN = 1001;

	private static final String URI_PREFIX = "bbzslaunch";
	private static final String URI_SCHEME = URI_PREFIX + "://";

	public static final String ACTION_NAME_INVITE_FAMILY = "invite_family";

	public static void openActivity(Context context, String event) {
		Intent intent = new Intent(context, ActionDispatchActivity.class);
		intent.putExtra("EXTRA_NAME_EVENT", event);
		context.startActivity(intent);
		LogUtil.v("start action dispatch: " + event);
	}

	public static String getInviteFamilyEvent(String inviteCode) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("invite_code", inviteCode);
		return getEvent(ACTION_NAME_INVITE_FAMILY, map);
	}

	public static String getEvent(String action, Map<String, String> values) {
		StringBuilder valueSB = new StringBuilder();
		if (values != null && !values.isEmpty()) {
			boolean isFirst = true;
			for(Map.Entry<String, String> entry : values.entrySet()) {
				if (!TextUtils.isEmpty(entry.getKey()) && !TextUtils.isEmpty(entry.getValue())) {
					if (!isFirst) {
						valueSB.append("&");
					} else {
						valueSB.append("?");
						isFirst = false;
					}
					valueSB.append(entry.getKey());
					valueSB.append("=");
					valueSB.append(entry.getValue());
				}
			}
		}

		return URI_SCHEME + action + valueSB.toString();
	}

	private Runnable mLoginRunnable;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		final Uri uri = getIntent().getData();
		String event = getIntent().getStringExtra("EXTRA_NAME_EVENT");
		if(uri == null && TextUtils.isEmpty(event)) {
			finish();
			return;
		}
		String url = uri != null ? uri.toString() : event;
		LogUtil.e("start : " + (uri != null ? "from web" : "fromw self") + "; data: " + url);

		String action = URLUtil.getHost(url);
		if (!URLUtil.getProtocol(url).equals(URI_PREFIX)) {
			finish();
			return;
		}

		LogUtil.e("action : " + action);

		HashMap<String, String> requestValue = URLUtil.getURLRequest(url);

		if(ACTION_NAME_INVITE_FAMILY.equals(action)) {//邀请家人
			final String inviteCode = requestValue.get("invite_code");
			LogUtil.e("inviteCode : " + inviteCode);
			if(!TextUtils.isEmpty(inviteCode)) {
				mLoginRunnable = new Runnable() {
					@Override
					public void run() {
						LogUtil.e("is login success!!!");
						finish();
						App.getHandler().post(new Runnable() {
							@Override
							public void run() {
							}
						});
					}
				};
				if(!UserData.isLogin()){
					startActivity(LoginActivity.getIntent(this, LoginActivity.ACTION_TYPE_ONLY_QUICK));
				}else{
					mLoginRunnable.run();
				}
				return;
			}
		}
		if(uri != null) {
			startActivity(new Intent(this, WelcomeActiviy.class));
			finish();
		}
	}
	
	@Override
	public void onUserLoginStateChange(boolean isLogin) {
		super.onUserLoginStateChange(isLogin);
		if(mLoginRunnable != null && isLogin){
			mLoginRunnable.run();
			mLoginRunnable = null;
		}
	}

	@Override
	protected boolean isMainActivity() {
		return true;
	}
	
	@Override
	protected int getStatusBarTintResource() {
		return 0;
	}
}
