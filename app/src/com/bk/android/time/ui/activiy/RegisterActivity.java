package com.bk.android.time.ui.activiy;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.bk.android.assistant.R;
import com.bk.android.time.data.UserData;
import com.bk.android.time.model.lightweight.LoginViewModel;
import com.bk.android.time.ui.BaseAppActivity;

import gueei.binding.Binder.InflateResult;

public class RegisterActivity extends BaseAppActivity{
	public static void openActivity(Context context) {
		Intent intent = new Intent(context, RegisterActivity.class);
		context.startActivity(intent);
	}
	
	private LoginViewModel mLoginViewModel;
	
	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		InflateResult result = inflateView(R.layout.uniq_login_lay);
		mLoginViewModel = new LoginViewModel(this, this, true);
		bindView(result, mLoginViewModel);
		setContentView(result.rootView);
		mLoginViewModel.onStart();
		setTitle(R.string.tag_register);
		setRightButton(getString(R.string.btn_text_login), 0, 0);
		setRightButtonEnabled(true);
	}
	
	@Override
	public boolean onTitleButtonClick(boolean isLeft) {
		if(!isLeft){
			ActivityChannels.openLoginActivity(this_);
			finish();
		}
		return super.onTitleButtonClick(isLeft);
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		mLoginViewModel.onRelease();
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
	}

	
	@Override
	public void finish() {
		if(UserData.isLogin()){
			setResult(RESULT_OK);
		}else{
			setResult(RESULT_CANCELED);
		}
		super.finish();
	}
	
	@Override
	public void onUserLoginStateChange(boolean isLogin) {
		super.onUserLoginStateChange(isLogin);
		if(isLogin){
			finish();
		}
	}
}