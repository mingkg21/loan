package com.bk.android.time.ui.activiy;

import gueei.binding.Binder.InflateResult;
import android.os.Bundle;

import com.bk.android.assistant.R;
import com.bk.android.time.model.BaseDialogViewModel;
import com.bk.android.time.model.lightweight.SettingViewModel;
import com.bk.android.time.ui.BaseAppActivity;
import com.bk.android.time.util.DialogUtil;

/** 设置界面
 * @author mingkg21
 * @email mingkg21@gmail.com
 * @date 2014年3月29日
 */
public class SettingActivity extends BaseAppActivity {
	
	private SettingViewModel mSettingViewModel;
	
	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		InflateResult result = inflateView(R.layout.uniq_setting_lay);
		mSettingViewModel = new SettingViewModel(this, this);
		bindView(result, mSettingViewModel);
		setContentView(result.rootView);
		mSettingViewModel.onStart();
		setTitle(R.string.user_center_setting);
	}

	@Override
	protected void onResume() {
		super.onResume();
		mSettingViewModel.onResume();
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		mSettingViewModel.onRelease();
	}
	
	@Override
	public BaseDialogViewModel bindDialogViewModel(String type,
			Object viewModel, Object... extraParams) {
		if(SettingViewModel.LOGOUT_TIP_DIALOG.equals(type)){
			return DialogUtil.createConfirmDialogNotT(this, R.string.tip_logout, null);
		}
		return super.bindDialogViewModel(type, viewModel, extraParams);
	}
}
