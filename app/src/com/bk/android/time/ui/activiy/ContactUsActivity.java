package com.bk.android.time.ui.activiy;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.ClipboardManager;
import android.view.View;

import com.bk.android.assistant.R;
import com.bk.android.time.ui.BaseAppActivity;
import com.bk.android.time.util.ToastUtil;

/** 联系我们界面
 * @author mingkg21
 * @email mingkg21@gmail.com
 * @date 2017年4月7日
 */
public class ContactUsActivity extends BaseAppActivity {

	public static void openActivity(Context context) {
		context.startActivity(new Intent(context, ContactUsActivity.class));
	}
	
	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setContentView(R.layout.uniq_contactus_lay);
		setTitle(R.string.setting_contact_us);

		findViewById(R.id.business_copy_tv).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				ClipboardManager cm = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
				cm.setText(getString(R.string.contact_us_business));
				ToastUtil.showLongToast(ContactUsActivity.this, "商务合作联系方式已复制！");
			}
		});
		
		findViewById(R.id.complain_copy_tv).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				ClipboardManager cm = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
				cm.setText(getString(R.string.contact_us_complain));
				ToastUtil.showLongToast(ContactUsActivity.this, "投诉与建议联系方式已复制！");
			}
		});
		
	}

}
