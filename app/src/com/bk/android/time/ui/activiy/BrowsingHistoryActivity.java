package com.bk.android.time.ui.activiy;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.bk.android.assistant.R;
import com.bk.android.time.model.lightweight.BrowsingHistoryViewModel;
import com.bk.android.time.ui.BaseAppActivity;

/** 浏览记录
 * @author mingkg21
 * @date 2017年04月10日
 *
 */
public class BrowsingHistoryActivity extends BaseAppActivity {
	
	public static void openActivity(Context context) {
		Intent intent = new Intent(context, BrowsingHistoryActivity.class);
		context.startActivity(intent);
	}

	private BrowsingHistoryViewModel mBrowsingHistoryViewModel;

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);

		mBrowsingHistoryViewModel = new BrowsingHistoryViewModel(this, this);
		setContentView(bindView(R.layout.uniq_browsing_history_lay, mBrowsingHistoryViewModel));

		setTitle(R.string.tag_browsing_history);

		mBrowsingHistoryViewModel.onStart();

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if(mBrowsingHistoryViewModel != null) {
			mBrowsingHistoryViewModel.onRelease();
		}
	}
}
