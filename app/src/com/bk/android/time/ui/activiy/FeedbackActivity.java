package com.bk.android.time.ui.activiy;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.bk.android.assistant.R;
import com.bk.android.time.model.lightweight.FeedbackViewModel;
import com.bk.android.time.ui.BaseAppActivity;

public class FeedbackActivity extends BaseAppActivity {
	public static void openActivity(Context context) {
		context.startActivity(new Intent(context,FeedbackActivity.class));
	}

	private FeedbackViewModel mFeedbackViewModel;
	
	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		setTitle(R.string.tag_feedback);
		mFeedbackViewModel = new FeedbackViewModel(this, this);
		setContentView(bindView(R.layout.uniq_feedback_lay, mFeedbackViewModel));
		mFeedbackViewModel.onStart();
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		mFeedbackViewModel.onRelease();
	}
	
}
