package com.bk.android.time.ui.activiy;

import android.content.Intent;
import android.os.Bundle;

import com.bk.android.assistant.R;
import com.bk.android.time.model.BaseDialogViewModel;
import com.bk.android.time.model.lightweight.PersonInfoEditViewModel;
import com.bk.android.time.ui.BaseAppActivity;
import com.bk.android.time.util.DialogUtil;

/** 个人信息编辑
 * @author mingkg21
 * @email mingkg21@gmail.com
 * @date 2014年4月21日
 */
public class PersonInfoEditActivity extends BaseAppActivity {
	
	private PersonInfoEditViewModel mPersonInfoEditViewModel;

	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		mPersonInfoEditViewModel = new PersonInfoEditViewModel(this, this);
		setContentView(bindView(R.layout.uniq_person_info_edit, mPersonInfoEditViewModel));
		setTitle(R.string.tag_personal_edit);
		mPersonInfoEditViewModel.onStart();
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(mPersonInfoEditViewModel != null) {
			mPersonInfoEditViewModel.onActivityResult(this_,requestCode, resultCode, data);
		}
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		if(isFinishing()){
			mPersonInfoEditViewModel.onRelease();
		}
	}

	@Override
	public BaseDialogViewModel bindDialogViewModel(String type,
												   Object viewModel, Object... extraParams) {
		if(PersonInfoEditViewModel.LOGOUT_TIP_DIALOG.equals(type)){
			return DialogUtil.createConfirmDialogNotT(this, R.string.tip_logout, null);
		}
		return super.bindDialogViewModel(type, viewModel, extraParams);
	}
}
