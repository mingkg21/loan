package com.bk.android.time.ui.activiy;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.bk.android.time.model.BaseDataModel;
import com.bk.android.time.model.lightweight.NetEaseNewskListModel;
import com.bk.android.time.ui.BaseAppActivity;
import com.bk.android.time.ui.fragment.CommonWebFragment;
import com.bk.android.time.util.TestUtil;

public class NetEaseNewsDetailActivity extends BaseAppActivity {
	
	public static void openActivity(Context context, String id){
		Intent intent = new Intent(context,NetEaseNewsDetailActivity.class);
		intent.putExtra(CommonWebFragment.EXTRA_NAME_URL, id);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(intent);
	}

	private BaseDataModel.SimpleLoadCallBack mSimpleLoadCallBack;
	private NetEaseNewskListModel mNetEaseNewskListModel;
	
	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);

		setTitle("资讯详情");

		String url = getIntent().getStringExtra(CommonWebFragment.EXTRA_NAME_URL);

//		mSimpleLoadCallBack = new BaseDataModel.SimpleLoadCallBack() {
//			@Override
//			public boolean onSuccess(String key, Object data, DataResult<?> dataSource) {
//
//				if (mNetEaseNewskListModel.isGetDetailkey(key)) {
//					NetEaseNewsData netEaseNewsData = (NetEaseNewsData) data;
//
//					CommonWebFragment webFragment = new CommonWebFragment();
//					Bundle args = new Bundle();
//					args.putString(CommonWebFragment.EXTRA_NAME_HTML_DATA, netEaseNewsData.getData().getBody());
//					webFragment.setArguments(args);
//					webFragment.setWebTitle(false);
//					getSupportFragmentManager().beginTransaction().add(getContentLayId(), webFragment).commit();
//				}
//
//				return super.onSuccess(key, data, dataSource);
//			}
//		};
//
//		mNetEaseNewskListModel = new NetEaseNewskListModel();
//		mNetEaseNewskListModel.addCallBack(mSimpleLoadCallBack);
//		mNetEaseNewskListModel.getDetail(url);

		setTouchBackEnabled(false);

		String data = "<html><header><meta name=\"viewport\" content=\"width=device-width\"></header><body>" + TestUtil.getInstance().getData(url) + "</body></html?";

		CommonWebFragment webFragment = new CommonWebFragment();
		Bundle args = new Bundle();
		args.putString(CommonWebFragment.EXTRA_NAME_HTML_DATA, data);
		webFragment.setArguments(args);
		webFragment.setWebTitle(false);
		getSupportFragmentManager().beginTransaction().add(getContentLayId(), webFragment).commit();
		
	}
}
