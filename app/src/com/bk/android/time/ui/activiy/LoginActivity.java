package com.bk.android.time.ui.activiy;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.TextView;

import com.bk.android.assistant.R;
import com.bk.android.time.data.UserData;
import com.bk.android.time.ui.fragment.LoginFragment;

import java.util.ArrayList;

public class LoginActivity extends BaseTabActivity {
	
	public static final int ACTION_TYPE_CAN_BACK = 1;
	public static final int ACTION_TYPE_ONLY_QUICK = 2;
	
	public static Intent getIntent(Context context, int action) {
		Intent intent = new Intent(context, LoginActivity.class);
		intent.putExtra("extra_name_action_type", action);
		return intent;
	}
	
	public static void openActivity(Context context, int action) {
		context.startActivity(getIntent(context, action));
	}
	
	private int mActionType;
	
	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		
		mActionType = getIntent().getIntExtra("extra_name_action_type", ACTION_TYPE_ONLY_QUICK);
		
		setTitle(R.string.tag_login);
		
//		if(mActionType == ACTION_TYPE_ONLY_QUICK) {
//			setLeftButtonEnabled(false);
//		}
		setRightButton(getString(R.string.btn_text_register), 0, 0);
		setRightButtonEnabled(true);
	}

	@Override
	public boolean onTitleButtonClick(boolean isLeft) {
		if(!isLeft){
			ActivityChannels.openRegisterActivity(this_);
		}
		return super.onTitleButtonClick(isLeft);
	}

	@Override
	protected int getLayoutResId() {
		return R.layout.com_pager_tab_lay;
	}

	@Override
	protected ArrayList<String> initTabTitles() {
		ArrayList<String> tmep = new ArrayList<>();
		tmep.add("短信快捷登录");
		tmep.add("账号密码登录");
		return tmep;
	}

	@Override
	protected View initTabView(String title) {
		View view = super.initTabView(title);
		if (view instanceof TextView) {
			((TextView) view).setTextColor(getResources().getColorStateList(R.color.tab_text));
		}
		return view;
	}

	@Override
	protected View initLeftTabView(String title) {
		View view = super.initLeftTabView(title);
		view.setBackgroundResource(R.drawable.tab_bg_left);
		return view;
	}

	@Override
	protected View initRightTabView(String title) {
		View view = super.initRightTabView(title);
		view.setBackgroundResource(R.drawable.tab_bg_right);
		return view;
	}

	@Override
	protected Fragment getItemFragment(int position) {
		LoginFragment loginFragment =  new LoginFragment();
		loginFragment.setBySmsLogin(position == 0);
		return loginFragment;
	}

	@Override
	public void onUserLoginStateChange(boolean isLogin) {
		super.onUserLoginStateChange(isLogin);
		if(isLogin){
			finish();
		}
	}

	@Override
	public void finish() {
		if(UserData.isLogin()){
			setResult(RESULT_OK);
		}else{
			setResult(RESULT_CANCELED);
		}
		super.finish();
	}
}