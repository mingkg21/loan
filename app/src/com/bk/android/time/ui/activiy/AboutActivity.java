package com.bk.android.time.ui.activiy;

import android.os.Bundle;
import android.widget.TextView;

import com.bk.android.assistant.R;
import com.bk.android.time.ui.BaseAppActivity;
import com.bk.android.util.AppUtil;

/** 关于界面
 * @author mingkg21
 * @email mingkg21@gmail.com
 * @date 2014年3月29日
 */
public class AboutActivity extends BaseAppActivity {
	
	private int mClickCount;
	private long mLastRecordTime;

	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setContentView(R.layout.uniq_about_lay);
		setTitle(R.string.setting_about);
		
		TextView versionTV = (TextView) findViewById(R.id.tv_version);
		versionTV.setText(getString(R.string.about_version, AppUtil.getApkVersionName(getApplicationContext())));

		TextView contentTV = (TextView) findViewById(R.id.about_content_tv);
		contentTV.setText(getString(R.string.about_content, "\"" + getString(R.string.app_name) + "\""));

		TextView copyrightTV = (TextView) findViewById(R.id.about_copyright_tv);
		copyrightTV.setText(getString(R.string.about_copyright, getString(R.string.copyright_name)));
		
//		findViewById(R.id.about_app_name).setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View arg0) {
//				long time = System.currentTimeMillis();
//				if(mLastRecordTime == 0) {
//					mLastRecordTime = time;
//				}
//				if((time - mLastRecordTime) < 1000) {
//					mClickCount++;
//					mLastRecordTime = time;
//					if(mClickCount >= 5) {
//						Preferences.getInstance().setSwitchNormalServerVisibility();
//					}
//				} else {
//					mClickCount = 0;
//					mLastRecordTime = 0;
//				}
//			}
//		});

//		if (BuildConfig.IS_DEBUG) {
//			setRightButton("提交日志", -1, -1);
//			setRightButtonEnabled(true);
//		}
	}

}
