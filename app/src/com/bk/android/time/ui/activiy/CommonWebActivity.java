package com.bk.android.time.ui.activiy;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.bk.android.time.data.UserData;
import com.bk.android.time.ui.BaseAppActivity;
import com.bk.android.time.ui.fragment.CommonWebFragment;

public class CommonWebActivity extends BaseAppActivity {
	
	public static void openActivity(Context context,String url){
		Intent intent = new Intent(context,CommonWebActivity.class);
		intent.putExtra(CommonWebFragment.EXTRA_NAME_URL, url);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(intent);
	}
	
	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		CommonWebFragment webFragment = new CommonWebFragment();
		Bundle args = new Bundle();  
		String url = getIntent().getStringExtra(CommonWebFragment.EXTRA_NAME_URL);
		args.putString(CommonWebFragment.EXTRA_NAME_URL,url);
		webFragment.setArguments(args); 
		webFragment.setWebTitle(true);
		getSupportFragmentManager().beginTransaction().add(getContentLayId(), webFragment).commit();
		
		setTouchBackEnabled(false);
		
	}

	@Override
	public boolean onTitleButtonClick(boolean isLeft) {
		if(!isLeft){
			Runnable runnable = new Runnable() {
				@Override
				public void run() {
				}
			};
			if(!UserData.isLogin()){
				showLoginView(runnable);
			}else{
				runnable.run();
			}
		}
		return super.onTitleButtonClick(isLeft);
	}
}
