package com.bk.android.time.ui.activiy;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.TextView;

import com.bk.android.assistant.R;
import com.bk.android.time.ui.fragment.MyIdentityFragment;
import com.bk.android.time.ui.fragment.MyInfoFragment;
import com.bk.android.time.ui.fragment.MyPropertyFragment;

import java.util.ArrayList;

/** 我的信息
 * Created by mingkg21 on 2017/4/10.
 */

public class MyInfoActivity extends BaseTabActivity {

    public static void openActivity(Context context) {
        context.startActivity(new Intent(context, MyInfoActivity.class));
    }

    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setTitle(R.string.user_center_my_info);

    }

    @Override
    protected int getLayoutResId() {
        return R.layout.com_pager_tab_lay;
    }

    @Override
    protected ArrayList<String> initTabTitles() {
        ArrayList<String> tmep = new ArrayList<>();
        tmep.add("个人信息");
        tmep.add("身份信息");
        tmep.add("房产信息");
        return tmep;
    }

    @Override
    protected View initTabView(String title) {
        View view = super.initTabView(title);
        view.setBackgroundResource(R.drawable.tab_bg_middle);
        if (view instanceof TextView) {
            ((TextView) view).setTextColor(getResources().getColorStateList(R.color.tab_text));
        }
        return view;
    }

    @Override
    protected View initLeftTabView(String title) {
        View view = super.initLeftTabView(title);
        view.setBackgroundResource(R.drawable.tab_bg_left);
        return view;
    }

    @Override
    protected View initRightTabView(String title) {
        View view = super.initRightTabView(title);
        view.setBackgroundResource(R.drawable.tab_bg_right);
        return view;
    }

    @Override
    protected Fragment getItemFragment(int position) {
        if (position == 0) {
            return new MyInfoFragment();
        } else if (position == 1) {
            return new MyIdentityFragment();
        }
        return new MyPropertyFragment();
    }

}
