package com.bk.android.time.ui.activiy;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.bk.android.assistant.R;
import com.bk.android.time.data.UserData;
import com.bk.android.time.ui.BaseAppActivity;
import com.bk.android.time.ui.fragment.CreditCardFragment;
import com.bk.android.time.ui.fragment.HomeFragment;
import com.bk.android.time.ui.fragment.LoanFragment;
import com.bk.android.time.ui.fragment.NetEaseNewsFragment;
import com.bk.android.time.ui.fragment.PersonalInfoFragment;
import com.bk.android.time.update.AppUpdate;
import com.bk.android.ui.widget.viewpager.AbsFragmentPagerTabAdapter;
import com.bk.android.ui.widget.viewpager.SlideTabWidget;
import com.bk.android.ui.widget.viewpager.ViewPagerTabHost;

public class MainActivity extends BaseAppActivity {

	public static final String EXTRA_CURRENT_ID = "EXTRA_CURRENT_ID";

	public static int ID_HOME = 0;
	public static int ID_LOAN = 1;
	public static int ID_CREDIT = 2;
	public static int ID_MY = 3;

	private int mCurrentID;
	private ViewPagerTabHost mViewPager;
	private SlideTabWidget mSlideTabWidget;

	private Runnable mUpdateRunnable = new Runnable() {
		@Override
		public void run() {
			if(!isFinishing()){
				AppUpdate.checkUpdate();
			}
		}
	};

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		findViewById(getContentLayId()).setBackgroundResource(R.color.transparent);
		setContentView(R.layout.uniq_main_lay);

//		new PushMsgModel().getPushMsg();

		if (UserData.isShowNews()) {
			ID_MY = 1;
		}

		mViewPager = (ViewPagerTabHost) findViewById(R.id.main_content_lay);
		mViewPager.setCanSlide(false);
		mViewPager.setOffscreenPageLimit(4);
		mViewPager.setOnPageChangeListener(new OnPageChangeListener() {
			@Override
			public void onPageSelected(int arg0) {
				setFragment(arg0);
			}

			@Override
			public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
				if(positionOffset == 0){
					for (int i = 0; i < 4; i++) {
						Fragment fragment = getSupportFragmentManager().findFragmentByTag(createTag(i));
						if(fragment != null && fragment.getView() != null){
							if(i != position){
								fragment.getView().setVisibility(View.GONE);
							}else{
								fragment.getView().setVisibility(View.VISIBLE);
							}
						}
					}
				}else{
					Fragment fragment = getSupportFragmentManager().findFragmentByTag(createTag(position));
					if(fragment != null && fragment.getView() != null){
						fragment.getView().setVisibility(View.VISIBLE);
					}
					fragment = getSupportFragmentManager().findFragmentByTag(createTag(position + 1));
					if(fragment != null && fragment.getView() != null){
						fragment.getView().setVisibility(View.VISIBLE);
					}
				}
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {}
		});
		mSlideTabWidget = (SlideTabWidget)findViewById(android.R.id.tabs);
		mSlideTabWidget.initialize(true, new ColorDrawable(Color.TRANSPARENT));
		if(hasSmartBar()){
			mSlideTabWidget.setVisibility(View.GONE);
		}

		mCurrentID = getIntent().getIntExtra(EXTRA_CURRENT_ID, ID_HOME);
		if(arg0 != null){
			mCurrentID = arg0.getInt("mCurrentID",ID_HOME);
		}
		mViewPager.setAdapter(new Adapter(getSupportFragmentManager()));

		if(mCurrentID < 0){
			mCurrentID = ID_HOME;
		}

		mViewPager.setCurrentTab(mCurrentID);
		setFragment(mCurrentID);

		mViewPager.postDelayed(mUpdateRunnable, 2000);

	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	public void finish() {
		super.finish();
		mViewPager.removeCallbacks(mUpdateRunnable);
	}

	@Override
	protected void onStop() {
		super.onStop();
		if(isFinishing()){
			mViewPager.releaseRes();
		}
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		if(intent != null){
			int currentID = intent.getIntExtra(EXTRA_CURRENT_ID, -1);
			if(currentID >= 0){
				mCurrentID = currentID;
				mViewPager.setCurrentTab(currentID);
			}
		}
	}

	@Override
	public void onUserLoginStateChange(boolean isLogin) {
		super.onUserLoginStateChange(isLogin);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		try {//FragmentManagerImpl 可能报错
			super.onSaveInstanceState(outState);
			outState.putInt("mCurrentID", mCurrentID);
		} catch (Exception e) {}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if(hasSmartBar()){
			MenuItem item = menu.add(0, ID_HOME, menu.size(), getTagTipById(ID_HOME));
			item.setIcon(getTagImgById(ID_HOME));
			item.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

			item = menu.add(0, ID_LOAN, menu.size(),  getTagTipById(ID_LOAN));
			item.setIcon(getTagImgById(ID_LOAN));
			item.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

			item = menu.add(0, ID_CREDIT, menu.size(), getTagTipById(ID_CREDIT));
			item.setIcon(getTagImgById(ID_CREDIT));
			item.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

			item = menu.add(0, ID_MY, menu.size(), getTagTipById(ID_MY));
			item.setIcon(getTagImgById(ID_MY));
			item.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
			return super.onCreateOptionsMenu(menu) || true;
		}
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(item != null){
			mViewPager.setCurrentTab(item.getItemId());
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected boolean isTitleBackBtnEnabled() {
		return false;
	}

	private String getTagTipById(int id){
		if(id == ID_CREDIT){
			return getString(R.string.tag_credit_card);
		} else if(id == ID_HOME){
			return getString(R.string.tag_home);
		} else if(id == ID_MY){
			return getString(R.string.tag_personal);
		} else if(id == ID_LOAN){
			return getString(R.string.tag_loan);
		}
		return getString(R.string.app_name);
	}

	private int getTagImgById(int id){
		if(id == ID_HOME){
			return R.drawable.ic_indicator_home;
		} else if(id == ID_CREDIT){
			return R.drawable.ic_indicator_credit;
		} else if(id == ID_MY){
			return R.drawable.ic_indicator_my;
		} else if(id == ID_LOAN){
			return R.drawable.ic_indicator_loan;
		}
		return 0;
	}

	private void setFragment(int id){
		mCurrentID = id;
	}

	private String createTag(int id){
		return "main_tag_"+id;
	}

	private Fragment newFragmentById(int id){
		Fragment fragment = null;
		if(id == ID_HOME){
			if (UserData.isShowNews()) {
				fragment = new NetEaseNewsFragment();
			} else {
				fragment = new HomeFragment();
			}
		} else if(id == ID_CREDIT){
			fragment = new CreditCardFragment();
		} else if(id == ID_MY){
			fragment = new PersonalInfoFragment();
		} else if(id == ID_LOAN){
			fragment = new LoanFragment();
		}
		return fragment;
	}

	@Override
	protected boolean isMainActivity() {
		return true;
	}

	private class Adapter extends AbsFragmentPagerTabAdapter{
		public Adapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			return newFragmentById(position);
		}

		@Override
		public View getIndicator(int position) {
			View contentView = getLayoutInflater().inflate(R.layout.uniq_main_indicator, null);
			TextView tv = (TextView) contentView.findViewById(R.id.main_indicator_tv);
			tv.setCompoundDrawablesWithIntrinsicBounds(0, getTagImgById(position), 0, 0);
			tv.setText(getTagTipById(position));
			return contentView;
		}

		@Override
		public String getTab(int position) {
			return createTag(position);
		}

		@Override
		public String getFragmentTag(int position) {
			return createTag(position);
		}

		@Override
		public int getCount() {
			return ID_MY + 1;
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	protected boolean filterFragmentActivityResult(Fragment fragment) {
		return super.filterFragmentActivityResult(fragment);
	}
}
