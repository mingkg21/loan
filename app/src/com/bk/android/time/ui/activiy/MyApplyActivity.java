package com.bk.android.time.ui.activiy;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.bk.android.assistant.R;
import com.bk.android.time.ui.fragment.MyApplyCreditCardFragment;
import com.bk.android.time.ui.fragment.MyApplyLoanFragment;

import java.util.ArrayList;

/** 我的申请
 * Created by mingkg21 on 2017/4/8.
 */

public class MyApplyActivity extends BaseTabActivity {

    public static void openActivity(Context context) {
        context.startActivity(new Intent(context, MyApplyActivity.class));
    }

    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setTitle(R.string.user_center_my_apply);
    }

    @Override
    protected ArrayList<String> initTabTitles() {
        ArrayList<String> tmep = new ArrayList<>();
        tmep.add("贷款");
        tmep.add("信用卡");
        return tmep;
    }

    @Override
    protected Fragment getItemFragment(int position) {
        if (position == 0) {
            return new MyApplyLoanFragment();
        }
        return new MyApplyCreditCardFragment();
    }

    @Override
    public boolean onTitleButtonClick(boolean isLeft) {
        if(!isLeft){
            ActivityChannels.openRegisterActivity(this_);
        }
        return super.onTitleButtonClick(isLeft);
    }



}
