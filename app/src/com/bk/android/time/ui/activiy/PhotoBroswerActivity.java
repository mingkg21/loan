package com.bk.android.time.ui.activiy;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;

import com.bk.android.assistant.R;
import com.bk.android.binding.command.OnItemClickCommand;
import com.bk.android.time.app.App;
import com.bk.android.time.model.BaseDataViewModel;
import com.bk.android.time.ui.BaseAppActivity;
import com.bk.android.time.ui.ILoadView;
import com.bk.android.time.util.AppFileUtil;
import com.bk.android.time.util.ToastUtil;

import java.io.File;
import java.util.ArrayList;

import gueei.binding.collections.ArrayListObservable;
import gueei.binding.observables.BooleanObservable;
import gueei.binding.observables.StringObservable;

public class PhotoBroswerActivity extends BaseAppActivity {

	public static void openActivity(Context context, int requestCode, int totalSize, int currentSize) {
		Intent intent = new Intent(context, PhotoBroswerActivity.class);
		intent.putExtra("extra_name_total_size", totalSize);
		intent.putExtra("extra_name_current_size", currentSize);
		((Activity) context).startActivityForResult(intent, requestCode);
	}
	
	private PhotoBroswerViewModel mPhotoBroswerViewModel;
	private View mContentView;

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);

		Intent intent = getIntent();
		int currentSize = intent.getIntExtra("extra_name_current_size", 0);
		int totalSize = intent.getIntExtra("extra_name_total_size", 0);
		mPhotoBroswerViewModel = new PhotoBroswerViewModel(this, this, totalSize, currentSize);
		mContentView = bindView(R.layout.uniq_photo_broswer, mPhotoBroswerViewModel);
		setContentView(mContentView);
		mPhotoBroswerViewModel.onStart();

		setTitle(totalSize, currentSize);

		setRightButtonEnabled(true);
		setRightButton(getString(R.string.btn_text_confirm), -1, -1);
	}

	public void setTitle(int totalSize, int currentSize) {
		super.setTitle("选择图片（" + currentSize + "/" + totalSize + "）");
	}

	@Override
	public boolean onTitleButtonClick(boolean isLeft) {
		if (!isLeft) {
			if (mPhotoBroswerViewModel != null) {
				if (mPhotoBroswerViewModel.getSelectArr().size() > 0) {
					Intent intent = new Intent();
					intent.putStringArrayListExtra("value", mPhotoBroswerViewModel.getSelectArr());
					setResult(RESULT_OK, intent);
				}
			}
			finish();
			return true;
		}
		return super.onTitleButtonClick(isLeft);
	}

	@Override
	public void finish() {
		if (mPhotoBroswerViewModel != null) {
			mPhotoBroswerViewModel.onRelease();
		}
		super.finish();
	}

	public class PhotoBroswerViewModel extends BaseDataViewModel {

		public final ArrayListObservable<ImgItemViewModel> bImgItems = new ArrayListObservable<ImgItemViewModel>(ImgItemViewModel.class);

		private ArrayList<String> mBitmapInfos = new ArrayList<>();

		private int mTotalSize;
		private int mCurrentSize;
		private ArrayList<String> mSelectArr = new ArrayList<>();

		public PhotoBroswerViewModel(Context context, ILoadView loadView, int totalSize, int currentSize) {
			super(context, loadView);
			mTotalSize = totalSize;
			mCurrentSize = currentSize;
		}

		@Override
		public void onRelease() {

		}

		@Override
		public void onStart() {
			showLoadView();
			mLoadDataThread.start();
		}

		private ArrayList<String> getSelectArr() {
			return mSelectArr;
		}

		Thread mLoadDataThread = new Thread(){
			@Override
			public void run() {
				try {
					long time = System.currentTimeMillis();
					ArrayList<String> tempList = getImages();
					mBitmapInfos.addAll(tempList);
				} catch (Exception e) {
					e.printStackTrace();
				}
				App.getInstance().getHandler().post(new Runnable() {
					@Override
					public void run() {
						ArrayListObservable<ImgItemViewModel> tmpList = new ArrayListObservable<ImgItemViewModel>(ImgItemViewModel.class);
						for (String path : mBitmapInfos) {
							tmpList.add(new ImgItemViewModel(path));
						}
						bImgItems.setAll(tmpList);
						hideLoadView();
					}
				});
			}
		};

		private ArrayList<String> getImages() throws Exception {
			ArrayList<String> tempList = new ArrayList<String>();
			ContentResolver contentResolver = App.getInstance().getContentResolver();
			Cursor cursor = contentResolver.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
					new String[]{MediaStore.Images.Media.DATA, MediaStore.Images.Media.DATE_TAKEN, MediaStore.Images.Media.LONGITUDE, MediaStore.Images.Media.LATITUDE, MediaStore.Images.Media.WIDTH, MediaStore.Images.Media.HEIGHT, MediaStore.Images.Media.SIZE},
					MediaStore.Images.Media.DATA + " NOT LIKE '%" + AppFileUtil.getAppPath()+ "%' AND (" + MediaStore.Images.Media.MIME_TYPE + "=? OR " + MediaStore.Images.Media.MIME_TYPE + "=?)",
					new String[] {"image/jpeg", "image/png"}, MediaStore.Images.Media.DATE_TAKEN + " desc");
			if(cursor != null){
				while (cursor.moveToNext()) {
					try {
						String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
						if(!TextUtils.isEmpty(path)){
							File file = new File(path);
							if(file.exists()){
								tempList.add(path);
							}
						}
					} catch (Exception e) {e.printStackTrace();}
				}
				cursor.close();
			}
			return tempList;
		}

		public final OnItemClickCommand bOnItemClickCommand = new OnItemClickCommand() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				ImgItemViewModel item = (ImgItemViewModel) parent.getItemAtPosition(position);
				if(item != null){
					boolean isSelected = item.bIsSelect.get();
					if (isSelected) {
						item.bIsSelect.set(false);
						mSelectArr.remove(item.mDataSource);
					} else {
						int selectSize = mSelectArr.size();
						if ((mCurrentSize + selectSize) >= mTotalSize) {
							ToastUtil.showToast(getContext(), getString(R.string.photo_select_limit, (mTotalSize - mCurrentSize)));
							return;
						}
						item.bIsSelect.set(true);
						mSelectArr.add(item.mDataSource);
						setTitle(mTotalSize, mCurrentSize + mSelectArr.size());
					}

				}
			}
		};

		public class ImgItemViewModel{
			public String mDataSource;
			public final StringObservable bCoverUrl = new StringObservable();
			public final BooleanObservable bIsSelect = new BooleanObservable(false);
			public final BooleanObservable bIsChoiceModel = new BooleanObservable(true);
			public final StringObservable bScaleType = new StringObservable("centerCrop");

			public ImgItemViewModel(String dataSource){
				bCoverUrl.set(dataSource);
				mDataSource = dataSource;
			}

		}
	}

}
