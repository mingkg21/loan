package com.bk.android.time.ui.activiy;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.bk.android.assistant.R;
import com.bk.android.time.ui.fragment.MyActivityFragment;
import com.bk.android.time.ui.fragment.MyMsgFragment;

import java.util.ArrayList;

/** 我的消息
 * Created by mingkg21 on 2017/4/8.
 */

public class MyMsgActivity extends BaseTabActivity {

    public static final String EXTRA_NAME_ACTION_ACTIVITY = "EXTRA_NAME_ACTION_ACTIVITY";
    public static final String EXTRA_NAME_ACTION_MSG = "EXTRA_NAME_ACTION_MSG";

    public static void openActivity(Context context) {
        context.startActivity(new Intent(context, MyMsgActivity.class));
    }

    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setTitle(R.string.user_center_msg);

        String type = getIntent().getStringExtra("action");
        if (EXTRA_NAME_ACTION_ACTIVITY.equals(type)) {
            setCurrentTab(0);
        } else if (EXTRA_NAME_ACTION_MSG.equals(type)) {
            setCurrentTab(1);
        }
    }

    @Override
    protected ArrayList<String> initTabTitles() {
        ArrayList<String> tmep = new ArrayList<>();
        tmep.add("活动");
        tmep.add("消息");
        return tmep;
    }

    @Override
    protected Fragment getItemFragment(int position) {
        if (position == 0) {
            return new MyActivityFragment();
        }
        return new MyMsgFragment();
    }

    @Override
    public boolean onTitleButtonClick(boolean isLeft) {
        if(!isLeft){
            ActivityChannels.openRegisterActivity(this_);
        }
        return super.onTitleButtonClick(isLeft);
    }



}
