package com.bk.android.time.ui.activiy;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.bk.android.assistant.R;
import com.bk.android.time.model.lightweight.NetEaseNewsListViewModel;
import com.bk.android.time.ui.BaseAppActivity;

/** 浏览记录
 * @author mingkg21
 * @date 2017年04月10日
 *
 */
public class NetEaseNewsActivity extends BaseAppActivity {
	
	public static void openActivity(Context context) {
		Intent intent = new Intent(context, NetEaseNewsActivity.class);
		context.startActivity(intent);
	}

	private NetEaseNewsListViewModel mListViewModel;

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);

		mListViewModel = new NetEaseNewsListViewModel(this, this);
		setContentView(bindView(R.layout.com_list_lay, mListViewModel));

		setTitle(R.string.tag_browsing_history);

		mListViewModel.onStart();

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if(mListViewModel != null) {
			mListViewModel.onRelease();
		}
	}
}
