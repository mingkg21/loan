package com.bk.android.time.ui.activiy;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.widget.ListView;

import com.bk.android.assistant.R;
import com.bk.android.time.model.lightweight.CardProcessQueryViewModel;
import com.bk.android.time.ui.BaseAppActivity;

public class CardProccessQueryActivity extends BaseAppActivity {
	
	public static void openActivity(Context context) {
		Intent intent = new Intent(context, CardProccessQueryActivity.class);
		context.startActivity(intent);
	}

	private CardProcessQueryViewModel mCardProcessQueryViewModel;

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);

		mCardProcessQueryViewModel = new CardProcessQueryViewModel(this, this);
		setContentView(bindView(R.layout.uniq_card_proccess_query_lay, mCardProcessQueryViewModel));

		ListView listView = (ListView) findViewById(R.id.list_view);
		listView.setDivider(new ColorDrawable(getResources().getColor(R.color.divider_line)));
		listView.setDividerHeight(1);

		setTitle("查询进度");

		mCardProcessQueryViewModel.onStart();

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if(mCardProcessQueryViewModel != null) {
			mCardProcessQueryViewModel.onRelease();
		}
	}
}
