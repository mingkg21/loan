package com.bk.android.time.ui;
/**
 * 代表网络请求相关界面
 * @author linyiwei
 */
public interface INetLoadView extends ILoadView{
	/**
	 * 设置加载重试界面背景是否透明
	 * @param isTransparent
	 */
	public void setLoadingViewTransparent(boolean isTransparent);
	/**
	 * 显示这种网络的界面
	 */
	public void showNetSettingView(boolean canClean);
	/**
	 * 显示这种网络的界面
	 */
	public void showNetSettingDialog(boolean canClean);
	/**
	 * 显示这种网络的界面
	 */
	public void hideNetSettingDialog();
	/**
	 * 隐藏这种网络的界面
	 */
	public void hideNetSettingView();
	/**
	 * 显示重试界面
	 */
	public void showRetryView(Runnable retryTask,boolean canClean);
	/**
	 * 隐藏重试界面
	 */
	public void hideRetryView();
	/**
	 * 显示登录界面
	 * @param retryTask
	 */
	public void showLoginView(Runnable retryTask);
	
	public interface LoginCallback extends Runnable{
		public void cancel();
	}
}
