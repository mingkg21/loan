package com.bk.android.time.ui;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.TextView;

import com.bk.android.assistant.R;
import com.bk.android.time.anim.ViewAnimDecorator;
import com.bk.android.time.app.AbsFragment;
import com.bk.android.time.ui.widget.MainRootLayout;
import com.bk.android.util.AppUtil;

public abstract class BaseFragment extends AbsFragment{
	private TextView mLoadingTv;
	private View mNetRetryLay;
	private TextView mNetRetryTipTv;
	private Button mNetRetryBut;
	private ViewGroup mLoadingView;
	private boolean isTransparentLoadingView;
	private ImageView mCloseView;
	private View mContentLayout;
	private Integer mContentVisibility;
	private Runnable mFailRetryTask;
	private MainRootLayout mRootLayout;
	private Handler mHandler = new Handler(Looper.getMainLooper());
	
	public BaseFragment(){}

	@Override
	public final View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		mRootLayout = (MainRootLayout) inflater.inflate(R.layout.com_fragment_lay, null);
		mRootLayout.setTouchBackEnabled(false);
		mRootLayout.setInitFinish();
		mNetRetryLay = mRootLayout.findViewById(R.id.net_retry_lay);
		mNetRetryTipTv = (TextView)mRootLayout.findViewById(R.id.net_retry_tip_tv);
		mNetRetryBut = (Button)mRootLayout.findViewById(R.id.net_retry_but);
		mCloseView = (ImageView)mRootLayout.findViewById(R.id.net_retry_tip_iv);
		mCloseView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				hideRetryView();
			}
		});
		mLoadingView = (ViewGroup) mRootLayout.findViewById(R.id.loading_lay);
		mLoadingTv = (TextView)mRootLayout.findViewById(R.id.loading_tv);

		setContentView(onCreateContentView(inflater, container, savedInstanceState));
		mHandler.post(new Runnable() {
			@Override
			public void run() {
				onPostCreateContentView();
			}
		});
		return mRootLayout;
	}
	
	@Override
	public final void onDestroyView() {
		super.onDestroyView();
		onDestroyContentView();
	}

	protected void setContentView(View view){
		if(mRootLayout == null){
			return;
		}
		if(mContentLayout != null){
			mRootLayout.removeView(mContentLayout);
		}
		mContentLayout = view;
		if(mContentLayout != null){
			if(mContentVisibility != null){
				mContentLayout.setVisibility(mContentVisibility);
				mContentVisibility = null;
			}
			mRootLayout.addView(mContentLayout,new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		}
	}

	public abstract View onCreateContentView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState);

	public abstract void onDestroyContentView();

	public void onPostCreateContentView(){}
	
	protected int getDefaultLoadingViewBGColor(){
		return R.color.com_color_5;
	}
	
	private boolean isTransparentLoadingView() {
		return isTransparentLoadingView;
	}
	
	@Override
	public void setLoadingViewTransparent(boolean isTransparent){
		isTransparentLoadingView = isTransparent;
	}
	
	protected boolean isViewAnimEnabled() {
		return true;
	}
	
	@SuppressWarnings("deprecation")
	private void setViewBGColor(View view,boolean isLoadingView){
		if(isTransparentLoadingView()){
			if(isLoadingView){
				view.setBackgroundColor(getResources().getColor(getDefaultLoadingViewBGColor()) + 0x99000000);
				view.setClickable(true);
			}else{
				view.setBackgroundDrawable(null);
				view.setClickable(false);
			}
		}else{
			view.setBackgroundColor(getResources().getColor(getDefaultLoadingViewBGColor()));
			view.setClickable(true);
		}
	}
	
	protected String getLoadingTip(){
		return getString(R.string.tip_loading);
	}
	
	protected void onHideLoadAndRetryView() {
		onHideLoadAndRetryView(true);
	}
	
	protected void onHideLoadAndRetryView(boolean isCancelFailRetryTask) {
		if(isCancelFailRetryTask){
			cancelFailRetryTask();
		}
		if (!isAdded()) {
			return;
		}
		if(mContentLayout != null){
			mContentLayout.setVisibility(View.VISIBLE);
		}else{
			mContentVisibility = View.VISIBLE;
		}
		mNetRetryBut.setOnClickListener(null);
		hideNetSettingDialog();
		ViewAnimDecorator.hideView(mLoadingView, isViewAnimEnabled());
		ViewAnimDecorator.hideView(mNetRetryLay, isViewAnimEnabled());
	}
	
	private void showAllChild(ViewGroup view){
		for (int i = 0; i < view.getChildCount(); i++) {
			view.getChildAt(i).setVisibility(View.VISIBLE);
		}
	}
	
	private void hideAllChild(ViewGroup view){
		for (int i = 0; i < view.getChildCount(); i++) {
			view.getChildAt(i).setVisibility(View.INVISIBLE);
		}
	}
	
	@Override
	public void showLoadView() {
		cancelFailRetryTask();
		if (!isAdded()) {
			return;
		}
		if(!isTransparentLoadingView()){
			if(mContentLayout != null){
				mContentLayout.setVisibility(View.INVISIBLE);
			}else{
				mContentVisibility = View.INVISIBLE;
			}
		}
		
		mLoadingTv.setText(getLoadingTip());
		mNetRetryLay.setVisibility(View.GONE);
		setViewBGColor(mLoadingView, true);
		showAllChild(mLoadingView);
		ViewAnimDecorator.showView(mLoadingView, false);
	}

	@Override
	public void updateProgress(int progress, int maxProgress, int msgFormat) {
		if(mLoadingTv.isShown()){
			int per = (int) (progress * 1f / maxProgress * 100);
			String str = per + "%";
			if(msgFormat > 0){
				mLoadingTv.setText(getString(msgFormat, str));
			}else{
				mLoadingTv.setText(getLoadingTip() + str);
			}
		}
	}
	
	@Override
	public void hideLoadView() {
		onHideLoadAndRetryView();
	}

	@Override
	public void showLoginView(Runnable retryTask) {
		if (!isAdded()) {
			return;
		}
		if(getActivity() instanceof INetLoadView){
			((INetLoadView) getActivity()).showLoginView(retryTask);
		}
	}

	private void cancelFailRetryTask(){
		if(mFailRetryTask instanceof LoginCallback){
			((LoginCallback) mFailRetryTask).cancel();
		}
		mFailRetryTask = null;
	}
	
	@Override
	public void showNetSettingView(boolean canClean) {
		cancelFailRetryTask();
		if (!isAdded()) {
			return;
		}
		mNetRetryLay.setVisibility(View.VISIBLE);
		mLoadingView.setVisibility(View.GONE);
		mNetRetryTipTv.setText(R.string.tip_on_net);
		mNetRetryBut.setText(R.string.btn_text_setting);
		if(canClean){
			mCloseView.setImageResource(R.drawable.load_and_retry_close_icon);
		}else{
			mCloseView.setImageResource(R.drawable.loading_tip_icon);
		}
		mCloseView.setClickable(canClean);
		mNetRetryBut.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				AppUtil.gotoSettingActivity(getActivity());
			}
		});
		hideAllChild(mLoadingView);
		setViewBGColor(mLoadingView,false);
		ViewAnimDecorator.showView(mLoadingView,false);
		ViewAnimDecorator.showView(mNetRetryLay,false);
	}

	@Override
	public void showNetSettingDialog(boolean canClean) {
		cancelFailRetryTask();
		if (!isAdded()) {
			return;
		}
		if(getActivity() instanceof INetLoadView){
			((INetLoadView) getActivity()).showNetSettingDialog(canClean);
		}
	}

	@Override
	public void hideNetSettingView() {
		onHideLoadAndRetryView();
	}

	@Override
	public void hideNetSettingDialog() {
		if(getActivity() instanceof INetLoadView){
			((INetLoadView) getActivity()).hideNetSettingDialog();
		}
	}
	
	@Override
	public void showRetryView(final Runnable retryTask,boolean canClean) {
		if (!isAdded()) {
			return;
		}
		mNetRetryLay.setVisibility(View.VISIBLE);
		mLoadingView.setVisibility(View.GONE);
		mNetRetryTipTv.setText(R.string.tip_err_server);
		mNetRetryBut.setText(R.string.btn_text_retry);
		if(canClean){
			mCloseView.setImageResource(R.drawable.load_and_retry_close_icon);
		}else{
			mCloseView.setImageResource(R.drawable.loading_tip_icon);
		}
		mCloseView.setClickable(canClean);
		mFailRetryTask = retryTask;
		mNetRetryBut.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				onHideLoadAndRetryView(false);
				if(mFailRetryTask != null){
					mFailRetryTask.run();
				}
				cancelFailRetryTask();
			}
		});
		hideAllChild(mLoadingView);
		setViewBGColor(mLoadingView,false);
		ViewAnimDecorator.showView(mLoadingView,false);
		ViewAnimDecorator.showView(mNetRetryLay,false);
	}

	@Override
	public void hideRetryView() {
		onHideLoadAndRetryView();
	}
}
