package com.bk.android.time.ui;
/**
 * 代表加载加载数据的View
 * @author linyiwei
 *
 */
public interface ILoadView extends IView{
	/**
	 * 显示加载数据的View
	 */
	public void showLoadView();
	/**
	 * 隐藏加载数据的View
	 */
	public void hideLoadView();
	/**
	 * 更新进度
	 * @param msgFormat 
	 */
	public void updateProgress(int progress,int maxProgress, int msgFormat);
}
