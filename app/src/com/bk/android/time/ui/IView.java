package com.bk.android.time.ui;

import com.bk.android.time.app.observer.IAppContextObserver;
import com.bk.android.time.model.BaseDialogViewModel;

/**
 * 代表一个界面，所有界面或者界面接口都实现或继承它
 * @author linyiwei
 */
public interface IView {
	/**
	 * 退出界面
	 */
	public void finish();
	/**
	 * 绑定DialogViewModel窗口
	 * @param extraParams
	 * @param context
	 * @param tag
	 * @param <T>
	 * @return 返回true表示已经绑定成功
	 */
	public BaseDialogViewModel bindDialogViewModel(String type,Object viewModel, Object... extraParams);
	
	public void addViewContextObservable(IAppContextObserver contextCallBack);
}
