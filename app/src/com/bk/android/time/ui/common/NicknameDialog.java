package com.bk.android.time.ui.common;

import android.content.Context;
import android.view.Gravity;

import com.bk.android.assistant.R;
import com.bk.android.time.app.AbsDialog;
import com.bk.android.time.model.BaseViewModel;
import com.bk.android.time.model.common.NickNameDialogViewModel;

import gueei.binding.Binder;
import gueei.binding.Binder.InflateResult;

/** 验证手机号码
 * @author mingkg21
 * @email mingkg21@gmail.com
 * @date 2016年6月6日
 */
public class NicknameDialog extends AbsDialog {

	protected NickNameDialogViewModel mViewModel;

	public NicknameDialog(Context context, final NickNameDialogViewModel viewModel, BaseViewModel... baseViewModels) {
		super(context, R.style.AppCenterDialogTheme);
		setGravity(Gravity.CENTER);
		setFullWidth(false);
		mViewModel = viewModel;
		mViewModel.bindDialog(this);
		InflateResult result = Binder.inflateView(getContext(), R.layout.uniq_dialog_nickname, null,false);
		
		Binder.bindView(getContext(), result, mViewModel);

		for(int i = 0;baseViewModels != null && i < baseViewModels.length; i++){
			Binder.bindView(getContext(), result, baseViewModels[i]);
		}
		setContentView(result.rootView);
	}
}
