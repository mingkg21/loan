package com.bk.android.time.ui.common;

import android.content.Context;

import com.bk.android.assistant.R;
import com.bk.android.time.app.AbsDialog;
import com.bk.android.time.model.BaseViewModel;
import com.bk.android.time.model.common.ShareDialogViewModel;

import gueei.binding.Binder;

/** 微信分享选择的对话框
 * @author mingkg21
 * @email mingkg21@gmail.com
 * @date 2014年4月1日
 */
public class WXShareDialog extends AbsDialog {
	
	protected ShareDialogViewModel mViewModel;
	
	public WXShareDialog(Context context, ShareDialogViewModel viewModel,BaseViewModel... baseViewModels) {
		super(context,R.style.ShareDialogTheme);
		mViewModel = viewModel;
		mViewModel.bindDialog(this);
		Binder.InflateResult result = Binder.inflateView(getContext(), R.layout.uniq_share_lay, null,false);
		Binder.bindView(getContext(), result, mViewModel);
		for(int i = 0;baseViewModels != null && i < baseViewModels.length; i++){
			Binder.bindView(getContext(), result, baseViewModels[i]);
		}
		setContentView(result.rootView);
	}
}
