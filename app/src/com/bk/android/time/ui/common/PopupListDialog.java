package com.bk.android.time.ui.common;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.bk.android.assistant.R;
import com.bk.android.time.app.AbsDialog;

import java.util.ArrayList;
import java.util.Arrays;

/** 选择列表对话框
 * Created by mingkg21 on 2017/4/10.
 */

public class PopupListDialog extends AbsDialog {

    private ListView mListView;
    private Adapter mAdapter;
    private ArrayList<String> mdata;
    private OnItemSelectedListener mOnItemSelectedListener;

    public PopupListDialog(Context context, int resId, OnItemSelectedListener listener) {
        super(context, R.style.ShareDialogTheme);
        mOnItemSelectedListener = listener;
        setContentView(R.layout.uniq_dialog_popup_list_lay);
        mListView = (ListView) findViewById(R.id.content_lv);
        mdata = new ArrayList<>();
        String[] stss = getContext().getResources().getStringArray(resId);
        if (stss != null) {
            mdata.addAll(Arrays.asList(stss));
        }
        mAdapter = new Adapter(context, mdata);
        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String str = (String) parent.getItemAtPosition(position);
                if (mOnItemSelectedListener != null) {
                    mOnItemSelectedListener.onItemSelected(position, str);
                }
                dismiss();
            }
        });
    }

    public interface OnItemSelectedListener {
        public void onItemSelected(int position, String str);
    }

    private class Adapter extends BaseAdapter {

        private ArrayList<String> mDatas = new ArrayList<>();
        private Context mContext;

        public Adapter(Context context, ArrayList<String> datas) {
            super();
            mContext = context;
            if (datas != null) {
                mDatas.addAll(datas);
            }
        }

        @Override
        public int getCount() {
            return mDatas.size();
        }

        @Override
        public String getItem(int position) {
            return mDatas.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, android.view.View convertView, ViewGroup parent) {

            ViewHolder viewHolder = null;
            if (convertView == null) {
                convertView = LayoutInflater.from(mContext).inflate(R.layout.uniq_dialog_popup_list_item_lay, null);

                viewHolder = new ViewHolder(convertView);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            viewHolder.bindView(getItem(position));

            return convertView;
        }

    }

    private class ViewHolder {

        private TextView mTextView;

        public ViewHolder(View view) {
            mTextView = (TextView) view.findViewById(R.id.content_tv);
        }

        public void bindView(String str) {
            mTextView.setText(str);
        }

    }
}
