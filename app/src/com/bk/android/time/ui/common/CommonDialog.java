package com.bk.android.time.ui.common;

import android.content.Context;
import android.view.Gravity;
import android.view.inputmethod.InputMethodManager;

import com.bk.android.assistant.R;
import com.bk.android.time.app.AbsDialog;
import com.bk.android.time.model.common.CommonDialogViewModel;
/**
 * 通用Dialog
 * @author linyiwei
 */
public class CommonDialog extends AbsDialog {
	protected CommonDialogViewModel mViewModel;
	
	public CommonDialog(Context context,CommonDialogViewModel viewModel) {
		this(context, viewModel, R.style.AppCenterDialogTheme);
	}
	
	public CommonDialog(Context context,CommonDialogViewModel viewModel, int theme) {
		super(context,theme);
		mViewModel = viewModel;
		mViewModel.bindDialog(this);
		setFullWidth(false);
		setContentView(bindView(R.layout.com_dialog_lay, null, mViewModel));
		setGravity(Gravity.CENTER);
	}

	@Override
	public void dismiss() {
		InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
		if (imm != null && getCurrentFocus() != null) {
			imm.hideSoftInputFromWindow(getCurrentFocus().getApplicationWindowToken(), 0); // 强制隐藏键盘
		}
		super.dismiss();
	}
}
