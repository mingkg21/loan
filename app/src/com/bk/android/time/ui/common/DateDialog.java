package com.bk.android.time.ui.common;

import net.simonvt.datepicker.DatePicker;
import android.content.Context;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import com.bk.android.assistant.R;
import com.bk.android.time.app.AbsDialog;

/** 
 * @author mingkg21
 * @email mingkg21@gmail.com
 * @date 2014年12月17日
 */
public class DateDialog extends AbsDialog {
	
	private DatePicker mDatePicker;
	private OnDateListener mDateListener;
	
	public DateDialog(Context context, String title, long time, OnDateListener listener) {
		super(context, R.style.AppCenterDialogTheme);
		setGravity(Gravity.CENTER);
		setFullWidth(false);
		setContentView(R.layout.uniq_dialog_date);
		
		mDatePicker = (DatePicker) findViewById(R.id.date_dp);
		if(time > 0) {
			mDatePicker.setTime(time);
		}
		
		TextView titleTV = (TextView) findViewById(R.id.title_tv);
		if(!TextUtils.isEmpty(title)) {
			titleTV.setText(title);
		}
		
		mDateListener = listener;
		
		findViewById(R.id.ok_btn).setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View view) {
				if(mDateListener != null) {
					mDateListener.onTime(mDatePicker.getTime());
				}
				dismiss();
			}
		});
		
		findViewById(R.id.cancel_btn).setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View view) {
				cancel();
			}
		});
	}
	
	public void setMaxDate(long maxDate) {
		mDatePicker.setMaxDate(maxDate);
	}
	
	public interface OnDateListener {
		public void onTime(long time);
	}
	
}
