package com.bk.android.time.ui;

import android.graphics.drawable.Drawable;
import android.view.View;


public interface ITitleBar {
	public static final int MENU_ITEM_ID_LEFT_BUTTON = "MENU_ITEM_ID_LEFT_BUTTON".hashCode();
	public static final int MENU_ITEM_ID_RIGHT_BUTTON = "MENU_ITEM_ID_RIGHT_BUTTON".hashCode();
	/**
	 * 设置title文本
	 * @param titleStr title文本
	 */
	public void setTitle(CharSequence title);
	/**
	 * 设置title文本
	 * @param titleStr title文本
	 */
	public void setTitle(int titleId);
	/**
	 * 重置TitleBar
	 */
	public void resetTitleBar();
	/**
	 * 设置左边按钮 
	 * <p>点击事件会触发一个 onOptionsItemSelected(MenuItem item)事件 item id 为 ITitleBar.MENU_ITEM_ID_LEFT_BUTTON</p>
	 * @param tip
	 * @param icon
	 * @param bgRes 
	 */
	public void setLeftButton(String tip,int icon, int bgRes);
	/**
	 * 设置由变按钮边按钮
	 * <p>点击事件会触发一个 onOptionsItemSelected(MenuItem item)事件 item id 为 ITitleBar.MENU_ITEM_ID_RIGHT_BUTTON</p>
	 * @param tip
	 * @param icon
	 * @param bgRes 
	 */
	public void setRightButton(String tip,int icon, int bgRes);

	public void setLeftButtonImageView(Drawable drawable);

	public void setRightButtonImageView(Drawable drawable);

	/**
	 * 设置启用 LeftButton
	 * @param isEnabled
	 */
	public void setLeftButtonEnabled(boolean isEnabled);
	/**
	 * 设置启用 RightButton
	 * @param isEnabled
	 */
	public void setRightButtonEnabled(boolean isEnabled);
	/**
	 * 设置启用 TitleBar
	 * @param isEnabled
	 */
	public void setTitleBarEnabled(boolean isEnabled);
	/**
	 * 获取View
	 * @return
	 */
	public View getLeftButtonView();
	/**
	 * 获取View
	 * @return
	 */
	public View getRightButtonView();
	/**
	 * 删除自定义TitleView
	 * @param view
	 */
	public void removeTitleView(View view);
	/**
	 * 添加自定义TitleView
	 * @param view
	 */
	public void setTitleView(View view);
	/**
	 * 
	 * @param l
	 */
	public void addTitleListener(OnTitleListener l);

	public interface OnTitleListener{
		public boolean onTitleButtonClick(boolean isLeft);
		public boolean onTitleClick();
	}
}
