package com.bk.android.time.thridparty;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;

import com.bk.android.time.app.App;
import com.bk.android.time.data.Preferences;
import com.bk.android.time.util.ToastUtil;
import com.sina.weibo.sdk.api.ImageObject;
import com.sina.weibo.sdk.api.TextObject;
import com.sina.weibo.sdk.api.WeiboMultiMessage;
import com.sina.weibo.sdk.api.share.IWeiboShareAPI;
import com.sina.weibo.sdk.api.share.SendMultiMessageToWeiboRequest;
import com.sina.weibo.sdk.api.share.WeiboShareSDK;
import com.sina.weibo.sdk.auth.Oauth2AccessToken;
import com.sina.weibo.sdk.auth.WeiboAuth;
import com.sina.weibo.sdk.auth.WeiboAuthListener;
import com.sina.weibo.sdk.auth.sso.SsoHandler;
import com.sina.weibo.sdk.exception.WeiboException;
import com.sina.weibo.sdk.net.RequestListener;

/**
 * @author mingkg21
 * @email mingkg21@gmail.com
 * @date 2014年9月17日
 * 
 */
public class WeiboShareManage {

	private IWeiboShareAPI mWeiboShareAPI;
	private SsoHandler mSsoHandler;
	private static WeiboShareManage mInstance;
	
	private WeiboShareManage(Context context) {
		mWeiboShareAPI = WeiboShareSDK.createWeiboAPI(context, WeiboConstants.APP_KEY);
		mWeiboShareAPI.registerApp();
	}
	
	public static WeiboShareManage getInstance(Context context) {
		if(mInstance == null) {
			mInstance = new WeiboShareManage(context);
		}
		return mInstance;
	}

	public void sendMultiMessage(String title, String content, String url, String imagePath) {
		if (!mWeiboShareAPI.isWeiboAppInstalled()) {
			ToastUtil.showToast(App.getInstance(), "请安装微博客户端进行分享^_^！");
			return;
		}
		if(title == null){
			title = "";
		}
		if(content == null){
			content = "";
		}
		if(url == null){
			url = "";
		}
		WeiboMultiMessage weiboMessage = new WeiboMultiMessage();// 初始化微博的分享消息
		TextObject textObject = new TextObject();
		textObject.text = content + " " + url;
		textObject.title = title;
		weiboMessage.textObject = textObject;
		
		if(!TextUtils.isEmpty(imagePath)){
			ImageObject imageObject = new ImageObject();
			imageObject.imagePath = imagePath;
			weiboMessage.imageObject = imageObject;
		}
		
		SendMultiMessageToWeiboRequest request = new SendMultiMessageToWeiboRequest();
		request.transaction = String.valueOf(System.currentTimeMillis());
		request.multiMessage = weiboMessage;
		mWeiboShareAPI.sendRequest(request); // 发送请求消息到微博,唤起微博分享界面
	}
	
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(mSsoHandler != null){
			mSsoHandler.authorizeCallBack(requestCode, resultCode, data);
		}
	}

	public void sendMultiMessageInBg(Activity activity,String content,Bitmap bitmap,BgCallback callback){
		sendMultiMessageInBg(activity, content,"", bitmap, callback);
	}
	
	public void sendMultiMessageInBg(Activity activity,String content,String url,Bitmap bitmap,BgCallback callback){
		sendMultiMessageInBg(activity,"", content,url, bitmap, callback);
	}
	
	public void sendMultiMessageInBg(Activity activity,final String title,final String content,final String url,final Bitmap bitmap,final BgCallback callback){
		Oauth2AccessToken accessToken = Preferences.getInstance().getWeiboAccessToken();
		callback.onStrart();
		if(accessToken != null && accessToken.isSessionValid()) {
			sendMultiMessageInBg(accessToken,title,content,url,bitmap,callback);
		}else{
			if (!mWeiboShareAPI.isWeiboAppInstalled()) {
				ToastUtil.showToast(App.getInstance(), "请安装微博客户端进行分享^_^！");
				callback.onEnd(false);
				return;
			}
			mSsoHandler = new SsoHandler(activity, 
					new WeiboAuth(activity.getApplicationContext(), WeiboConstants.APP_KEY, WeiboConstants.REDIRECT_URL, WeiboConstants.SCOPE));
			mSsoHandler.authorize(new WeiboAuthListener() {
				@Override
				public void onWeiboException(WeiboException arg0) {
					callback.onEnd(false);
					mSsoHandler = null;
				}
				@Override
				public void onComplete(Bundle arg0) {
					final Oauth2AccessToken accessToken = Oauth2AccessToken.parseAccessToken(arg0);
					if(accessToken != null && accessToken.isSessionValid()) {
						Preferences.getInstance().setWeiboAccessToken(accessToken);
						sendMultiMessageInBg(accessToken,title,content,url,bitmap,callback);
					}else{
						callback.onEnd(false);
					}
					mSsoHandler = null;
				}
				@Override
				public void onCancel() {
					callback.onEnd(false);
					mSsoHandler = null;
				}
			});
		}
	}
	
	private void sendMultiMessageInBg(Oauth2AccessToken accessToken,String title,String content,String url,Bitmap bitmap,final BgCallback callback){
		callback.onStrartSend();
		if(TextUtils.isEmpty(title)){
			title = "";
		}else{
			title += "\n";
		}
		if(content == null){
			content = "";
		}
		if(url == null){
			url = "";
		}
		String text = title + content + " " + url;
		if(text.length() > 280){
			text = title + content.substring(0,content.length() - text.length() + 278) + "…" + url;
		}
		final UsersAPI usersAPI = new UsersAPI(accessToken);
		usersAPI.sendMsg(text,bitmap, new RequestListener(){
			@Override
			public void onComplete(String arg0) {
				callback.onEnd(true);
			}
			@Override
			public void onWeiboException(WeiboException arg0) {
				callback.onEnd(false);
				arg0.printStackTrace();
			}});
	}
	
	public interface BgCallback{
		public void onStrart();
		public void onStrartSend();
		public void onEnd(boolean iSucceed);
	}
}
