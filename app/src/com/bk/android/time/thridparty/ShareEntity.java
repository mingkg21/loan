package com.bk.android.time.thridparty;

import java.io.Serializable;
import java.util.ArrayList;

/** 分享的实体
 * @author mingkg21
 * @email mingkg21@gmail.com
 * @date 2014年5月29日
 *
 */
public class ShareEntity implements Serializable{
	private static final long serialVersionUID = 4603517023248733979L;
	public static final int TYPE_NEWS = 1;
	public static final int TYPE_CONTENT = 2;
	public static final int TYPE_APP = 3;
	public static final int TYPE_WORK_ACHIEVEMENT = 4;
	public static final int TYPE_RANKING = 5;
	public static final int TYPE_WORK = 6;
	public static final int TYPE_RECORD = 7;
	public static final int TYPE_INVITE_FRIEND = 8;
	public static final int TYPE_POST = 9;
	public static final int TYPE_TOPIC = 10; //话题或版块
	public static final int TYPE_GOODS = 11; //兑换
	public static final int TYPE_FAMILY_INVITE = 12; //家庭邀请
	public static final int TYPE_BOARD_RECORD = 13; //习惯
	public static final int TYPE_BABY_INVITE_CODE = 14; //宝宝邀请码
	public static final int TYPE_BABY_RECORD = 15;//新版记录
	public static final int TYPE_IMG = 16;//单图
	public static final int TYPE_MAGAZINE = 17;//影集
	public static final int TYPE_BABY_INVITE_CODE_OPEN_WX_APP = 18;//宝宝邀请码分享,表示微信分享(用户点开微信跳转app)

	public boolean isNeedQzone = false;
	public boolean isNeedFriend = false;
	public boolean isNeedWeibo = false;
	
	public String title;
	public String summary;
	public String webUrl;
	public ArrayList<String> imgUrls;
	
	public int type;
	public Integer childType;
	public String id;
	

}
