package com.bk.android.time.thridparty;

/** 微博的常量类
 * @author mingkg21
 * @email mingkg21@gmail.com
 * @date 2014年3月21日
 */
public class WeiboConstants {
	
	public static final String APP_KEY = "479646351"; // 应用的APP_KEY
	public static final String REDIRECT_URL = "http://www.banketime.com/";// 应用的回调页
	public static final String SCOPE = // 应用申请的高级权限
	"email,direct_messages_read,direct_messages_write,"
			+ "friendships_groups_read,friendships_groups_write,statuses_to_me_read,"
			+ "follow_app_official_microblog," + "invitation_write";

}
