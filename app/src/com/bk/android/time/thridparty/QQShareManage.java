package com.bk.android.time.thridparty;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;

import com.bk.android.assistant.R;
import com.bk.android.time.data.net.WebConfig;
import com.tencent.connect.share.QQShare;
import com.tencent.connect.share.QzoneShare;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.Tencent;

import java.util.ArrayList;


/** QQ分享
 * @author mingkg21
 * @email mingkg21@gmail.com
 * @date 2014年4月22日
 */
public class QQShareManage {

	private Tencent mTencent;
	
	private String appName;
	
	private static QQShareManage instance;
	
	private QQShareManage(Context context) {
		appName = context.getString(R.string.app_name);
		mTencent = Tencent.createInstance(TencentConstants.TENCENT_ID, context);
	}
	
	public static QQShareManage getInstance(Context context) {
		if(instance == null) {
			instance = new QQShareManage(context.getApplicationContext());
		}
		return instance;
	}
	
	public void shareToQQImg(final Activity activity, String imageUrl, IUiListener iUiListener) {
		Bundle params = new Bundle();
	    params.putInt(QQShare.SHARE_TO_QQ_KEY_TYPE, QQShare.SHARE_TO_QQ_TYPE_IMAGE);
	    params.putString(QQShare.SHARE_TO_QQ_IMAGE_LOCAL_URL,imageUrl);
	    params.putString(QQShare.SHARE_TO_QQ_APP_NAME, appName);
	    mTencent.shareToQQ(activity, params,iUiListener);
	}
	
	public void shareToQQ(final Activity activity,  String callBackUrl, String title, String imageUrl, String summary, IUiListener iUiListener) {
		if(title != null && title.length() > 30){
			title = title.substring(0, 28) + "…";
		}
		if(summary != null && summary.length() > 80){
			summary = summary.substring(0, 78) + "…";
		}
		
		if(TextUtils.isEmpty(imageUrl)) {
			imageUrl = WebConfig.getInstance().getShareIcondUrl();
		}
		
		final Bundle params = new Bundle();
		params.putInt(QQShare.SHARE_TO_QQ_KEY_TYPE,	QQShare.SHARE_TO_QQ_TYPE_DEFAULT);
		params.putString(QQShare.SHARE_TO_QQ_TITLE, title);
		params.putString(QQShare.SHARE_TO_QQ_SUMMARY, summary);
		params.putString(QQShare.SHARE_TO_QQ_TARGET_URL, callBackUrl);
		params.putString(QQShare.SHARE_TO_QQ_IMAGE_URL, imageUrl);
		params.putString(QQShare.SHARE_TO_QQ_APP_NAME, appName);
//		params.putInt(QQShare.SHARE_TO_QQ_EXT_INT, "其他附加功能");
		mTencent.shareToQQ(activity, params, iUiListener);
		
	}
	
	public void shareToQZone(final Activity activity, String callBackUrl, String title, ArrayList<String> imageUrls, String summary, IUiListener iUiListener) {
		if(title != null && title.length() > 200){
			title = title.substring(0, 198) + "…";
		}
		if(summary != null && summary.length() > 600){
			summary = summary.substring(0, 598) + "…";
		}
		final Bundle params = new Bundle();
		params.putInt(QzoneShare.SHARE_TO_QZONE_KEY_TYPE,QzoneShare.SHARE_TO_QZONE_TYPE_IMAGE_TEXT);
		params.putString(QzoneShare.SHARE_TO_QQ_TITLE, title);
		params.putString(QzoneShare.SHARE_TO_QQ_SUMMARY, summary);
		params.putString(QzoneShare.SHARE_TO_QQ_TARGET_URL, callBackUrl);
		if(imageUrls == null) {
			imageUrls = new ArrayList<String>();
			imageUrls.add(WebConfig.getInstance().getShareIcondUrl());
		}
		params.putStringArrayList(QzoneShare.SHARE_TO_QQ_IMAGE_URL, imageUrls);
		mTencent.shareToQzone(activity, params, iUiListener);
	}
	
}
