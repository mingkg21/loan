package com.bk.android.time.app;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import com.bk.android.time.ui.activiy.MyMsgActivity;

import org.json.JSONObject;

import cn.jpush.android.api.JPushInterface;

/**
 * Created by mingkg21 on 2017/5/10.
 */

public class JPushReceiver extends BroadcastReceiver {

    private static final String TAG = "JPushReceiver";

    private NotificationManager nm;

    @Override
    public void onReceive(Context context, Intent intent) {
        if (null == nm) {
            nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        }

        Bundle bundle = intent.getExtras();
        if (JPushInterface.ACTION_NOTIFICATION_OPENED.equals(intent.getAction())) {
            openNotification(context,bundle);
        }
    }

    private void openNotification(Context context, Bundle bundle){
        String extras = bundle.getString(JPushInterface.EXTRA_EXTRA);
        try {
            JSONObject extrasJson = new JSONObject(extras);
            String msgContent = extrasJson.optString("msgContent");
            if (!TextUtils.isEmpty(msgContent)) {
                JSONObject msgContentObject = new JSONObject(msgContent);
                String action = msgContentObject.optString("action");
                Intent intent = null;
                if ("action_recommend_loan".equals(action)) {
                    intent = new Intent(context, MyMsgActivity.class);
                    intent.putExtra("action", MyMsgActivity.EXTRA_NAME_ACTION_ACTIVITY);
                } else if ("action_recommend_system".equals(action)) {
                    intent = new Intent(context, MyMsgActivity.class);
                    intent.putExtra("action", MyMsgActivity.EXTRA_NAME_ACTION_MSG);
                }
                if (intent != null) {
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            }
        } catch (Exception e) {
            return;
        }
    }

}
