package com.bk.android.time.app.resident;

import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;

public class ResidentService extends Service implements Runnable {
	private static final String ACTION_HEART_BEAT = "ACTION_HEART_BEAT";
	private static final long HEART_BEAT_TIEM = 1000 * 30;
	private boolean isInit;
	private Handler mHandler;
	private long mLastTime;
	
	@Override
	public void onCreate() {
		super.onCreate();
		mHandler = new Handler();
//		MessageModel.getInstance().start();
//		NewServerMsgModel.getInstance().start();
	}

	@Override
	public IBinder onBind(Intent intent) {
		throw new UnsupportedOperationException("Cannot bind to Download Manager Service");
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		super.onStartCommand(intent, flags, startId);
		if(!isInit || ACTION_HEART_BEAT.equals(intent.getAction())){
			run();
			isInit = true;
		}
		return getApplicationInfo().targetSdkVersion < Build.VERSION_CODES.ECLAIR 
				? START_STICKY_COMPATIBILITY : START_STICKY;
	}
	
	@Override
	public void run() {
		if(SystemClock.elapsedRealtime() - mLastTime >= HEART_BEAT_TIEM - 1000){
			mLastTime = SystemClock.elapsedRealtime();
		}
		mHandler.removeCallbacks(this);
		mHandler.postDelayed(this, HEART_BEAT_TIEM + 10000);
	}

}
