package com.bk.android.time.app.resident;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class ResidentServiceReceiver extends BroadcastReceiver {
	@Override
	public void onReceive(Context context, Intent intent) {
		context.startService(new Intent(context, ResidentService.class));
	}
}
