// IServiceInterface.aidl
package com.bk.android.time.app.resident;

// Declare any non-default types here with import statements

interface IServiceInterface {
    void startService();
    void stopService();
}
