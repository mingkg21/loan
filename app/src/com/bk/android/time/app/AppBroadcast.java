package com.bk.android.time.app;
/**
 * @author linyiwei
 * @email 21551594@qq.com
 * @date 2012-07-27
 */
public class AppBroadcast {
	/** 关闭应用程序的ACTION */
	public static final String ACTION_CLOSE_APP = "com.lectek.android.action.CLOSE_APP";
	/** 用户登录状态改变的ACTION */
	public static final String ACTION_USER_LOGIN_STATE_CHANGE = "com.lectek.android.action.ACTION_USER_LOGIN_STATE_CHANGE";
	/** 通知简繁体变化 */
	public static final String ACTION_LANGUAGE_CHANGE = "com.lectek.android.action.LANGUAGE_CHANGE";
}
