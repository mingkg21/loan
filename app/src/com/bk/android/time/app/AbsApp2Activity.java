package com.bk.android.time.app;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.bk.android.app.BaseApp;
import com.bk.android.app.observer.Base2Activity;
import com.bk.android.time.app.observer.AppContextObservable;
import com.bk.android.time.app.observer.IAppContextObservable;
import com.bk.android.time.app.observer.IAppContextObserver;
import com.bk.android.time.data.UserData;
import com.bk.android.time.model.BaseDialogViewModel;
import com.bk.android.time.ui.ILoadView;
import com.bk.android.time.ui.activiy.MainActivity;
import com.bk.android.time.util.DialogUtil;
import com.bk.android.time.util.MobclickUtil;
import com.bk.android.time.util.NightModeUtil;
import com.bk.android.time.util.SystemBarTintManager;
import com.bk.android.util.ActivityTaskUtil;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 * @author linyiwei
 * @email 21551594@qq.com
 * @date 2012-07-27
 */
public abstract class AbsApp2Activity extends Base2Activity implements IAppContextObserver,IAppContextObservable,ILoadView{
	public static final String EXTRA_INTENT_ARR = "EXTRA_INTENT_ARR";

	private AppContextObservable mAppContextObservable;
	private ArrayList<WeakReference<IAppContextObserver>> mViewContextObservers;
	
	@Override
	protected void onCreate(Bundle arg0) {
		mViewContextObservers = new ArrayList<WeakReference<IAppContextObserver>>();
		mAppContextObservable = new AppContextObservable();
		super.onCreate(arg0);
		registerContextObservable(this);
		BaseApp.getInstance().handleEnterApp(this,arg0);
		
		try {
			ArrayList<Intent> intents = getIntent().getParcelableArrayListExtra(EXTRA_INTENT_ARR);
			if(intents != null && !intents.isEmpty()){
				Intent intent = intents.remove(0);
				intent.putParcelableArrayListExtra(EXTRA_INTENT_ARR, intents);
				startActivity(intent);
			}
		} catch (Exception e) {}
		
//		if(isSetTranslucentStatus()) {
////			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
////			    //状态栏透明 需要在创建SystemBarTintManager 之前调用。
////	            setTranslucentStatus(true);
////	        }
////			SystemBarTintManager tintManager = new SystemBarTintManager(this);
////	        tintManager.setStatusBarTintEnabled(true);
////	        //使StatusBarTintView 和 actionbar的颜色保持一致，风格统一。
////	        tintManager.setStatusBarTintResource(getStatusBarTintResource());
////	        // 设置状态栏的文字颜色
////	        tintManager.setStatusBarDarkMode(false, this);
//
//			setTranslucentStatus(this, getResources().getColor(getStatusBarTintResource()));
//		}
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		BaseApp.getInstance().saveInstanceState(outState);
	}
	
	protected abstract int getStatusBarTintResource();
	
	protected boolean isSetTranslucentStatus() {
		return true;
	}

//	/**
//	 * 设置状态栏颜色
//	 *
//	 * @param activity 需要设置的activity
//	 * @param color    状态栏颜色值
//	 */
//	public void setTranslucentStatus(Activity activity, int color) {
//		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//			// 设置状态栏透明
//			activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
//			// 生成一个状态栏大小的矩形
//			View statusView = createStatusView(activity, color);
//			// 添加 statusView 到布局中
//			ViewGroup decorView = (ViewGroup) activity.getWindow().getDecorView();
//			decorView.addView(statusView);
//			// 设置根布局的参数
//			ViewGroup rootView = (ViewGroup) ((ViewGroup) activity.findViewById(android.R.id.content)).getChildAt(0);
//			rootView.setFitsSystemWindows(true);
//			rootView.setClipToPadding(true);
//		}
//	}

	/**
	 * 生成一个和状态栏大小相同的矩形条
	 *
	 * @param activity 需要设置的activity
	 * @param color    状态栏颜色值
	 * @return 状态栏矩形条
	 */
	protected View createStatusView(Activity activity, int color) {
		// 获得状态栏高度
		int resourceId = activity.getResources().getIdentifier("status_bar_height", "dimen", "android");
		int statusBarHeight = activity.getResources().getDimensionPixelSize(resourceId);

		// 绘制一个和状态栏一样高的矩形
		View statusView = new View(activity);
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
				statusBarHeight);
		statusView.setLayoutParams(params);
		statusView.setBackgroundColor(color);
		return statusView;
	}
	
	@TargetApi(19)
    protected void setTranslucentStatus(boolean on) {
        Window win = getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        final int bits = WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS;
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		mAppContextObservable.release();
	}

	private void dispatchExitApp(){
		onExitApp();
		BaseApp.getInstance().handleExitApp();
		super.finish();
	}
	
	/**
	 * 当应用程序退出时执行的回调，父类会在onExitApp执行完后执行{@link #finish()}方法
     * 
     * @see #onPreExitApp()
	 */
	protected void onExitApp() {
		
	}
	
	@Override
	public void onLanguageChange(){
		
	}
	
	@Override
	public void onNetworkChange(boolean isAvailable) {
		
	}
	
	@Override
	public void onLoadTheme() {
		
	}
	
	@Override
	public void onUserLoginStateChange(boolean isLogin) {
		
	}
	
	@Override
	public void showLoadView() {
	}
	
	@Override
	public void hideLoadView() {
	}

	@Override
	public void updateProgress(int progress, int maxProgress, int msgFormat) {
	}

	@Override
	protected void dispatchNetworkChange(boolean isAvailable) {
		dispatchContextNetworkChange(isAvailable);
	}
	
	@Override
	public final void dispatchLanguageChange() {
		mAppContextObservable.dispatchLanguageChange();
		for (WeakReference<IAppContextObserver> callBack : mViewContextObservers) {
			if(callBack.get() != null){
				callBack.get().onLanguageChange();
			}
		}
	}
	
	@Override
	public final void dispatchUserLoginStateChange(boolean isLogin){
		mAppContextObservable.dispatchUserLoginStateChange(isLogin);
		for (WeakReference<IAppContextObserver> callBack : mViewContextObservers) {
			if(callBack.get() != null){
				callBack.get().onUserLoginStateChange(isLogin);
			}
		}
	}
	
	@Override
	public final void dispatchContextNetworkChange(boolean isAvailable) {
		mAppContextObservable.dispatchContextNetworkChange(isAvailable);
		for (WeakReference<IAppContextObserver> callBack : mViewContextObservers) {
			if(callBack.get() != null){
				callBack.get().onNetworkChange(isAvailable);
			}
		}
	}
	
	@Override
	public final void registerContextObservable(IAppContextObserver contextCallBack){
		mAppContextObservable.registerContextObservable(contextCallBack);
	}
	
	@Override
	public final void unregisterContextObservable(IAppContextObserver contextCallBack){
		mAppContextObservable.unregisterContextObservable(contextCallBack);
	}
	
	@Override
	public void addViewContextObservable(IAppContextObserver contextCallBack) {
		if(!containsCallBack(contextCallBack)){
			mViewContextObservers.add(new WeakReference<IAppContextObserver>(contextCallBack));
		}
	}
	
	private boolean containsCallBack(IAppContextObserver contextCallBack){
		for (WeakReference<IAppContextObserver> callBack : mViewContextObservers) {
			if(callBack.get() != null && callBack.get().equals(contextCallBack)){
				return true;
			}
		}
		return false;
	}
	
	@Override
	protected void onInitBroadcastReceiver() {
		//初始化退出应用程序监听
		registerBroadcastReceiver(AppBroadcast.ACTION_CLOSE_APP, new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				dispatchExitApp();
			}
		});
		//初始化登录状态变更监听
		registerBroadcastReceiver(AppBroadcast.ACTION_USER_LOGIN_STATE_CHANGE, new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				dispatchUserLoginStateChange(UserData.isLogin());
			}
		});
		registerBroadcastReceiver(AppBroadcast.ACTION_LANGUAGE_CHANGE, new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				dispatchLanguageChange();
			}
		});
	}
	
	@Override
	public BaseDialogViewModel bindDialogViewModel(String type,Object viewModel, Object... extraParams) {
		return DialogUtil.bindDialogViewModel(this_, type, viewModel);
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		MobclickUtil.onPause(this);
	}

	@Override
	protected void onResume() {
		super.onResume();
		MobclickUtil.onResume(this);
		NightModeUtil.check(true);
	}
	
	protected boolean isMainActivity() {
		return false;
	}
	
	@Override
	public void finish() {
		if(!isMainActivity()){
			if(ActivityTaskUtil.isBaseActivity(this,this.getClass())){
				Intent intent = new Intent(this,MainActivity.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
			}
		}
		super.finish();
	}
}
