package com.bk.android.time.app;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;

import com.bk.android.app.observer.IActivityObservable;
import com.bk.android.app.observer.IActivityObserver;
import com.bk.android.binding.app.BindingFragment;
import com.bk.android.time.app.observer.IAppContextObservable;
import com.bk.android.time.app.observer.IAppContextObserver;
import com.bk.android.time.model.BaseDialogViewModel;
import com.bk.android.time.ui.INetLoadView;
import com.bk.android.time.ui.ITitleBar;
import com.bk.android.time.ui.ITitleBar.OnTitleListener;
import com.bk.android.time.ui.IView;
import com.bk.android.time.util.DialogUtil;

public abstract class AbsFragment extends BindingFragment implements IAppContextObserver,IActivityObserver,ITitleBar,INetLoadView, OnTitleListener{
	private OnTitleListener mOnTitleListener = new OnTitleListener() {
		@Override
		public boolean onTitleButtonClick(boolean isLeft) {
			if(getUserVisibleHint() && isAdded()){
				return AbsFragment.this.onTitleButtonClick(isLeft);
			}
			return false;
		}

		@Override
		public boolean onTitleClick() {
			if(getUserVisibleHint() && isAdded()){
				return AbsFragment.this.onTitleClick();
			}
			return false;
		}
	};
	
	public AbsFragment(){
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		if(activity instanceof IAppContextObservable){
			((IAppContextObservable) activity).registerContextObservable(this);
		}
		if(getActivity() instanceof IActivityObservable){
			((IActivityObservable) getActivity()).registerActivityObserver(this);
		}
		addTitleListener(mOnTitleListener);
	}

	@Override
	public void onDetach() {
		super.onDetach();
		if(getActivity() instanceof IAppContextObservable){
			((IAppContextObservable) getActivity()).unregisterContextObservable(this);
		}
		if(getActivity() instanceof IActivityObservable){
			((IActivityObservable) getActivity()).unregisterActivityObserver(this);
		}
	}

	@Override
	public void onNetworkChange(boolean isAvailable) {
		if(isAvailable){
			hideNetSettingView();
		}
	}

	@Override
	public void onLoadTheme() {
		
	}

	@Override
	public void onUserLoginStateChange(boolean isLogin) {
		
	}

	@Override
	public void onLanguageChange() {
		
	}

	@Override
	public void finish() {
		if(getActivity() != null && !getActivity().isFinishing()){
			getActivity().finish();
		}
	}

	@Override
	public void addViewContextObservable(IAppContextObserver contextCallBack) {
		if(getActivity() instanceof IView){
			((IView) getActivity()).addViewContextObservable(contextCallBack);
		}
	}
	
	@Override
	public boolean onBackPressed() {
		return false;
	}
	
	@Override
	public boolean onActivityResume() {
		return false;
	}

	@Override
	public boolean onActivityPause() {
		return false;
	}

	protected LayoutInflater getLayoutInflater() {
		return LayoutInflater.from(getActivity());
	}
	
	@Override
	public void removeTitleView(View view) {
		if(getActivity() instanceof ITitleBar){
			((ITitleBar) getActivity()).removeTitleView(view);
		}
	}

	@Override
	public void setTitleView(View view) {
		if(getActivity() instanceof ITitleBar){
			((ITitleBar) getActivity()).setTitleView(view);
		}
	}

	@Override
	public void setTitle(CharSequence title) {
		if(getActivity() instanceof ITitleBar){
			((ITitleBar) getActivity()).setTitle(title);
		}
	}
	
	@Override
	public void setTitle(int titleId) {
		if(getActivity() instanceof ITitleBar){
			((ITitleBar) getActivity()).setTitle(titleId);
		}
	}

	@Override
	public void resetTitleBar() {
		if(getActivity() instanceof ITitleBar){
			((ITitleBar) getActivity()).resetTitleBar();
		}
	}
	
	@Override
	public View getLeftButtonView() {
		if(getActivity() instanceof ITitleBar){
			return ((ITitleBar) getActivity()).getLeftButtonView();
		}
		return null;
	}
	
	@Override
	public View getRightButtonView() {
		if(getActivity() instanceof ITitleBar){
			return ((ITitleBar) getActivity()).getRightButtonView();
		}		
		return null;
	}
	
	@Override
	public void setLeftButton(String tip, int icon, int bgRes) {
		if(getActivity() instanceof ITitleBar){
			((ITitleBar) getActivity()).setLeftButton(tip, icon, bgRes);
		}
	}

	@Override
	public void setRightButton(String tip, int icon, int bgRes) {
		if(getActivity() instanceof ITitleBar){
			((ITitleBar) getActivity()).setRightButton(tip, icon, bgRes);
		}
	}

	@Override
	public void setLeftButtonImageView(Drawable drawable) {
		if(getActivity() instanceof ITitleBar){
			((ITitleBar) getActivity()).setLeftButtonImageView(drawable);
		}
	}

	@Override
	public void setRightButtonImageView(Drawable drawable) {
		if(getActivity() instanceof ITitleBar){
			((ITitleBar) getActivity()).setRightButtonImageView(drawable);
		}
	}

	@Override
	public void setLeftButtonEnabled(boolean isEnabled) {
		if(getActivity() instanceof ITitleBar){
			((ITitleBar) getActivity()).setLeftButtonEnabled(isEnabled);
		}
	}

	@Override
	public void setRightButtonEnabled(boolean isEnabled) {
		if(getActivity() instanceof ITitleBar){
			((ITitleBar) getActivity()).setRightButtonEnabled(isEnabled);
		}
	}

	@Override
	public void setTitleBarEnabled(boolean isEnabled) {
		if(getActivity() instanceof ITitleBar){
			((ITitleBar) getActivity()).setTitleBarEnabled(isEnabled);
		}
	}

	@Override
	public void addTitleListener(OnTitleListener l) {
		if(getActivity() instanceof ITitleBar){
			((ITitleBar) getActivity()).addTitleListener(l);
		}
	}
	
	@Override
	public boolean onTitleButtonClick(boolean isLeft) {
		return false;
	}
	
	@Override
	public boolean onTitleClick(){
		return false;
	}

	@Override
	public BaseDialogViewModel bindDialogViewModel(String type,
			Object viewModel, Object... extraParams) {
		return DialogUtil.bindDialogViewModel(getActivity(), type, viewModel);
	}
}
