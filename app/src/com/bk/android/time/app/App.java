package com.bk.android.time.app;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.support.multidex.MultiDex;
import android.text.TextUtils;
import android.util.Log;

import com.bk.android.app.BaseApp;
import com.bk.android.assistant.BuildConfig;
import com.bk.android.time.app.resident.ResidentService;
import com.bk.android.time.data.Preferences;
import com.bk.android.time.data.RecordFlieNetUrlData;
import com.bk.android.time.data.UserData;
import com.bk.android.time.data.net.HttpConnect;
import com.bk.android.time.model.BaseDataModel;
import com.bk.android.time.model.BaseDataModel.SimpleLoadCallBack;
import com.bk.android.time.model.lightweight.UserInfoModel;
import com.bk.android.time.update.AppUpdate;
import com.bk.android.time.util.AppFileUtil;
import com.bk.android.time.util.MobclickUtil;
import com.bk.android.time.util.NightModeUtil;
import com.bk.android.time.widget.media.StreamPlayer;
import com.bk.android.util.AppUtil;
import com.tencent.bugly.Bugly;
import com.tencent.bugly.crashreport.CrashReport;
import com.tencent.bugly.crashreport.CrashReport.UserStrategy;

import cn.jpush.android.api.JPushInterface;
import gueei.binding.BindingLog;

public class App extends BaseApp implements Runnable{
//	libpl_droidsonroids_gif_surface.so GIF库
//	libpl_droidsonroids_gif.so GIF库

	private BroadcastReceiver mReceiver;
	private UserInfoModel mUserInfoModel;
	private SimpleLoadCallBack mSimpleLoadCallBack;
	
	public static App getInstance(){
		return (App) BaseApp.getInstance();
	}
	
	@Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);  
        MultiDex.install(this);  
    } 
	
	@Override
	public void onCreate() {
		super.onCreate();
		try {
			startService(new Intent(this, ResidentService.class));
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(!isCurrentProcess(this)){
			return;
		}

		AppUpdate.init(this);
		BindingLog.isDebug = isDebug();
		mUserInfoModel = new UserInfoModel();
		mSimpleLoadCallBack = new SimpleLoadCallBack(){
			@Override
			public boolean onDataChange(String groupKey, BaseDataModel dataModel) {
				return super.onDataChange(groupKey, dataModel);
			}
		};
		mUserInfoModel.addCallBack(mSimpleLoadCallBack);
		mReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				//监听用户状态变化
				if(UserData.isLogin()){
					CrashReport.setUserId(UserData.getUserId());
				} else {//用户退出
					getHandler().removeCallbacks(mSyncUserInfoModel);
				}
			}
		};
		registerReceiver(mReceiver, new IntentFilter(AppBroadcast.ACTION_USER_LOGIN_STATE_CHANGE));

		//腾讯Bugly配置
		UserStrategy strategy = new UserStrategy(getApplicationContext()); //App的策略Bean
		strategy.setAppChannel(getUMengChannel());     //设置渠道
		Bugly.init(this, "cf6ed6c357", isDebug(), strategy);//初始化SDK
		String uid = UserData.getUserId();
		if(!TextUtils.isEmpty(uid)){
			CrashReport.setUserId(uid);
		}else{
			String deviceId = AppUtil.getDeviceId(this);
			CrashReport.setUserId(deviceId == null ? "" : deviceId);
		}
		
		MobclickUtil.init(getApplicationContext());

		RecordFlieNetUrlData.initCache();
		
		Preferences.getInstance().initClickGoToMarketIgnoreDate();
		
		JPushInterface.init(this);

		Log.v("APP", "channel: " + getUMengChannel());

	}

	public boolean isMarketVersion() {
		String channelName = getUMengChannel();
		if (TextUtils.isEmpty(channelName)) {
			return false;
		}
		if (channelName.contains("market_ezloan")) {
			return false;
		}
		if (channelName.contains("market_")) {
			return true;
		}
		return false;
	}

	public String getUMengChannel() {
		return BuildConfig.CHANNEL_PRE + AppUtil.getUMengChannel(this);
	}

	private String getUserId() {
		String uid = UserData.getUserId();
		if (TextUtils.isEmpty(uid)) {
			uid = AppUtil.getDeviceId(this);
		}
		return uid;
	}

	@Override
	public String getAppSdcardDir() {
		return AppFileUtil.getAppPath();
	}
	
	@Override//XXX 发版必须调用父类方法
	public int getApkVersionCode() {
		return super.getApkVersionCode();
	}

	@Override//XXX 发版必须 false
	public boolean isWriteLogToSdcard() {
		return BuildConfig.IS_DEBUG;
	}

	@Override//XXX 发版必须 false
	public boolean isDebug() {
		return BuildConfig.IS_DEBUG;
	}

	@Override//XXX 1.内部刷量 2.自然流量 3.外部刷量 发版必须 2.自然流量
	public String getBrushType() {
		return "2";
	}
	
	@Override//XXX 发版必须 true
	public boolean isNormalServer() {
//		return Preferences.getInstance().isNormalServer();
		return true;
	}
	
	//XXX 发版必须 检查记录数据是否对旧版本进行屏蔽，如果进行屏蔽需要增加版本号数
	public int getRecordDataVersion(){
//		return 0;//2.3.1
		return 2;//2.3.5 ios 先上视频了
	}
	
	public boolean isCheckLegalCopy(){
		return false;
	}
	
	Runnable mSyncUserInfoModel = new Runnable() {
		@Override
		public void run() {
			mUserInfoModel.loadNetUserInfo();
		}
	};
	
	@Override
	public void onEnterApp(Activity activity) {
		HttpConnect.onEnterApp();
		AppFileUtil.initAppPath();
		String userId = UserData.getUserId();
		getHandler().post(this);

	}

	@Override
	public void onExitApp() {
		HttpConnect.onExitApp();
		getHandler().removeCallbacks(mSyncUserInfoModel);
		StreamPlayer.getInstance().stop();
		NightModeUtil.setStart(false);
	}
	
	@Override
	public void run() {
		if(!isLegalCopy()){
			closeApp();
		}
	}
	
	public void closeApp(){
		sendBroadcast(new Intent(AppBroadcast.ACTION_CLOSE_APP));
	}
	
	public static boolean isLegalCopy(){
		Context c = BaseApp.getInstance();try{Signature[] sigs = c.getPackageManager().getPackageInfo(c.getPackageName(), PackageManager.GET_SIGNATURES).signatures;if(sigs != null && sigs[0] != null){String sigStr = sigs[0].toCharsString();if(sigStr != null && sigStr.hashCode() != -1337901221){return false || !App.getInstance().isCheckLegalCopy();}}}catch (Exception e) {}return true;
	}
}
