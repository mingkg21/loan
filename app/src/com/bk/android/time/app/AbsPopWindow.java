package com.bk.android.time.app;

import android.view.Gravity;
import android.view.View;

import com.bk.android.binding.app.BindingPopWindow;
import com.bk.android.time.app.observer.IAppContextObserver;
import com.bk.android.time.model.BaseDialogViewModel;
import com.bk.android.time.ui.IDialogView;
import com.bk.android.time.ui.IView;
import com.bk.android.time.util.DialogUtil;

public abstract class AbsPopWindow extends BindingPopWindow implements IDialogView{
	public AbsPopWindow(View parent) {
		super(parent);
	}
	
	public AbsPopWindow(View parent, int lpWidth, int lpHeight) {
		super(parent, lpWidth, lpHeight);
	}

	@Override
	public void finish() {
		dismiss();
	}
	
	@Override
	public void addViewContextObservable(IAppContextObserver contextCallBack) {
		if(getContext() instanceof IView){
			((IView) getContext()).addViewContextObservable(contextCallBack);
		}
	}

	@Override
	public BaseDialogViewModel bindDialogViewModel(String type,
			Object viewModel, Object... extraParams) {
		return DialogUtil.bindDialogViewModel(getContext(), type, viewModel);
	}

	@Override
	public void show() {
		showAtLocation(Gravity.CENTER);
	}

	@Override
	public void setCancelable(boolean cancel) {
	}

	@Override
	public void setCanceledOnTouchOutside(boolean cancel) {
	}
	
	@Override
	public void setOnCancelListener(OnCancelListener listener) {
	}
}
