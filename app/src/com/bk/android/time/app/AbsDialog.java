package com.bk.android.time.app;

import android.content.Context;
import android.view.Gravity;

import com.bk.android.assistant.R;
import com.bk.android.binding.app.BindingDialog;
import com.bk.android.time.app.observer.IAppContextObserver;
import com.bk.android.time.model.BaseDialogViewModel;
import com.bk.android.time.ui.IDialogView;
import com.bk.android.time.ui.IView;
import com.bk.android.time.util.DialogUtil;

public abstract class AbsDialog extends BindingDialog implements IDialogView{
	public AbsDialog(Context context) {
		super(context);
		init();
	}
	
	protected AbsDialog(Context context, boolean cancelable,OnCancelListener cancelListener) {
		super(context, cancelable, cancelListener);
		init();
	}
	
	public AbsDialog(Context context, int theme) {
		super(context, theme);
		init();
	}
	
	private void init(){
		setCanceledOnTouchOutside(true);
	}
	
	@Override
	public void setGravity(int gravity) {
		if((gravity & Gravity.BOTTOM) == Gravity.BOTTOM){
			getWindow().setWindowAnimations(R.style.DialogBottomAnimation);
		}else{
			getWindow().setWindowAnimations(R.style.DialogAnimation);
		}
		super.setGravity(gravity);
	}

	@Override
	public void finish() {
		dismiss();
	}
	
	@Override
	public void addViewContextObservable(IAppContextObserver contextCallBack) {
		if(getActivity() instanceof IView){
			((IView) getActivity()).addViewContextObservable(contextCallBack);
		}
	}
	
	@Override
	public BaseDialogViewModel bindDialogViewModel(String type,Object viewModel, Object... extraParams) {
		return DialogUtil.bindDialogViewModel(getActivity(), type, viewModel);
	}
}
