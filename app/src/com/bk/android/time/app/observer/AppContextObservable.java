package com.bk.android.time.app.observer;

import java.util.ArrayList;
import java.util.List;

import com.bk.android.app.observer.AbsObservable;


public class AppContextObservable extends AbsObservable<IAppContextObserver> implements IAppContextObservable {
	
	@Override
	public void registerContextObservable(IAppContextObserver contextCallBack) {
		registerObserver(contextCallBack);
	}

	@Override
	public void unregisterContextObservable(IAppContextObserver contextCallBack) {
		unregisterObserver(contextCallBack);
	}
	
	@Override
	public void dispatchUserLoginStateChange(boolean isLogin){
		List<IAppContextObserver> list = new ArrayList<IAppContextObserver>(getObservers());
		for(IAppContextObserver contextCallBack : list){
			contextCallBack.onUserLoginStateChange(isLogin);
		}
	}

	@Override
	public void dispatchContextNetworkChange(boolean isAvailable) {
		List<IAppContextObserver> list = new ArrayList<IAppContextObserver>(getObservers());
		for(IAppContextObserver contextCallBack : list){
			contextCallBack.onNetworkChange(isAvailable);
		}
	}

	@Override
	public void dispatchLanguageChange() {
		List<IAppContextObserver> list = new ArrayList<IAppContextObserver>(getObservers());
		for(IAppContextObserver contextCallBack : list){
			contextCallBack.onLanguageChange();
		}
	}
}
