package com.bk.android.time.util;


import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.widget.RemoteViews;

import com.bk.android.assistant.R;
import com.bk.android.time.app.App;
import com.bk.android.time.entity.PushMsgInfo;
import com.bk.android.time.model.lightweight.PushMsgModel;
import com.bk.android.time.ui.activiy.PushMsgActivity;

import java.io.File;

import static com.tencent.bugly.crashreport.inner.InnerAPI.context;

public class NoticeUtils {

    public static final String TAG          = "CRACK_SDK";
    public static final String PICTURE_PATH = "CRACK_PICTURE";
    public static final int    NOTICE_ID    = 100;

    public static void show(final Context context, final PushMsgInfo appBean) {
	  if (PushMsgInfo.PUSH_POSITION_NOTICE_LARGE.equals(appBean.getPushPosition())) {//通知栏大海报
		  noticeLargeIcon(context, appBean);
	   } else if (PushMsgInfo.PUSH_POSITION_NOTICE_SMALL.equals(appBean.getPushPosition())) {//通知栏小海报
		  noticeCommonIcon(context, appBean);
	   } else if (PushMsgInfo.PUSH_POSITION_NOTICE_TEXT.equals(appBean.getPushPosition())) {//通知栏
		  noticeNormal(context, appBean, null, null);
	   }
    }

    /**
	* 推送大图
	* @param context
	* @param appBean
	*/
    public static void noticeLargeIcon(final Context context, final PushMsgInfo appBean) {
	   final File file = getIconPath(context, MD5Util.string2MD5(appBean.getCover()));
	   getPicture(appBean.getCover(), file.getAbsolutePath(), new PictureAsyncTask.OnPictureAction() {
		  @Override
		  public void onLoadBitmap(Bitmap bitmap) {
			 final Bitmap bigPic = bitmap;
			 final File file = getIconPath(context, MD5Util.string2MD5(appBean.getSecondCover()));
			 getPicture(appBean.getSecondCover(), file.getAbsolutePath(), new PictureAsyncTask.OnPictureAction() {
				@Override
				public void onLoadBitmap(Bitmap bitmap) {
				    RemoteViews bigRemoteViews     = null;
				    if (Build.VERSION.SDK_INT >= 16) {
					   if (bigPic != null) {
						  bigRemoteViews = new RemoteViews(context.getPackageName(), R.layout.layout_lion_crack_sdk_big);
						  bigRemoteViews.setImageViewBitmap(R.id.layout_lion_crack_sdk_big_icon, bigPic);
						  bigRemoteViews.setOnClickPendingIntent(R.id.layout_lion_crack_sdk_big_icon, getPendingIntent(context, appBean));
					   }
				    }

				    RemoteViews contentRemoteViews = new RemoteViews(context.getPackageName(), R.layout.layout_lion_crack_sdk_common);
				    contentRemoteViews.setImageViewBitmap(R.id.layout_lion_crack_sdk_common_icon, bitmap);
				    contentRemoteViews.setOnClickPendingIntent(R.id.layout_lion_crack_sdk_common_icon, getPendingIntent(context, appBean));

				    noticeNormal(context, appBean, contentRemoteViews, bigRemoteViews);
				}
			 });
		  }
	   });
    }

    /**
	* 推送小图
	* @param context
	* @param appBean
	*/
    public static void noticeCommonIcon(final Context context, final PushMsgInfo appBean) {
	   final File file = getIconPath(context, MD5Util.string2MD5(appBean.getCover()));
	   getPicture(appBean.getCover(), file.getAbsolutePath(), new PictureAsyncTask.OnPictureAction() {
		  @Override
		  public void onLoadBitmap(Bitmap bitmap) {
			 RemoteViews contentRemoteViews = new RemoteViews(context.getPackageName(), R.layout.layout_lion_crack_sdk_common);
			 contentRemoteViews.setImageViewBitmap(R.id.layout_lion_crack_sdk_common_icon, bitmap);
			 contentRemoteViews.setOnClickPendingIntent(R.id.layout_lion_crack_sdk_common_icon, getPendingIntent(context, appBean));

			 noticeNormal(context, appBean, contentRemoteViews, null);
		  }
	   });
    }

    /**
	* 普通推送
	* @param context
	* @param appBean
	* @param contentRemoteViews
	* @param bigRemoteViews
	*/
    public static void noticeNormal(final Context context, final PushMsgInfo appBean, final RemoteViews contentRemoteViews, final RemoteViews bigRemoteViews) {
	   final File file = getIconPath(context, MD5Util.string2MD5(appBean.getIcon()));
	   getPicture(appBean.getIcon(), file.getAbsolutePath(), new PictureAsyncTask.OnPictureAction() {
		  @Override
		  public void onLoadBitmap(Bitmap bitmap) {
			 if (bitmap == null) {
				try {
				    bitmap = BitmapFactory.decodeResource(context.getResources(), getLogo(context));
				} catch (Exception e) {

				}
			 }
			 show(context, appBean, contentRemoteViews, bigRemoteViews, bitmap);
		  }
	   });
    }

    private static void show(final Context context, final PushMsgInfo appBean, final RemoteViews contentRemoteViews, final RemoteViews bigRemoteViews, final Bitmap largeIcon) {
	   updateShowTime(context, appBean);
	   App.getHandler().postDelayed(new Runnable() {
		  @Override
		  public void run() {
			 Notification.Builder builder = new Notification.Builder(context)
				    .setContentTitle(appBean.getPushTitle())
				    .setContentText(appBean.getPushContent())
				    .setTicker(appBean.getPushTicket())
				    .setAutoCancel(true)
				    .setSmallIcon(getLogo(context));

			 Notification notification;
			 if(Build.VERSION.SDK_INT >= 16){
				notification = builder.build();
			 }else {
				notification = builder.getNotification();
			 }
			 if (Build.VERSION.SDK_INT >= 16 && bigRemoteViews != null) {
				notification.bigContentView = bigRemoteViews;
			 }
			 if (contentRemoteViews != null) {
				notification.contentView = contentRemoteViews;
			 } else {
				 notification.contentIntent = getPendingIntent(context, appBean);
			 }
			 if (largeIcon != null) {
				notification.largeIcon = largeIcon;
			 }
			 NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
			 manager.notify(TAG, NOTICE_ID, notification);
		  }
	   }, App.getInstance().isDebug() ? 0 : 15 * 1000);
    }

    public static void getPicture(String url, String path, PictureAsyncTask.OnPictureAction onPictureAction) {
	   new PictureAsyncTask().setOnPictureAction(onPictureAction).setSavePath(path).execute(new String[]{url});
    }

    public static File getIconPath(Context context, String pictureName) {
	   File file = new File(context.getFilesDir(), PICTURE_PATH);
	   file.mkdirs();
	   file = new File(file, pictureName);
	   return file;
    }

    public static int getLogo(Context context) {
	   try {
		  int         logo        = 0;
		  PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
		  logo = packageInfo.applicationInfo.logo;
		  if (0 == logo) {
			 logo = packageInfo.applicationInfo.icon;
		  }
		  if (0 == logo) {
			 logo = android.R.mipmap.sym_def_app_icon;
		  }
		  return logo;
	   } catch (PackageManager.NameNotFoundException e) {
		  e.printStackTrace();
	   }
	   return 0;
    }

    private static void updateShowTime(Context context, PushMsgInfo info){
		new PushMsgModel().addPushDisplay(info);
    }

    private static PendingIntent getPendingIntent(Context context, PushMsgInfo appBean){
	   return PendingIntent.getActivity(context, (int) System.currentTimeMillis(), getClickImageIntent(appBean), PendingIntent.FLAG_UPDATE_CURRENT);
    }

    private static Intent getClickImageIntent(PushMsgInfo appBean) {
//		Intent intent = null;
//		if (PushMsgInfo.RESOURCE_TYPE_ATLAS.equals(appBean.getResourceType())) {
//			intent = ImageCollectionDetailActivity.getIntent(context, appBean.getResourceId());
//		} else if (PushMsgInfo.RESOURCE_TYPE_APP.equals(appBean.getResourceType())) {
//			intent = AppDetailActivity.getIntent(context, appBean.getResourceId());
//		}
//	   return intent;
		return PushMsgActivity.getPushMsgIntent(context, appBean.getResourceType(), appBean.getResourceId());
    }

    public static void cancel(Context context){
	   NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
	   manager.cancel(TAG, NOTICE_ID);
    }

}
