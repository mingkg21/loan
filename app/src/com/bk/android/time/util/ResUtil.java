package com.bk.android.time.util;

import com.bk.android.assistant.R;

/**
 * 定义通用资源type，使R文件耦合限制在这个类内部
 * @author linyiwei
 */
public class ResUtil {
//	public static int STR
	public static int STR_APP_NAME = R.string.app_name;
	public static int STR_TIP_ERR_SERVER = R.string.tip_err_server;
	public static int STR_TIP_ERR_LOGIN = R.string.tip_err_login;
	public static int STR_TIP_ON_NET = R.string.tip_on_net;
	public static int STR_TIP_LOADING = R.string.tip_loading;
	public static int STR_BTN_TEXT_SUBMIT = R.string.btn_text_submit;
	public static int STR_BTN_TEXT_CONFIRM = R.string.btn_text_confirm;
	public static int STR_BTN_TEXT_CANCEL = R.string.btn_text_cancel;
	public static int STR_TIP_DEL_SUCCEED = R.string.tip_del_succeed;
	public static int STR_TIP_DEL_FAIL = R.string.tip_del_fail;
	public static int STR_TIP_SUBMIT_SUCCEED = R.string.tip_submit_succeed;
	public static int STR_TIP_SAVE_SUCCEED = R.string.tip_save_succeed;
	public static int STR_TIP_NICKNAME_REPEAT = R.string.tip_nickname_repeat;
	public static int STR_TIP_DISPLAY_TITLE = R.string.tip_display_title;
	public static int STR_TIP_LOGIN_FAIL = R.string.tip_login_fail;
	public static int STR_TIP_LOGIN_SUCCEED = R.string.tip_login_succeed;
	public static int STR_TIP_REGISTER_FAIL = R.string.tip_register_fail;
	public static int STR_TIP_REGISTER_SUCCEED = R.string.tip_register_succeed;
	public static int STR_TIP_ADD_SUBMIT_SUCCEED = R.string.tip_add_submit_succeed;
	public static int STR_TIP_FLOOR = R.string.tip_floor;
	public static int STR_TIP_ANONYMITY = R.string.tip_anonymity;

//	public static int COL
	public static int COL_COM_1 = R.color.com_color_1;
	public static int COL_COM_2 = R.color.com_color_2;
	public static int COL_COM_3 = R.color.com_color_3;
	public static int COL_COM_4 = R.color.com_color_4;
	public static int COL_COM_5 = R.color.com_color_5;
	public static int COL_COM_6 = R.color.com_color_6;
	public static int COL_COM_7 = R.color.com_color_7;
	public static int COL_COM_8 = R.color.com_color_8;

//	public static int DIM
	public static int DIM_FONT_COMMON_0 = R.dimen.font_common_0;
	public static int DIM_FONT_COMMON_1 = R.dimen.font_common_1;
	public static int DIM_FONT_COMMON_2 = R.dimen.font_common_2;
	public static int DIM_FONT_COMMON_3 = R.dimen.font_common_3;
	public static int DIM_FONT_COMMON_4 = R.dimen.font_common_4;
	public static int DIM_FONT_COMMON_5 = R.dimen.font_common_5;
	public static int DIM_FONT_COMMON_6 = R.dimen.font_common_6;
	public static int DIM_FONT_COMMON_7 = R.dimen.font_common_7;

//	public static int LAY
	
//	public static int DRA
}
