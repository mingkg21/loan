package com.bk.android.time.util;

import android.app.Application;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.hardware.SensorManager;
import android.net.ConnectivityManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.telephony.CellLocation;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.WindowManager;

import com.bk.android.time.app.App;
import com.bk.android.util.ApnUtil;
import com.bk.android.util.AppUtil;

import org.json.JSONObject;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import cn.jpush.android.api.JPushInterface;

public class MachineInfoUtil {

    private static final String ARM = "armeabi";

    /**
     * 手机信息管理实例
     */
    private static MachineInfoUtil mInst = null;

    /**
     * LOG标识
     */
    private static final String TAG = "MachineInfo";

    /**
     * IMEI（国际移动设备唯一识别符）
     */
    public String IMEI = "0000000000000000";

    /**
     * IMSI（国际移动注册用户认证号码）
     */
    public String IMSI = "";

    /**
     * 生产商，如HTC
     */
    public String MANUFACTURER = "";

    /**
     * 型号别名
     */
    public String MODEL = "";

    /**
     * 设备类型，如passion
     */
    public String DEVICE_TYPE = "";

    /**
     * SDK 版本号
     */
    public int SDK_VERSION = 3;
    /**
     * SDK 版本号
     */
    public String SDK_;

    /**
     * 像素密度
     */
    public int DENSITY = 160;

    /**
     * 网络连接类型
     */
    public String CONNECT_TYPE = "";

    /**
     * Android 序列号
     */
    public String ANDROID_ID = "";

    /**
     * 硬件
     */
    public String HARDWARE = "default";

    //ro.build.display.id
    public String DISPLAY_ID = "unknown";

    //4.2.2
    public String SDK_VERSION_STR = "unknown";

    public String BUILD_BOARD = "unknown";

    /**
     * 分辨率
     */
    public String RESOLUTION = "";

    /**
     * 版本号
     */
    public int SSH_CLIENT_VERSION_CODE = 0;

    /**
     * 版本号
     */
    public String SSH_CLIENT_VERSION_NAME;

    /**
     * CPU平台
     */
    public String CPU_PLATFORM = "";

    /**
     * 系统上下文变量
     */
    private Context mContext;

    public String macAddress;

    public boolean mIsArmeabi;

    private MachineInfoUtil(Context context) {
        mContext = context;
        initInfo();
    }

    private void initInfo() {
        IMEI = checkString(getIMEI());
        IMSI = checkString(getIMSI());
        ANDROID_ID = checkString(getANDROID_ID());
        int connectType = ApnUtil.getConnectType(mContext);
        if (connectType == ConnectivityManager.TYPE_WIFI) {
            CONNECT_TYPE = "wifi";
        } else {
            CONNECT_TYPE = "2g/3g";
        }
        SSH_CLIENT_VERSION_CODE = getVersionCode(mContext);
        SSH_CLIENT_VERSION_NAME = checkString(getVersionName(mContext));
        CPU_PLATFORM = checkString(android.os.Build.CPU_ABI);
        mIsArmeabi = CPU_PLATFORM.contains(ARM);
        RESOLUTION = checkString(getResolution());
        getOtherInfo();

        macAddress = checkString(getMacAddress());
    }

    private String checkString(String str) {
        if (TextUtils.isEmpty(str)) return "null";
        return str;
    }

    private void getOtherInfo() {
        try {
            DENSITY = mContext.getResources().getDisplayMetrics().densityDpi;
            SDK_VERSION = android.os.Build.VERSION.SDK_INT;
            SDK_ = android.os.Build.VERSION.RELEASE;//系统版本
            checkString(SDK_);

            MANUFACTURER = checkString(android.os.Build.MANUFACTURER);
            MODEL = checkString(android.os.Build.MODEL);
            DEVICE_TYPE = checkString(android.os.Build.DEVICE);
            HARDWARE = checkString(android.os.Build.HARDWARE);
            DISPLAY_ID = checkString(android.os.Build.DISPLAY);
            SDK_VERSION_STR = checkString(android.os.Build.VERSION.RELEASE);
            BUILD_BOARD = checkString(checkString(android.os.Build.BOARD));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    //获取prop信息
//    public String getProp(String key) {
//	   String content = Utils.execCmd("getprop " + key);
//	   List<String> strList = StrUtils.splitStr(content, "\r\n", false);
//	   if (strList != null) {
//		  if (strList.size() == 1)
//			 return strList.get(0);
//		  else if (strList.size() > 1) {
//			 return strList.get(strList.size() - 1);
//		  }
//	   }
//	   return "unknown";
//    }

    private String getIMEI() {
        TelephonyManager tm = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
        String imei = tm.getDeviceId();
        if (TextUtils.isEmpty(imei)) {
            imei = tm.getSimSerialNumber();
        }
        if (TextUtils.isEmpty(imei)) {
            imei = "0000000000000000";
        }
        return imei;
    }

    private String getIMSI() {
        TelephonyManager tm = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
        String imsi = tm.getSubscriberId();
        if (TextUtils.isEmpty(imsi)) {
            imsi = "";
        }
        return imsi;
    }

    private int getPhoneType() {
        TelephonyManager tm = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
        return tm.getPhoneType();
    }

    public static MachineInfoUtil getInstance(Context context) {
        if (null == mInst) {
            if (!(context instanceof Application)) {
                context = context.getApplicationContext();
            }
            mInst = new MachineInfoUtil(context);
        }
        return mInst;
    }

    private String getANDROID_ID() {
//	   return ReflectionUtils.getAndroidID();
        return "";
    }

    private String getResolution() {
        DisplayMetrics dm = new DisplayMetrics();
        WindowManager mgr = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
        mgr.getDefaultDisplay().getMetrics(dm);
        return "" + dm.heightPixels + "x" + dm.widthPixels;
    }


    static public boolean externalMemoryAvailable() {
        return android.os.Environment.getExternalStorageState().equals(
                android.os.Environment.MEDIA_MOUNTED);
    }

    /**
     * 获取可用外部存储容量
     *
     * @return
     * @time 2012-2-28 | 下午01:57:40
     */
    static public long getAvailableExternalMemorySize() {
        if (externalMemoryAvailable()) {
            File path = Environment.getExternalStorageDirectory();
            StatFs stat = new StatFs(path.getPath());
            long blockSize = stat.getBlockSize();
            long availableBlocks = stat.getAvailableBlocks();
            return availableBlocks * blockSize;
        } else {
            return -1;
        }
    }

    /**
     * 获取可用内部存储容量
     *
     * @return
     * @author xxj
     * @time 2012-2-28 | 下午01:57:59
     */
    static public long getAvailableInternalMemorySize() {
        File path = Environment.getDataDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize = stat.getBlockSize();
        long availableBlocks = stat.getAvailableBlocks();
        return availableBlocks * blockSize;
    }

    /**
     * 获取当前软件版本号
     *
     * @param context
     * @return
     * @author xxj
     * @time 2012-2-28 | 下午02:13:52
     */
    public static int getVersionCode(Context context) {
        int versionCode = 0;
        PackageManager pm = context.getPackageManager();
        try {
            PackageInfo pi = pm.getPackageInfo(context.getPackageName(), 0);
            versionCode = pi.versionCode;
        } catch (NameNotFoundException e) {
        }
        return versionCode;
    }


    /**
     * 获取当前软件版本号
     *
     * @param context
     * @return
     * @author xxj
     * @time 2012-2-28 | 下午02:13:52
     */
    public static String getVersionName(Context context) {
        String versionName = "";
        PackageManager pm = context.getPackageManager();
        try {
            PackageInfo pi = pm.getPackageInfo(context.getPackageName(), 0);
            versionName = pi.versionName;
        } catch (NameNotFoundException e) {
        }
        return versionName;
    }

    private String getMacAddress() {
        WifiManager wifiManager = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
        WifiInfo info = wifiManager.getConnectionInfo();
        if (info != null) {
            return info.getMacAddress();
        }
        return null;
    }

    private JSONObject getCellInfo() {
        JSONObject cell = new JSONObject();
        TelephonyManager mTelNet = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
        CellLocation mCellLocation = mTelNet.getCellLocation();
        GsmCellLocation location = null;
        if (mCellLocation instanceof GsmCellLocation) {
            location = (GsmCellLocation) mTelNet.getCellLocation();
        }
        if (location == null) {
            return null;
        }

        int mcc = 0, mnc = 0, cid = 0, lac = 0;

        String operator = mTelNet.getNetworkOperator();
        if (operator != null && !operator.equals("")) {
            mcc = Integer.parseInt(operator.substring(0, 3));
            mnc = Integer.parseInt(operator.substring(3));
            cid = location.getCid();
            lac = location.getLac();
        }

        try {
            cell.put("mcc", mcc);
            cell.put("mnc", mnc);
            cell.put("lac", lac);
            cell.put("cid", cid);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return cell;
    }

    /**
     * Get device name, manufacturer + model
     *
     * @return device name
     */
    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;

        if (model.startsWith(manufacturer)) {
            return StrUtils.capitalize(model);
        } else {
            return StrUtils.capitalize(manufacturer) + " " + model;
        }
    }

    /**
     * To determine whether it contains a gyroscope
     *
     * @return
     */
    public static boolean isHaveGravity(Context context) {
        SensorManager manager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        if (manager == null) {
            return false;
        }
        return true;
    }

    public JSONObject getClientHeader() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("device_no", encode(IMEI));
            jsonObject.put("os_version", encode(SDK_));
            jsonObject.put("channel_code", SSH_CLIENT_VERSION_CODE);
            jsonObject.put("client_version_name", encode(SSH_CLIENT_VERSION_NAME));
            jsonObject.put("client_version_code", SSH_CLIENT_VERSION_CODE);

            jsonObject.put("client_bundle_id", "android");

            jsonObject.put("client_channel_name", encode(App.getInstance().getUMengChannel()));
            jsonObject.put("client_channel_id", encode(AppUtil.getChannelId(mContext)));

            jsonObject.put("push_device_id", encode(JPushInterface.getRegistrationID(mContext)));
            jsonObject.put("client_package_name", App.getInstance().getPackageName());

//            jsonObject.put("client_cpu_architecture", encode(CPU_PLATFORM));
//            jsonObject.put("client_mac_addr", encode(macAddress));
//            jsonObject.put("client_bundle_id", encode(mContext.getPackageName()));
//            jsonObject.put("model_name", encode(MODEL));
//            jsonObject.put("manufacturer", encode(Build.MANUFACTURER));
//            jsonObject.put("module_name", encode(Build.PRODUCT));
//            jsonObject.put("imsi", encode(IMSI));
//            jsonObject.put("language", encode(Locale.getDefault().getLanguage()));
//            jsonObject.put("is_mobiledevice", true);
//            jsonObject.put("phone_type", getPhoneType());
//            try {
//                jsonObject.put("cell", getCellInfo());
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            jsonObject.put("resolution", encode(getResolution()));
//            jsonObject.put("have_wifi", ApnUtil.isWifiWork(mContext));
//
//            try {
//                LocationManager locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
//                jsonObject.put("have_gps", locationManager == null ? false : true);
//            } catch (Exception e) {
//            }
//
//            jsonObject.put("device_name", encode(getDeviceName()));
//            jsonObject.put("have_gravity", isHaveGravity(mContext));
//
//            try {
//                BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
//                jsonObject.put("have_bt", adapter == null ? false : true);
//            } catch (Exception e) {
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//	   Logger.i(jsonObject.toString());
        return jsonObject;
    }

    private String encode(String s) {
        try {
            return URLEncoder.encode(s, "utf-8");
        } catch (UnsupportedEncodingException e) {
            return URLEncoder.encode(s);
        }
    }

}
