package com.bk.android.time.util;

import java.util.Timer;
import java.util.TimerTask;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;

import com.bk.android.time.app.App;
import com.bk.android.util.AppUtil;

public class NightModeUtil {
	private static boolean isStart;
	private static WindowManager sWindowManager;
	private static View sRootView;
	private static LayoutParams sParams;
	
	private static Timer sTimer = new Timer();
	
	private static TimerTask sTimerTask = new TimerTask() {
		@Override
		public void run() {
			check(AppUtil.isTopAppTask(App.getInstance(), App.getInstance().getPackageName()));
		}
	};
	
	public static void check(boolean isAppShow){
		if(isStart){
			if(!isAppShow){
				if(sRootView.isShown()){
					App.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							sRootView.setVisibility(View.GONE);
						}
					});
				}
			}else{
				if(!sRootView.isShown()){
					App.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							sRootView.setVisibility(View.VISIBLE);
						}
					});
				}
			}
		}
	}
	
	public static void setStart(boolean start){
		isStart = start;
		initView();
		if(isStart){
			sRootView.setVisibility(View.VISIBLE);
			if(sRootView.getParent() == null){
				sWindowManager.addView(sRootView, sParams);
			}
		}else{
			if(sRootView.getParent() != null){
				sWindowManager.removeView(sRootView);
			}
		}
	}
	
	public static boolean isStart(){
		return isStart;
	}
	
	private static void initView(){
		if(sWindowManager == null){
			sTimer.scheduleAtFixedRate(sTimerTask, 1000, 1000);

			sParams = new LayoutParams();
	        // 类型
			sParams.type = LayoutParams.TYPE_TOAST;

			// FLAG_NOT_TOUCH_MODAL不阻塞事件传递到后面的窗口
	        // 设置 FLAG_NOT_FOCUSABLE 悬浮窗口较小时，后面的应用图标由不可长按变为可长按
			sParams.flags = LayoutParams.FLAG_NOT_FOCUSABLE | LayoutParams.FLAG_NOT_TOUCH_MODAL | LayoutParams.FLAG_LAYOUT_IN_SCREEN | LayoutParams.FLAG_NOT_TOUCHABLE;
	        // 不设置这个弹出框的透明遮罩显示为黑色
			sParams.format = PixelFormat.TRANSLUCENT;

			sParams.width = LayoutParams.MATCH_PARENT;
			sParams.height = LayoutParams.MATCH_PARENT;

			sParams.gravity = Gravity.CENTER;
	        // 获取应用的Context
	        Context context = App.getInstance();
	        // 获取WindowManager
	        sWindowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
	        sRootView = new View(context);
	        sRootView.setBackgroundColor(Color.parseColor("#66000000"));
		}
	}
}
