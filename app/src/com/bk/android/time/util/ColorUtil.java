package com.bk.android.time.util;

import java.util.HashMap;

import android.graphics.Color;
import android.text.TextUtils;

public class ColorUtil {
	public static Integer parseHtmlColor(String color) {
		if (TextUtils.isEmpty(color)){
			return null;
		}
		color = color.trim();
		Integer i = COLORS.get(color.toLowerCase());
		if (i != null) {
			return i;
		} else if(color.indexOf("rgb") != -1){
			try {
				i = convertRGBAToInt(color);
			} catch (Exception nfe) {
				nfe.printStackTrace();
			}
		} else if(color.indexOf("#") != -1){
			try {
				i = convertValueToInt(color);
			} catch (Exception nfe) {
				nfe.printStackTrace();
			}
		}
		return i;
	}
	
	private static final Integer convertRGBAToInt(String color){
		int start = color.indexOf("(") + 1;
		int end = color.indexOf(")");
		String[] colors = null;
		if(0 <= start && start <= end && end <= color.length()){
			colors = color.substring(start, end).split(",");
		}
		if(colors != null){
			for (int i = 0; i < colors.length; i++) {
				colors[i] = colors[i].trim();
			}
			if(colors.length == 3){
				return Color.rgb(convertColorValue(colors[0]), convertColorValue(colors[1])
						, convertColorValue(colors[2]));
			}else if(colors.length == 4){
				String alpha = colors[3];
				if(!TextUtils.isEmpty(alpha) && alpha.charAt(0) == '.'){
					alpha = "0" + alpha;
				}
				return Color.argb((int) (255 * Float.valueOf(alpha)),convertColorValue(colors[0])
						, convertColorValue(colors[1]), convertColorValue(colors[2]));
			}
		}
		return null;
	}
	
	private static int convertColorValue(String value){
		if(TextUtils.isEmpty(value)){
			return 0;
		}
		if(value.length() > 1 && value.indexOf("%") != -1){
			return (int) (Float.valueOf(value.substring(0, value.length() - 1)) * 255 / 100);
		}else{
			return Integer.valueOf(value);
		}
	}
	
	public static int getHtmlColor(String color) {
		return getHtmlColor(color, 0);
	}
	
	public static int getHtmlColor(String color,int defaultColor) {
		Integer i = parseHtmlColor(color);
		return i != null ? i : defaultColor;
	}

	private static final Integer convertValueToInt(String charSeq) {
		StringBuffer nm = new StringBuffer(charSeq);
		if ('#' == nm.charAt(0)) {
			if(nm.length() == 4){
				nm.insert(3, nm.charAt(3));
				nm.insert(2, nm.charAt(2));
				nm.insert(1, nm.charAt(1));
			}
		}
		try {
	        return Color.parseColor(nm.toString());
		} catch (IllegalArgumentException e) {
			return null;
		}
	}
	
	private static HashMap<String, Integer> COLORS = buildColorMap();

	private static HashMap<String, Integer> buildColorMap(){
		HashMap<String, Integer> map = new HashMap<String, Integer>();
		map.put("aqua", 0xFF00FFFF);
		map.put("black", 0xFF000000);
		map.put("blue", 0xFF0000FF);
		map.put("fuchsia", 0xFFFF00FF);
		map.put("green", 0xFF008000);
		map.put("grey", 0xFF808080);
		map.put("lime", 0xFF00FF00);
		map.put("maroon", 0xFF800000);
		map.put("navy", 0xFF000080);
		map.put("olive", 0xFF808000);
		map.put("purple", 0xFF800080);
		map.put("red", 0xFFFF0000);
		map.put("silver", 0xFFC0C0C0);
		map.put("teal", 0xFF008080);
		map.put("white", 0xFFFFFFFF);
		map.put("yellow", 0xFFFFFF00);
		map.put("transparent", 0);
		return map;
	}
}
