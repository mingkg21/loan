package com.bk.android.time.util;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnPreDrawListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.bk.android.assistant.R;

/** 对话框工具
 * @author mingkg21
 * @email mingkg21@gmail.com
 * @date 2011-08-15
 */
public final class ToastUtil {
	private static Toast sToast;
	private static Toast sViewToast;

	public static void init(Context context){
		if(sToast == null){
			sToast = new Toast(context.getApplicationContext());
		}
		if(sViewToast == null){
			sViewToast = new Toast(context.getApplicationContext());
		}
	}
	
	public static void showToast(Context context, int contentId){
		markToast(context, context.getString(contentId), Gravity.CENTER_HORIZONTAL, Toast.LENGTH_SHORT).show();
	}
	
	public static void showLongToast(Context context, int contentId){
		markToast(context, context.getString(contentId), Gravity.CENTER_HORIZONTAL, Toast.LENGTH_LONG).show();
	}
	
	public static void showLongToast(Context context, String content){
		markToast(context, content, Gravity.CENTER_HORIZONTAL, Toast.LENGTH_LONG).show();
	}
	
	public static void showToast(Context context, String content){
		markToast(context, content, Gravity.CENTER_HORIZONTAL, Toast.LENGTH_SHORT).show();
	}
	
	private static Toast markToast(Context context, String contentStr, int gravity,int duration){
		if(sToast == null){
			sToast = new Toast(context.getApplicationContext());
		}
		Toast toast = sToast;
		toast.setDuration(duration);
		toast.setGravity(Gravity.CENTER, 0, 0);
		LayoutInflater inflater = LayoutInflater.from(context.getApplicationContext());
		View view = inflater.inflate(R.layout.com_toast_lay, null);
		toast.setView(view);
		TextView content = (TextView) view.findViewById(R.id.toast_content);
		content.setGravity(gravity);
		if(!TextUtils.isEmpty(contentStr)){
			content.setVisibility(View.VISIBLE);
			content.setText(contentStr);
		}else{
			content.setVisibility(View.GONE);
		}
		return toast;
	}
	
	public static Toast showToast(Context context,View view ,int duration ){
		Toast toast = markToast(context, view, duration);
		toast.show();
		return toast;
	}
	
	public static Toast markToast(Context context,View view ,int duration){
		if(sViewToast == null){
			sViewToast = new Toast(context.getApplicationContext());
		}
		Toast toast = sViewToast;
		toast = new Toast(context.getApplicationContext());
		toast.setDuration(duration);
		toast.setGravity(Gravity.NO_GRAVITY, 0, 0);
		toast.setView(view);
		return toast;
	}
	
	public static void dismissToast(){
		if(sToast != null){
			sToast.cancel();
		}
		if(sViewToast != null){
			sViewToast.cancel();
		}
	}
	
	public static Toast showCoinToast(Context context, int coin){
		LayoutInflater inflater = LayoutInflater.from(context.getApplicationContext());
		View view = inflater.inflate(R.layout.com_toast_coin_lay, null);
		TextView content = (TextView) view.findViewById(R.id.toast_content);
		content.setText("+" + coin);
		Toast toast = markToast(context, view, Toast.LENGTH_SHORT);
		toast.show();
		return toast;
	}

	public static void showPopupTip(final Activity context, final View parent, final int resId, final Runnable runnable) {
		if(context.isFinishing()){
			return;
		}
		final Runnable showRunnable = new Runnable() {
			@Override
			public void run() {
				if(!this.equals(parent.getTag())){
					return;
				}
				final PopupWindow popupWindow  = new PopupWindow(context);
				final View view = LayoutInflater.from(context).inflate(R.layout.uniq_popup_tip, null);
				ImageView imageView = (ImageView) view.findViewById(R.id.content_iv);
				imageView.setImageResource(resId);
				final Animation animation = AnimationUtils.loadAnimation(context, R.anim.popup_tip);
				imageView.setAnimation(animation);
				view.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						if (runnable != null) {
							runnable.run();
						}
						if (popupWindow != null) {
							popupWindow.dismiss();
						}
					}
				});

				popupWindow.setWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
				popupWindow.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
				popupWindow.setContentView(view);
				popupWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
				popupWindow.setOutsideTouchable(true);

				popupWindow.getContentView().measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
				int popupWidth = popupWindow.getContentView().getMeasuredWidth();
				int popupHeight =  popupWindow.getContentView().getMeasuredHeight();
				int[] location = new int[2];
				parent.getLocationOnScreen(location);
				popupWindow.showAtLocation(parent, Gravity.NO_GRAVITY, (location[0] + parent.getWidth() / 2) - popupWidth / 2,
						location[1] - popupHeight);
				if(this.equals(parent.getTag())){
					parent.setTag(null);
				}
			}
		};
		if(parent.getTag() instanceof Runnable){
			parent.removeCallbacks((Runnable) parent.getTag());
		}
		parent.setTag(showRunnable);
		if(!isNeedPostRunnable(parent)){
			parent.postDelayed(showRunnable, 500);
		}else{
			parent.getViewTreeObserver().addOnPreDrawListener(new OnPreDrawListener() {
				@Override
				public boolean onPreDraw() {
					if(!context.isFinishing()){
						if(isNeedPostRunnable(parent)){
							parent.invalidate();
						}else{
							parent.getViewTreeObserver().removeOnPreDrawListener(this);
							parent.postDelayed(showRunnable, 500);
						}
					}else{
						parent.getViewTreeObserver().removeOnPreDrawListener(this);
					}
					return true;
				}
			});
			parent.invalidate();
		}
	}
	
	private static boolean isNeedPostRunnable(View parent){
		if(parent.getWindowToken() == null){
			return true;
		}
		if(parent.isLayoutRequested() && (parent.getWidth() == 0 || parent.getHeight() == 0)){
			return true;
		}
		return false;
	}
}
