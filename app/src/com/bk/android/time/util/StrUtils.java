package com.bk.android.time.util;

import java.util.ArrayList;
import java.util.List;

public class StrUtils {

    static final String digits         = "0123456789ABCDEF";
    public final static String DEFAULT_ENCODE = "utf8";

    public static int countStr(String parent, String sep) {
	   if (sep == null || sep.length() == 0) return 0;
	   String p     = parent;
	   int    count = 0;
	   while (true) {
		  int idx = p.indexOf(sep);
		  if (idx == -1) break;
		  else {
			 count++;
			 p = p.substring(idx + sep.length());
		  }
	   }
	   return count;
    }

    public static String removeHost(String url) {
	   if (url.startsWith("http://")) {
		  url = url.substring(7);
	   } else if (url.startsWith("https://")) {
		  url = url.substring(8);
	   }
	   for (int idx = 0; idx < url.length(); ++idx) {
		  if (url.charAt(idx) == '/') {
			 url = url.substring(idx);
			 break;
		  }
	   }
	   return url;
    }

    public static int skipEmptyLetters(String str) {
	   return skipEmptyLetters(str, 0);
    }

    public static int skipEmptyLetters(String str, int start) {
	   int idx = start;
	   for (; idx < str.length(); ++idx) {
		  if (!isEmptyLetter(str.charAt(idx)))
			 break;
	   }
	   return idx;
    }

    public static String removeSuffix(String str) {
	   int index = str.lastIndexOf('.');
	   if (index < 0) return str;
	   else return str.substring(0, index);
    }


    public static boolean isEmptyLetter(char ch) {
	   return ch == ' ' || ch == '\t' || ch == '\n' || ch == '\r';
    }

    public static String formatStr(String str) {
	   StringBuilder builder = new StringBuilder();
	   char          ch      = 0;
	   for (int idx = 0; idx < str.length(); ++idx) {
		  ch = str.charAt(idx);
		  if (ch == '\r')
			 continue;
		  builder.append(ch);
	   }
	   String result = builder.toString();
	   return result.trim();
	   // return builder.toString();
    }

    public static List<String> splitStr(String parent, String sep,
										boolean allowEmpty) {
	   List<String> strList = new ArrayList<String>();
	   int          pre     = 0;
	   while (pre < parent.length()) {
		  int cur = pre;
		  for (; cur < parent.length() && sep.indexOf(parent.charAt(cur)) == -1; ++cur) ;
		  if (allowEmpty || cur > pre) {
			 strList.add(parent.substring(pre, cur));
		  }
		  pre = cur + 1;
	   }
	   return strList;
    }


    public static List<String> splitStrWithSepStr(String parent, String sep, boolean allowEmpty) {
	   List<String> strList = new ArrayList<String>();
	   int          pos     = 0;
	   while (pos < parent.length()) {
		  int idx = parent.indexOf(sep, pos);
		  if (idx == -1) {
			 strList.add(parent.substring(pos));
			 break;
		  }
		  if (allowEmpty || idx > pos) {
			 strList.add(parent.substring(pos, idx));
		  }
		  pos = idx + sep.length();
	   }
	   return strList;
    }

    public static String trimStr(String str, String sep) {
	   for (int idx = 0; idx < str.length(); ++idx) {
		  char ch = str.charAt(idx);
		  if (sep.indexOf(ch) != -1) {
			 return str.substring(0, idx);
		  }
	   }
	   return str;
    }

    public static String retriveDigit(String str) {
	   StringBuilder builder = new StringBuilder();
	   for (int idx = 0; idx < str.length(); ++idx) {
		  char ch = str.charAt(idx);
		  if (ch >= '0' && ch <= '9') {
			 builder.append(ch);
		  }
	   }
	   return builder.toString();
    }

    public static String getSubStr(String str, int start, int end) {
	   if (end > str.length())
		  end = str.length();
	   if (start < 0)
		  start = 0;
	   return str.substring(start, end);
    }

    public static String retrF(String str) {
	   str = str.trim();
	   int idx = str.indexOf(' ');
	   if (idx == -1) return str;
	   else {
		  return str.substring(0, idx);
	   }
    }

    public static String changeNullStr(String str) {
	   if ("null".equals(str)) {
		  return "";
	   }
	   return str;
    }

    public static String listToStr(List<String> strList, char sep, boolean nullToEmpty) {
	   if (strList == null && nullToEmpty) {
		  return "";
	   }
	   if (strList == null && !nullToEmpty) {
		  return null;
	   }
	   StringBuilder builder = new StringBuilder();
	   for (int idx = 0; idx < strList.size(); ++idx) {
		  builder.append(strList.get(idx));
		  if (idx < strList.size() - 1) {
			 builder.append(sep);
		  }
	   }
	   return builder.toString();
    }

    /**
	* Capitalize the first letter
	*
	* @param s model,manufacturer
	* @return Capitalize the first letter
	*/
    public static String capitalize(String s) {
	   if (s == null || s.length() == 0) {
		  return "";
	   }
	   char first = s.charAt(0);
	   if (Character.isUpperCase(first)) {
		  return s;
	   } else {
		  return Character.toUpperCase(first) + s.substring(1);
	   }
    }

//	//从指定文件夹读取内容
//	public static List<String> getSendSharedStrings(Context context,String FileName) {
//		List<String> list = new ArrayList<String>();
//		try {
//			InputStream is = context.getAssets().open(FileName);
//			BufferedReader br = new BufferedReader(new InputStreamReader(is,DEFAULT_ENCODE));
//			String ss = null;
//			while ((ss = br.readLine()) != null) {
//				list.add(ss);
//			}
//			br.close();
//			is.close();
//		} catch (Exception e) {
//			Logger.v("StrUtils error:%s", new Object[]{e.getMessage()});
//		}
//		return list;
//	}

//	public static String UrlEncode(String s) {
//		StringBuilder buf = new StringBuilder(s.length() + 16);
//		for (int i = 0; i < s.length(); ++i) {
//			char ch = s.charAt(i);
//			if (ch == '+') {
//				buf.append("%2B");
//			} else if (ch == '&') {
//				buf.append("%26");
//			} else
//				buf.append(ch);
//		}
//		return buf.toString();
//		// StringBuilder buf = new StringBuilder(s.length() + 16);
//		// for (int i = 0; i < s.length(); i++) {
//		// char ch = s.charAt(i);
//		// if ((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z')
//		//                    || (ch >= '0' && ch <= '9') || ".-*_()[]^@".indexOf(ch) > -1) { //$NON-NLS-1$
//		// buf.append(ch);
//		// } else if (ch == ' ') {
//		// buf.append('+');
//		// } else {
//		// byte[] bytes = new String(new char[] { ch }).getBytes();
//		// for (int j = 0; j < bytes.length; j++) {
//		// buf.append('%');
//		// buf.append(digits.charAt((bytes[j] & 0xf0) >> 4));
//		// buf.append(digits.charAt(bytes[j] & 0xf));
//		// }
//		// }
//		// }
//		// return buf.toString();
//	}
}
