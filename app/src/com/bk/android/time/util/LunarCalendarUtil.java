package com.bk.android.time.util;

import com.bk.android.util.FormatUtil;

/** 农历工具类
 * @author mingkg21
 * @email mingkg21@gmail.com
 * @date 2014年5月23日
 *
 */
public class LunarCalendarUtil {
	
//	public static String getLunarCalendar(Context context, long millisecond) {
//		Calendar calendar = Calendar.getInstance();
//		if(millisecond > 0) {
//			calendar.setTime(new Date(millisecond));
//		}
//		int year = calendar.get(Calendar.YEAR);
//		int month = calendar.get(Calendar.MONTH) + 1;
//		int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
//		return getLunarCalendar(context, year, month, dayOfMonth);
//	}
//
//	public static String getLunarCalendar(Context context, int year, int month, int dayOfMonth) {
//		int[] lunar = LunarCalendar.solarToLunar(year, month, dayOfMonth);
//		return context.getString(R.string.diary_lunar, calendarToChineseNuber(lunar[1]), calendarToChineseNuber(lunar[2]));
//	}
	
	private static String calendarToChineseNuber(int lunar) {
		int high = lunar / 10;
		int low = lunar % 10;
		StringBuilder sb = new StringBuilder();
		if(high > 1) {
			sb.append(getChineseNumber1TO9(high));
			sb.append("十");
		} else if(high == 1) {
			sb.append("十");
		}
		if(low > 0) {
			sb.append(getChineseNumber1TO9(low));
		}
		return sb.toString();
	}
	
	private static String getChineseNumber1TO9(int number) {
		switch (number) {
		case 1:
			return "一";
		case 2:
			return "二";
		case 3:
			return "三";
		case 4:
			return "四";
		case 5:
			return "五";
		case 6:
			return "六";
		case 7:
			return "七";
		case 8:
			return "八";
		case 9:
			return "九";

		default:
			return "";
		}
	}
//	
//	public static String getWeek(Context context, int dayOfWeek) {
//		int resId = -1;
//		switch(dayOfWeek) {
//		case 1:
//			resId = R.string.diary_week_sunday;
//			break;
//		case 2:
//			resId = R.string.diary_week_monday;
//			break;
//		case 3:
//			resId = R.string.diary_week_tuesday;
//			break;
//		case 4:
//			resId = R.string.diary_week_wednesday;
//			break;
//		case 5:
//			resId = R.string.diary_week_thursday;
//			break;
//		case 6:
//			resId = R.string.diary_week_friday;
//			break;
//		case 7:
//			resId = R.string.diary_week_saturday;
//			break;
//		}
//		return context.getString(resId);
//	}
	
	public static String getWeek(long millisecond) {
		return FormatUtil.formatSimpleDate("EEEE", millisecond);
	}

}
