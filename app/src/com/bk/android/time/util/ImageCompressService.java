package com.bk.android.time.util;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.Bitmap.Config;
import android.os.Binder;
import android.os.IBinder;

import com.bk.android.time.app.App;
import com.bk.android.util.BitmapUtil;
import com.bk.android.util.FileUtil;
import com.bk.android.util.LogUtil;

public class ImageCompressService extends Service {
	private static final String TAG = "ImageCompressService";
	private static final String ACTION_COMPRESS_IMAGE_START = "ACTION_COMPRESS_IMAGE_START_IMAGE_COMPRESS_SERVICE";
	private static final String ACTION_COMPRESS_IMAGE_FINISH = "ACTION_COMPRESS_IMAGE_FINISH_IMAGE_COMPRESS_SERVICE";

	private ServiceBinder mServiceBinder = new ServiceBinder();
	
	@Override
	public IBinder onBind(final Intent intent) {
		new Thread(){
			@Override
			public void run() {
				try {
					LogUtil.i(TAG, "onBind");
					if(ACTION_COMPRESS_IMAGE_START.equals(intent.getAction())){
						doCompressImage(intent);
					}
				} catch (Exception e) {
					e.printStackTrace();
				} catch (Error e) {
					e.printStackTrace();
				}
			}
		}.start();
		return mServiceBinder;
	}
	
	private void doCompressImage(Intent intent){
		BitmapUtil.setInPreferredConfig(Config.ARGB_8888);
		String source = intent.getStringExtra("source");
		String target = intent.getStringExtra("target");
		int targetW = intent.getIntExtra("targetW", 0);
		int targetH = intent.getIntExtra("targetH", 0);
		int quality = intent.getIntExtra("quality", 0);
		BitmapUtil.syncCompressionImg(source, target, targetW, targetH, quality);
		sendBroadcast(new Intent(ACTION_COMPRESS_IMAGE_FINISH));
	}
	
	public static Context getContext(){
		return App.getInstance();
	}
	
	public synchronized static boolean syncCompressionImg(String source, String target, int targetW, int targetH, int quality){
		LogUtil.i(TAG, "syncCompressionImg s");
		FileUtil.delFiles(target);
		ImageServiceConnection connection = new ImageServiceConnection();
		ImageServiceReceiver receiver = new ImageServiceReceiver(connection);
		Intent intent = new Intent(getContext(), ImageCompressService.class);
		intent.putExtra("source", source);
		intent.putExtra("target", target);
		intent.putExtra("targetW", targetW);
		intent.putExtra("targetH", targetH);
		intent.putExtra("quality", quality);
		intent.setAction(ACTION_COMPRESS_IMAGE_START);
		getContext().registerReceiver(receiver, new IntentFilter(ACTION_COMPRESS_IMAGE_FINISH));
		getContext().bindService(intent, connection, Context.BIND_AUTO_CREATE);
		
		for (int i = 0; i < 20; i++) {
			if(!connection.isConnected() || receiver.isUnbindService()){
				break;
			}
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {}
		}
		
		getContext().unregisterReceiver(receiver);
		if(!receiver.isUnbindService()){
			getContext().unbindService(connection);
		}
		LogUtil.i(TAG, "syncCompressionImg e");
		return FileUtil.isFileExists(target);
	}
	
	private static class ImageServiceReceiver extends BroadcastReceiver{
		private boolean isUnbind;
		private ImageServiceConnection mConnection;
		private Thread mThread;

		private ImageServiceReceiver(ImageServiceConnection connection){
			mConnection = connection;
			mThread = Thread.currentThread();
		}
		
		public void release() {
			mThread = null;
			mConnection = null;
		}
		
		public boolean isUnbindService(){
			return isUnbind;
		}

		@Override
		public void onReceive(Context context, Intent intent) {
			if(intent != null && ACTION_COMPRESS_IMAGE_FINISH.equals(intent.getAction())){
				LogUtil.i(TAG, "onReceive");
				if(mConnection != null){
					getContext().unbindService(mConnection);
				}
				if(mThread != null){
					mThread.interrupt();
				}
				isUnbind = true;
				release();
			}
		}
	}
	
	private static class ImageServiceConnection implements ServiceConnection{
		private IBinder mService;
		private Thread mThread;
		
		private ImageServiceConnection(){
			mThread = Thread.currentThread();
		}
		
		public void release() {
			mThread = null;
			mService = null;
		}

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			mService = service;
			LogUtil.i(TAG, "onServiceConnected");
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			if(mThread != null){
				mThread.interrupt();
			}
			release();
			LogUtil.i(TAG, "onServiceDisconnected");
		}
		
		public boolean isConnected(){
			return mService == null || mService.isBinderAlive();
		}
	}
	
	public static class ServiceBinder extends Binder{} 
}
