package com.bk.android.time.util;

/**
 * Created by mingkg21 on 16/6/6.
 */
public class PriceUtil {

    public static String formatPrice(int score) {
        int yuan = score / 100;
        int fen = score % 100;

        StringBuilder sb = new StringBuilder();
        sb.append(yuan);
        sb.append(".");
        if (fen > 0) {
            if (fen < 10) {
                sb.append("0");
            }
            sb.append(fen);
        } else {
            sb.append("00");
        }
        return sb.toString();
    }

    public static String getPrice(int score) {
        StringBuilder sb = new StringBuilder();
        sb.append(formatPrice(score));
        sb.append("元");
        return sb.toString();
    }

}
