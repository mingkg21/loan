package com.bk.android.time.util;

import com.bk.android.time.data.Preferences;

/**
 * Created by mingkg21 on 15/12/1.
 */
public class NewUserGuideRecord {

    private static NewUserGuideRecord instance;

    private NewUserGuideRecord() {

    }

    public static NewUserGuideRecord getIntance() {
        if (instance == null) {
            instance = new NewUserGuideRecord();
        }
        return instance;
    }

    public void setNewUserGuideStatus() {
        if (Preferences.getInstance().getNewUserGuideRecordStatus() == 0) {
            Preferences.getInstance().setNewUserGuideRecordStatus(1);
        }
    }

    public void setClickNewRecord() {
        if (Preferences.getInstance().getNewUserGuideRecordStatus() != 0) {
            Preferences.getInstance().setNewUserGuideRecordStatus(3);
        }
    }

    public void setGuideStatus() {
        if (Preferences.getInstance().getNewUserGuideRecordStatus() == 1) {
            Preferences.getInstance().setNewUserGuideRecordStatus(2);
        } else if (Preferences.getInstance().getNewUserGuideRecordStatus() == 2) {
            Preferences.getInstance().setNewUserGuideRecordStatus(3);
        }
    }

    public boolean isShowRecordTip() {
        if (Preferences.getInstance().getNewUserGuideRecordStatus() == 2) {
            return true;
        }
        return false;
    }

}
