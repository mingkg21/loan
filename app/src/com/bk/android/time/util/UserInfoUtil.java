package com.bk.android.time.util;

/**
 * Created by mingkg21 on 2017/5/2.
 */

public class UserInfoUtil {

    public static String marryFlag(String flag) {
        if ("1".equals(flag)) {
            return "已婚";
        } else if ("0".equals(flag)) {
            return "未婚";
        } else {
            return "";
        }
    }

    public static String marryFlagStr(String str) {
        if ("已婚".equals(str)) {
            return "1";
        } else if ("未婚".equals(str)) {
            return "0";
        } else {
            return "";
        }
    }

    public static String educationLevel(String level) {
        if ("high_school".equals(level)) {
            return "高中";
        } else if ("junior_college".equals(level)) {
            return "大专";
        } else if ("university".equals(level)) {
            return "本科";
        } else if ("master".equals(level)) {
            return "硕士";
        } else if ("phd".equals(level)) {
            return "博士";
        } else {
            return "";
        }
    }

    public static String educationLevelStr(String str) {
        if ("高中".equals(str)) {
            return "high_school";
        } else if ("大专".equals(str)) {
            return "junior_college";
        } else if ("本科".equals(str)) {
            return "university";
        } else if ("硕士".equals(str)) {
            return "master";
        } else if ("博士".equals(str)) {
            return "phd";
        } else {
            return "";
        }
    }

    public static String profession(String profession) {
        if ("officer".equals(profession)) {
            return "上班族";
        } else if ("bussiness".equals(profession)) {
            return "个体户";
        } else if ("company".equals(profession)) {
            return "企业主";
        } else if ("freeworker".equals(profession)) {
            return "自由职业";
        } else {
            return "";
        }
    }

    public static String professionStr(String str) {
        if ("上班族".equals(str)) {
            return "officer";
        } else if ("个体户".equals(str)) {
            return "bussiness";
        } else if ("企业主".equals(str)) {
            return "company";
        } else if ("自由职业".equals(str)) {
            return "freeworker";
        } else {
            return "";
        }
    }

    public static String incomType(String incomeType) {
        if ("bank".equals(incomeType)) {
            return "银行代发";
        } else if ("transfers".equals(incomeType)) {
            return "转账工资";
        } else if ("cash".equals(incomeType)) {
            return "现金发放";
        } else {
            return "";
        }
    }

    public static String incomTypeStr(String str) {
        if ("银行代发".equals(str)) {
            return "bank";
        } else if ("转账工资".equals(str)) {
            return "transfers";
        } else if ("现金发放".equals(str)) {
            return "cash";
        } else {
            return "";
        }
    }

    public static String companyType(String companyType) {
        if ("normal".equals(companyType)) {
            return "普通企业";
        } else if ("enterprise".equals(companyType)) {
            return "事业单位";
        } else if ("big_company".equals(companyType)) {
            return "大型垄断国企";
        } else if ("500_company".equals(companyType)) {
            return "500强企业";
        } else if ("market".equals(companyType)) {
            return "上市企业";
        } else if ("other".equals(companyType)) {
            return "其他";
        } else {
            return "";
        }
    }

    public static String companyTypeStr(String str) {
        if ("普通企业".equals(str)) {
            return "normal";
        } else if ("事业单位".equals(str)) {
            return "enterprise";
        } else if ("大型垄断国企".equals(str)) {
            return "big_company";
        } else if ("500强企业".equals(str)) {
            return "500_company";
        } else if ("上市企业".equals(str)) {
            return "market";
        } else if ("其他".equals(str)) {
            return "other";
        } else {
            return "";
        }
    }

    public static String companyWorkTime(String companyWorkTime) {
        if ("less_than_3".equals(companyWorkTime)) {
            return "不足3个月";
        } else if ("3to5".equals(companyWorkTime)) {
            return "3到5个月";
        } else if ("6to12".equals(companyWorkTime)) {
            return "6到12个月";
        } else if ("1to3year".equals(companyWorkTime)) {
            return "1到3年";
        } else if ("4to7year".equals(companyWorkTime)) {
            return "4到7年";
        } else if ("more7year".equals(companyWorkTime)) {
            return "7年以上";
        } else {
            return "";
        }
    }

    public static String companyWorkTimeStr(String str) {
        if ("不足3个月".equals(str)) {
            return "less_than_3";
        } else if ("3到5个月".equals(str)) {
            return "3to5";
        } else if ("6到12个月".equals(str)) {
            return "6to12";
        } else if ("1到3年".equals(str)) {
            return "1to3year";
        } else if ("4到7年".equals(str)) {
            return "4to7year";
        } else if ("7年以上".equals(str)) {
            return "more7year";
        } else {
            return "";
        }
    }

    public static String containHouseType(String containHouseType) {
        if ("none".equals(containHouseType)) {
            return "无房产";
        } else if ("house".equals(containHouseType)) {
            return "商品住宅";
        } else if ("shop".equals(containHouseType)) {
            return "商铺";
        } else if ("office".equals(containHouseType)) {
            return "办公楼";
        } else if ("factory".equals(containHouseType)) {
            return "厂房";
        } else if ("self_house".equals(containHouseType)) {
            return "自建房";
        } else {
            return "";
        }
    }

    public static String containHouseTypeStr(String str) {
        if ("无房产".equals(str)) {
            return "none";
        } else if ("商品住宅".equals(str)) {
            return "house";
        } else if ("商铺".equals(str)) {
            return "shop";
        } else if ("办公楼".equals(str)) {
            return "office";
        } else if ("厂房".equals(str)) {
            return "factory";
        } else if ("自建房".equals(str)) {
            return "self_house";
        } else {
            return "";
        }
    }

    public static String containCarType(String containCarType) {
        if ("none".equals(containCarType)) {
            return "无车产";
        } else if ("has".equals(containCarType)) {
            return "名下有车";
        } else if ("has_not_mine".equals(containCarType)) {
            return "有车但车已被抵押";
        } else {
            return "";
        }
    }

    public static String containCarTypeStr(String str) {
        if ("无车产".equals(str)) {
            return "none";
        } else if ("名下有车".equals(str)) {
            return "has";
        } else if ("有车但车已被抵押".equals(str)) {
            return "has_not_mine";
        } else {
            return "";
        }
    }

    public static String creditStanding(String creditStanding) {
        if ("none".equals(creditStanding)) {
            return "无信用记录";
        } else if ("good".equals(creditStanding)) {
            return "信用记录良好";
        } else if ("little".equals(creditStanding)) {
            return "少量逾期";
        } else if ("bad".equals(creditStanding)) {
            return "征信较差";
        } else {
            return "";
        }
    }

    public static String creditStandingStr(String str) {
        if ("无信用记录".equals(str)) {
            return "none";
        } else if ("信用记录良好".equals(str)) {
            return "good";
        } else if ("少量逾期".equals(str)) {
            return "little";
        } else if ("征信较差".equals(str)) {
            return "bad";
        } else {
            return "";
        }
    }

    public static String yesOrNo(String yesOrNo) {
        if ("1".equals(yesOrNo)) {
            return "是";
        } else if ("0".equals(yesOrNo)) {
            return "否";
        } else {
            return "";
        }
    }

    public static String yesOrNoStr(String str) {
        if ("是".equals(str)) {
            return "1";
        } else if ("否".equals(str)) {
            return "0";
        } else {
            return "";
        }
    }

}
