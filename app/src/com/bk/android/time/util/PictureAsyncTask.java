package com.bk.android.time.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by  2016/12/12 0012.
 */

public class PictureAsyncTask extends AsyncTask<String, Integer, Boolean> {

    private OnPictureAction mAction;
    private String          mSavePath;

    public PictureAsyncTask setOnPictureAction(OnPictureAction action) {
	   this.mAction = action;
	   return this;
    }

    public PictureAsyncTask setSavePath(String savePath) {
	   this.mSavePath = savePath;
	   return this;
    }

    @Override
    protected Boolean doInBackground(String... params) {
	   try {
		  File         saveFile     = new File(mSavePath);
		  if(saveFile.length() > 0) return true;
		  URL               httpUrl = new URL(params[0].replaceAll(" ", "%20"));
		  HttpURLConnection http    = (HttpURLConnection) httpUrl.openConnection();
		  HttpURLConnection.setFollowRedirects(true);
		  http.setConnectTimeout(5 * 1000);
		  http.setReadTimeout(12 * 1000);
		  http.setRequestMethod("GET");
		  http.setRequestProperty("Accept", "image/gif, image/jpeg, image/pjpeg, image/pjpeg, application/x-shockwave-flash, application/xaml+xml, application/vnd.ms-xpsdocument, application/x-ms-xbap, application/x-ms-application, application/vnd.ms-excel, application/vnd.ms-powerpoint, application/msword, */*");     //设置客户端可以接受的返回数据类型
		  http.setRequestProperty("Accept-Language", "zh-CN");     //设置客户端使用的语言问中文
		  http.setRequestProperty("Referer", params[0]);     //设置请求的来源，便于对访问来源进行统计
		  http.setRequestProperty("Charset", "UTF-8");     //设置通信编码为UTF-8
		  http.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.2; Trident/4.0; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729)");     //客户端用户代理
		  http.setRequestProperty("Connection", "Keep-Alive");     //使用长连接
		  InputStream  inStream = http.getInputStream();
		  OutputStream outputStream = new FileOutputStream(saveFile);
		  byte[] buffer      = new byte[1024 * 1024];     //设置本地数据缓存的大小为16k
		  int    offset      = 0;
		  while ((offset = inStream.read(buffer, 0, buffer.length)) != -1) {
			 outputStream.write(buffer, 0, offset);
		  }
		  outputStream.close();
		  inStream.close();
	   } catch (MalformedURLException e) {
		  e.printStackTrace();
		  return false;
	   } catch (IOException e) {
		  e.printStackTrace();
		  return false;
	   }
	   return true;
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
	   super.onPostExecute(aBoolean);
	   if(!aBoolean){
		  if(mSavePath.length() > 0){
			 aBoolean = true;
		  }
	   }
	   if (mAction != null) {
		  mAction.onLoadBitmap(aBoolean ? BitmapFactory.decodeFile(mSavePath) : null);
	   }
    }

    public interface OnPictureAction {

	   void onLoadBitmap(Bitmap bitmap);

    }

}
