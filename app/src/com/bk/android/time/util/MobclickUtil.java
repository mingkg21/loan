package com.bk.android.time.util;

import android.app.Activity;
import android.content.Context;

import com.bk.android.time.app.App;
import com.umeng.analytics.AnalyticsConfig;
import com.umeng.analytics.MobclickAgent;

/** 统计模块
 *
 */
public class MobclickUtil {
	private static Context getContext() {
		return App.getInstance();
	}
	
	public static void init(Context context){
		String channel = App.getInstance().getUMengChannel();
		AnalyticsConfig.setChannel(channel);
	}
	
	/** 判断是否需要记录，如果是自然发布的则需要记录
	 * @return
	 */
	private static boolean isRecord() {
		if(!"2".equals(App.getInstance().getBrushType())) {
			return false;
		}
		return true;
	}
	
	public static void onPause(Activity context) {
		MobclickAgent.onPause(context);
	}
	
	public static void onResume(Activity context) {
		MobclickAgent.onResume(context);
	}

}