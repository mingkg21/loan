package com.bk.android.time.util;

import java.util.HashMap;

import android.content.Context;
import android.content.res.Resources;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;

import com.bk.android.time.app.App;

public class FaceUtil {
	private static String[] sFaceKeys_1 = "wx,pz,se,fd,dy,ll,hx,bz,shui,dk,gg,fn,tp,cy,jy,ng,kuk,lengh,zk,tuu,tx".split(",");
	private static String[] sFaceKeys_2 = "ka,baiy,am,jie,kun,jk,lh,hanx,db,fendou,zhm,yiw,xu,yun,zhem,shuai,kl,qiao,zj,ch,kb".split(",");
	private static String[] sFaceKeys_3 = "gz,qd,huaix,zhh,yhh,hq,bs,wq,kk,yx,qq,xia,kel,cd,xig,pj,lq,pp,kf,fan,zt".split(",");
	private static String[] sFaceKeys_4 = "mg,dx,sa,xin,xs,dg,shd,zhd,dao,zq,pch,bb,yl,ty,lw,yb,qiang,ruo,ws,shl,bq".split(",");

	private static String[] sFaceKeys_other = "height,weight".split(",");

	private static String[] sFaceKeys_cn_1 = "微笑,撇嘴,色,发呆,得意,流泪,害羞,闭嘴,睡,大哭,尴尬,愤怒,调皮,呲牙,惊讶,难过,酷,冷汗,抓狂,吐,偷笑".split(",");
	private static String[] sFaceKeys_cn_2 = "可爱,白眼,傲慢,饥饿,困,惊恐,流汗,憨笑,大兵,奋斗,咒骂,疑问,嘘,晕,折磨,衰,骷髅,敲打,再见,擦汗,抠鼻".split(",");
	private static String[] sFaceKeys_cn_3 = "鼓掌,糗大了,坏笑,左哼哼,右哼哼,哈欠,鄙视,委屈,快哭了,阴险,亲亲,吓,可怜,菜刀,西瓜,啤酒,篮球,乒乓,咖啡,饭,猪头".split(",");
	private static String[] sFaceKeys_cn_4 = "玫瑰,凋谢,示爱,爱心,心碎,蛋糕,闪电,炸弹,刀,足球,瓢虫,便便,月亮,太阳,礼物,拥抱,强,弱,握手,胜利,抱拳".split(",");
	
	private static String[] sFaceKeys_cn_other = "身高,体重".split(",");

	private static String[][] sFaceKeys = new String[][]{sFaceKeys_1,sFaceKeys_2,sFaceKeys_3,sFaceKeys_4};
	private static String[][] sFaceKeyCNs = new String[][]{sFaceKeys_cn_1,sFaceKeys_cn_2,sFaceKeys_cn_3,sFaceKeys_cn_4};

	private static HashMap<String,String> sFaceKeyMap = new HashMap<String, String>();
	private static HashMap<String,String> sFaceKeyCNMap = new HashMap<String, String>();
	static{
		for (int i = 0;i < sFaceKeys.length;i++) {
			String[] keyGroup = sFaceKeys[i];
			String[] keyCNGroup = sFaceKeyCNs[i];
			for (int j = 0;j < keyGroup.length;j++) {
				sFaceKeyMap.put(keyGroup[j], keyCNGroup[j]);
				sFaceKeyCNMap.put(keyCNGroup[j], keyGroup[j]);
			}
		}
		String[] keyGroup = sFaceKeys_other;
		String[] keyCNGroup = sFaceKeys_cn_other;
		for (int j = 0;j < keyCNGroup.length;j++) {
			sFaceKeyMap.put(keyGroup[j], keyCNGroup[j]);
			sFaceKeyCNMap.put(keyCNGroup[j], keyGroup[j]);
		}
	}
	
	public static String[][] getFaceKeys(){
		return sFaceKeyCNs;
	}
	
	public static int getFaceRes(String key){
		if(sFaceKeyCNMap.containsKey(key)){
			key = sFaceKeyCNMap.get(key);
		}
		String name = "face_"+key;
		return getResources().getIdentifier(name, "drawable", getContext().getPackageName());
	}

	public static String getFacePlaceholder(String key,boolean isSimplePlaceholder){
		if(isSimplePlaceholder){
			return "\uFFFC";
		}
		if(sFaceKeyMap.containsKey(key)){
			key = sFaceKeyMap.get(key);
		}
		return "[/"+key+"]";
	}
	
	public static Editable loadFace(String content){
		SpannableStringBuilder spans = new SpannableStringBuilder();
		loadFace(spans,content);
		return spans;
	}
	
	public static void loadFace(Editable spans,String content){
		loadFace(spans, content, false);
	}
	
	public static void loadFace(Editable spans,String content,boolean isSimplePlaceholder){
		if(content == null || spans == null){
			return;
		}
		StringBuffer sb = new StringBuffer();
		int length = content.length();
		int faceStar = -1;
		for (int j = 0; j < length; j++) {
			char c = content.charAt(j);
			if(c == '[' && j + 1 < length - 1 && content.charAt(j + 1) == '/'){
				faceStar = j;
			}else if(c == ']' && faceStar >= 0){
				int res = 0;
				if(sb.length() > 2){
					String key = sb.substring(2,sb.length());
					res = FaceUtil.getFaceRes(key);
					if(res > 0){
						int start = spans.length();
						spans.append(getFacePlaceholder(key,isSimplePlaceholder));
						addFaceSpan(spans, res, start,spans.length());
					}
				}
				if(res <= 0){
					sb.append(c);
					spans.append(sb.toString());
				}
				faceStar = -1;
				sb.setLength(0);
				continue;
			}
			if(faceStar < 0){
				spans.append(c);
			}else{
				sb.append(c);
			}
		}
	}
	
	public static void addFace(String key,Editable editable,int start){
		String addStr = getFacePlaceholder(key,false);
		editable.insert(start , addStr);
		int res = getFaceRes(key);
		if(res > 0){
			addFaceSpan(editable, res, start, start + addStr.length());
		}
	}
	
	private static void addFaceSpan(Editable editable,int res,int start,int end){
		ImageSpan span = new ImageSpan(getContext(),res);
		editable.setSpan(span, start,end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
	}
	
	private static Resources getResources() {
		return App.getInstance().getResources();
	}
	
	private static Context getContext() {
		return App.getInstance();
	}
}
