package com.bk.android.time.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.FaceDetector;

import com.bk.android.util.LogUtil;

/**
 * Created by mingkg21 on 15/10/10.
 */
public class FaceDetectUtil {

    private static final int FACES_MAX = 1;

    public static boolean detectFace(String path) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        options.inSampleSize = 4;
        Bitmap bitmap = BitmapFactory.decodeFile(path, options);
        long time = System.currentTimeMillis();
        boolean value = hasFace(bitmap);
        LogUtil.v("PhotoLoadAsyncTask", "time: " + (System.currentTimeMillis() - time));
        bitmap.recycle();
        bitmap = null;
        return  value;
    }

    public static boolean hasFace(Bitmap bitmap) {
        if (bitmap == null) {
            return false;
        }
        long time = System.currentTimeMillis();
        FaceDetector.Face[] faces = new FaceDetector.Face[FACES_MAX];
        FaceDetector faceDetector = new FaceDetector(bitmap.getWidth(), bitmap.getHeight(), FACES_MAX);
        boolean value = faceDetector.findFaces(bitmap, faces) != 0;
//        LogUtil.v("PhotoLoadAsyncTask", "time: " + (System.currentTimeMillis() - time));
        return value;
    }

}
