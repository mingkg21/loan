package com.bk.android.time.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.RandomAccessFile;
import java.math.BigInteger;
import java.security.MessageDigest;

/**
 * 采用MD5加密解密
 *
 * @author tfq
 * @datetime 2011-10-13
 */
public class MD5Util {

    public static String getPartFileMd5(File f, long startPos, int length) {
	   MessageDigest md5 = null;
	   try {
		  md5 = MessageDigest.getInstance("MD5");
	   } catch (Exception e) {
		  e.printStackTrace();
		  return null;
	   }

	   RandomAccessFile raf = null;
	   try {
		  raf = new RandomAccessFile(f, "r");
		  byte[] buffer   = new byte[1024 * 64];
		  int    count    = 0;
		  int    sizeOnce = 1024 * 64;
		  raf.seek(startPos);
		  if (length < 1024 * 64) sizeOnce = length;
		  while ((count = raf.read(buffer, 0, sizeOnce)) != -1) {
			 md5.update(buffer, 0, count);
			 length -= count;
			 if (length <= 0) break;
			 if (length < sizeOnce) {
				sizeOnce = length;
			 }
		  }
		  byte[]       md5Bytes = md5.digest();
		  StringBuffer hexValue = new StringBuffer();
		  for (int i = 0; i < md5Bytes.length; i++) {
			 int val = ((int) md5Bytes[i]) & 0xff;
			 if (val < 16)
				hexValue.append("0");
			 hexValue.append(Integer.toHexString(val));
		  }
		  return hexValue.toString();
	   } catch (Exception e) {
		  return null;
	   } finally {
		  try {
			 if (raf != null) raf.close();
		  } catch (Exception e) {}
	   }
    }

    /***
	* MD5加码 生成32位md5码
	*/
    public static String string2MD5(String inStr) {
	   byte[] byteArray = inStr.getBytes();
	   return hexDigest(byteArray);
    }

    public static String hexDigest(byte[] bytes){
	   MessageDigest md5;
	   try {
		  md5 = MessageDigest.getInstance("MD5");
	   } catch (Exception e) {
		  System.out.println(e.toString());
		  e.printStackTrace();
		  return "";
	   }
	   byte[]       md5Bytes = md5.digest(bytes);
	   StringBuffer hexValue = new StringBuffer();
	   for (int i = 0; i < md5Bytes.length; i++) {
		  int val = ((int) md5Bytes[i]) & 0xff;
		  if (val < 16)
			 hexValue.append("0");
		  hexValue.append(Integer.toHexString(val));
	   }
	   return hexValue.toString();
    }

    /**
	* 加密解密算法 执行一次加密，两次解密
	*/
    public static String convertMD5(String inStr) {

	   char[] a = inStr.toCharArray();
	   for (int i = 0; i < a.length; i++) {
		  a[i] = (char) (a[i] ^ 't');
	   }
	   String s = new String(a);
	   return s;

    }

    // 测试主函数
    public static void main(String args[]) {
	   String s = new String("tangfuqiang");
	   System.out.println("原始：" + s);
	   System.out.println("MD5后：" + string2MD5(s));
	   System.out.println("加密的：" + convertMD5(s));
	   System.out.println("解密的：" + convertMD5(convertMD5(s)));

    }

    //获取文件MD5值
    public static String getFileMD5(File file) {
	   if (!file.isFile()) {
		  return null;
	   }
	   MessageDigest digest   = null;
	   FileInputStream in       = null;
	   byte            buffer[] = new byte[1024];
	   int             len;
	   try {
		  digest = MessageDigest.getInstance("MD5");
		  in = new FileInputStream(file);
		  while ((len = in.read(buffer, 0, 1024)) != -1) {
			 digest.update(buffer, 0, len);
		  }
		  in.close();
	   } catch (Exception e) {
		  e.printStackTrace();
		  return null;
	   }
	   BigInteger bigInt  = new BigInteger(1, digest.digest());
	   StringBuilder builder = new StringBuilder(bigInt.toString(16));
	   while (builder.length() < 32) {
		  builder.insert(0, '0');
	   }
	   return builder.toString();
    }

}
