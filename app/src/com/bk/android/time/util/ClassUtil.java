package com.bk.android.time.util;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import android.text.TextUtils;

public class ClassUtil {
	public static boolean hasMethodByName(Object obj, String methodName){
		boolean hasMethod = false;
		if(!TextUtils.isEmpty(methodName)){
			try {
				Method[] methods = obj.getClass().getMethods();
				if(methods != null){
					for (Method method : methods) {
						if(methodName.equals(method.getName())){
							hasMethod = true;
							break;
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return hasMethod;
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T getFieldByName(Object obj, String fieldName){
		try {
			Field field = obj.getClass().getDeclaredField(fieldName);
			if(field != null){
				field.setAccessible(true);
				return (T)field.get(obj);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
