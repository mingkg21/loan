package com.bk.android.time.util;

import com.bk.android.util.FileUtil;
import com.bk.android.util.LockUtil;

import java.io.File;
import java.util.LinkedList;

/**
 * 文件操作工具
 * @author lyw
 * @date 2014-1-10
 */
public final class AppFileUtil {

	/** 软件根目录*/
	private static final String APP_PATH_NAME = "Loan/";
	/** 游戏目录*/
	private static final String GAME_PATH = "GamePath/";
	/** 图片缓存目录*/
	private static final String IMG_CACHE_PATH = "ImgCache/";
	/** 多媒体缓存目录*/
	private static final String MEDIA_CACHE_PATH = "MediaCache/";
	/** 记录缓存目录*/
	private static final String RECORD_CACHE_PATH = "RecordCache/";
	/** 录音缓存目录*/
	private static final String RECORD_AUDIO_CACHE_PATH = "RecordAudioCache/";
	/** 记录图片缓存目录*/
	private static final String RECORD_IMG_CACHE_PATH = "RecordImgCache/";
	/** 录音缓存文件名*/
	private static final String RECORD_CACHE_FILE = "RecordCacheFile";
	/** 录音缓存文件名（中间临时文件，但确认录音有效之后才保存）*/
	private static final String RECORD_CACHE_FILE_TEMP = "RecordCacheFileTemp";
	/** 欢迎图片目录*/
	private static final String WELCOME_IMG_PATH = "WELCOME_IMG_PATH/";
	/** 欢迎音频目录*/
	private static final String WELCOME_AUDIO_PATH = "WELCOME_AUDIO_PATH/";
	/** 下载APP目录*/
	private static final String DOWNLOAD_APP_PATH = "DOWNLOAD_APP_PATH/";
	/** 欢迎图片文件*/
	private static final String WELCOME_IMG_FILE = "WELCOME_IMG_FILE";
	/** 欢迎音频文件*/
	private static final String WELCOME_AUDIO_FILE = "WELCOME_AUDIO_FILE";
	/** 杂志模版框架文件夹*/
	private static final String MAGAZINE_FRAME = "MagzineFrame/";
	/** 字体文件夹*/
	private static final String FONT_FILE = "FontFile/";
	/** 杂志模版框架文件夹*/
	private static final String SUBMIT_LOG = "SubmitLog/";
	
	private static String APP_PATH;
	
	public static String getAppPath(){
		if(APP_PATH == null){
			APP_PATH = initAppPath();
		}
		return APP_PATH;
	}
	
	public static String getFontFilePath(){
		String path = getAppPath() + FONT_FILE;
		FileUtil.checkPath(path);
		return path;
	}
	
	public static String getMediaCachePath(){
		String path = getAppPath() + MEDIA_CACHE_PATH;
		FileUtil.checkPath(path);
		return path;
	}
	
	public static String getImgCachePath(){
		String path = getAppPath() + IMG_CACHE_PATH;
		FileUtil.checkPath(path);
		return path;
	}
	
	public static String getRecordCachePath(){
		String path = getAppPath() + RECORD_CACHE_PATH;
		FileUtil.checkPath(path);
		return path;
	}
	
	public static String getRecordAudioCachePath(){
		String path = getRecordCachePath() + RECORD_AUDIO_CACHE_PATH;
		FileUtil.checkPath(path);
		return path;
	}
	
	public static String getRecordImgCachePath(){
		String path = getRecordCachePath() + RECORD_IMG_CACHE_PATH;
		FileUtil.checkPath(path);
		return path;
	}
	
	public static String getWelcomeImgPath(){
		String path = getAppPath() + WELCOME_IMG_PATH;
		FileUtil.checkPath(path);
		return path;
	}
	
	public static String getWelcomeAudioPath(){
		String path = getAppPath() + WELCOME_AUDIO_PATH;
		FileUtil.checkPath(path);
		return path;
	}
	
	public static String getGamePath(){
		String path = getAppPath() + GAME_PATH;
		FileUtil.checkPath(path);
		return path;
	}
	
	public static String getDownloadAppPath(){
		String path = getAppPath() + DOWNLOAD_APP_PATH;
		FileUtil.checkPath(path);
		return path;
	}
	
	public static String getMagazineFramePath(){
		String path = getAppPath() + MAGAZINE_FRAME;
		FileUtil.checkPath(path);
		return path;
	}
	
	public static String getSubmitLogPath(){
		String path = getAppPath() + SUBMIT_LOG;
		FileUtil.checkPath(path);
		return path;
	}
	
	public static String getWelcomeImgFile(){
		return getWelcomeImgPath() + WELCOME_IMG_FILE;
	}
	
	public static String getWelcomeAudioFile(){
		return getWelcomeAudioPath() + WELCOME_AUDIO_FILE;
	}
	
	public static String getRecordImgCacheFile(){
		return getRecordImgCachePath() + System.currentTimeMillis() + ".jpg";
	}

	public static String getAvatarCacheFile() {
		return getImgCachePath() + "avatar.jpg";
	}
	
	public static String getRecordAudioCacheFile(){
		return getRecordAudioCachePath() + RECORD_CACHE_FILE;
	}
	
	public static String getRecordAudioCacheTempFile(){
		return getRecordAudioCachePath() + RECORD_CACHE_FILE_TEMP;
	}
	
	public static String initAppPath(){
		APP_PATH = FileUtil.getDcimPath() + File.separator + APP_PATH_NAME;
		FileUtil.checkPath(APP_PATH);
		return APP_PATH;
	}
	
	
	private static boolean sIscheckRecordImgCacheSize = false;
	
	public synchronized static void checkRecordImgCacheSize(){
		if(sIscheckRecordImgCacheSize){
			return;
		}
		sIscheckRecordImgCacheSize = true;
		checkSdcardImageSize(getRecordImgCachePath(), 1024L * 1024 * 50);
	}
	
	private static void checkSdcardImageSize(final String path,final long maxSize){
		Thread thread = new Thread(){
			@Override
			public void run() {
				File fileDir = new File(path);
				long totalSpace = 0;
				File[] fileArr = fileDir.listFiles();
				LinkedList<File> fileList = new LinkedList<File>();
				if(fileArr != null && fileArr.length > 0){
					for (File arrFile : fileArr) {
						totalSpace += arrFile.length();
						int i = 0;
						for (File listFile : fileList) {
							if(listFile.lastModified() >= arrFile.lastModified()){
								break;
							}
							i++;
						}
						fileList.add(i,arrFile);
					}
					for (;fileList.size() > 0 && totalSpace > maxSize;) {
						File file = fileList.removeFirst();
						synchronized (LockUtil.getFileLock(file.getAbsolutePath())) {
							long length = file.length();
							FileUtil.delFiles(file.getAbsolutePath());
							totalSpace -= length;
						}
					}
				}
			}
		};
		thread.setPriority(Thread.MIN_PRIORITY);
		thread.start();
	}
}
