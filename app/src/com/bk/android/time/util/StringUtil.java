package com.bk.android.time.util;

public class StringUtil {
	public static int findEndBracket(CharSequence source,int start,char preBracket,char postBracket){
		int end = -1;
		Integer bracketSize = null;
		for (int i = start; i < source.length(); i++) {
			char c = source.charAt(i);
			if(c == preBracket){
				bracketSize = bracketSize != null ? bracketSize + 1 : 1;
			}else if(c == postBracket){
				bracketSize = bracketSize != null ? bracketSize - 1 : 0;
			}
			if(bracketSize != null && bracketSize == 0){
				end = i;
				break;
			}
		}
		return end;
	}
	
	public static String parseJsonString(String source, String key, char preBracket,char postBracket){
		return parseJsonString(new StringBuffer(source), key, preBracket, postBracket);
	}
	
	public static String parseJsonString(StringBuffer source, String key, char preBracket,char postBracket){
		if(source.length() > 0){
			key = "\"" + key + "\":";
			int start = source.lastIndexOf(key);
			if(start != -1){
				int end = findEndBracket(source, start, preBracket, postBracket);
				if(start < end){
					try {
						return source.substring(start + key.length(), end + 1);
					} catch (Exception e) {}
				}
			}
		}
		return null;
	}
}
