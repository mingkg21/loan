package com.bk.android.time.util;

import android.content.Context;
import android.view.Gravity;
import android.view.View;

import com.bk.android.app.BaseApp;
import com.bk.android.assistant.R;
import com.bk.android.time.model.BaseDialogViewModel;
import com.bk.android.time.model.BaseDialogViewModel.OnBtnClickCallBack;
import com.bk.android.time.model.BaseViewModel;
import com.bk.android.time.model.common.CommonDialogViewModel;
import com.bk.android.time.model.common.WaitingDialogViewModel;
import com.bk.android.time.ui.common.CommonDialog;
import com.bk.android.util.AppUtil;
/**
 * 构造CommonDialogViewModel工具类
 * @author linyiwei
 */
public class DialogUtil {
	/**
	 * 构建确认窗口 提示内容为字符串 取消为关闭窗口
	 * @param context
	 * @param content
	 * @param leftBtnClick
	 * @return
	 */
	public static CommonDialogViewModel createConfirmDialog(Context context,int content
			,OnBtnClickCallBack leftBtnClick){
		return createConfirmDialog(context, getString(ResUtil.STR_APP_NAME), getString(content)
				, getString(ResUtil.STR_BTN_TEXT_CONFIRM), getString(ResUtil.STR_BTN_TEXT_CANCEL),leftBtnClick);
	}
	/**
	 * 构建确认窗口 提示内容为字符串 取消为关闭窗口
	 * @param context
	 * @param title
	 * @param content
	 * @param leftBtnClick
	 * @return
	 */
	public static CommonDialogViewModel createConfirmDialog(Context context,int title,int content
			,OnBtnClickCallBack leftBtnClick){
		return createConfirmDialog(context, getString(title), getString(content)
				, getString(ResUtil.STR_BTN_TEXT_CONFIRM), getString(ResUtil.STR_BTN_TEXT_CANCEL),leftBtnClick);
	}
	/**
	 * 构建确认窗口 提示内容为字符串 取消为关闭窗口
	 * @param context
	 * @param title
	 * @param content
	 * @param letfButStr
	 * @param rightButStr
	 * @param leftBtnClick
	 * @return
	 */
	public static CommonDialogViewModel createConfirmDialog(Context context,int title,int content
			,int letfButStr,int rightButStr,OnBtnClickCallBack leftBtnClick){
		return createConfirmDialog(context, getString(title), getString(content)
				, getString(letfButStr), getString(rightButStr),leftBtnClick);
	}
	/**
	 * 构建确认窗口 无title 提示内容为字符串 取消为关闭窗口
	 * @param context
	 * @param content
	 * @param leftBtnClick
	 * @return
	 */
	public static CommonDialogViewModel createConfirmDialogNotT(Context context,int content
			,OnBtnClickCallBack leftBtnClick){
		return createConfirmDialog(context, null, getString(content)
				, getString(ResUtil.STR_BTN_TEXT_CONFIRM), getString(ResUtil.STR_BTN_TEXT_CANCEL),leftBtnClick);
	}
	/**
	 * 构建确认窗口 无title 提示内容为字符串 取消为关闭窗口
	 * @param context
	 * @param content
	 * @param leftBtnClick
	 * @return
	 */
	public static CommonDialogViewModel createConfirmDialogNotT(Context context,String content
			,OnBtnClickCallBack leftBtnClick){
		return createConfirmDialog(context, null, content
				, getString(ResUtil.STR_BTN_TEXT_CONFIRM), getString(ResUtil.STR_BTN_TEXT_CANCEL),leftBtnClick);
	}
	/**
	 * 构建确认窗口 无title 提示内容为字符串 取消为关闭窗口
	 * @param context
	 * @param content
	 * @param letfButStr
	 * @param rightButStr
	 * @param leftBtnClick
	 * @return
	 */
	public static CommonDialogViewModel createConfirmDialogNotT(Context context,int content
			,int letfButStr,int rightButStr,OnBtnClickCallBack leftBtnClick){
		return createConfirmDialog(context, null, getString(content)
				, getString(letfButStr), getString(rightButStr),leftBtnClick);
	}
	/**
	 * 构建确认窗口 无title 提示内容为字符串 取消为关闭窗口
	 * @param context
	 * @param content
	 * @param letfButStr
	 * @param rightButStr
	 * @param leftBtnClick
	 * @return
	 */
	public static CommonDialogViewModel createConfirmDialogNotT(Context context,String content
			,String letfButStr,String rightButStr,OnBtnClickCallBack leftBtnClick){
		return createConfirmDialog(context, null, content
				, letfButStr, rightButStr,leftBtnClick);
	}
	/**
	 * 构建确认窗口  提示内容为字符串  取消为关闭窗口
	 * @param context
	 * @param title 为空则无title
	 * @param content
	 * @param letfButStr
	 * @param rightButStr
	 * @param leftBtnClick
	 * @return
	 */
	public static CommonDialogViewModel createConfirmDialog(Context context,String title,String content
			,String letfButStr,String rightButStr,OnBtnClickCallBack leftBtnClick){
		CommonDialogViewModel dialogViewModel = new CommonDialogViewModel(context);
		dialogViewModel.setContentStr(content);
		dialogViewModel.setTitle(title);
		dialogViewModel.setBtn(letfButStr, rightButStr, leftBtnClick, new OnBtnClickCallBack() {
			@Override
			public void onClick(View v, BaseDialogViewModel viewModel) {
				if(viewModel.isShowing()){
					viewModel.cancel();
				}
			}
		});
		new CommonDialog(context, dialogViewModel);
		return dialogViewModel;
	}
	/**
	 * 构建确认窗口 无title 提示内容为字符串 取消为关闭窗口
	 * @param context
	 * @param content
	 * @param leftBtnClick
	 * @return
	 */
	public static CommonDialogViewModel createConfirmDialogNotTSingle(Context context,int content,OnBtnClickCallBack leftBtnClick){
		return createSingleConfirmDialog(context, null, getString(content), getString(ResUtil.STR_BTN_TEXT_CONFIRM));
	}
	/**
	 * 构建确认窗口 无title 提示内容为字符串 取消为关闭窗口
	 * @param context
	 * @param content
	 * @param leftBtnClick
	 * @return
	 */
	public static CommonDialogViewModel createConfirmDialogNotTSingle(Context context,String content){
		return createSingleConfirmDialog(context, null, content, getString(ResUtil.STR_BTN_TEXT_CONFIRM));
	}
	/**
	 * 构建确认窗口 无title 提示内容为字符串 取消为关闭窗口
	 * @param context
	 * @param content
	 * @param letfButStr
	 * @param rightButStr
	 * @param leftBtnClick
	 * @return
	 */
	public static CommonDialogViewModel createConfirmDialogNotTSingle(Context context,int content,int letfButStr){
		return createSingleConfirmDialog(context, null, getString(content), getString(letfButStr));
	}
	/**
	 * 构建确认窗口 无title 提示内容为字符串 取消为关闭窗口
	 * @param context
	 * @param content
	 * @param letfButStr
	 * @param rightButStr
	 * @param leftBtnClick
	 * @return
	 */
	public static CommonDialogViewModel createConfirmDialogNotTSingle(Context context,String content,String letfButStr){
		return createSingleConfirmDialog(context, null, content, letfButStr);
	}
	/**
	 * 构建确认窗口  提示内容为字符串  取消为关闭窗口
	 * @param context
	 * @param title 为空则无title
	 * @param content
	 * @param letfButStr
	 * @param rightButStr
	 * @param leftBtnClick
	 * @return
	 */
	public static CommonDialogViewModel createSingleConfirmDialog(Context context,String title,String content,String letfButStr){
		CommonDialogViewModel dialogViewModel = new CommonDialogViewModel(context);
		dialogViewModel.setContentStr(content);
		dialogViewModel.setTitle(title);
		dialogViewModel.setBtn(letfButStr,null, new OnBtnClickCallBack() {
			@Override
			public void onClick(View v, BaseDialogViewModel viewModel) {
				if(viewModel.isShowing()){
					viewModel.cancel();
				}
			}
		},null);
		new CommonDialog(context, dialogViewModel);
		return dialogViewModel;
	}
	/**
	 *  构建确认窗口  无title 提示内容为自定义View 取消为关闭窗口
	 * @param context
	 * @param contentRes
	 * @param viewModel
	 * @return
	 */
	public static CommonDialogViewModel createConfirmVDialogNotT(Context context,int contentRes,Object viewModel){
		return createConfirmVDialog(context, null, contentRes , viewModel
				,getString(ResUtil.STR_BTN_TEXT_CONFIRM), getString(ResUtil.STR_BTN_TEXT_CANCEL),null);
	}
	/**
	 *  构建确认窗口  无title 提示内容为自定义View 取消为关闭窗口
	 * @param context
	 * @param contentRes
	 * @param viewModel
	 * @param leftBtnClick
	 * @return
	 */
	public static CommonDialogViewModel createConfirmVDialogNotT(Context context,int contentRes
			,Object viewModel,OnBtnClickCallBack leftBtnClick){
		return createConfirmVDialog(context, null, contentRes , viewModel
				,getString(ResUtil.STR_BTN_TEXT_CONFIRM), getString(ResUtil.STR_BTN_TEXT_CANCEL),leftBtnClick);
	}
	
	/**
	 * 构建确认窗口  无title 提示内容为自定义View 取消为关闭窗口
	 * @param context
	 * @param contentRes
	 * @param viewModel
	 * @param letfButStr
	 * @param rightButStr
	 * @param leftBtnClick
	 * @return
	 */
	public static CommonDialogViewModel createConfirmVDialogNotT(Context context,int contentRes
			,Object viewModel,String letfButStr,String rightButStr,OnBtnClickCallBack leftBtnClick){
		return createConfirmVDialog(context, null, contentRes , viewModel, letfButStr, rightButStr,leftBtnClick);
	}
	/**
	 * 构建确认窗口  无title 提示内容为自定义View 取消为关闭窗口
	 * @param context
	 * @param contentRes
	 * @param viewModel
	 * @param letfButStr
	 * @param rightButStr
	 * @param leftBtnClick
	 * @return
	 */
	public static CommonDialogViewModel createConfirmVDialogNotT(Context context,int contentRes
			,Object viewModel,int letfButStr,int rightButStr,OnBtnClickCallBack leftBtnClick){
		return createConfirmVDialog(context, null, contentRes , viewModel, getString(letfButStr), getString(rightButStr),leftBtnClick);
	}
	/**
	 * 构建确认窗口  提示内容为自定义View 取消为关闭窗口
	 * @param context
	 * @param contentRes
	 * @param viewModel
	 * @param leftBtnClick
	 * @return
	 */
	public static CommonDialogViewModel createConfirmVDialog(Context context,int contentRes
			,Object viewModel,OnBtnClickCallBack leftBtnClick){
		return createConfirmVDialog(context, getString(ResUtil.STR_APP_NAME), contentRes ,viewModel, getString(ResUtil.STR_BTN_TEXT_CONFIRM)
				, getString(ResUtil.STR_BTN_TEXT_CANCEL),leftBtnClick);
	}
	/**
	 * 构建确认窗口  提示内容为自定义View 取消为关闭窗口
	 * @param context
	 * @param title
	 * @param contentRes
	 * @param viewModel
	 * @param leftBtnClick
	 * @return
	 */
	public static CommonDialogViewModel createConfirmVDialog(Context context,String title,int contentRes
			,Object viewModel,OnBtnClickCallBack leftBtnClick){
		return createConfirmVDialog(context, title, contentRes ,viewModel, getString(ResUtil.STR_BTN_TEXT_CONFIRM)
				, getString(ResUtil.STR_BTN_TEXT_CANCEL),leftBtnClick);
	}
	/**
	 * 构建确认窗口  提示内容为自定义View 取消为关闭窗口
	 * @param context
	 * @param title
	 * @param contentRes
	 * @param viewModel
	 * @param leftBtnClick
	 * @return
	 */
	public static CommonDialogViewModel createConfirmVDialog(Context context,int title,int contentRes
			,Object viewModel,OnBtnClickCallBack leftBtnClick){
		return createConfirmVDialog(context, getString(title), contentRes ,viewModel, getString(ResUtil.STR_BTN_TEXT_CONFIRM)
				, getString(ResUtil.STR_BTN_TEXT_CANCEL),leftBtnClick);
	}
	/**
	 * 构建确认窗口  提示内容为自定义View 取消为关闭窗口
	 * @param context
	 * @param title
	 * @param contentRes
	 * @param viewModel
	 * @param letfButStr
	 * @param rightButStr
	 * @param leftBtnClick
	 * @return
	 */
	public static CommonDialogViewModel createConfirmVDialog(Context context,int title,int contentRes
			,Object viewModel,int letfButStr,int rightButStr,OnBtnClickCallBack leftBtnClick){
		return createConfirmVDialog(context, getString(title), contentRes , viewModel,getString(letfButStr)
				, getString(rightButStr),leftBtnClick);
	}
	/** 构建确认窗口  提示内容为自定义View 取消为关闭窗口
	 * @param context
	 * @param contentRes
	 * @param viewModel
	 * @param letfButStr
	 * @param rightButStr
	 * @param leftBtnClick
	 * @return
	 */
	public static CommonDialogViewModel createConfirmVDialog(Context context,int contentRes
			,Object viewModel,int letfButStr,int rightButStr,OnBtnClickCallBack leftBtnClick){
		return createConfirmVDialog(context, null, contentRes , viewModel,getString(letfButStr)
				, getString(rightButStr),leftBtnClick);
	}
	/**
	 * 构建确认窗口  提示内容为自定义View 取消为关闭窗口
	 * @param context
	 * @param title
	 * @param contentRes
	 * @param viewModel
	 * @param letfButStr
	 * @param rightButStr
	 * @param leftBtnClick
	 * @return
	 */
	public static CommonDialogViewModel createConfirmVDialog(Context context,String title,int contentRes
			,Object viewModel,String letfButStr,String rightButStr,OnBtnClickCallBack leftBtnClick){
		CommonDialogViewModel dialogViewModel = new CommonDialogViewModel(context);
		dialogViewModel.setContentViewRes(contentRes);
		dialogViewModel.setContentViewViewModel(viewModel);
		dialogViewModel.setTitle(title);
		dialogViewModel.setBtn(letfButStr, rightButStr, leftBtnClick, new OnBtnClickCallBack() {
			@Override
			public void onClick(View v, BaseDialogViewModel viewModel) {
				if(viewModel.isShowing()){
					viewModel.cancel();
				}
			}
		});
		new CommonDialog(context, dialogViewModel);
		return dialogViewModel;
	}
	/**
	 * 构建确认窗口  提示内容为自定义View 单按钮没标题
	 * @param context
	 * @param title
	 * @param contentRes
	 * @param viewModel
	 * @param leftBtnClick
	 * @return
	 */
	public static CommonDialogViewModel createConfirmVDialogSingleNotT(Context context,int contentRes
			,Object viewModel,OnBtnClickCallBack leftBtnClick){
		return createConfirmVDialogSingleNotT(context, contentRes ,viewModel, getString(ResUtil.STR_BTN_TEXT_CONFIRM),leftBtnClick);
	}
	/**
	 * 构建确认窗口  提示内容为自定义View 单按钮没标题
	 * @param context
	 * @param title
	 * @param contentRes
	 * @param viewModel
	 * @param letfButStr
	 * @param rightButStr
	 * @param leftBtnClick
	 * @return
	 */
	public static CommonDialogViewModel createConfirmVDialogSingleNotT(Context context,int contentRes
			,Object viewModel,int letfButStr,OnBtnClickCallBack leftBtnClick){
		return createConfirmVDialogSingleNotT(context, contentRes , viewModel,getString(letfButStr),leftBtnClick);
	}
	/**
	 * 构建确认窗口  提示内容为自定义View 单按钮没标题
	 * @param context
	 * @param title
	 * @param contentRes
	 * @param viewModel
	 * @param letfButStr
	 * @param rightButStr
	 * @param leftBtnClick
	 * @return
	 */
	public static CommonDialogViewModel createConfirmVDialogSingleNotT(Context context,int contentRes,Object viewModel,String letfButStr,OnBtnClickCallBack leftBtnClick){
		CommonDialogViewModel dialogViewModel = new CommonDialogViewModel(context);
		dialogViewModel.setContentViewRes(contentRes);
		dialogViewModel.setContentViewViewModel(viewModel);
		dialogViewModel.setBtn(letfButStr,"", leftBtnClick,null);
		new CommonDialog(context, dialogViewModel);
		return dialogViewModel;
	}
	/**
	 * 构建确认窗口  提示内容为自定义View 单按钮
	 * @param context
	 * @param contentRes
	 * @param viewModel
	 * @param leftBtnClick
	 * @return
	 */
	public static CommonDialogViewModel createConfirmVDialogSingle(Context context,int contentRes
			,Object viewModel,OnBtnClickCallBack leftBtnClick){
		return createConfirmVDialogSingle(context, getString(ResUtil.STR_APP_NAME), contentRes ,viewModel, getString(ResUtil.STR_BTN_TEXT_CONFIRM),leftBtnClick);
	}
	/**
	 * 构建确认窗口  提示内容为自定义View 单按钮
	 * @param context
	 * @param title
	 * @param contentRes
	 * @param viewModel
	 * @param leftBtnClick
	 * @return
	 */
	public static CommonDialogViewModel createConfirmVDialogSingle(Context context,String title,int contentRes
			,Object viewModel,OnBtnClickCallBack leftBtnClick){
		return createConfirmVDialogSingle(context, title, contentRes ,viewModel, getString(ResUtil.STR_BTN_TEXT_CONFIRM),leftBtnClick);
	}
	/**
	 * 构建确认窗口  提示内容为自定义View 单按钮
	 * @param context
	 * @param title
	 * @param contentRes
	 * @param viewModel
	 * @param leftBtnClick
	 * @return
	 */
	public static CommonDialogViewModel createConfirmVDialogSingle(Context context,int title,int contentRes
			,Object viewModel,OnBtnClickCallBack leftBtnClick){
		return createConfirmVDialogSingle(context, getString(title), contentRes ,viewModel, getString(ResUtil.STR_BTN_TEXT_CONFIRM),leftBtnClick);
	}
	/**
	 * 构建确认窗口  提示内容为自定义View 单按钮
	 * @param context
	 * @param title
	 * @param contentRes
	 * @param viewModel
	 * @param letfButStr
	 * @param rightButStr
	 * @param leftBtnClick
	 * @return
	 */
	public static CommonDialogViewModel createConfirmVDialogSingle(Context context,int title,int contentRes
			,Object viewModel,int letfButStr,OnBtnClickCallBack leftBtnClick){
		return createConfirmVDialogSingle(context, getString(title), contentRes , viewModel,getString(letfButStr),leftBtnClick);
	}
	/**
	 * 构建确认窗口  提示内容为自定义View 单按钮
	 * @param context
	 * @param title
	 * @param contentRes
	 * @param viewModel
	 * @param letfButStr
	 * @param rightButStr
	 * @param leftBtnClick
	 * @return
	 */
	public static CommonDialogViewModel createConfirmVDialogSingle(Context context,String title,int contentRes
			,Object viewModel,String letfButStr,OnBtnClickCallBack leftBtnClick){
		CommonDialogViewModel dialogViewModel = new CommonDialogViewModel(context);
		dialogViewModel.setContentViewRes(contentRes);
		dialogViewModel.setContentViewViewModel(viewModel);
		dialogViewModel.setTitle(title);
		dialogViewModel.setBtn(letfButStr,"", leftBtnClick,null);
		new CommonDialog(context, dialogViewModel);
		return dialogViewModel;
	}
	/**
	 * 构建等待窗口，无title，无按钮
	 * @param context
	 * @return
	 */
	public static CommonDialogViewModel createWaitingDialog(Context context){
		WaitingDialogViewModel dialogViewModel = new WaitingDialogViewModel(context);
		CommonDialog commonDialog = new CommonDialog(context, dialogViewModel,R.style.AppCenterDialogTheme);
		commonDialog.setFullWidth(false);
		commonDialog.setGravity(Gravity.CENTER);
		return dialogViewModel;
	}
	
	/**
	 * 构建流量警告
	 * @param context
	 * @return
	 */
	public static CommonDialogViewModel createTrafficWarningDialog(Context context){
		final CommonDialogViewModel dialogViewModel = createConfirmDialogNotT(context, R.string.tip_traffic_warning
				,R.string.btn_text_confirm,  R.string.btn_text_cancel
				, new EmptyOnBtnClickCallBack());
		return dialogViewModel;
	}
	
	/**
	 * 构建流量警告
	 * @param context
	 * @return
	 */
	public static CommonDialogViewModel createNotNetDialog(final Context context){
		CommonDialogViewModel dialogViewModel = createConfirmDialogNotT(context, R.string.tip_on_net
				,R.string.btn_text_net_setting,  R.string.btn_text_next_do
				, new OnBtnClickCallBack() {
					@Override
					public void onClick(View v, BaseDialogViewModel viewModel) {
						AppUtil.gotoSettingActivity(context);
					}
				});
		return dialogViewModel;
	}
	
	public static BaseDialogViewModel bindDialogViewModel(Context context,String type,Object viewModel) {
		if(BaseViewModel.WAITING_DIALOG.equals(type)){
			return DialogUtil.createWaitingDialog(context);
		}else if(BaseViewModel.TRAFFIC_WARNING_DIALOG.equals(type)){
			return DialogUtil.createTrafficWarningDialog(context);
		}else if(BaseViewModel.NOT_NET_DIALOG.equals(type)){
			return DialogUtil.createNotNetDialog(context);
		}
		return null;
	}
	
	private static String getString(int resId){
		return BaseApp.getInstance().getString(resId);
	}
	
	private static class EmptyOnBtnClickCallBack implements OnBtnClickCallBack{
		@Override
		public void onClick(View v, BaseDialogViewModel viewModel) {
		}
	}
}
