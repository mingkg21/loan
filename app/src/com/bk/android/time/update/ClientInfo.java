package com.bk.android.time.update;

import com.bk.android.time.entity.BaseDataEntity;
import com.google.gson.annotations.SerializedName;

/**
 * 客户端信息
 * 
 * @author mingkg21
 * @date 2010-4-16
 * @email mingkg21@gmail.com
 */
public final class ClientInfo extends BaseDataEntity{
	private static final long serialVersionUID = 7833235030508907871L;
	/** 客户端升级版本号 */
	@SerializedName("version_code")
	private int updateVersion;
	/** 客户端升级版本号 */
	@SerializedName("version_name")
	private String updateVersionName;
	/** 客户端升级地址 */
	@SerializedName("download_url")
	private String updateURL;
	/** 是否强制升级 */
	@SerializedName("must_update_flag")
	private int mustUpdate;
	/** 客户端升级版本信息 */
	@SerializedName("whatsnew")
	private String updateMessage = "暂无";
	/** 客户端是否静默下载 */
	@SerializedName("up_slient")
	private int silenceUpdate;
	/** 版本更新时间*/
	@SerializedName("released_datetime")
	private long time;
	/** 文件大小*/
	@SerializedName("download_size")
	private long size;
	/**
	 * @return the updateVersionName
	 */
	public String getUpdateVersionName() {
		return updateVersionName;
	}
	/**
	 * @param updateVersionName the updateVersionName to set
	 */
	public void setUpdateVersionName(String updateVersionName) {
		this.updateVersionName = updateVersionName;
	}
	/**
	 * @return the updateVersion
	 */
	public int getUpdateVersion() {
		return updateVersion;
	}
	/**
	 * @param updateVersion the updateVersion to set
	 */
	public void setUpdateVersion(int updateVersion) {
		this.updateVersion = updateVersion;
	}
	/**
	 * @return the updateURL
	 */
	public String getUpdateURL() {
		return updateURL;
	}
	/**
	 * @param updateURL the updateURL to set
	 */
	public void setUpdateURL(String updateURL) {
		this.updateURL = updateURL;
	}
	/**
	 * @return the mustUpdate
	 */
	public boolean isMustUpdate() {
		return mustUpdate == 1;
	}
	/**
	 * @param mustUpdate the mustUpdate to set
	 */
	public void setMustUpdate(int mustUpdate) {
		this.mustUpdate = mustUpdate;
	}
	/**
	 * @return the updateMessage
	 */
	public String getUpdateMessage() {
		return updateMessage;
	}
	/**
	 * @param updateMessage the updateMessage to set
	 */
	public void setUpdateMessage(String updateMessage) {
		this.updateMessage = updateMessage;
	}
	/**
	 * @return the silenceUpdate
	 */
	public boolean isSilenceUpdate() {
		return silenceUpdate == 1;
	}
	/**
	 * @param silenceUpdate the silenceUpdate to set
	 */
	public void setSilenceUpdate(int silenceUpdate) {
		this.silenceUpdate = silenceUpdate;
	}
	/**
	 * @return the time
	 */
	public long getTime() {
		return time;
	}
	/**
	 * @param time the time to set
	 */
	public void setTime(long time) {
		this.time = time;
	}
	/**
	 * @return the size
	 */
	public long getSize() {
		return size;
	}
	/**
	 * @param size the size to set
	 */
	public void setSize(long size) {
		this.size = size;
	}
}
