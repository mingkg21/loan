package com.bk.android.time.update;

import android.app.Activity;
import android.os.Bundle;
import android.text.format.Formatter;
import android.text.method.ScrollingMovementMethod;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bk.android.app.BaseActivity;
import com.bk.android.assistant.R;
import com.bk.android.time.data.Preferences;
import com.bk.android.update.IUpdateActivity;
import com.bk.android.update.UpdateInfo;
import com.bk.android.update.UpdateManager;

public class UpdateActivity extends BaseActivity implements IUpdateActivity{
	private ProgressBar progressBar;
	private TextView progressTv;

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		setGravity(getDefaultGravity());
		WindowManager.LayoutParams lp = getWindow().getAttributes();
		lp.width = getResources().getDisplayMetrics().widthPixels;
		getWindow().setAttributes(lp);
		UpdateManager.getInstance().dispatchActivityCreate(this);
	}

	@Override
	public void setContentView(int layoutResID) {
		setContentView(getLayoutInflater().inflate(layoutResID, null));
	}
	
	@Override
	public void setContentView(View view) {
		setContentView(view,null);
	}
	
	@Override
	public void setContentView(View view, LayoutParams params) {
		params = new LayoutParams((int) (getResources().getDisplayMetrics().widthPixels * 0.8f), LayoutParams.WRAP_CONTENT);
		super.setContentView(view, params);
	}
	
	public void setGravity(int gravity){
		Window window = getWindow();    
		window.setGravity(gravity);
	}
	
	private int getDefaultGravity(){
		return Gravity.CENTER;
	}
	
	@Override
	public void finish() {
		UpdateManager.getInstance().dispatchActivityFinish(this);
		super.finish();
	}

	private void setFinishOnTouchOutside(UpdateInfo updateInfo) {
		if(android.os.Build.VERSION.SDK_INT >= 11){
			setFinishOnTouchOutside(!updateInfo.isMustUpdate());
		}
	}
	
	@Override
	public void onShowUpdateInfo(final UpdateInfo updateInfo,final OnClickListener confirmListener, final OnClickListener cancelListener) {
		setFinishOnTouchOutside(updateInfo);
		final Preferences preferences = Preferences.getInstance();
		setContentView(R.layout.uniq_update_details_lay);
		((TextView) findViewById(R.id.update_client_title)).setText(getString(R.string.check_client_update_version_tip, updateInfo.getUpdateVersionName()));
		TextView updateMessageTV = ((TextView) findViewById(R.id.update_message));
		updateMessageTV.setText(updateInfo.getUpdateMessage());
		updateMessageTV.setMovementMethod(ScrollingMovementMethod.getInstance());
//		final CheckBox cBox = (CheckBox)findViewById(R.id.no_ask_cb);
		
		Button okBtn = (Button) findViewById(R.id.dialog_update_now);
		Button cancelBtn = (Button) findViewById(R.id.dialog_update_later);
		
		if(updateInfo.isMustUpdate()){
			cancelBtn.setVisibility(View.GONE);
		}else{
//			cBox.setVisibility(View.VISIBLE);
//			cBox.setChecked(false);
			cancelBtn.setVisibility(View.VISIBLE);
		}
		// 现在更新
		okBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(confirmListener != null){
					confirmListener.onClick(v);
				}
				if(!updateInfo.isMustUpdate()){
//					if(cBox.isChecked()){
//						String updateVersion = updateInfo.getUpdateVersionName();
//						preferences.setShowUpdateAgain(updateVersion, true);
//					}else{
//						preferences.setShowUpdateAgain("", false);
//					}
				}
			}
		});
		// 稍后更新
		cancelBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(cancelListener != null){
					cancelListener.onClick(v);
				}
				if(!updateInfo.isMustUpdate()){
//					if(cBox.isChecked()){
//						String updateVersion = updateInfo.getUpdateVersionName();
//						preferences.setShowUpdateAgain(updateVersion, true);
//					}else{
//						preferences.setShowUpdateAgain("", false);
//					}
				}
			}
		});
	}

	@Override
	public void onShowInstallUpdate(final UpdateInfo updateInfo,final OnClickListener confirmListener,final OnClickListener cancelListener) {
		setFinishOnTouchOutside(updateInfo);
		final Preferences preferences = Preferences.getInstance();
		setContentView(R.layout.uniq_update_details_lay);
		((TextView) findViewById(R.id.update_client_title)).setText(getString(R.string.check_client_update_version_tip, updateInfo.getUpdateVersionName()));
		TextView updateMessageTV = ((TextView) findViewById(R.id.update_message));
		updateMessageTV.setText(updateInfo.getUpdateMessage());
		updateMessageTV.setMovementMethod(ScrollingMovementMethod.getInstance());
//		final CheckBox cBox = (CheckBox)findViewById(R.id.no_ask_cb);
		
		Button okBtn = (Button) findViewById(R.id.dialog_update_now);
		okBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(confirmListener != null){
					confirmListener.onClick(v);
				}
				if(!updateInfo.isMustUpdate()){
//					if(cBox.isChecked()){
//						String updateVersion = updateInfo.getUpdateVersionName();
//						preferences.setShowUpdateAgain(updateVersion, true);
//					}else{
//						preferences.setShowUpdateAgain("", false);
//					}
				}
			}
		});
		Button cancelBtn = (Button) findViewById(R.id.dialog_update_later);
		if(updateInfo.isMustUpdate()){
			cancelBtn.setVisibility(View.GONE);
		}else{
//			cBox.setVisibility(View.VISIBLE);
//			cBox.setChecked(false);
			cancelBtn.setVisibility(View.VISIBLE);
		}
		cancelBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(cancelListener != null){
					cancelListener.onClick(v);
				}
				if(!updateInfo.isMustUpdate()){
//					if(cBox.isChecked()){
//						String updateVersion = updateInfo.getUpdateVersionName();
//						preferences.setShowUpdateAgain(updateVersion, true);
//					}else{
//						preferences.setShowUpdateAgain("", false);
//					}
				}
			}
		});
	
	}

	@Override
	public void onShowUpdateDownlaodInfo(UpdateInfo updateInfo,
			OnClickListener backgrounderListener,
			OnClickListener cancelListener, long currentBytes, long totalBytes) {
		setFinishOnTouchOutside(updateInfo);
		setContentView(R.layout.uniq_update_waitting_lay);
		((TextView) findViewById(R.id.update_client_title)).setText(getString(R.string.check_client_update_version_tip, updateInfo.getUpdateVersionName()));
		TextView updateMessageTV = ((TextView) findViewById(R.id.update_message));
		updateMessageTV.setText(updateInfo.getUpdateMessage());
		updateMessageTV.setMovementMethod(ScrollingMovementMethod.getInstance());
		
		progressBar = (ProgressBar) findViewById(R.id.update_pb);
		progressTv = (TextView) findViewById(R.id.update_tv);
		
		onUpdateProgressBar(updateInfo,(int)currentBytes , (int)totalBytes);
		
		if(updateInfo.isMustUpdate()){
			findViewById(R.id.update_cancel_btn).setVisibility(View.GONE);
			findViewById(R.id.update_back_btn).setVisibility(View.GONE);
		}else{
			findViewById(R.id.update_cancel_btn).setVisibility(View.VISIBLE);
			findViewById(R.id.update_back_btn).setVisibility(View.VISIBLE);
		}
		findViewById(R.id.update_cancel_btn).setOnClickListener(cancelListener);
		findViewById(R.id.update_back_btn).setOnClickListener(backgrounderListener);
	}

	@Override
	public void onUpdateProgressBar(UpdateInfo updateInfo, long currentBytes,
			long totalBytes) {
		setFinishOnTouchOutside(updateInfo);
		if(currentBytes < 0 || totalBytes < 0 || progressBar == null || progressTv == null){
			return;
		}
		int progressAmount = 0;
		if(totalBytes > 0){
			progressAmount = (int) (currentBytes * 100 / totalBytes);
			if (progressBar != null) {
				progressBar.setProgress(progressAmount);
			}
			progressBar.setVisibility(View.VISIBLE);
		}else{
			progressBar.setVisibility(View.GONE);
		}
		if (progressTv != null) {
			StringBuilder sb = new StringBuilder();
			sb.append(Formatter.formatFileSize(this, currentBytes));
			if(totalBytes > 0){
				sb.append("/");
				sb.append(Formatter.formatFileSize(this, totalBytes));
			}
			progressTv.setText(getString(R.string.update_download_size_tip,sb));
		}
		
	}

	@Override
	public void onShowDownloadFail(UpdateInfo updateInfo,
			OnClickListener retryListener, OnClickListener cancelListener) {
		setFinishOnTouchOutside(updateInfo);
		setContentView(R.layout.uniq_update_waitting_lay);
		((TextView) findViewById(R.id.update_client_title)).setText(getString(R.string.check_client_update_version_tip, updateInfo.getUpdateVersionName()));
		((TextView) findViewById(R.id.update_message)).setText(R.string.update_download_fail_tip);
		findViewById(R.id.update_pb).setVisibility(View.GONE);
		findViewById(R.id.update_tv).setVisibility(View.GONE);
		Button okBtn = (Button) findViewById(R.id.update_back_btn);
		okBtn.setText(R.string.update_download_now_redownload);
		okBtn.setOnClickListener(retryListener);
		Button cancelBtn = (Button) findViewById(R.id.update_cancel_btn);
		cancelBtn.setText(R.string.btn_text_next_do);
		cancelBtn.setOnClickListener(cancelListener);
	}

	@Override
	public Activity getActivity() {
		return this;
	}

	@Override
	protected void dispatchNetworkChange(boolean isAvailable) {
	}

	@Override
	protected void onInitBroadcastReceiver() {
	}
}
