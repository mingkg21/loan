package com.bk.android.time.update;

import android.app.Activity;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.app.NotificationCompat.Builder;
import android.text.format.Formatter;
import android.view.View;
import android.widget.RemoteViews;

import com.bk.android.assistant.R;
import com.bk.android.data.net.AbsConnect;
import com.bk.android.data.net.RequestData;
import com.bk.android.os.TerminableThread;
import com.bk.android.time.app.App;
import com.bk.android.time.app.AppBroadcast;
import com.bk.android.time.data.Preferences;
import com.bk.android.time.data.net.HttpConnect;
import com.bk.android.time.model.common.CommonDialogViewModel;
import com.bk.android.time.util.AppFileUtil;
import com.bk.android.time.util.DialogUtil;
import com.bk.android.time.util.ToastUtil;
import com.bk.android.update.IHttpFactory;
import com.bk.android.update.INotification;
import com.bk.android.update.IUpdateApp;
import com.bk.android.update.UpdateInfo;
import com.bk.android.update.UpdateManager;
import com.bk.android.update.UpdateSetting;
import com.bk.android.util.ApnUtil;
import com.bk.android.util.AppUtil;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;

public class AppUpdate{
	public static void init(Context context){
		UpdateSetting updateSetting = new UpdateSetting();
		updateSetting.mApkSavePath = AppFileUtil.getAppPath() + "updateApk/";
		updateSetting.mHttpFactory = new HttpFactory();
		updateSetting.mNotification = new UpdateNotification();
		updateSetting.mUpdateApp = new UpdateApp();
		updateSetting.mUpdateActivityCls = UpdateActivity.class;
		UpdateManager.init(context, updateSetting);
	}
	
	private static class UpdateApp implements IUpdateApp{
		@Override
		public IntentFilter getCloseAppIntentFilter() {
			return new IntentFilter(AppBroadcast.ACTION_CLOSE_APP);
		}

		@Override
		public void exitApp(Context context) {
			context.sendBroadcast(new Intent(AppBroadcast.ACTION_CLOSE_APP));
		}

		@Override
		public void clearShowUpdateAgain(Context context) {
			Preferences.getInstance().setShowUpdateAgain("", false);
		}
	}
	
	private static class HttpFactory implements IHttpFactory{
		@Override
		public IHttpController createHttp(Context context, String url) {
			final DefaultHttpClient client = AbsConnect.getDefaultHttpClient(context);
			final HttpGet get = new HttpGet(url);
			return new IHttpController() {
				
				@Override
				public void shutdown() {
					if(client != null){
						client.getConnectionManager().shutdown();
					}
				}
				
				@Override
				public HttpResponse execute() throws Exception {
					return client.execute(get);
				}
			};
		}
	}
	
	private static class UpdateNotification implements INotification{
		private Notification sNotification;
		@Override
		public Notification fillDownlaodInfoNotification(Context context,UpdateInfo updateInfo, Intent intent, long currentBytes,long totalBytes) {
			int progress = 0;
			
			if(totalBytes != 0 && totalBytes > 0){
				progress = (int) ( ( currentBytes * 1f / totalBytes ) * 100 );
			}
			
			RemoteViews remoteViews = null;
			if(sNotification == null){
				remoteViews = new RemoteViews(context.getPackageName(), R.layout.uniq_update_notification_download_lay);
				Builder builder = new Builder(context);
				builder.setSmallIcon(R.drawable.ic_launcher);
				builder.setTicker(context.getString(R.string.update_client_tip));
				builder.setOngoing(true);
				builder.setContent(remoteViews);
				sNotification = builder.build();
			}else{
				remoteViews = sNotification.contentView;
			}
			sNotification.contentIntent = PendingIntent.getActivity(
					context,
					this.hashCode() + 1,
					intent,
					PendingIntent.FLAG_UPDATE_CURRENT);
			if(totalBytes > 0){
				remoteViews.setProgressBar(R.id.notification_download_progress, 100, progress , false);
				remoteViews.setViewVisibility(R.id.notification_download_progress, View.VISIBLE);
				remoteViews.setViewVisibility(R.id.notification_download_content_tv, View.GONE);
				StringBuilder sb = new StringBuilder();
				sb.append(Formatter.formatFileSize(context, currentBytes));
				if(totalBytes > 0){
					sb.append("/");
					sb.append(Formatter.formatFileSize(context, totalBytes));
				}
				remoteViews.setTextViewText(R.id.notification_download_title_tv,context.getString(R.string.update_client_size_tip,sb.toString()));
			}else{
				remoteViews.setViewVisibility(R.id.notification_download_content_tv, View.VISIBLE);
				remoteViews.setTextViewText(R.id.notification_download_content_tv,Formatter.formatFileSize(context, currentBytes));
				remoteViews.setViewVisibility(R.id.notification_download_progress, View.GONE);
				remoteViews.setTextViewText(R.id.notification_download_title_tv,context.getString(R.string.update_client_tip));
			}
			return sNotification;
		}

		@Override
		public Notification fillDownloadFailNotification(Context context,UpdateInfo updateInfo, Intent intent) {
			PendingIntent contentIntent = PendingIntent.getActivity(
					context,
					this.hashCode(),   
					intent,  
					PendingIntent.FLAG_UPDATE_CURRENT);
			
			String str = context.getString(R.string.update_download_fail_tip);
			Builder builder = new Builder(context);
			builder.setSmallIcon(R.drawable.ic_launcher);
			builder.setContentTitle(str);
			builder.setContentText(context.getString(R.string.update_download_fail_click_tip));
			builder.setAutoCancel(true);
			builder.setTicker(str);
			builder.setContentIntent(contentIntent);
			builder.setDefaults(Notification.DEFAULT_SOUND);
			return builder.build();
		}
	}
	
	public static UpdateInfo getUpdateInfo(ClientInfo clientInfo){
		UpdateInfo updateInfo = new UpdateInfo(clientInfo.isMustUpdate(),clientInfo.isSilenceUpdate(), clientInfo.getUpdateMessage(), 
				clientInfo.getSize(), clientInfo.getUpdateURL(), clientInfo.getUpdateVersionName() ,AppUtil.getApkVersionName(App.getInstance()));
		return updateInfo;
	}
	
	private static CheckUpdateCallback sCheckUpdateCallback;
	
	public static void checkUpdate() {
		checkUpdate(null);
	}
	
	public static void checkUpdate(final CheckUpdateCallback callback) {
		sCheckUpdateCallback = new CheckUpdateCallback() {
			@Override
			public void onResult(boolean isSucceed, ClientInfo clientInfo) {
				if(callback != null){
					callback.onResult(isSucceed, clientInfo);
				}
				if(sCheckUpdateFromUserCallback != null){
					return;
				}
				if(clientInfo != null && this.equals(sCheckUpdateCallback)){
					final String uVersion = Preferences.getInstance().getShowUpdateAgain();
					if(!clientInfo.getUpdateVersionName().equals(uVersion) || clientInfo.isMustUpdate()){
						UpdateManager.getInstance().startUpdateInfo(AppUpdate.getUpdateInfo(clientInfo),false);
					}
				}
			}
			@Override
			public void onPreLoad() {}
			@Override
			public void onPostLoad() {}
		};
		handCheckUpdate(sCheckUpdateCallback);
	}
	
	private static CheckUpdateCallback sCheckUpdateFromUserCallback;

	public static void checkUpdateFromUser(final Activity activity) {
		if(sCheckUpdateFromUserCallback != null){
			return;
		}
		final CommonDialogViewModel dialog = DialogUtil.createWaitingDialog(activity);
		sCheckUpdateFromUserCallback = new CheckUpdateCallback() {
			@Override
			public void onResult(boolean isSucceed, ClientInfo clientInfo) {
				if(clientInfo != null){
					UpdateManager.getInstance().startUpdateInfo(AppUpdate.getUpdateInfo(clientInfo));
				}else if(isSucceed){
					ToastUtil.showToast(activity, R.string.check_client_no_need_update);
				}else{
					ToastUtil.showToast(activity, R.string.tip_err_server);
				}
			}
			
			@Override
			public void onPreLoad() {
				if(!activity.isFinishing()){
					dialog.show();
				}
			}
			
			@Override
			public void onPostLoad() {
				if(!activity.isFinishing()){
					dialog.finish();
				}
				sCheckUpdateFromUserCallback = null;
			}
		};
		final TerminableThread terminableThread = handCheckUpdate(sCheckUpdateFromUserCallback);
		dialog.setOnDismissListener(new OnDismissListener() {
			@Override
			public void onDismiss(DialogInterface dialog) {
				if(terminableThread != null){
					terminableThread.cancel();
				}
			}
		});
	}
	
	private static ClientInfo sClientInfo = null;
	private static CheckUpdateTask sCheckUpdateTask;
	
	private static TerminableThread handCheckUpdate(CheckUpdateCallback callback) {
		if(sClientInfo != null){
			callback.onPreLoad();
			callback.onResult(true,sClientInfo);
			callback.onPostLoad();
			return null;
		}
		if(sCheckUpdateTask != null){
			sCheckUpdateTask.addCallback(callback);
			sCheckUpdateTask.onPreLoad();
			return sCheckUpdateTask.mTerminableThread;
		}
		sCheckUpdateTask = new CheckUpdateTask();
		sCheckUpdateTask.addCallback(callback);
		sCheckUpdateTask.onPreLoad();
		sCheckUpdateTask.mTerminableThread = new TerminableThread(new Runnable() {
			ClientInfoEntity data;
			@Override
			public void run() {
				if(ApnUtil.isNetAvailable(App.getInstance())){
					HashMap<String, Object> sendData = new HashMap<>();
					RequestData requestData = new RequestData("common.clientversion.checkUpdates", sendData);
					data = HttpConnect.getInstance().connection(requestData, ClientInfoEntity.class);
				}
				App.getHandler().post(new Runnable() {
					@Override
					public void run() {
						boolean isSucceed = data != null && data.isSucceed();
						boolean isCancel = sCheckUpdateTask.mTerminableThread.isCancel();
						ClientInfo clientInfo = null;
						if (isSucceed) {
							clientInfo = sClientInfo = data.getData();
//							Preferences.getInstance().setUpdateInfo(clientInfo);
						}else{
//							clientInfo = Preferences.getInstance().getUpdateInfo();
						}
						if(!isCancel && App.getInstance().isEnterApp()){
							sCheckUpdateTask.onResult(isSucceed,clientInfo);
						}
						sCheckUpdateTask.onPostLoad();
						sCheckUpdateTask = null;
					}
				});
			}
		});
		sCheckUpdateTask.mTerminableThread.start();
		return sCheckUpdateTask.mTerminableThread;
	}
	
	public interface CheckUpdateCallback{
		public void onPreLoad();
		public void onPostLoad();
		public void onResult(boolean isSucceed,ClientInfo clientInfo);
	}
	
	private static class CheckUpdateTask{
		private TerminableThread mTerminableThread;
		private ArrayList<WeakReference<CheckUpdateCallback>> callbacks = new ArrayList<WeakReference<CheckUpdateCallback>>();
		private void onPreLoad(){
			for (WeakReference<CheckUpdateCallback> callback : callbacks) {
				if(callback.get() != null){
					callback.get().onPreLoad();
				}
			}
		}
		
		private void onPostLoad(){
			for (WeakReference<CheckUpdateCallback> callback : callbacks) {
				if(callback.get() != null){
					callback.get().onPostLoad();
				}
			}
		}
		
		private void onResult(boolean isSucceed,ClientInfo clientInfo){
			for (WeakReference<CheckUpdateCallback> callback : callbacks) {
				if(callback.get() != null){
					callback.get().onResult(isSucceed,clientInfo);
				}
			}
		}
		
		private void addCallback(CheckUpdateCallback newCallback){
			for (WeakReference<CheckUpdateCallback> callback : callbacks) {
				if(callback.get() != null && callback.equals(newCallback)){
					return;
				}
			}
			callbacks.add(new WeakReference<CheckUpdateCallback>(newCallback));
		}
	}
}
