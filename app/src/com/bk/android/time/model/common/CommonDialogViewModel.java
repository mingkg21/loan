package com.bk.android.time.model.common;

import gueei.binding.IObservable;
import gueei.binding.Observer;
import gueei.binding.observables.BooleanObservable;
import gueei.binding.observables.IntegerObservable;
import gueei.binding.observables.ObjectObservable;
import gueei.binding.observables.StringObservable;

import java.util.Collection;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import com.bk.android.binding.command.OnClickCommand;
import com.bk.android.time.model.BaseDialogViewModel;

public class CommonDialogViewModel extends BaseDialogViewModel {
	public final IntegerObservable bBtnLayoutVisibility = new IntegerObservable(View.VISIBLE);
	public final IntegerObservable bLeftBtnVisibility = new IntegerObservable(View.VISIBLE);
	public final IntegerObservable bRightBtnVisibility = new IntegerObservable(View.VISIBLE);
	public final BooleanObservable bLeftBtnEnabled = new BooleanObservable(true);
	public final BooleanObservable bRightBtnEnabled = new BooleanObservable(true);
	public final BooleanObservable bIsCheck = new BooleanObservable();
	public final BooleanObservable bIsCheckVisibility = new BooleanObservable(false);
	public final IntegerObservable bTitleVisibility = new IntegerObservable(View.VISIBLE);
	public final IntegerObservable bStrContentVisibility = new IntegerObservable(View.VISIBLE);
	public final IntegerObservable bContentViewRes = new IntegerObservable(0);
	public final StringObservable bLeftBtnStr = new StringObservable();
	public final StringObservable bRightBtnStr = new StringObservable();
	public final StringObservable bContentStr = new StringObservable();
	public final StringObservable bTitleStr = new StringObservable();
	public final ObjectObservable bContentViewModel = new ObjectObservable();
	
	public final OnClickCommand bLeftBtnClickCommand = new OnClickCommand() {
		@Override
		public void onClick(View v) {
			if(mLeftBtnClickCallBack != null){
				mLeftBtnClickCallBack.onClick(v,CommonDialogViewModel.this);
			}
		}
	};
	
	public final OnClickCommand bRightBtnClickCommand = new OnClickCommand() {
		@Override
		public void onClick(View v) {
			if(mRightBtnClickCallBack != null){
				mRightBtnClickCallBack.onClick(v,CommonDialogViewModel.this);
			}
		}
	};
	
	private OnBtnClickCallBack mLeftBtnClickCallBack;
	private OnBtnClickCallBack mRightBtnClickCallBack;
	
	public CommonDialogViewModel(Context context) {
		super(context);
		onInit();
		bTitleVisibility.set(View.GONE);
		bLeftBtnVisibility.set(View.GONE);
		bRightBtnVisibility.set(View.GONE);
		bBtnLayoutVisibility.set(View.GONE);
		bStrContentVisibility.set(View.GONE);
	}
	
	public void setContentViewRes(int contentRes){
		bContentViewRes.set(contentRes);
	}
	
	public void setContentViewViewModel(Object contentViewModel){
		bContentViewModel.set(contentViewModel);
	}
	
	public void setTitle(String title){
		bTitleStr.set(title);
		if(TextUtils.isEmpty(title)){
			bTitleVisibility.set(View.GONE);
		}else{
			bTitleVisibility.set(View.VISIBLE);
		}
	}
	
	public void setContentStr(String contentStr){
		bContentStr.set(contentStr);
		if(TextUtils.isEmpty(contentStr)){
			bStrContentVisibility.set(View.GONE);
		}else{
			bStrContentVisibility.set(View.VISIBLE);
		}
	}
	
	public void setLeftBtnEnabled(boolean enabled){
		bLeftBtnEnabled.set(enabled);
	}
	
	public void setRightBtnEnabled(boolean enabled){
		bRightBtnEnabled.set(enabled);
	}
	
	public void setLeftBtnClickCallBack(OnBtnClickCallBack leftBtnClickCallBack) {
		this.mLeftBtnClickCallBack = leftBtnClickCallBack;
		setBtnClickCallBack(mLeftBtnClickCallBack, mRightBtnClickCallBack);
	}
	
	public void setRightBtnClickCallBack(OnBtnClickCallBack rightBtnClickCallBack) {
		this.mRightBtnClickCallBack = rightBtnClickCallBack;
		setBtnClickCallBack(mLeftBtnClickCallBack, mRightBtnClickCallBack);
	}
	
	public void setBtn(String letfBtnStr,String rightBtnStr,OnBtnClickCallBack leftBtnClick,OnBtnClickCallBack rightBtnClick){
		setBtnClickCallBack(leftBtnClick, rightBtnClick);
		bLeftBtnStr.set(letfBtnStr);
		bRightBtnStr.set(rightBtnStr);
	}
	
	private void setBtnClickCallBack(OnBtnClickCallBack leftBtnClick,OnBtnClickCallBack rightBtnClick){
		if(leftBtnClick == null){
			bLeftBtnVisibility.set(View.GONE);
		}else{
			bLeftBtnVisibility.set(View.VISIBLE);
			bBtnLayoutVisibility.set(View.VISIBLE);
		}
		if(rightBtnClick == null){
			bRightBtnVisibility.set(View.GONE);
			if(leftBtnClick == null){
				bBtnLayoutVisibility.set(View.GONE);
			}
		}else{
			bRightBtnVisibility.set(View.VISIBLE);
			bBtnLayoutVisibility.set(View.VISIBLE);
		}
		mLeftBtnClickCallBack = leftBtnClick;
		mRightBtnClickCallBack = rightBtnClick;
	}
	
	private void onInit(){
		bContentViewRes.subscribe(new Observer() {
			@Override
			public void onPropertyChanged(IObservable<?> prop,
					Collection<Object> initiators) {
				Integer integer = ((IntegerObservable)prop).get();
				if(integer == null || integer == 0){
					bStrContentVisibility.set(View.VISIBLE);
				}else{
					bStrContentVisibility.set(View.GONE);
				}
			}
		});
	}

	public OnBtnClickCallBack getLeftBtnClickCallBack() {
		return mLeftBtnClickCallBack;
	}

	public OnBtnClickCallBack getRightBtnClickCallBack() {
		return mRightBtnClickCallBack;
	}
}
