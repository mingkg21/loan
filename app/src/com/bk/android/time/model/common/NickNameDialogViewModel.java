package com.bk.android.time.model.common;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;

import com.bk.android.binding.command.OnClickCommand;
import com.bk.android.data.DataResult;
import com.bk.android.time.app.AppBroadcast;
import com.bk.android.time.data.UserData;
import com.bk.android.time.entity.BaseEntity;
import com.bk.android.time.model.BaseDataModel;
import com.bk.android.time.model.BaseDataModel.ILoadCallBack;
import com.bk.android.time.model.BaseDialogViewModel;
import com.bk.android.time.model.lightweight.UserInfoModel;
import com.bk.android.time.util.ToastUtil;

import gueei.binding.observables.StringObservable;

/**
 * @author mingkg21
 * @email mingkg21@gmail.com
 * @date 2016年6月4日
 *
 */
public class NickNameDialogViewModel extends BaseDialogViewModel implements ILoadCallBack {

	public final OnClickCommand bConfirmClickCommand = new OnClickCommand() {
		@Override
		public void onClick(View v) {
			String nickname = bNickname.get();
			if (TextUtils.isEmpty(nickname)) {
				ToastUtil.showToast(getContext(), "请输入昵称！");
				return;
			}
			mPhoneModel.modifyNickname(nickname);
		}
	};

	public final OnClickCommand bCancelClickCommand = new OnClickCommand() {
		@Override
		public void onClick(View v) {
			finish();
		}
	};

	public final StringObservable bNickname = new StringObservable();
	private UserInfoModel mPhoneModel;
	private BaseDialogViewModel mLoadDialogViewModel;
	private int mStatus;
	private int mLimitTime;

	public NickNameDialogViewModel(Context context) {
		super(context);
		mPhoneModel = new UserInfoModel();
		mPhoneModel.addCallBack(this);
	}

	@Override
	public boolean onSuccess(String key, Object data, DataResult<?> dataSource) {
		if (mPhoneModel.isModifyNicknameKey(key)) {
			ToastUtil.showToast(getContext(), "昵称修改成功！");
			UserData.setName(bNickname.get());
			getContext().sendBroadcast(new Intent(AppBroadcast.ACTION_USER_LOGIN_STATE_CHANGE));
			finish();
			return true;
		}
		return false;
	}
	
	@Override
	public boolean onServerFail(Runnable retryTask, String key, Object data) {
		if (mPhoneModel.isModifyNicknameKey(key)) {
			BaseEntity<?> baseEntity = (BaseEntity<?>) data;
			ToastUtil.showToast(getContext(), baseEntity.getErrMsg());
			return true;
		}
		return false;
	}
	
	@Override
	public boolean onFail(Runnable retryTask, String key, int state) {
		return false;
	}
	
	@Override
	public boolean onRequestProgressUpdate(String key, int progress,
			int maxProgress) {
		return false;
	}
	
	@Override
	public boolean onPreLoad(String key, int state) {
		mLoadDialogViewModel = bindDialogViewModel(WAITING_DIALOG, null);
		mLoadDialogViewModel.show();
		return false;
	}
	
	@Override
	public boolean onPreBackgrounLoad(String key, int state) {
		return false;
	}
	
	@Override
	public boolean onPostLoad(String key, int state) {
		if(mLoadDialogViewModel != null) {
			mLoadDialogViewModel.finish();
		}
		return false;
	}
	
	@Override
	public boolean onDataChange(String groupKey, BaseDataModel dataModel) {
		return false;
	}
	
	@Override
	public boolean isFinished() {
		return false;
	}
	
}
