package com.bk.android.time.model.common;

import gueei.binding.observables.StringObservable;
import android.content.Context;

import com.bk.android.assistant.R;
import com.bk.android.time.util.ResUtil;

public class WaitingDialogViewModel extends CommonDialogViewModel {
	public final StringObservable mWaitingTip = new StringObservable();
	
	public WaitingDialogViewModel(Context context) {
		super(context);
		setContentViewRes(R.layout.com_load_vertical_lay);
		setContentViewViewModel(this);
		setCancelable(false);
		setCanceledOnTouchOutside(false);
	}
	
	public void updateProgress(int progress, int maxProgress, int msgFormat) {
		int per = (int) (progress * 1f / maxProgress * 100);
		String str = per + "%";
		if(msgFormat > 0){
			mWaitingTip.set(getString(msgFormat, str));
		}else{
			mWaitingTip.set(getString(ResUtil.STR_TIP_LOADING) + str);
		}
	}
	
	public void setTip(String tip){
		mWaitingTip.set(tip);
	}

	@Override
	public void show() {
		mWaitingTip.set(getString(ResUtil.STR_TIP_LOADING));
		super.show();
	}
}
