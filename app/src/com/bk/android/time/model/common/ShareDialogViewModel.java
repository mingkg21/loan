package com.bk.android.time.model.common;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;

import com.bk.android.binding.command.OnClickCommand;
import com.bk.android.time.model.BaseDialogViewModel;
import com.bk.android.time.thridparty.ShareEntity;

import gueei.binding.observables.BooleanObservable;
import gueei.binding.observables.StringObservable;

public class ShareDialogViewModel extends BaseDialogViewModel {
	public final StringObservable bTitle = new StringObservable();
	public final BooleanObservable bIsShowQZone = new BooleanObservable(true);

	public final OnClickCommand bWXClickCommand = new OnClickCommand() {
		@Override
		public void onClick(View v) {
			mShareModel.shareWX();
			finish();
			if(mOnClickListener != null) {
				mOnClickListener.onClick(null);
			}
		}
	};
	
	public final OnClickCommand bFriendnClickCommand = new OnClickCommand() {
		@Override
		public void onClick(View v) {
			mShareModel.shareFriend();
			finish();
			if(mOnClickListener != null) {
				mOnClickListener.onClick(null);
			}
		}
	};
	
	public final OnClickCommand bQQClickCommand = new OnClickCommand() {
		@Override
		public void onClick(View v) {
			mShareModel.shareQQ();
			finish();
			if(mOnClickListener != null) {
				mOnClickListener.onClick(null);
			}
		}
	};
	
	public final OnClickCommand bWeiboClickCommand = new OnClickCommand() {
		@Override
		public void onClick(View v) {
			mShareModel.shareWeibo();
			finish();
			if(mOnClickListener != null) {
				mOnClickListener.onClick(null);
			}
		}
	};
	
	public final OnClickCommand bQZoneClickCommand = new OnClickCommand() {
		@Override
		public void onClick(View v) {
			mShareModel.shareQZone();
			finish();
			if(mOnClickListener != null) {
				mOnClickListener.onClick(null);
			}
		}
	};
	
	public final OnClickCommand bCancelClickCommand = new OnClickCommand() {
		@Override
		public void onClick(View v) {
			if(mOnClickListener != null) {
				mOnClickListener.onClick(null);
			}
			finish();
		}
	};

	private OnClickListener mOnClickListener;
	private ShareModel mShareModel;
	
	public ShareDialogViewModel(Context context, ShareEntity shareEntity) {
		this(context, null, shareEntity, null);
	}

	public ShareDialogViewModel(Context context, String title, ShareEntity shareEntity) {
		this(context, title, shareEntity, null);
	}
	
	public ShareDialogViewModel(Context context, String title, ShareEntity shareEntity, OnClickListener listener) {
		super(context);
		bTitle.set(title);
		mShareModel = new ShareModel(context,shareEntity);
		mOnClickListener = listener;
		bIsShowQZone.set(mShareModel.isShowQZone());
	}
}
