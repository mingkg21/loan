package com.bk.android.time.model.common;

import gueei.binding.observables.BooleanObservable;
import gueei.binding.observables.IntegerObservable;
import android.content.Context;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;

import com.bk.android.binding.command.OnClickCommand;
import com.bk.android.binding.command.OnScrollCommand;
import com.bk.android.binding.command.OnScrollStateChangedCommand;
import com.bk.android.data.DataResult;
import com.bk.android.data.ServiceProxy;
import com.bk.android.time.model.BaseNetDataViewModel;
import com.bk.android.time.ui.INetLoadView;
import com.bk.android.time.ui.widget.binding.command.OnRefreshCommand;
import com.bk.android.time.util.ResUtil;
import com.bk.android.time.util.ToastUtil;
import com.bk.android.ui.widget.pulltorefresh.PullToRefreshBase;
/**
 * 
 * @author linyiwei
 *
 */
public abstract class PagingLoadViewModel extends BaseNetDataViewModel implements OnScrollListener{
	public final BooleanObservable bHeadLoadingVisibility = new BooleanObservable(false);
	public final BooleanObservable bFootLoadingVisibility = new BooleanObservable(false);
	public final HeadFootViewModel bFootViewModel = new HeadFootViewModel();
	public final OnScrollStateChangedCommand bOnScrollStateChangedCommand = new OnScrollStateChangedCommand() {
		@Override
		public void onScrollStateChanged(AbsListView view, int scrollState) {
			PagingLoadViewModel.this.onScrollStateChanged(view, scrollState);
		}
	};
	
	public final OnScrollCommand bOnScrollCommand = new OnScrollCommand() {
		@Override
		public void onScroll(AbsListView view, int firstVisibleItem,int visibleItemCount, int totalItemCount) {
			PagingLoadViewModel.this.onScroll(view, firstVisibleItem, visibleItemCount, totalItemCount);
		}
	};
	
	public final BooleanObservable bRefreshComplete = new BooleanObservable(true);
	public final BooleanObservable bRefreshEnabled = new BooleanObservable(true);
	public final OnRefreshCommand bOnRefresh = new OnRefreshCommand() {
		@Override
		public void onRefresh(PullToRefreshBase<?> refreshView) {
			bRefreshComplete.set(false);
			if(pullToRefresh()){
				bRefreshComplete.set(false);
				onPullToRefresh();
			}else{
				bRefreshComplete.set(true);
			}
		}
	};
	
	//跳转到顶部
	public final OnClickCommand bGotoTopCommand = new OnClickCommand() {
		@Override
		public void onClick(View v) {
			gotoTop();
		}
	};
	
	public final IntegerObservable bGotoTop = new IntegerObservable();
	public final BooleanObservable bGotoTopVisibility = new BooleanObservable(false);
	
	
	public PagingLoadViewModel(Context context, INetLoadView loadView) {
		super(context, loadView);
	}
	
	public void gotoTop(){
		bGotoTop.set(0);
		bGotoTopVisibility.set(false);
	}
	
	protected void resetViewModel(){
		bRefreshComplete.set(true);
		hideLoadView();
		hideRetryView();
		bHeadLoadingVisibility.set(false);
		bFootLoadingVisibility.set(false);
	}
	
	protected boolean pullToRefresh(){
		return getPagingLoadModel().forceLoadPage();
	}
	
	protected void onPullToRefresh(){
	}
	
	@Override
	public boolean onServerFail(Runnable retryTask, String key, Object data) {
		if(getPagingLoadModel().isPagingRequest(key)){
			if(getPagingLoadModel().isClearData()){
				if(needRetryTask(key)){
					showRetryView(retryTask);
				}else{
					ToastUtil.showToast(getContext(), ResUtil.STR_TIP_ERR_SERVER);
				}
			}
			bRefreshComplete.set(true);
			return false;
		}
		return super.onServerFail(retryTask, key, data);
	}
	
	@Override
	public boolean onFail(Runnable retryTask, String key, int state) {
		if(state != ServiceProxy.REQUEST_STATE_FAILURE_NO_NET && state != ServiceProxy.REQUEST_STATE_FAILURE_NO_LOGIN){
			if(getPagingLoadModel().isPagingRequest(key)){
				if(getPagingLoadModel().isClearData()){
					if(needRetryTask(key)){
						showRetryView(retryTask);
					}else{
						ToastUtil.showToast(getContext(), ResUtil.STR_TIP_ERR_SERVER);
					}
				}
				bRefreshComplete.set(true);
				return false;
			}
		}
		return super.onFail(retryTask, key, state);
	}

	@Override
	public boolean onPreBackgrounLoad(String key, int state) {
		if(getPagingLoadModel().isPagingRequest(key)){
			if(!bRefreshComplete.get()){
				return false;
			}
			return false;
		}
		return super.onPreBackgrounLoad(key, state);
	}
	
	@Override
	public boolean onPreLoad(String key, int state) {
		if(getPagingLoadModel().isPagingRequest(key)){
			if(!bRefreshComplete.get()){
				return false;
			}
			if(getPagingLoadModel().isClearData() && isClearDataShowLoadFillView()){
				showLoadView();
			}else{
				if(getPagingLoadModel().isNext()){
					bFootLoadingVisibility.set(true);
				}else{
					bHeadLoadingVisibility.set(true);
				}
			}
			return false;
		}
		return super.onPreLoad(key, state);
	}
	
	@Override
	public boolean onPostLoad(String key, int state) {
		if(getPagingLoadModel().isPagingRequest(key)){
			if(!bRefreshComplete.get()){
				bRefreshComplete.set(true);
				return false;
			}
			hideLoadView();
			bFootLoadingVisibility.set(false);
			bHeadLoadingVisibility.set(false);
			return false;
		}
		return super.onPostLoad(key, state);
	}

	@Override
	public boolean onSuccess(String key, Object data, DataResult<?> dataSource) {
		return super.onSuccess(key, data, dataSource);
	}
	
	protected boolean onScrollFirstItem(){
		return getPagingLoadModel().loadPrevPage();
	}
	
	protected boolean onScrollLastItem(){
		return getPagingLoadModel().loadNextPage();
	}
	
	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		int firstVisiblePosition = view.getFirstVisiblePosition();
		//是否显示跳转到顶部的按钮
		if(firstVisiblePosition > 0) {
			bGotoTopVisibility.set(true);
		} else {
			bGotoTopVisibility.set(false);
		}
		if(view.getCount() <= 0){
			return;
		}
		if(firstVisiblePosition == 0){
			if(scrollState != OnScrollListener.SCROLL_STATE_TOUCH_SCROLL || view.getChildAt(0).getTop() == 0){
				return;
			}
		}
		if(view.getLastVisiblePosition() >= view.getCount() - 2){//向下滚动
			if(onScrollLastItem()){
				return;
			}
			return;
		}
		if(firstVisiblePosition <= 1){//向上滚动
			if(onScrollFirstItem()){
				return;
			}
		}
	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {}
	
	protected abstract PagingLoadModel<?,?> getPagingLoadModel();

	@Override
	protected boolean needRetryTask(String key) {
		return getPagingLoadModel().isNeedLoadData();
	}
	
	@Override
	protected boolean canClearLoadAndRetryView() {
		return !getPagingLoadModel().isNeedLoadData();
	}
	
	protected boolean isClearDataShowLoadFillView(){
		return true;
	};

	public class HeadFootViewModel{
		public final BooleanObservable bHeadLoadingVisibility = PagingLoadViewModel.this.bHeadLoadingVisibility;
		public final BooleanObservable bFootLoadingVisibility = PagingLoadViewModel.this.bFootLoadingVisibility;
		public final BooleanObservable bIsNotNext = new BooleanObservable(false);
		public final OnClickCommand bLoadClickCommand = new OnClickCommand() {
			@Override
			public void onClick(View v) {
				getPagingLoadModel().loadNextPage(true);
			}
		};
	}
}
