package com.bk.android.time.model.common;

import gueei.binding.observables.IntegerObservable;

import java.util.Calendar;
import java.util.Date;

import android.text.TextUtils;

public class DatePickerViewModel {
	public final IntegerObservable bYear = new IntegerObservable();
	public final IntegerObservable bMonth = new IntegerObservable();
	public final IntegerObservable bDay = new IntegerObservable();
	
	
	public void setTime(String time) {
		if(!TextUtils.isEmpty(time)){
			String[] date = time.split("-");
			if(date.length == 3){
				bYear.set(Integer.valueOf(date[0]));
				bMonth.set(Integer.valueOf(date[1]) - 1);
				bDay.set(Integer.valueOf(date[2]));
			}
		}
		checkInit();
	}
	
	public void setTime(long oldTime) {
		if(oldTime <= 0){
			oldTime = System.currentTimeMillis();
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date(oldTime));
		bYear.set(calendar.get(Calendar.YEAR));
		bMonth.set(calendar.get(Calendar.MONTH));
		bDay.set(calendar.get(Calendar.DAY_OF_MONTH));
		checkInit();
	}
	
	public long getTime(){
		checkInit();
		Calendar calendar = Calendar.getInstance();
		calendar.set(bYear.get(), bMonth.get(), bDay.get());
		return calendar.getTimeInMillis();
	}

	public int getYear() {
		checkInit();
		return bYear.get();
	}

	public int getMonth() {
		checkInit();
		return bMonth.get();
	}

	public int getDay() {
		checkInit();
		return bDay.get();
	}
	
	private void checkInit(){
		if(bYear.get() == null || bMonth.get() == null || bDay.get() == null){
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(new Date());
			bYear.set(calendar.get(Calendar.YEAR));
			bMonth.set(calendar.get(Calendar.MONTH));
			bDay.set(calendar.get(Calendar.DAY_OF_MONTH));
		}
	}
}
