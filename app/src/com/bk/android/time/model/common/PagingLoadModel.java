package com.bk.android.time.model.common;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;

import com.bk.android.data.BaseDataRequest;
import com.bk.android.data.DataResult;
import com.bk.android.data.ServiceProxy.RequestDataResult;
import com.bk.android.time.model.BaseDataModel;

public abstract class PagingLoadModel<Result extends Serializable,ResultItem> extends BaseDataModel{
	private Result mCurrentNextResult;
	private Result mCurrentPreResult;
	private BaseDataRequest mDateProvider;
	private ArrayList<ResultItem> mDatas;
	private boolean isNext;
	private boolean isClearData;
	private boolean hasDate;
	private boolean isLoadData ;
	private int mTempIndex;
	protected boolean isForce;

	public PagingLoadModel(){
		this(new ArrayList<ResultItem>());
	}
	
	public PagingLoadModel(ArrayList<ResultItem> dataContainer){
		mDatas = dataContainer;
		hasDate = true;
	}
	
	public boolean isNeedLoadData(){
		return hasDate && mDatas.isEmpty();
	}
	
	public boolean isLoadData(){
		return isLoadData;
	}
	
	public ArrayList<ResultItem> getData(){
		return mDatas;
	}
	
	public boolean loadNextPage(){
		return loadNextPage(false);
	}
	
	public boolean loadPrevPage(){
		return loadPrevPage(false);
	}
	
	public boolean loadNextPage(boolean isForce){
		return loadPage(false,true,isForce);
	}
	
	public boolean loadPrevPage(boolean isForce){
		return loadPage(false,false,isForce);
	}
	
	public final boolean loadPage() {
		return loadPage(true,true,false);
	}
	
	public final boolean forceLoadPage() {
		return loadPage(true,true,true);
	}
	
	private boolean loadPage(final boolean isClearData,final boolean isNext,final boolean isForce) {
		if(isClearData){
			hasDate = true  ;
		}
		boolean isSuccess = false;
		if(isLoadData || !hasDate){
			return isSuccess;
		}
		mTempIndex = -1;
		if(isClearData){
			if(hasData(null, true, isForce)){
				final BaseDataRequest dateProvider = getDataRequest(null, true, isClearData, isForce);
				isSuccess = startTask(dateProvider,isForce,new RequestDataResult() {
					@Override
					public void onResult(boolean isSuccess) {
						if(isSuccess){
							PagingLoadModel.this.isClearData = isClearData;
							PagingLoadModel.this.isNext = isNext;
							PagingLoadModel.this.isForce = isForce;
							mDateProvider = dateProvider;
							isLoadData = true;
						}
					}
				});
			}
		}else{
			Result currentResult = null;
			if(isNext){
				currentResult = mCurrentNextResult;
			}else{
				currentResult = mCurrentPreResult;
			}
			if(hasData(currentResult, isNext, isForce)){
				final BaseDataRequest dateProvider = getDataRequest(currentResult, isNext, isClearData, isForce);
				isSuccess = startTask(dateProvider,isForce,new RequestDataResult() {
					@Override
					public void onResult(boolean isSuccess) {
						if(isSuccess){
							PagingLoadModel.this.isClearData = isClearData;
							PagingLoadModel.this.isNext = isNext;
							PagingLoadModel.this.isForce = isForce;
							mDateProvider = dateProvider;
							isLoadData = true;
						}
					}
				});
			}
		}
		return isSuccess;
	}
	
	public Result getCurrentResult(){
		return getCurrentResult(true);
	}
	
	public Result getCurrentResult(boolean isNext){
		return isNext ? mCurrentNextResult : mCurrentPreResult;
	}
	
	@Override
	protected void dispatchPostLoad(String key, int state) {
		if(isPagingRequest(key)){
			isLoadData = false;
		}
		super.dispatchPostLoad(key, state);
	}
	
	@Override
	protected boolean checkData(String key, DataResult<Object> dataResult) {
		if(isPagingRequest(key)){
			if(super.checkData(key, dataResult)){
				try {
					Type type = this.getClass().getGenericSuperclass();
					 if(type instanceof ParameterizedType){
						ParameterizedType parameterizedType = (ParameterizedType) type;
						Class<?> clazz = (Class<?>) parameterizedType.getActualTypeArguments()[0];
						return clazz.isAssignableFrom(dataResult.getData().getClass());
					 }
				} catch (Exception e) {
					return true;
				}
			}
			return false;
		}
		return super.checkData(key, dataResult);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected void dispatchSuccess(String key,Object data, DataResult<?> dataSource){
		if(isPagingRequest(key)){
			Result currentResult = (Result) data;
			if(isClearData){//如果跳页，最开始和最后的页数都是一样的
				mDatas.clear();
				mCurrentPreResult = null;
				mCurrentNextResult = null;
			}
			if(isNext){
				mCurrentNextResult = currentResult;
				if(mCurrentPreResult == null){
					mCurrentPreResult = currentResult;
				}
			}else{
				mCurrentPreResult = currentResult;
				if(mCurrentNextResult == null){
					mCurrentNextResult = currentResult;
				}
			}
			ArrayList<ResultItem> result = getListDate(currentResult);
			if(result != null){
				if(isLoadData && isNext){
					mTempIndex = mDatas.size();
				}
				if(isNext){
					if(isLoadData || mTempIndex == -1){
						mDatas.addAll(result);
					}else{
						if(mTempIndex < mDatas.size()){
							mDatas.subList(mTempIndex, mDatas.size()).clear();
						}
						mDatas.addAll(mTempIndex,result);
					}
				}else{
					if(mTempIndex == -1){
						if(isLoadData){
							mTempIndex = result.size();
						}
					}else{
						mDatas.subList(0,mTempIndex).clear();
					}
					mDatas.addAll(0, result);
				}
			}else{
				hasDate = false;
			}
		}
		super.dispatchSuccess(key, data, dataSource);
	}

	protected abstract ArrayList<ResultItem> getListDate(Result result);

	protected abstract BaseDataRequest getDataRequest(Result result,boolean isNext, boolean isClearData, boolean isForce);

	protected abstract boolean hasData(Result result,boolean isNext, boolean isForce);
	
	protected String getRequestKey(){
		return mDateProvider != null ? mDateProvider.getDataKey() : "";
	}
	
	protected String getRequestGroupKey(){
		return mDateProvider != null ? mDateProvider.getDataGroupKey() : "";
	}

	protected BaseDataRequest getDateProvider(){
		return mDateProvider;
	}
	
	public boolean isPagingRequest(String key){
		return getRequestKey().equals(key);
	}
	
	public boolean isPagingGroupRequest(String key){
		return getRequestGroupKey().equals(key);
	}
	
	public boolean isForce(){
		return isForce;
	}
	
	public boolean isClearData(){
		return isClearData;
	}
	
	public boolean isNext(){
		return isNext;
	}
	
	public boolean hasData(boolean isNext){
		Result result = null;
		if(isNext){
			result = mCurrentNextResult;
		}else{
			result = mCurrentPreResult;
		}
		return hasData(result,isNext,false);
	}
}