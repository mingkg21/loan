package com.bk.android.time.model.common;

import android.app.Activity;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.net.Uri;

import com.bk.android.app.BaseApp;
import com.bk.android.assistant.R;
import com.bk.android.time.model.BaseDataModel;
import com.bk.android.time.thridparty.QQShareManage;
import com.bk.android.time.thridparty.ShareEntity;
import com.bk.android.time.thridparty.WeiboShareManage;
import com.bk.android.time.util.ToastUtil;
import com.bk.android.time.weixin.WXAPIManager;
import com.bk.android.time.widget.ImageLoader;
import com.bk.android.util.BitmapUtil;
import com.bk.android.util.FileUtil;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.UiError;

import java.io.ByteArrayOutputStream;
import java.util.List;

public class ShareModel extends BaseDataModel {
	private WXAPIManager mWxapiManager;
	private ShareEntity mShareEntity;
	private ActivityInfo mQZoneActivityInfo;
	private Context mContext;
	
	public ShareModel(Context context,ShareEntity shareEntity) {
		mContext = context;
		mWxapiManager = WXAPIManager.getInstance(context);
		setShareEntity(shareEntity);
	}
	
	@Override
	public Context getContext() {
		return mContext;
	}
	
	public void setShareEntity(ShareEntity shareEntity){
		mShareEntity = shareEntity;
		if(hasShareEntity() && mShareEntity.type == ShareEntity.TYPE_IMG){
			Intent intent = new Intent(Intent.ACTION_SEND);
			intent.addCategory(Intent.CATEGORY_DEFAULT);
			intent.setPackage("com.qzone");
			intent.setType("image/png");
			PackageManager pm = getContext().getPackageManager();
			List<ResolveInfo> resolveInfos = pm.queryIntentActivities(intent,PackageManager.COMPONENT_ENABLED_STATE_DEFAULT);
			if (resolveInfos != null) {
				for (ResolveInfo resolveInfo : resolveInfos) {
					if("com.qzone".equals(resolveInfo.activityInfo.packageName)){
						mQZoneActivityInfo = resolveInfo.activityInfo;
						break;
					}
				}
			}
		}
	}
	
	public boolean hasShareEntity(){
		return mShareEntity != null;
	}
	
	public boolean isShowQZone() {
		return mShareEntity.type != ShareEntity.TYPE_IMG || mQZoneActivityInfo != null;
	}
	
	public boolean shareWX(){
		if(hasShareEntity()) {
			if(mShareEntity.type == ShareEntity.TYPE_IMG){
				handleResImg(getContext(), mShareEntity);
				String imgUrl = null;
				if(mShareEntity.imgUrls != null && mShareEntity.imgUrls.size() > 0) {
					imgUrl = mShareEntity.imgUrls.get(0);
				}
				if(!FileUtil.isFileExists(imgUrl)){
					ToastUtil.showToast(getContext(), getString(R.string.photo_share_fault));
					return false;
				}
				mWxapiManager.setBundle(mShareEntity.type, mShareEntity.id);
				mWxapiManager.sendImage(mShareEntity.title, mShareEntity.summary, imgUrl, true);
			}else{
				byte[] bitmapByte = bitmapToByte(getImage(getContext(), mShareEntity));
				mWxapiManager.setBundle(mShareEntity.type, mShareEntity.id);
				mWxapiManager.shareApp(mShareEntity.title, mShareEntity.summary, bitmapByte, mShareEntity.webUrl, true);
			}
			return true;
		}
		return false;
	}
	
	public boolean shareFriend(){
		if(hasShareEntity()) {
			if(mShareEntity.type == ShareEntity.TYPE_IMG){
				handleResImg(getContext(), mShareEntity);
				String imgUrl = null;
				if(mShareEntity.imgUrls != null && mShareEntity.imgUrls.size() > 0) {
					imgUrl = mShareEntity.imgUrls.get(0);
				}
				if(!FileUtil.isFileExists(imgUrl)){
					ToastUtil.showToast(getContext(), getString(R.string.photo_share_fault));
					return false;
				}
				mWxapiManager.setBundle(mShareEntity.type, mShareEntity.id);
				mWxapiManager.sendImage(mShareEntity.title, mShareEntity.summary, imgUrl, false);
			}else{
				byte[] bitmapByte = bitmapToByte(getImage(getContext(), mShareEntity));
				mWxapiManager.setBundle(mShareEntity.type, mShareEntity.id);
				mWxapiManager.shareApp(mShareEntity.summary, mShareEntity.summary, bitmapByte, mShareEntity.webUrl, false);
			}
			return true;
		}
		return false;
	}
	
	public boolean shareQQ(){
		if(hasShareEntity()) {
			handleResImg(getContext(), mShareEntity);
			String imgUrl = null;
			if(mShareEntity.imgUrls != null && mShareEntity.imgUrls.size() > 0) {
				imgUrl = mShareEntity.imgUrls.get(0);
			}
			if(mShareEntity.type == ShareEntity.TYPE_IMG){
				if(!FileUtil.isFileExists(imgUrl)){
					ToastUtil.showToast(getContext(), getString(R.string.photo_share_fault));
					return false;
				}
				QQShareManage.getInstance(getContext()).shareToQQImg((Activity)getContext(), imgUrl, iUiListener);
			}else{
				QQShareManage.getInstance(getContext()).shareToQQ((Activity)getContext(), mShareEntity.webUrl, mShareEntity.title, imgUrl, mShareEntity.summary, iUiListener);
			}
			return true;
		}
		return false;
	}
	
	public boolean shareWeibo(){
		if(hasShareEntity()) {
			handleResImg(getContext(), mShareEntity);
			String imgUrl = null;
			if(mShareEntity.imgUrls != null && mShareEntity.imgUrls.size() > 0) {
				imgUrl = mShareEntity.imgUrls.get(0);
			}
			if(mShareEntity.type == ShareEntity.TYPE_IMG){
				if(!FileUtil.isFileExists(imgUrl)){
					ToastUtil.showToast(getContext(), getString(R.string.photo_share_fault));
					return false;
				}
			}
			WeiboShareManage.getInstance(getContext()).sendMultiMessage(mShareEntity.title, mShareEntity.summary, mShareEntity.webUrl, imgUrl);
			return true;
		}
		return false;
	}
	
	public boolean shareQZone(){
		if(hasShareEntity()) {
			handleResImg(getContext(), mShareEntity);
			if(mShareEntity.type == ShareEntity.TYPE_IMG){
				if(mQZoneActivityInfo == null){
					return false;
				}
				String imgUrl = null;
				if(mShareEntity.imgUrls != null && mShareEntity.imgUrls.size() > 0) {
					imgUrl = mShareEntity.imgUrls.get(0);
				}
				if(!FileUtil.isFileExists(imgUrl)){
					ToastUtil.showToast(getContext(), getString(R.string.photo_share_fault));
					return false;
				}
				Intent intent = new Intent(Intent.ACTION_SEND);
				intent.setComponent(new ComponentName(mQZoneActivityInfo.packageName,mQZoneActivityInfo.name));
				intent.putExtra(Intent.EXTRA_STREAM,Uri.parse("file://" + imgUrl));
				intent.setType("image/png");
				getContext().startActivity(intent);
			}else{
				QQShareManage.getInstance(getContext()).shareToQZone((Activity)getContext(), mShareEntity.webUrl, mShareEntity.title, mShareEntity.imgUrls, mShareEntity.summary, iUiListener);
			}
			return true;
		}
		return false;
	}
	
	IUiListener iUiListener = new IUiListener() {
		
		@Override
		public void onError(UiError arg0) {
			ToastUtil.showToast(BaseApp.getInstance(), arg0.errorDetail);
		}
		
		@Override
		public void onComplete(Object arg0) {
			ToastUtil.showToast(BaseApp.getInstance(), "分享成功~");
		}
		
		@Override
		public void onCancel() {
			ToastUtil.showToast(BaseApp.getInstance(), "分享取消~");
		}
	};
	
	public static Bitmap getImage(Context context, ShareEntity shareEntity) {
		Bitmap bitmap = null;
		if(shareEntity.imgUrls != null && shareEntity.imgUrls.size() > 0) {
			bitmap = ImageLoader.getInstance().getLocalImage(shareEntity.imgUrls.get(0));
		} 
		if(bitmap == null) {
			bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_launcher);
		}
		return bitmap;
	}

	public static byte[] bitmapToByte(Bitmap bitmap){
		if(bitmap == null){
			return null;
		}
		bitmap = ThumbnailUtils.extractThumbnail(bitmap, 150, 150);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.JPEG, 70, baos);
		return baos.toByteArray();
	}
	
	public static void handleResImg(Context context, ShareEntity shareEntity){
		String imgUrl = null;
		if(shareEntity.imgUrls != null && shareEntity.imgUrls.size() > 0) {
			imgUrl = shareEntity.imgUrls.get(0);
		}else{
			return;
		}
		String filePath = imgUrl;
		Uri uri = Uri.parse(imgUrl);
		if(uri != null){
			String scheme = uri.getScheme();
			if("http".equals(scheme)){
				filePath = ImageLoader.getInstance().urlToPath(filePath);
				if(FileUtil.isFileExists(filePath)){
					shareEntity.imgUrls.set(0, filePath);
				}
			}else if(ContentResolver.SCHEME_FILE.equals(scheme)){
				filePath = filePath.substring(7);
				if(!FileUtil.isFileExists(filePath)){
					shareEntity.imgUrls.clear();
				}else{
					shareEntity.imgUrls.set(0, filePath);
				}
			}else if(ContentResolver.SCHEME_ANDROID_RESOURCE.equals(scheme)
            		|| ContentResolver.SCHEME_CONTENT.equals(scheme)){
				filePath = ImageLoader.getInstance().urlToPath(imgUrl);
				if(!FileUtil.isFileExists(filePath)){
					Bitmap bitmap = getImage(context,shareEntity);
					BitmapUtil.saveImage(filePath, bitmap);
				}
				if(!FileUtil.isFileExists(filePath)){
					shareEntity.imgUrls.clear();
				}else{
					shareEntity.imgUrls.set(0, filePath);
				}
			}
		}
	}
}
