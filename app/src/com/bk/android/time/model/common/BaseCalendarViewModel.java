package com.bk.android.time.model.common;

import gueei.binding.collections.ArrayListObservable;
import gueei.binding.observables.IntegerObservable;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import android.content.Context;
import android.view.View;
import android.widget.AdapterView;

import com.bk.android.binding.command.OnItemSelectedCommand;
import com.bk.android.time.model.BaseNetDataViewModel;
import com.bk.android.time.ui.INetLoadView;
import com.bk.android.time.ui.widget.CalendarGridView;
import com.bk.android.time.ui.widget.CalendarGridView.AbsDayItemData;
import com.bk.android.time.ui.widget.binding.command.OnCalendarItemClickCommand;
import com.bk.android.time.ui.widget.calendar.CalendarDataProvider;
import com.bk.android.time.ui.widget.calendar.MonthCellDescriptor;
import com.bk.android.time.ui.widget.calendar.MonthDescriptor;

public class BaseCalendarViewModel extends BaseNetDataViewModel {
	public final IntegerObservable bSelectedItem = new IntegerObservable(0);
	public final ArrayListObservable<ItemCardViewModel> bCardItems = new ArrayListObservable<ItemCardViewModel>(ItemCardViewModel.class){
		@Override
		public void onLoad(int position) {
			bCardItems.get(position).fillData(position);
			if(position > 0){
				bCardItems.get(position - 1).fillData(position - 1);
			}
			if(position < bCardItems.size() - 1){
				bCardItems.get(position + 1).fillData(position + 1);
			}
		}
	};
	
	public final OnItemSelectedCommand bOnItemSelectedCommand = new OnItemSelectedCommand() {
		@Override
		public void onItemSelected(AdapterView<?> parent, View view, int position,long id) {
			ItemCardViewModel item = bCardItems.get(position);
			onSelectedMonth(item,position);
		}
	};
	
	protected ItemCardViewModel mSelectedMonth;
	protected MonthCellDescriptor mToday;
	protected MonthCellDescriptor mSelectedDay;
	protected int mTodayMonthIndex;
	protected int mSelectedMonthIndex;
	protected int mSelectedDayMonthIndex;
	protected DayItemData mSelectedItemViewModel;
	protected CalendarDataProvider mCalendarDataProvider;
	private	SimpleDateFormat mFormat = new SimpleDateFormat("yyyy-MM");

	public BaseCalendarViewModel(Context context, INetLoadView loadView) {
		super(context, loadView);
		mCalendarDataProvider = new CalendarDataProvider();
		Calendar nextYear = Calendar.getInstance();
		if(isNeedShowFuture()){
		    nextYear.add(Calendar.YEAR, 10);
		}else{
		    nextYear.add(Calendar.YEAR, 0);
		}
	    Calendar lastYear = Calendar.getInstance();
	    lastYear.add(Calendar.YEAR, -10);
		mCalendarDataProvider.init(lastYear.getTime(), nextYear.getTime(),true);
	}

	protected boolean isNeedShowFuture(){
		return true;
	}
	
	@Override
	protected boolean canClearLoadAndRetryView() {
		return false;
	}

	@Override
	public void onStart() {
		fillCalendarData(mCalendarDataProvider.getMonthsData());
	}

	@Override
	public void onRelease() {
	}
	
	public void nextPreMonth() {
		bSelectedItem.set(mSelectedMonthIndex + 1);

	}

	public void gotoPreMonth() {
		bSelectedItem.set(mSelectedMonthIndex - 1);
	}
	
	public void gotoTodayMonth(){
		bSelectedItem.set(mTodayMonthIndex);
	}

	protected void fillCalendarData(List<MonthDescriptor> months){
		Calendar today = Calendar.getInstance();
		ArrayListObservable<ItemCardViewModel> tempCardItems = new ArrayListObservable<ItemCardViewModel>(ItemCardViewModel.class);
		int position = 0;
		boolean isBreak = false;
		for (MonthDescriptor monthDescriptor : months) {
			ItemCardViewModel item = new ItemCardViewModel();
			item.mMonth = monthDescriptor;
			item.mTag = mFormat.format(monthDescriptor.getDate());
			if(monthDescriptor.isCurrentMonth(today)){
				item.fillData(position);
				isBreak = !isNeedShowFuture();
			}
			tempCardItems.add(item);
			position++;
			if(isBreak){
				break;
			}
		}
		bCardItems.setAll(tempCardItems);
		bSelectedItem.set(mTodayMonthIndex);
	}
	
	protected void onSelectedMonth(ItemCardViewModel item, int position){
		mSelectedMonth = item;
		mSelectedMonthIndex = position;
	}
	
	protected void onDayItemDataItemClick(DayItemData item) {
	}
	
	protected boolean selectedDay(DayItemData item) {
		if(mSelectedItemViewModel != null){
			if(mSelectedItemViewModel == item){
				return false;
			}
			mSelectedItemViewModel.setSelected(false);
			setItemViewModelStateBg(mSelectedItemViewModel);
		}
		mSelectedItemViewModel = item;
		mSelectedItemViewModel.setSelected(true);
		mSelectedDayMonthIndex = mSelectedMonthIndex;
		mSelectedDay = item.getDateSource();
		setItemViewModelStateBg(mSelectedItemViewModel);
		return true;
	}
	
	protected void setItemViewModelStateBg(DayItemData itemViewModel) {
	}
	
	public class ItemCardViewModel{
		public String mTag;
		public MonthDescriptor mMonth;
		public List<List<MonthCellDescriptor>> mMonthData;
		public final IntegerObservable bSelectedItem = new IntegerObservable(0);
		public final ArrayListObservable<DayItemData> bItems = new ArrayListObservable<DayItemData>(DayItemData.class);
		public final OnCalendarItemClickCommand bOnItemClickCommand = new OnCalendarItemClickCommand() {
			@Override
			public void onCalendarItemClick(CalendarGridView view, int position) {
				DayItemData item = bItems.get(position);
				onDayItemDataItemClick(item);
				if(item.isCurrentMonth()){
					if(selectedDay(item)){
						bSelectedItem.set(position);
					}
				}
			}
		};
		
		private void fillData(int position){
			if(mMonthData == null){
				ArrayListObservable<DayItemData> temptems = new ArrayListObservable<DayItemData>(DayItemData.class);
				mMonthData = mCalendarDataProvider.getMonthCells(mMonth);
				int indexDay = 0;
				for (List<MonthCellDescriptor> list : mMonthData) {
					for (MonthCellDescriptor day : list) {
						DayItemData item = new DayItemData(day);
						item.setDateStr(day.getValue()+"");
						item.setCurrentMonth(day.isCurrentMonth());
						item.setToday(day.isToday());
						setItemViewModelStateBg(item);
						if(day.isCurrentMonth() && day.isToday()){
							mToday = day;
							mSelectedDay = day;
							mTodayMonthIndex = position;
							mSelectedMonth = this;
							mSelectedMonthIndex = position;
							selectedDay(item);
							bSelectedItem.set(indexDay);
						}
						temptems.add(item);
						indexDay++;
					}
				}
				bItems.setAll(temptems);
			}
		}
	}
	
	public class DayItemData extends AbsDayItemData<MonthCellDescriptor>{
		public DayItemData(MonthCellDescriptor data) {
			setDateSource(data);
		}
	}
}
