package com.bk.android.time.model.lightweight;

import android.content.Context;
import android.view.View;

import com.bk.android.binding.command.OnClickCommand;
import com.bk.android.data.DataResult;
import com.bk.android.time.model.BaseNetDataViewModel;
import com.bk.android.time.ui.INetLoadView;
import com.bk.android.time.ui.activiy.ActivityChannels;
import com.bk.android.time.util.ToastUtil;

import gueei.binding.observables.BooleanObservable;
import gueei.binding.observables.StringObservable;

/** 忘记密码的界面ViewModel
 * @author mingkg21
 * @date 2017年4月3日
 */
public class ForgetPasswordViewModel extends BaseNetDataViewModel {

	public static final String TAG = "ForgetPasswordViewModel";

	public final StringObservable bUserNameText = new StringObservable();
	public final StringObservable bSMSCodeText = new StringObservable();

    public final StringObservable bNewPasswordText = new StringObservable();
    public final StringObservable bNewPasswordAgainText = new StringObservable();

	public final BooleanObservable bIsFirstStep = new BooleanObservable(true);
	public final BooleanObservable bIsBySNS = new BooleanObservable(true);
	public final BooleanObservable bIsLogin = new BooleanObservable(false);
	public final BooleanObservable bIsRegister = new BooleanObservable(false);

	/** 下一步
	 *
	 */
	public final OnClickCommand bNextStepClickCommand = new OnClickCommand() {
		@Override
		public void onClick(View v) {
            if (!UserInfoModel.verifyUserNameEmpty(bUserNameText.get()) &&
                    !UserInfoModel.verifySMSCodedEmpty(bSMSCodeText.get())
                    ) {
                bIsFirstStep.set(false);
            }
		}
	};

	/** 重置密码
	 *
	 */
	public final OnClickCommand bResetClickCommand = new OnClickCommand() {
		@Override
		public void onClick(View v) {
            if (UserInfoModel.verifyUserPassword(bNewPasswordText.get())) {
                if (bNewPasswordText.get().equals(bNewPasswordAgainText.get())) {
					mUserInfoModel.resetPassword(bUserNameText.get(), bNewPasswordText.get(), bSMSCodeText.get());
                } else {
                    ToastUtil.showToast(getContext(), "两次输入密码不一致！");
                }
            }
		}
	};

	private UserInfoModel mUserInfoModel;

	public ForgetPasswordViewModel(Context context, INetLoadView loadView) {
		super(context, loadView);
		mUserInfoModel = new UserInfoModel();
		mUserInfoModel.addCallBack(this);

	}

	@Override
	public boolean onPostLoad(String key, int state) {
		hideLoadDialog();
		return false;
	}

	@Override
	public boolean onPreLoad(String key, int state) {
		showLoadDialog();
		return false;
	}
	
	@Override
	protected boolean needRetryTask(String key) {
		return false;
	}

	@Override
	public boolean onServerFail(Runnable retryTask, String key, Object data) {
		return super.onServerFail(retryTask, key, data);
	}

	@Override
	public boolean onSuccess(String key, Object data, DataResult<?> dataSource) {
		if (mUserInfoModel.isResetPasswordKey(key)) {
			ToastUtil.showToast(getContext(), "重置密码成功，请重新登陆！");
			ActivityChannels.openLoginActivity(getContext());
			finish();
			return true;
		}
		return super.onSuccess(key, data, dataSource);
	}
	
	@Override
	public void onStart() {
	}

	@Override
	public void onRelease() {
	}
	
	@Override
	protected boolean canClearLoadAndRetryView() {
		return true;
	}

}
