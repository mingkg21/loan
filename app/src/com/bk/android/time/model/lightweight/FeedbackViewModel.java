package com.bk.android.time.model.lightweight;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.bk.android.assistant.R;
import com.bk.android.binding.command.OnClickCommand;
import com.bk.android.binding.command.OnTextChangedCommand;
import com.bk.android.data.DataResult;
import com.bk.android.time.model.common.PagingLoadModel;
import com.bk.android.time.model.common.PagingLoadViewModel;
import com.bk.android.time.ui.INetLoadView;
import com.bk.android.time.util.ToastUtil;
import com.bk.android.util.CommonUtil;

import gueei.binding.observables.BooleanObservable;
import gueei.binding.observables.StringObservable;

public class FeedbackViewModel extends PagingLoadViewModel {

	public final StringObservable bContent = new StringObservable();
	public final StringObservable bSurplusContentSize = new StringObservable();
	public final StringObservable bContact = new StringObservable();
	public final BooleanObservable bIsFeedbackEnabled = new BooleanObservable(false);
	
	public final OnTextChangedCommand bOnTextChangedCommand = new OnTextChangedCommand() {
		@Override
		public void onTextChanged(EditText editText, CharSequence s, int start,
				int before, int count) {
			bIsFeedbackEnabled.set(editText.length() > 0);
			bSurplusContentSize.set(getString(R.string.tip_surplus_content_size, 1000 - editText.length()));
		}
	};

	public final OnClickCommand bFeedbackClickCommand = new OnClickCommand() {
		@Override
		public void onClick(View v) {
			String contact = bContact.get();
			String content = bContent.get();

			String contactType = "";
			if (TextUtils.isEmpty(content)) {
				ToastUtil.showToast(getContext(), R.string.feedback_check_content_is_not_null);
				return;
			}
			content = content.trim();
			if (!TextUtils.isEmpty(contact)) {
				if (CommonUtil.isPhoneNumber(contact)) {
					contactType = "phone";
				} else if (CommonUtil.checkEmail(contact)) {
					contactType = "email";
				} else if (TextUtils.isDigitsOnly(contact)) {
					contactType = "qq";
				} else {
					ToastUtil.showToast(getContext(), R.string.feedback_check_contact_is_fail);
					return;
				}
			} else {
				contact = "";
			}

			mFeedbackModel.userFeedback("3", content, contactType, contact);
		}
	};
	
	private String mQQ;
	private String mNumber;
	private FeedbackModel mFeedbackModel;
	
	public FeedbackViewModel(Context context, INetLoadView loadView) {
		super(context, loadView);
		bSurplusContentSize.set(getString(R.string.tip_surplus_content_size, 1000));
		mFeedbackModel = new FeedbackModel();
		mFeedbackModel.addCallBack(this);
	}

	private void gotoAddFeedback(){
		bContent.set("");
	}
	
	@Override
	protected PagingLoadModel<?, ?> getPagingLoadModel() {
		return mFeedbackModel;
	}

	@Override
	public void onStart() {
	}

	@Override
	public void onRelease() {
	}

	@Override
	protected boolean needRetryTask(String key) {
		return false;
	}
	
	@Override
	public boolean onPreLoad(String key, int state) {
		showLoadDialog();
		return false;
	}
	
	@Override
	public boolean onPostLoad(String key, int state) {
		hideLoadDialog();
		return false;
	}
	
	@Override
	public boolean onSuccess(String key, Object data, DataResult<?> dataSource) {
		if(mFeedbackModel.isUserFeedbackKey(key)){
			bContent.set("");
			bContact.set("");
			ToastUtil.showToast(getContext(), R.string.feedback_success);
		}
		return super.onSuccess(key, data, dataSource);
	}
	
}
