package com.bk.android.time.model.lightweight;

import com.bk.android.data.BaseDataRequest;
import com.bk.android.time.data.request.net.NetEaseDetailRequest;
import com.bk.android.time.data.request.net.NetEaseRequest;
import com.bk.android.time.entity.NetEaseNews;
import com.bk.android.time.entity.NetEaseNewsListData;
import com.bk.android.time.model.common.PagingLoadModel;

import java.util.ArrayList;

public class NetEaseNewskListModel extends PagingLoadModel<NetEaseNewsListData, NetEaseNews> {

	private String isGetDetailKey;

	public NetEaseNewskListModel() {
	}

	@Override
	protected ArrayList<NetEaseNews> getListDate(NetEaseNewsListData result) {
		return result.getData();
	}

	@Override
	protected BaseDataRequest getDataRequest(NetEaseNewsListData result, boolean isNext, boolean isClearData, boolean isForce) {
		if(result == null){
			return new NetEaseRequest(0);
		}
		return new NetEaseRequest(result.getNextPage());
	}

	@Override
	protected boolean hasData(NetEaseNewsListData result, boolean isNext, boolean isForce) {
		if(result == null){
			return true;
		}
		return isNext && result.haveNext();
	}

	public void getDetail(String id) {
		NetEaseDetailRequest request = new NetEaseDetailRequest(id);
		isGetDetailKey = request.getDataKey();
		startTask(request);
	}

	public boolean isGetDetailkey(String key) {
		return key.equals(isGetDetailKey);
	}
}
