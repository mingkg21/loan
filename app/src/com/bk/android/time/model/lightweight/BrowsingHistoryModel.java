package com.bk.android.time.model.lightweight;

import com.bk.android.data.BaseDataRequest;
import com.bk.android.time.data.request.net.UserBrowseRequest;
import com.bk.android.time.entity.LoanInfo;
import com.bk.android.time.entity.LoanInfoListData;
import com.bk.android.time.model.common.PagingLoadModel;

import java.util.ArrayList;


public class BrowsingHistoryModel extends PagingLoadModel<LoanInfoListData, LoanInfo> {

	@Override
	protected ArrayList<LoanInfo> getListDate(LoanInfoListData result) {
		return result.getData();
	}

	@Override
	protected BaseDataRequest getDataRequest(LoanInfoListData result, boolean isNext, boolean isClearData, boolean isForce) {
		if(result == null){
			return new UserBrowseRequest(1);
		}
		return new 	UserBrowseRequest(result.getNextPage());
	}

	@Override
	protected boolean hasData(LoanInfoListData result, boolean isNext, boolean isForce) {
		if(result == null){
			return true;
		}
		return isNext && result.haveNext();
	}
}

