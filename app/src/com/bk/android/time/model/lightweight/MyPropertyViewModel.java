package com.bk.android.time.model.lightweight;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import com.bk.android.assistant.R;
import com.bk.android.binding.command.OnClickCommand;
import com.bk.android.data.DataResult;
import com.bk.android.time.data.UserData;
import com.bk.android.time.entity.UserInfo;
import com.bk.android.time.model.BaseDataModel;
import com.bk.android.time.model.BaseNetDataViewModel;
import com.bk.android.time.ui.INetLoadView;
import com.bk.android.time.ui.common.PopupListDialog;
import com.bk.android.time.util.ToastUtil;
import com.bk.android.time.util.UserInfoUtil;

import java.util.HashMap;

import gueei.binding.observables.StringObservable;

/**
 *
 */
public class MyPropertyViewModel extends BaseNetDataViewModel {

	public final StringObservable bCreditCard = new StringObservable();
	public final StringObservable bProperty = new StringObservable();
	public final StringObservable bCar = new StringObservable();
	public final StringObservable bLoan = new StringObservable();
	public final StringObservable bCredit = new StringObservable();

	public final OnClickCommand bCreditCardClickCommand = new OnClickCommand() {
		@Override
		public void onClick(View v) {
			PopupListDialog dialog = new PopupListDialog(getContext(), R.array.yes_no, new PopupListDialog.OnItemSelectedListener() {
				@Override
				public void onItemSelected(int position, String str) {
					bCreditCard.set(str);
				}
			});
			dialog.show();
		}
	};

	public final OnClickCommand bPropertyClickCommand = new OnClickCommand() {
		@Override
		public void onClick(View v) {
			PopupListDialog dialog = new PopupListDialog(getContext(), R.array.property, new PopupListDialog.OnItemSelectedListener() {
				@Override
				public void onItemSelected(int position, String str) {
					bProperty.set(str);
				}
			});
			dialog.show();
		}
	};

	public final OnClickCommand bCarClickCommand = new OnClickCommand() {
		@Override
		public void onClick(View v) {
			PopupListDialog dialog = new PopupListDialog(getContext(), R.array.car, new PopupListDialog.OnItemSelectedListener() {
				@Override
				public void onItemSelected(int position, String str) {
					bCar.set(str);
				}
			});
			dialog.show();
		}
	};

	public final OnClickCommand bLoanClickCommand = new OnClickCommand() {
		@Override
		public void onClick(View v) {
			PopupListDialog dialog = new PopupListDialog(getContext(), R.array.yes_no, new PopupListDialog.OnItemSelectedListener() {
				@Override
				public void onItemSelected(int position, String str) {
					bLoan.set(str);
				}
			});
			dialog.show();
		}
	};

	public final OnClickCommand bCreditClickCommand = new OnClickCommand() {
		@Override
		public void onClick(View v) {
			PopupListDialog dialog = new PopupListDialog(getContext(), R.array.credit, new PopupListDialog.OnItemSelectedListener() {
				@Override
				public void onItemSelected(int position, String str) {
					bCredit.set(str);
				}
			});
			dialog.show();
		}
	};

	public final OnClickCommand bSaveClickCommand = new OnClickCommand() {
		@Override
		public void onClick(View v) {
			if (TextUtils.isEmpty(bCreditCard.get())) {
				ToastUtil.showToast(getContext(), "请选择是否有信用卡！");
				return;
			}
			if (TextUtils.isEmpty(bProperty.get())) {
				ToastUtil.showToast(getContext(), "请选择名下房产信息！");
				return;
			}
			if (TextUtils.isEmpty(bCar.get())) {
				ToastUtil.showToast(getContext(), "请选择名下车产信息！");
				return;
			}
			if (TextUtils.isEmpty(bLoan.get())) {
				ToastUtil.showToast(getContext(), "请选择是否有成功贷款记录！");
				return;
			}
			if (TextUtils.isEmpty(bCredit.get())) {
				ToastUtil.showToast(getContext(), "请选择信用情况！");
				return;
			}

			HashMap<String, Object> parameterMap = new HashMap<>();
			parameterMap.put("hasCreditFlag", UserInfoUtil.yesOrNoStr(bCreditCard.get()));
			parameterMap.put("containHouseType", UserInfoUtil.containHouseTypeStr(bProperty.get()));
			parameterMap.put("containCarType", UserInfoUtil.containCarTypeStr(bCar.get()));
			parameterMap.put("successLoanFlag", UserInfoUtil.yesOrNoStr(bLoan.get()));
			parameterMap.put("creditStanding", UserInfoUtil.creditStandingStr(bCredit.get()));

			mUserInfoModel.saveUserInfo(parameterMap);
		}
	};

	private UserInfoModel mUserInfoModel;

	public MyPropertyViewModel(Context context, INetLoadView loadView) {
		super(context, loadView);
		mUserInfoModel = new UserInfoModel();
		mUserInfoModel.addCallBack(this);

	}

	@Override
	protected boolean canClearLoadAndRetryView() {
		return true;
	}
	
	@Override
	public boolean onPreLoad(String key, int state) {
		if (mUserInfoModel.isEditUserInfoKey(key)) {
			showLoadDialog();
			return false;
		}
		return false;
	}
	
	@Override
	public boolean onPreBackgrounLoad(String key, int state) {
		return false;
	}
	
	@Override
	public boolean onPostLoad(String key, int state) {
		if (mUserInfoModel.isEditUserInfoKey(key)) {
			hideLoadDialog();
			return false;
		}
		return false;
	}

	@Override
	public boolean onSuccess(String key, Object data, DataResult<?> dataSource) {
		if (mUserInfoModel.isEditUserInfoKey(key)) {
			ToastUtil.showToast(getContext(), "编辑房产信息成功！");

			UserData.mUserInfo.setHasCreditFlag(UserInfoUtil.yesOrNoStr(bCreditCard.get()));
			UserData.mUserInfo.setContainHouseType(UserInfoUtil.containHouseTypeStr(bProperty.get()));
			UserData.mUserInfo.setContainCarType(UserInfoUtil.containCarTypeStr(bCar.get()));
			UserData.mUserInfo.setSuccessLoanFlag(UserInfoUtil.yesOrNoStr(bLoan.get()));
			UserData.mUserInfo.setCreditStanding(UserInfoUtil.yesOrNoStr(bCredit.get()));
			return true;
		}
		return super.onSuccess(key, data, dataSource);
	}
	
	@Override
	public boolean onDataChange(String groupKey,BaseDataModel dataModel) {
		if(UserInfoModel.DATA_GROUP_KEY.equals(groupKey)){
			refresh();
		}
		return super.onDataChange(groupKey,dataModel);
	}

	@Override
	public void onUserLoginStateChange(boolean isLogin) {
		refresh();
	}
	
	private void setUserInfo(){
		UserInfo userInfo = UserData.mUserInfo;
		bCreditCard.set(UserInfoUtil.yesOrNo(userInfo.getHasCreditFlag()));
		bProperty.set(UserInfoUtil.containHouseType(userInfo.getContainHouseType()));
		bCar.set(UserInfoUtil.containCarType(userInfo.getContainCarType()));
		bLoan.set(UserInfoUtil.yesOrNo(userInfo.getSuccessLoanFlag()));
		bCredit.set(UserInfoUtil.creditStanding(userInfo.getCreditStanding()));
	}
	
	public void refresh(){
		if(UserData.getUserId() == UserData.DESIRE_USER_ID){
		}else{
			setUserInfo();
		}
	}

	@Override
	public void onStart() {
		refresh();
	}

	@Override
	public void onRelease() {
	}
	
}
