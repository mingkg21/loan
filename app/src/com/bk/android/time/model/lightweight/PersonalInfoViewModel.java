package com.bk.android.time.model.lightweight;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import com.bk.android.assistant.R;
import com.bk.android.binding.command.OnClickCommand;
import com.bk.android.data.DataResult;
import com.bk.android.time.data.UserData;
import com.bk.android.time.model.BaseDataModel;
import com.bk.android.time.model.BaseNetDataViewModel;
import com.bk.android.time.ui.INetLoadView;
import com.bk.android.time.ui.activiy.ActivityChannels;

import gueei.binding.observables.BooleanObservable;
import gueei.binding.observables.IntegerObservable;
import gueei.binding.observables.ObjectObservable;
import gueei.binding.observables.StringObservable;

public class PersonalInfoViewModel extends BaseNetDataViewModel {

	public final StringObservable bHeadUrl = new StringObservable();
	public final StringObservable bPhone = new StringObservable();
	public final ObjectObservable bName = new ObjectObservable();
	public final IntegerObservable bPoint = new IntegerObservable(0);

	public final BooleanObservable bIsLogin = new BooleanObservable(false);
	public final BooleanObservable bHadSignIn = new BooleanObservable(false);

	public final OnClickCommand bSignInClickCommand = new OnClickCommand() {
		@Override
		public void onClick(View v) {
			if (bHadSignIn.get()) {
				return;
			}
			ActivityChannels.openLoginActivity(getContext());
		}
	};

	public final OnClickCommand bSignUpClickCommand = new OnClickCommand() {
		@Override
		public void onClick(View v) {
			if (bHadSignIn.get()) {
				return;
			}
			ActivityChannels.openRegisterActivity(getContext());
		}
	};

	public final OnClickCommand bEditInfoClickCommand = new OnClickCommand() {
		@Override
		public void onClick(View v) {
			if(!UserData.isLogin()){
				ActivityChannels.openLoginActivity(getContext());
			}else{
				ActivityChannels.openMyInfoActivity(getContext());
			}
		}
	};

	public final OnClickCommand bSettingClickCommand = new OnClickCommand() {
		@Override
		public void onClick(View v) {
			ActivityChannels.openSettingActivity(getContext());
		}
	};

	public final OnClickCommand bMyApplyClickCommend = new OnClickCommand() {
		@Override
		public void onClick(View v) {
			checkLogin(new Runnable() {
				@Override
				public void run() {
					ActivityChannels.openMyApplyActivity(getContext());
				}
			});
		}
	};

	public final OnClickCommand bMyMsgClickCommend = new OnClickCommand() {
		@Override
		public void onClick(View v) {
			checkLogin(new Runnable() {
				@Override
				public void run() {
					ActivityChannels.openMyMsgActivity(getContext());
				}
			});
		}
	};

	public final OnClickCommand bBrowsingHistoryClickCommand = new OnClickCommand() {
		@Override
		public void onClick(View v) {

			checkLogin(new Runnable() {
				@Override
				public void run() {
					ActivityChannels.openBrowsingHistoryActivity(getContext());
				}
			});

//			ActivityChannels.openPersonalCreditReportyActivity(getContext());
		}
	};

	private UserInfoModel mUserInfoModel;

	public PersonalInfoViewModel(Context context, INetLoadView loadView) {
		super(context, loadView);
		mUserInfoModel = new UserInfoModel();
		mUserInfoModel.addCallBack(this);
	}

	@Override
	protected boolean canClearLoadAndRetryView() {
		return true;
	}
	
	@Override
	public boolean onPreLoad(String key, int state) {
		return false;
	}
	
	@Override
	public boolean onPreBackgrounLoad(String key, int state) {
		return false;
	}
	
	@Override
	public boolean onPostLoad(String key, int state) {
		return false;
	}
	
	@Override
	public boolean onFail(Runnable retryTask, String key, int state) {
		return false;
	}
	
	@Override
	public boolean onServerFail(Runnable retryTask, String key, Object data) {
		return false;
	}
	
	@Override
	public boolean onSuccess(String key, Object data, DataResult<?> dataSource) {
		if (mUserInfoModel.isLoadNetUserInfoKey(key)) {
			refresh();
			return true;
		}
		return super.onSuccess(key, data, dataSource);
	}
	
	@Override
	public boolean onDataChange(String groupKey,BaseDataModel dataModel) {
		if(UserInfoModel.DATA_GROUP_KEY.equals(groupKey)){
			refresh();
		}
		return super.onDataChange(groupKey,dataModel);
	}

	@Override
	public void onUserLoginStateChange(boolean isLogin) {
		refresh();
	}
	
	private void setUserInfo(){
		if (UserData.mUserInfo != null) {
			bPhone.set(UserData.mUserInfo.getPhone());
			String name = UserData.mUserInfo.getRealName();
			if(!TextUtils.isEmpty(name)) {
				bName.set(name);
			} else {
				bName.set(getString(R.string.user_center_name_setting_tip));
			}
		} else {
			bPhone.set(null);
			String name = UserData.getName();
			if(!TextUtils.isEmpty(name)) {
				bName.set(name);
			} else {
				bName.set(getString(R.string.user_center_name_setting_tip));
			}
		}
		bPoint.set(UserData.getPoint());
		bHeadUrl.set(UserData.getUserIcon());
		bHadSignIn.set(UserData.isSignIn());
	}
	
	public void refresh(){
		if(UserData.getUserId() == UserData.DESIRE_USER_ID){
			bPoint.set(0);
			bName.set(getString(R.string.user_center_name_login_tip));
			bHeadUrl.set(null);
			bIsLogin.set(false);
			bHadSignIn.set(false);
			bPhone.set(null);
		}else{
			setUserInfo();
			bIsLogin.set(true);
		}
	}
	
	public void onResume() {
		refresh();
		mUserInfoModel.loadNetUserInfo();
	}
	
	@Override
	public void onStart() {
	}

	@Override
	public void onRelease() {
	}
	
}
