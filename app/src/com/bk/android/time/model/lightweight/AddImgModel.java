package com.bk.android.time.model.lightweight;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;

import com.bk.android.time.model.BaseDataModel;
import com.bk.android.time.ui.photo.ImageHandler;
import com.bk.android.time.util.AppFileUtil;
import com.bk.android.util.BitmapUtil;

import java.io.File;
import java.io.Serializable;

public class AddImgModel extends BaseDataModel {
	private static final int REQUEST_CODE_IMAGE_CROP_RESULT = 7001;
	public static final int REQUEST_CODE_PHOTO_SELECT_RESULT = 7002;
	private static final int REQUEST_CODE_IMAGE_CAPTURE = 7003;

	private static final int REQUEST_CODE_IMAGE_CROP_NEW = 7004;
	private static final int REQUEST_CODE_IMAGE_CROP_RESULT_NEW = 7005;

	private static final int REQUEST_CODE_IMAGE_SELECT= 7010;
	private static final int REQUEST_CODE_IMAGE_SELECT_RESULT= 7011;


	private static final String SELECT_IMGS_KEY = "SELECT_IMGS_KEY";
	private static final String ADD_IMG_KEY = "ADD_IMG_KEY";
	private static final String EDIT_IMGS_KEY = "EDIT_IMGS_KEY";

	private Uri mFaceFileUri;
	private int[] mKeepRatioWH;
	private String mImageCapture;

	public AddImgModel(boolean keepRatio){
		this(keepRatio ? new int[]{300,300} : null);
	}

	public AddImgModel(int[] keepRatioWH){
		mKeepRatioWH = keepRatioWH;
	}

	public boolean isKeepRatio(){
		return mKeepRatioWH != null && mKeepRatioWH.length == 2;
	}

	public boolean isAddImgKey(String key){
		return key.equals(ADD_IMG_KEY);
	}

	public boolean isSelectImgsKey(String key){
		return key.equals(SELECT_IMGS_KEY);
	}

	public boolean isEditImgsKey(String key){
		return key.equals(EDIT_IMGS_KEY);
	}

	private void onGetImgIntentData(Intent data){
		try {
			Bundle extras = data.getExtras();
			if (extras != null) {
				String filePath = AppFileUtil.getRecordImgCacheFile();
				if(BitmapUtil.saveImage(filePath,(Bitmap) extras.getParcelable("data"))){
					dispatchSuccess(ADD_IMG_KEY, filePath, null);
				}
			}else if(data.getData() != null){
				String filePath = AppFileUtil.getRecordImgCacheFile();
				if(BitmapUtil.saveImage(filePath,BitmapUtil.getBitmapByteByUri(data.getData()))){
					dispatchSuccess(ADD_IMG_KEY, filePath, null);
				}
			}
		} catch (Exception e) {}
	}


	/** 拍照
	 * @param context
	 */
	public void captureImage(Activity context){
		if(isKeepRatio()) {
			Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			mFaceFileUri = getFileUri();
			intent.putExtra(MediaStore.EXTRA_OUTPUT,mFaceFileUri);
			context.startActivityForResult(intent, REQUEST_CODE_IMAGE_CROP_RESULT);
		} else {
			Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			mImageCapture = AppFileUtil.getRecordImgCacheFile();
			intent.putExtra(MediaStore.EXTRA_OUTPUT,Uri.fromFile(new File(mImageCapture)));
			context.startActivityForResult(intent, REQUEST_CODE_IMAGE_CAPTURE);
		}
	}

	public void selectPhoto(Activity context) {
		Intent intent = new Intent(Intent.ACTION_PICK);
		intent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
		context.startActivityForResult(intent, REQUEST_CODE_IMAGE_SELECT_RESULT);
	}

	public void pickPhoto(Activity context) {
//		try {
//			pickPhoto(context, REQUEST_CODE_IMAGE_CROP_RESULT, null, null);
//		} catch (Exception e) {}
		Intent intent = new Intent(Intent.ACTION_PICK);
		intent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
		context.startActivityForResult(intent, REQUEST_CODE_IMAGE_CROP_RESULT);
	}

	public void cropPhoto(Activity context) {
		Intent intent = new Intent(Intent.ACTION_PICK);
		intent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
		context.startActivityForResult(intent, REQUEST_CODE_IMAGE_CROP_NEW);
	}

	public void selectPhoto(Activity context,ImageHandler imageHandler) {
//		PhotoSelectActivity.openActivity(context, REQUEST_CODE_PHOTO_SELECT_RESULT, imageHandler);
	}

	public void selectPhoto(Activity context,int photoNumberLimit) {
	}

	public void cropPhoto(Activity context, int resultCode, Uri imgUri) {

		Intent intent = new Intent("com.android.camera.action.CROP");
		intent.setDataAndType(imgUri, "image/*");
		// 设置裁剪
		intent.putExtra("crop", "true");
		intent.putExtra("aspectX", mKeepRatioWH[0]);
		intent.putExtra("aspectY", mKeepRatioWH[1]);
		// outputX outputY 是裁剪图片宽高
		intent.putExtra("outputX", mKeepRatioWH[0]);
		intent.putExtra("outputY", mKeepRatioWH[1]);
		intent.putExtra("scale", true);
		intent.putExtra("output", Uri.fromFile(new File(AppFileUtil.getAvatarCacheFile())));
		intent.putExtra("return-data", false);
		intent.putExtra("outputFormat", "JPEG");
		intent.putExtra("noFaceDetection", true);

		context.startActivityForResult(intent, resultCode);
	}

	/**
	 * 裁剪图片方法实现
	 *
	 */
	public void pickPhoto(Activity context, int resultCode, Uri imgUri, Bitmap data) {
		Intent intent = null;
		intent = new Intent("com.android.camera.action.CROP");
		if(imgUri != null){
			String uriStr = imgUri.getPath();
			intent.setDataAndType(Uri.fromFile(new File(uriStr)), "image/*");
		}else{
			if(data != null){
				intent.putExtra("data", data);
			}else{
				intent = new Intent(Intent.ACTION_GET_CONTENT);
			}
			intent.setType("image/*");
		}
		// aspectX aspectY 是宽高的比例
		if(isKeepRatio()){
			// 设置裁剪
			intent.putExtra("crop", "true");
			intent.putExtra("aspectX", mKeepRatioWH[0]);
			intent.putExtra("aspectY", mKeepRatioWH[1]);
			// outputX outputY 是裁剪图片宽高
			intent.putExtra("outputX", mKeepRatioWH[0]);
			intent.putExtra("outputY", mKeepRatioWH[1]);
		}
		intent.putExtra("return-data", true);
		context.startActivityForResult(intent, resultCode);
	}

	public boolean onActivityResult(Activity activity,int requestCode, int resultCode, Intent data) {
		if(requestCode == REQUEST_CODE_PHOTO_SELECT_RESULT) {
//			if(resultCode == Activity.RESULT_OK) {
//				if(data != null) {
//					@SuppressWarnings("unchecked")
//					ArrayList<BitmapInfo> resultList = (ArrayList<BitmapInfo>) data.getSerializableExtra(PhotoSelectActivity.EXTRA_NAME_PHOTO_SELECT_LIST);
//					boolean editMode = data.getBooleanExtra("edit_mode", false);
//					if(resultList != null){
//						dispatchSuccess(editMode ? EDIT_IMGS_KEY : SELECT_IMGS_KEY, resultList, null);
//					}
//				}
//				return true;
//			}
		}
		// 结果码不等于取消时候
		if (resultCode != Activity.RESULT_CANCELED) {
			switch (requestCode) {
				case REQUEST_CODE_IMAGE_CROP_NEW:
					cropPhoto(activity, REQUEST_CODE_IMAGE_CROP_RESULT_NEW, data.getData());
					break;
				case REQUEST_CODE_IMAGE_CROP_RESULT_NEW:
					File file = new File(AppFileUtil.getAvatarCacheFile());
					if(file != null && file.exists()){
						dispatchSuccess(ADD_IMG_KEY, AppFileUtil.getAvatarCacheFile(), null);
					}

					break;
				case REQUEST_CODE_IMAGE_SELECT_RESULT:
					try {
						if(data.getData() != null){
							String filePath = AppFileUtil.getRecordImgCacheFile();
							if(BitmapUtil.saveImage(filePath, BitmapUtil.getBitmapByteByUri(data.getData()))){
								dispatchSuccess(ADD_IMG_KEY, filePath, null);
							}
						}
					} catch (Exception e) {}
					break;
				case REQUEST_CODE_IMAGE_CROP_RESULT:
					if (data != null) {
						onGetImgIntentData(data);
					}else{
						//如果data为空 ， EXP：samsungN9009会出现此问题
						if(mFaceFileUri != null){
							pickPhoto(activity, REQUEST_CODE_IMAGE_CROP_RESULT, mFaceFileUri, null);
							mFaceFileUri = null;
						}
					}
					return true;
				case REQUEST_CODE_IMAGE_CAPTURE:
					if (resultCode == Activity.RESULT_OK && mImageCapture != null) {
						try {
							ExifInterface localExifInterface = new ExifInterface(mImageCapture);
							int rotateInt = localExifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
							if(rotateInt != ExifInterface.ORIENTATION_NORMAL) {
								float rotate = getImageRotate(rotateInt);
								Bitmap bitmap = BitmapUtil.getBitmapInSdcard(mImageCapture,BitmapUtil.IMG_QUALITY_NON);
								Matrix m = new Matrix();
								m.postRotate(rotate);
								bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(),bitmap.getHeight(), m, true);
								BitmapUtil.saveImage(mImageCapture, bitmap);
								bitmap.recycle();
							}
						} catch (Exception e) {
							e.printStackTrace();
						}

						dispatchSuccess(ADD_IMG_KEY, mImageCapture, null);
						mImageCapture = null;
					}
					return true;
			}
		}
		return false;
	}

	/**
	 * 获得旋转角度
	 * @param rotate
	 * @return
	 */
	private static float getImageRotate(int rotate){
		float f;
		if (rotate == ExifInterface.ORIENTATION_ROTATE_90){
			f = 90.0F;
		} else if (rotate == ExifInterface.ORIENTATION_ROTATE_180){
			f = 180.0F;
		} else if (rotate == ExifInterface.ORIENTATION_ROTATE_270){
			f = 270.0F;
		} else {
			f = 0.0F;
		}
		return f;
	}

	private Uri getFileUri(){
		return Uri.fromFile(new File(AppFileUtil.getRecordImgCacheFile()));
	}

	public void onSaveInstanceState(Bundle outState) {
		if(mFaceFileUri != null){
			outState.putString("mFaceFileUri", mFaceFileUri.toString());
		}
		if(mImageCapture != null){
			outState.putString("mImageCapture", mImageCapture);
		}
	}

	public void onRestoreInstanceState(Bundle inState) {
		String imageCapture = inState.getString("mImageCapture");
		if(imageCapture != null){
			mImageCapture = imageCapture;
		}
		String faceFileUri = inState.getString("mFaceFileUri");
		if(faceFileUri != null){
			mFaceFileUri = Uri.parse(faceFileUri);
		}
	}

	public static class BitmapInfo implements Serializable{
		private static final long serialVersionUID = 4165270628796877976L;
		public String mId;
		public String mPath;
		public int mWidth;
		public int mHeight;
		public long mLastModified;
		public String[] mLongitude;
		public boolean isUpload;
		public long mDuration;
		public long mSize;
		private boolean isVideo;

		public BitmapInfo(String url, int width, int height) {
			this.mPath = url;
			this.mWidth = width;
			this.mHeight = height;
			if(url != null){
				this.mId = String.valueOf(url.hashCode());
			}else{
				this.mId = "0";
			}
		}

		public void setVideo(boolean video){
			isVideo = video;
		}

		public boolean isVideo(){
			return isVideo;
		}
	}
}
