package com.bk.android.time.model.lightweight;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;

import com.bk.android.binding.command.OnClickCommand;
import com.bk.android.data.DataResult;
import com.bk.android.time.data.UserData;
import com.bk.android.time.entity.ImageUploadInfo;
import com.bk.android.time.entity.ImageUploadInfoData;
import com.bk.android.time.entity.UserInfo;
import com.bk.android.time.entity.UserInfoData;
import com.bk.android.time.model.BaseDataModel;
import com.bk.android.time.model.BaseDialogViewModel;
import com.bk.android.time.model.BaseNetDataViewModel;
import com.bk.android.time.model.common.CommonDialogViewModel;
import com.bk.android.time.model.common.NickNameDialogViewModel;
import com.bk.android.time.ui.INetLoadView;
import com.bk.android.time.ui.common.NicknameDialog;
import com.bk.android.time.util.ResUtil;
import com.bk.android.time.util.ToastUtil;

import gueei.binding.observables.BooleanObservable;
import gueei.binding.observables.StringObservable;

public class PersonInfoEditViewModel extends BaseNetDataViewModel {

	public final static String LOGOUT_TIP_DIALOG = "LOGOUT_TIP_DIALOG";
	
	private static final int REQUEST_CODE_BABY_NICKNAME = 2000;
	private static final int REQUEST_CODE_CITY = 2001;

	public final StringObservable bHeadUrl = new StringObservable();
	public final StringObservable bBabyNicknameText = new StringObservable();
	public final StringObservable bPhoneText = new StringObservable();
	public final BooleanObservable bIsNullPhone = new BooleanObservable(false);

	public final OnClickCommand bNicknameClickCommand = new OnClickCommand() {
		@Override
		public void onClick(View v) {
			NickNameDialogViewModel dialogViewModel = new NickNameDialogViewModel(getContext());
			new NicknameDialog(getContext(), dialogViewModel);
			dialogViewModel.setCancelable(true);
			dialogViewModel.setCanceledOnTouchOutside(true);
			dialogViewModel.show();
		}
	};
	
	public final OnClickCommand bHeadClickCommand = new OnClickCommand() {
		@Override
		public void onClick(View v) {
			mAddImgModel.cropPhoto((Activity) getContext());
		}
	};

	public final OnClickCommand bLogoutClickCommand = new OnClickCommand() {
		@Override
		public void onClick(View v) {
			CommonDialogViewModel dialog = (CommonDialogViewModel) bindDialogViewModel(LOGOUT_TIP_DIALOG, null);
			dialog.setLeftBtnClickCallBack(new BaseDialogViewModel.OnBtnClickCallBack() {
				@Override
				public void onClick(View v, BaseDialogViewModel viewModel) {
					mUserInfoModel.logout();
					viewModel.finish();
					finish();
				}
			});
			dialog.show();
		}
	};

	private AddImgModel mAddImgModel;
	private UserInfoModel mUserInfoModel;
	private UserInfo mUserInfo;
	private String mUploadImageUrl;

	public PersonInfoEditViewModel(Context context, INetLoadView loadView) {
		super(context, loadView);
		mAddImgModel = new AddImgModel(true);
		mAddImgModel.addCallBack(this);
		mUserInfoModel = new UserInfoModel();
		mUserInfoModel.addCallBack(this);

	}

	@Override
	protected boolean canClearLoadAndRetryView() {
		return true;
	}

	@Override
	protected boolean needRetryTask(String key) {
		return super.needRetryTask(key);
	}
	
	@Override
	public boolean onPreLoad(String key, int state) {
		if (mUserInfoModel.isModifyUserIconeKey(key)) {
			return false;
		}
		showLoadDialog();
		return false;
	}

	@Override
	public boolean onPostLoad(String key, int state) {
		if (mUserInfoModel.isGetImageUploadTokenKey(key)) {
			return false;
		}
		hideLoadDialog();
		return false;
	}

	@Override
	public void onUserLoginStateChange(boolean isLogin) {
		refresh();
		super.onUserLoginStateChange(isLogin);
	}

	@Override
	public boolean onDataChange(String groupKey,BaseDataModel dataModel) {
		if(dataModel.equals(mUserInfoModel) && UserInfoModel.DATA_GROUP_KEY.equals(groupKey)){
			refresh();
		}
		return super.onDataChange(groupKey,dataModel);
	}
	
	@Override
	public boolean onFail(Runnable retryTask, String key, int state) {
		if (mUserInfoModel.isGetImageUploadTokenKey(key)) {
			hideLoadDialog();
			ToastUtil.showToast(getContext(), "修改头像失败，请稍后重试！！");
		} else if (mUserInfoModel.isUploadFile(key)) {
			hideLoadDialog();
			ToastUtil.showToast(getContext(), "修改头像失败，请稍后重试！！");
		}
		return super.onFail(retryTask, key, state);
	}
	
	@Override
	public boolean onServerFail(Runnable retryTask, String key, Object data) {
		if(mUserInfoModel.isEditUserInfoKey(key)){
			ToastUtil.showToast(getContext(), ResUtil.STR_TIP_NICKNAME_REPEAT);
			return false;
		} else if (mUserInfoModel.isModifyUserIconeKey(key)) {
			ToastUtil.showToast(getContext(), "修改头像失败，请稍后重试！！");
			return false;
		} else if (mUserInfoModel.isGetImageUploadTokenKey(key)) {
			hideLoadDialog();
		}
		return super.onServerFail(retryTask, key, data);
	}
	
	@Override
	public boolean onSuccess(String key, Object data, DataResult<?> dataSource) {
		if(mAddImgModel.isAddImgKey(key)){
			mUploadImageUrl = (String) data;
//			bHeadUrl.set("file://" + data);
//			mUserInfo.setIcon(bHeadUrl.get());
			mUserInfoModel.getImageUploadToken();
		} else if(mUserInfoModel.isEditUserInfoKey(key)){
			ToastUtil.showToast(getContext(), ResUtil.STR_TIP_SAVE_SUCCEED);
			if(((UserInfoData) data).getData() != null){
				mUserInfo.setIcon(((UserInfoData) data).getData().getIcon());
			}
			finish();
		} else if (mUserInfoModel.isModifyUserIconeKey(key)) {
			refresh();
			ToastUtil.showToast(getContext(), "修改头像成功！！");
		} else if (mUserInfoModel.isGetImageUploadTokenKey(key)) {
			ImageUploadInfoData imageUploadInfoData = (ImageUploadInfoData) data;
			ImageUploadInfo imageUploadInfo = imageUploadInfoData.getData();
			mUserInfoModel.uploadFile(mUploadImageUrl, imageUploadInfo.getKeyPrefix() + System.currentTimeMillis(), imageUploadInfo.getUploadToken());
		}
		return super.onSuccess(key, data, dataSource);
	}
	
	public void onActivityResult(Activity activity,int requestCode, int resultCode, Intent data) {
		if(!mAddImgModel.onActivityResult(activity, requestCode, resultCode, data)){
			if(requestCode == REQUEST_CODE_BABY_NICKNAME) {
				if(resultCode == Activity.RESULT_OK) {
					String babyNickname = data.getStringExtra("value");
					mUserInfo.setName(babyNickname);
					bBabyNicknameText.set(babyNickname);
				}
			}
		}
	}

	@Override
	public void onStart() {
		refresh();
	}

	@Override
	public void onRelease() {
	}

	private void refresh() {
		mUserInfo = mUserInfoModel.getUserInfo();
		bBabyNicknameText.set(UserData.getName());
		bPhoneText.set(UserData.getPhone());
		bHeadUrl.set(UserData.getUserIcon());
		bIsNullPhone.set(TextUtils.isEmpty(bPhoneText.get()));
	}
}
