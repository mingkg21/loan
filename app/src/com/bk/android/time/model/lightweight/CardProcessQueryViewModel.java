package com.bk.android.time.model.lightweight;

import android.content.Context;
import android.view.View;
import android.widget.AdapterView;

import com.bk.android.assistant.R;
import com.bk.android.binding.command.OnItemClickCommand;
import com.bk.android.data.DataResult;
import com.bk.android.time.entity.BankQuery;
import com.bk.android.time.entity.BankQueryListData;
import com.bk.android.time.model.BaseNetDataViewModel;
import com.bk.android.time.ui.INetLoadView;
import com.bk.android.time.ui.activiy.ActivityChannels;

import gueei.binding.collections.ArrayListObservable;
import gueei.binding.observables.BooleanObservable;
import gueei.binding.observables.FloatObservable;
import gueei.binding.observables.IntegerObservable;
import gueei.binding.observables.StringObservable;

public class CardProcessQueryViewModel extends BaseNetDataViewModel {

	public final ArrayListObservable<ItemViewModel> bItems = new ArrayListObservable<ItemViewModel>(ItemViewModel.class);

	public final StringObservable bEmptyStr = new StringObservable(getString(R.string.tip_empty_tip));
	public final IntegerObservable bEmptyImg = new IntegerObservable(R.drawable.ic_loan_nodate);
	public final FloatObservable bPadding = new FloatObservable(0f);
	public final BooleanObservable bIsEmpty = new BooleanObservable(false);
	public final IntegerObservable bItemTemplate = new IntegerObservable(R.layout.uniq_card_proccess_query_item);
	public final BooleanObservable bFootLoadingVisibility = new BooleanObservable(false);

	public final OnItemClickCommand bOnItemClickCommand = new OnItemClickCommand() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			ItemViewModel itemViewModel = (ItemViewModel) parent.getItemAtPosition(position);
			ActivityChannels.openCommonWebActivity(getContext(), itemViewModel.mDataSource.getQueryUrl());
		}
	};

	private CardProcessQueryModel mCardProcessQueryModel;

	public CardProcessQueryViewModel(Context context, INetLoadView loadView) {
		super(context, loadView);
		mCardProcessQueryModel = new CardProcessQueryModel();
		mCardProcessQueryModel.addCallBack(this);

	}

	@Override
	protected boolean needRetryTask(String key) {
		return false;
	}

	@Override
	public boolean onSuccess(String key, Object data, DataResult<?> dataSource) {
		if (mCardProcessQueryModel.isCardProcessQueryKey(key)) {
			BankQueryListData bankQueryListData = (BankQueryListData) data;

			ArrayListObservable<ItemViewModel> temp = new ArrayListObservable<ItemViewModel>(ItemViewModel.class);
			for (BankQuery bankQuery : bankQueryListData.getData()) {
				temp.add(new ItemViewModel(bankQuery));
			}

			bItems.setAll(temp);
		}
		return super.onSuccess(key, data, dataSource);
	}
	
	@Override
	public void onStart() {
		mCardProcessQueryModel.cardProcessQuery();
	}

	@Override
	public void onRelease() {
	}
	
	@Override
	protected boolean canClearLoadAndRetryView() {
		return true;
	}

	public class ItemViewModel{

		public BankQuery mDataSource;
		public final StringObservable bIconUrl = new StringObservable();
		public final StringObservable bName = new StringObservable();

		public ItemViewModel(BankQuery bankQuery) {
			mDataSource = bankQuery;
			bName.set(bankQuery.getQueryName());
			bIconUrl.set(bankQuery.getBankLogo());
		}

	}
	
}
