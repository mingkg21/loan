package com.bk.android.time.model.lightweight;

import android.content.Context;
import android.view.View;
import android.widget.AdapterView;

import com.bk.android.binding.command.OnClickCommand;
import com.bk.android.binding.command.OnItemClickCommand;
import com.bk.android.data.DataResult;
import com.bk.android.time.data.net.WebConfig;
import com.bk.android.time.entity.LoanInfo;
import com.bk.android.time.model.common.PagingLoadModel;
import com.bk.android.time.model.common.PagingLoadViewModel;
import com.bk.android.time.ui.INetLoadView;
import com.bk.android.time.ui.activiy.ActivityChannels;

import java.util.ArrayList;

import gueei.binding.collections.ArrayListObservable;
import gueei.binding.observables.BooleanObservable;
import gueei.binding.observables.StringObservable;

public class BrowsingHistoryViewModel extends PagingLoadViewModel {

	public final ArrayListObservable<GroupItemViewModel> bGroupItems = new ArrayListObservable<GroupItemViewModel>(GroupItemViewModel.class);

	public final BooleanObservable bIsEmpty = new BooleanObservable(false);
	public final StringObservable bEmptyStr = new StringObservable("暂无浏览记录");

	public final OnItemClickCommand bOnItemClickCommand = new OnItemClickCommand() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//			ActivityChannels.openCommonWebActivity(getContext(), WebConfig.getInstance().getBrowsingHistoryDeatilUrl(bGroupItems.get(position)));
		}
	};

	private BrowsingHistoryModel mShoppingCartListModel;

	public BrowsingHistoryViewModel(Context context, INetLoadView loadView) {
		super(context, loadView);
		mShoppingCartListModel = new BrowsingHistoryModel();
		mShoppingCartListModel.addCallBack(this);

	}

	@Override
	public void onStart() {
		mShoppingCartListModel.loadPage();
	}
	
	@Override
	protected boolean needRetryTask(String key) {
		return super.needRetryTask(key);
	}

	@Override
	public boolean onSuccess(String key, Object data, DataResult<?> dataSource) {
		if (mShoppingCartListModel.isPagingRequest(key)) {
			ArrayList<LoanInfo> list = mShoppingCartListModel.getData();
			ArrayListObservable<GroupItemViewModel> mTempItems = new ArrayListObservable<GroupItemViewModel>(GroupItemViewModel.class);

			GroupItemViewModel groupItemViewModel = null;
			for (LoanInfo loanInfo : list) {
				if (groupItemViewModel == null || !groupItemViewModel.bTime.get().equals(loanInfo.getBrowseDatetime())) {
					groupItemViewModel = new GroupItemViewModel(loanInfo.getBrowseDatetime());
					mTempItems.add(groupItemViewModel);
				}
				groupItemViewModel.addItem(loanInfo);
			}
			bGroupItems.setAll(mTempItems);
			bIsEmpty.set(bGroupItems.isEmpty());
			return true;
		}
		return super.onSuccess(key, data, dataSource);
	}

	@Override
	protected PagingLoadModel<?, ?> getPagingLoadModel() {
		return mShoppingCartListModel;
	}

	@Override
	public void onRelease() {}
	
	public class GroupItemViewModel {

		public final StringObservable bTime = new StringObservable();

		public final ArrayListObservable<ItemViewModel> bItems = new ArrayListObservable<ItemViewModel>(ItemViewModel.class);

		public GroupItemViewModel(String title) {
			bTime.set(title);
			bItems.clear();
		}

		public void addItem(LoanInfo loanInfo) {
			bItems.add(new ItemViewModel(loanInfo));
		}

	}

	public class ItemViewModel{

		public LoanInfo mDataSource;
		public final StringObservable bIconUrl = new StringObservable();
		public final StringObservable bTitle = new StringObservable();
		public final StringObservable bTime = new StringObservable();
		public final StringObservable bMoneyRange = new StringObservable();
		public final StringObservable bRate = new StringObservable();
		public final StringObservable bRateType = new StringObservable();

		public ItemViewModel(LoanInfo loanInfo) {
			mDataSource = loanInfo;
			bTitle.set(loanInfo.getTitle());
			bIconUrl.set(loanInfo.getIcon());
			bTime.set(loanInfo.getSummary());
			bMoneyRange.set(loanInfo.getMinLoan() + "-" + loanInfo.getMaxLoan() + "元");
			bRate.set(loanInfo.getMinInterestRate()  + "%");
			if ("day".equals(loanInfo.getInterestType())) {
				bRateType.set("日利率");
			} else {
				bRateType.set("月利率");
			}
		}

		public final OnClickCommand bItemClickCommand = new OnClickCommand() {
			@Override
			public void onClick(View v) {
				ActivityChannels.openCommonWebActivity(getContext(), WebConfig.getInstance().getBrowsingHistoryDeatilUrl(mDataSource.getId() + ""));
			}
		};

	}

}
