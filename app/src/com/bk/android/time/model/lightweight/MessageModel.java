package com.bk.android.time.model.lightweight;

import com.bk.android.data.BaseDataRequest;
import com.bk.android.time.data.request.net.MsgListRequest;
import com.bk.android.time.entity.MessageInfo;
import com.bk.android.time.entity.MessageInfoListData;
import com.bk.android.time.model.common.PagingLoadModel;

import java.util.ArrayList;


public class MessageModel extends PagingLoadModel<MessageInfoListData, MessageInfo> {

	private String mMsgType;

	public MessageModel(String msgType) {
		mMsgType = msgType;
	}

	@Override
	protected ArrayList<MessageInfo> getListDate(MessageInfoListData result) {
		return result.getData();
	}

	@Override
	protected BaseDataRequest getDataRequest(MessageInfoListData result, boolean isNext, boolean isClearData, boolean isForce) {
		if(result == null){
			return new MsgListRequest(1, mMsgType);
		}
		return new 	MsgListRequest(result.getNextPage(), mMsgType);
	}

	@Override
	protected boolean hasData(MessageInfoListData result, boolean isNext, boolean isForce) {
		if(result == null){
			return true;
		}
		return isNext && result.haveNext();
	}
}

