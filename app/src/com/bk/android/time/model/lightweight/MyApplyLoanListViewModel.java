package com.bk.android.time.model.lightweight;

import android.content.Context;

import com.bk.android.assistant.R;
import com.bk.android.time.model.common.PagingLoadModel;
import com.bk.android.time.ui.INetLoadView;

import java.io.Serializable;

import gueei.binding.collections.ArrayListObservable;
import gueei.binding.observables.StringObservable;

/**
 * Created by mingkg21 on 2017/4/9.
 */

public class MyApplyLoanListViewModel extends BaseListViewModel {

    public final ArrayListObservable<ItemViewModel> bItems = new ArrayListObservable<ItemViewModel>(ItemViewModel.class);

    public MyApplyLoanListViewModel(Context context, INetLoadView loadView) {
        super(context, loadView);
        bEmptyStr.set("您还没有贷款申请记录");
        for (int i = 0; i < 30; i++) {
            ItemViewModel item = new ItemViewModel();
            item.bTitle.set(i + "_title");
            bItems.add(item);
        }
    }

    @Override
    protected int initItemTemplate() {
        return R.layout.uniq_my_loan_item_lay;
    }

    @Override
    protected ArrayListObservable<?> getItems() {
        return bItems;
    }

    @Override
    protected void initItemsData(Serializable dataa) {

    }

    @Override
    protected void onItemClick(Object item, int position) {

    }

    @Override
    protected PagingLoadModel initPagingLoadModel() {
        return new BrowsingHistoryModel();
    }

    public class ItemViewModel {

        public final StringObservable bIconUrl = new StringObservable();
        public final StringObservable bTitle = new StringObservable();
        public final StringObservable bTime = new StringObservable();
        public final StringObservable bPhone = new StringObservable();

    }
}
