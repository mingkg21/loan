package com.bk.android.time.model.lightweight;

import com.bk.android.data.DataResult;
import com.bk.android.time.data.UserData;
import com.bk.android.time.data.request.net.ConfigLoadRequest;
import com.bk.android.time.entity.ConfigData;
import com.bk.android.time.model.BaseDataModel;

/**
 * @author mingkg21
 * @email mingkg21@gmail.com
 * @date 2016年12月27日
 *
 */
public class SystemConfigModel extends BaseDataModel{
	
	private String mGetSystemConfigKey;

	public boolean isGetSystemConfigKey(String key){
		return key.equals(mGetSystemConfigKey);
	}
	
	public void getSystemConfig(){
		ConfigLoadRequest request = new ConfigLoadRequest();
		mGetSystemConfigKey = request.getDataKey();
		startTask(request);
	}

	@Override
	protected void dispatchSuccess(String key, Object data, DataResult<?> dataSource) {
		if (isGetSystemConfigKey(key)) {
			ConfigData simpleData = (ConfigData) data;
			UserData.setShowNews(simpleData.getData().isClientShwow());
		}
		super.dispatchSuccess(key, data, dataSource);
	}
}
