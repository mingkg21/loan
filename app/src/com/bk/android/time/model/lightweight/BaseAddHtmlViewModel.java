package com.bk.android.time.model.lightweight;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;

import com.bk.android.assistant.R;
import com.bk.android.binding.command.OnClickCommand;
import com.bk.android.binding.command.OnGainFocusCommand;
import com.bk.android.binding.command.OnItemClickCommand;
import com.bk.android.binding.command.OnLostFocusCommand;
import com.bk.android.data.DataResult;
import com.bk.android.time.entity.MixDataInfo;
import com.bk.android.time.model.BaseDialogViewModel;
import com.bk.android.time.model.BaseNetDataViewModel;
import com.bk.android.time.model.lightweight.AddImgModel.BitmapInfo;
import com.bk.android.time.ui.INetLoadView;
import com.bk.android.time.ui.activiy.ActivityChannels;
import com.bk.android.time.ui.photo.ImageHandler;
import com.bk.android.time.util.FaceUtil;
import com.bk.android.time.util.ToastUtil;
import com.bk.android.util.BitmapUtil;

import java.util.ArrayList;

import gueei.binding.collections.ArrayListObservable;
import gueei.binding.observables.BooleanObservable;
import gueei.binding.observables.IntegerObservable;

public abstract class BaseAddHtmlViewModel extends BaseNetDataViewModel{
	public static final String ADD_DIALOG = "ADD_IMG_DIALOG";

	public final ArrayListObservable<FaceGroupItemViewModel> bFaceGroupItems = new ArrayListObservable<FaceGroupItemViewModel>(FaceGroupItemViewModel.class);
	
	public final BooleanObservable bIsShowFacePanle = new BooleanObservable(false);
	public final BooleanObservable bIsProgressShow = new BooleanObservable(false);
	public final IntegerObservable bSyncProgress = new IntegerObservable(0);
	public final BooleanObservable bCanDeleteImg = new BooleanObservable(false);
	public final BooleanObservable bIsEditContent = new BooleanObservable(false);

	public final OnGainFocusCommand bOnGainFocusCommand = new OnGainFocusCommand() {
		@Override
		public void onGainFocus(View view) {
			bIsEditContent.set(true);
		}
	};
	
	public final OnLostFocusCommand bOnLostFocusCommand = new OnLostFocusCommand() {
		@Override
		public void onLostFocus(View view) {
			bIsEditContent.set(false);
		}
	};
	
	public final OnClickCommand bShootingImgClickCommand = new OnClickCommand() {
		@Override
		public void onClick(View v) {
			gotoAddShootingImg();
		}
	};
	
	public final OnClickCommand bPhotoImgClickCommand = new OnClickCommand() {
		@Override
		public void onClick(View v) {
			gotoAddPhotoImg();
		}
	};
	
	public final OnClickCommand bDeleteImgClickCommand = new OnClickCommand() {
		@Override
		public void onClick(View v) {
			onDeleteImg();
		}
	};

	public final OnClickCommand bContentClickCommand = new OnClickCommand() {
		@Override
		public void onClick(View v) {
			bIsShowFacePanle.set(false);
		}
	};

	public final OnClickCommand bFaceClickCommand = new OnClickCommand() {
		@Override
		public void onClick(View v) {
			bIsShowFacePanle.set(!bIsShowFacePanle.get());
			faceClick();
		}
	};
	
	public final OnClickCommand bCancelClickCommand = new OnClickCommand() {

		@Override
		public void onClick(View v) {
			if(mAddDialogViewModel != null && mAddDialogViewModel.isShowing()){
				mAddDialogViewModel.finish();
			}
		}
	};
	
	protected AddImgModel mAddImgModel;
	private AddHtmlView mHtmlView;
	private BaseDialogViewModel mAddDialogViewModel;

	public BaseAddHtmlViewModel(Context context, INetLoadView loadView,AddHtmlView htmlView) {
		super(context, loadView);
		mHtmlView = htmlView;
		mAddImgModel = new AddImgModel(getkeepRatioWH());
		mAddImgModel.addCallBack(this);
		
		fillFace();
	}

	protected int[] getkeepRatioWH(){
		return null;
	}

	protected abstract int getMaxImgSize();

	protected int checkImgSize(){
		ArrayList<MixDataInfo> mixDataInfos = MixDataInfo.getMixDataInfos(getHtml());
		int size = 0;
		if(mixDataInfos != null){
			for (MixDataInfo mixDataInfo : mixDataInfos) {
				if(!TextUtils.isEmpty(mixDataInfo.getImgUrl())){
					size++;
				}
			}
		}
		return getMaxImgSize() - size;
	}

	protected void faceClick() {

	}
	
	protected String getHtml() {
		return mHtmlView.getHtml();
	}
	
	protected void addImage(String url, int width, int height) {
		mHtmlView.addImage(url, width, height);
	}

	protected void setHtml(String html) {
		mHtmlView.setHtml(html);
	}
	
	protected void addFace(String key) {
		mHtmlView.addFace(key);
	}
	
	protected void onDeleteImg() {
		if(mAddDialogViewModel != null && mAddDialogViewModel.isShowing()){
			mAddDialogViewModel.finish();
		}
	}
	
	protected void gotoAddShootingImg(){
		if(mAddDialogViewModel != null && mAddDialogViewModel.isShowing()){
			mAddDialogViewModel.finish();
		}
		if(checkImgSize() == 0){
			ToastUtil.showToast(getContext(), getString(R.string.photo_select_limit,getMaxImgSize()));
		}else{
			mAddImgModel.captureImage((Activity) getContext());
		}
	}
	
	protected void gotoAddPhotoImg(){
		if(mAddDialogViewModel != null && mAddDialogViewModel.isShowing()){
			mAddDialogViewModel.finish();
		}
		int size = checkImgSize();
		if(size == 0){
			ToastUtil.showToast(getContext(), getString(R.string.photo_select_limit,getMaxImgSize()));
		}else{
			mAddImgModel.selectPhoto((Activity) getContext(),getImageHandler(size));
		}
	}
	
	protected ImageHandler getImageHandler(int size){
		return ImageHandler.create(size, false);
	}
	
	public void showAddImgDialog(){
		mAddDialogViewModel = bindDialogViewModel(ADD_DIALOG, this);
		mAddDialogViewModel.setOnDismissListener(new OnDismissListener() {
			@Override
			public void onDismiss(DialogInterface dialog) {
				mAddDialogViewModel = null;
			}
		});
		mAddDialogViewModel.show();
	}
	
	public void showEditImg(ArrayList<BitmapInfo> imgs, int position){
	}

	protected String setItemImgPath(String path){
		if(path == null){
			return path;
		}
		if(path.startsWith("file://") || path.startsWith("http://") || path.startsWith("android.resource://")) {
			return path;
		} else {
			return "file://" + path;
		}
	}

	@Override
	protected boolean canClearLoadAndRetryView() {
		return true;
	}

	@Override
	protected boolean needRetryTask(String key) {
		return false;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public boolean onSuccess(String key, Object data, DataResult<?> dataSource) {
		if(mAddImgModel.isAddImgKey(key)){
			String imgPath = (String) data;
			int[] WH = BitmapUtil.getLocalWH(imgPath,BitmapUtil.IMG_QUALITY_1);
			ArrayList<BitmapInfo> bitmapInfos = new ArrayList<BitmapInfo>();
			bitmapInfos.add(new BitmapInfo(imgPath, WH[0], WH[1]));
			onAddImgs(bitmapInfos);
			return true;
		} else if(mAddImgModel.isSelectImgsKey(key)){
			onAddImgs((ArrayList<BitmapInfo>) data);
			return true;
		} else if(mAddImgModel.isEditImgsKey(key)) {
			onEditImgs((ArrayList<BitmapInfo>) data);
			return true;
		}
		return super.onSuccess(key, data, dataSource);
	}
	
	protected void onAddImgs(ArrayList<BitmapInfo> bitmapInfos) {
		if(bitmapInfos == null){
			return;
		}
		for (BitmapInfo bitmapInfo : bitmapInfos) {
			addImage(bitmapInfo.mPath, bitmapInfo.mWidth, bitmapInfo.mHeight);
		}
	}
	
	protected void onEditImgs(ArrayList<BitmapInfo> imgs){
	}

	private void fillFace(){
		String[][] groupKeys = FaceUtil.getFaceKeys();
		ArrayListObservable<FaceGroupItemViewModel> groupTemps = new ArrayListObservable<FaceGroupItemViewModel>(FaceGroupItemViewModel.class);
		for (String[] keys : groupKeys) {
			FaceGroupItemViewModel groupItem = new FaceGroupItemViewModel();
			ArrayListObservable<FaceItemViewModel> temps = new ArrayListObservable<FaceItemViewModel>(FaceItemViewModel.class);
			for (String key : keys) {
				FaceItemViewModel item = new FaceItemViewModel();
				item.mFaceKey = key;
				item.bFaceRes.set(FaceUtil.getFaceRes(key));
				temps.add(item);
			}
			groupItem.bFaceItems.setAll(temps);
			groupTemps.add(groupItem);
		}
		bFaceGroupItems.setAll(groupTemps);
	}
	
	public boolean onBackPressed(){
		if(bIsShowFacePanle.get()){
			bIsShowFacePanle.set(false);
			return true;
		}
		return false;
	}
	
	public void onRestoreInstanceState(Bundle inState) {
		setHtml(inState.getString("newMix"));
		mAddImgModel.onRestoreInstanceState(inState);
	}

	public void onSaveInstanceState(Bundle outState) {
		outState.putString("newMix", getHtml());
		mAddImgModel.onSaveInstanceState(outState);
	}
	
	public void onActivityResult(Activity activity, int requestCode,int resultCode, Intent data) {
		mAddImgModel.onActivityResult(activity, requestCode, resultCode, data);
	}
	
	public class FaceGroupItemViewModel{
		public final ArrayListObservable<FaceItemViewModel> bFaceItems = new ArrayListObservable<FaceItemViewModel>(FaceItemViewModel.class);
		public final OnItemClickCommand bOnFaceItemClickCommand = new OnItemClickCommand() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,long id) {
				FaceItemViewModel item = (FaceItemViewModel) parent.getItemAtPosition(position);
				if(item != null){
					addFace(item.mFaceKey);
				}
			}
		};
	}
	
	public class FaceItemViewModel{
		public String mFaceKey;
		public final IntegerObservable bFaceRes = new IntegerObservable();
	}

	public interface AddHtmlView{
		public void setHtml(String html);
		public String getHtml();
		public void addImage(String url,int width,int height);
		public void addFace(String key);
	}
}
