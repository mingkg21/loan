package com.bk.android.time.model.lightweight;

import android.content.Context;

import com.bk.android.assistant.R;
import com.bk.android.time.entity.NetEaseNews;
import com.bk.android.time.entity.NetEaseNewsListData;
import com.bk.android.time.model.common.PagingLoadModel;
import com.bk.android.time.ui.INetLoadView;
import com.bk.android.time.ui.activiy.NetEaseNewsDetailActivity;

import java.io.Serializable;
import java.util.ArrayList;

import gueei.binding.collections.ArrayListObservable;
import gueei.binding.observables.StringObservable;

/**
 * Created by mingkg21 on 2017/7/15.
 */

public class NetEaseNewsListViewModel extends BaseListViewModel {

    public final ArrayListObservable<ItemViewModel> bItems = new ArrayListObservable<ItemViewModel>(ItemViewModel.class);

    public NetEaseNewsListViewModel(Context context, INetLoadView loadView) {
        super(context, loadView);

    }

    @Override
    protected int initItemTemplate() {
        return R.layout.uniq_net_ease_news_item_lay;
    }

    @Override
    protected ArrayListObservable<?> getItems() {
        return bItems;
    }

    @Override
    protected void initItemsData(Serializable data) {
        ArrayListObservable<ItemViewModel> temps = new ArrayListObservable<ItemViewModel>(ItemViewModel.class);

        NetEaseNewsListData messageInfoListData = (NetEaseNewsListData) data;
        ArrayList<NetEaseNews> infos = messageInfoListData.getData();
        for (NetEaseNews info : infos) {
            temps.add(new ItemViewModel(info));
        }
        bItems.setAll(temps);
    }

    @Override
    protected void onItemClick(Object item, int position) {
        ItemViewModel itemViewModel = (ItemViewModel) item;
        NetEaseNewsDetailActivity.openActivity(getContext(), itemViewModel.mDataSource.getId());
    }

    @Override
    protected PagingLoadModel initPagingLoadModel() {
        return new NetEaseNewskListModel();
    }

    public class ItemViewModel {

        public NetEaseNews mDataSource;

        public final StringObservable bIconUrl = new StringObservable();
        public final StringObservable bTitle = new StringObservable();
        public final StringObservable bTime = new StringObservable();

        public ItemViewModel(NetEaseNews info) {
            mDataSource = info;
            bTitle.set(info.getTitle());
            bIconUrl.set(info.getIcon());
            bTime.set(info.getTime());
        }

    }
}
