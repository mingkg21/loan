package com.bk.android.time.model.lightweight;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import com.bk.android.assistant.R;
import com.bk.android.binding.command.OnClickCommand;
import com.bk.android.data.DataResult;
import com.bk.android.time.data.UserData;
import com.bk.android.time.entity.UserInfo;
import com.bk.android.time.model.BaseDataModel;
import com.bk.android.time.model.BaseNetDataViewModel;
import com.bk.android.time.ui.INetLoadView;
import com.bk.android.time.ui.common.PopupListDialog;
import com.bk.android.time.util.ToastUtil;
import com.bk.android.time.util.UserInfoUtil;

import java.util.HashMap;

import gueei.binding.observables.StringObservable;

/**
 *
 */
public class MyIdentityViewModel extends BaseNetDataViewModel {

	public final StringObservable bProfession = new StringObservable();
	public final StringObservable bIncomeType = new StringObservable();
	public final StringObservable bIncome = new StringObservable();
	public final StringObservable bCompany = new StringObservable();
	public final StringObservable bWorkTime = new StringObservable();
	public final StringObservable bProvidentFund = new StringObservable();
	public final StringObservable bSocialSecurity = new StringObservable();

	public final OnClickCommand bProfessionClickCommand = new OnClickCommand() {
		@Override
		public void onClick(View v) {
			PopupListDialog dialog = new PopupListDialog(getContext(), R.array.profession, new PopupListDialog.OnItemSelectedListener() {
				@Override
				public void onItemSelected(int position, String str) {
					bProfession.set(str);
				}
			});
			dialog.show();
		}
	};

	public final OnClickCommand bIncomenClickCommand = new OnClickCommand() {
		@Override
		public void onClick(View v) {
			PopupListDialog dialog = new PopupListDialog(getContext(), R.array.income_type, new PopupListDialog.OnItemSelectedListener() {
				@Override
				public void onItemSelected(int position, String str) {
					bIncomeType.set(str);
				}
			});
			dialog.show();
		}
	};

	public final OnClickCommand bCompanyClickCommand = new OnClickCommand() {
		@Override
		public void onClick(View v) {
			PopupListDialog dialog = new PopupListDialog(getContext(), R.array.company_type, new PopupListDialog.OnItemSelectedListener() {
				@Override
				public void onItemSelected(int position, String str) {
					bCompany.set(str);
				}
			});
			dialog.show();
		}
	};

	public final OnClickCommand bWorkTimeClickCommand = new OnClickCommand() {
		@Override
		public void onClick(View v) {
			PopupListDialog dialog = new PopupListDialog(getContext(), R.array.work_time, new PopupListDialog.OnItemSelectedListener() {
				@Override
				public void onItemSelected(int position, String str) {
					bWorkTime.set(str);
				}
			});
			dialog.show();
		}
	};

	public final OnClickCommand bProvidentFundClickCommand = new OnClickCommand() {
		@Override
		public void onClick(View v) {
			PopupListDialog dialog = new PopupListDialog(getContext(), R.array.yes_no, new PopupListDialog.OnItemSelectedListener() {
				@Override
				public void onItemSelected(int position, String str) {
					bProvidentFund.set(str);
				}
			});
			dialog.show();
		}
	};

	public final OnClickCommand bSocialSecurityClickCommand = new OnClickCommand() {
		@Override
		public void onClick(View v) {
			PopupListDialog dialog = new PopupListDialog(getContext(), R.array.yes_no, new PopupListDialog.OnItemSelectedListener() {
				@Override
				public void onItemSelected(int position, String str) {
					bSocialSecurity.set(str);
				}
			});
			dialog.show();
		}
	};

	public final OnClickCommand bSaveClickCommand = new OnClickCommand() {
		@Override
		public void onClick(View v) {
			if (TextUtils.isEmpty(bProfession.get())) {
				ToastUtil.showToast(getContext(), "请选择职业身份！");
				return;
			}
			if (TextUtils.isEmpty(bIncomeType.get())) {
				ToastUtil.showToast(getContext(), "请选择收入形式！");
				return;
			}
			if (TextUtils.isEmpty(bIncome.get())) {
				ToastUtil.showToast(getContext(), "请输入月收入！");
				return;
			}
			if (TextUtils.isEmpty(bCompany.get())) {
				ToastUtil.showToast(getContext(), "请选择公司类型！");
				return;
			}
			if (TextUtils.isEmpty(bWorkTime.get())) {
				ToastUtil.showToast(getContext(), "请选择当前公司工作时间！");
				return;
			}
			if (TextUtils.isEmpty(bProvidentFund.get())) {
				ToastUtil.showToast(getContext(), "请选择是否连续6个月以上缴纳公积金！");
				return;
			}
			if (TextUtils.isEmpty(bSocialSecurity.get())) {
				ToastUtil.showToast(getContext(), "请选择是否连续6个月以上缴纳社保！");
				return;
			}
			HashMap<String, Object> parameterMap = new HashMap<>();
			parameterMap.put("profession", UserInfoUtil.professionStr(bProfession.get()));
			parameterMap.put("incomeType", UserInfoUtil.incomTypeStr(bIncomeType.get()));
			parameterMap.put("incomeValue", bIncome.get());
			parameterMap.put("companyType", UserInfoUtil.companyTypeStr(bCompany.get()));
			parameterMap.put("companyWorkTime", UserInfoUtil.companyWorkTimeStr(bWorkTime.get()));
			parameterMap.put("moreSixFundFlag", UserInfoUtil.yesOrNoStr(bProvidentFund.get()));
			parameterMap.put("moreSixSocialFlag", UserInfoUtil.yesOrNoStr(bSocialSecurity.get()));

			mUserInfoModel.saveUserInfo(parameterMap);
		}
	};

	private UserInfoModel mUserInfoModel;

	public MyIdentityViewModel(Context context, INetLoadView loadView) {
		super(context, loadView);
		mUserInfoModel = new UserInfoModel();
		mUserInfoModel.addCallBack(this);

	}

	@Override
	protected boolean canClearLoadAndRetryView() {
		return true;
	}
	
	@Override
	public boolean onPreLoad(String key, int state) {
		if (mUserInfoModel.isEditUserInfoKey(key)) {
			showLoadDialog();
			return false;
		}
		return false;
	}
	
	@Override
	public boolean onPreBackgrounLoad(String key, int state) {
		return false;
	}
	
	@Override
	public boolean onPostLoad(String key, int state) {
		if (mUserInfoModel.isEditUserInfoKey(key)) {
			hideLoadDialog();
			return false;
		}
		return false;
	}
	
	@Override
	public boolean onSuccess(String key, Object data, DataResult<?> dataSource) {
		if (mUserInfoModel.isEditUserInfoKey(key)) {
			ToastUtil.showToast(getContext(), "编辑身份信息成功！");

			UserData.mUserInfo.setProfession(UserInfoUtil.professionStr(bProfession.get()));
			UserData.mUserInfo.setIncomeType(UserInfoUtil.incomTypeStr(bIncomeType.get()));
			UserData.mUserInfo.setIncomeValue(bIncome.get());
			UserData.mUserInfo.setCompanyType(UserInfoUtil.companyTypeStr(bCompany.get()));
			UserData.mUserInfo.setCompanyWorkTime(UserInfoUtil.companyWorkTimeStr(bWorkTime.get()));
			UserData.mUserInfo.setMoreSixFundFlag(UserInfoUtil.yesOrNoStr(bProvidentFund.get()));
			UserData.mUserInfo.setMoreSixSocialFlag(UserInfoUtil.yesOrNoStr(bSocialSecurity.get()));
			return true;
		}
		return super.onSuccess(key, data, dataSource);
	}
	
	@Override
	public boolean onDataChange(String groupKey,BaseDataModel dataModel) {
		if(UserInfoModel.DATA_GROUP_KEY.equals(groupKey)){
			refresh();
		}
		return super.onDataChange(groupKey,dataModel);
	}

	@Override
	public void onUserLoginStateChange(boolean isLogin) {
		refresh();
	}
	
	private void setUserInfo(){
		UserInfo userInfo = UserData.mUserInfo;
		bProfession.set(UserInfoUtil.profession(userInfo.getProfession()));
		bIncomeType.set(UserInfoUtil.incomType(userInfo.getIncomeType()));
		bIncome.set(userInfo.getIncomeValue());
		bCompany.set(UserInfoUtil.companyType(userInfo.getCompanyType()));
		bWorkTime.set(UserInfoUtil.companyWorkTime(userInfo.getCompanyWorkTime()));
		bProvidentFund.set(UserInfoUtil.yesOrNo(userInfo.getMoreSixFundFlag()));
		bSocialSecurity.set(UserInfoUtil.yesOrNo(userInfo.getMoreSixSocialFlag()));
	}
	
	public void refresh(){
		if(UserData.getUserId() == UserData.DESIRE_USER_ID){
		}else{
			setUserInfo();
		}
	}
	
	@Override
	public void onStart() {
		refresh();
	}

	@Override
	public void onRelease() {
	}
}
