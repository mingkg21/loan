package com.bk.android.time.model.lightweight;

import com.bk.android.data.BaseDataRequest;
import com.bk.android.time.data.request.net.FeedbackAddRequest;
import com.bk.android.time.data.request.net.FeedbackListRequest;
import com.bk.android.time.data.request.net.UserFeedbackRequest;
import com.bk.android.time.entity.FeedbackInfo;
import com.bk.android.time.entity.FeedbackList;
import com.bk.android.time.entity.FeedbackListData;
import com.bk.android.time.model.common.PagingLoadModel;

import java.util.ArrayList;

public class FeedbackModel extends PagingLoadModel<FeedbackListData, FeedbackInfo> {

	private String isAddFeedbackKey;
	private String isUserFeedbackKey;

	public FeedbackModel() {
	}
	
	@Override
	protected ArrayList<FeedbackInfo> getListDate(FeedbackListData result) {
		FeedbackList list = result.getData();
		if(list != null){
			return list.getDataList();
		}
		return null;
	}

	@Override
	protected BaseDataRequest getDataRequest(FeedbackListData result,boolean isNext, boolean isClearData, boolean isForce) {
		if(result == null){
			return new FeedbackListRequest(0);
		}
		return new FeedbackListRequest(result.getData().getNextstartpos());
	}

	@Override
	protected boolean hasData(FeedbackListData result,boolean isNext, boolean isForce) {
		if(result == null){
			return true;
		}
		FeedbackList list = result.getData();
		if(list == null){
			return false;
		}
		return isNext && list.hasnext();
	}
	
	public boolean isAddFeedbackKey(String key){
		return key.equals(isAddFeedbackKey);
	}
	
	public void addFeedback(String content,String qq,String number){
		FeedbackAddRequest request = new FeedbackAddRequest(content, qq, number);
		isAddFeedbackKey = request.getDataKey();
		startTask(request);
	}

	public boolean isUserFeedbackKey(String key){
		return key.equals(isUserFeedbackKey);
	}

	public void userFeedback(String kindId, String content, String contactType, String contact){
		UserFeedbackRequest request = new UserFeedbackRequest(kindId, content, contactType, contact);
		isUserFeedbackKey = request.getDataKey();
		startTask(request);
	}
}
