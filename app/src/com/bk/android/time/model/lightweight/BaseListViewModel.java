package com.bk.android.time.model.lightweight;

import android.content.Context;
import android.view.View;
import android.widget.AdapterView;

import com.bk.android.assistant.R;
import com.bk.android.binding.command.OnItemClickCommand;
import com.bk.android.data.DataResult;
import com.bk.android.time.model.common.PagingLoadModel;
import com.bk.android.time.model.common.PagingLoadViewModel;
import com.bk.android.time.ui.INetLoadView;

import java.io.Serializable;

import gueei.binding.collections.ArrayListObservable;
import gueei.binding.observables.BooleanObservable;
import gueei.binding.observables.FloatObservable;
import gueei.binding.observables.IntegerObservable;
import gueei.binding.observables.StringObservable;

public abstract class BaseListViewModel extends PagingLoadViewModel {

    public final BooleanObservable bIsEmpty = new BooleanObservable(false);
    public final IntegerObservable bItemTemplate = new IntegerObservable();
    public final FloatObservable bPadding = new FloatObservable(getDimension(R.dimen.list_padding));
    public final StringObservable bEmptyStr = new StringObservable(getString(R.string.tip_empty_tip));
    public final IntegerObservable bEmptyImg = new IntegerObservable(R.drawable.ic_loan_nodate);

    public final OnItemClickCommand bOnItemClickCommand = new OnItemClickCommand() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            BaseListViewModel.this.onItemClick(parent.getItemAtPosition(position), position);
        }
    };

    private PagingLoadModel mPagingLoadModel;

    public BaseListViewModel(Context context, INetLoadView loadView) {
        super(context, loadView);
        mPagingLoadModel = initPagingLoadModel();
        mPagingLoadModel.addCallBack(this);

        bItemTemplate.set(initItemTemplate());
    }

    protected abstract int initItemTemplate();

    @Override
    public void onStart() {
        mPagingLoadModel.loadPage();
    }

    @Override
    public void onRelease() {
    }

    protected abstract PagingLoadModel initPagingLoadModel();

    @Override
    public PagingLoadModel getPagingLoadModel() {
        return mPagingLoadModel;
    }

    protected abstract ArrayListObservable<?> getItems();

    @Override
    public boolean onFail(Runnable retryTask, String key, int state) {
        if (!getItems().isEmpty() && getPagingLoadModel().isPagingRequest(key)) {
            return false;
        }
        return super.onFail(retryTask, key, state);
    }

    @Override
    public boolean onServerFail(Runnable retryTask, String key, Object data) {
        if (!getItems().isEmpty() && getPagingLoadModel().isPagingRequest(key)) {
            return false;
        }
        return super.onServerFail(retryTask, key, data);
    }

    @Override
    public boolean onSuccess(String key, Object data, DataResult<?> dataSource) {
        if (mPagingLoadModel.isPagingRequest(key)) {
            fillData();
        }
        return super.onSuccess(key, data, dataSource);
    }

    private void fillData() {
        initItemsData(mPagingLoadModel.getCurrentResult());
        bIsEmpty.set(getItems().isEmpty());
    }

    protected abstract void initItemsData(Serializable data);

    protected abstract void onItemClick(Object item, int position);

}
