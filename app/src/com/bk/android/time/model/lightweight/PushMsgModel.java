package com.bk.android.time.model.lightweight;

import android.text.TextUtils;

import com.bk.android.dao.DBKeyValueProviderProxy;
import com.bk.android.data.DataResult;
import com.bk.android.time.data.dao.DBPreferencesProvider;
import com.bk.android.time.data.request.net.AddPushRequest;
import com.bk.android.time.data.request.net.GetPushMsgRequest;
import com.bk.android.time.entity.PushMsgInfo;
import com.bk.android.time.entity.PushMsgInfoList;
import com.bk.android.time.entity.PushMsgInfoListData;
import com.bk.android.time.model.BaseDataModel;
import com.bk.android.time.util.NoticeUtils;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author mingkg21
 * @email mingkg21@gmail.com
 * @date 2016年12月28日
 *
 */
public class PushMsgModel extends BaseDataModel{

	private static final String PREFERENCE_TYPE_DATA = "PREFERENCE_TYPE_PUSH_DATA";

	private static final String KEY_PUSH_UPDATE_TIME = "KEY_PUSH_UPDATE_TIME";
	private static final String KEY_PUSH_LIST = "KEY_PUSH_LIST";
	
	private String mGetPushMsgKey;

	public boolean isGetPushMsgKey(String key){
		return key.equals(mGetPushMsgKey);
	}
	
	public void getPushMsg(){
		GetPushMsgRequest request = new GetPushMsgRequest();
		mGetPushMsgKey = request.getDataKey();
		startTask(request);
	}

	public void addPushDisplay(PushMsgInfo info) {
		addPush(info.getPushId(), AddPushRequest.UPDATE_TYPE_DISPLAY);
		setPushUpdateTime(System.currentTimeMillis());
		setPushHistory(info.getPushId() + "");
	}

	public void addPushClick(int pushId) {
		addPush(pushId, AddPushRequest.UPDATE_TYPE_CLICK);
	}

	private void addPush(int pushId, String updateType) {
		AddPushRequest request = new AddPushRequest(pushId, updateType);
		startTask(request);
	}

	@Override
	protected void dispatchSuccess(String key, Object data, DataResult<?> dataSource) {
		if (isGetPushMsgKey(key)) {
			PushMsgInfoListData pushMsgInfoListData = (PushMsgInfoListData) data;
			if (pushMsgInfoListData != null && pushMsgInfoListData.getData() != null && pushMsgInfoListData.getData().getPushMsgList() != null) {
				PushMsgInfoList pushMsgInfoList = pushMsgInfoListData.getData();
				String intervalTime = pushMsgInfoList.getIntervalTime();
				if (TextUtils.isEmpty(intervalTime)) {
					return;
				}
				try {
					long time = Long.valueOf(intervalTime);
					if ((System.currentTimeMillis() - getPushUpdateTime()) < time) {
						return;
					}
				} catch (Exception e) {
					return;
				}
				ArrayList<PushMsgInfo> pushMsgInfos = pushMsgInfoList.getPushMsgList();
				if (pushMsgInfos != null && pushMsgInfos.size() > 0) {
					HashMap<String, String> pushHistoryMap = getPushHistoryMap();
					PushMsgInfo pushInfo = null;
					int count = 0;
					for (PushMsgInfo info : pushMsgInfos) {
						if (!pushHistoryMap.containsKey(info.getPushId() + "")) {
							pushInfo = info;
							break;
						} else {
							count++;
						}
					}
//					if (pushInfo == null) {
//						pushInfo = pushMsgInfos.get(0);
//					}
//					if (count == pushMsgInfos.size()) {
//						clearPushHistory();
//					}
					if (pushInfo != null) {
						NoticeUtils.show(getContext(), pushInfo);
					}
				}
			}
		}
		super.dispatchSuccess(key, data, dataSource);
	}

	public static long getPushUpdateTime(){
		return getProxy().getLong(KEY_PUSH_UPDATE_TIME, PREFERENCE_TYPE_DATA, 0);
	}

	public void setPushUpdateTime(long value){
		getProxy().putLong(KEY_PUSH_UPDATE_TIME, value, PREFERENCE_TYPE_DATA);
	}

	private HashMap<String, String> getPushHistoryMap() {
		HashMap<String, String> result = new HashMap<>();
		String[] temp = getPushHistory();
		for (String s : temp) {
			result.put(s, s);
		}
		return result;
	}

	private String[] getPushHistory() {
		String str = getProxy().getString(KEY_PUSH_LIST, PREFERENCE_TYPE_DATA, "");
		if (!TextUtils.isEmpty(str)) {
			return str.split(",");
		}
		return new String[]{};
	}

	private void setPushHistory(String value){
		if (TextUtils.isEmpty(value)) {
			return;
		}

		StringBuilder sb = new StringBuilder();
		sb.append(value);
		String[] searchHistory = getPushHistory();
		if (searchHistory != null) {
			for (String s : searchHistory) {
				if (!value.equals(s)) {
					sb.append(",");
					sb.append(s);
				}
			}
		}
		getProxy().putString(KEY_PUSH_LIST, sb.toString(), PREFERENCE_TYPE_DATA);
	}

	public void clearPushHistory() {
		getProxy().putString(KEY_PUSH_LIST, "", PREFERENCE_TYPE_DATA);
	}

	private static DBKeyValueProviderProxy getProxy(){
		return DBPreferencesProvider.getProxy();
	}
}
