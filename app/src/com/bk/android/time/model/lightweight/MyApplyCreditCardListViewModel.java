package com.bk.android.time.model.lightweight;

import android.content.Context;

import com.bk.android.assistant.R;
import com.bk.android.time.model.common.PagingLoadModel;
import com.bk.android.time.ui.INetLoadView;

import java.io.Serializable;

import gueei.binding.collections.ArrayListObservable;
import gueei.binding.observables.StringObservable;

/**
 * Created by mingkg21 on 2017/4/9.
 */

public class MyApplyCreditCardListViewModel extends BaseListViewModel {

    public final ArrayListObservable<ItemViewModel> bItems = new ArrayListObservable<ItemViewModel>(ItemViewModel.class);

    public MyApplyCreditCardListViewModel(Context context, INetLoadView loadView) {
        super(context, loadView);

        for (int i = 0; i < 30; i++) {
            bItems.add(new ItemViewModel(i + "_信用卡"));
        }
    }

    @Override
    protected int initItemTemplate() {
        return R.layout.uniq_my_apply_credit_item_lay;
    }

    @Override
    protected ArrayListObservable<?> getItems() {
        return bItems;
    }

    @Override
    protected void initItemsData(Serializable data) {

    }

    @Override
    protected void onItemClick(Object item, int position) {

    }

    @Override
    protected PagingLoadModel initPagingLoadModel() {
        return new BrowsingHistoryModel();
    }

    public class ItemViewModel {

        public final StringObservable bIconUrl = new StringObservable();
        public final StringObservable bName = new StringObservable();

        public ItemViewModel(String name) {
            bName.set(name);
        }

    }
}
