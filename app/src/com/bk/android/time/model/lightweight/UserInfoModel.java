package com.bk.android.time.model.lightweight;

import android.content.Intent;
import android.text.TextUtils;

import com.bk.android.assistant.R;
import com.bk.android.data.DataResult;
import com.bk.android.time.app.App;
import com.bk.android.time.app.AppBroadcast;
import com.bk.android.time.data.Preferences;
import com.bk.android.time.data.UserData;
import com.bk.android.time.data.request.net.EditUserInfoRequest;
import com.bk.android.time.data.request.net.GetImageUploadTokenRequest;
import com.bk.android.time.data.request.net.LoginRequest;
import com.bk.android.time.data.request.net.ModifyNicknameRequest;
import com.bk.android.time.data.request.net.ModifyUserIconRequest;
import com.bk.android.time.data.request.net.RegisterRequest;
import com.bk.android.time.data.request.net.ResetPassowrdRequest;
import com.bk.android.time.data.request.net.SendSmsRequest;
import com.bk.android.time.data.request.net.UserInfoRequest;
import com.bk.android.time.entity.LoginInfo;
import com.bk.android.time.entity.LoginInfoData;
import com.bk.android.time.entity.UserInfo;
import com.bk.android.time.entity.UserInfoData;
import com.bk.android.time.model.BaseDataModel;
import com.bk.android.time.util.ToastUtil;
import com.bk.android.util.LogUtil;
import com.qiniu.android.http.ResponseInfo;
import com.qiniu.android.storage.Configuration;
import com.qiniu.android.storage.UpCompletionHandler;
import com.qiniu.android.storage.UploadManager;

import org.json.JSONObject;

import java.util.HashMap;

public class UserInfoModel extends BaseDataModel {

	public static final String DATA_GROUP_KEY = "USER_DATA_GROUP_KEY";

	private String mEditUserInfoKey;
	private String mLoadNetUserInfoKey;
	private String mGetUserInfoKey;
	private String mModifyNicknameKey;
	private String mModifyUserIconKey;
	private String mGetImageUploadTokenKey;
	private String mIsUploadFileKey;
	private String mResetPasswordKey;

	private String mIsSendSMSKey;
	private String mRegisterKey;
	private String mLoginKey;

	public void login(UserInfo userInfo){
		if(userInfo != null){
			setUserInfoDetails(userInfo);
			Preferences.getInstance().setCheckRelogin(userInfo.getId(), "0");
			getContext().sendBroadcast(new Intent(AppBroadcast.ACTION_USER_LOGIN_STATE_CHANGE));
		}
	}
	
	public void logout(){
		setUserInfoDetails(null);
		UserData.logout();
		getContext().sendBroadcast(new Intent(AppBroadcast.ACTION_USER_LOGIN_STATE_CHANGE));
	}
	
	public boolean isGetUserInfoKey(String key){
		return key.equals(mGetUserInfoKey);
	}
	
	public void getUserInfo(String uid){
		UserInfoRequest request = new UserInfoRequest();
		mGetUserInfoKey = request.getDataKey();
		startTask(request);
	}

	public void resetPassword(String phone, String password, String validateCode) {
		ResetPassowrdRequest request = new ResetPassowrdRequest(phone, password, validateCode);
		mResetPasswordKey = request.getDataKey();
		startTask(request);
	}

	public boolean isResetPasswordKey(String key){
		return key.equals(mResetPasswordKey);
	}

	public void modifyNickname(String nickname) {
		ModifyNicknameRequest request = new ModifyNicknameRequest(nickname);
		mModifyNicknameKey = request.getDataKey();
		startTask(request);
	}

	public boolean isModifyNicknameKey(String key){
		return key.equals(mModifyNicknameKey);
	}

	public void modifyUserIcon(String icon) {
		ModifyUserIconRequest request = new ModifyUserIconRequest(icon);
		mModifyUserIconKey = request.getDataKey();
		startTask(request);
	}

	public boolean isModifyUserIconeKey(String key){
		return key.equals(mModifyUserIconKey);
	}

	public void getImageUploadToken() {
		GetImageUploadTokenRequest request = new GetImageUploadTokenRequest();
		mGetImageUploadTokenKey = request.getDataKey();
		startTask(request);
	}

	public boolean isGetImageUploadTokenKey(String key){
		return key.equals(mGetImageUploadTokenKey);
	}

	public void setUserInfoDetails(UserInfo userInfo){
		setUserInfo(userInfo);
		UserData.mUserInfo = userInfo;
	}
	
	public void setUserInfo(UserInfo userInfo){
		if(userInfo == null){
			UserData.setName("");
			UserData.setUserIcon("");
			UserData.setPhone("");
			UserData.setUserId("");
			UserData.setUUid("");
			UserData.setRole(UserInfo.ROLE_USER);
			UserData.setLevel(0);
			UserData.setExperience(0);
			UserData.setPoint(0);
			UserData.setSignFlag(0);
			UserData.setDefaultIconFlag(1);

			//个人信息
			UserData.setLocationCity("");
			UserData.setIdCard("");
			UserData.setMarryFlag("");
			UserData.setEducationLevel("");

			//身份信息
			UserData.setProfession("");
			UserData.setIncomeType("");
			UserData.setIncomeValue("");
			UserData.setCompanyType("");
			UserData.setCompanyWorkTime("");
			UserData.setMoreSixFundFlag("");
			UserData.setMoreSixSocialFlag("");

			//房产信息
			UserData.setHasCreditFlag("");
			UserData.setContainHouseType("");
			UserData.setContainCarType("");
			UserData.setSuccessLoanFlag("");
			UserData.setCreditStanding("");
		}else{
			UserData.setUserId(userInfo.getId());
			UserData.setName(userInfo.getName());
			UserData.setUserIcon(userInfo.getIcon());
			UserData.setPhone(userInfo.getPhone());
			UserData.setLevel(userInfo.getCurrentLevel());
			UserData.setExperience(userInfo.getExperience());
			UserData.setPoint(userInfo.getPoint());
			UserData.setSignFlag(userInfo.getSignFlag());
			UserData.setDefaultIconFlag(userInfo.getDefaultIconFlag());

			//个人信息
			UserData.setLocationCity(userInfo.getLocationCity());
			UserData.setIdCard(userInfo.getIdCard());
			UserData.setMarryFlag(userInfo.getMarryFlag());
			UserData.setEducationLevel(userInfo.getEducationLevel());

			//身份信息
			UserData.setProfession(userInfo.getProfession());
			UserData.setIncomeType(userInfo.getIncomeType());
			UserData.setIncomeValue(userInfo.getIncomeValue());
			UserData.setCompanyType(userInfo.getCompanyType());
			UserData.setCompanyWorkTime(userInfo.getCompanyWorkTime());
			UserData.setMoreSixFundFlag(userInfo.getMoreSixFundFlag());
			UserData.setMoreSixSocialFlag(userInfo.getMoreSixSocialFlag());

			//房产信息
			UserData.setHasCreditFlag(userInfo.getHasCreditFlag());
			UserData.setContainHouseType(userInfo.getContainHouseType());
			UserData.setContainCarType(userInfo.getContainCarType());
			UserData.setSuccessLoanFlag(userInfo.getSuccessLoanFlag());
			UserData.setCreditStanding(userInfo.getCreditStanding());
		}
		notifyDataChange();
	}

	public UserInfo getUserInfo(){
		UserInfo userInfo = new UserInfo();
		userInfo.setId(UserData.getUserId());
		userInfo.setName(UserData.getName());
		userInfo.setIcon(UserData.getUserIcon());
		userInfo.setPhone(UserData.getPhone());
		userInfo.setExperience(UserData.getExperience());
		userInfo.setPoint(UserData.getPoint());
		userInfo.setCurrentLevel(UserData.getLevel());
		userInfo.setSignFlag(UserData.getSignFlag());
		userInfo.setDefaultIconFlag(UserData.getDefaultIconFlag());

		//个人信息
		userInfo.setLocationCity(UserData.getLocationCity());
		userInfo.setIdCard(UserData.getIdCard());
		userInfo.setMarryFlag(UserData.getMarryFlag());
		userInfo.setEducationLevel(UserData.getEducationLevel());

		//身份信息
		userInfo.setProfession(UserData.getProfession());
		userInfo.setIncomeType(UserData.getIncomeType());
		userInfo.setIncomeValue(UserData.getIncomeValue());
		userInfo.setCompanyType(UserData.getCompanyType());
		userInfo.setCompanyWorkTime(UserData.getCompanyWorkTime());
		userInfo.setMoreSixFundFlag(UserData.getMoreSixFundFlag());
		userInfo.setMoreSixSocialFlag(UserData.getMoreSixSocialFlag());

		//房产信息
		userInfo.setHasCreditFlag(UserData.getHasCreditFlag());
		userInfo.setContainHouseType(UserData.getContainHouseType());
		userInfo.setContainCarType(UserData.getContainCarType());
		userInfo.setSuccessLoanFlag(UserData.getSuccessLoanFlag());
		userInfo.setCreditStanding(UserData.getCreditStanding());

		return userInfo;
	}
	
	private void notifyDataChange(){
		notifyDataChange(DATA_GROUP_KEY);
	}
	
	public boolean saveUserInfo(UserInfo userInfo){
		UserInfo temp = getUserInfo();
		if(!temp.getId().equals(userInfo.getId())){
			return false;
		}
		if(!temp.equals(userInfo)){
//			EditUserInfoRequest dataRequest = new EditUserInfoRequest(
//					userInfo.getName(),
//					userInfo.getSex(),
//					userInfo.getSex(),
//					userInfo.getCity(),
//					userInfo.getHobby(),
//					userInfo.getIcon(),
//					userInfo.getBrithday());
//			mEditUserInfoKey = dataRequest.getDataKey();
//			startTask(dataRequest);
			return true;
		}
		return false;
	}
	
	public void saveUserInfo(HashMap<String, Object> parameterMap){
		EditUserInfoRequest dataRequest = new EditUserInfoRequest(parameterMap);
		mEditUserInfoKey = dataRequest.getDataKey();
		startTask(dataRequest);
	}
	
	public boolean isEditUserInfoKey(String key){
		return key.equals(mEditUserInfoKey);
	}

	public void loadNetUserInfo(){
		UserInfoRequest request = new UserInfoRequest();
		mLoadNetUserInfoKey = request.getDataKey();
		startTask(request);
	}

	public boolean isLoadNetUserInfoKey(String key){
		return key.equals(mLoadNetUserInfoKey);
	}

	public boolean isLogin(String key){
		return key.equals(mLoginKey);
	}

	public boolean isRegister(String key){
		return key.equals(mRegisterKey);
	}

	public void loginTask(String userName,String password, String validateCode){
		LoginRequest loginRequest = new LoginRequest(userName, password, validateCode);
		mLoginKey = loginRequest.getDataKey();
		startTask(loginRequest);
	}

	public void register(String usetName, String password, String securityCode){
		RegisterRequest loginRequest = new RegisterRequest(usetName, password, securityCode);
		mRegisterKey = loginRequest.getDataKey();
		startTask(loginRequest);
	}

	/** 获取验证码
	 *
	 * @param phone
	 */
	public void getSecurityCode(String phone){
		SendSmsRequest request = new SendSmsRequest(phone);
		mIsSendSMSKey = request.getDataKey();
		startTask(request);
	}

	/**
	 *
	 * @param filePath 文件路径
	 * @param key 指定七牛服务上的文件名，或 null
	 * @param token 从服务端SDK获取
	 */
	public void uploadFile(String filePath, String key, String token) {
		Configuration config = new Configuration.Builder().build();
		UploadManager uploadManager = new UploadManager(config);
		mIsUploadFileKey = uploadManager.hashCode() + "";
		uploadManager.put(filePath, key, token,
				new UpCompletionHandler() {
					@Override
					public void complete(String key, ResponseInfo info, JSONObject res) {
						//res包含hash、key等信息，具体字段取决于上传策略的设置
						LogUtil.i("qiniu", key + ",\r\n " + info + ",\r\n " + res);

						if (info != null) {
							if (info.isOK()) {
								modifyUserIcon(key);
								return;
							}
						}
						dispatchFail(null, mIsUploadFileKey, 1);
					}
				}, null);
	}

	public boolean isUploadFile(String key) {
		return key.equals(mIsUploadFileKey);
	}
	
	@Override
	protected void dispatchSuccess(String key, Object data, DataResult<?> dataSource) {
		if(isLoadNetUserInfoKey(key)){
			UserInfoData userInfoData = (UserInfoData) data;
			UserInfo userInfo = userInfoData.getData();
			setUserInfoDetails(userInfo);
		} else if(key.equals(mGetUserInfoKey)) {
			UserInfoData userInfoData = (UserInfoData) data;
			data = userInfoData.getData();
		} else if(isModifyUserIconeKey(key)){
			UserInfoData userInfoData = (UserInfoData) data;
			UserInfo userInfo = userInfoData.getData();
			setUserInfoDetails(userInfo);
			getContext().sendBroadcast(new Intent(AppBroadcast.ACTION_USER_LOGIN_STATE_CHANGE));
		} else if (isLogin(key) || isRegister(key) || isResetPasswordKey(key)) {
			LoginInfoData loginInfoData = (LoginInfoData) data;
			LoginInfo loginInfo = loginInfoData.getData();
			UserData.setUserId(loginInfo.getUserId());
			UserData.setToken(loginInfo.getAuthorizationToken());
			UserData.setName(loginInfo.getDisplayName());
			loadNetUserInfo();
			getContext().sendBroadcast(new Intent(AppBroadcast.ACTION_USER_LOGIN_STATE_CHANGE));
		} else if (isEditUserInfoKey(key)) {
			getContext().sendBroadcast(new Intent(AppBroadcast.ACTION_USER_LOGIN_STATE_CHANGE));
		}
		super.dispatchSuccess(key, data, dataSource);
	}

	@Override
	protected void dispatchServerFail(Runnable retryTask, String key, Object data) {
		super.dispatchServerFail(retryTask, key, data);
	}

	public static boolean verifyUserNameEmpty(String userName) {
		if(TextUtils.isEmpty(userName)) {//用户名为空的提示
			ToastUtil.showToast(App.getInstance(), R.string.login_err_user_null);
			return true;
		}
		return false;
	}

	public static boolean verifySMSCodedEmpty(String smsCode) {
		if(TextUtils.isEmpty(smsCode)) {//密码为空的提示
			ToastUtil.showToast(App.getInstance(), R.string.login_err_smscode_null);
			return true;
		}
		return false;
	}

	public static boolean verifyUserPasswordEmpty(String password) {
		if(TextUtils.isEmpty(password)) {//密码为空的提示
			ToastUtil.showToast(App.getInstance(), R.string.login_err_psw_null);
			return true;
		}
		return false;
	}

	public static boolean verifyUserPassword(String password) {
		if((password.length() < 6) || (password.length() > 16)) {//密码位数不对的提示
			ToastUtil.showToast(App.getInstance(), R.string.login_err_psw_number);
			return false;
		}
		return true;
	}

	/** 登录校验用户名和密码
	 *
	 * @param userName
	 * @param password
	 * @return
	 */
	public static boolean checkLogin(String userName, String password) {
		if (verifyUserNameEmpty(userName) ||
				verifyUserPasswordEmpty(password) ||
				!verifyUserPassword(password)
				) {
			return false;
		}
		return true;
	}

}
