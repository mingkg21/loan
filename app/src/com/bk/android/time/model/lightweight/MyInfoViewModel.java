package com.bk.android.time.model.lightweight;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;

import com.bk.android.assistant.R;
import com.bk.android.binding.command.OnClickCommand;
import com.bk.android.data.DataResult;
import com.bk.android.time.data.UserData;
import com.bk.android.time.entity.UserInfo;
import com.bk.android.time.model.BaseDataModel;
import com.bk.android.time.model.BaseNetDataViewModel;
import com.bk.android.time.ui.INetLoadView;
import com.bk.android.time.ui.common.PopupListDialog;
import com.bk.android.time.util.ToastUtil;
import com.bk.android.time.util.UserInfoUtil;
import com.bk.android.util.CommonUtil;
import com.zaaach.citypicker.CityPickerActivity;

import java.util.HashMap;

import gueei.binding.observables.StringObservable;

import static android.app.Activity.RESULT_OK;

/**
 *
 */
public class MyInfoViewModel extends BaseNetDataViewModel {

	public final StringObservable bName = new StringObservable();
	public final StringObservable bPhone = new StringObservable();
	public final StringObservable bIDCard = new StringObservable();
	public final StringObservable bCity = new StringObservable();
	public final StringObservable bMarriage = new StringObservable();
	public final StringObservable bEducation = new StringObservable();

	public final OnClickCommand bEducationClickCommand = new OnClickCommand() {
		@Override
		public void onClick(View v) {
			PopupListDialog dialog = new PopupListDialog(getContext(), R.array.education, new PopupListDialog.OnItemSelectedListener() {
				@Override
				public void onItemSelected(int position, String str) {
					bEducation.set(str);
				}
			});
			dialog.show();
		}
	};

	public final OnClickCommand bMarriageClickCommand = new OnClickCommand() {
		@Override
		public void onClick(View v) {
			PopupListDialog dialog = new PopupListDialog(getContext(), R.array.marriage, new PopupListDialog.OnItemSelectedListener() {
				@Override
				public void onItemSelected(int position, String str) {
					bMarriage.set(str);
				}
			});
			dialog.show();
		}
	};

	public final OnClickCommand bCityClickCommand = new OnClickCommand() {
		@Override
		public void onClick(View v) {
			CityPickerActivity.openActivity((Activity) getContext());
		}
	};

	public final OnClickCommand bSaveClickCommand = new OnClickCommand() {
		@Override
		public void onClick(View v) {
			if (TextUtils.isEmpty(bName.get())) {
				ToastUtil.showToast(getContext(), "姓名不能为空哦！");
				return;
			}
			if (TextUtils.isEmpty(bPhone.get())) {
				ToastUtil.showToast(getContext(), "请输入手机号！");
				return;
			}
			if (!CommonUtil.isPhoneNumber(bPhone.get())) {
				ToastUtil.showToast(getContext(), "请输入正确的手机号！");
				return;
			}
			if (TextUtils.isEmpty(bIDCard.get())) {
				ToastUtil.showToast(getContext(), "请输入身份证！");
				return;
			}
			if (TextUtils.isEmpty(bCity.get())) {
				ToastUtil.showToast(getContext(), "请选择所在城市！");
				return;
			}
			if (TextUtils.isEmpty(bMarriage.get())) {
				ToastUtil.showToast(getContext(), "请选择婚姻情况！");
				return;
			}
			if (TextUtils.isEmpty(bEducation.get())) {
				ToastUtil.showToast(getContext(), "请选择文化程度！");
				return;
			}
			if ((bIDCard.get().length() != 18) && (bIDCard.get().length() != 15)) {
				ToastUtil.showToast(getContext(), "请输入正确的身份证！");
				return;
			}
			HashMap<String, Object> parameterMap = new HashMap<>();
			parameterMap.put("phone", bPhone.get());
			parameterMap.put("realName", bName.get());
			parameterMap.put("idCard", bIDCard.get());
			parameterMap.put("locationCity", bCity.get());
			parameterMap.put("marryFlag", UserInfoUtil.marryFlagStr(bMarriage.get()));
			parameterMap.put("educationLevel",UserInfoUtil.educationLevelStr(bEducation.get()));

			mUserInfoModel.saveUserInfo(parameterMap);
		}
	};

	private UserInfoModel mUserInfoModel;

	public MyInfoViewModel(Context context, INetLoadView loadView) {
		super(context, loadView);
		mUserInfoModel = new UserInfoModel();
		mUserInfoModel.addCallBack(this);

	}

	@Override
	protected boolean canClearLoadAndRetryView() {
		return true;
	}
	
	@Override
	public boolean onPreLoad(String key, int state) {
		if (mUserInfoModel.isEditUserInfoKey(key)) {
			showLoadDialog();
			return false;
		}
		return false;
	}
	
	@Override
	public boolean onPreBackgrounLoad(String key, int state) {
		return false;
	}
	
	@Override
	public boolean onPostLoad(String key, int state) {
		if (mUserInfoModel.isEditUserInfoKey(key)) {
			hideLoadDialog();
			return false;
		}
		return false;
	}
	
	@Override
	public boolean onSuccess(String key, Object data, DataResult<?> dataSource) {
		if (mUserInfoModel.isEditUserInfoKey(key)) {
			ToastUtil.showToast(getContext(), "编辑个人信息成功！");
			UserData.mUserInfo.setPhone(bPhone.get());
			UserData.mUserInfo.setRealName(bName.get());
			UserData.mUserInfo.setIdCard(bIDCard.get());
			UserData.mUserInfo.setLocationCity(bCity.get());
			UserData.mUserInfo.setMarryFlag(UserInfoUtil.marryFlagStr(bMarriage.get()));
			UserData.mUserInfo.setEducationLevel(UserInfoUtil.educationLevelStr(bEducation.get()));
			return true;
		}
		return super.onSuccess(key, data, dataSource);
	}
	
	@Override
	public boolean onDataChange(String groupKey,BaseDataModel dataModel) {
		if(UserInfoModel.DATA_GROUP_KEY.equals(groupKey)){
			refresh();
		}
		return super.onDataChange(groupKey,dataModel);
	}

	@Override
	public void onUserLoginStateChange(boolean isLogin) {
		refresh();
	}
	
	private void setUserInfo(){
		UserInfo userInfo = UserData.mUserInfo;
		String name = userInfo.getRealName();
		if(!TextUtils.isEmpty(name)) {
			bName.set(name);
		} else {
			bName.set("");
		}
		bPhone.set(userInfo.getPhone());
		bIDCard.set(userInfo.getIdCard());
		bCity.set(userInfo.getLocationCity());
		bEducation.set(UserInfoUtil.educationLevel(userInfo.getEducationLevel()));
		bMarriage.set(UserInfoUtil.marryFlag(userInfo.getMarryFlag()));
	}
	
	public void refresh(){
		if(UserData.getUserId() == UserData.DESIRE_USER_ID){
			bName.set(getString(R.string.user_center_name_login_tip));
		}else{
			setUserInfo();
		}
	}

	@Override
	public void onStart() {
		refresh();
	}

	@Override
	public void onRelease() {
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == CityPickerActivity.REQUEST_CODE_PICK_CITY && resultCode == RESULT_OK){
			if (data != null){
				String city = data.getStringExtra(CityPickerActivity.KEY_PICKED_CITY);
				bCity.set(city);
			}
		}
	}
}
