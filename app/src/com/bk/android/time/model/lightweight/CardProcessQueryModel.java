package com.bk.android.time.model.lightweight;

import com.bk.android.time.data.request.net.CardProcessQueryRequest;
import com.bk.android.time.model.BaseDataModel;

public class CardProcessQueryModel extends BaseDataModel{

	private String mBankQueryKey;

	public boolean isCardProcessQueryKey(String key){
		return key.equals(mBankQueryKey);
	}
	
	public void cardProcessQuery(){
		CardProcessQueryRequest request = new CardProcessQueryRequest();
		mBankQueryKey = request.getDataKey();
		startTask(request);
	}
}
