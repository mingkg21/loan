package com.bk.android.time.model.lightweight;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import com.bk.android.assistant.R;
import com.bk.android.binding.command.OnClickCommand;
import com.bk.android.data.DataResult;
import com.bk.android.time.model.BaseNetDataViewModel;
import com.bk.android.time.ui.INetLoadView;
import com.bk.android.time.ui.activiy.ActivityChannels;
import com.bk.android.time.util.ResUtil;
import com.bk.android.time.util.ToastUtil;

import gueei.binding.observables.BooleanObservable;
import gueei.binding.observables.StringObservable;

/** 登陆界面ViewModel
 * @author mingkg21
 * @date 2017年4月13日
 */
public class LoginViewModel extends BaseNetDataViewModel {

	public static final String TAG = "LoginViewModel";

	public final StringObservable bUserNameText = new StringObservable();
	public final StringObservable bPasswordNameText = new StringObservable();
	public final StringObservable bSMSCodeText = new StringObservable();
	public final BooleanObservable bIsBySNS = new BooleanObservable(false);
	public final BooleanObservable bIsRegister = new BooleanObservable(false);

	public final OnClickCommand bGotoSignUpCommand = new OnClickCommand() {
		@Override
		public void onClick(View v) {
			ActivityChannels.openRegisterActivity(getContext());
		}
	};

	public final OnClickCommand bGotoForgetPasswordClickCimmand = new OnClickCommand() {
		@Override
		public void onClick(View v) {
			ActivityChannels.openForgetPasswordActivity(getContext());
		}
	};
	
	/** 登陆的commend
	 * 
	 */
	public final OnClickCommand bSignInClickCommand = new OnClickCommand() {
		@Override
		public void onClick(View v) {
			String userName = bUserNameText.get();
			String password = bPasswordNameText.get();
			String validateCode = bSMSCodeText.get();

			if(TextUtils.isEmpty(userName)) {//用户名为空的提示
				ToastUtil.showToast(getContext(), R.string.login_err_user_null);
				return;
			}
			if (bIsBySNS.get()) {
				if(TextUtils.isEmpty(validateCode)) {//密码为空的提示
					ToastUtil.showToast(getContext(), R.string.login_err_smscode_null);
					return;
				}
			} else {
				if(TextUtils.isEmpty(password)) {//密码为空的提示
					ToastUtil.showToast(getContext(), R.string.login_err_psw_null);
					return;
				}
				if((password.length() < 6) || (password.length() > 16)) {//密码位数不对的提示
					ToastUtil.showToast(getContext(), R.string.login_err_psw_number);
					return;
				}
			}
			mUserInfoModel.loginTask(userName, password, validateCode);
		}
	};
	
	/** 注册的commend
	 * 
	 */
	public final OnClickCommand bSignUpClickCommand = new OnClickCommand() {
		@Override
		public void onClick(View v) {
			String userName = bUserNameText.get();
			String password = bPasswordNameText.get();
			String smsCode = bSMSCodeText.get();
			if(canLogin(userName, password)) {
				if (TextUtils.isEmpty(smsCode)) {
					ToastUtil.showToast(getContext(), "请输入验证码！");
					return;
				}
				mUserInfoModel.register(userName, password, smsCode);
			}
		}
	};
	
	public static final String ACTION_LOGIN_WX = "action_login_wx";

	private UserInfoModel mUserInfoModel;
	private boolean mIsFromGuide;

	public LoginViewModel(Context context, INetLoadView loadView, boolean isRegister) {
		super(context, loadView);
		mUserInfoModel = new UserInfoModel();
		mUserInfoModel.addCallBack(this);

		bIsRegister.set(isRegister);
	}

	public void setBySMSLogin(boolean isBySms) {
		bIsBySNS.set(isBySms);
	}
	
	private boolean canLogin(String userName, String password) {
		if(!TextUtils.isEmpty(userName) && !TextUtils.isEmpty(password)) {
//			if(!CommonUtil.checkEmail(userName) && !CommonUtil.isPhoneNumber(userName)) {//邮箱格式不对的提示
//				ToastUtil.showToast(getContext(), R.string.login_err_email_format);
//				return false;
//			}
			if((password.length() < 6) || (password.length() > 16)) {//密码位数不对的提示
				ToastUtil.showToast(getContext(), R.string.login_err_psw_number);
				return false;
			}
		} else {
			if(TextUtils.isEmpty(userName)) {//用户名为空的提示
				ToastUtil.showToast(getContext(), R.string.login_err_user_null);
				return false;
			} 
			if(TextUtils.isEmpty(password)) {//密码为空的提示
				ToastUtil.showToast(getContext(), R.string.login_err_psw_null);
				return false;
			}
		}
		return true;
	}
	
	@Override
	public boolean onPostLoad(String key, int state) {
		hideLoadDialog();
		return false;
	}

	@Override
	public boolean onPreLoad(String key, int state) {
		showLoadDialog();
		return false;
	}
	
	@Override
	protected boolean needRetryTask(String key) {
		return false;
	}

	@Override
	public boolean onSuccess(String key, Object data, DataResult<?> dataSource) {
		if(mUserInfoModel.isLogin(key)){
			ToastUtil.showToast(getContext(), ResUtil.STR_TIP_LOGIN_SUCCEED);
		}else if(mUserInfoModel.isRegister(key)){
			ToastUtil.showToast(getContext(), ResUtil.STR_TIP_REGISTER_SUCCEED);
		}
		return super.onSuccess(key, data, dataSource);
	}
	
	@Override
	public void onStart() {
	}

	@Override
	public void onRelease() {
	}
	
	@Override
	protected boolean canClearLoadAndRetryView() {
		return true;
	}
	
	public interface CallBask{
		public void onLoginFinsh(com.bk.android.time.entity.UserInfo userInfo);
	}
}
