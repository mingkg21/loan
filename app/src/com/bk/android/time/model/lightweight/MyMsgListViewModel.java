package com.bk.android.time.model.lightweight;

import android.content.Context;

import com.bk.android.assistant.R;
import com.bk.android.time.data.request.net.MsgListRequest;
import com.bk.android.time.entity.MessageInfo;
import com.bk.android.time.entity.MessageInfoListData;
import com.bk.android.time.model.common.PagingLoadModel;
import com.bk.android.time.ui.INetLoadView;
import com.bk.android.time.ui.activiy.ActivityChannels;

import java.io.Serializable;
import java.util.ArrayList;

import gueei.binding.collections.ArrayListObservable;
import gueei.binding.observables.StringObservable;

/**
 * Created by mingkg21 on 2017/4/9.
 */

public class MyMsgListViewModel extends BaseListViewModel {

    public final ArrayListObservable<ItemViewModel> bItems = new ArrayListObservable<ItemViewModel>(ItemViewModel.class);

    public MyMsgListViewModel(Context context, INetLoadView loadView) {
        super(context, loadView);

    }

    @Override
    protected int initItemTemplate() {
        return R.layout.uniq_my_msg_msg_item_lay;
    }

    @Override
    protected ArrayListObservable<?> getItems() {
        return bItems;
    }

    @Override
    protected void initItemsData(Serializable data) {
        ArrayListObservable<ItemViewModel> temps = new ArrayListObservable<ItemViewModel>(ItemViewModel.class);

        MessageInfoListData messageInfoListData = (MessageInfoListData) data;
        ArrayList<MessageInfo> infos = messageInfoListData.getData();
        for (MessageInfo info : infos) {
            temps.add(new ItemViewModel(info));
        }
        bItems.setAll(temps);
    }

    @Override
    protected void onItemClick(Object item, int position) {
        ItemViewModel itemViewModel = (ItemViewModel) item;
        ActivityChannels.openCommonWebActivity(getContext(), itemViewModel.mDatasource.getUrl());
    }

    @Override
    protected PagingLoadModel initPagingLoadModel() {
        return new MessageModel(MsgListRequest.MSG_TYPE_SYSTEM);
    }

    public class ItemViewModel {

        public MessageInfo mDatasource;

        public final StringObservable bIconUrl = new StringObservable();
        public final StringObservable bName = new StringObservable();
        public final StringObservable bContent = new StringObservable();
        public final StringObservable bTime = new StringObservable();

        public ItemViewModel(MessageInfo info) {
            mDatasource = info;
            bName.set(info.getTitle());
            bIconUrl.set(info.getCover());
            bContent.set(info.getContent());
            bTime.set(info.getTime());
        }

    }
}
