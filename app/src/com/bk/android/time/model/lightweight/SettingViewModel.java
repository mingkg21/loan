package com.bk.android.time.model.lightweight;

import android.app.Activity;
import android.content.Context;
import android.view.View;

import com.bk.android.assistant.R;
import com.bk.android.binding.command.OnClickCommand;
import com.bk.android.data.DataResult;
import com.bk.android.time.app.App;
import com.bk.android.time.data.Preferences;
import com.bk.android.time.data.UserData;
import com.bk.android.time.data.net.WebConfig;
import com.bk.android.time.model.BaseDialogViewModel;
import com.bk.android.time.model.BaseDialogViewModel.OnBtnClickCallBack;
import com.bk.android.time.model.BaseNetDataViewModel;
import com.bk.android.time.model.common.CommonDialogViewModel;
import com.bk.android.time.model.common.ShareDialogViewModel;
import com.bk.android.time.thridparty.ShareEntity;
import com.bk.android.time.ui.INetLoadView;
import com.bk.android.time.ui.activiy.ActivityChannels;
import com.bk.android.time.ui.common.WXShareDialog;
import com.bk.android.time.update.AppUpdate;
import com.bk.android.time.util.ToastUtil;
import com.bk.android.util.AppMarketUtil;
import com.bk.android.util.AppUtil;

import gueei.binding.observables.BooleanObservable;
import gueei.binding.observables.StringObservable;

/**
 * 设置界面ViewModel
 *
 * @author mingkg21
 * @email mingkg21@gmail.com
 * @date 2014年3月29日
 */
public class SettingViewModel extends BaseNetDataViewModel {

    public final static String LOGOUT_TIP_DIALOG = "LOGOUT_TIP_DIALOG";

    public final BooleanObservable bHasNewFeedback = new BooleanObservable(false);
    public final BooleanObservable bSwitchServerVisibility = new BooleanObservable(false);
    public final BooleanObservable bHasShare = new BooleanObservable(true);
    public final StringObservable bSwitchServerText = new StringObservable();
    public final BooleanObservable bIsLogin = new BooleanObservable(false);
    public final BooleanObservable bSubmitLogVisibility = new BooleanObservable("interiorTest".equals(App.getInstance().getUMengChannel()));

    public final OnClickCommand bSwitchServerClickCommand = new OnClickCommand() {
        @Override
        public void onClick(View v) {
            boolean isNormalServer = !Preferences.getInstance().isNormalServer();
            AppUtil.clearAllAppData(getContext());
            Preferences.getInstance().setNormaServer(isNormalServer);
            Preferences.getInstance().setSwitchNormalServerVisibility();
            setNormalServer();
            App.getInstance().closeApp();
            ToastUtil.showLongToast(getContext(), "设置成功，请重新打开！");
            post(new Runnable() {
                @Override
                public void run() {
                    System.exit(0);
                }
            }, 500);
        }
    };

    public final OnClickCommand bClearDataClickCommand = new OnClickCommand() {
        @Override
        public void onClick(View v) {
            AppUtil.clearAllAppData(getContext());
            ToastUtil.showToast(getContext(), "缓存清除成功！");
        }
    };

    public final OnClickCommand bFeedbackClickCommand = new OnClickCommand() {
        @Override
        public void onClick(View v) {
            ActivityChannels.openFeedbackActivity(getContext());
        }
    };

    public final OnClickCommand bEvaluateClickCommand = new OnClickCommand() {
        @Override
        public void onClick(View v) {
            AppMarketUtil.gotoMarket(getContext());
        }
    };

    public final OnClickCommand bShareAppClickCommand = new OnClickCommand() {
        @Override
        public void onClick(View v) {
//			Runnable runnable = new Runnable() {
//				@Override
//				public void run() {
//					ActivityChannels.openCommonWebActivity(getContext(), WebConfig.getInstance().getLogrecord(UserData.getUserId()));
//				}
//			};
//			checkLogin(runnable);
            ShareEntity entity = new ShareEntity();
            entity.title = "贷款花，快速借贷好帮手";
            entity.summary = "贷款花，快速借贷好帮手";
            entity.webUrl = "http://daikuanhua.com";
            ShareDialogViewModel dialogViewModel = new ShareDialogViewModel(getContext(), entity);
            new WXShareDialog(getContext(), dialogViewModel);
            dialogViewModel.setCancelable(true);
            dialogViewModel.setCanceledOnTouchOutside(true);
            dialogViewModel.show();
        }
    };

    public final OnClickCommand bAboutClickCommand = new OnClickCommand() {
        @Override
        public void onClick(View v) {
            ActivityChannels.openAboutActivity(getContext());
        }
    };

    public final OnClickCommand bContactUsClickCommand = new OnClickCommand() {
        @Override
        public void onClick(View v) {
            ActivityChannels.openContactUsActivity(getContext());
        }
    };

    public final OnClickCommand bCheckUpdateClickCommand = new OnClickCommand() {
        @Override
        public void onClick(View v) {
            AppUpdate.checkUpdateFromUser((Activity) getContext());
        }
    };

    public final OnClickCommand bMessageSettingClickCommand = new OnClickCommand() {
        @Override
        public void onClick(View v) {
        }
    };

    public final OnClickCommand bFAQClickCommand = new OnClickCommand() {
        @Override
        public void onClick(View v) {
            ActivityChannels.openCommonWebActivity(getContext(), WebConfig.getInstance().getFAQUrl());
        }
    };

    public final OnClickCommand bLogoutClickCommand = new OnClickCommand() {
        @Override
        public void onClick(View v) {
            CommonDialogViewModel dialog = (CommonDialogViewModel) bindDialogViewModel(LOGOUT_TIP_DIALOG, null);
            dialog.setLeftBtnClickCallBack(new OnBtnClickCallBack() {
                @Override
                public void onClick(View v, BaseDialogViewModel viewModel) {
                    mUserInfoModel.logout();
                    viewModel.finish();

                    showLoginView(null);
                }
            });
            dialog.show();
        }
    };

    private UserInfoModel mUserInfoModel;

    public SettingViewModel(Context context, INetLoadView loadView) {
        super(context, loadView);

        mUserInfoModel = new UserInfoModel();
        mUserInfoModel.addCallBack(this);//用于接收用户信息修改通知

        bHasShare.set(!getContext().getResources().getBoolean(R.bool.is_copy));

        refreshNewFeedback();
    }

    @Override
    public void onStart() {
        refresh();
    }

    public void onResume() {
        setNormalServer();
        bSwitchServerVisibility.set(false);
        if (Preferences.getInstance().isSwitchNormalServerVisibility()) {
            bSwitchServerVisibility.set(true);
        }
    }

    @Override
    public void onRelease() {
    }

    @Override
    protected boolean canClearLoadAndRetryView() {
        return true;
    }

    public void refresh() {
        if (UserData.getUserId() == UserData.DESIRE_USER_ID) {
            bIsLogin.set(false);
        } else {
            bIsLogin.set(true);
        }
    }

    @Override
    public void onUserLoginStateChange(boolean isLogin) {
        refresh();
    }

    @Override
    public boolean onSuccess(String key, Object data, DataResult<?> dataSource) {
        return super.onSuccess(key, data, dataSource);
    }

    public void refreshNewFeedback() {
    }

    private void setNormalServer() {
        if (Preferences.getInstance().isNormalServer()) {
            bSwitchServerText.set("正式服务器");
        } else {
            bSwitchServerText.set("测试服务器");
        }
    }
}
