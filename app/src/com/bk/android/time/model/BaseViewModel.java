package com.bk.android.time.model;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;

import com.bk.android.time.app.observer.IAppContextObserver;
import com.bk.android.time.ui.IView;
/**
 * 所有ViewModel基类
 * @author linyiwei
 *
 * @param <CallBack>
 */
public abstract class BaseViewModel extends BaseModel<IView> implements IView,IAppContextObserver{
	public static final String WAITING_DIALOG = "WAITING_DIALOG";
	public static final String TRAFFIC_WARNING_DIALOG = "TRAFFIC_WARNING_DIALOG";
	public static final String NOT_NET_DIALOG = "NOT_NET_DIALOG";

	protected BaseViewModel this_ = this;
	private Context mContext;
	private IView mLoadView;

	public BaseViewModel(Context context,IView loadView){
		mContext = context;
		mLoadView = loadView;
		if(loadView != null){
			super.addCallBack(loadView);
			loadView.addViewContextObservable(this);
		}
	}
	
	@Override
	public Context getContext() {
		return mContext;//返回Activity的Context，父类返回的是APP
	}
	
	protected IView getCallBack(){
		WeakReference<IView> weak = super.getCallBacks().get(0);
		if(weak != null){
			return weak.get();
		}
		return null;
	}
	
	@Deprecated
	@Override
	public final void addCallBack(IView l) {}
	
	@Deprecated
	@Override
	protected final ArrayList<WeakReference<IView>> getCallBacks() {
		return null;
	}

	@Deprecated
	@Override
	public final void traversalCallBacks(CallBackHandler<IView> handler) {}
	
	/**
	 * ViewModel生命周期的开始
	 */
	public abstract void onStart();
	/**
	 * ViewModel生命周期的结束
	 */
	public abstract void onRelease();

	@Override
	public void finish() {
		IView view = getCallBack();
		if(view != null){
			try {
				view.finish();
			} catch (Exception e) {e.printStackTrace();}
		}
	}

	@Override
	public void addViewContextObservable(IAppContextObserver contextCallBack) {
		IView view = getCallBack();
		if(view != null){
			try {
				view.addViewContextObservable(contextCallBack);
			} catch (Exception e) {e.printStackTrace();}
		}
	}

	@Override
	public BaseDialogViewModel bindDialogViewModel(String type,Object viewModel, Object... extraParams) {
		IView view = getCallBack();
		if(view != null){
			try {
				return view.bindDialogViewModel(type,viewModel,extraParams);
			} catch (Exception e) {e.printStackTrace();}
		}
		return null;
	}
	
    public boolean isActivity() {
		return mLoadView instanceof Activity;
    }
    
    public boolean isFinished() {
    	boolean isFinished = false;
    	if(mContext instanceof Activity){
    		isFinished = ((Activity) mContext).isFinishing();
    	}
    	if(!isFinished && mLoadView instanceof Fragment){
    		isFinished = !((Fragment) mLoadView).isAdded();
    	}
		return isFinished;
    }

	@Override
	public void onLoadTheme() {
	}

	@Override
	public void onUserLoginStateChange(boolean isLogin) {
	}

	@Override
	public void onNetworkChange(boolean isAvailable) {
	}
	
	@Override
	public void onLanguageChange() {
	}
}
