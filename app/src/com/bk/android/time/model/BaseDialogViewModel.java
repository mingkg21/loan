package com.bk.android.time.model;

import android.content.Context;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnDismissListener;
import android.content.DialogInterface.OnShowListener;
import android.view.View;

import com.bk.android.time.ui.IDialogView;
import com.bk.android.time.ui.IView;
/**
 * Dialog的ViewModel基类
 * @author linyiwei
 *
 */
public class BaseDialogViewModel extends BaseViewModel implements IDialogView{
	private IDialogView mDialogView;
	public BaseDialogViewModel(Context context) {
		super(context,null);
	}

	public void bindDialog(IDialogView dialogView){
		mDialogView = dialogView;
	}
	
	@Override
	protected IView getCallBack() {
		return mDialogView;
	}
	
	@Override
	public void show() {
		if(!isFinished()){
			IDialogView view = (IDialogView) getCallBack();
			if(view != null){
				view.show();
			}
		}
	}

	@Override
	public boolean isShowing() {
		IDialogView view = (IDialogView) getCallBack();
		if(view != null){
			return view.isShowing();
		}
		return false;
	}


	@Override
	public void cancel() {
		IDialogView view = (IDialogView) getCallBack();
		if(view != null){
			view.cancel();
		}
	}
	
	@Override
	public void setOnCancelListener(OnCancelListener listener) {
		IDialogView view = (IDialogView) getCallBack();
		if(view != null){
			view.setOnCancelListener(listener);
		}
	}

	@Override
	public void setOnDismissListener(OnDismissListener listener) {
		IDialogView view = (IDialogView) getCallBack();
		if(view != null){
			view.setOnDismissListener(listener);
		}
	}

	@Override
	public void setOnShowListener(OnShowListener listener) {
		IDialogView view = (IDialogView) getCallBack();
		if(view != null){
			view.setOnShowListener(listener);
		}
	}
	
	@Override
	public void setCancelable(boolean cancel) {
		IDialogView view = (IDialogView) getCallBack();
		if(view != null){
			view.setCancelable(cancel);
		}
	}

	@Override
	public void setCanceledOnTouchOutside(boolean cancel) {
		IDialogView view = (IDialogView) getCallBack();
		if(view != null){
			view.setCanceledOnTouchOutside(cancel);
		}
	}

	public interface OnBtnClickCallBack{
		public void onClick(View v,BaseDialogViewModel viewModel);
	}

	@Override
	public final void onStart() {
	}

	@Override
	public final void onRelease() {
	}
}
