package com.bk.android.time.model;

import android.content.Context;
import android.content.DialogInterface.OnDismissListener;

import com.bk.android.data.DataResult;
import com.bk.android.time.model.BaseDataModel.ILoadCallBack;
import com.bk.android.time.model.common.WaitingDialogViewModel;
import com.bk.android.time.ui.ILoadView;
/**
 * 加载非网络数据ViewModel基类
 * @author linyiwei
 *
 */
public abstract class BaseDataViewModel extends BaseViewModel implements ILoadView,ILoadCallBack{
	private WaitingDialogViewModel mLoadDialog;

	public BaseDataViewModel(Context context,ILoadView loadView) {
		super(context,loadView);
	}

	@Override
	public void showLoadView(){
		ILoadView loadView = (ILoadView) getCallBack();
		if(loadView != null){
			try {
				loadView.showLoadView();
			} catch (Exception e) {e.printStackTrace();}
		}
	};
	
	@Override
	public void hideLoadView(){
		ILoadView loadView = (ILoadView) getCallBack();
		if(loadView != null){
			try {
				loadView.hideLoadView();
			} catch (Exception e) {e.printStackTrace();}
		}
	}

	public void setLoadDialogTip(String tip){
		if(mLoadDialog != null){
			mLoadDialog.setTip(tip);
		}
	}
	
	@Override
	public void updateProgress(int progress, int maxProgress, int msgFormat) {
		if(isLoadDialogShow()){
			mLoadDialog.updateProgress(progress, maxProgress, msgFormat);
		}
		ILoadView loadView = (ILoadView) getCallBack();
		if(loadView != null){
			try {
				loadView.updateProgress(progress, maxProgress, msgFormat);
			} catch (Exception e) {e.printStackTrace();}
		}
	}
	
	@Override
	public boolean onPreLoad(String key, int state) {
		showLoadView();
		return false;
	}

	@Override
	public boolean onPostLoad(String key, int state) {
		hideLoadView();
		return false;
	}

	@Override
	public boolean onRequestProgressUpdate(String key, int progress,int maxProgress) {
		return false;
	}

	@Override
	public boolean onPreBackgrounLoad(String key, int state) {
		return false;
	}

	protected void showLoadDialog(){
		showLoadDialog(true,null);
	}
	
	protected void showLoadDialog(boolean cancel){
		showLoadDialog(cancel,null);
	}
	
	protected void showLoadDialog(final boolean cancel,final OnDismissListener listener){
		Runnable run = new Runnable() {
			@Override
			public void run() {
				if(!isLoadDialogShow()){
					mLoadDialog = (WaitingDialogViewModel) bindDialogViewModel(WAITING_DIALOG, null);
					if(mLoadDialog != null){
						mLoadDialog.setOnDismissListener(listener);
						mLoadDialog.setCancelable(cancel);
						mLoadDialog.show();
					}
				}
			}
		};
		runOnUiThread(run);
	}
	
	protected void hideLoadDialog(){
		Runnable run = new Runnable() {
			@Override
			public void run() {
				if(isLoadDialogShow()){
					mLoadDialog.setOnDismissListener(null);
					mLoadDialog.finish();
				}
			}
		};
		runOnUiThread(run);
	}
	
	protected boolean isLoadDialogShow(){
		return mLoadDialog != null && mLoadDialog.isShowing();
	}
	
	@Override
	public boolean onSuccess(String key, Object data, DataResult<?> dataSource) {
		return false;
	}
	
	@Override
	public boolean onFail(Runnable retryTask, String key, int state) {
		return false;
	}
	
	@Override
	public boolean onServerFail(Runnable retryTask, String key, Object data) {
		return false;
	}
	
	@Override
	public boolean onDataChange(String groupKey,BaseDataModel dataModel) {
		return false;
	}
}
