package com.bk.android.time.model;

import android.content.Context;
import android.text.TextUtils;

import com.bk.android.assistant.R;
import com.bk.android.data.DataResult;
import com.bk.android.data.ServiceProxy;
import com.bk.android.time.data.UserData;
import com.bk.android.time.entity.BaseEntity;
import com.bk.android.time.ui.INetLoadView;
import com.bk.android.time.util.ResUtil;
import com.bk.android.time.util.ToastUtil;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

public abstract class BaseNetDataViewModel extends BaseDataViewModel implements INetLoadView{
	private ArrayList<Runnable> mNotNetRetryTasks;
	private ArrayList<Runnable> mLoginRetryTasks;
	private ArrayList<Runnable> mFailRetryTasks;

	private WeakReference<RetryTaskRunnable> mLoginTaskRunnable;
	private WeakReference<RetryTaskRunnable> mFailTaskRunnable;

	public BaseNetDataViewModel(Context context, INetLoadView loadView) {
		super(context, loadView);
		mNotNetRetryTasks = new ArrayList<Runnable>();
		mLoginRetryTasks = new ArrayList<Runnable>();
		mFailRetryTasks = new ArrayList<Runnable>();
		mLoginTaskRunnable = new WeakReference<RetryTaskRunnable>(null);
		mFailTaskRunnable = new WeakReference<RetryTaskRunnable>(null);
	}

	@Override
	public boolean onPreLoad(String key, int state) {
		setLoadingViewTransparent(isLoadingViewTransparent());
		return super.onPreLoad(key, state);
	}

	@Override
	public boolean onPostLoad(String key, int state) {
		setLoadingViewTransparent(isLoadingViewTransparent());
		return super.onPostLoad(key, state);
	}

	@Override
	public boolean onFail(Runnable retryTask, String key, int state) {
		if(retryTask != null){
			setLoadingViewTransparent(isLoadingViewTransparent());
			if(state == ServiceProxy.REQUEST_STATE_FAILURE_NO_NET){
				onHandleNotNetStartFail(key);
				if(needRetryTask(key)){
					mNotNetRetryTasks.add(retryTask);
				}
			}else if(state == ServiceProxy.REQUEST_STATE_FAILURE_NO_LOGIN){
				showLoginView(retryTask);
			}else{
				if(needRetryTask(key)){
					showRetryView(retryTask);
				}else{
					ToastUtil.showToast(getContext(), ResUtil.STR_TIP_ERR_SERVER);
				}
			}
		}
		return super.onFail(retryTask, key, state);
	}
	
	@Override
	public boolean onServerFail(Runnable retryTask, String key, Object data) {
		setLoadingViewTransparent(isLoadingViewTransparent());
//		if(needRetryTask(key)){
//			showRetryView(retryTask);
//		}else{
			toastFail(data);
//		}
		return super.onServerFail(retryTask, key, data);
	}
	
	private void toastFail(Object data) {
		BaseEntity<?> baseEntity = (BaseEntity<?>) data;
		//XXX 15/8/28 处理服务器下发的错误消息
		String msg = null;
		if(baseEntity != null){
			msg = baseEntity.getErrMsg();
		}
		if(!TextUtils.isEmpty(msg)){
			ToastUtil.showToast(getContext(), msg);
		}else{
			ToastUtil.showToast(getContext(), ResUtil.STR_TIP_ERR_SERVER);
		}
	}

	@Override
	public boolean onSuccess(String key,Object data, DataResult<?> dataSource) {
		return super.onSuccess(key, data, dataSource);
	}

	/**
	 * 处理因无网络而启动失败的情况
	 */
	protected void onHandleNotNetStartFail(String key){
		if(canClearLoadAndRetryView() || !needRetryTask(key)){
//			showNetSettingDialog();
			ToastUtil.showToast(getContext(), ResUtil.STR_TIP_ON_NET);
		}else{
			showNetSettingView();
		}
	}
	/**
	 * 任务是否需要重试
	 * @param key
	 * @return
	 */
	protected boolean needRetryTask(String key) {
		return true;
	}
	/**
	 * 界面数据是否已经加载过了
	 * @return
	 */
	protected abstract boolean canClearLoadAndRetryView();

	/**
	 * 加载重试界面背景是否透明
	 * @return
	 */
	protected boolean isLoadingViewTransparent(){
		return canClearLoadAndRetryView();
	}

	@Override
	public void showNetSettingView(boolean canClean) {
		INetLoadView loadView = (INetLoadView) getCallBack();
		if(loadView != null){
			try {
				loadView.showNetSettingView(canClean);
			} catch (Exception e) {e.printStackTrace();}
		}
	}

	@Override
	public void showNetSettingDialog(boolean canClean) {
		INetLoadView loadView = (INetLoadView) getCallBack();
		if(loadView != null){
			try {
				loadView.showNetSettingDialog(canClean);
			} catch (Exception e) {e.printStackTrace();}
		}
	}
	
	@Override
	public void showRetryView(Runnable retryTask,boolean canClean) {
		INetLoadView loadView = (INetLoadView) getCallBack();
		if(loadView != null){
			try {
				loadView.showRetryView(retryTask,canClean);
			} catch (Exception e) {e.printStackTrace();}
		}
	}
	
	@Override
	public void showLoginView(Runnable retryTask) {
		mLoginRetryTasks.add(retryTask);
		RetryTaskRunnable retryTaskRunnable = mLoginTaskRunnable.get();
		if(retryTaskRunnable == null){
			retryTaskRunnable = new RetryTaskRunnable(mLoginRetryTasks);
			mLoginTaskRunnable = new WeakReference<RetryTaskRunnable>(retryTaskRunnable);
		}
		
		INetLoadView loadView = (INetLoadView) getCallBack();
		if(loadView != null){
			try {
				loadView.showLoginView(retryTaskRunnable);
			} catch (Exception e) {e.printStackTrace();}
		}
	}
	
	@Override
	public void hideNetSettingView() {
		INetLoadView loadView = (INetLoadView) getCallBack();
		if(loadView != null){
			try {
				loadView.hideNetSettingView();
			} catch (Exception e) {e.printStackTrace();}
		}
	}
	
	@Override
	public void hideNetSettingDialog() {
		INetLoadView loadView = (INetLoadView) getCallBack();
		if(loadView != null){
			try {
				loadView.hideNetSettingDialog();
			} catch (Exception e) {e.printStackTrace();}
		}
	}
	
	@Override
	public void hideRetryView() {
		INetLoadView loadView = (INetLoadView) getCallBack();
		if(loadView != null){
			try {
				loadView.hideRetryView();
			} catch (Exception e) {e.printStackTrace();}
		}
	}

	@Override
	public void setLoadingViewTransparent(boolean isTransparent) {
		INetLoadView loadView = (INetLoadView) getCallBack();
		if(loadView != null){
			try {
				loadView.setLoadingViewTransparent(isTransparent);
			} catch (Exception e) {e.printStackTrace();}
		}
	}

	public void showNetSettingView() {
		showNetSettingView(canClearLoadAndRetryView());
	}

	public void showNetSettingDialog() {
		showNetSettingDialog(canClearLoadAndRetryView());
	}
	
	public final void showRetryView(Runnable retryTask) {
		mFailRetryTasks.add(retryTask);
		RetryTaskRunnable retryTaskRunnable = mFailTaskRunnable.get();
		if(retryTaskRunnable == null){
			retryTaskRunnable = new RetryTaskRunnable(mFailRetryTasks);
			mFailTaskRunnable = new WeakReference<RetryTaskRunnable>(retryTaskRunnable);
		}
		showRetryView(retryTaskRunnable,canClearLoadAndRetryView());
	}
	
	@Override
	public void onNetworkChange(boolean isAvailable) {
		if(isAvailable){
			for (Runnable retryTask : mNotNetRetryTasks) {
				retryTask.run();
			}
			mNotNetRetryTasks.clear();
		}
	}
	
	public void checkBaby(Runnable runnable){
		if(runnable == null) {
			return;
		}
		runnable.run();
	}
	
	public void checkLogin(Runnable runnable) {
		if(runnable == null) {
			return;
		}
		if(!UserData.isLogin()){
			ToastUtil.showToast(getContext(), R.string.tip_not_login_doing);
			showLoginView(runnable);
		}else{
			runnable.run();
		}
	}
	
	private static class RetryTaskRunnable implements LoginCallback{
		private WeakReference<ArrayList<Runnable>> mRunnables;
		private RetryTaskRunnable(ArrayList<Runnable> runnables){
			mRunnables = new WeakReference<ArrayList<Runnable>>(runnables);
		}
		
		@Override
		public void run() {
			try {
				ArrayList<Runnable> runnables = mRunnables.get();
				if(runnables != null){
					for (Runnable runnable : runnables) {
						if(runnable != null){
							runnable.run();
						}
					}
					runnables.clear();
				}
			} catch (Exception e) {}
		}
		
		@Override
		protected void finalize() throws Throwable {
			super.finalize();
			cancel();
		}

		@Override
		public void cancel() {
			ArrayList<Runnable> runnables = mRunnables.get();
			if(runnables != null){
				runnables.clear();
			}	
		}
	}
}
