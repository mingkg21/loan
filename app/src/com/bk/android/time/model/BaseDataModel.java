package com.bk.android.time.model;

import java.io.Serializable;
import java.util.HashMap;

import android.text.TextUtils;

import com.bk.android.data.BaseDataRequest;
import com.bk.android.data.BaseNetDataRequest;
import com.bk.android.data.DataResult;
import com.bk.android.data.ServiceProxy;
import com.bk.android.data.ServiceProxy.DataClient;
import com.bk.android.data.ServiceProxy.RequestDataResult;
import com.bk.android.os.TerminableThreadPool;
import com.bk.android.time.entity.BaseEntity;
import com.bk.android.time.model.BaseDataModel.ILoadCallBack;
import com.bk.android.util.LogUtil;

public class BaseDataModel extends BaseModel<ILoadCallBack> implements DataClient{
	protected static final String TAG = BaseDataModel.class.getSimpleName();
	private ServiceProxy mServiceProxy;
	private HashMap<String,BaseDataRequest> mTaskMap;

	public BaseDataModel(){
		mServiceProxy = new ServiceProxy(getContext(), this);
		mTaskMap = new HashMap<String, BaseDataRequest>();
	}

	protected void startTask(BackgroundTask dackgroundTask){
		TerminableThreadPool terminableThreadPool = new TerminableThreadPool(dackgroundTask);
		terminableThreadPool.start();
	}
	
	public void registerPersistenceTask(String key,String value){
		mServiceProxy.registerPersistenceTask(key,value);
	}
	
	public void unregisterPersistenceTask(String key){
		mServiceProxy.unregisterPersistenceTask(key);
	}
	
	protected String getPersistenceTaskValue(String key){
		return mServiceProxy.getPersistenceTaskValue(key);
	}
	/** 同步数据
	 * @param data */
	protected void syncDataCache(BaseDataRequest dataRequest, Serializable data){
		syncDataCache(dataRequest, data , null);
	}
	
	/** 同步数据
	 * @param data */
	protected void syncDataCache(BaseDataRequest dataRequest, Serializable data,String noticeGroupKey){
		mServiceProxy.syncDataCache(dataRequest, data , noticeGroupKey);
	}
	
	/** 清除缓存
	 * @param groupKey 
	 * @param data */
	protected void clearDataCache(String groupKey){
		mServiceProxy.clearDataCache(groupKey);
	}
	
	/** 设置缓存超时
	 * @param groupKey 
	 * @param data */
	protected void setDataCacheTimeout(String groupKey){
		mServiceProxy.setDataCacheTimeout(groupKey);
	}
	
	/** 执行任务*/
	protected boolean startTask(BaseDataRequest dataRequest){
		return startTask(dataRequest,false);
	}
	
	protected boolean startTask(BaseDataRequest dataRequest,boolean forceStart){
		return startTask(dataRequest,forceStart,null);
	}
	
	/** 执行任务*/
	protected boolean startTask(final BaseDataRequest dataRequest,boolean forceStart,final RequestDataResult result){
		boolean isSuccess = mServiceProxy.requestData(dataRequest,forceStart,new RequestDataResult() {
			@Override
			public void onResult(boolean isSuccess) {
				if(result != null){
					result.onResult(isSuccess);
				}
				if(isSuccess){
					mTaskMap.put(dataRequest.getDataKey(), dataRequest);
				}
			}
		});
		return isSuccess;
	}
	
	/** 终止任务*/
	protected void stopTask(BaseDataRequest dataRequest){
		stopTask(dataRequest.getDataKey());
	}
	
	/** 终止任务*/
	public void stopTask(String key){
		if(TextUtils.isEmpty(key)){
			return;
		}
		mTaskMap.remove(key);
		mServiceProxy.stopTask(key);
	}
	
	/** 终止任务*/
	public void stopAllTask(){
		mTaskMap.clear();
		mServiceProxy.stopAllTask();
	}
	
	protected void dispatchPreBackgrounLoad(final String key,final int state){
		traversalCallBacks(new CallBackHandler<ILoadCallBack>() {
			@Override
			public boolean handle(ILoadCallBack callBack) {
				if(!callBack.isFinished()){
					try {
						return callBack.onPreBackgrounLoad(key, state);
					} catch (Exception e) {e.printStackTrace();}
				}
				return false;
			}
		});
	}
	
	protected void dispatchPreLoad(final String key,final int state){
		traversalCallBacks(new CallBackHandler<ILoadCallBack>() {
			@Override
			public boolean handle(ILoadCallBack callBack) {
				if(!callBack.isFinished()){
					try {
						return callBack.onPreLoad(key, state);
					} catch (Exception e) {e.printStackTrace();}
				}
				return false;
			}
		});
	}
	
	protected void dispatchPostLoad(final String key,final int state){
		traversalCallBacks(new CallBackHandler<ILoadCallBack>() {
			@Override
			public boolean handle(ILoadCallBack callBack) {
				if(!callBack.isFinished()){
					try {
						return callBack.onPostLoad(key, state);
					} catch (Exception e) {e.printStackTrace();}
				}
				return false;
			}
		});
	}
	
	protected void dispatchSuccess(final String key,final Object data,final DataResult<?> dataSource){
		traversalCallBacks(new CallBackHandler<ILoadCallBack>() {
			@Override
			public boolean handle(ILoadCallBack callBack) {
				if(!callBack.isFinished()){
					try {
						return callBack.onSuccess(key,data, dataSource);
					} catch (Exception e) {e.printStackTrace();}
				}
				return false;
			}
		});
	}
	
	protected void dispatchServerFail(final Runnable retryTask,final String key, final Object data){
		traversalCallBacks(new CallBackHandler<ILoadCallBack>() {
			@Override
			public boolean handle(ILoadCallBack callBack) {
				if(!callBack.isFinished()){
					try {
						return callBack.onServerFail(retryTask, key, data);
					} catch (Exception e) {e.printStackTrace();}
				}
				return false;
			}
		});
	}
	
	protected void dispatchFail(final Runnable retryTask,final String key,final int state){
		traversalCallBacks(new CallBackHandler<ILoadCallBack>() {
			@Override
			public boolean handle(ILoadCallBack callBack) {
				if(!callBack.isFinished()){
					try {
						return callBack.onFail(retryTask, key, state);
					} catch (Exception e) {e.printStackTrace();}
				}
				return false;
			}
		});
	}
	
	protected void dispatchDataChange(final String groupKey){
		traversalCallBacks(new CallBackHandler<ILoadCallBack>() {
			@Override
			public boolean handle(ILoadCallBack callBack) {
				if(!callBack.isFinished()){
					try {
						return callBack.onDataChange(groupKey,BaseDataModel.this);
					} catch (Exception e) {e.printStackTrace();}
				}
				return false;
			}
		});
	}
	
	protected void dispatchProgressUpdate(final String key,final int progress,final int maxProgress){
		traversalCallBacks(new CallBackHandler<ILoadCallBack>() {
			@Override
			public boolean handle(ILoadCallBack callBack) {
				if(!callBack.isFinished()){
					try {
						return callBack.onRequestProgressUpdate(key, progress, maxProgress);
					} catch (Exception e) {e.printStackTrace();}
				}
				return false;
			}
		});
	}
	
	@Override
	public final void onNotifyDataChange(String groupKey) {
		dispatchDataChange(groupKey);
	}
	
	protected void notifyDataChange(String groupKey){
		mServiceProxy.notifyDataChange(groupKey);
	}
	
	protected void notifyDataChangeContainSelf(String groupKey){
		ServiceProxy.notifyDataChange(groupKey,null);
	}
	
	@Override
	public void onRequestProgressUpdate(String key, int progress,int maxProgress) {
		dispatchProgressUpdate(key, progress, maxProgress);
	}
	
//	private boolean isNeedfilter(BaseDataRequest dataRequest){
//		if(dataRequest instanceof AbsNetDataRequest){
//			return ((AbsNetDataRequest) dataRequest).isUserChange();
//		}
//		return false;
//	}
	
	@Override
	public final void onResultRequest(String key, int state, DataResult<Object> dataResult) {
		if(state == ServiceProxy.REQUEST_STATE_STARTING){
			dispatchPreLoad(key, state);
		}
		if(state == ServiceProxy.REQUEST_STATE_STARTING_BACKGROUND){
			dispatchPreBackgrounLoad(key, state);
		}
		if(state == ServiceProxy.REQUEST_STATE_SUCCESS){
			dispatchPreBackgrounLoad(key, state);
			dispatchPostLoad(key, state);
			final BaseDataRequest dataRequest = mTaskMap.remove(key);
			if(dataRequest != null){
				if(checkData(key, dataResult)){
					dispatchSuccess(key,dataResult.getData(), dataResult);
				}else{
					if(dataRequest instanceof BaseNetDataRequest){
						Runnable runnable = new Runnable() {
							@Override
							public void run() {
								if(dataRequest != null){
									startTask(dataRequest);
								}
							}
						};
						dispatchFail(runnable, key, state);
					}else{
						dispatchFail(null, key, state);
					}
				}
			}
		}else if(checkData(key, dataResult)){
			dispatchSuccess(key,dataResult.getData(), dataResult);
		}
	}

	@Override
	public final void onResultRequestFinish(String key, int state,
			DataResult<Object> dataResult) {
		dispatchPostLoad(key, state);
		final BaseDataRequest dataRequest = mTaskMap.remove(key);
		if(dataRequest == null){
			return;
		}
		if(state == ServiceProxy.REQUEST_STATE_SUCCESS && !checkData(key, dataResult)){
			state = ServiceProxy.REQUEST_STATE_FAILURE;
		}
		if(state == ServiceProxy.REQUEST_STATE_SUCCESS){
			if(dataResult.getData() instanceof BaseEntity<?>){
				BaseEntity<?> data = (BaseEntity<?>) dataResult.getData();
				if(((BaseEntity<?>)data).isSucceed()){
					dispatchSuccess(key,data, dataResult);
				}else{
					Runnable runnable = new Runnable() {
						@Override
						public void run() {
							if(dataRequest != null){
								startTask(dataRequest);
							}
						}
					};
					dispatchServerFail(runnable, key, data);
				}
			}else{
				dispatchSuccess(key,dataResult.getData(), dataResult);
			}
		}else{
			if(dataRequest instanceof BaseNetDataRequest){
				Runnable runnable = new Runnable() {
					@Override
					public void run() {
						if(dataRequest != null){
							startTask(dataRequest);
						}
					}
				};
				dispatchFail(runnable, key, state);
			}else{
				dispatchFail(null, key, state);
			}
		}
	}

	protected boolean checkData(String key,DataResult<Object> dataResult){
		return dataResult != null && dataResult.getData() != null;
	}
	
	@Override
	public final Integer getClientId() {
		return hashCode();
	}
	
	public abstract class BackgroundTask implements Runnable{
		public abstract Object runTask();
		@Override
		public final void run() {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					dispatchPreLoad(getKey(), ServiceProxy.REQUEST_STATE_STARTING);
				}
			});
			final DataResult<Object> dataResult = new DataResult<Object>();
			Object data = null;
			try {
				data = runTask();
			} catch (Exception e) {
				LogUtil.e(TAG, e);
			}
			dataResult.setNewData(true);
			dataResult.setData(data);
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					dispatchPostLoad(getKey(), ServiceProxy.REQUEST_STATE_STOP);
					if(dataResult.getData() != null){
						dispatchSuccess(getKey(), dataResult.getData(), dataResult);
					}else{
						dispatchFail(null, getKey(), ServiceProxy.REQUEST_STATE_FAILURE);
					}
				}
			});
		}

		public String getKey(){
			return this.getClass().getName() + "_" + hashCode();
		};
	}
	
	public static interface ILoadCallBack{
		/**
		 * 数据变更通知
		 * @param groupKey
		 * @return
		 */
		public boolean onDataChange(String groupKey,BaseDataModel dataModel);
		/**
		 * @param key
		 * @param state
		 * @return
		 */
		public boolean onPreLoad(String key,int state);
		/**
		 * 后台加载开始回调
		 * @param key
		 * @param state
		 * @return
		 */
		public boolean onPreBackgrounLoad(String key,int state);
		/**
		 * @param key
		 * @param state
		 * @return
		 */
		public boolean onPostLoad(String key,int state);
		/**
		 * @param data 数据
		 * @param dataSource
		 * @return 返回true标识事件已经消耗，不会继续传递
		 */
		public boolean onSuccess(String key,Object data, DataResult<?> dataSource);
		/**
		 * @param retryTask 重试任务
		 * @param key 任务标识
		 * @param state 任务状态
		 * @return
		 */
		public boolean onFail(Runnable retryTask,String key,int state);
		/**
		 * @param retryTask 重试任务
		 * @param key 任务标识
		 * @param state 任务状态
		 * @return
		 */
		public boolean onServerFail(Runnable retryTask,String key,Object data);
		/**
		 * 
		 * @param key
		 * @param progress
		 * @param maxProgress
		 */
		public boolean onRequestProgressUpdate(String key, int progress,int maxProgress);
		/**
		 * 界面是否已经退出
		 * @return
		 */
		public boolean isFinished();
	}
	
	public static class SimpleLoadCallBack implements ILoadCallBack{
		@Override
		public boolean onDataChange(String groupKey, BaseDataModel dataModel) {
			return false;
		}

		@Override
		public boolean onPreLoad(String key, int state) {
			return false;
		}

		@Override
		public boolean onPreBackgrounLoad(String key, int state) {
			return false;
		}

		@Override
		public boolean onPostLoad(String key, int state) {
			return false;
		}

		@Override
		public boolean onSuccess(String key, Object data, DataResult<?> dataSource) {
			return false;
		}

		@Override
		public boolean onFail(Runnable retryTask, String key, int state) {
			return false;
		}

		@Override
		public boolean onServerFail(Runnable retryTask, String key, Object data) {
			return false;
		}

		@Override
		public boolean onRequestProgressUpdate(String key, int progress,
				int maxProgress) {
			return false;
		}

		@Override
		public boolean isFinished() {
			return false;
		}
	}
}
