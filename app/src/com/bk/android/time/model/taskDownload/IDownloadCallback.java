package com.bk.android.time.model.taskDownload;

public interface IDownloadCallback {
	public void onDownloadStateChange(String id,int state);
	public void onDownloadProgressChange(String id,int current,int max);
	public void onDownloadInfoLoadFinish();
}
