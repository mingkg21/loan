package com.bk.android.time.model.taskDownload;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DownloadInfo<T> {
	public static final int STATE_NON = -1;
	public static final int STATE_START = 0;
	public static final int STATE_STARTING = 1;
	public static final int STATE_STOP = 2;
	public static final int STATE_FAIL = 3;
	public static final int STATE_SUCCEED = 4;

	private int state;
	@Expose
	@SerializedName("currentProgress")
	private int currentProgress;
	@Expose
	@SerializedName("maxProgress")
	private int maxProgress;
	private T data;
	
	public DownloadInfo(T data){
		this.data = data;
	}
	/**
	 * @return the state
	 */
	public int getState() {
		return state;
	}
	/**
	 * @param state the state to set
	 */
	public void setState(int state) {
		this.state = state;
	}
	/**
	 * @return the currentProgress
	 */
	public int getCurrentProgress() {
		return currentProgress;
	}
	/**
	 * @param currentProgress the currentProgress to set
	 */
	public void setCurrentProgress(int currentProgress) {
		this.currentProgress = currentProgress;
	}
	/**
	 * @return the maxProgress
	 */
	public int getMaxProgress() {
		return maxProgress;
	}
	/**
	 * @param maxProgress the maxProgress to set
	 */
	public void setMaxProgress(int maxProgress) {
		this.maxProgress = maxProgress;
	}
	/**
	 * @return the taskInfo
	 */
	public T getData() {
		return data;
	}
	
	public boolean isStart(){
		return state == STATE_START || state == STATE_STARTING;
	}
	
	public void setData(T data) {
		this.data = data;
	}
}
