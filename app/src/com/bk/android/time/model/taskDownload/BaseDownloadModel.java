package com.bk.android.time.model.taskDownload;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;

import com.bk.android.os.AbsThreadPool;
import com.bk.android.time.data.dao.DBPreferencesProvider;
import com.bk.android.time.model.BaseModel;
import com.bk.android.time.model.taskDownload.BaseDownloadTask.Callback;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public abstract class BaseDownloadModel<Data> extends BaseModel<IDownloadCallback> {
	private static final String NOTIFY_FINISH_TAG = "NOTIFY_FINISH_TAG";
	private static final String DOWNLOAD_INFO_DATA = "DOWNLOAD_INFO_DATA";
	private static final String DOWNLOAD_INFO_PRE = "DOWNLOAD_INFO_PRE";

	private DownloadThreadPool mThreadPool;
	private Handler mHandler;
	private HashMap<String, DownloadTaskGroup> mDownloadTaskGroups;
	private LinkedHashMap<String, DownloadInfo<Data>> mDownloadInfoMap;
	private boolean isLoadDataFinish;
	private NotificationManager mNotificationManager;
	private long lastNotifyTime;
	private ArrayList<String> mNotifyFinishIds;
	
	protected BaseDownloadModel(){
		mNotificationManager = (NotificationManager) getContext().getSystemService(Context.NOTIFICATION_SERVICE);
		mThreadPool = new DownloadThreadPool();
		mHandler = new Handler(Looper.getMainLooper());
		mNotifyFinishIds = new ArrayList<String>();
		mDownloadTaskGroups = new HashMap<String, DownloadTaskGroup>();
		mDownloadInfoMap = new LinkedHashMap<String, DownloadInfo<Data>>();
		new Thread(){
			ArrayList<DownloadInfo<Data>> downloadInfos = new ArrayList<DownloadInfo<Data>>();
			@SuppressWarnings("unchecked")
			@Override
			public void run() {
				ArrayList<String> dataStrs = getAllDatas();
				if(dataStrs != null){
					Gson gson = new Gson();
					for (String dataStr : dataStrs) {
						Data data = dataStrToObj(dataStr);
						if(data != null){
							String downloadStr = getDownloadInfoData(getDataId(data));
							DownloadInfo<Data> downloadInfo = null;
							if(!TextUtils.isEmpty(downloadStr)){
								try {
									downloadInfo = gson.fromJson(downloadStr, DownloadInfo.class);
								} catch (Exception e) {} catch (Error e) {}
								if(downloadInfo != null){
									downloadInfo.setData(data);
								}
							}
							if(downloadInfo == null || downloadInfo.getMaxProgress() == 0 || downloadInfo.getCurrentProgress() == downloadInfo.getMaxProgress()){
								downloadInfo = new DownloadInfo<Data>(data);
								int[] progress = getDataDownloadProgress(data);
								if(progress != null){
									downloadInfo.setCurrentProgress(progress[0]);
									downloadInfo.setMaxProgress(progress[1]);
								}else{
									downloadInfo.setCurrentProgress(0);
									downloadInfo.setMaxProgress(100);
								}
							}
							if(downloadInfo.getCurrentProgress() == downloadInfo.getMaxProgress()){
								downloadInfo.setState(DownloadInfo.STATE_SUCCEED);
							}else{
								downloadInfo.setState(DownloadInfo.STATE_STOP);
							}
							downloadInfos.add(downloadInfo);
						}
					}
				}
				mHandler.post(new Runnable() {
					@Override
					public void run() {
						isLoadDataFinish = true;
						mDownloadInfoMap.clear();
						for (DownloadInfo<Data> taskDownloadInfo : downloadInfos) {
							mDownloadInfoMap.put(getDataId(taskDownloadInfo.getData()), taskDownloadInfo);
						}
						dispatchOnLoadFinish();
					}
				});
			}
		}.start();
	}
	
	protected abstract Data downloadPreHandleData(Data data);
	
	protected abstract BaseDownloadTask<Data> newBaseDownloadTask(DownloadTaskGroup downloadTaskGroup);
	
	protected abstract void saveTaskInfoData(String dataId, String dataObjToStr);

	protected abstract void deleteLocalFile(Data data);

	protected abstract void removeTaskInfoData(String id);
	
	protected abstract Notification getFinishNotification(DownloadInfo<Data> downloadInfo);

	protected abstract boolean hasData(String id);

	protected abstract int[] getDataDownloadProgress(Data data);

	protected abstract String getDataId(Data data);

	protected abstract Data dataStrToObj(String data);
	
	protected abstract String dataObjToStr(Data data);

	protected abstract ArrayList<String> getAllDatas();
	
	private void saveDownloadInfoData(String id,String value) {
		DBPreferencesProvider.getProxy().putString(DOWNLOAD_INFO_PRE + id, value, DOWNLOAD_INFO_DATA);
	}
	
	private String getDownloadInfoData(String id) {
		return DBPreferencesProvider.getProxy().getString(DOWNLOAD_INFO_PRE + id, DOWNLOAD_INFO_DATA, null);
	}
	
	private void removeDownloadInfoData(String id) {
		DBPreferencesProvider.getProxy().remove(DOWNLOAD_INFO_PRE + id ,DOWNLOAD_INFO_DATA);
	}
	
	public boolean isLoadDataFinish(){
		return isLoadDataFinish;
	}
	
	public ArrayList<DownloadInfo<Data>> getDownloadInfos(){
		ArrayList<DownloadInfo<Data>> downloadInfos = new ArrayList<DownloadInfo<Data>>(mDownloadInfoMap.values());
		return downloadInfos;
	}
	
	public int getDownloadInfoSize(){
		return mDownloadInfoMap.size();
	}
	
	public DownloadInfo<Data> getDownloadInfo(String id){
		DownloadInfo<Data> downloadInfo = mDownloadInfoMap.get(id);
		return downloadInfo;
	}
		
	public void onEnterApp(){
	}
	
	public void onExitApp(){
		cancelAllNotify();
		stopAllDownload();
	}
	
	public void cancelNotify(String id){
		mNotificationManager.cancel(NOTIFY_FINISH_TAG, id.hashCode());
		mNotifyFinishIds.remove(id);
	}
	
	private void cancelAllNotify(){
		for (String id : mNotifyFinishIds) {
			mNotificationManager.cancel(NOTIFY_FINISH_TAG, id.hashCode());
		}
		mNotifyFinishIds.clear();
	}
	
	private void notifyFinish(String id){
		try {
			DownloadInfo<Data> downloadInfo = mDownloadInfoMap.get(id);
			if(downloadInfo != null){
				mNotifyFinishIds.add(id);
				boolean needSound = System.currentTimeMillis() - lastNotifyTime > 2000;
				lastNotifyTime = System.currentTimeMillis();
				
				Notification notification = getFinishNotification(downloadInfo);
				if(notification != null){
					if(needSound){
						notification.defaults = Notification.DEFAULT_SOUND;
					}
			        mNotificationManager.notify(NOTIFY_FINISH_TAG, id.hashCode(), notification);
				}
			}
		} catch (Exception e) {} catch (Error e) {}
	}
	
	protected void dispatchOnStateChange(final String id,final int state){
		if(state == DownloadInfo.STATE_SUCCEED){
			notifyFinish(id);
		}
		traversalCallBacks(new CallBackHandler<IDownloadCallback>() {
			@Override
			public boolean handle(IDownloadCallback callBack) {
				callBack.onDownloadStateChange(id, state);
				return false;
			}
		});
	}
	
	protected void dispatchOnProgressChange(final String id,final int current,final int max){
		traversalCallBacks(new CallBackHandler<IDownloadCallback>() {
			@Override
			public boolean handle(IDownloadCallback callBack) {
				callBack.onDownloadProgressChange(id, current, max);
				return false;
			}
		});
	}
	
	protected void dispatchOnLoadFinish(){
		traversalCallBacks(new CallBackHandler<IDownloadCallback>() {
			@Override
			public boolean handle(IDownloadCallback callBack) {
				callBack.onDownloadInfoLoadFinish();
				return false;
			}
		});
	}
	
	public void startDownload(Data data){
		if(!isLoadDataFinish){
			return;
		}
		if(!hasData(getDataId(data))){
			saveTaskInfoData(getDataId(data), dataObjToStr(data));
		}
		synchronized (mDownloadTaskGroups) {
			DownloadTaskGroup downloadTaskGroup = mDownloadTaskGroups.get(getDataId(data));
			if(downloadTaskGroup == null){
				DownloadInfo<Data> downloadInfo = mDownloadInfoMap.get(getDataId(data));
				if(downloadInfo == null){
					downloadInfo = new DownloadInfo<Data>(data);
					downloadInfo.setState(DownloadInfo.STATE_NON);
					mDownloadInfoMap.put(getDataId(data), downloadInfo);
				}
				downloadTaskGroup = new DownloadTaskGroup(downloadInfo);
				mDownloadTaskGroups.put(getDataId(data), downloadTaskGroup);
				mThreadPool.addTask(downloadTaskGroup);
				downloadTaskGroup.setDownloadTask(newBaseDownloadTask(downloadTaskGroup));
			}else{
				downloadTaskGroup.setDownloadTask(newBaseDownloadTask(downloadTaskGroup));
			}
		}
	}
	
	public void stopDownload(String id){
		if(!isLoadDataFinish){
			return;
		}
		synchronized (mDownloadTaskGroups) {
			DownloadTaskGroup downloadTaskGroup = mDownloadTaskGroups.get(id);
			if(downloadTaskGroup != null){
				downloadTaskGroup.setDownloadTask(null);
			}
		}
	}
	
	public void stopAllDownload(){
		if(!isLoadDataFinish){
			return;
		}
		synchronized (mDownloadTaskGroups) {
			for (DownloadTaskGroup downloadTaskGroup : mDownloadTaskGroups.values()) {
				downloadTaskGroup.setDownloadTask(null);
			}
		}
	}

	public void deleteDownload(final String id){
		if(!isLoadDataFinish){
			return;
		}
		synchronized (mDownloadInfoMap) {
			stopDownload(id);
			DownloadInfo<Data> downloadInfo = getDownloadInfo(id);
			if(downloadInfo != null){
				mDownloadInfoMap.remove(id);
				removeTaskInfoData(id);
				removeDownloadInfoData(id);
				deleteLocalFile(downloadInfo.getData());
				mHandler.post(new Runnable() {
					@Override
					public void run() {
						dispatchOnStateChange(id, DownloadInfo.STATE_NON);
					}
				});
			}
		}
	}
	
	private void removeDownloadTask(final String id){
		synchronized (mDownloadTaskGroups) {
			mDownloadTaskGroups.remove(id);
		}
	}
	
	class DownloadTaskGroup implements Runnable, Callback{
		private DownloadInfo<Data> mDownloadInfo;
		private BaseDownloadTask<Data> mDownloadTask;
		private boolean isStarting;
		
		private DownloadTaskGroup(DownloadInfo<Data> downloadInfo){
			mDownloadInfo = downloadInfo;
		}
		
		private void setDownloadTask(BaseDownloadTask<Data> downloadTask){
			if(mDownloadTask != null && mDownloadTask != downloadTask){
				mDownloadTask.setStop();
				setState(mDownloadTask, DownloadInfo.STATE_STOP);
			}
			mDownloadTask = downloadTask;
			if(isStarting){
				setState(downloadTask, DownloadInfo.STATE_STARTING);
			}else{
				setState(downloadTask, DownloadInfo.STATE_START);
			}
		}
		
		@Override
		public void run() {
			isStarting = true;
			synchronized (this) {
				setState(mDownloadTask, DownloadInfo.STATE_STARTING);
			}
			Data data = downloadPreHandleData(mDownloadInfo.getData());
			synchronized (this) {
				if(data == null){
					removeDownloadTask(getDataId(mDownloadInfo.getData()));
					setState(mDownloadTask, DownloadInfo.STATE_FAIL);
					return;
				}
			}			
			mDownloadInfo.setData(data);
			BaseDownloadTask<Data> downloadTask = null;
			while (true){
				downloadTask = mDownloadTask;
				if(downloadTask != null){
					setState(downloadTask, DownloadInfo.STATE_STARTING);
					downloadTask.run(data);
				}
				synchronized (this) {
					if(mDownloadTask == null || downloadTask == mDownloadTask){
						if(mDownloadTask != null){
							if(mDownloadInfo.getCurrentProgress() == mDownloadInfo.getMaxProgress()){
								if(data != null){
									mDownloadInfo.setData(data);
									saveTaskInfoData(getDataId(data), dataObjToStr(data));
								}
								setState(mDownloadTask, DownloadInfo.STATE_SUCCEED);
							}else{
								setState(mDownloadTask, DownloadInfo.STATE_FAIL);
							}
						}
						removeDownloadTask(getDataId(data));
						break;
					}
				}
			}
		}

		boolean isStop(BaseDownloadTask<?> downloadTask){
			return downloadTask == null || mDownloadTask != downloadTask;
		}
		
		void setState(final BaseDownloadTask<Data> downloadTask,final int state){
			if(!isStop(downloadTask) && mDownloadInfo.getState() != state){
				mDownloadInfo.setState(state);
				mHandler.post(new Runnable() {
					@Override
					public void run() {
						dispatchOnStateChange(getDataId(mDownloadInfo.getData()), state);
					}
				});
			}
		}
		
		void setProgress(final BaseDownloadTask<?> downloadTask,final int currentProgress,final int maxProgress){
			if(!isStop(downloadTask)){
				if(mDownloadInfo.getCurrentProgress() < currentProgress || mDownloadInfo.getMaxProgress() != maxProgress){
					mDownloadInfo.setCurrentProgress(currentProgress);
					mDownloadInfo.setMaxProgress(maxProgress);
					Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
					saveDownloadInfoData(getDataId(mDownloadInfo.getData()), gson.toJson(mDownloadInfo, DownloadInfo.class));
					mHandler.post(new Runnable() {
						@Override
						public void run() {
							dispatchOnProgressChange(getDataId(mDownloadInfo.getData()), currentProgress, maxProgress);
						}
					});
				}
			}
		}

		@Override
		public void onDownloadProgressChange(BaseDownloadTask<?> downloadTask,
				int current, int max) {
			setProgress(downloadTask, current, max);			
		}
	}
	
	private class DownloadThreadPool extends AbsThreadPool{
		@Override
		protected int getCorePoolSize() {
			return 3;
		}

		@Override
		protected int getMaximumPoolSize() {
			return 3;
		}

		@Override
		protected long getKeepAliveTime() {
			return 1;
		}

		@Override
		protected TimeUnit getTimeUnit() {
			return TimeUnit.SECONDS;
		}

		@Override
		protected BlockingQueue<Runnable> newQueue() {
			return new LinkedBlockingQueue<Runnable>();
		}
		
		@Override
		protected Runnable onAddTask(Runnable newTask) {
			return super.onAddTask(new SimplePriorityRunnable(newTask));
		}
		
		@Override
		protected ThreadFactory newThreadFactory() {
			return new DefaultThreadFactory(Thread.MIN_PRIORITY);
		}
	}
}
