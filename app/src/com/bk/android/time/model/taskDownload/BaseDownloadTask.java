package com.bk.android.time.model.taskDownload;

public abstract class BaseDownloadTask<T> {
	protected Callback mCallback;
	protected boolean isStop;
	
	public BaseDownloadTask(Callback callback){
		mCallback = callback;
		isStop = false;
	}
	
	void setStop(){
		isStop = true;
	}
	
	abstract void run(T data);
	
	public interface Callback {
		public void onDownloadProgressChange(BaseDownloadTask<?> downloadTask,int current,int max);
	}
}
