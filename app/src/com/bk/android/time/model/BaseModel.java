package com.bk.android.time.model;

import android.content.Context;
import android.content.res.Resources;
import android.os.Handler;
import android.os.Looper;

import com.bk.android.app.BaseApp;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
/**
 * 所有Model和VModel基类
 * @author linyiwei
 *
 */
public class BaseModel<CallBack>{
	private static Handler mHandler = new Handler(Looper.getMainLooper());
	private ArrayList<WeakReference<CallBack>> mCallBacks = new ArrayList<WeakReference<CallBack>>();
	
	protected ArrayList<WeakReference<CallBack>> getCallBacks(){
		return mCallBacks;
	}
	
	/**
	 * 添加回调
	 * @param l
	 */
	public void addCallBack(CallBack l){
		check();
		if(!containsCallBack(l)){
			mCallBacks.add(new WeakReference<CallBack>(l));
		}
	}
	
	private boolean containsCallBack(CallBack l){
		for (WeakReference<CallBack> callBack : mCallBacks) {
			if(callBack.get() != null && callBack.get().equals(l)){
				return true;
			}
		}
		return false;
	}
	/**
	 * 便利回调
	 * @param handler 如果CallBackHandler返回true回调就不会继续传播
	 */
	public void traversalCallBacks(CallBackHandler<CallBack> handler){
		boolean isHandle = false;
		for (int i = 0; i < getCallBacks().size(); i++) {
			WeakReference<CallBack> callBack = getCallBacks().get(i);
			if(callBack.get() != null){
				if(!isHandle){
					isHandle = handler.handle(callBack.get());
				}
			}else{
				getCallBacks().remove(i);
				i--;
			}
		}
	}
	
	public Context getContext() {
		return BaseApp.getInstance();
	}
	
	public void removeCallbacks(Runnable r){
		BaseApp.getHandler().removeCallbacks(r);
	}
	
	public void post(Runnable r){
		BaseApp.getHandler().post(r);
	}
	
	public void post(Runnable r, long delayMillis){
		BaseApp.getHandler().postDelayed(r, delayMillis);
	}
	
	public static Resources getResources(){
		return BaseApp.getInstance().getResources();
	}
	
	public static int getColor(int id) {
		return getResources().getColor(id);
	}
	
	public static String getString(int id){
		return getResources().getString(id);
	}
	
	public static String getString(int id, Object... formatArgs){
		return getResources().getString(id, formatArgs);
	}

	public static float getDimension(int id) {
		return getResources().getDimension(id);
	}
	
	/**
	 * 检测当前线程是否是主线程
	 */
	public static void check(){
		if(Thread.currentThread() != Looper.getMainLooper().getThread()){
			new RuntimeException("Must be running on the UI thread");
		}
	}
	/**
	 * 运行在主线程
	 */
	public static void runOnUiThread(Runnable action) {
		if (Thread.currentThread() != Looper.getMainLooper().getThread()) {
			mHandler.post(action);
		} else {
			action.run();
		}
	}
	
	public interface CallBackHandler<CallBack>{
		public boolean handle(CallBack callBack);
	}
}
