package com.bk.android.weixin;

import android.text.TextUtils;

/**
 * @author mingkg21
 * @email mingkg21@gmail.com
 * @date 2013-4-10
 */
public class AppMessage extends Message {
	
	private String extInfo;

	public AppMessage(String title, String description, boolean isSession, String extInfo) {
		super(title, description, isSession);
		if(!TextUtils.isEmpty(extInfo)) {
			this.extInfo = extInfo;
		} else {
			this.extInfo = description;
		}
	}

	/**
	 * @return the extInfo
	 */
	public String getExtInfo() {
		return extInfo;
	}

	/**
	 * @param extInfo the extInfo to set
	 */
	public void setExtInfo(String extInfo) {
		this.extInfo = extInfo;
	}

}
