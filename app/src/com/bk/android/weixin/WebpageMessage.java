package com.bk.android.weixin;


/** 网页消息实体
 * @author mingkg21
 * @email mingkg21@gmail.com
 * @date 2013-4-9
 */
public class WebpageMessage extends Message{
	
	private String webpageUrl;
	
	public WebpageMessage(String webpageUrl, String title, String description, boolean isSession){
		super(title, description, isSession);
		this.webpageUrl = webpageUrl;
	}

	/**
	 * @return the webpageUrl
	 */
	public String getWebpageUrl() {
		return webpageUrl;
	}

	/**
	 * @param webpageUrl the webpageUrl to set
	 */
	public void setWebpageUrl(String webpageUrl) {
		this.webpageUrl = webpageUrl;
	}

}
