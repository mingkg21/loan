package com.bk.android.weixin;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.tencent.mm.sdk.modelmsg.SendMessageToWX;
import com.tencent.mm.sdk.modelmsg.WXAppExtendObject;
import com.tencent.mm.sdk.modelmsg.WXImageObject;
import com.tencent.mm.sdk.modelmsg.WXMediaMessage;
import com.tencent.mm.sdk.modelmsg.WXWebpageObject;
import com.tencent.mm.sdk.modelpay.PayReq;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.sdk.openapi.WXAPIFactory;

/** 微信实现类
 * @author mingkg21
 * @email mingkg21@gmail.com
 * @date 2013-4-9
 */
public abstract class WXAPIImpl {
	
	public static final String BUNDLE_TYPE = "BUNDLE_TYPE";
	public static final String BUNDLE_SOURCE_TYPE = "BUNDLE_SOURCE_TYPE";
	public static final String BUNDLE_ID = "BUNDLE_ID";
	
	protected IWXAPI api;
	protected Context mContext;
	public static Bundle sMsgBundle;
	
	public WXAPIImpl(Context context){
		mContext = context;
		registerApp(context, getAppID());
	}
	
	/** 获取APP ID
	 * @return
	 */
	protected abstract String getAppID();
	
	/** 注册到微信
	 * @param context
	 * @param appID
	 */
	private void registerApp(Context context, String appID){
		api = WXAPIFactory.createWXAPI(context, appID);
		api.registerApp(appID);
	}
	
	public boolean isWXAppInstalled(){
		return api.isWXAppInstalled();
	}
	public void handleIntent(Intent intent, IWXAPIEventHandler handler){
		api.handleIntent(intent, handler);
	}
	
	public void setBundle(int type, String id) {
		sMsgBundle = new Bundle();
		sMsgBundle.putInt(BUNDLE_TYPE, type);
		sMsgBundle.putString(BUNDLE_ID, id);
	}
	
	public boolean sendPayReq(String appid, String partnerid, String prepayid, String noncestr, String timestamp, String packageData, String sign){
		PayReq req = new PayReq();
		req.appId = appid;
		req.partnerId = partnerid;
		req.prepayId = prepayid;
		req.nonceStr = noncestr;
		req.timeStamp = timestamp;
		req.packageValue = packageData;
		req.sign = sign;
		return api.sendReq(req);
	}
	/** 发送网页信息到微信
	 * @param message
	 */
	public void sendWebpageMessage(WebpageMessage message){
		WXWebpageObject webpage = new WXWebpageObject();
		webpage.webpageUrl = message.getWebpageUrl();
		WXMediaMessage msg = new WXMediaMessage(webpage);
		if(message.getThumb() != null){
//			msg.thumbData = message.getThumbData();
			msg.setThumbImage(message.getThumb());
		}
		msg.title = message.getTitle();
		msg.description = message.getDescription();
		if(message.getThumbData() != null){
			msg.thumbData = message.getThumbData();
		}
		
		sendMessage("webpage", msg, message.isSession());
	}
	
	public void sendImgMessage(Message message){
		WXImageObject imgObject = new WXImageObject();
		imgObject.imagePath = message.getImgPath();
		WXMediaMessage msg = new WXMediaMessage(imgObject);
		if(message.getThumb() != null){
//			msg.thumbData = message.getThumbData();
			msg.setThumbImage(message.getThumb());
		}
		msg.title = message.getTitle();
		msg.description = message.getDescription();
		if(message.getThumbData() != null){
			msg.thumbData = message.getThumbData();
		}
		sendMessage("img", msg, message.isSession());
	}
	
	/** 发送APP信息到微信
	 * @param message
	 */
	public void sendAppMessage(AppMessage message){
		final WXAppExtendObject appdata = new WXAppExtendObject();
		appdata.extInfo = message.getExtInfo();
		final WXMediaMessage msg = new WXMediaMessage();
		if(message.getThumb() != null){
//			msg.thumbData = message.getThumbData();
			msg.setThumbImage(message.getThumb());
		}
		msg.title = message.getTitle();
		msg.description = message.getDescription();
		msg.mediaObject = appdata;
		
		sendMessage("appdata", msg, message.isSession());
	}
	
	private void sendMessage(String type, WXMediaMessage msg, boolean isSession){
		SendMessageToWX.Req req = new SendMessageToWX.Req();
		req.transaction = buildTransaction(type);
		req.message = msg;
		req.scene = isSession ? SendMessageToWX.Req.WXSceneSession : SendMessageToWX.Req.WXSceneTimeline;
		api.sendReq(req);
	}
	
	private String buildTransaction(final String type) {
		return (type == null) ? String.valueOf(System.currentTimeMillis()) : type + System.currentTimeMillis();
	}
}
