package com.bk.android.weixin;

import android.graphics.Bitmap;

/**
 * @author mingkg21
 * @email mingkg21@gmail.com
 * @date 2013-4-10
 */
public class Message {
	private String title;
	private String description;
	private String imgPath;
	private boolean isSession = true;
	private byte[] thumbData;
	private Bitmap thumb;
	
	public Message(String title, String description, boolean isSession){
		this.title = title;
		this.description = description;
		this.isSession = isSession;
	}

	/**
	 * @return the imgPath
	 */
	public String getImgPath() {
		return imgPath;
	}

	/**
	 * @param imgPath the imgPath to set
	 */
	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}

	/**
	 * @return the isSession
	 */
	public boolean isSession() {
		return isSession;
	}

	/**
	 * @param isSession the isSession to set
	 */
	public void setSession(boolean isSession) {
		this.isSession = isSession;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the thumbData
	 */
	public byte[] getThumbData() {
		return thumbData;
	}

	/**
	 * @param thumbData the thumbData to set
	 */
	public void setThumbData(byte[] thumbData) {
		this.thumbData = thumbData;
	}
	
	public void setThumbData(Bitmap thumb){
		setThumbData(Util.bmpToByteArray(thumb, true));
	}

	/**
	 * @return the thumb
	 */
	public Bitmap getThumb() {
		return thumb;
	}

	/**
	 * @param thumb the thumb to set
	 */
	public void setThumb(Bitmap thumb) {
		this.thumb = thumb;
	}

}
