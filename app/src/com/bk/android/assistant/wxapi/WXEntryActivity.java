package com.bk.android.assistant.wxapi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.bk.android.time.model.lightweight.LoginViewModel;
import com.bk.android.time.weixin.WXAPIManager;
import com.bk.android.util.LogUtil;
import com.bk.android.weixin.WXAPIImpl;
import com.tencent.mm.sdk.constants.ConstantsAPI;
import com.tencent.mm.sdk.modelbase.BaseReq;
import com.tencent.mm.sdk.modelbase.BaseResp;
import com.tencent.mm.sdk.modelmsg.SendAuth;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.sdk.openapi.WXAPIFactory;

/**
 * @author mingkg21
 * @email mingkg21@gmail.com
 * @date 2014年5月30日
 *
 */
public class WXEntryActivity extends Activity implements IWXAPIEventHandler {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try {
			IWXAPI api = WXAPIFactory.createWXAPI(this, WXAPIManager.APP_ID, false);
			api.handleIntent(getIntent(), this);
		} catch (Exception e) {
			finish();
		}
	}

	@Override
	public void onReq(BaseReq req) {
		LogUtil.v("wx req errCode: " + req.toString());
        LogUtil.v("wx req getType: " + req.getType());
		
		switch (req.getType()) {
		case ConstantsAPI.COMMAND_SHOWMESSAGE_FROM_WX:
//			WXMediaMessage wxMsg = ((ShowMessageFromWX.Req) req).message;
//			WXAppExtendObject obj = (WXAppExtendObject) wxMsg.mediaObject;
//
//			String extInfo = obj.extInfo;
//			if(!TextUtils.isEmpty(extInfo)) {
//                LogUtil.v("wx req extInfo: " + extInfo);
//				String inviteCode = InviteBabySpaceViewModel.getInviteCode(extInfo);
//                LogUtil.v("wx req inviteCode: " + inviteCode);
//				if(!TextUtils.isEmpty(inviteCode)) {
////					ActivityChannels.openInputInviteCodeActivity(WXEntryActivity.this, inviteCode, true);
//					ActivityChannels.openActionDispatchActivity(WXEntryActivity.this, ActionDispatchActivity.getInviteFamilyEvent(inviteCode));
//				}
//			}
			
			finish();
			break;

		default:
			break;
		}
	}

	@Override
	public void onResp(BaseResp resp) {
		LogUtil.v("resp errCode: " + resp.errCode + "; " + resp.errStr);
		switch (resp.errCode) {
		case BaseResp.ErrCode.ERR_OK:
			if(resp instanceof SendAuth.Resp) {
				final SendAuth.Resp authResp = (SendAuth.Resp) resp;
				LogUtil.v("resp state: " + authResp.state);
				if(WXAPIManager.AUTH_STATE.equals(authResp.state)) {
					LogUtil.v("send broadcast");
					Intent intent = new Intent(LoginViewModel.ACTION_LOGIN_WX);
					intent.putExtra("value", authResp.code);
					sendBroadcast(intent);
					finish();
					return;
				}
			}
			int type = 0;
			String id = "0";
			if(WXAPIImpl.sMsgBundle != null){
				type = WXAPIImpl.sMsgBundle.getInt(WXAPIImpl.BUNDLE_TYPE);
				id = WXAPIImpl.sMsgBundle.getString(WXAPIImpl.BUNDLE_ID);
				WXAPIImpl.sMsgBundle = null;
			}
			break;
		case BaseResp.ErrCode.ERR_USER_CANCEL:
			break;
		case BaseResp.ErrCode.ERR_AUTH_DENIED:
			break;
		default:
			break;
		}
		finish();
	}

}
