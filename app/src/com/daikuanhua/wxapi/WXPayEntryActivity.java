package com.daikuanhua.wxapi;

import android.app.Activity;
import android.os.Bundle;

import com.bk.android.time.weixin.WXAPIManager;
import com.bk.android.util.LogUtil;
import com.tencent.mm.sdk.modelbase.BaseReq;
import com.tencent.mm.sdk.modelbase.BaseResp;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.sdk.openapi.WXAPIFactory;

/**
 * @author mingkg21
 * @email mingkg21@gmail.com
 * @date 2014年5月30日
 *
 */
public class WXPayEntryActivity extends Activity implements IWXAPIEventHandler {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try {
			IWXAPI api = WXAPIFactory.createWXAPI(this, WXAPIManager.APP_ID, false);
			api.handleIntent(getIntent(), this);
		} catch (Exception e) {
			e.printStackTrace();
			finish();
		}
	}

	@Override
	public void onReq(BaseReq req) {
		LogUtil.i("WXPayEntryActivity", "wx req errCode: " + req.toString());
        LogUtil.i("WXPayEntryActivity", "wx req getType: " + req.getType());
		finish();
	}

	@Override
	public void onResp(BaseResp resp) {
		LogUtil.i("WXPayEntryActivity", "resp errCode: " + resp.errCode + "; " + resp.errStr);
//		if(resp.getType() == ConstantsAPI.COMMAND_PAY_BY_WX){
//			OrderModel orderModel = new OrderModel();
//			switch (resp.errCode) {
//				case BaseResp.ErrCode.ERR_OK:
//					orderModel.notifyPayResp(true, false);
//					break;
//				case BaseResp.ErrCode.ERR_USER_CANCEL:
//					orderModel.notifyPayResp(false, true);
//					break;
//				default:
//					orderModel.notifyPayResp(false, false);
//					break;
//			}
//		}
		finish();
	}
}
