package com.daikuanhua.wxapi;

import android.app.Activity;
import android.os.Bundle;

import com.bk.android.app.BaseApp;
import com.bk.android.time.util.ToastUtil;
import com.bk.android.time.weixin.WXAPIManager;
import com.tencent.mm.sdk.constants.ConstantsAPI;
import com.tencent.mm.sdk.modelbase.BaseReq;
import com.tencent.mm.sdk.modelbase.BaseResp;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.sdk.openapi.WXAPIFactory;

/**
 * @author mingkg21
 * @email mingkg21@gmail.com
 * @date 2014年5月30日
 *
 */
public class WXEntryActivity extends Activity implements IWXAPIEventHandler {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try {
			IWXAPI api = WXAPIFactory.createWXAPI(this, WXAPIManager.APP_ID, false);
			api.handleIntent(getIntent(), this);
		} catch (Exception e) {
			finish();
		}
	}

	@Override
	public void onReq(BaseReq req) {
		switch (req.getType()) {
		case ConstantsAPI.COMMAND_SHOWMESSAGE_FROM_WX:
			finish();
			break;

		default:
			break;
		}
	}

	@Override
	public void onResp(BaseResp resp) {
		switch (resp.errCode) {
		case BaseResp.ErrCode.ERR_OK:
			ToastUtil.showToast(BaseApp.getInstance(), "分享成功~");
			break;
		case BaseResp.ErrCode.ERR_USER_CANCEL:
			ToastUtil.showToast(BaseApp.getInstance(), "分享取消~");
			break;
		default:
			break;
		}
		finish();
	}

}
