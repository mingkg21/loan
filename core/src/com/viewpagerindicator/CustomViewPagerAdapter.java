package com.viewpagerindicator;

import android.view.View;

public interface CustomViewPagerAdapter {
	public View getCustomView(int index);
	public int getCustomIndex(View view);
}
