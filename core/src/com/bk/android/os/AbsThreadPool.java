package com.bk.android.os;

import java.util.ArrayList;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class AbsThreadPool{
    private static ArrayList<AbsThreadPool> mPools = new ArrayList<AbsThreadPool>();
    {
    	mPools.add(this);
    }
    protected BlockingQueue<Runnable> queue;
    protected ThreadPoolExecutor executor;
    private boolean isDestroy = false;
    private IClearOutmodedTaskSetting mClearOutmodedTaskSetting;
    
    protected abstract int getCorePoolSize();
    
    protected abstract int getMaximumPoolSize();
    
    protected abstract long getKeepAliveTime();
    
    protected abstract TimeUnit getTimeUnit();
    
    protected abstract BlockingQueue<Runnable> newQueue();
    
    protected IClearOutmodedTaskSetting newClearOutmodedTaskSetting(){
		return null;
    };
    
    protected ThreadFactory newThreadFactory(){
		return new DefaultThreadFactory(Thread.NORM_PRIORITY);
    }
    
    public boolean checkInit(){
    	if(isDestroy){
    		return false;
    	}
    	if(queue == null || executor == null || executor.isShutdown()){
    		synchronized (this) {
    			if(queue == null){
            		queue = newQueue();
            	}
            	if(executor == null || executor.isShutdown()){
            		mClearOutmodedTaskSetting = newClearOutmodedTaskSetting();
            		executor = new ThreadPoolExecutor(
                			getCorePoolSize(),
                			getMaximumPoolSize(),
                			getKeepAliveTime(),
                			getTimeUnit(),
                			queue,newThreadFactory());
            	}
			}
    	}
    	return true;
    }
    /**
     * 
     * @return
     */
    public ThreadPoolExecutor getExecutor(){
		return executor;
    }
    /**
    * 增加新的任务
    * 每增加一个新任务，都要唤醒任务队列
    * @param newTask
    */
    public final void addTask(Runnable newTask) {
    	if(newTask == null){
    		return;
    	}
    	newTask = onAddTask(newTask);
    	if(newTask == null){
    		return;
    	}
    	if(checkInit()){
			clearOutmodedTask();
    		executor.execute(newTask);
    	}
    }
    
    protected void clearOutmodedTask(){
    	if(mClearOutmodedTaskSetting == null){
    		return;
    	}
    	BlockingQueue<Runnable> queue = executor.getQueue();
    	for(;true;){
//			if(queue != null && queue.size() > getMaximumPoolSize() * 3 && queue.size() > 15){
    		if(mClearOutmodedTaskSetting.isNeedPoll(queue)){
				queue.poll();
			}else{
				break;
			}
		}
    }
    
    /**
     * 可通过重写此方法对添加的任务进行装饰
     * @param newTask
     * @return
     */
    protected Runnable onAddTask(Runnable newTask){
		return newTask;
    }
    
    protected boolean isNeedAutoRelease(){
    	return true;
    }
    
    public final void releaseRes(){
    	synchronized (this) {
	    	if(executor != null && !executor.isShutdown() ){
	    		executor.shutdown();
	    		if(queue != null) {
	    			queue.clear();
	    			queue =  null;
	    		}
	    		executor = null;
	    		mClearOutmodedTaskSetting = null;
	    	}
    	}
    }
    
    protected void onDestroy(){
    	
    }
    
    /**
    * 销毁线程池
    */
    public final synchronized void destroy() {
    	synchronized (this) {
	    	if(!isDestroy){
	    		releaseRes();
	    		isDestroy = true;
	    		onDestroy();
	    		mPools.remove(this);
	    	}
    	}
    }
    
    public static synchronized void releaseAllPools(){
    	for(AbsThreadPool pool: mPools){
    		if(pool.isNeedAutoRelease()){
        		pool.releaseRes();
    		}
    	}
    }
    
    public static class DefaultThreadFactory implements ThreadFactory {
        private static final AtomicInteger poolNumber = new AtomicInteger(1);
        private final ThreadGroup group;
        private final AtomicInteger threadNumber = new AtomicInteger(1);
        private final String namePrefix;
        private Integer priority;
        public DefaultThreadFactory(Integer priority) {
            SecurityManager s = System.getSecurityManager();
            group = (s != null)? s.getThreadGroup() :
                                 Thread.currentThread().getThreadGroup();
            namePrefix = "AbsThreadPool - " +
                          poolNumber.getAndIncrement() +
                         " - thread - ";
            this.priority = priority;
        }

        @Override
		public Thread newThread(Runnable r) {
            Thread t = new Thread(group, r,
                                  namePrefix + threadNumber.getAndIncrement(),
                                  0);
            if (t.isDaemon())
                t.setDaemon(false);
            if (priority == null){
                t.setPriority(Thread.NORM_PRIORITY);
            }else{
                t.setPriority(priority);
            }
            return t;
        }
    }
    
    public static class PriorityRunnable implements Runnable,Comparable<PriorityRunnable> {
		private Runnable mTask;
		protected long priority;
		
		public PriorityRunnable(Runnable newTask,int priority){
			mTask = newTask;
			onSetPriority(priority);
		}
		
		protected void onSetPriority(long priority) {
			this.priority = priority;
		}

		@Override
		public int compareTo(PriorityRunnable another) {
			return (int) (another.priority - priority);
		}

		protected void onSetPriority(int priority){
			this.priority = priority;
		}
		
		@Override
		public void run() {
			if(mTask != null){
				mTask.run();
				mTask = null;
			}
		}
	}
    
    public static class SimplePriorityRunnable extends PriorityRunnable{
		private long LAST_PRIORITY = 0;
		
		public SimplePriorityRunnable(Runnable newTask){
			super(newTask, 0);
		}
		
		@Override
		protected void onSetPriority(long priority) {
			this.priority = ++LAST_PRIORITY ;
			if(LAST_PRIORITY == Long.MAX_VALUE){
				LAST_PRIORITY = 0;
			}
		}
	}
    
    public interface IClearOutmodedTaskSetting{
    	public abstract boolean isNeedPoll(BlockingQueue<Runnable> queue);
    }
}
