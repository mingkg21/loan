package com.bk.android.os;


public class TerminableThread extends AbsTerminableThread {
	public TerminableThread(){
		this(null);
	}
	public TerminableThread(Runnable task){
		super(task);
	}
	
	@Override
	protected void runTask(Runnable runnable) {
		Thread thread = new Thread(runnable);
		thread.setName("TerminableThread");
		thread.start();
	}
}
