package com.bk.android.os;

public interface ITerminableThread {
	public void start();
	public void cancel();
	public boolean isCancel();
}
