package com.bk.android.os;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.TimeUnit;

public class TerminableThreadPool extends AbsTerminableThread {
	private ThreadPool mThreadPool;
	public TerminableThreadPool(){
		this(null);
	}
	public TerminableThreadPool(Runnable task){
		super(task);
		mThreadPool = ThreadPool.getInstance();
	}
	
	@Override
	protected void runTask(Runnable runnable) {
		mThreadPool.addTask(runnable);
	}
	
	public static void releaseRes(){
		ThreadPool.getInstance().releaseRes();
	}
	
	private static class ThreadPool extends AbsThreadPool{
	    /* 单例 */
	    private static ThreadPool instance = new ThreadPool();

	    public static synchronized ThreadPool getInstance() {
	        if (instance == null){
	        	instance = new ThreadPool();
	        }
	        return instance;
	    }
	    
	    private ThreadPool(){
	    	
	    }

		@Override
		protected int getCorePoolSize() {
			return 6;
		}

		@Override
		protected int getMaximumPoolSize() {
			return 100;
		}

		@Override
		protected long getKeepAliveTime() {
			return 100;
		}

		@Override
		protected TimeUnit getTimeUnit() {
			return TimeUnit.MILLISECONDS;
		}


		@Override
		protected Runnable onAddTask(Runnable newTask) {
			if(newTask instanceof Comparable){
				return super.onAddTask(newTask);
			}
			return super.onAddTask(new SimplePriorityRunnable(newTask));
		}
		
		@Override
		protected BlockingQueue<Runnable> newQueue() {
			return new PriorityBlockingQueue<Runnable>();
		}
	}
}
