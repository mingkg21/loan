package com.bk.android.data;

import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.HashSet;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;

import com.bk.android.app.BaseApp;

public class ServiceProxy {
//	private static final String TAG = "DataService";
	private static final long REQUEST_TIME_OUT_DELAY = Integer.MAX_VALUE;
	private static final long REQUEST_FAILURE_TIME_OUT_DELAY = Integer.MAX_VALUE;
	/** 请求结果：值 终止*/
	public static final int REQUEST_STATE_STOP = 0;
	/** 请求结果：值 执行中*/
	public static final int REQUEST_STATE_STARTING = 1;
	/** 请求结果：值 后台执行中*/
	public static final int REQUEST_STATE_STARTING_BACKGROUND = 2;
	/** 请求结果：值 成功*/
	public static final int REQUEST_STATE_SUCCESS = 3;
	/** 请求结果：值 失败*/
	public static final int REQUEST_STATE_FAILURE = 4;
	/** 请求结果：值 无网络导致的失败*/
	public static final int REQUEST_STATE_FAILURE_NO_NET = 5;
	/** 请求结果：值 未登录导致的失败*/
	public static final int REQUEST_STATE_FAILURE_NO_LOGIN = 6;
	
	private static HashMap<String,NotifyChangeRunnable> sNotifyTaskMap = new HashMap<String, NotifyChangeRunnable>();
			
	private static Handler sHandler = new Handler(Looper.getMainLooper());
	
	private DataReceiver mDataReceiver;
	private Context mContext;
	private HashMap<String,Runnable> mTaskMap;
	private HashMap<String,String> mTaskGroupMap;
	private HashMap<String,String> mPersistenceTask;
	private DataClient mDataClient;
	
	public ServiceProxy(Context context,DataClient dataClient){
		mDataClient = dataClient;
		mContext = DataContext.getInstance(context);
		mDataReceiver = new DataReceiver(this,dataClient,mDataClient.getClientId());
		IntentFilter intentFilter = new IntentFilter(DataService.ACTION_RESULT_REQUEST_FINISH);
		intentFilter.addAction(DataService.ACTION_RESULT_REQUEST);
		intentFilter.addAction(DataService.ACTION_NOTIFY_DATA_CHANGE);
		intentFilter.addAction(DataService.ACTION_RESULT_PROGRESS);
		mContext.registerReceiver(mDataReceiver,intentFilter);
		mTaskMap = new HashMap<String,Runnable>();
		mTaskGroupMap = new HashMap<String, String>();
		mPersistenceTask = new HashMap<String, String>();
	}
	
	@Override
	protected void finalize() throws Throwable {
		mContext.unregisterReceiver(mDataReceiver);
		release();
		super.finalize();
	}
	
	public void registerPersistenceTask(String key,String value){
		mPersistenceTask.put(key, value);
	}
	
	public void unregisterPersistenceTask(String key){
		mPersistenceTask.remove(key);
	}
	
	public String getPersistenceTaskValue(String key){
		return mPersistenceTask.get(key);
	}
	
	public void syncDataCache(BaseDataRequest dateProvider,Serializable data,String noticeGroupKey){
		Intent intent = new Intent(DataService.ACTION_SYNC_DATA_CACHE);
		intent.setClass(mContext, DataService.class);
		intent.putExtra(DataService.EXTRA_PROVIDER, dateProvider);
		intent.putExtra(DataService.EXTRA_CLIENT_ID, mDataClient.getClientId());
		intent.putExtra(DataService.EXTRA_SYNC_DATA, data);
		intent.putExtra(DataService.EXTRA_SYNC_NOTICE_GROUP_KEY, noticeGroupKey);
		mContext.startService(intent);
	}
	
	public void clearDataCache(String groupKey){
		Intent intent = new Intent(DataService.ACTION_CLEAR_DATA_CACHE);
		intent.setClass(mContext, DataService.class);
		intent.putExtra(DataService.EXTRA_CLIENT_ID, mDataClient.getClientId());
		intent.putExtra(DataService.EXTRA_GROUP_KEY, groupKey);
		mContext.startService(intent);
	}
	
	public void setDataCacheTimeout(String groupKey){
		Intent intent = new Intent(DataService.ACTION_SET_DATA_CACHE_TIMEOUT);
		intent.setClass(mContext, DataService.class);
		intent.putExtra(DataService.EXTRA_CLIENT_ID, mDataClient.getClientId());
		intent.putExtra(DataService.EXTRA_GROUP_KEY, groupKey);
		mContext.startService(intent);
	}
	
	public boolean requestData(BaseDataRequest dateProvider,boolean forceStart,RequestDataResult result){
		if(dateProvider == null){
			if(result != null){
				result.onResult(false);
			}
			return false;
		}
		dateProvider.onClientInit();
		final String finalKey = dateProvider.getDataKey();
		String finalGroupKey = dateProvider.getDataKey();
		if(mTaskMap.containsKey(finalKey) || mTaskGroupMap.containsValue(finalGroupKey)){
			if(result != null){
				result.onResult(false);
			}
			return false;
		}
		Intent intent = new Intent(DataService.ACTION_REQUEST_DATA);
		intent.setClass(mContext, DataService.class);
		intent.putExtra(DataService.EXTRA_PROVIDER, dateProvider);
		intent.putExtra(DataService.EXTRA_CLIENT_ID, mDataClient.getClientId());
		intent.putExtra(DataService.EXTRA_FORCE_START,forceStart);
		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				mTaskMap.remove(finalKey);
				mTaskGroupMap.remove(finalKey);
				mDataReceiver.onResultRequestFinish(finalKey, REQUEST_STATE_FAILURE, null);
			}
		};
		mTaskMap.put(finalKey, runnable);
		mTaskGroupMap.put(finalKey, finalGroupKey);
		sHandler.postDelayed(runnable,REQUEST_TIME_OUT_DELAY);
		if(result != null){
			result.onResult(true);
		}
		mContext.startService(intent);
		return true;
	}
	
	public void stopTask(String key){
		Intent intent = new Intent(DataService.ACTION_REQUEST_STOP_TASK);
		intent.setClass(mContext, DataService.class);
		intent.putExtra(DataService.EXTRA_KEY,key);
		intent.putExtra(DataService.EXTRA_CLIENT_ID, mDataClient.getClientId());
		mContext.startService(intent);
		releaseTask(key);
	}
	
	private void releaseTask(String key){
		Runnable oldRunnable = mTaskMap.remove(key);
		sHandler.removeCallbacks(oldRunnable);
		mTaskGroupMap.remove(key);
	}
	
	public void stopAllTask(){
		HashSet<String> keySet = new HashSet<String>(mTaskMap.keySet());
		for (String key : keySet) {
			stopTask(key);
		}
	}
	
	private void release(){
		if(mTaskMap.isEmpty()){
			return;
		}
		HashSet<String> keySet = new HashSet<String>(mTaskMap.keySet());
		for (String key : keySet) {
			releaseTask(key);
		}
	}
	
	public void notifyDataChange(String groupKey){
		notifyDataChange(groupKey, mDataClient.getClientId());
	}
	
	public static void notifyDataChange(String groupKey,Integer clientId){
		String key = groupKey + "_" + clientId;
		NotifyChangeRunnable runnable = sNotifyTaskMap.get(key);
		if(runnable == null){
			runnable = new NotifyChangeRunnable(groupKey, clientId);
			sNotifyTaskMap.put(key, runnable);
		}
		sHandler.removeCallbacks(runnable);
		if(runnable.getListNotifyTime() == null){
			sHandler.post(runnable);
		}else{
			long difference = SystemClock.uptimeMillis() - runnable.getListNotifyTime();
			if(difference < 500){
				sHandler.postDelayed(runnable, 500 - difference);
			}else{
				sHandler.post(runnable);
			}
		}
	}
	
	private static class DataReceiver extends BroadcastReceiver{
		private WeakReference<DataClient> mDataClient;
		private WeakReference<ServiceProxy> mServiceProxy;
		private Integer mClientId;
		private DataReceiver(ServiceProxy serviceProxy,DataClient dataClient,Integer clientId){
			mDataClient = new WeakReference<DataClient>(dataClient);
			mServiceProxy = new WeakReference<ServiceProxy>(serviceProxy);
			mClientId = clientId;
		}
		
		@Override
		public void onReceive(Context context,final Intent intent) {
			final ServiceProxy serviceProxy = mServiceProxy.get();
			if(intent == null || serviceProxy == null){
				return;
			}
			if(DataService.ACTION_NOTIFY_DATA_CHANGE.equals(intent.getAction())){
				String groupKey = intent.getStringExtra(DataService.EXTRA_GROUP_KEY);
				Integer clientId = (Integer) intent.getSerializableExtra(DataService.EXTRA_CLIENT_ID);
				if(!mClientId.equals(clientId)){
					onNotifyDataChange(groupKey);
				}
				return;
			}
			final String key = intent.getStringExtra(DataService.EXTRA_KEY);
			if(!serviceProxy.mTaskMap.containsKey(key) && !serviceProxy.mPersistenceTask.containsKey(key)){
				return;
			}
			if(DataService.ACTION_RESULT_REQUEST.equals(intent.getAction())){
				Integer clientId = (Integer) intent.getSerializableExtra(DataService.EXTRA_CLIENT_ID);
				if(!mClientId.equals(clientId)){
					return;
				}
				int state = intent.getIntExtra(DataService.EXTRA_STATE, -1);
				if(state != REQUEST_STATE_SUCCESS){
					Runnable runnable = new Runnable() {
						@Override
						public void run() {
							serviceProxy.mTaskMap.remove(key);
							serviceProxy.mTaskGroupMap.remove(key);
							onResultRequestFinish(key, REQUEST_STATE_FAILURE, null);
						}
					};
					Runnable oldRunnable = serviceProxy.mTaskMap.put(key, runnable);
					sHandler.removeCallbacks(oldRunnable);
					sHandler.postDelayed(runnable,REQUEST_FAILURE_TIME_OUT_DELAY);
				}else{
					serviceProxy.mTaskGroupMap.remove(key);
					Runnable oldRunnable = serviceProxy.mTaskMap.remove(key);
					sHandler.removeCallbacks(oldRunnable);
				}
				DataResult<Object> dataResult = getData(intent);
				onResultRequest(key, state, dataResult);
			}else if(DataService.ACTION_RESULT_REQUEST_FINISH.equals(intent.getAction())){
				serviceProxy.mTaskGroupMap.remove(key);
				Runnable oldRunnable = serviceProxy.mTaskMap.remove(key);
				sHandler.removeCallbacks(oldRunnable);
				int state = intent.getIntExtra(DataService.EXTRA_STATE, -1);
				DataResult<Object> dataResult = getData(intent);
				onResultRequestFinish(key, state, dataResult);
			}else if(DataService.ACTION_RESULT_PROGRESS.equals(intent.getAction())){
				int progress = intent.getIntExtra(DataService.EXTRA_PROGRESS, -1);
				int maxProgress = intent.getIntExtra(DataService.EXTRA_MAX_PROGRESS, -1);
				onRequestProgressUpdate(key, progress, maxProgress);
				Runnable oldRunnable = serviceProxy.mTaskMap.get(key);
				sHandler.removeCallbacks(oldRunnable);
				sHandler.postDelayed(oldRunnable,REQUEST_FAILURE_TIME_OUT_DELAY);
			}
		}
		
		@SuppressWarnings("unchecked")
		private DataResult<Object> getData(Intent intent){
			DataResult<Object> dataResult = null;
			try {
				return (DataResult<Object>) intent.getSerializableExtra(DataService.EXTRA_DATA);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return dataResult;
		}
		
		private void onNotifyDataChange(String groupKey) {
			final DataClient dataClient = mDataClient.get();
			if(dataClient == null){
				return;
			}
			try {
				dataClient.onNotifyDataChange(groupKey);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		private void onResultRequest(String key,int state,DataResult<Object> dataResult){
			final DataClient dataClient = mDataClient.get();
			if(dataClient == null){
				return;
			}
			try {
				dataClient.onResultRequest(key, state, dataResult);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		private void onResultRequestFinish(String key,int state,DataResult<Object> dataResult){
			final DataClient dataClient = mDataClient.get();
			if(dataClient == null){
				return;
			}
			try {
				dataClient.onResultRequestFinish(key, state, dataResult);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		private void onRequestProgressUpdate(String key,int progress,int maxProgress){
			final DataClient dataClient = mDataClient.get();
			if(dataClient == null){
				return;
			}
			try {
				dataClient.onRequestProgressUpdate(key, progress, maxProgress);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static class NotifyChangeRunnable implements Runnable{
		private String mGroupKey;
		private Integer mClientId;
		private Long mListNotifyTime;
		
		public NotifyChangeRunnable(String groupKey,Integer clientId){
			mGroupKey = groupKey;
			mClientId = clientId;
		}
		
		public Long getListNotifyTime() {
			return mListNotifyTime;
		}

		@Override
		public void run() {
			mListNotifyTime = SystemClock.uptimeMillis();
			Intent intent = new Intent(DataService.ACTION_NOTIFY_DATA_CHANGE);
			intent.putExtra(DataService.EXTRA_GROUP_KEY,mGroupKey);
			intent.putExtra(DataService.EXTRA_CLIENT_ID,mClientId);
			DataContext.getInstance(BaseApp.getInstance()).sendBroadcast(intent);
		}
	}
	
	public interface DataClient{
		public void onResultRequest(String key,int state,DataResult<Object> dataResult);
		public void onRequestProgressUpdate(String key, int progress,int maxProgress);
		public void onNotifyDataChange(String groupKey);
		public void onResultRequestFinish(String key,int state,DataResult<Object> dataResult);
		public Integer getClientId();
	}
	
	public interface RequestDataResult{
		public void onResult(boolean isSuccess);
	}
}
