package com.bk.android.data;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import android.os.Parcel;
import android.os.Parcelable;

import com.bk.android.app.BaseApp;
/**
 * 子类必须有空的构造方法，这个构造方法是保证序列化正常进行的关键，且必须是公开外部类或者公开的静态内部类
 * @param source
 */
public class BaseParcelable implements Parcelable{
	public static final BaseCreator<BaseParcelable> CREATOR = new BaseCreator<BaseParcelable>();
	
	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public final void writeToParcel(Parcel out, int flags) {
		out.writeSerializable(getClass());
		onWriteToParcel(out, flags);
	}
	/**
	 * 通过继承由子类实现序列化过程可提升序列化速度
	 * @param out
	 * @param flags
	 */
	protected void onWriteToParcel(Parcel out, int flags){
		Field[] fields = getClass().getDeclaredFields();
		if(fields != null){
			for (Field field : fields) {
				try {
//					if(Modifier.isStatic(field.getModifiers())){
//						continue;
//					}
					if(field.getType().isAssignableFrom(Object[].class)){
						if(field.getType().isAssignableFrom(byte[].class)){
							out.writeByteArray((byte[]) field.get(this));
						}else if(field.getType().isAssignableFrom(int[].class)){
							out.writeIntArray((int[]) field.get(this));
						}else if(field.getType().isAssignableFrom(long[].class)){
							out.writeLongArray((long[]) field.get(this));
						}else if(field.getType().isAssignableFrom(char[].class)){
							out.writeCharArray((char[]) field.get(this));
						}else if(field.getType().isAssignableFrom(float[].class)){
							out.writeFloatArray((float[]) field.get(this));
						}else if(field.getType().isAssignableFrom(double[].class)){
							out.writeDoubleArray((double[]) field.get(this));
						}else if(field.getType().isAssignableFrom(boolean[].class)){
							out.writeBooleanArray((boolean[]) field.get(this));
						}else{
							out.writeArray((Object[]) field.get(this));
						}
					}else{
						if(field.getType().isAssignableFrom(byte.class)){
							out.writeByte(field.getByte(this));
						}else if(field.getType().isAssignableFrom(short.class)){
							out.writeInt(field.getInt(this));
						}else if(field.getType().isAssignableFrom(int.class)){
							out.writeInt(field.getInt(this));
						}else if(field.getType().isAssignableFrom(long.class)){
							out.writeLong(field.getLong(this));
						}else if(field.getType().isAssignableFrom(char.class)){
							out.writeInt(field.getChar(this));
						}else if(field.getType().isAssignableFrom(float.class)){
							out.writeFloat(field.getFloat(this));
						}else if(field.getType().isAssignableFrom(double.class)){
							out.writeDouble(field.getDouble(this));
						}else if(field.getType().isAssignableFrom(boolean.class)){
							out.writeByte((byte) (field.getBoolean(this) ? 1 : 0));
						}else if(field.getType().isAssignableFrom(String.class)){
							out.writeString((String) field.get(this));
						}else{
							out.writeValue(field.get(this));
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
	/**
	 * 通过继承由子类实现序列化过程可提升序列化速度
	 * @param out
	 * @param flags
	 */
	protected void onRestoreToParcel(Parcel in){
		Field[] fields = getClass().getDeclaredFields();
		if(fields != null){
			for (Field field : fields) {
				try {
					if(Modifier.isStatic(field.getModifiers())){
						continue;
					}
					if(field.getType().isAssignableFrom(Object[].class)){
						field.set(this, in.readValue(BaseApp.getInstance().getClassLoader()));
					}else{
						if(field.getType().isAssignableFrom(byte.class)){
							field.setByte(this, in.readByte());
						}else if(field.getType().isAssignableFrom(short.class)){
							field.setShort(this, (short) in.readInt());
						}else if(field.getType().isAssignableFrom(int.class)){
							field.setInt(this, in.readInt());
						}else if(field.getType().isAssignableFrom(long.class)){
							field.setLong(this, in.readLong());
						}else if(field.getType().isAssignableFrom(char.class)){
							field.setChar(this, (char) in.readInt());
						}else if(field.getType().isAssignableFrom(float.class)){
							field.setFloat(this,in.readFloat());
						}else if(field.getType().isAssignableFrom(double.class)){
							field.setDouble(this,in.readDouble());
						}else if(field.getType().isAssignableFrom(boolean.class)){
							field.setBoolean(this,in.readByte() == 1 ? true : false);
						}else if(field.getType().isAssignableFrom(String.class)){
							field.set(this, in.readString());
						}else{
							field.set(this, in.readValue(BaseApp.getInstance().getClassLoader()));
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	/**
	 * 必须在子类里添加一下变量才能自动序列化
	 * public static final BaseCreator<T> CREATOR = new BaseCreator<T>();
	 * @author linyiwei
	 * @param <T>
	 */
	public static class BaseCreator<T extends BaseParcelable> implements Parcelable.Creator<T>{
		public BaseCreator(){
		}
		
		@Override
		public T createFromParcel(Parcel in) {
			try {
				Class<?> mType = (Class<?>) in.readSerializable();
				BaseParcelable obj = (BaseParcelable) mType.newInstance();
				obj.onRestoreToParcel(in);
				return (T) obj;
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		public T[] newArray(int size) {
			return (T[]) Array.newInstance(BaseParcelable.class, 1);
		}
	}
}
