package com.bk.android.data;

import android.content.Context;
import android.os.Handler;

import com.bk.android.util.ApnUtil;

public abstract class BaseNetDataRequest extends BaseDataRequest {
	private static final long serialVersionUID = -2946097418655235190L;
	@Override
	Result run(Context context,Handler handler,int clientId) {
		if(checkNet(context)){
			if(checkLogin()){
				if(checkRequest(context)){
					return super.run(context,handler,clientId);
				}else{
					return new Result(ServiceProxy.REQUEST_STATE_FAILURE, null, null,null,false);
				}
			}else{
				return new Result(ServiceProxy.REQUEST_STATE_FAILURE_NO_LOGIN, null, null,null,false);
			}
		}else{
			return new Result(ServiceProxy.REQUEST_STATE_FAILURE_NO_NET, null, null,null,false);
		}
	}
	
	protected boolean isUpload(){
		return false;
	}
	
	protected boolean checkNet(Context context){
		return ApnUtil.isNetAvailable(context);
	}
	/**
	 * 登录验证，如果不需要登录直接返回true
	 * @return
	 */
	protected abstract boolean checkLogin();
	/**
	 * 验证请求是否可执行
	 * @return
	 */
	protected abstract boolean checkRequest(Context context);
}
