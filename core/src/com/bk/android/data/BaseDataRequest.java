package com.bk.android.data;

import java.io.Serializable;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;

import com.bk.android.os.TerminableThreadPool;
import com.bk.android.util.LogUtil;
/**
 * 数据提供者封装获取数据的方法
 * @author linyiwei
 */
public abstract class BaseDataRequest implements Serializable{
	private static final long serialVersionUID = -4132751639289635538L;
	private static final String TAG = "BaseDateProvider";
	public static String createKey(BaseDataRequest obj,Object... param){
		StringBuffer key = new StringBuffer(obj.getClass().getName()+":");
		if(param != null){
			for (Object object : param) {
				key.append("_");
				if(object != null){
					key.append(object.toString());
				}
			}
		}
		return key.toString();
	}
	
	public static String createGroupKey(BaseDataRequest obj,Object... param){
		return createGroupKey(obj.getClass(), param);
	}
	
	public static String createGroupKey(Class<? extends BaseDataRequest> clazz,Object... param){
		StringBuffer key = new StringBuffer(clazz.getName());
		if(param != null){
			for (Object object : param) {
				key.append("_");
				if(object != null){
					key.append(object.toString());
				}
			}
		}
		return key.toString();
	}

	private Handler mHandler;
	
	private int mClientId;
	private Long mLastProgress;
	private Message mLastMessage;
	
	protected void onClientInit(){
	}
	/**
	 * 缓存时间小于等于0代表不预读缓存，等于0代表不缓存
	 * @return
	 */
	public abstract int getCacheTime();
	/**
	 * 数据任务唯一标识
	 * @return
	 */
	public String getDataGroupKey(){
		return createGroupKey(this);
	}
	/**
	 * 数据任务唯一标识
	 * @return
	 */
	public abstract String getDataKey();
	/**
	 * 需要通知数据变更的请求GroupKey
	 * @return
	 */
	public abstract String[] getNeedNotifyGroupKey();
	/**
	 * 设置进度通知
	 * @param progress
	 * @param maxProgress
	 */
	public void setProgress(int progress,int maxProgress){
		if(mHandler != null){
			if(mLastMessage != null){
				mHandler.removeMessages(DataService.HANDLER_WHAT_PROGRESS,mLastMessage);
			}
			Message msg = new Message();
			msg.what = DataService.HANDLER_WHAT_PROGRESS;
			msg.obj = new ProgressInfo(getDataKey(),mClientId,progress,maxProgress);
			if(mLastProgress == null){
				mLastProgress = 0L;
			}
			if(SystemClock.uptimeMillis() - mLastProgress < 200){
				mLastMessage = msg;
				mHandler.sendMessageDelayed(mLastMessage, 200);
				return;
			}
			mLastMessage = null;
			mLastProgress = SystemClock.uptimeMillis();
			mHandler.sendMessage(msg);
		}
	}
	
	protected Serializable start(Context context,BaseDataRequest child,boolean isNotice){
		Handler handler = null;
		if(isNotice){
			handler = mHandler;
		}
		Result data = child.run(context,handler,mClientId);
		return data.data;
	}
	/**
	 * 执行获取数据的任务
	 * @param context
	 * @return
	 */
	Result run(Context context,Handler handler,int clientId){
		Serializable data = null;
		Exception err = null;
		mHandler = handler;
		mClientId = clientId;
		try {
			data = onRunTask(context);
		} catch (Exception e) {
			LogUtil.e(TAG, e);
			err = e;
		}
		mClientId = -1;
		mHandler = null;
		return new Result(
				data != null ? ServiceProxy.REQUEST_STATE_SUCCESS : ServiceProxy.REQUEST_STATE_FAILURE,
				data,
				err,
				getNeedNotifyGroupKey(),
				data != null ? isDataNeedSave(data) : false);
	}
	
	protected boolean isDataNeedSave(Serializable data){
		return true;
	}
	
	protected void handlerSyncData(DataResult<?> dataResult,Serializable data){
	}
	/**
	 * 执行获取数据任务回调，由子类实现具体的任务
	 * @param context
	 * @return
	 */
	protected abstract Serializable onRunTask(Context context);
	
	class Result{
		int state;
		Serializable data;
		boolean isNeedSave;
		Exception e;
		String[] needNotifyGroupKeys;
		public Result(int state, Serializable data,Exception e,String[] needNotifyGroupKeys,boolean isNeedSave) {
			this.state = state;
			this.data = data;
			this.e = e;
			this.needNotifyGroupKeys = needNotifyGroupKeys;
			this.isNeedSave = isNeedSave;
		}
	}
	
	protected void runInBackground(Runnable runnable){
		new TerminableThreadPool(runnable).start(); 
	}
	
	protected final void notifyDataChange(String groupKey){
		ServiceProxy.notifyDataChange(groupKey, mClientId);
	}
	
	public boolean isNeedSaveDB() {
		return false;
	}

	public boolean isNeedSaveCache() {
		return true;
	}
}
