package com.bk.android.data;

import java.io.Serializable;

import android.content.Context;

interface DataManage {
	public void submitResult(TaskInfo taskInfo,Serializable data,boolean isNeedSave);
	public void submitResultInThread(TaskInfo taskInfo,Serializable data,boolean isNeedSave);
	public Context getContext();
}
