package com.bk.android.data;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import android.os.Handler;

import com.bk.android.app.BaseApp;
import com.bk.android.data.BaseDataRequest.Result;
import com.bk.android.os.AbsTerminableThread;
import com.bk.android.os.AbsThreadPool;
import com.bk.android.os.ITerminableThread;
import com.bk.android.os.TerminableThread;
import com.bk.android.os.TerminableThreadPool;

final class TaskInfo{
	/** 放回结果*/
	private int mRequestState;
	/** 异常信息*/
	private Exception mUserErr;
	/** 任务*/
	private BaseDataRequest mDateProvider;
	/** 需要通知数据变更的请求*/
	private String[] mNeedNotifyGroupKeys;
	/** 任务线程*/
	private ITerminableThread mITerminableThread;
	/** 数据服务*/
	private DataManage mDataManage;
	/** 请求发起者*/
	private int mClientId;
	public TaskInfo(){}
	
	public TaskInfo(BaseDataRequest dateProvider,DataManage dataManage){
		mRequestState = ServiceProxy.REQUEST_STATE_STOP;
		mDateProvider = dateProvider;
		mDataManage = dataManage;
	}
	
	private void handRunTask(final ITerminableThread finalThread, Handler handler, int clientId){
		if(finalThread == mITerminableThread){
			final Result result = mDateProvider.run(mDataManage.getContext(),handler,clientId);
			if(finalThread == mITerminableThread){
				mDataManage.submitResultInThread(TaskInfo.this, result.data,result.isNeedSave);
			}
			BaseApp.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					if(finalThread == mITerminableThread){
						mITerminableThread = null;
						mRequestState = result.state;
						mUserErr = result.e;
						mNeedNotifyGroupKeys = result.needNotifyGroupKeys;
						mDataManage.submitResult(TaskInfo.this, result.data,result.isNeedSave);
					}
				}
			});
		}
	}
	
	public void start(final Handler handler,final int clientId){
		mClientId = clientId;
		mRequestState = ServiceProxy.REQUEST_STATE_STARTING;
		if(mDateProvider instanceof BaseNetDataRequest){
			if(((BaseNetDataRequest) mDateProvider).isUpload()){
				mITerminableThread = new UploadTaskPool(){
					private ITerminableThread finalThread = this;
					@Override
					public void run() {
						handRunTask(finalThread, handler, clientId);
					}
				};
			}else{
				mITerminableThread = new TerminableThreadPool(){
					private ITerminableThread finalThread = this;
					@Override
					public void run() {
						handRunTask(finalThread, handler, clientId);
					}
				};
			}
		}else{
			mITerminableThread = new TerminableThread(){
				private ITerminableThread finalThread = this;
				@Override
				public void run() {
					handRunTask(finalThread, handler, clientId);
				}
			};
		}
		mITerminableThread.start();
	}
	
	public void cancel(){
		if(mITerminableThread != null){
			mITerminableThread.cancel();
			mITerminableThread = null;
			mRequestState = ServiceProxy.REQUEST_STATE_STOP;
		}
	}
	
	public String getDataKey() {
		return mDateProvider.getDataKey();
	}
	
	public String getDataGroupKey() {
		return mDateProvider.getDataGroupKey();
	}
	
	public int getCacheTime(){
		return mDateProvider.getCacheTime();
	}
	
	/**
	 * @return the mResultCode
	 */
	public int getRequestState() {
		return mRequestState;
	}
	/**
	 * @return the mUserErr
	 */
	public Exception getUserErr() {
		return mUserErr;
	}
	/**
	 * @return the mNeedNotifyGroupKeys
	 */
	public String[] getNeedNotifyGroupKeys() {
		return mNeedNotifyGroupKeys;
	}

	public boolean isNeedSaveDB() {
		return mDateProvider.isNeedSaveDB();
	}
	
	public boolean isNeedSaveCache() {
		return mDateProvider.isNeedSaveCache();
	}
	
	public int getClientId(){
		return mClientId;
	}
	
	public static class UploadTaskPool extends AbsTerminableThread {
		private ThreadPool mThreadPool;
		public UploadTaskPool(){
			this(null);
		}
		public UploadTaskPool(Runnable task){
			super(task);
			mThreadPool = ThreadPool.getInstance();
		}
		
		@Override
		protected void runTask(Runnable runnable) {
			mThreadPool.addTask(runnable);
		}
		
		private static class ThreadPool extends AbsThreadPool{
		    /* 单例 */
		    private static ThreadPool instance = new ThreadPool();

		    public static synchronized ThreadPool getInstance() {
		        if (instance == null){
		        	instance = new ThreadPool();
		        }
		        return instance;
		    }
		    
		    private ThreadPool(){}

			@Override
			protected int getCorePoolSize() {
				return 2;
			}

			@Override
			protected int getMaximumPoolSize() {
				return 128;
			}

			@Override
			protected long getKeepAliveTime() {
				return 1;
			}

			@Override
			protected TimeUnit getTimeUnit() {
				return TimeUnit.SECONDS;
			}

			@Override
			protected BlockingQueue<Runnable> newQueue() {
				return new LinkedBlockingQueue<Runnable>();
			}
		}
	}
}
