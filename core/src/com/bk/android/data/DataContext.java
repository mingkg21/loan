package com.bk.android.data;

import java.util.ArrayList;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.Looper;

public class DataContext extends ContextWrapper {
	private static DataContext sDataContext;
	
	static DataContext getInstance(Context base){
		if(sDataContext == null){
			sDataContext = new DataContext(base.getApplicationContext());
		}
		return sDataContext;
	}
	
	private DataService mDataService;
	private ArrayList<ReceiverInfo> mReceivers = new ArrayList<ReceiverInfo>();
	private Handler mHandler = new Handler(Looper.getMainLooper());

	private DataContext(Context base) {
		super(base);
		mDataService = new DataService();
		mDataService.onCreate(this);
	}
	
	@Override
	public void sendBroadcast(final Intent intent) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				for (int i = 0;i >= 0 && i < mReceivers.size();i++) {
					ReceiverInfo oldReceiver = mReceivers.get(i);
					if(oldReceiver != null){
						if(oldReceiver.filter != null && oldReceiver.receiver != null && oldReceiver.filter.hasAction(intent.getAction())){
							oldReceiver.receiver.onReceive(getBaseContext(), intent);
						}
						if(oldReceiver.receiver == null){
							mReceivers.remove(i);
							i--;
						}
					}
				}
			}
		});
	}
	
	@Override
	public Intent registerReceiver(final BroadcastReceiver receiver,
			final IntentFilter filter) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if(receiver != null && !containsReceiver(receiver)){
					mReceivers.add(new ReceiverInfo(receiver, filter));
				}
			}
		});
		return null;
	}
	
	@Override
	public void unregisterReceiver(final BroadcastReceiver receiver) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				for (int i = 0;i < mReceivers.size();i++) {
					ReceiverInfo oldReceiver = mReceivers.get(i);
					if(oldReceiver != null && oldReceiver.receiver != null && oldReceiver.receiver.equals(receiver)){
						oldReceiver.receiver = null;
						oldReceiver.filter = null;
					}
				}
			}
		});
	}
	
	@Override
	public ComponentName startService(final Intent service) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				mDataService.onStart(service, 0);
			}
		});
		return null;
	}
	
	@Override
	public boolean stopService(Intent name) {
		return true;
	}
	
	@Override
	public void unbindService(ServiceConnection conn) {
	}
	
	private boolean containsReceiver(BroadcastReceiver receiver){
		for (int i = 0;i < mReceivers.size();i++) {
			ReceiverInfo oldReceiver = mReceivers.get(i);
			if(oldReceiver != null && oldReceiver.receiver != null && oldReceiver.receiver.equals(receiver)){
				return true;
			}
		}
		return false;
	}
	
	private final void runOnUiThread(Runnable action) {
        if (Thread.currentThread() != mHandler.getLooper().getThread()) {
            mHandler.post(action);
        } else {
            action.run();
        }
    }
	
	private class ReceiverInfo{
		private BroadcastReceiver receiver;
		private IntentFilter filter;
		public ReceiverInfo(BroadcastReceiver receiver, IntentFilter filter) {
			this.receiver = receiver;
			this.filter = filter;
		}
	}
}
