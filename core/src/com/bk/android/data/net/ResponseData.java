package com.bk.android.data.net;

import org.apache.http.Header;
import org.apache.http.util.ByteArrayBuffer;

/**
 * 响应的实体
 * 
 * @author mingkg21
 * @date 2010-3-31
 * @email mingkg21@gmail.com
 */
public class ResponseData {
	public String resultCode = "";
	public ByteArrayBuffer resultContent;
	public Header[] requestHeaders;
	public Header[] responseHeaders;

	/** 请求的Action */
	public String action;
}
