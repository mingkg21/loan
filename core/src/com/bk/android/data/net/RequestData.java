package com.bk.android.data.net;

import java.lang.ref.WeakReference;
import java.util.HashMap;


/**
 * 请求消息封装
 * 
 * @author mingkg21
 * @email mingkg21@gmail.com
 * @date 2010-4-6
 */
public class RequestData {
	/** 请求方式：POST方式 */
	public static final String REQUEST_METHOD_POST = "POST";
	/** 请求方式：GET方式 */
	public static final String REQUEST_METHOD_GET = "GET";

	/** 请求的Action */
	private String action;
	/** 请求方法 */
	private String requestMethod;
	/** 请求消息体*/
	private String sendData;
	/** 复合数据*/
	private HashMap<String, String> sendCompositeData;
	/** 进度回调*/
	private WeakReference<ProgressCallback> mProgressCallback;

	private String baseUrl;

	private HashMap<String, Object> sendDataMap;
	
	public RequestData(String requestMethod, String sendData, String action){
		this.requestMethod = requestMethod;
		this.sendData = sendData;
		this.action = action;
	}

	public RequestData(String action, HashMap<String, Object> sendDataMap){
		this.requestMethod = REQUEST_METHOD_POST;
		this.action = action;
		this.sendDataMap = sendDataMap;
	}

	public RequestData(String requestMethod, String baseUrl){
		this.requestMethod = requestMethod;
		this.baseUrl = baseUrl;
	}
	
	public RequestData(String requestMethod, HashMap<String, String> sendCompositeData, String action){
		this.requestMethod = requestMethod;
		this.sendCompositeData = sendCompositeData;
		this.action = action;
	}

	/**
	 * @return the action
	 */
	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	/**
	 * @return the requestMethod
	 */
	public String getRequestMethod() {
		return requestMethod;
	}

	/**
	 * @return the sendData
	 */
	public String getSendData() {
		return sendData;
	}

	public String getBaseUrl() {
		return baseUrl;
	}

	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}

	/**
	 * @return the sendExtraFileData
	 */
	public HashMap<String, String> getSendCompositeData() {
		return sendCompositeData;
	}

	public HashMap<String, Object> getSendDataMap() {
		return sendDataMap;
	}

	public RequestData setProgressCallback(ProgressCallback c){
		mProgressCallback = new WeakReference<RequestData.ProgressCallback>(c);
		return this;
	}
	
	public void onUploadProgress(int p,int m){
		if(mProgressCallback != null && mProgressCallback.get() != null){
			mProgressCallback.get().onUploadProgress(p, m);
		}
	}
	
	public interface ProgressCallback{
		public void onUploadProgress(int p,int m);
	}
}
