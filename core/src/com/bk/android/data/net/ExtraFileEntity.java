package com.bk.android.data.net;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map.Entry;

import org.apache.http.entity.AbstractHttpEntity;

import android.text.TextUtils;

public class ExtraFileEntity extends AbstractHttpEntity implements Cloneable{
	private HashMap<String, File> mFileMap;
	private HashMap<String, String> mParamMap;
	private String end = "\r\n";
	private String twoHyphens = "--";
	private String boundary = "---------------------------7d4a6d158c9";
	private RequestData mRequestData;
	
	public ExtraFileEntity(RequestData requestData) throws UnsupportedEncodingException{
		HashMap<String, String> extraFileData = requestData.getSendCompositeData();
		if (extraFileData == null) {
            throw new IllegalArgumentException("requestData may not be null");
        }
		mRequestData = requestData;
		mFileMap = new HashMap<String, File>();
		mParamMap = new HashMap<String, String>();
		boolean isFile = false;
		for(Entry<String, String> entry: extraFileData.entrySet()){
			isFile = false;
			String dataString = entry.getValue() ;
			if(!TextUtils.isEmpty(dataString)){
				int start = dataString.indexOf("file://");
				if(start != -1){
					dataString = dataString.substring("file://".length());
				}
				File file = new File(dataString);
				if(file.exists() && file.isFile()){
					mFileMap.put(entry.getKey(), file);
					isFile = true;
				}
				if(!isFile){
					mParamMap.put(entry.getKey(), entry.getValue());
				}
			}
		}
		setContentType("multipart/form-data;charset=UTF-8;boundary="+boundary);
		mRequestData.onUploadProgress(0, mFileMap.size());
	}
	
	@Override
	public boolean isRepeatable() {
		return true;
	}

	@Override
	public long getContentLength() {
		return -1;
	}

	@Override
	public InputStream getContent() throws IOException,
			IllegalStateException {
		return null;
	}

	@Override
	public void writeTo(OutputStream outstream) throws IOException {
		if (outstream == null) {
            throw new IllegalArgumentException("Output stream may not be null");
        }
		
		DataOutputStream ds = new DataOutputStream(outstream);
		StringBuffer params = new StringBuffer();
		// 上传的表单参数部分，格式请参考文章
		for (Entry<String, String> entry : mParamMap.entrySet()) {// 构建表单字段内容
			params.append(twoHyphens + boundary + end);  
			params.append("Content-Disposition: form-data; name=\""
					+ entry.getKey() + "\"" + end);
			params.append(end); 
			params.append(entry.getValue());
			params.append(end);
		}
//		params.append("--");      
//		params.append(boundary);      
//		params.append("\r\n");
		ds.write(params.toString().getBytes());
//		ds.writeBytes(twoHyphens + boundary + end);
		for(Entry<String, File> entry: mFileMap.entrySet()){
			writeFile(ds,entry.getKey(),entry.getValue());
		}
		
		ds.writeBytes(twoHyphens + boundary + twoHyphens + end);
		ds.flush();
	}
	
	int i = 0;
	private void writeFile(DataOutputStream outstream,String fileKey,File file) throws IOException{
		outstream.writeBytes(twoHyphens + boundary + end);
		outstream.writeBytes("Content-Disposition: form-data; "
				+ "name=\"" + fileKey + "\";filename=\"" + file.getName() + "\"" + end);
//		outstream.writeBytes("Content-Disposition: form-data;"
//				+ "name=\"" + fileKey + "\";" + end);
		outstream.writeBytes(end);
		/* 取得文件的FileInputStream */
		FileInputStream fStream = new FileInputStream(file);
		/* 设置每次写入1024bytes */
		int bufferSize = 1024;
		byte[] buffer = new byte[bufferSize];
		long length = file.length();
		long curlength = 0;
		int line = -1;
		/* 从文件读取数据至缓冲区 */
		while ((line = fStream.read(buffer)) != -1) {
			/* 将资料写入DataOutputStream中 */
			outstream.write(buffer, 0, line);
			if(length > 0){
				curlength += line;
				mRequestData.onUploadProgress((int) ((curlength * 1f / length) * 100 + i * 100), mFileMap.size() * 100);
			}
		}
		outstream.writeBytes(end);
		/* close streams */
		fStream.close();
		i++;
	}
	
	@Override
	public boolean isStreaming() {
		return false;
	}
}
