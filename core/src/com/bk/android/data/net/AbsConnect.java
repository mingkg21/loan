package com.bk.android.data.net;

import android.content.Context;
import android.text.TextUtils;

import com.bk.android.util.ApnUtil;

import org.apache.http.client.params.HttpClientParams;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.Proxy.Type;
import java.net.URL;

public abstract class AbsConnect {
	public static final int TIME_OUT_CON = 30 * 1000;
	public static final int TIME_OUT_SO = 2 * 60 * 1000;
	
	public static DefaultHttpClient getDefaultHttpClient(Context context) {
		DefaultHttpClient httpClient = getHttpClient();
//		System.getProperties().remove("proxySet");
//    	System.getProperties().remove("http.proxyHost");
//    	System.getProperties().remove("http.proxyPort");
//		if (!ApnUtil.isWifiWork(context)) {
//			String proxyHost = android.net.Proxy.getDefaultHost();
//			if (proxyHost != null) {
//				HttpHost proxy = new HttpHost(proxyHost, 80);
//				httpClient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);
//			}
//		}
		return httpClient;
	}

	public static DefaultHttpClient getHttpClient() {
		// 创建 HttpParams 以用来设置 HTTP 参数（这一部分不是必需的）
		HttpParams params = new BasicHttpParams();
		// 设置连接超时和 Socket 超时，以及 Socket 缓存大小
		HttpConnectionParams.setConnectionTimeout(params, TIME_OUT_CON);
		HttpConnectionParams.setSoTimeout(params, TIME_OUT_SO);
		HttpConnectionParams.setSocketBufferSize(params, 8192);
		// 设置重定向，缺省为 true
		HttpClientParams.setRedirecting(params, true);
		DefaultHttpClient httpClient = new DefaultHttpClient(params);
		return httpClient;
	}
	
	public HttpURLConnection getURLConnection(Context context, URL url) throws Exception{
		// 创建一个 URL 连接，如果有代理的话可以指定一个代理。
		Proxy proxy = null;
		HttpURLConnection connection = null;
		if (!ApnUtil.isWifiWork(context)) {
			String proxyHost = android.net.Proxy.getDefaultHost();
			if (proxyHost != null) {
				proxy = new Proxy(Type.HTTP, new InetSocketAddress(android.net.Proxy.getDefaultHost(), android.net.Proxy.getDefaultPort()));
			}
		}
		if(proxy != null){
			connection = (HttpURLConnection) url.openConnection(proxy);
		}else{
			connection = (HttpURLConnection) url.openConnection();
		}
		// 在开始和服务器连接之前，可能需要设置一些网络参数
		connection.setConnectTimeout(TIME_OUT_CON);
		connection.setDoOutput(true);// 使用 URL 连接进行输出
		connection.setDoInput(true);// 使用 URL 连接进行输入
		connection.setUseCaches(true);// 忽略缓存
		connection.setRequestMethod("GET");// 设置URL请求方法
		return connection;
	}
	
	public abstract ResponseData connection(RequestData requestData);
	
	public static String createSendData(String... keyValues){
		return createSendData(null,keyValues);
	}
	
	public static String createSendData(ParamEncipher paramEncipher,String... keyValues){
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < keyValues.length; i += 2) {
			String key = keyValues[i];
			String value = keyValues[i+1];
			if(key == null || value == null) {
				continue;
			}
			if(sb.length() != 0){
				sb.append("&");
			}
			sb.append(key+"=");
			if(paramEncipher != null){
				String newValue = paramEncipher.encrypt(key, value);
				if(TextUtils.isEmpty(newValue)){
					if(!TextUtils.isEmpty(value)){
						sb.append(value);
					}
				}else{
					sb.append(newValue);
				}
			}else{
				if(!TextUtils.isEmpty(value)){
					sb.append(value);
				}
			}
		}
		return sb.toString();
	}
	
	public interface ParamEncipher{
		public String encrypt(String key,String value);
	}
}
