package com.bk.android.data;

import java.io.Serializable;
import java.util.HashMap;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import android.util.SparseArray;

import com.bk.android.util.LogUtil;
/**
 * 数据服务，负责执行获取数据的逻辑
 * @author linyiwei
 */
public final class DataService implements DataManage{
	private static final String TAG = "DataService";

	static final int HANDLER_WHAT_PROGRESS = 0;
	static final int HANDLER_WHAT_RESULT = 1;

	/** 通知缓存数据变更*/
	static final String ACTION_NOTIFY_DATA_CHANGE = "ACTION_NOTIFY_DATA_CHANGE";
	/** 同步缓存数据*/
	static final String ACTION_SYNC_DATA_CACHE = "ACTION_SYNC_DATA_CACHE";
	/** 清除缓存数据*/
	static final String ACTION_CLEAR_DATA_CACHE = "ACTION_CLEAR_DATA_CACHE";
	/** 清除缓存数据*/
	static final String ACTION_SET_DATA_CACHE_TIMEOUT = "ACTION_SET_DATA_CACHE_TIMEOUT";
	/** 请求数据*/
	static final String ACTION_REQUEST_DATA = "ACTION_REQUEST_DATA";
	/** 请求终止请求数据*/
	static final String ACTION_REQUEST_STOP_TASK = "ACTION_REQUEST_STOP_TASK";
	/** 数据请求完成*/
	static final String ACTION_RESULT_REQUEST_FINISH = "ACTION_RESULT_REQUEST_FINISH";
	/** 数据请求结果*/
	static final String ACTION_RESULT_REQUEST = "ACTION_RESULT_REQUEST";
	/** 相应进度*/
	static final String ACTION_RESULT_PROGRESS = "ACTION_RESULT_PROGRESS";
	
	/** 请求任务*/
	static final String EXTRA_PROVIDER = "EXTRA_PROVIDER";
	/** 待同步的数据*/
	static final String EXTRA_SYNC_DATA = "EXTRA_SYNC_DATA";
	/** 同步是否需要通知*/
	static final String EXTRA_SYNC_NOTICE_GROUP_KEY = "EXTRA_SYNC_NOTICE_GROUP_KEY";
	/** 是否强制重新获取数据，否则有缓存就不在获取*/
	static final String EXTRA_FORCE_START = "EXTRA_FORCE_START";
	/** 请求任务状态*/
	static final String EXTRA_STATE = "EXTRA_STATE";
	/** 请求任务ID*/
	static final String EXTRA_KEY = "EXTRA_KEY";
	/** 请求任务组ID*/
	static final String EXTRA_GROUP_KEY = "EXTRA_GROUP_KEY";
	/** 数据*/
	static final String EXTRA_DATA = "EXTRA_DATA";
	/** 请求客户端ID*/
	static final String EXTRA_CLIENT_ID = "EXTRA_CLIENT_ID";
	/** 当前进度*/
	static final String EXTRA_PROGRESS = "EXTRA_PROGRESS";
	/** 最多进度*/
	static final String EXTRA_MAX_PROGRESS = "EXTRA_MAX_PROGRESS";

	private DataService this_ = this;
	private SparseArray<SparseArray<DataResult<Object>>> mCache;
	private SparseArray<SparseArray<TaskInfo>> mDataGroupTasks;
	private HashMap<String, TaskInfo> mDataTasks;

	private Handler mHandler = new Handler(Looper.getMainLooper()){
		@Override
		public void handleMessage(Message msg) {
			if(msg.what == HANDLER_WHAT_PROGRESS){
				ProgressInfo data = (ProgressInfo) msg.obj;
				Intent intent = new Intent(ACTION_RESULT_PROGRESS);
				intent.putExtra(EXTRA_KEY, data.mDataKey);
				intent.putExtra(EXTRA_PROGRESS, data.mProgress);
				intent.putExtra(EXTRA_MAX_PROGRESS, data.mMaxProgress);
				sendBroadcast(intent);
			}
		}
	};

	private Context mContext;

	public void onCreate(Context context) {
		mContext = context;
		mDataGroupTasks = new SparseArray<SparseArray<TaskInfo>>();
		mDataTasks = new HashMap<String, TaskInfo>();
		mCache = new SparseArray<SparseArray<DataResult<Object>>>();
	}
	
	public void onStart(Intent intent, int startId) {
		if(intent == null){
			return;
		}
		String action = intent.getAction();
		if(action == null){
			return;
		}
		Integer clientId = (Integer) intent.getSerializableExtra(EXTRA_CLIENT_ID);
		if(clientId == null){
			return;
		}
		if(ACTION_REQUEST_DATA.equals(action)){
			BaseDataRequest dateProvider = (BaseDataRequest) intent.getSerializableExtra(EXTRA_PROVIDER);
			if(dateProvider == null){
				return;
			}
			boolean forceStart = intent.getBooleanExtra(EXTRA_FORCE_START, false);
			handlerRequestData(dateProvider,forceStart,clientId);
		}else if(ACTION_REQUEST_STOP_TASK.equals(action)){
			String key = intent.getStringExtra(EXTRA_KEY);
			if(key == null){
				return;
			}
			stopTask(key);
		}else if(ACTION_SYNC_DATA_CACHE.equals(action)){
			BaseDataRequest dateProvider = (BaseDataRequest) intent.getSerializableExtra(EXTRA_PROVIDER);
			if(dateProvider == null){
				return;
			}
			Serializable data = intent.getSerializableExtra(EXTRA_SYNC_DATA);
			if(data == null){
				return;
			}
			String noticeGroupKey = intent.getStringExtra(EXTRA_SYNC_NOTICE_GROUP_KEY);
			handlerSyncData(dateProvider, data,noticeGroupKey,clientId);
		}else if(ACTION_CLEAR_DATA_CACHE.equals(action)){
			String groupKey = intent.getStringExtra(EXTRA_GROUP_KEY);
			if(groupKey != null){
				clearData(groupKey.hashCode());
			}else{
				mCache.clear();
			}
		}else if(ACTION_SET_DATA_CACHE_TIMEOUT.equals(action)){
			String groupKey = intent.getStringExtra(EXTRA_GROUP_KEY);
			if(groupKey != null){
				SparseArray<DataResult<Object>> groupCache = mCache.get(groupKey.hashCode());
				if(groupCache != null){
					for (int i = 0; i < groupCache.size(); i++) {
						DataResult<Object> data = groupCache.valueAt(i);
						if(data != null){
							data.setCacheTime(0);
							DBCache.getInstance().saveData(groupCache.keyAt(i),groupKey.hashCode(),data);
						}
					}
				}
			}
		}
	}

	private void handlerSyncData(BaseDataRequest dateProvider,Serializable data,String noticeGroupKey,Integer clientId){
		DataResult<Object> dataResult = getData(dateProvider);
		if(dataResult != null && dataResult.getData() != null){
			if(data.getClass().equals(dataResult.getData().getClass())){
				dataResult.setData(data);
			}else{
				dateProvider.handlerSyncData(dataResult,data);
			}
			int dataGroupKey = dateProvider.getDataGroupKey().hashCode();
			int key = dateProvider.getDataKey().hashCode();
			if(DBCache.getInstance().hasData(key,dataGroupKey)){
				DBCache.getInstance().saveData(key,dataGroupKey, dataResult);
			}
		}
		
		if(!TextUtils.isEmpty(noticeGroupKey)){
			notifyDataChange(new String[]{noticeGroupKey},clientId,true);
		}
	}
	
	private void handlerRequestData(BaseDataRequest dateProvider,boolean forceStart,int clientId){
//		synchronized (mDataGroupTasks) {
			LogUtil.i(TAG,"DataService RequestData provider="+dateProvider.getClass().getSimpleName());
			DataResult<Object> dataResult = null;
			dataResult = getData(dateProvider);
			if(dataResult != null){
				if(dataResult.getDataType() == DataResult.DATA_NORMAL && !forceStart){
					Intent intent = new Intent(ACTION_RESULT_REQUEST);
					intent.putExtra(EXTRA_CLIENT_ID, clientId);
					intent.putExtra(EXTRA_KEY, dateProvider.getDataKey());
					intent.putExtra(EXTRA_STATE, ServiceProxy.REQUEST_STATE_SUCCESS);
					packageData(intent,dateProvider.getDataKey(), dataResult, false);
					sendBroadcast(intent);
					return;
				}
			}
			int groupKey = dateProvider.getDataGroupKey().hashCode();
			int key = dateProvider.getDataKey().hashCode();
			SparseArray<TaskInfo> taskInfos = mDataGroupTasks.get(groupKey);
			if(taskInfos == null){
				taskInfos = new SparseArray<TaskInfo>();
				mDataGroupTasks.put(groupKey, taskInfos);
			}
			TaskInfo dataTask = taskInfos.get(key);
			if(dataTask == null){
				dataTask = new TaskInfo(dateProvider,this_);
				taskInfos.put(key, dataTask);
				mDataTasks.put(dateProvider.getDataKey(), dataTask);
			}
			Intent intent = new Intent(ACTION_RESULT_REQUEST);
			intent.putExtra(EXTRA_CLIENT_ID, clientId);
			intent.putExtra(EXTRA_KEY, dateProvider.getDataKey());
			packageData(intent,dateProvider.getDataKey(), dataResult, false);
			intent.putExtra(EXTRA_STATE, dataResult == null ? 
					ServiceProxy.REQUEST_STATE_STARTING : ServiceProxy.REQUEST_STATE_STARTING_BACKGROUND);
			sendBroadcast(intent);
			if(dataTask.getRequestState() != ServiceProxy.REQUEST_STATE_STARTING){
				dataTask.start(mHandler,clientId);
			}
//		}
	}
	
	private void packageData(Intent intent,String key,DataResult<Object> dataResult,boolean isNew){
		if(dataResult != null && dataResult.getData() != null){
			try{
				dataResult.setNewData(isNew);
				intent.putExtra(EXTRA_DATA, dataResult);
	        }catch(Exception e){ 
	            e.printStackTrace(); 
	        } 
		}
	}
	
	public void clearData(int groupKey){
//		synchronized (mCache) {
			mCache.remove(groupKey);
//		}
	}
	
	public DataResult<Object> getData(BaseDataRequest dateProvider){
//		synchronized (mCache) {
			DataResult<Object> data = null;
			int dataGroupKey = dateProvider.getDataGroupKey().hashCode();//1293990239
			int key = dateProvider.getDataKey().hashCode();//-1194372428
			SparseArray<DataResult<Object>> groupCache = mCache.get(dataGroupKey);
			if(groupCache != null){
				data = groupCache.get(key);
			}
			if(data == null){
				data = DBCache.getInstance().getData(key,dataGroupKey);
				if(data != null){
					if(groupCache == null){
						groupCache = new SparseArray<DataResult<Object>>();
						mCache.put(dataGroupKey,groupCache);
					}
					if(dateProvider.isNeedSaveCache()){
						groupCache.put(key, data);
					}
				}
			}
			return data;
//		}
	}
	
	public void saveData(TaskInfo taskInfo,DataResult<Object> dataResult){
		if(taskInfo.getCacheTime() != 0){
			int dataGroupKey = taskInfo.getDataGroupKey().hashCode();
			int key = taskInfo.getDataKey().hashCode();
			SparseArray<DataResult<Object>> groupCache = mCache.get(dataGroupKey);
			if(groupCache == null){
				groupCache = new SparseArray<DataResult<Object>>();
				mCache.put(dataGroupKey,groupCache);
			}
			boolean hasData = false;
			if(groupCache.size() > 0){
				DataResult<Object> dataResultTemp = groupCache.valueAt(0);
				if(groupCache.keyAt(0) != key){
					dataResult.setCreateTime(dataResultTemp.getCreateTime());
					hasData = true;
				}else{
					hasData = groupCache.size() > 1;
				}
			}
			if(!hasData){
				if(taskInfo.isNeedSaveDB()){
					//只保留列表请求的第一页，因为所有请求必须从第一开始所以groupCache.size() == 0的时候就是第一页
					DBCache.getInstance().saveData(key,dataGroupKey, dataResult);
				}
				if(taskInfo.isNeedSaveCache()){
					groupCache.put(key,dataResult);
				}
			}
		}
	}
	
	public void stopTask(String key){
//		synchronized (mDataTasks) {
			TaskInfo dataTask = mDataTasks.remove(key);
			if(dataTask != null){
				int groupKey = dataTask.getDataGroupKey().hashCode();
				SparseArray<TaskInfo> tasks = mDataGroupTasks.get(groupKey);
				if(tasks != null){
					mDataGroupTasks.remove(groupKey);
					for (int i = 0; i < tasks.size(); i++) {
						TaskInfo task = tasks.valueAt(i);
						task.cancel();
					}
				}
			}
//		}
	}
	
	@Override
	public void submitResultInThread(TaskInfo taskInfo, Serializable data,boolean isNeedSave) {
		DataResult<Object> dataResult = new DataResult<Object>(taskInfo.getCacheTime(),data);
		if(isNeedSave){
			saveData(taskInfo, dataResult);
		}
	}
	
	@Override
	public void submitResult(TaskInfo taskInfo, Serializable data,boolean isNeedSave) {
		DataResult<Object> dataResult = new DataResult<Object>(taskInfo.getCacheTime(),data);
		Intent intent = new Intent(ACTION_RESULT_REQUEST_FINISH);
		intent.putExtra(EXTRA_KEY, taskInfo.getDataKey());
		intent.putExtra(EXTRA_STATE, taskInfo.getRequestState());
		intent.putExtra(EXTRA_CLIENT_ID, taskInfo.getClientId());
		packageData(intent,taskInfo.getDataKey(), dataResult,true);
		sendBroadcast(intent);
		if(dataResult.getData() != null){
			if(isNeedSave){
				notifyDataChange(taskInfo.getNeedNotifyGroupKeys(),taskInfo.getClientId(),false);
			}
		}
	}

	private void notifyDataChange(String[] needNotifyGroupKeys,Integer clientId,boolean isNeedClear){
		if(needNotifyGroupKeys != null){
			for (String groupKey : needNotifyGroupKeys) {
				if(isNeedClear){
					clearData(groupKey.hashCode());
				}
				SparseArray<TaskInfo> tasks = mDataGroupTasks.get(groupKey.hashCode());
				if(tasks != null){
					mDataGroupTasks.remove(groupKey.hashCode());
					for (int i = 0; i < tasks.size(); i++) {
						TaskInfo task = tasks.valueAt(i);
						mDataTasks.remove(task.getDataKey());
						task.cancel();
					}
				}
				Intent intent = new Intent(ACTION_NOTIFY_DATA_CHANGE);
				intent.putExtra(EXTRA_GROUP_KEY,groupKey);
				intent.putExtra(EXTRA_CLIENT_ID,clientId);
				sendBroadcast(intent);
			}
		}
	}
	
	@Override
	public Context getContext() {
		return mContext;
	}

	private void sendBroadcast(Intent intent) {
		mContext.sendBroadcast(intent);
	}
}
