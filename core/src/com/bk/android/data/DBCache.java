package com.bk.android.data;

import java.util.ArrayList;

import android.text.TextUtils;

import com.bk.android.dao.DBKeyValueBlobProviderProxy;
import com.bk.android.util.LogUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class DBCache {
	private static final String DATA_RESULT_KEY_PRE = "DATA_RESULT_KEY_PRE";
	private static final String DATA_KEY_PRE = "DATA_KEY_PRE";
	private static final String DATA_KEY_TYPE = "DATA_KEY_TYPE";

	private static DBCache instance;
	
	private Gson mGson;
	private Gson mExposeGson;
	
	private DBCache() {
		mGson = new Gson();
		mExposeGson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
	}
	
	static DBCache getInstance() {
		if(instance == null) {
			instance = new DBCache();
		}
		return instance;
	}
	
	private static DBKeyValueBlobProviderProxy getProxy(){
		return DBCacheProvider.getProxy();
	}
	
	boolean hasData(int key,int groupKey){
		return getProxy().hasData(DATA_KEY_PRE + key,groupKey+"");
	}
	
	void saveData(int key,int groupKey,DataResult<Object> data) {
		long time = System.currentTimeMillis();
		String dataResultValue = mExposeGson.toJson(data, DataResult.class);
		String dataValue = mGson.toJson(data.getData(), data.getDataClass());

		getProxy().putString(DATA_RESULT_KEY_PRE + key, dataResultValue, groupKey+"");
		getProxy().putString(DATA_KEY_PRE + key, dataValue, groupKey+"");
		getProxy().putInt(key+"", groupKey,DATA_KEY_TYPE);
		LogUtil.i("DBCache", "saveData time="+(System.currentTimeMillis() - time));
	}
	
	@SuppressWarnings("unchecked")
	DataResult<Object> getData(int key,int groupKey){
		long time = System.currentTimeMillis();
		String dataValue = getProxy().getString(DATA_KEY_PRE + key,groupKey+"", null);
		String dataResultValue = getProxy().getString(DATA_RESULT_KEY_PRE + key,groupKey+"", null);
		DataResult<Object> dataResult = null;
		if(!TextUtils.isEmpty(dataResultValue)){
			try {
				dataResult = mExposeGson.fromJson(dataResultValue, DataResult.class);
			} catch (Exception e) {
				LogUtil.e("DBCache", e);
			} catch (Error e) {
				LogUtil.e("DBCache", e);
			}
		}
		LogUtil.i("DBCache", "getData time="+(System.currentTimeMillis() - time));
		if(dataResult != null){
			return (DataResult<Object>) dataStrToData(dataResult,dataValue,dataResult.getDataClass());
		}else{
			return null;
		}
	}
	
	ArrayList<String> getAllKey(){
		return getProxy().getAllKeyByType(DATA_KEY_TYPE,100);
	}
	
	int getGroupKey(String key){
		return getProxy().getInt(key,DATA_KEY_TYPE, -1);
	}
	
	<T> DataResult<T> dataStrToData(DataResult<T> dataResult,String dataValue,Class<T> clazz){
		T data = null;
		if(!TextUtils.isEmpty(dataValue)){
			try {
				data = mGson.fromJson(dataValue, clazz);
			} catch (Exception e) {
				LogUtil.e("DBCache", e);
			} catch (Error e) {
				LogUtil.e("DBCache", e);
			}
		}
		if(data != null){
			dataResult.setData(data);
			return dataResult;
		}
		return null;
	}
}
