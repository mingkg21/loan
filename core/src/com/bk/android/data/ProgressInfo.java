package com.bk.android.data;

class ProgressInfo {
	String mDataKey;
	int mClientId;
	int mProgress;
	int mMaxProgress;
	public ProgressInfo(String mDataKey, int mClientId, int mProgress,
			int maxProgress) {
		this.mDataKey = mDataKey;
		this.mClientId = mClientId;
		this.mProgress = mProgress;
		this.mMaxProgress = maxProgress;
	}
}
