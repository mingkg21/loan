package com.bk.android.data;

import java.io.Serializable;

import com.bk.android.app.BaseApp;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * 数据结果
 * @author linyiwei
 */
public final class DataResult<T> implements Serializable{
	private static final long serialVersionUID = -7771815530568713559L;
	/** 数据类型：值 没数据*/
	public static final int DATA_NULL = -1;
	/** 数据类型：值 未过期*/
	public static final int DATA_NORMAL = 0;
	/** 数据类型：值 过期缓存*/
	public static final int DATA_STALE_CACHE = 1;
	
	/** 缓存有效时间*/
	@Expose
	@SerializedName("mCacheTime")
	private int mCacheTime;
	/** 缓存创建时间*/
	@Expose
	@SerializedName("mCreateTime")
	private long mCreateTime;
	/** 数据类型*/
	@Expose
	@SerializedName("mDataClass")
	private String mDataClass;
	/** 返回结果*/
	private T mData;
	/** 是否是刚刚从网络获取的新数据*/
	private boolean isNewData;

	public DataResult(){}
	
	public DataResult(int cacheTime,T data){
		setData(data);
		mCacheTime = cacheTime;
		mCreateTime = System.currentTimeMillis();
	}
	
	/** 是否是刚刚从网络获取的新数据*/
	public boolean isNewData(){
		return isNewData;
	}
	/** 是否是刚刚从网络获取的新数据*/
	public void setNewData(boolean newData){
		isNewData = newData;
	}
	/**
	 * @return
	 */
	public int getDataType() {
		if(mData != null){
			if(mCacheTime <= 0){
				return DATA_NORMAL;
			}
			long time = System.currentTimeMillis() - mCreateTime;
			if(time < getCacheTime() && time >= 0){
				return DATA_NORMAL;
			}else{
				return DATA_STALE_CACHE;
			}
		}
		return DATA_NULL;
	}
	
	void setCreateTime(long createTime){
		mCreateTime = createTime;
	}
	/**
	 * @return the mCacheTime
	 */
	long getCreateTime() {
		return mCreateTime;
	}
	
	void setCacheTime(int cacheTime){
		mCacheTime = cacheTime;
	}
	/**
	 * @return the mCacheTime
	 */
	int getCacheTime() {
		return Math.abs(mCacheTime);
	}
	/**
	 * @return the mData
	 */
	public T getData() {
		return mData;
	}
	
	public void setData(T data) {
		mData = data;
		if(mData != null){
			mDataClass = mData.getClass().getName();
		}else{
			mDataClass = null;
		}
	}
	
	@SuppressWarnings("unchecked")
	Class<T> getDataClass(){
		try {
			return (Class<T>) BaseApp.getInstance().getClassLoader().loadClass(mDataClass);
		} catch (ClassNotFoundException e) {}
		return null;
	}

	public boolean isNeedInfoData() {
		return mCacheTime > 0;
	}
}
