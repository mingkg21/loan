package com.bk.android.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.text.TextUtils;

public class ApnUtil {
	/**
	 * 判断是否启动WIFI
	 * 
	 * @param context
	 * @return
	 */
	public static boolean isWifiWork(Context context) {
		WifiManager wm = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
		if (wm == null) {// 有些手机阉割到WIFI，这里会为null。如：宇龙E230A
			return false;
		}
		if (wm.getWifiState() == WifiManager.WIFI_STATE_ENABLED) {
			ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
			if(cm != null){
				NetworkInfo networkInfo = cm.getActiveNetworkInfo();
				if (networkInfo != null) {
					String ei = networkInfo.getTypeName();
					if (!TextUtils.isEmpty(ei)) {
						int index = ei.toUpperCase().indexOf("WIFI");
						if (index > -1) {
							return true;
						}
					}
					return false;
				}
			}
			return true;
		}
		return false;
	}
	/**
	 * 判断是否有网络连接
	 * 
	 * @param context
	 * @return
	 */
	public static boolean isNetAvailable(final Context context) {
		NetworkInfo networkInfo = getActiveNetworkInfo(context);
		if (networkInfo != null) {
			if (networkInfo.getState().compareTo(NetworkInfo.State.CONNECTED) == 0) {
				return true;
			}
		}
		return false;
	}
	
	private static NetworkInfo getActiveNetworkInfo(Context context) {
		try {// ConnectivityManager 可能会为空
			ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo networkInfo = cm.getActiveNetworkInfo();
			return networkInfo;
		} catch (Exception e) {
			return null;
		}
	}

	public static int getConnectType(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = cm.getActiveNetworkInfo();
		if (networkInfo != null && networkInfo.isConnectedOrConnecting()) {
			return networkInfo.getType();
		}
		return -1;
	}
}
