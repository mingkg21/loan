package com.bk.android.util;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.AppTask;
import android.app.ActivityManager.MemoryInfo;
import android.app.ActivityManager.RecentTaskInfo;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.usage.UsageEvents;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;

import com.bk.android.app.BaseApp;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Enumeration;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class AppUtil {

	private static String sUMengChannel;
	private static String sChannelId;

	/**
	 * 是否模拟器
	 * @param context
	 * @return
	 */
	public static boolean isSimulator(Context context){
		TelephonyManager tm = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
		String deviceId = tm.getDeviceId();
		if(TextUtils.isEmpty(deviceId) || "000000000000000".equalsIgnoreCase(deviceId)){
			return true;
		}
		if("Android".equalsIgnoreCase(tm.getSimOperatorName())){
			return true;
		}
		if("310260".equalsIgnoreCase(tm.getSimOperator())){
			return true;
		}
		WifiManager wifiManager = (WifiManager)context.getSystemService(Context.WIFI_SERVICE);
		if(wifiManager == null){
			return true;
		}else{
			WifiInfo wifiInfo = wifiManager.getConnectionInfo();
			if(wifiInfo == null || TextUtils.isEmpty(wifiInfo.getMacAddress())){
				return true;
			}
		}
		return false;
	}

	/** 判断当前设备中有没有“有权查看使用权限的应用”这个选项 (5.0及5.0以上系统)
	 * 	这个选项是Android 系统中自带的，但是现在国内很多厂商ROM众多，很多都给阉割掉了，例如：小米、魅族只流。
	 *
	 * @param context
	 * @return
     */
	public static boolean hadUsageAccessSettings(Context context) {
		PackageManager packageManager = context.getApplicationContext().getPackageManager();
		Intent intent = new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS);
		List<ResolveInfo> list = packageManager.queryIntentActivities(intent,
				PackageManager.MATCH_DEFAULT_ONLY);
		return list.size() > 0;
	}

	/** 判断调用该设备中“有权查看使用权限的应用”这个选项的APP有没有打开
	 *
	 * @param context
	 * @return
     */
	public static boolean isOpenUsageAccessSettings(Context context) {
		if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.LOLLIPOP) {
			return true;
		}
		long ts = System.currentTimeMillis();
		UsageStatsManager usageStatsManager = (UsageStatsManager) context.getApplicationContext().getSystemService("usagestats");
		List<UsageStats> queryUsageStats = usageStatsManager.queryUsageStats(
				UsageStatsManager.INTERVAL_BEST, 0, ts);
		if (queryUsageStats == null || queryUsageStats.isEmpty()) {
			return false;
		}
		return true;
	}

	/** 跳转到“有权查看使用权限的应用”选项
	 *
	 * @param context
     */
	public static void gotoUsageAccessSettings(Context context) {
		Intent intent = new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS);
		context.startActivity(intent);
	}

	public static List<UsageStats> getRunningApp(Context context) {
		UsageStatsManager usm = (UsageStatsManager) context.getSystemService("usagestats");
		long time = System.currentTimeMillis();
		List<UsageStats> appList = usm.queryUsageStats(UsageStatsManager.INTERVAL_DAILY, time - 1000 * 1000, time);
		return appList;
	}

	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	public static String getTaskPackname(Context context) {
		String currentApp = "CurrentNULL";
		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
			List<UsageStats> appList = getRunningApp(context);
			if (appList != null && appList.size() > 0) {
				SortedMap<Long, UsageStats> mySortedMap = new TreeMap<Long, UsageStats>();
				for (UsageStats usageStats : appList) {
					mySortedMap.put(usageStats.getLastTimeUsed(), usageStats);
					LogUtil.v("AppUtil", "usage stats: " + usageStats.getPackageName());
				}
				if (mySortedMap != null && !mySortedMap.isEmpty()) {
					currentApp = mySortedMap.get(mySortedMap.lastKey()).getPackageName();
				}
				LogUtil.v("AppUtil", "current app : " + currentApp);
			}
		} else {
			ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
			List<ActivityManager.RunningAppProcessInfo> tasks = am.getRunningAppProcesses();
			currentApp = tasks.get(0).processName;
		}
		Log.e("TAG", "Current App in foreground is: " + currentApp);
		return currentApp;
	}

	public static String getLauncherTopApp(Context context) {
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
			ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
			List <ActivityManager.RunningTaskInfo> appTasks = activityManager.getRunningTasks(1);
			if (null != appTasks && !appTasks.isEmpty()) {
				return appTasks.get(0).topActivity.getPackageName();
			}
		} else {
			long endTime = System.currentTimeMillis();
			long beginTime = endTime - 1000 * 1000;
			UsageStatsManager usageStatsManager = (UsageStatsManager) context.getSystemService("usagestats");
			String result = "";
			String className = "";
			UsageEvents.Event event = new UsageEvents.Event();
			UsageEvents usageEvents = usageStatsManager.queryEvents(beginTime, endTime);
			while (usageEvents.hasNextEvent()) {
				usageEvents.getNextEvent(event);
				if (event.getEventType() == UsageEvents.Event.MOVE_TO_FOREGROUND) {
					result = event.getPackageName();
					className = event.getClassName();
				}
			}
			LogUtil.v("AppUtil", "package name: " + result);
			LogUtil.v("AppUtil", "class name: " + className);
			if (!android.text.TextUtils.isEmpty(result)) {
				return result;
			}
		}
		return "";
	}

	/**
	 * 从手机上读取是否已安装
	 * @param context
	 * @param packageName
	 * @return
	 */
	public static boolean isInstalled(Context context,String packageName){
		PackageInfo packageInfo;
        try {
            packageInfo = context.getPackageManager().getPackageInfo(packageName,PackageManager.GET_SIGNATURES);
        } catch (NameNotFoundException e) {
            packageInfo = null;
            e.printStackTrace();
        }
        if(packageInfo == null){
    		return false;
        }else{
    		return true;
        }
	}
	/**
	 * 获取ActivityManager
	 * @param context
	 * @return
	 */
	public static ActivityManager getActivityManager(Context context){
		return (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
	}
	/**
	 * 获取最近用户过的软件
	 * @param context
	 * @param taskSize
	 * @return
	 */
	public static List<RecentTaskInfo> getRecentTasks(Context context,int taskSize){
		return getActivityManager(context).getRecentTasks(taskSize,ActivityManager.RECENT_WITH_EXCLUDED);
	}
	/**
	 * 获取运行的应用程序
	 * @param context
	 * @param taskSize
	 * @return
	 */
	public static List<RunningTaskInfo> getRunningTasks(Context context,int taskSize){
		return getActivityManager(context).getRunningTasks(taskSize);
	}
	/**
	 * 获取最近用户过的软件
	 * @param context
	 * @return
	 */
	public static List<AppTask> getAppTasks(Context context){
		return getActivityManager(context).getAppTasks();
	}
	/**
	 * 获取当前运行进程数
	 * @param context
	 * @return
	 */
	public static List<RunningAppProcessInfo> getRunningAppProcesses(Context context){
		return getActivityManager(context).getRunningAppProcesses();
	}

	/** 判断应用程序的Activity是否在最前面
	 * @param context
	 * @param packageName
	 * @param activityName
     * @return
     */
	public static boolean isTopActivity(Context context, String packageName, String activityName) {
		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {//大于等于5.0
			long endTime = System.currentTimeMillis();
			long beginTime = endTime - 1000 * 1000;
			UsageStatsManager usageStatsManager = (UsageStatsManager) context.getSystemService("usagestats");
			String tmpPackageName = "";
			String tmpActivityName = "";
			UsageEvents.Event event = new UsageEvents.Event();
			UsageEvents usageEvents = usageStatsManager.queryEvents(beginTime, endTime);
			while (usageEvents.hasNextEvent()) {
				usageEvents.getNextEvent(event);
				if (event.getEventType() == UsageEvents.Event.MOVE_TO_FOREGROUND) {
					tmpPackageName = event.getPackageName();
					tmpActivityName = event.getClassName();
				}
			}
			if (packageName.equals(tmpPackageName) && activityName.equals(tmpActivityName)) {
				return true;
			}
		} else {
			ActivityManager mActivityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
			ComponentName topActivity = mActivityManager.getRunningTasks(1).get(0).topActivity;
			if (topActivity.getPackageName().equals(packageName) && topActivity.getClassName().equals(activityName)) {
				return true;
			}
		}
		return false;
	}

	public static String getTopActivityName(Context context) {
		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {//大于等于5.0
			long endTime = System.currentTimeMillis();
			long beginTime = endTime - 1000 * 1000;
			UsageStatsManager usageStatsManager = (UsageStatsManager) context.getSystemService("usagestats");
			String tmpPackageName = "";
			String tmpActivityName = "";
			UsageEvents.Event event = new UsageEvents.Event();
			UsageEvents usageEvents = usageStatsManager.queryEvents(beginTime, endTime);
			while (usageEvents.hasNextEvent()) {
				usageEvents.getNextEvent(event);
				if (event.getEventType() == UsageEvents.Event.MOVE_TO_FOREGROUND) {
					tmpPackageName = event.getPackageName();
					tmpActivityName = event.getClassName();
				}
			}
			return tmpActivityName;
		} else {
			ActivityManager mActivityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
			ComponentName topActivity = mActivityManager.getRunningTasks(1).get(0).topActivity;
			return topActivity.getClassName();
		}
	}

	/**
	 * 判断应用程序是否显示在最前端
	 * @param context
	 * @param packageName
	 * @return
	 */
	public static boolean isTopAppTask(Context context,String packageName){
		if (android.os.Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.LOLLIPOP) {//大于5.0
			List<UsageStats> appList = getRunningApp(context);
			if (appList != null && appList.size() > 0) {
				SortedMap<Long, UsageStats> mySortedMap = new TreeMap<Long, UsageStats>();
				for (UsageStats usageStats : appList) {
					mySortedMap.put(usageStats.getLastTimeUsed(), usageStats);
				}
				if (mySortedMap != null && !mySortedMap.isEmpty()) {
					if (mySortedMap.get(mySortedMap.lastKey()).getPackageName().equals(packageName)) {
						return true;
					}
				}
			}
		} else {
			List<RunningAppProcessInfo> processInfos = getRunningAppProcesses(context);
			if(processInfos != null){
				for (RunningAppProcessInfo processInfo : processInfos) {
					if(processInfo != null && processInfo.importance == RunningAppProcessInfo.IMPORTANCE_FOREGROUND
							&& processInfo.processName.equals(packageName)){
						return true;
					}
				}
			}
		}
		return false;
	}

	/** 安装之前的判断
	 *
	 * @param context
	 * @param packageName
     * @return
     */
	public static boolean isTopAppTaskForInstall(Context context,String packageName){
		List<RunningAppProcessInfo> processInfos = getRunningAppProcesses(context);
		if(processInfos != null){
			for (RunningAppProcessInfo processInfo : processInfos) {
				if(processInfo != null && processInfo.importance == RunningAppProcessInfo.IMPORTANCE_FOREGROUND
						&& processInfo.processName.equals(packageName)){
					return true;
				}
			}
		}
		return false;
	}

	/** 卸载软件
	 * @param context
	 * @param packageName
	 */
	public static void gotoUninstall(Context context, String packageName) {
		if(TextUtils.isEmpty(packageName)) {
			return;
		}
		Uri packageUri = Uri.parse("package:" + packageName);
		Intent intent = new Intent(Intent.ACTION_DELETE, packageUri);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(intent);
	}
	
	/** 安装软件
	 * @param context
	 * @param apkPath
	 */
	public static void gotoInstall(Context context, String apkPath) {
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.setDataAndType(Uri.parse("file://" + apkPath),"application/vnd.android.package-archive");  
		context.startActivity(intent); 
	}
	/**
	 * 获取root权限
	 * @param context
	 * @return
	 */
	public static boolean getRootPermission(Context context) {  
	    Process process = null;  
	    DataOutputStream os = null;  
	    try {  
	        String cmd="chmod 777 " + context.getPackageCodePath();  
	        process = Runtime.getRuntime().exec("su"); //切换到root帐号  
	        os = new DataOutputStream(process.getOutputStream());  
	        os.writeBytes(cmd + "\n");  
	        os.writeBytes("exit\n");  
	        os.flush();  
	        process.waitFor();
	    } catch (Exception e) {
	        return false;  
	    } finally {
	        try {
	            if (os != null) {  
	                os.close();  
	            }  
	            process.destroy();  
	        } catch (Exception e) {  
	        }  
	    }  
	    return true;  
	}  
	/**
	 * 静默安装
	 * @param context
	 * @param apkPath
	 * @return
	 */
	public static boolean installApk(Context context, String apkPath) {
		 return execRootCmdSilent("pm install -r " + apkPath) == 0;
	}
	
	public static int execRootCmdSilent(String cmd) {
		int result = -1;
		DataOutputStream dos = null;
		try {
			Process p = Runtime.getRuntime().exec("su");
			dos = new DataOutputStream(p.getOutputStream());
			dos.writeBytes(cmd + "\n");
			dos.flush();
			dos.writeBytes("exit\n");
			dos.flush();
			p.waitFor();
			result = p.exitValue();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (dos != null) {
				try {
					dos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return result;
	}
	/**
	 * 打开应用程序
	 * @param context
	 * @param packageName
	 */
	public static void openApp(Context context,String packageName){
		try{
	        Intent intent = context.getPackageManager().getLaunchIntentForPackage(packageName);
	        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	        context.startActivity(intent);
	    }catch(Exception e){
	    }
	}
	/** 到系统的设置网络界面
	 * @param context
	 */
	public static void gotoSettingActivity(Context context){
		try {
			String action = android.provider.Settings.ACTION_WIRELESS_SETTINGS;
			if(Build.VERSION.SDK_INT >= 14){//4.0的设置
				action = android.provider.Settings.ACTION_SETTINGS;
			}
			Intent intent = new Intent(action);
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.startActivity(intent);
		} catch (Exception e) {
		}
	}
	
	/**
	 * 获取APK信息
	 * @param context
	 */
	public static String getApkVersionName(Context context) {
		PackageManager pm = context.getPackageManager();
		String packageName = context.getPackageName();
		PackageInfo apkInfo = null;
		try {
			apkInfo = pm.getPackageInfo(packageName, 0);
		} catch (NameNotFoundException e) {
			apkInfo = null;
			e.printStackTrace();
		}
		String versionName = "0.0.0";
		
		if (apkInfo != null && !TextUtils.isEmpty(apkInfo.versionName)) {
			versionName = apkInfo.versionName;
		}
		
		return versionName;
	}
	
	/**
	 * 获取APK信息
	 * @param context
	 */
	public static int getApkVersionCode(Context context) {
		PackageManager pm = context.getPackageManager();
		String packageName = context.getPackageName();
		PackageInfo apkInfo = null;
		try {
			apkInfo = pm.getPackageInfo(packageName, 0);
		} catch (NameNotFoundException e) {
			apkInfo = null;
			e.printStackTrace();
		}
		
		return apkInfo != null ? apkInfo.versionCode : 0;
	}

	private static String getChannelFilePath() {
		return BaseApp.getInstance().getAppSdcardDir() + "channel";
	}

	private static String getUMengChannel(Context context, final String matchStr) {
		String result = null;
		ApplicationInfo appinfo = context.getApplicationInfo();
		String sourceDir = appinfo.sourceDir;
		ZipFile zipfile = null;
		try {
			zipfile = new ZipFile(sourceDir);
			Enumeration<?> entries = zipfile.entries();
			while (entries.hasMoreElements()) {
				ZipEntry entry = ((ZipEntry) entries.nextElement());
				String entryName = entry.getName();
				if (entryName.contains(matchStr)) {
					result = entryName.replaceAll(matchStr, "");
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (zipfile != null) {
				try {
					zipfile.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return result;
	}
	
	public static String getUMengChannel(Context context) {
		if(sUMengChannel != null){
			return sUMengChannel;
		}

//		//从SDCARD读取渠道号，用于覆盖安装后还是之前的渠道号
//		sUMengChannel = FileUtil.inPutFileToStr(getChannelFilePath());
//
//		if(sUMengChannel != null){
//			return sUMengChannel;
//		}

		sUMengChannel = getUMengChannel(context, "META-INF/channel_");
		
        if(sUMengChannel == null){
        	Object data = null;
    		try {
    			ApplicationInfo ai = context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
    			data = ai.metaData.get("CHANNEL");
    		} catch (Exception e) {
    			e.printStackTrace();
    		}
    		sUMengChannel = data != null ? data.toString() : null;
        }

		if (!TextUtils.isEmpty(sUMengChannel)) {
			FileUtil.putStrToFile(sUMengChannel, getChannelFilePath(), false);
		}
		
		return sUMengChannel;
	}

	public static String getChannelId(Context context) {
		if (sChannelId != null) {
			return sChannelId;
		}
		sChannelId = getUMengChannel(context, "META-INF/channelID_");

		if(sChannelId == null){
			Object data = null;
			try {
				ApplicationInfo ai = context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
				data = ai.metaData.get("CHANNEL_ID");
			} catch (Exception e) {
				e.printStackTrace();
			}
			sChannelId = data != null ? data.toString() : null;
		}
		return sChannelId;
	}
	
	public static String getFirstChannel(Context context) {
		Object data = null;
		try {
			ApplicationInfo ai = context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
			data = ai.metaData.get("FIRST_CHANNEL");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data != null ? data.toString() : null;
	}
	
	public static void closeInputMethod(Activity activity){
		try {  
            InputMethodManager inputMethodManager = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);  
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(),InputMethodManager.HIDE_NOT_ALWAYS);  
        } catch (Exception e) {}  
	}
	
	public static String getDeviceId(Context context){
		try {
			TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
			return tm.getDeviceId();
		} catch (Exception e) {
			return "";
		}
	}
	
	public static String getDeviceName(){
		return android.os.Build.MODEL;   
	}

	public static String getDeviceMac() {
		String wifiMacAddress = "";
		WifiManager wifiManager = (WifiManager) BaseApp.getInstance().getSystemService(Context.WIFI_SERVICE);
		if(wifiManager != null){
			WifiInfo wifiInfo = wifiManager.getConnectionInfo();
			if(wifiInfo != null){
				wifiMacAddress = wifiInfo.getMacAddress();
			}
		}
		return wifiMacAddress;
	}
	
	public static String getSysVersionName(){
		return Build.VERSION.SDK;
	}

	public static String getDeviceManufacturer(){
		return Build.MANUFACTURER;
	}
	
	public static int getSysVersionCode(){
		return android.os.Build.VERSION.SDK_INT;   
	}
	
	public static void clearAllAppData(Context context){
		try {
			FileUtil.delFiles(context.getCacheDir().getParentFile());
		} catch (Exception e) {}
	}
	
	public static void freeMemory(int importance,boolean freeSelf){
		try {
			Context context = BaseApp.getInstance();
			ActivityManager activityManger = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
			List<RunningAppProcessInfo> list = activityManger.getRunningAppProcesses();
			if (list != null){
				for (int i = 0; i < list.size(); i++) {
					RunningAppProcessInfo apinfo = list.get(i);
					String[] pkgList = apinfo.pkgList;
					if (pkgList != null && apinfo.processName != null && apinfo.importance > importance) {
						if(freeSelf || apinfo.processName.indexOf(context.getPackageName()) == -1){
							for (int j = 0; j < pkgList.length; j++) {
								activityManger.killBackgroundProcesses(pkgList[j]);
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static long getAvailMemory(Context context) {
		// 获取android当前可用内存大小
		ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		MemoryInfo mi = new MemoryInfo();
		am.getMemoryInfo(mi);
		// mi.availMem; 当前系统的可用内存

		// return Formatter.formatFileSize(context, mi.availMem);//
		// 将获取的内存大小规格化
		return mi.availMem / (1024 * 1024);
	}

	public static long getTotalMemory(Context context) {
		String str1 = "/proc/meminfo";// 系统内存信息文件
		String str2;
		String[] arrayOfString;
		long initial_memory = 0;

		try {
			FileReader localFileReader = new FileReader(str1);
			BufferedReader localBufferedReader = new BufferedReader(localFileReader, 8192);
			str2 = localBufferedReader.readLine();// 读取meminfo第一行，系统总内存大小
			arrayOfString = str2.split("\\s+");
			initial_memory = Integer.valueOf(arrayOfString[1]).intValue() * 1024;// 获得系统总内存，单位是KB，乘以1024转换为Byte
			localBufferedReader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		// return Formatter.formatFileSize(context, initial_memory);//
		// Byte转换为KB或者MB，内存大小规格化
		return initial_memory / (1024 * 1024);
	}
}
