package com.bk.android.util;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;

public class OSUtil {
	private static Handler mHandler = new Handler(Looper.getMainLooper());
	
	public static boolean isProcessByTag(Context context,String tag) {
		int pid = android.os.Process.myPid();
		ActivityManager activityMag = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		String processName = context.getApplicationInfo().processName + tag;
		for (RunningAppProcessInfo appProcess : activityMag.getRunningAppProcesses()) {
			if (appProcess.pid == pid 
					&& appProcess.processName.equalsIgnoreCase(processName)) {
				return true;
			}
		}
		return false;
	}
	
	public static boolean isMainThread(){
		return mHandler.getLooper().getThread() == Thread.currentThread();
	}
}
