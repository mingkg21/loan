package com.bk.android.util;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

/** APP market工具类
 * @author mingkg21
 * @email mingkg21@gmail.com
 * @date 2013-10-6
 */
public class AppMarketUtil {
	
	/** 跳转到软件市场
	 * @param context
	 */
	public static boolean gotoMarket(Context context){
		Uri uri = Uri.parse("market://details?id=" + context.getPackageName());
		Intent intent = new Intent(Intent.ACTION_VIEW, uri);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		try {
			context.startActivity(intent);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

}
