package com.bk.android.util;

import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

public class ReleaseUtil {
	/**
	 * Utility method to help release drawables from Activity once activity is onDestroy
	 * see more at: http://www.alonsoruibal.com/bitmap-size-exceeds-vm-budget/
	 * @param view
	 */
	public static void releaseDrawables(View view) {
    	if( view == null ) return;
        if (view.getBackground() != null) {
        	view.getBackground().setCallback(null);
        	view.destroyDrawingCache();
        	view.setBackgroundDrawable(null);
        }
        
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
            	releaseDrawables(((ViewGroup) view).getChildAt(i));
            }            
        	try {
        		if((view instanceof AdapterView<?>)) {
        			AdapterView<?> adapterView = (AdapterView<?>)view;
        			adapterView.setAdapter(null);        			
        		} else {
        			((ViewGroup) view).removeAllViews();
        		}
        	} catch(Exception e) {
        	}
        }
    }	
}
