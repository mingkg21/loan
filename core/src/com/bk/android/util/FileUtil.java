package com.bk.android.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

import android.content.ContentResolver;
import android.content.Context;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Environment;
import android.os.FileUtils;
import android.os.StatFs;
import android.provider.MediaStore.Images;
import android.text.TextUtils;

import com.bk.android.app.BaseApp;

/**
 * 文件操作工具
 * @author lyw
 * @date 2014-1-10
 */
public final class FileUtil {
	private static final String TAG = FileUtil.class.getSimpleName();
	private static final int BUFFER_SIZE = 4 * 1024;
	
	public static void checkPath(String path){
		File file = new File(path);
		if(!file.exists()){
			file.mkdirs();
		}
	}
	/** 获取SDCARD的DCIM目录
	 * @return
	 */
	public static File getSDCardFile() {
		return Environment.getExternalStorageDirectory();
	}
	/** 获取SDCARD的DCIM目录
	 * @return
	 */
	public static File getDcimPath(){
		if(Build.VERSION.SDK_INT >= 8) {
			return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
		} else {
			return new File(Environment.getExternalStorageDirectory() + "/dcim");
		}
	}
	/**
	 * 判断是否有SDCARD
	 * 
	 * @return
	 */
	public static boolean isSDCardExist() {
		if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * 判断某个文件是否存在
	 * 
	 * @param filePath
	 * @return
	 */
	public static boolean isFileExists(String filePath) {
		if (!isSDCardExist()) {
			return false;
		}
		File file = new File(filePath);
		if (file != null && file.exists()) {
			return true;
		}
		return false;
	}
	/**
	 * 获取存储卡的可用空间
	 * 
	 * @return
	 */
	public static long getStorageSize() {
		if (isSDCardExist()) {
			try {
				StatFs stat = new StatFs(getSDCardFile().getAbsolutePath());
				long blockSize = stat.getBlockSize();
				return stat.getAvailableBlocks() * blockSize;
			} catch (Exception e) {}
		}
		return 0;
	}
	/**
	 * 更新文件修改时间
	 * @param dir
	 * @param fileName
	 * @return
	 */
	public static boolean updateFileTime(String dir, String fileName) {
		File file = new File(dir, fileName);
		if (!file.exists()) {
			return false;
		}
		long newModifiedTime = System.currentTimeMillis();
		return file.setLastModified(newModifiedTime);
	}

	/**
	 * 创建文件目录
	 * 
	 * @param path
	 */
	public static boolean createFileDir(String path) {
		File file = new File(path);
		if (!file.exists()) {
			file.mkdirs();
			return true;
		}
		file = null;
		return false;
	}
	/**
	 * 检测文件路径的目录是否存在，不存在就尝试创建目录
	 * @param path 文件路径
	 * @return 目录是否可用
	 */
	public static boolean checkPathDir(String path){
		if(TextUtils.isEmpty(path) || path.charAt(path.length() - 1) == '/'){
			return false;
		}
		String dirStr = path;
		int end = dirStr.lastIndexOf('/') + 1;
		if(end != -1){
			dirStr = dirStr.substring(0, end);
		}else{
			return false;
		}
		return checkDir(dirStr);
	}
	/**
	 * 检测目录是否存在，不存在就尝试创建目录
	 * @param path 文件目录
	 * @return 目录是否可用
	 */
	public static boolean checkDir(String dirPath){
		if(TextUtils.isEmpty(dirPath)){
			return false;
		}
		File dir = new File(dirPath);
		if(dir.exists() || dir.mkdirs()){
			return true;
		}
		return false;
	}
	/**
	 * 写文件到SDCARD指定的位置
	 *
	 * @param str
	 * @param filePath
	 * @return
	 */
	public static boolean putStrToFile(String str, String filePath, boolean append) {
		if (TextUtils.isEmpty(str)) {
			return false;
		}
		if (TextUtils.isEmpty(filePath)) {
			return false;
		}
		try {
			FileWriter fw = new FileWriter(filePath, append);
			fw.write(str);
			fw.flush();
			fw.close();
			return true;
		} catch (Exception e) {
		}
		return false;
	}
	/**
	 * 写文件到SDCARD指定的位置
	 * 
	 * @param str
	 * @param filePath
	 * @return
	 */
	public static boolean outPutStrToFile(String str, String filePath) {
		return putStrToFile(str, filePath, true);
	}
	
	public static String inPutAssetsToStr(String fileName){
		if (TextUtils.isEmpty(fileName)) {
			return null;
		}
		try {
			StringBuffer sb = new StringBuffer();
			InputStream is = BaseApp.getInstance().getAssets().open(fileName);
			byte[] buffer = new byte[1024 * 8];
			int line = -1;
			while ((line = is.read(buffer)) != -1) {
				sb.append(new String(buffer, 0, line));
			}
			is.close();
			return sb.toString();
		} catch (Exception e) {}
		return null;
	}
	
	public static String inPutFileToStr(String filePath){
		if (TextUtils.isEmpty(filePath)) {
			return null;
		}
		try {
			StringBuffer sb = new StringBuffer();
			FileReader is = new FileReader(filePath);
			char[] buffer = new char[1024 * 8];
			int line = -1;
			while ((line = is.read(buffer)) != -1) {
				sb.append(buffer, 0, line);
			}
			is.close();
			return sb.toString();
		} catch (Exception e) {}
		return null;
	}

	public static boolean outCrashToFile(String str, String filePath,int maxSize) {
		if (TextUtils.isEmpty(str)) {
			return false;
		}
		if (TextUtils.isEmpty(filePath)) {
			return false;
		}
		try {
			File file = new File(filePath);
			if (file.exists()) {
				if (maxSize < file.length()) {
					file.delete();
				}
			}
			FileWriter fw = new FileWriter(filePath, true);
			fw.write(str);
			fw.flush();
			fw.close();
			return true;
		} catch (Exception e) {
		}
		return false;
	}
	
	public static void copyFileToSdcard(Context context, String fileName,
			String filePath) {
		File file = new File(filePath);
		if (file.exists()) {
			return;
		}
		try {
			AssetManager am = context.getAssets();
			InputStream is = am.open(fileName);
			BufferedInputStream bis = new BufferedInputStream(is);
			if (!file.exists()) {
				file.createNewFile();
			}
			if (file.isFile()) {
				RandomAccessFile oSavedFile = new RandomAccessFile(file, "rw");
				oSavedFile.seek(0);
				int bufferSize = 4096;
				byte[] b = new byte[bufferSize];
				int nRead;
				int currentBytes = 0;
				int bytesNotified = currentBytes;
				long timeLastNotification = 0;
				for (;;) {
					nRead = bis.read(b, 0, bufferSize);

					if (nRead == -1) {
						break;
					}
					currentBytes += nRead;
					oSavedFile.write(b, 0, nRead);
					long now = System.currentTimeMillis();
					if (currentBytes - bytesNotified > bufferSize
							&& now - timeLastNotification > 1500) {
						bytesNotified = currentBytes;
						timeLastNotification = now;
					}
				}
				oSavedFile.close();
			}
			is.close();
		} catch (Exception e) {

		}
	}
	
	public static void copyDataToFile(String path, InputStream is) {
		try {
			FileOutputStream fos = new FileOutputStream(path, false);
			int bufferSize = 4096;
			byte[] b = new byte[bufferSize];
			int nRead;
			for (;;) {
				nRead = is.read(b, 0, bufferSize);
				if (nRead == -1) {
					break;
				}
				fos.write(b, 0, nRead);
			}
			fos.close();
			is.close();
		} catch (Exception e) {}
	}
	
	public static void compressZip(String srcPath, String outPath) {  
		File zipFile = new File(outPath);  
        File file = new File(srcPath);  
        if (!file.exists())
            throw new RuntimeException(srcPath + "不存在！");  
        try {
            ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipFile));  
            String basedir = "";  
            compressZip(file, out, basedir);  
            out.close();  
        } catch (Exception e) {  
            throw new RuntimeException(e);  
        }  
    }  
	
	public static void compressZip(File file, ZipOutputStream out, String basedir) {  
        /* 判断是目录还是文件 */  
        if (file.isDirectory()) {  
            System.out.println("压缩：" + basedir + file.getName());  
            compressZipDirectory(file, out, basedir);  
        } else {  
            System.out.println("压缩：" + basedir + file.getName());  
            compressZipFile(file, out, basedir);  
        }  
    }  
  
    /** 压缩一个目录 */  
	public static void compressZipDirectory(File dir, ZipOutputStream out, String basedir) {  
        if (!dir.exists())  
            return;  
  
        File[] files = dir.listFiles();  
        for (int i = 0; i < files.length; i++) {  
            /* 递归 */  
            compressZip(files[i], out, basedir + dir.getName() + "/");  
        }  
    }  
  
    /** 压缩一个文件 */  
    public static void compressZipFile(File file, ZipOutputStream out, String basedir) {  
        if (!file.exists()) {  
            return;  
        }  
        try {  
            BufferedInputStream bis = new BufferedInputStream(  
                    new FileInputStream(file));  
            ZipEntry entry = new ZipEntry(basedir + file.getName());  
            out.putNextEntry(entry);  
            int count;  
            byte data[] = new byte[8192];  
            while ((count = bis.read(data, 0, 8192)) != -1) {  
                out.write(data, 0, count);  
            }  
            bis.close();  
        } catch (Exception e) {  
            throw new RuntimeException(e);  
        }  
    }  
    
	public static void upZipFile(String zipFilePath, String folderPath)throws ZipException,IOException {
		if(TextUtils.isEmpty(zipFilePath)){
			return;
		}
		upZipFile(new File(zipFilePath), folderPath);
	}
	
	public static void upZipFile(File zipFile, String folderPath)throws ZipException,IOException {
	    ZipFile zfile = new ZipFile(zipFile);
	    Enumeration<? extends ZipEntry> zList = zfile.entries();
	    ZipEntry ze = null;
	    byte[] buf = new byte[1024];
	    while(zList.hasMoreElements()){
	        ze = (ZipEntry)zList.nextElement();    
	        if(ze.isDirectory()){
	            String dirstr = folderPath + ze.getName();
	            File f = new File(dirstr);
	            f.mkdir();
	            continue;
	        }
	        OutputStream os = new BufferedOutputStream(new FileOutputStream(getRealZipFileName(folderPath, ze.getName())));
	        InputStream is = new BufferedInputStream(zfile.getInputStream(ze));
	        int readLen = 0;
	        while ((readLen = is.read(buf, 0, 1024))!= -1) {
	            os.write(buf, 0, readLen);
	        }
	        is.close();
	        os.close();    
	    }
	    zfile.close();
	}
	
	
	/** 给定根目录，返回一个相对路径所对应的实际文件名.
	* @param baseDir 指定根目录
	* @param absFileName 相对路径名，来自于ZipEntry中的name
	* @return java.io.File 实际的文件*/
	private static File getRealZipFileName(String baseDir, String absFileName){
	    String[] dirs = absFileName.split("/");
	    File dirFile = null;
	    if(dirs.length > 1){
	    	dirFile = new File(baseDir + "/" + absFileName).getParentFile();
	    }else{
	    	dirFile = new File(baseDir);
	    }
	    if(dirFile != null && !dirFile.exists()){
	    	dirFile.mkdirs();
	    }
	    return new File(dirFile, dirs[dirs.length - 1]);
	}
	
	public static boolean copyFile(String filePath,String targetFilePath){
		if (TextUtils.isEmpty(filePath)) {
			return false;
		}
		if (!FileUtil.isSDCardExist()) {
			LogUtil.v(TAG, "sdcard is not exist!");
			return false;
		}
		File file = new File(filePath);
		File targetFile = new File(targetFilePath);
		try {
			FileOutputStream os = new FileOutputStream(targetFile,false);
			FileInputStream is = new FileInputStream(file);
			byte[] responseByteArray = new byte[BUFFER_SIZE];
			int line = -1;
			while ((line = is.read(responseByteArray)) != -1) {
				os.write(responseByteArray, 0, line);
			}
			os.close();
			is.close();
			return true;
		} catch (IOException e) {
			LogUtil.e(TAG, "save image err", e);
		}
		return false;
	}

	public static int getFileSize(String filePath) {
		File file = new File(filePath);
		FileInputStream fis = null;
		int fileLen = 0;
		try {
			fis = new FileInputStream(file);
			fileLen = fis.available(); // 这就是文件大小
		} catch (FileNotFoundException e) {
		} catch (IOException e) {
		}
		return fileLen;
	}
	
	/** 获取SDCARD的ID
	 * @return
	 */
	public static int getSdcardID(){
		return FileUtils.getFatVolumeId(getSDCardFile().getName());
	}
	/**
	 * 删除文件，文件夹
	 * @param filePath
	 * @return
	 */
	public static boolean delFiles(String path) {
		return delFiles(path,false);
	}
	
	public static boolean delFiles(String path,boolean delDirectory) {
		if(TextUtils.isEmpty(path)){
			return false;
		}
		return delFiles(new File(path),delDirectory);
	}
	/**
	 * 删除文件，文件夹
	 * @param filePath
	 * @return
	 */
	
	public static boolean delFiles(File file) {
		return delFiles(file,false);
	}
	
	public static boolean delFiles(File file,boolean delDirectory) {
		if(file == null){
			return false;
		}
		if (!file.exists()) {
			return true;
		}
		if (file.isDirectory()) {
			File[] files = file.listFiles();
			boolean isSuccess = true;
			if(files != null){
				for (File fileChild : files) {
					isSuccess = delFiles(fileChild,true) && isSuccess;
				}
			}
			return delDirectory ? isSuccess && file.delete() : isSuccess;
		}else{
			if(file.delete()){
				return true;
			}else{
				return false;
			}
		}
	}
	
	/** 保存照片
	 * @param context
	 * @param filePath
	 * @param name
	 * @return
	 */
	public static String savePhoto(Context context, String filePath, String name) {
		if(!isSDCardExist()) {
			return null;
		}
		String url = null;
		try {
			ContentResolver cr = context.getContentResolver();
			url = Images.Media.insertImage(cr, filePath, name, "");
		} catch (FileNotFoundException e) {
		}
		return url;
	}
	
	/** 保存照片
	 * @param context
	 * @param filePath
	 * @param name
	 * @return
	 */
	public static String savePhoto(Context context, Bitmap bitmap, String name) {
		if(!isSDCardExist()) {
			return null;
		}
		String url = null;
		try {
			ContentResolver cr = context.getContentResolver();
			url = Images.Media.insertImage(cr, bitmap, name, "");
		} catch (Exception e) {}
		return url;
	}
	
	public static String[] getImgLongitude(String path){
		Cursor cursor = BaseApp.getInstance().getContentResolver().query(Images.Media.EXTERNAL_CONTENT_URI, new String[]{Images.Media.LONGITUDE,Images.Media.LATITUDE},
				Images.Media.DATA + "=?", new String[] {path},null);
		String[] longitude = new String[2];
		if(cursor != null){
			if(cursor.moveToFirst()){
				longitude[0] = cursor.getString(cursor.getColumnIndex(Images.Media.LONGITUDE));
				longitude[1] = cursor.getString(cursor.getColumnIndex(Images.Media.LATITUDE));
			}
			cursor.close();
		}
		return longitude;
	}
	
	public static long getImgAddDate(String path){
		Cursor cursor = BaseApp.getInstance().getContentResolver().query(Images.Media.EXTERNAL_CONTENT_URI, new String[]{Images.Media.DATE_ADDED},
				Images.Media.DATA + "=?", new String[] {path},null);
		long time = 0;
		if(cursor != null){
			if(cursor.moveToFirst()){
				time = cursor.getLong(cursor.getColumnIndex(Images.Media.DATE_ADDED));
			}
			cursor.close();
		}
		if(time <= 0){
			time = new File(path).lastModified();
		}else{
			time *= 1000;
		}
		return time;
	}
	
	public static long getImgTakenDate(String path){
		Cursor cursor = BaseApp.getInstance().getContentResolver().query(Images.Media.EXTERNAL_CONTENT_URI, new String[]{Images.Media.DATE_TAKEN},
				Images.Media.DATA + "=?", new String[] {path},null);
		long time = 0;
		if(cursor != null){
			if(cursor.moveToFirst()){
				time = cursor.getLong(cursor.getColumnIndex(Images.Media.DATE_TAKEN));
			}
			cursor.close();
		}
		if(time <= 0){
			time = new File(path).lastModified();
		}
		return time;
	}
	
	public static int getImgOrientation(String path){
		Cursor cursor = BaseApp.getInstance().getContentResolver().query(Images.Media.EXTERNAL_CONTENT_URI, new String[]{Images.Media.ORIENTATION},
				Images.Media.DATA + "=?", new String[] {path},null);
		int orientation = 0;
		if(cursor != null){
			if(cursor.moveToFirst()){
				orientation = cursor.getInt(cursor.getColumnIndex(Images.Media.ORIENTATION));
			}
			cursor.close();
		}
		return orientation;
	}
}
