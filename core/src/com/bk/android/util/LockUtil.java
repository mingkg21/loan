package com.bk.android.util;

import android.util.SparseArray;

public class LockUtil {
	private static SparseArray<Integer> sLockMap = new SparseArray<Integer>();
	private static SparseArray<Integer> sFileLockMap = new SparseArray<Integer>();

	public static Integer getLock(String id){
		Integer hashCode = null;
		synchronized (sLockMap) {
			hashCode = sLockMap.get(id.hashCode());
			if(hashCode == null){
				hashCode = Integer.valueOf(id.hashCode());
				sLockMap.put(id.hashCode(), hashCode);
			}
		}
		return hashCode;
	}
	
	public static Integer getFileLock(String url){
		Integer hashCode = null;
		synchronized (sFileLockMap) {
			hashCode = sFileLockMap.get(url.hashCode());
			if(hashCode == null){
				hashCode = Integer.valueOf(url.hashCode());
				sFileLockMap.put(url.hashCode(), hashCode);
			}
		}
		return hashCode;
	}
}
