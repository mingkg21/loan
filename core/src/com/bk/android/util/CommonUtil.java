package com.bk.android.util;

import android.text.TextUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommonUtil {
	
	/**
	 * 判断合法的EMAIL
	 * \\w+([-.]\\w+)*@\\w+([-]\\w+)*\\.(\\w+([-]\\w+)*\\.)*[a-z]{2,3}$
	 * @param email
	 * @return 
	 */
	public static boolean checkEmail(String email) {
		if(TextUtils.isEmpty(email)) {
			return false;
		}
		String check = "^[a-zA-Z0-9_\\-]+(\\.[_a-zA-Z0-9\\-]+)*@([_a-zA-Z0-9\\-]+\\.)+([a-zA-Z]{2}|aero|arpa|biz|com|coop|edu|gov|info|int|jobs|mil|museum|name|nato|net|org|pro|travel)$";
		try {
			Pattern regex = Pattern.compile(check);
			Matcher matcher = regex.matcher(email);
			if (matcher.matches()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			LogUtil.e("checkIsEmail error:" + email);
			return false;
		}
	}
	
	/** 判断是否手机号码
	 * 移动的号段：134(0-8)、135、136、137、138、139、147（预计用于TD上网卡）、150、151、152、157（TD专用）、
	 * 158、159、187（未启用）、182、183、188（TD专用）
	 * 联通的号段：130、131、132、155、156（世界风专用）、185（未启用）、186（3g）、145
	 * 电信的号段：133、153、180、181、189 网通在并入联通之前只经营固话和小灵通，没有手机号段。
	 * 中国卫通的卫星电话使用1349号段，现在卫通的基础电信服务并入了电信、不知这号段有没有移交给电信。
	 * 新增号段:170、177、184
	 * @param number
	 * @return
	 */
//	public static boolean isPhoneNumber(String number) {
//		Pattern pattern = Pattern
//				.compile("^(134|135|136|137|138|139|147|150|151|152|157|158|159|182|183|187|188|130|131|132|155|156|185|186|145|133|153|180|181|189|170|177|178|184|176|173)[0-9]{8}$");
//		Matcher matcher = pattern.matcher(number);
//		if (matcher.matches()) {
//			return true;
//		}
//		return false;
//	}

	public static boolean isPhoneNumber(String number) {
		Pattern pattern = Pattern
				.compile("1[34578][0-9]{9}");
		Matcher matcher = pattern.matcher(number);
		if (matcher.matches()) {
			return true;
		}
		return false;
	}
}
