package com.bk.android.util;

import android.text.TextUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class FormatUtil {
	public static final long MINUTE_TIME = 1000 * 60;
	public static final long HOUR_TIME = MINUTE_TIME * 60;
	public static final long DAY_TIME = HOUR_TIME * 24;
	
	public static String getMillisecondTimeStr(long millisecond) {
		return getMillisecondTimeStr(millisecond, ":", true);
	}
	
	public static String getMillisecondTimeStr(long millisecond, boolean isLong) {
		return getMillisecondTimeStr(millisecond, ":", isLong);
	}
	
	public static String getMillisecondTimeStr(long millisecond, String decollator, boolean isLong) {
		int maxSec = (int) (millisecond / 1000);
		int sec = (int) (maxSec % 60);
		int min = (int) (maxSec % 3600 / 60);
		int hour = (int) (maxSec / 3600);
		String timeStr = "";
		if(hour > 0){
			if(hour < 10){
				timeStr += "0" + hour + decollator;
			}else{
				timeStr += hour + decollator;
			}
		}else if(isLong){
			timeStr += "00" + decollator;
		}
		if(min < 10){
			timeStr += "0" + min;
		}else{
			timeStr += min;
		}
		if (sec < 10){
			timeStr += decollator + "0" + sec;
		}else{
			timeStr += decollator + sec;
		}
		return timeStr;
	}
	
	public static String getDayTime(long millisecond){
		return getDayTime(millisecond,"天", "小时", "分钟", "前", "刚刚");
	}
	
	public static boolean isToday(long milliseconds) {
		Calendar today = Calendar.getInstance();  
		today.set(Calendar.HOUR_OF_DAY, 0);  
		today.set(Calendar.MINUTE, 0);  
		today.set(Calendar.SECOND, 0);  
		today.set(Calendar.MILLISECOND, 0);  
		
		Calendar tomorrow = Calendar.getInstance();  
		tomorrow.set(Calendar.HOUR_OF_DAY, 0);  
		tomorrow.set(Calendar.MINUTE, 0);  
		tomorrow.set(Calendar.SECOND, 0);  
		tomorrow.set(Calendar.MILLISECOND, 0);  
		tomorrow.set(Calendar.DAY_OF_MONTH, today.get(Calendar.DAY_OF_MONTH) + 1);
		
		long todayMilliseconds = today.getTimeInMillis();
		long tomorrowMilliseconds = tomorrow.getTimeInMillis();
		
		if((milliseconds >= todayMilliseconds) && (milliseconds < tomorrowMilliseconds)) {
			return true;
		} 
		return false;
	}
	
	public static String getDayDate(long milliseconds) {
		Calendar today = Calendar.getInstance();  
		today.set(Calendar.HOUR_OF_DAY, 0);  
		today.set(Calendar.MINUTE, 0);  
		today.set(Calendar.SECOND, 0);  
		today.set(Calendar.MILLISECOND, 0);  
		
		Calendar tomorrow = Calendar.getInstance();  
		tomorrow.set(Calendar.HOUR_OF_DAY, 0);  
		tomorrow.set(Calendar.MINUTE, 0);  
		tomorrow.set(Calendar.SECOND, 0);  
		tomorrow.set(Calendar.MILLISECOND, 0);  
		tomorrow.set(Calendar.DAY_OF_MONTH, today.get(Calendar.DAY_OF_MONTH) + 1);
		
		Calendar yesterday = Calendar.getInstance();  
		yesterday.set(Calendar.HOUR_OF_DAY, 0);  
		yesterday.set(Calendar.MINUTE, 0);  
		yesterday.set(Calendar.SECOND, 0);  
		yesterday.set(Calendar.MILLISECOND, 0);  
		yesterday.set(Calendar.DAY_OF_MONTH, today.get(Calendar.DAY_OF_MONTH) - 1);
		
		long todayMilliseconds = today.getTimeInMillis();
		long tomorrowMilliseconds = tomorrow.getTimeInMillis();
		long yesterdayMilliseconds = yesterday.getTimeInMillis();
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(milliseconds);
		
		if((milliseconds >= todayMilliseconds) && (milliseconds < tomorrowMilliseconds)) {
			return "今天";
		} else if((milliseconds >= yesterdayMilliseconds) && (milliseconds < todayMilliseconds)) {
			return "昨天";
		} else {
			SimpleDateFormat sdf = null;
			if(calendar.get(Calendar.YEAR) == today.get(Calendar.YEAR)) {
				sdf = new SimpleDateFormat("MM-dd");
			} else {
				sdf = new SimpleDateFormat("yyyy-MM-dd");
			}
			return sdf.format(new Date(milliseconds));
		}
	}
	
	/** 论坛楼层格式化
	 * @param floor
	 * @return
	 */
	public static String formatFloor(int floor){
		if(floor == 1) {
			return "沙发";
		} else if(floor == 2) {
			return "板凳";
		} else {
			StringBuilder sb = new StringBuilder();
			sb.append(floor);
			sb.append("楼");
			return sb.toString();
		}
	}
	
	public static String getDayTime(long millisecond,String ds,String hs,String mis,String ago,String now){
		String timeStr = "";

		String millisecondStr = millisecond + "";
		if (millisecondStr.length() == 10) {
			millisecond *= 1000;
		}

		long time = System.currentTimeMillis() - millisecond;
		int d = (int) (time / DAY_TIME);
		int h = (int) (time / HOUR_TIME);
		int mi = (int) (time / MINUTE_TIME);
		if(d > 0){
			if(d > 3){
				timeStr = formatSimpleDate("yyyy-MM-dd", millisecond);
			}else{
				timeStr = d + ds + ago;
			}
		}else if(h > 0){
			timeStr = h + hs + ago;
		}else if(mi > 0){
			timeStr = mi + mis + ago;
		}else{
			timeStr = now;
		}
		return timeStr;
	}
	/**
	 * "yyyy-MM-dd HH:mm:ss"
	 * @param pattern
	 * @param millisecond
	 * @return
	 */
	public static String formatSimpleDate(String pattern,long millisecond){
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		return format.format(new Date(millisecond));
	}
	
	public static String formatSize(long size){
		if(size < 10000){
			return size + "";
		}else{
			return size / 10000 + "万";
		}
	}
	
	public static String getAge(String brithday){
		return getAge(brithday,System.currentTimeMillis());
	}

	public static String getAge(String brithday,long relativeTime){
		try {
			long[] ageRelativeDay = getAgeRelativeDay(brithday, relativeTime);
			int years = (int) ageRelativeDay[0];
			int months = (int) ageRelativeDay[1];
			int days = (int) ageRelativeDay[2];
			long bridthdayTime = ageRelativeDay[3];
			long relativeDayTime = ageRelativeDay[4];
			StringBuilder sb = new StringBuilder();
			if(years >= 0) {
				if(years > 0) {
					sb.append(years);
					sb.append("岁");
				}
				if(months > 0) {
					sb.append(months);
					sb.append("个月");
				}
				if(days > 0) {
					sb.append(days);
					sb.append("天");
				}
			} 
			
			if(sb.length() == 0) {
				int day = (int) Math.ceil((bridthdayTime - relativeDayTime) / (24 * 3600 * 1000));
				sb.append("出生前");
				sb.append(day);
				sb.append("天");
			}
			return sb.toString();
		} catch (Exception e) {}
		return null;
	}
	
	public static long[] getAgeRelativeDay(String brithday){
		return getAgeRelativeDay(brithday, System.currentTimeMillis());
	}
	
	public static long[] getAgeRelativeDay(String brithday,long relativeTime){
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		return getAgeRelativeDay(brithday, format.format(new Date(relativeTime)));
	}
	
	public static long[] getAgeRelativeDay(String brithday, String relativeDay){
		if(TextUtils.isEmpty(relativeDay)) {
			return null;
		}
		if(TextUtils.isEmpty(brithday)) {
			return null;
		}
		int index = relativeDay.indexOf(" ");
		if(index != -1){
			relativeDay = relativeDay.substring(0, index);
		}
		
		String[] birthdayTmp = brithday.split("-");
		int birthdayYear = Integer.valueOf(birthdayTmp[0]);
		int birthdayMonth = Integer.valueOf(birthdayTmp[1]);
		int birthdayDay = Integer.valueOf(birthdayTmp[2]);
		
		String[] relativeDayTmp = relativeDay.split("-");
		int relativeDayYear = Integer.valueOf(relativeDayTmp[0]);
		int relativeDayMonth = Integer.valueOf(relativeDayTmp[1]);
		int relativeDayDay = Integer.valueOf(relativeDayTmp[2]);
		
		long bridthdayTime;
		long relativeDayTime;
		
		Calendar birthdayCal = Calendar.getInstance();
		birthdayCal.set(birthdayYear, birthdayMonth - 1, birthdayDay);
		birthdayCal.set(Calendar.HOUR_OF_DAY, 0);
		birthdayCal.set(Calendar.MINUTE, 0);
		birthdayCal.set(Calendar.SECOND, 0);
		birthdayCal.set(Calendar.MILLISECOND, 0);
		bridthdayTime = birthdayCal.getTimeInMillis();
		
		Calendar relativeDayyCal = Calendar.getInstance();
		relativeDayyCal.set(relativeDayYear, relativeDayMonth - 1, relativeDayDay);
		relativeDayyCal.set(Calendar.HOUR_OF_DAY, 0);
		relativeDayyCal.set(Calendar.MINUTE, 0);
		relativeDayyCal.set(Calendar.SECOND, 0);
		relativeDayyCal.set(Calendar.MILLISECOND, 0);
		relativeDayTime = relativeDayyCal.getTimeInMillis();
		
		int years = relativeDayYear - birthdayYear;
		int months = relativeDayMonth - birthdayMonth;
		int days = relativeDayDay - birthdayDay;
		days += 1;
		
		if(days < 0) {
			relativeDayyCal.set(Calendar.DATE, 1);
			relativeDayyCal.add(Calendar.DATE, -1);
			days += relativeDayyCal.get(Calendar.DATE);
			months--;
		}
		if(months < 0) {
			months += 12;
			years--;
		}
		return new long[]{years,months,days,bridthdayTime,relativeDayTime};
	}
	
	public static long getRelativeDayTime(String brithday,int years,int months,int days){
		if(TextUtils.isEmpty(brithday)) {
			return -1;
		}
		String[] birthdayTmp = brithday.split("-");
		int birthdayYear = Integer.valueOf(birthdayTmp[0]);
		int birthdayMonth = Integer.valueOf(birthdayTmp[1]);
		int birthdayDay = Integer.valueOf(birthdayTmp[2]);
		
		long relativeDayTime;
		
		int relativeDayYear = years + birthdayYear;
		int relativeDayMonth = months + birthdayMonth;
		
		if(relativeDayMonth > 12){
			relativeDayMonth -= 12;
			relativeDayYear++;
		}
		Calendar relativeDayyCal = Calendar.getInstance();
		relativeDayyCal.set(relativeDayYear, relativeDayMonth - 1, 1);
		int	relativeDayDay = days - 1 + birthdayDay;
		
		if(relativeDayDay > relativeDayyCal.getActualMaximum(Calendar.DATE)){
			relativeDayDay = addNextDay(relativeDayDay,relativeDayyCal);
		}
		relativeDayyCal.set(Calendar.DATE, relativeDayDay);
		relativeDayyCal.set(Calendar.HOUR_OF_DAY, 0);
		relativeDayyCal.set(Calendar.MINUTE, 0);
		relativeDayyCal.set(Calendar.SECOND, 0);
		relativeDayyCal.set(Calendar.MILLISECOND, 0);
		relativeDayTime = relativeDayyCal.getTimeInMillis();
		return relativeDayTime;
	}
	
	private static int addNextDay(int relativeDayDay,Calendar relativeDayyCal){
		relativeDayDay -= relativeDayyCal.getActualMaximum(Calendar.DATE);
		relativeDayyCal.set(Calendar.DATE, relativeDayyCal.getActualMaximum(Calendar.DATE));
		relativeDayyCal.add(Calendar.DATE, 1);
		if(relativeDayDay > relativeDayyCal.getActualMaximum(Calendar.DATE)){
			return addNextDay(relativeDayDay, relativeDayyCal);
		}else{
			return relativeDayDay;
		}
	}
	
	public static SimpleDateFormat sSimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
	public static SimpleDateFormat sDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
	
	public static long changeToData(String day) {
		long time = 0;
		try {
			Date date = sDateFormat.parse(day);
			time = date.getTime();
		} catch (ParseException e) {
			try {
				Date date = sSimpleDateFormat.parse(day);
				time = date.getTime();
			} catch (ParseException e1) {}
		}
		return time;
	}
}
