package com.bk.android.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.ByteArrayBuffer;

import android.content.ContentResolver;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Build;
import android.os.Build.VERSION;
import android.provider.MediaStore.Images;
import android.provider.MediaStore.Video;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.SparseArray;

import com.bk.android.app.BaseApp;
import com.bk.android.data.net.AbsConnect;

public class BitmapUtil {
	/** 原图质量*/
	public static final int IMG_QUALITY_NON = -1;
	public static final int IMG_QUALITY_0 = 0;
	public static final int IMG_QUALITY_1 = 1;
	public static final int IMG_QUALITY_2 = 2;
	public static final int IMG_QUALITY_3 = 3;
	public static final int IMG_QUALITY_4 = 4;
	public static final int IMG_QUALITY_5 = 5;

	private static final String TAG = "BitmapUtil";
	private static final int BUFFER_SIZE = 16 * 1024;
	private static DisplayMetrics sDisplay;
	private static SparseArray<int[]> sSizeCache;
	private static SparseArray<Boolean> sFileDBCache;
	private static Config sInPreferredConfig = Config.RGB_565;
	
	private static ContentResolver getContentResolver(){
		return BaseApp.getInstance().getContentResolver();
	}
	
	private static Context getContext(){
		return BaseApp.getInstance();
	}
	
	private static Resources getResources(){
		return getContext().getResources();
	}
	
	private synchronized static DisplayMetrics getDisplay(){
		if(sDisplay == null){
			sDisplay = getResources().getDisplayMetrics();
		}
		return sDisplay;
	}
	
	public static void setInPreferredConfig(Config config){
		sInPreferredConfig = config;
	}
	
	public static String getResourcesUri(int res) {
		return ContentResolver.SCHEME_ANDROID_RESOURCE + "://" 
				+ getResources().getResourcePackageName(res) 
				+ "/drawable/" 
				+ getResources().getResourceEntryName(res);
	}
	
	public static Bitmap getResourceBitmap(String url){
		Bitmap bitmap = null;
		try {
			Uri uri = Uri.parse(url);
			if(uri != null){
				String scheme = uri.getScheme();
				if(ContentResolver.SCHEME_ANDROID_RESOURCE.equals(scheme)
	            		|| ContentResolver.SCHEME_CONTENT.equals(scheme)
	                    || ContentResolver.SCHEME_FILE.equals(scheme)){
					bitmap = BitmapUtil.clipScreenBoundsBitmap(getResources(),
							uri,BitmapUtil.getQualityScale(BitmapUtil.IMG_QUALITY_0));
				}
			}
		} catch (IOException e) {}
		return bitmap;
	}
	
	public synchronized static void syncCompressionImg(String source, String target,int targetW,int targetH,int quality){
		Bitmap bitmap = null;
		if(source.startsWith(ContentResolver.SCHEME_FILE)){
			source = source.substring(7);
		}
		if(source.startsWith(ContentResolver.SCHEME_ANDROID_RESOURCE) 
				|| source.startsWith(ContentResolver.SCHEME_CONTENT)){
			bitmap = getImageInResources(source,targetW,targetH);
		}else{
			bitmap = getBitmapInSdcard(source,targetW,targetH);
		}
		if(bitmap != null){
	        float imgSize = bitmap.getWidth() * bitmap.getHeight();
			float targetSize = targetW * targetH;
	        if(imgSize < targetSize){
	        	quality = 100;
	        }
			saveImage(target, bitmap,quality);
			bitmap.recycle();
		}
	}
	
	public static boolean downloadImg(String filePath,String url,LoadNetCallback callback) {
		boolean isRange = true;
		if (TextUtils.isEmpty(url) || TextUtils.isEmpty(filePath)) {
			return false;
		}
		if (!FileUtil.isSDCardExist()) {
			LogUtil.v(TAG, "sdcard is not exist!");
			return false;
		}
		
		DefaultHttpClient httpClient = AbsConnect.getDefaultHttpClient(getContext());
		if(callback != null){
			callback.onStartLoad(httpClient);
		}
		File file = new File(filePath + ".tmp");
		try {
			try {
				file.createNewFile();
			} catch (Exception e) {
				file.getParentFile().delete();
				file.getParentFile().mkdirs();
				file.createNewFile();
			}
			FileUtil.checkPath(file.getParent() + "/");
			long length = file.length();
			HttpGet httpGet = new HttpGet(url);
			if(callback != null && callback.isCancel()){
				return false;
			}
			if(isRange){
				httpGet.setHeader("RANGE", "bytes=" + length + "-");
			}
			HttpResponse httpResponse = httpClient.execute(httpGet);
			HttpEntity httpEntiry = httpResponse.getEntity();
			if (httpEntiry != null) {
				if(callback != null && callback.isCancel()){
					return false;
				}
				long contentLength = httpEntiry.getContentLength() + length;
				boolean hasLength = contentLength > 0;
				InputStream is = httpEntiry.getContent();
				byte[] responseByteArray = new byte[BUFFER_SIZE];
				FileOutputStream os = new FileOutputStream(file,isRange);
				int line = -1;
				int currentLength = 0;
				while ((line = is.read(responseByteArray)) != -1) {
					os.write(responseByteArray, 0, line);
					if(callback != null && hasLength){
						currentLength += line;
						callback.onDownloadProgress(url,currentLength + length,contentLength);
					}
				}
				os.close();
				is.close();
				if((httpEntiry.getContentLength() == 0 || contentLength == file.length()) && file.length() > 0){
					file.renameTo(new File(filePath));//TODO 图片完整性验证
					return true;
				}else{
					file.delete();
				}
			}
		} catch (Exception e) {
			LogUtil.w(TAG, e);
		} finally {
			if (httpClient != null) {
				httpClient.getConnectionManager().shutdown();
				httpClient = null;
			}
		}
		return false;
	}
	
	/**
	 * 存储图片到缓存目录
	 * 
	 * @param name
	 *            图片名字
	 * @param bitmap
	 *            保存的图
	 * @return
	 */
	public static boolean saveImage(String filePath, byte[] data) {
		if (TextUtils.isEmpty(filePath)) {
			return false;
		}
		if (!FileUtil.isSDCardExist()) {
			LogUtil.v(TAG, "sdcard is not exist!");
			return false;
		}
		try {
			File imgFile = new File(filePath);
			try {
				imgFile.createNewFile();
			} catch (Exception e) {
				imgFile.getParentFile().delete();
				imgFile.getParentFile().mkdirs();
				imgFile.createNewFile();
			}
			FileOutputStream os = new FileOutputStream(imgFile,false);
			os.write(data);
			os.close();
			return true;
		} catch (Exception e) {
			LogUtil.e(TAG, "save image err", e);
		}
		return false;
	}
	
	public static boolean saveImage(String filePath, Bitmap bitmap) {
		return saveImage(filePath, bitmap,100);
	}
	
	public static boolean saveImage(String filePath, Bitmap bitmap,int quality) {
		if(bitmap == null){
			return false;
		}
		if(bitmap.hasAlpha()){
			return saveImage(filePath, bitmap,quality,CompressFormat.PNG);
		}else{
			return saveImage(filePath, bitmap,quality,CompressFormat.JPEG);
		}
	}
	/**
	 * 存储图片到缓存目录
	 * 
	 * @param name
	 *            图片名字
	 * @param bitmap
	 *            保存的图
	 * @return
	 */
	public static boolean saveImage(String filePath, Bitmap bitmap,int quality,CompressFormat format) {
		if (TextUtils.isEmpty(filePath)) {
			return false;
		}
		if (!FileUtil.isSDCardExist()) {
			LogUtil.v(TAG, "sdcard is not exist!");
			return false;
		}
		try {
			File imgFile = new File(filePath);
			FileUtil.checkPath(imgFile.getParent() + "/");
			try {
				imgFile.createNewFile();
			} catch (Exception e) {
				imgFile.getParentFile().delete();
				imgFile.getParentFile().mkdirs();
				imgFile.createNewFile();
			}
			FileOutputStream out = new FileOutputStream(imgFile);
			//like PNG which is lossless, will ignore the quality setting :png忽略第二个参数
			bitmap.compress(format, quality, out);
			out.close();
			return true;
		} catch (Exception e) {
			LogUtil.e(TAG, "save image err", e);
		}
		return false;
	}
	
	public static byte[] getBitmapByteByUri(Uri uri){
		ByteArrayBuffer baos = new ByteArrayBuffer(1024);
		try {
			InputStream is = getContentResolver().openInputStream(uri);
			byte[] buffer = new byte[1024];
			int len = 0;
			while((len = is.read(buffer)) != -1){
				baos.append(buffer,0,len);
			}
			is.close();
		} catch (Exception e) {
			LogUtil.e(TAG, e);
		}
		return baos.toByteArray();
	}
	
	public static float getTargetSizeScale(int targetSize){
		return (float) Math.sqrt(targetSize * 1f / (getDisplay().widthPixels * getDisplay().heightPixels));
	}
	
	public static boolean isGifPath(String path){
		boolean isGifPath = false;
		if(path != null){
			if(path.toLowerCase().lastIndexOf(".gif") != -1){
				isGifPath = true;
			}
		}
		return isGifPath;
	}
	
	public static boolean isPngPath(String path){
		boolean isPngPath = false;
		if(path != null){
			if(path.toLowerCase().lastIndexOf(".png") != -1){
				isPngPath = true;
			}
		}
		return isPngPath;
	}
	
	public static boolean isMp4Path(String path){
		boolean isMp4Path = false;
		if(path != null){
			if(path.toLowerCase().lastIndexOf(".mp4") != -1){
				isMp4Path = true;
			}
		}
		return isMp4Path;
	}
	
	public static Bitmap getBitmapInSdcard(String filePath,int quality) {
		float scaled = getQualityScale(quality);
		int targetW = (int) (getDisplay().widthPixels * scaled);
        int targetH = (int) (getDisplay().heightPixels * scaled);
		return getBitmapInSdcard(filePath, targetW, targetH);
	}
	
	public static Bitmap getBitmapInSdcard(String filePath, int targetW, int targetH) {
		if (TextUtils.isEmpty(filePath)) {
			return null;
		}
		if (!FileUtil.isFileExists(filePath)) {
			return null;
		}
		Bitmap bitmap = null;
		if(isMp4Path(filePath)){
			bitmap = getVideoCoverInSdcard(filePath);
		}else{
			bitmap = getImageInSdcard(filePath, targetW, targetH);
		}
		return bitmap;
	}
	
	public static Bitmap getVideoCoverInSdcard(String filePath) {
		Bitmap bitmap = null;
		try {
			MediaMetadataRetriever retriever = new MediaMetadataRetriever();
			retriever.setDataSource(filePath);
			bitmap = retriever.getFrameAtTime(0);
			retriever.release();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bitmap;
	}
	
	public static Bitmap getImageInSdcard(String filePath, int targetW, int targetH) {
		Bitmap bitmap = null;
		Uri uri = Uri.parse("file://"+filePath);
		try {
			int degree = readPictureDegree(filePath);
			bitmap = clipScreenBoundsBitmap(getResources(),uri,targetW,targetH);
			if(degree != 0 && bitmap != null) {
				bitmap = rotaingImageView(degree, bitmap);
			}
			return bitmap;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bitmap;
	}
	
	public static Bitmap clipBitmap(Bitmap bitmap, int quality){
		float scaled = getQualityScale(quality);
		int targetW = (int) (getDisplay().widthPixels * scaled);
        int targetH = (int) (getDisplay().heightPixels * scaled);
		return clipBitmap(bitmap, targetW, targetH);
	}
	
	public static Bitmap clipBitmap(Bitmap bitmap, int targetW, int targetH){
		if(bitmap != null){
			float targetSize = targetW * targetH;
			int imgSize = bitmap.getWidth() * bitmap.getHeight();
			if(imgSize > targetSize){
				float densityScaled = (float) Math.sqrt(targetSize / imgSize);
				if(Math.abs(densityScaled - 1) > 0.01f){
					Bitmap bitmapTemp = Bitmap.createScaledBitmap(bitmap, (int)(densityScaled * bitmap.getWidth()), (int)(densityScaled * bitmap.getHeight()), true);
					if(bitmapTemp != null){
						bitmap.recycle();
						bitmap = bitmapTemp;
					}
				}
			}
		}
		return bitmap;
	}
	
	public static Bitmap getImageInResources(int res,int quality) {
		Uri uri = Uri.parse(getResourcesUri(res));
		try {
			return clipScreenBoundsBitmap(getResources(), uri, getQualityScale(quality));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static Bitmap getImageInResources(String resourcesUri,int targetW,int targetH) {
		Uri uri = Uri.parse(resourcesUri);
		try {
			return clipScreenBoundsBitmap(getResources(),uri,targetW,targetH);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static byte[] bitmapToJpgByte(Bitmap bitmap){
		return bitmapToJpgByte(bitmap,100);
	}
	
	public static byte[] bitmapToJpgByte(Bitmap bitmap,int quality){
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.JPEG, quality, os);
		return os.toByteArray();
	}
	
	public static byte[] bitmapToPngByte(Bitmap bitmap){
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.PNG, 100, os);
		return os.toByteArray();
	}
	
	public static Bitmap byteToBitmap(byte[] data,int inSampleSize){
		Options opts = new BitmapFactory.Options();
		opts.inSampleSize = inSampleSize;
		Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length,opts);
		return bitmap;
	}
	
	public static Bitmap byteToBitmap(byte[] data){
		Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
		return bitmap;
	}
	
	private static synchronized SparseArray<int[]> getSizeCache(){
		if(sSizeCache == null){
			sSizeCache = new SparseArray<int[]>();
		}
		return sSizeCache;
	}
	
	private static synchronized SparseArray<Boolean> getFileDBCache(){
		if(sFileDBCache == null){
			sFileDBCache = new SparseArray<Boolean>();
		}
		return sFileDBCache;
	}
	
	public static void saveLocalWH(String uriStr,int width,int height){
		if(TextUtils.isEmpty(uriStr)){
			return;
		}
		Uri uri = Uri.parse(uriStr);
		if(TextUtils.isEmpty(uri.getScheme())){
			uriStr = "file://" + uri.getPath();
			uri = Uri.parse(uriStr);
		}
		synchronized (getSizeCache()) {
			getSizeCache().put(uriStr.hashCode(), new int[]{width,height});
		}
	}
	
	public static int[] getLocalCacheWH(String uriStr){
		int[] wh = null;
		if(TextUtils.isEmpty(uriStr)){
			return wh;
		}
		Uri uri = Uri.parse(uriStr);
		if(TextUtils.isEmpty(uri.getScheme())){
			uriStr = "file://" + uri.getPath();
			uri = Uri.parse(uriStr);
		}
		synchronized (getSizeCache()) {
			wh = getSizeCache().get(uriStr.hashCode());
		}
		return wh;
	}
	
	public static int[] getLocalWH(String uriStr){
		return getLocalWH(uriStr,null,false);
	}
	
	public static int[] getLocalWH(String uriStr, Integer quality){
		return getLocalWH(uriStr,quality,false);
	}
	
	public static int[] getLocalWH(String uriStr, Integer quality , boolean forceLoadSource){
		int[] wh = null;
		if(TextUtils.isEmpty(uriStr)){
			return wh;
		}
		Uri uri = Uri.parse(uriStr);
		if(TextUtils.isEmpty(uri.getScheme())){
			uriStr = "file://" + uri.getPath();
			uri = Uri.parse(uriStr);
		}
		synchronized (getSizeCache()) {
			wh = getSizeCache().get(uriStr.hashCode());
		}
		String path = uri.getPath();
		boolean isMp4Path = isMp4Path(path);
		if(wh == null && !isMp4Path){
			boolean isFile = ContentResolver.SCHEME_FILE.equals(uri.getScheme());
			boolean isFileExists = false;
			int degree = -1;
			int width = 0;
			int height = 0;
			if(isFile){
				File file = new File(path);
				String parent = file.getParent();
				isFileExists = file.exists();
				if(isFileExists){
					if(!forceLoadSource){
						try {
							Boolean fileCanLoadDB = null;
							synchronized (getFileDBCache()) {
								fileCanLoadDB = getFileDBCache().get(parent.hashCode());
							}
							if(fileCanLoadDB == null){
								ContentResolver contentResolver = BaseApp.getInstance().getContentResolver();
								Cursor cursor = contentResolver.query(Images.Media.EXTERNAL_CONTENT_URI,new String[]{Images.Media.ORIENTATION,Images.Media.WIDTH,Images.Media.HEIGHT},
										Images.Media.DATA + "=?", new String[] {path},null);
								if(cursor != null){
									if(cursor.moveToFirst()){
//										degree = readPictureDegree(cursor.getInt(cursor.getColumnIndex(Images.Media.ORIENTATION)));//三星手机读取不到
										width = cursor.getInt(cursor.getColumnIndex(Images.Media.WIDTH));
										height = cursor.getInt(cursor.getColumnIndex(Images.Media.HEIGHT));
									}
									cursor.close();
								}
								synchronized (getFileDBCache()) {
									getFileDBCache().put(parent.hashCode(), false);
								}
							}
						} catch (Exception e) {}
					}
					if(degree == -1 && !isMp4Path){
						degree = readPictureDegree(path);
					}
				}else{
					degree = 0;
				}
			}else{
				degree = 0;
			}
			if(width == 0 || height == 0){
				if((!isFile || isFileExists) && !isMp4Path){
					wh = getLocalWHBySource(uri);
				}
			}else{
				wh = new int[]{width,height};
			}
			if(wh != null){
				if(isFile && degree != 0 && degree != 180) {
	    			int tmp = wh[0];
	    			wh[0] = wh[1];
	    			wh[1] = tmp;
	    		}
				synchronized (getSizeCache()) {
					getSizeCache().put(uriStr.hashCode(), wh);
				}
			}
		}
		if(wh != null){
			wh = new int[]{wh[0],wh[1]};
			if(quality != null){
				float densityScaled = 1;
		        float imgSize = wh[0] * wh[1];
		        float scaled = getQualityScale(quality);
				int targetW = (int) (getDisplay().widthPixels * scaled);
		        int targetH = (int) (getDisplay().heightPixels * scaled);
				float targetSize = targetW * targetH;
		        if(imgSize > targetSize){
		        	densityScaled = (float) Math.sqrt(targetSize / imgSize);
		        }
		        wh[0] = (int) (wh[0] * densityScaled);
		        wh[1] = (int) (wh[1] * densityScaled);
			}
		}
		return wh;
	}
	
	public static int[] getLocalWHBySource(String uriStr){
		if(TextUtils.isEmpty(uriStr)){
			return null;
		}
		Uri uri = Uri.parse(uriStr);
		if(TextUtils.isEmpty(uri.getScheme())){
			uriStr = "file://" + uri.getPath();
			uri = Uri.parse(uriStr);
		}
		return getLocalWHBySource(uri);
	}
	
	public static int[] getLocalWHBySource(Uri uri){
		int[] wh = null;
		try {
			InputStream is = getContentResolver().openInputStream(uri);
			Options opts = new BitmapFactory.Options();
			opts.inScaled = false;
	        opts.inJustDecodeBounds = true;
	        BitmapFactory.decodeStream(is, null, opts);
	        wh = new int[]{opts.outWidth,opts.outHeight};
	        is.close();
		} catch (Exception e) {
			LogUtil.e(TAG, e);
		}
		return wh;
	}
	
	public static int[] getLocalVideoWHByDB(String path){
		int width = 0;
		int height = 0;
		String id = null;
		try {
			ContentResolver contentResolver = BaseApp.getInstance().getContentResolver();
			Cursor cursor = contentResolver.query(Video.Media.EXTERNAL_CONTENT_URI, null,
					Video.Media.DATA + "=?", new String[] {path},null);
			if(cursor != null){
				if(cursor.moveToFirst()){
					id = cursor.getString(cursor.getColumnIndex(Video.Media._ID));
					String whData = cursor.getString(cursor.getColumnIndex(Video.Media.RESOLUTION));
					if(!TextUtils.isEmpty(whData)){
						String[] whDataArr = whData.split("x");
						width = Integer.valueOf(whDataArr[0]);
						height = Integer.valueOf(whDataArr[1]);
					}
				}
				cursor.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return getLocalVideoWHByDB(path, id, width, height);
	}
	
	public static int[] getLocalVideoWHByDB(String path, String id, int width, int height){
		int[] wh = null;
		int tw = 0;
		int th = 0;
		try {
			if(!TextUtils.isEmpty(id)){
				ContentResolver contentResolver = BaseApp.getInstance().getContentResolver();
				Cursor cursorThumbnails = contentResolver.query(Video.Thumbnails.EXTERNAL_CONTENT_URI, new String[]{Video.Thumbnails.WIDTH, Video.Thumbnails.HEIGHT},
						Video.Thumbnails.VIDEO_ID + "=?", new String[] {id}, null);
				if(cursorThumbnails != null){
					while (cursorThumbnails.moveToNext()) {
						int ttw = cursorThumbnails.getInt(cursorThumbnails.getColumnIndex(Video.Thumbnails.WIDTH));
						int tth = cursorThumbnails.getInt(cursorThumbnails.getColumnIndex(Video.Thumbnails.HEIGHT));
						if(tw < ttw && th < tth){
							tw = ttw;
							th = tth;
						}
					}
					cursorThumbnails.close();
				}
				if(tw > 0 && th > 0){
					if(width == 0 || height == 0){
						width = tw;
						height = th;
					}else{
						if(tw > th && width < height || tw < th && width > height){
							int temp = width;
							width = height;
							height = temp;
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if(width != 0 && height != 0 && tw > 0 && th > 0){
			wh = new int[]{width, height};
		}
		return wh;
	}
	
	public static String getImgFormat(String uriStr){
		String format = "jpg";
		if(TextUtils.isEmpty(uriStr)){
			return format;
		}
		try {
			Uri uri = Uri.parse(uriStr);
			String filePath = null;
			if(ContentResolver.SCHEME_FILE.equals(uri.getScheme())){
				filePath = uri.getPath();
			}else if(!ContentResolver.SCHEME_ANDROID_RESOURCE.equals(uri.getScheme())
					&& !ContentResolver.SCHEME_CONTENT.equals(uri.getScheme())
			        && !ContentResolver.SCHEME_FILE.equals(uri.getScheme())){
				filePath = uri.getPath();
				uriStr = "file://" + filePath;
				uri = Uri.parse(uriStr);
			}
			if(filePath != null && !FileUtil.isFileExists(filePath)){
				return format;
			}
			InputStream is = getContentResolver().openInputStream(uri);
			Options opts = new BitmapFactory.Options();
			opts.inScaled = false;
	        opts.inJustDecodeBounds = true;  
	        BitmapFactory.decodeStream(is, null, opts);
	        format = "image/png".equals(opts.outMimeType) ? "png" : "jpg";
	        is.close();
		} catch (Exception e) {
			LogUtil.e(TAG, e);
		}
		return format;
	}
	
	public static int[] getNetWH(String url){
		int[] wh = new int[2];
		if(TextUtils.isEmpty(url)){
			return wh;
		}
		DefaultHttpClient httpClient = AbsConnect.getDefaultHttpClient(getContext());
		try {
			HttpGet httpGet = new HttpGet(url);
			HttpResponse httpResponse = httpClient.execute(httpGet);
			HttpEntity httpEntiry = httpResponse.getEntity();
			if (httpEntiry != null) {
				InputStream is = httpEntiry.getContent();
				Options opts = new BitmapFactory.Options();
				opts.inScaled = false;
		        opts.inJustDecodeBounds = true;  
		        BitmapFactory.decodeStream(is, null, opts);
		        wh[0] = opts.outWidth;
		        wh[1] = opts.outHeight;
		        is.close();
			}
		} catch (Exception e) {
			LogUtil.e(TAG, e);
		} finally {
			if (httpClient != null) {
				httpClient.getConnectionManager().shutdown();
				httpClient = null;
			}
		}
		return wh;
	}
	
	public static Bitmap clipScreenBoundsBitmap(Resources resources,final Uri uri,int targetW,int targetH) throws IOException{
		return clipScreenBoundsBitmap(resources, uri, new InputStreamProvider() {
			@Override
			public InputStream newInputStream() {
				try {
					return getContentResolver().openInputStream(uri);
				} catch (Exception e) {
					LogUtil.e(TAG, e);
				}
				return null;
			}
		},targetW,targetH);
	}
	
	public static Bitmap clipScreenBoundsBitmap(Resources resources,final Uri uri,float scaled) throws IOException{
		int targetW = (int) (getDisplay().widthPixels * scaled);
        int targetH = (int) (getDisplay().heightPixels * scaled);
		return clipScreenBoundsBitmap(resources, uri, new InputStreamProvider() {
			@Override
			public InputStream newInputStream() {
				try {
					return getContentResolver().openInputStream(uri);
				} catch (Exception e) {
					LogUtil.e(TAG, e);
				}
				return null;
			}
		}, targetW, targetH);
	}
	
	public static Bitmap clipScreenBoundsBitmap(Resources resources, Uri uri ,InputStreamProvider isProvider,int targetW,int targetH) throws IOException{
		InputStream is = isProvider.newInputStream();
		Bitmap bitmap = null;
		if(is != null){
			Options opts = new BitmapFactory.Options();
			opts.inScaled = false;
	        opts.inJustDecodeBounds = true;
	        BitmapFactory.decodeStream(is, null, opts);
	        int imgW = opts.outWidth;
	        int imgH = opts.outHeight;  
	        if (!is.markSupported()) {
		        is.close();
		        is = isProvider.newInputStream();
			}else{
		        is.reset();
			}
	        if(is != null){
	        	bitmap = clipBitmap(resources, is, targetW, targetH, imgW, imgH, "image/png".equals(opts.outMimeType));
		        is.close();
	        }
		}
        return bitmap;
	}
	
	public static Bitmap clipBitmap(Resources resources,InputStream is,int targetW,int targetH,int imgW,int imgH, boolean isPng){
		try{
			if(is != null){
				Options opts = new BitmapFactory.Options();
		        float densityScaled = 1;
		        long imgSize = imgW * imgH;
		        float targetSize = targetW * targetH;
		        if(imgSize > targetSize){
		        	densityScaled = (float) Math.sqrt(targetSize / imgSize);
		        }
		        if(VERSION.SDK_INT >= 14){
					opts.inMutable = true;
		        }
				opts.inPreferredConfig = sInPreferredConfig;
		        if(densityScaled != 1){
		        	int inSampleSize = new BigDecimal(String.valueOf(1 / densityScaled)).setScale(0, BigDecimal.ROUND_HALF_UP).intValue();
		        	if(inSampleSize > 1){
				        densityScaled *= inSampleSize;
						opts.inSampleSize = inSampleSize;
		        	}
			        opts.inTargetDensity = getDisplay().densityDpi;
					opts.inDensity = (int) (getDisplay().densityDpi / densityScaled);
					opts.inScaled = true;
		        }
		        return BitmapFactory.decodeStream(is, null, opts);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static float getQualityScale(int quality){
		switch (quality) {
		case IMG_QUALITY_NON:
			return 1f;
		case IMG_QUALITY_0:
			return 0.7f;
		case IMG_QUALITY_1:
			return 0.5f;
		case IMG_QUALITY_2:
			return 0.4f;
		case IMG_QUALITY_3:
			return 0.3f;
		case IMG_QUALITY_4:
			return 0.2f;
		case IMG_QUALITY_5:
			return 0.1f;
		}
		return 1;
	}
	
	private static boolean checkNeedHandleDegree(){
		return Build.MANUFACTURER.equalsIgnoreCase("samsung") || Build.MANUFACTURER.equalsIgnoreCase("Meizu");
	}
	
	/**
     * 读取图片属性：旋转的角度
     * @param path 图片绝对路径
     * @return degree旋转的角度
     */
	public static int readPictureDegree(String path) {
		int degree = 0;
		boolean check = checkNeedHandleDegree();
		if (check) {
			try {
				degree = readPictureDegree(new ExifInterface(path).getAttributeInt(ExifInterface.TAG_ORIENTATION,ExifInterface.ORIENTATION_NORMAL),check);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return degree;
	}
	
	public static int readPictureDegree(int orientation) {
		return readPictureDegree(orientation,checkNeedHandleDegree());
	}
	
	private static int readPictureDegree(int orientation,boolean check) {
		int degree = 0;
		if (check) {
			switch (orientation) {
			case ExifInterface.ORIENTATION_ROTATE_90:
				degree = 90;
				break;
			case ExifInterface.ORIENTATION_ROTATE_180:
				degree = 180;
				break;
			case ExifInterface.ORIENTATION_ROTATE_270:
				degree = 270;
				break;
			}
		}
		return degree;
	}
      
	/** 旋转图片
	 * @param angle
	 * @param bitmap
	 * @return
	 */
	public synchronized static Bitmap rotaingImageView(int angle, Bitmap bitmap) {
		// 旋转图片 动作
		Matrix matrix = new Matrix();
		matrix.postRotate(angle);
		// 创建新的图片
		Bitmap resizedBitmap = Bitmap.createBitmap(bitmap, 0, 0,
				bitmap.getWidth(), bitmap.getHeight(), matrix, true);
		bitmap.recycle();
		return resizedBitmap;
	}
	
	public interface InputStreamProvider{
		public InputStream newInputStream();
	}
	
	public interface LoadNetCallback{
		public void onStartLoad(DefaultHttpClient httpClient);
		public void onDownloadProgress(String imageUrl,long p,long m);
		public boolean isCancel();
	}
	
	public interface DownloadCallback {
		public void onDownloadProgress(String imageUrl,int p,int m);
	}
}
