package com.bk.android.util;

import android.content.Context;
import android.content.res.Resources;
import android.util.TypedValue;
import android.view.View;
import android.view.View.MeasureSpec;

import com.bk.android.app.BaseApp;

/**
 * 尺寸计算工具类
 * @author linyiwei
 * @email 21551594@qq.com
 * @date 2012-07-27
 */
public class DimensionsUtil {
	private static Context getContext(){
		return BaseApp.getInstance();
	}
	
	private static Resources getResources(){
		return getContext().getResources();
	}
	
	public static float sizeToPX(int unit, float size){
		return TypedValue.applyDimension(unit, size, getResources().getDisplayMetrics());
	}
	
	public static int DIPToPX(float size){ 
		return (int) DIPToPXF(size);
	}
	
	public static float DIPToPXF(float size){ 
		return sizeToPX(TypedValue.COMPLEX_UNIT_DIP, size);
	}
	
	public static int PXToDIP(float pxValue){ 
        final float scale = getResources().getDisplayMetrics().density; 
        return (int)(pxValue / scale); 
	}
	
	public static void measureView(View view){
		final int widthMeasureSpec =
			    MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED);
		final int heightMeasureSpec =
		    MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED);
		view.measure(widthMeasureSpec, heightMeasureSpec);
	}
	
}
