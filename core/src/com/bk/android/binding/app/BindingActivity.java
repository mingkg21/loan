package com.bk.android.binding.app;

import gueei.binding.Binder;
import gueei.binding.Binder.InflateResult;

import java.lang.ref.WeakReference;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;

import com.bk.android.util.ReleaseUtil;

public class BindingActivity extends FragmentActivity{
	private WeakReference<View> mRootViewRef;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	@Override
    protected void onDestroy() {
		super.onDestroy();
    }
	
	// idea from : http://stackoverflow.com/questions/1147172/what-android-tools-and-methods-work-best-to-find-memory-resource-leaks
	/**
	 * Original Name: unbindDrawables. Change to this to avoid "bind" since binding is different meaning for A-B
	 */
	protected void releaseDrawables() {
		if(mRootViewRef != null && mRootViewRef.get() != null)
			ReleaseUtil.releaseDrawables(mRootViewRef.get());   		
	}
	
	protected View setAndBindRootView(int layoutId, Object... contentViewModel){
		if (mRootViewRef != null && mRootViewRef.get() !=null){
			throw new IllegalStateException("Root view is already created");
		}
		InflateResult result = Binder.inflateView(this, layoutId, null, false);
		mRootViewRef = new WeakReference<View>(result.rootView);
		for(int i = 0; i < contentViewModel.length; i++){
			Binder.bindView(this, result, contentViewModel[i]);
		}
		setContentView(mRootViewRef.get());
		return mRootViewRef.get();
	}
	
	protected InflateResult inflateView(int layoutId){
		return Binder.inflateView(this, layoutId, null, false);
	}
	
	protected View bindView(InflateResult result, Object... contentViewModel){
		for(int i = 0; i < contentViewModel.length; i++){
			Binder.bindView(this, result, contentViewModel[i]);
		}
		return result.rootView;
	}
	
	protected View bindView(int layoutId, Object... contentViewModel){
		InflateResult result = inflateView(layoutId);
		return bindView(result, contentViewModel);
	}
}
