package com.bk.android.binding.app;

import gueei.binding.Binder;
import gueei.binding.Binder.InflateResult;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.bk.android.app.BaseDialog;

public class BindingDialog extends BaseDialog {
	public BindingDialog(Context context) {
		super(context);
	}
	
	protected BindingDialog(Context context, boolean cancelable,
			OnCancelListener cancelListener) {
		super(context, cancelable, cancelListener);
	}
	
	public BindingDialog(Context context, int theme) {
		super(context, theme);
	}
	
	protected View bindView(int layoutId,ViewGroup parent, Object... contentViewModel){
		InflateResult result = Binder.inflateView(getActivity(), layoutId, parent, parent != null);
		for(int i = 0; i < contentViewModel.length; i++){
			Binder.bindView(getActivity(), result, contentViewModel[i]);
		}
		return result.rootView;
	}
}
