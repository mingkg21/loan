package com.bk.android.binding.app;

import gueei.binding.Binder;
import gueei.binding.Binder.InflateResult;
import android.view.View;
import android.view.ViewGroup;

import com.bk.android.app.BasePopWindow;

public abstract class BindingPopWindow extends BasePopWindow {
	public BindingPopWindow(View parent) {
		super(parent);
	}

	public BindingPopWindow(View parent, int lpWidth, int lpHeight) {
		super(parent, lpWidth, lpHeight);
	}

	protected View bindView(int layoutId,ViewGroup parent, Object... contentViewModel){
		InflateResult result = Binder.inflateView(getContext(), layoutId, parent, parent != null);
		for(int i = 0; i < contentViewModel.length; i++){
			Binder.bindView(getContext(), result, contentViewModel[i]);
		}
		return result.rootView;
	}
}
