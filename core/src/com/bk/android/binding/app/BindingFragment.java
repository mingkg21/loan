package com.bk.android.binding.app;

import gueei.binding.Binder;
import gueei.binding.Binder.InflateResult;
import gueei.binding.menu.OptionsMenuBinder;
import android.app.Activity;
import android.support.v4.app.Fragment;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;


@SuppressWarnings("deprecation")
public class BindingFragment extends Fragment{
	OptionsMenuBinder menuBinder;
	Object mMenuViewModel;
	
	protected void setAndBindOptionsMenu(int menuId, Object menuViewModel){
		if (menuBinder!=null){
			throw new IllegalStateException("Options menu can only set once");
		}
		menuBinder = new OptionsMenuBinder(menuId);
		mMenuViewModel = menuViewModel;
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		if (menuBinder != null && getActivity() != null){
			menuBinder.onCreateOptionsMenu(getActivity(), menu, mMenuViewModel);
		}
	}

	@Override
	public void onPrepareOptionsMenu(Menu menu) {
		if (menuBinder != null && getActivity() != null){
			menuBinder.onPrepareOptionsMenu(getActivity(), menu);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (menuBinder != null && getActivity() != null){
			return menuBinder.onOptionsItemSelected(getActivity(), item);
		}
		return super.onOptionsItemSelected(item);
	}
	
	protected View bindView(int layoutId,ViewGroup parent, Object... contentViewModel){
		Activity activity = getActivity();
		if(activity == null){
			return null;
		}
		InflateResult result = Binder.inflateView(activity, layoutId, parent, parent != null);
		bindView(result, contentViewModel);
		return result.rootView;
	}
	
	protected View bindView(InflateResult result, Object... contentViewModel){
		Activity activity = getActivity();
		if(activity == null){
			return null;
		}
		for(int i = 0; i < contentViewModel.length; i++){
			Binder.bindView(activity, result, contentViewModel[i]);
		}
		return result.rootView;
	}
	
	protected InflateResult inflateView(int layoutId,ViewGroup parent, boolean attachToRoot){
		Activity activity = getActivity();
		if(activity == null){
			return null;
		}
		return Binder.inflateView(activity, layoutId, parent, parent != null);
	}
}
