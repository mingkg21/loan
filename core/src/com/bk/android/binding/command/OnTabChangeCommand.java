package com.bk.android.binding.command;

import android.view.View;

public abstract class OnTabChangeCommand extends BaseCommand {
	@Override
	protected void onInvoke(View view, Object... args) {
		if(args == null || args.length < 1
				|| !(args[0] instanceof String)){
			return;
		}
		onTabChanged((String)args[0]);
	}
	
	public abstract void onTabChanged(String tabId);
}
