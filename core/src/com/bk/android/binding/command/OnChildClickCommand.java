package com.bk.android.binding.command;

import android.view.View;
import android.widget.ExpandableListView;

public abstract class OnChildClickCommand extends BaseCommand {
	@Override
	protected void onInvoke(View view, Object... args) {
		if(args == null || args.length < 4
				|| !(view instanceof ExpandableListView)
				|| !(args[0] instanceof View)
				|| !(args[1] instanceof Integer)
				|| !(args[2] instanceof Integer)
				|| !(args[3] instanceof Long)){
			return;
		}
		onChildClick((ExpandableListView)view,(View)args[0],(Integer)args[1],(Integer)args[2],(Long)args[3]);
	}

	public abstract boolean onChildClick(ExpandableListView parent, View v, int groupPosition,
            int childPosition, long id);
}
