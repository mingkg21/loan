package com.bk.android.binding.command;

import android.support.v4.view.ViewPager;
import android.view.View;

public abstract class OnViewPagerSelectedCommand extends BaseCommand {
	@Override
	protected void onInvoke(View view, Object... args) {
		if(args == null || args.length < 1
				|| !(view instanceof ViewPager)
				|| !(args[0] instanceof Integer)){
			return;
		}
		onPageSelected((ViewPager)view,(Integer)args[0]);
	}
	
	public abstract void onPageSelected(ViewPager view,int position);
}
