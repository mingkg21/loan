package com.bk.android.binding.command;

import android.view.View;
import android.widget.CompoundButton;

public abstract class OnCheckedChangeCommand extends BaseCommand{
	@Override
	protected void onInvoke(View view, Object... args) {
		if(args == null || args.length < 1 
				|| !(view instanceof CompoundButton) 
				|| !(args[0] instanceof Boolean)){
			return;
		}
		onCheckedChanged((CompoundButton)view, (Boolean)args[0]);
	}
	
	public abstract void onCheckedChanged(CompoundButton buttonView, boolean isChecked);
}
