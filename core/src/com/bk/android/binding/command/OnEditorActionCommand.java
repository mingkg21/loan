package com.bk.android.binding.command;

import gueei.binding.viewAttributes.textView.OnEditorActionViewEvent.EditorActionResult;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

public abstract class OnEditorActionCommand extends BaseCommand {
	@Override
	protected void onInvoke(View view, Object... args) {
		if(args == null || args.length < 3
				|| !(view instanceof TextView)
				|| !(args[0] instanceof Integer)
				|| !(args[2] instanceof EditorActionResult) ){
			return;
		}
		EditorActionResult result = (EditorActionResult)args[2];
		result.mResult = onEditorAction((TextView) view, (Integer)args[0], (KeyEvent)args[1]);
	}

	public abstract boolean onEditorAction(TextView v, int actionId, KeyEvent event);
}
