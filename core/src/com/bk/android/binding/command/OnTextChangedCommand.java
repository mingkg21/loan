package com.bk.android.binding.command;

import android.view.View;
import android.widget.EditText;

public abstract class OnTextChangedCommand extends BaseCommand {
	@Override
	protected void onInvoke(View view, Object... args) {
		if(args == null || args.length < 4
				|| !(view instanceof EditText)
				|| !(args[0] instanceof CharSequence)
				|| !(args[1] instanceof Integer)
				|| !(args[2] instanceof Integer)
				|| !(args[3] instanceof Integer)){
			return;
		}
		onTextChanged((EditText)view, (CharSequence)args[0], (Integer)args[1], (Integer)args[2], (Integer)args[3]);
	}

	public abstract void onTextChanged(EditText editText,CharSequence s, int start, int before, int count);
}
