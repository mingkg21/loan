package com.bk.android.binding.command;

import android.view.View;
import android.widget.SeekBar;

public abstract class OnSeekBarChangeCommand extends BaseCommand {
	@Override
	protected void onInvoke(View view, Object... args) {
		if(args == null || args.length < 2
				|| !(view instanceof SeekBar)
				|| !(args[0] instanceof Integer)
				|| !(args[1] instanceof Boolean)){
			return;
		}
		onProgressChanged((SeekBar)view,(Integer)args[0],(Boolean)args[1]);
	}
	
	public abstract void onProgressChanged(SeekBar seekBar, int progress,boolean fromUser);
}
