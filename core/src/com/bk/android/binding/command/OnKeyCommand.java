package com.bk.android.binding.command;

import gueei.binding.viewAttributes.view.KeyEventResult;
import android.view.KeyEvent;
import android.view.View;

public abstract class OnKeyCommand extends BaseCommand {
	@Override
	protected void onInvoke(View view, Object... args) {
		if(args == null || args.length < 3
				|| !(args[0] instanceof Integer)
				|| !(args[1] instanceof KeyEvent)
				|| !(args[2] instanceof KeyEventResult)){
			return;
		}
		KeyEventResult result = (KeyEventResult) args[2];
		result.eventConsumed = onKey(view,(Integer)args[0],(KeyEvent) args[1]);
	}
	
	public abstract boolean onKey(View v, int keyCode, KeyEvent event);
}
