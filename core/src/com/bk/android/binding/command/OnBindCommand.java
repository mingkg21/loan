package com.bk.android.binding.command;

import android.view.View;

public abstract class OnBindCommand extends BaseCommand {
	@Override
	protected void onInvoke(View view, Object... args) {
		onBind(view);
	}

	public abstract void onBind(View view);
}
