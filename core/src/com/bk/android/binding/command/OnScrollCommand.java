package com.bk.android.binding.command;

import android.view.View;
import android.widget.AbsListView;

public abstract class OnScrollCommand extends BaseCommand {
	
	@Override
	protected void onInvoke(View view, Object... args) {
		if(args == null || args.length < 3
				|| !(view instanceof AbsListView)
				|| !(args[0] instanceof Integer)
				|| !(args[1] instanceof Integer)
				|| !(args[2] instanceof Integer)){
			return;
		}
		onScroll((AbsListView)view,(Integer)args[0],(Integer)args[0],(Integer)args[0]);
	}
	
	public abstract void onScroll(AbsListView view, int firstVisibleItem,int visibleItemCount, int totalItemCount);
}
