package com.bk.android.binding.command;

import android.view.View;


public abstract class OnGainFocusCommand extends BaseCommand{
	@Override
	protected void onInvoke(View view, Object... args) {
		onGainFocus(view);
	}
	
	public abstract void onGainFocus(View view);
}