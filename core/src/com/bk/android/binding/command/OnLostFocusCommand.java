package com.bk.android.binding.command;

import android.view.View;

public abstract class OnLostFocusCommand extends BaseCommand{

	@Override
	protected void onInvoke(View view, Object... args) {
		onLostFocus(view);
	}

	public abstract void onLostFocus(View view);
}
