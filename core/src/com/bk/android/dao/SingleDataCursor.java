package com.bk.android.dao;

public class SingleDataCursor extends BaseDataCursor {
	private String mColumnName;
	private Object mData;
	public SingleDataCursor(String columnName,Object data){
		mColumnName = columnName;
		mData = data;
	}
	
	@Override
	public int getCount() {
		return 1;
	}

	@Override
	public String[] getColumnNames() {
		return new String[]{mColumnName} ;
	}

	@Override
	public String getString(int column) {
		if(mData instanceof String){
			return (String) mData;
		}
		return null;
	}

	@Override
	public long getLong(int column) {
		if(mData instanceof Boolean){
			return ((Boolean) mData) ? 1 : 0;
		}else if(mData instanceof Number){
			return ((Number) mData).longValue();
		}
		return 0;
	}

	@Override
	public double getDouble(int column) {
		if(mData instanceof Boolean){
			return ((Boolean) mData) ? 1 : 0;
		}else if(mData instanceof Number){
			return ((Number) mData).doubleValue();
		}
		return 0;
	}

	@Override
	public boolean isNull(int column) {
		return mData == null;
	}
}
