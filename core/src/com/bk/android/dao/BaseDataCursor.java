package com.bk.android.dao;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Map.Entry;

import android.content.ContentValues;
import android.database.AbstractCursor;
import android.database.Cursor;
import android.database.CursorWindow;

public abstract class BaseDataCursor extends AbstractCursor{
	@Override
	public final short getShort(int column) {
		return (short) getLong(column);
	}
	
	@Override
	public final int getInt(int column) {
		return (int) getLong(column);
	}

	@Override
	public final float getFloat(int column) {
		return (float) getDouble(column);
	}

	@Override
	public void fillWindow(int position, CursorWindow window) {
		if (position < 0 || position > getCount()) {
            return;
        }
        window.acquireReference();
        try {
            int oldpos = mPos;
            mPos = position - 1;
            window.clear();
            window.setStartPosition(position);
            int columnNum = getColumnCount();
            window.setNumColumns(columnNum);
            while (moveToNext() && window.allocRow()) {            
                for (int i = 0; i < columnNum; i++) {
                	long fieldLong = (long) getLong(i);
                    if(fieldLong == 0 || !window.putLong(fieldLong, mPos, i)){
                    	double fieldDouble = (double) getDouble(i);
	                    if(fieldDouble == 0 || !window.putDouble(fieldDouble, mPos, i)){
	                    	String field = getString(i);
		                    if (field == null || !window.putString(field, mPos, i)) {
		                    	if (!window.putNull(mPos, i)) {
		                            window.freeLastRow();
		                            break;
		                        }
		                    }
	                    }
                    }
                }
            }
            mPos = oldpos;
        } catch (IllegalStateException e){
            // simply ignore it
        } finally {
            window.releaseReference();
        }
	}
	
	public static boolean isConstant(Field field){
		if(field == null){
			return false;
		}
		return Modifier.isFinal(field.getModifiers()) && Modifier.isStatic(field.getModifiers());
	}
	
	public static <T> void getContentValuesToObject(ContentValues values,T obj){
		if(values == null){
			return;
		}
		for (Entry<String, Object> entry : values.valueSet()) {
			String name = entry.getKey();
			try {
				Field field = obj.getClass().getDeclaredField(name);
				if(field != null && !isConstant(field)){
					if(field.getType().isAssignableFrom(int.class)){
						field.setInt(obj, (Integer) entry.getValue());
					}else if(field.getType().isAssignableFrom(boolean.class)){
						field.setBoolean(obj, (Boolean) entry.getValue());
					}else if(field.getType().isAssignableFrom(float.class)){
						field.setFloat(obj, (Float) entry.getValue());
					}else if(field.getType().isAssignableFrom(byte.class)){
						field.setByte(obj,(Byte) entry.getValue());
					}else if(field.getType().isAssignableFrom(double.class)){
						field.setDouble(obj, (Double) entry.getValue());
					}else if(field.getType().isAssignableFrom(long.class)){
						field.setLong(obj, (Long) entry.getValue());
					}else if(field.getType().isAssignableFrom(short.class)){
						field.setShort(obj, (Short) entry.getValue());
					}else if(field.getType().isAssignableFrom(String.class)){
						field.set(obj, (String) entry.getValue());
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public static <T> void getObjToCursor(T obj,Cursor cursor){
		if(obj == null){
			return;
		}
		Field[] fields = obj.getClass().getDeclaredFields();
		if(fields != null){
			for (int i = 0; i < fields.length; i++) {
				Field field = fields[i];
				if(field != null && !isConstant(field)){
					try{
						if(field.getType().isAssignableFrom(int.class)){
							field.setInt(obj, cursor.getInt(cursor.getColumnIndex(field.getName())));
						}else if(field.getType().isAssignableFrom(boolean.class)){
							field.setBoolean(obj, cursor.getInt(cursor.getColumnIndex(field.getName())) == 1);
						}else if(field.getType().isAssignableFrom(float.class)){
							field.setFloat(obj, cursor.getFloat(cursor.getColumnIndex(field.getName())));
						}else if(field.getType().isAssignableFrom(byte.class)){
							field.setByte(obj,(byte) cursor.getShort(cursor.getColumnIndex(field.getName())));
						}else if(field.getType().isAssignableFrom(double.class)){
							field.setDouble(obj, cursor.getDouble(cursor.getColumnIndex(field.getName())));
						}else if(field.getType().isAssignableFrom(long.class)){
							field.setLong(obj, cursor.getLong(cursor.getColumnIndex(field.getName())));
						}else if(field.getType().isAssignableFrom(short.class)){
							field.setShort(obj, cursor.getShort(cursor.getColumnIndex(field.getName())));
						}else if(field.getType().isAssignableFrom(String.class)){
							field.set(obj,cursor.getString(cursor.getColumnIndex(field.getName())));
						}
					}catch (Exception e) {}
				}
			}
		}
	}
	
	public static <T> ContentValues getValuesToObj(T obj){
		ContentValues values = new ContentValues();
		Field[] fields = obj.getClass().getDeclaredFields();
		if(fields != null){
			for (int i = 0; i < fields.length; i++) {
				Field field = fields[i];
				if(field != null && !isConstant(field)){
					try{
						if(field.getType().isAssignableFrom(int.class)){
							values.put(field.getName(),field.getInt(obj));
						}else if(field.getType().isAssignableFrom(boolean.class)){
							values.put(field.getName(),field.getBoolean(obj));
						}else if(field.getType().isAssignableFrom(float.class)){
							values.put(field.getName(),field.getFloat(obj));
						}else if(field.getType().isAssignableFrom(byte.class)){
							values.put(field.getName(),field.getShort(obj));
						}else if(field.getType().isAssignableFrom(double.class)){
							values.put(field.getName(),field.getDouble(obj));
						}else if(field.getType().isAssignableFrom(long.class)){
							values.put(field.getName(),field.getLong(obj));
						}else if(field.getType().isAssignableFrom(short.class)){
							values.put(field.getName(),field.getShort(obj));
						}else if(field.getType().isAssignableFrom(String.class)){
							values.put(field.getName(),(String)field.get(obj));
						}
					}catch (Exception e) {}
				}
			}
		}
		return values;
	}
}
