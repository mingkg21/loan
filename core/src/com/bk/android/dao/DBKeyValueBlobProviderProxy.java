package com.bk.android.dao;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

public class DBKeyValueBlobProviderProxy {
	private static final Object READ_LOCK = new Object();
	
	private ArrayList<WeakReference<DataObserver>> mDataObserver;
	private Context mContext;
	private Uri mContentUri;
	
	public DBKeyValueBlobProviderProxy(Context context,Uri contentUri){
		mContext = context.getApplicationContext();
		mContentUri = contentUri;
		mDataObserver = new ArrayList<WeakReference<DataObserver>>();
	}
	
	private ContentResolver getContentResolver(){
		return mContext.getContentResolver();
	}
	
//	public Map<String, ?> getAll() {
//		return null;
//	}

//	public Set<String> getStringSet(String key, Set<String> defValues) {
//		return null;
//	}
	
	public void addDataObserver(DataObserver dataObserver){
		mDataObserver.add(new WeakReference<DataObserver>(dataObserver));
	}

	private Cursor queryByKey(String key){
		try {
			return getContentResolver().query(mContentUri,null,DBKeyValueBlobProvider._KEY + "=?", new String[]{key}, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private Cursor queryByKey(String key,String type){
		try {
			return getContentResolver().query(mContentUri,null,DBKeyValueBlobProvider._KEY + "=? AND " + DBKeyValueBlobProvider._TYPE + "=?", new String[]{key,type}, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private Cursor queryByType(String type,Integer max){
		String limit = "";
		if(max != null){
			limit = "LIMIT "+(max + 1);
		}
		try {
			return getContentResolver().query(mContentUri,null,DBKeyValueBlobProvider._TYPE + "=?", new String[]{type}, DBKeyValueBlobProvider._UPDATE_TIME + " DESC " + limit);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private Cursor queryLikeByType(String type,String like,Integer max){
		String limit = "";
		if(max != null){
			limit = "LIMIT "+(max + 1);
		}
		try {
			return getContentResolver().query(mContentUri,null,DBKeyValueBlobProvider._TYPE + "=? AND LIKE '%"+like+"%'", new String[]{type}, DBKeyValueBlobProvider._UPDATE_TIME + " DESC " + limit);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public int getCountByType(String type) {
		int count = 0;
		synchronized (READ_LOCK) {
			Cursor cursor = queryByType(type,null);
			if(cursor != null){
				count = cursor.getCount();
				cursor.close();
			}
		}
		return count;
	}
	
	public ArrayList<String> getAllType() {
		ArrayList<String> values = null;
		synchronized (READ_LOCK) {
			Cursor cursor = getContentResolver().query(mContentUri,null,null,null, null);
			if(cursor != null){
				values = new ArrayList<String>();
				for (;cursor.moveToNext();) {
					values.add(cursor.getString(cursor.getColumnIndex(DBKeyValueBlobProvider._TYPE)));
				}
				cursor.close();
			}
			return values;
		}
	}
	
	public ArrayList<String> getAllKeyByType(String type) {
		return getAllKeyByType(type, null);
	}
	
	public ArrayList<String> getAllKeyByType(String type,Integer max) {
		ArrayList<String> values = null;
		synchronized (READ_LOCK) {
			Cursor cursor = queryByType(type,max);
			if(cursor != null){
				values = new ArrayList<String>();
				for (;cursor.moveToNext();) {
					values.add(cursor.getString(cursor.getColumnIndex(DBKeyValueBlobProvider._KEY)));
				}
				cursor.close();
			}
			return values;
		}
	}
	
	public ArrayList<String> getAllKeyLikeByType(String type,String like) {
		return getAllKeyLikeByType(type,like, null);
	}
	
	public ArrayList<String> getAllKeyLikeByType(String type,String like,Integer max) {
		ArrayList<String> values = null;
		synchronized (READ_LOCK) {
			Cursor cursor = queryLikeByType(type,like,max);
			if(cursor != null){
				values = new ArrayList<String>();
				for (;cursor.moveToNext();) {
					values.add(cursor.getString(cursor.getColumnIndex(DBKeyValueBlobProvider._KEY)));
				}
				cursor.close();
			}
			return values;
		}
	}
	
	public ArrayList<byte[]> getAllValueByType(String type) {
		return getAllValueByType(type, null);
	}
	
	public ArrayList<byte[]> getAllValueByType(String type,Integer max) {
		ArrayList<byte[]> values = null;
		synchronized (READ_LOCK) {
			Cursor cursor = queryByType(type,max);
			if(cursor != null){
				values = new ArrayList<byte[]>();
				for (;cursor.moveToNext();) {
					values.add(cursor.getBlob(cursor.getColumnIndex(DBKeyValueBlobProvider._VALUE)));
				}
				cursor.close();
			}
			return values;
		}
	}
	
	public String getString(String key,String type, String defValue) {
		synchronized (READ_LOCK) {
			Cursor cursor = queryByKey(key,type);
			byte[] value = null;
			if(cursor != null){
				if(cursor.moveToFirst()){
					value = cursor.getBlob(cursor.getColumnIndex(DBKeyValueBlobProvider._VALUE));
				}
				cursor.close();
			}
			String data = (String) bytesToObj(value);
			return data != null ? data : defValue;
		}
	}
	
	public int getInt(String key,String type , int defValue) {
		synchronized (READ_LOCK) {
			Cursor cursor = queryByKey(key,type);
			byte[] value = null;
			if(cursor != null){
				if(cursor.moveToFirst()){
					value = cursor.getBlob(cursor.getColumnIndex(DBKeyValueBlobProvider._VALUE));
				}
				cursor.close();
			}
			Integer data = (Integer) bytesToObj(value);
			return data != null ? data : defValue;
		}
	}

	public long getLong(String key,String type , long defValue) {
		synchronized (READ_LOCK) {
			Cursor cursor = queryByKey(key,type);
			byte[] value = null;
			if(cursor != null){
				if(cursor.moveToFirst()){
					value = cursor.getBlob(cursor.getColumnIndex(DBKeyValueBlobProvider._VALUE));
				}
				cursor.close();
			}
			Long data = (Long) bytesToObj(value);
			return data != null ? data : defValue;
		}
	}

	public float getFloat(String key,String type , float defValue) {
		synchronized (READ_LOCK) {
			Cursor cursor = queryByKey(key,type);
			byte[] value = null;
			if(cursor != null){
				if(cursor.moveToFirst()){
					value = cursor.getBlob(cursor.getColumnIndex(DBKeyValueBlobProvider._VALUE));
				}
				cursor.close();
			}
			Float data = (Float) bytesToObj(value);
			return data != null ? data : defValue;
		}
	}

	public boolean getBoolean(String key,String type , boolean defValue) {
		synchronized (READ_LOCK) {
			Cursor cursor = queryByKey(key,type);
			byte[] value = null;
			if(cursor != null){
				if(cursor.moveToFirst()){
					value = cursor.getBlob(cursor.getColumnIndex(DBKeyValueBlobProvider._VALUE));
				}
				cursor.close();
			}
			Boolean data = (Boolean) bytesToObj(value);
			return data != null ? data : defValue;
		}
	}

	public byte[] getBytes(String key,String type) {
		synchronized (READ_LOCK) {
			Cursor cursor = queryByKey(key,type);
			byte[] value = null;
			if(cursor != null){
				if(cursor.moveToFirst()){
					value = cursor.getBlob(cursor.getColumnIndex(DBKeyValueBlobProvider._VALUE));
				}
				cursor.close();
			}
			return value;
		}
	}

	private boolean contains(String key,String type) {
		synchronized (READ_LOCK) {
			Cursor cursor = queryByKey(key);
			boolean isContains = false;
			if(cursor != null){
				isContains = cursor.getCount() > 0;
				cursor.close();
			}
			return isContains;
		}
	}
	
	public boolean hasData(String key,String type) {
		synchronized (READ_LOCK) {
			Cursor cursor = getContentResolver().query(mContentUri,null,
					DBKeyValueBlobProvider._KEY + "=? AND " + DBKeyValueBlobProvider._TYPE + "=? AND " + DBKeyValueBlobProvider._VALUE + " is not null",
					new String[]{key,type}, null);
			boolean isContains = false;
			if(cursor != null){
				isContains = cursor.getCount() > 0;
				cursor.close();
			}
			return isContains;
		}
	}

//	public boolean putStringSet(String key, Set<String> values) {
//		return false;
//	}
	
	private boolean saveValue(String key, byte[] value,String type){
		synchronized (READ_LOCK) {
			ContentValues values = new ContentValues();
			values.put(DBKeyValueBlobProvider._KEY, key);
			values.put(DBKeyValueBlobProvider._VALUE, value);
			values.put(DBKeyValueBlobProvider._TYPE, type);
			boolean result = getContentResolver().insert(mContentUri, values) != null;
			notice(key, type);
			return result;
		}
	}
	
	private boolean updateValue(String key, byte[] value,String type){
		synchronized (READ_LOCK) {
			ContentValues values = new ContentValues();
			values.put(DBKeyValueBlobProvider._KEY, key);
			values.put(DBKeyValueBlobProvider._VALUE, value);
			values.put(DBKeyValueBlobProvider._TYPE, type);
			int rows = getContentResolver().update(mContentUri, values,DBKeyValueBlobProvider._KEY + "=?", new String[]{key});
			notice(key, type);
			return rows > 0;
		}
	}
	
	public boolean putString(String key, String value,String type) {
		if(contains(key,type)){
			return updateValue(key, objToBytes(value) ,type);
		}else{
			return saveValue(key, objToBytes(value) ,type);
		}
	}

	public boolean putInt(String key, int value,String type) {
		if(contains(key,type)){
			return updateValue(key, objToBytes(value) ,type);
		}else{
			return saveValue(key, objToBytes(value) ,type);
		}
	}

	public boolean putLong(String key, long value,String type) {
		if(contains(key,type)){
			return updateValue(key, objToBytes(value),type);
		}else{
			return saveValue(key, objToBytes(value),type);
		}
	}

	public boolean putFloat(String key, float value,String type) {
		if(contains(key,type)){
			return updateValue(key, objToBytes(value),type);
		}else{
			return saveValue(key, objToBytes(value),type);
		}
	}

	public boolean putBoolean(String key, boolean value,String type) {
		if(contains(key,type)){
			return updateValue(key, objToBytes(value),type);
		}else{
			return saveValue(key, objToBytes(value),type);
		}
	}
	
	public boolean putBytes(String key, byte[] value,String type) {
		if(contains(key,type)){
			return updateValue(key, value,type);
		}else{
			return saveValue(key, value,type);
		}
	}

	public byte[] objToBytes(Object obj){
		byte[] data = null;
		try {
			ByteArrayOutputStream bos = new ByteArrayOutputStream(); 
	        ObjectOutputStream os =  new ObjectOutputStream(bos); 
	        os.writeObject(obj);
	        data = bos.toByteArray();
	        os.close();
		} catch (Exception e) {}
		return data;
	}
	
	public Object bytesToObj(byte[] data){
		Object obj = null;
		if(data != null){
			try {
				ObjectInputStream in = new ObjectInputStream (new ByteArrayInputStream(data));
				obj = in.readObject();
				in.close();
			} catch (Exception e) {}
		}
		return obj;
	}
	
	public boolean remove(String key, String type) {
		boolean result = getContentResolver().delete(mContentUri,DBKeyValueBlobProvider._KEY + "=?", new String[]{key,type}) > 0;
		notice(key,null);
		return result;
	}

	public boolean clear() {
		boolean result = getContentResolver().delete(mContentUri,null,null) > 0;
		notice(null,null);
		return result;
	}
	
	public boolean clear(String type) {
		boolean result = getContentResolver().delete(mContentUri,DBKeyValueBlobProvider._TYPE + "=?", new String[]{type}) > 0;
		notice(null,type);
		return result;
	}
	
	private void notice(String key,String type){
		for (WeakReference<DataObserver> dataObserver : mDataObserver) {
			if(dataObserver.get() != null){
				dataObserver.get().onDataChange(key, type);
			}
		}
	}
	
	public interface DataObserver{
		public void onDataChange(String key,String type);
	}
}
