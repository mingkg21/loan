package com.bk.android.dao;

import java.io.File;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;

import com.bk.android.app.BaseApp;
import com.bk.android.util.LogUtil;

public abstract class BaseContentProvider extends ContentProvider {
	protected static final int TYPE_QUERY = 0;
	protected static final int TYPE_INSERT = 1;
	protected static final int TYPE_DELETE = 2;
	protected static final int TYPE_UPDATE = 3;

	private static final String TAG = BaseContentProvider.class.getSimpleName();
	
	private DatabaseHelper mOpenHelper;
	
	protected abstract void checkAuthority(Uri uri,int type);

	protected abstract int getDBVersion();

	protected abstract String getTableName();
	
	protected abstract String getDBName();
	
	protected abstract void createTable(SQLiteDatabase db);
	
	protected abstract void upgrade(final SQLiteDatabase db, int oldV, final int newV);

	protected abstract void downgrade(SQLiteDatabase db, int oldVersion, int newVersion);
	
	@Override
	public boolean onCreate() {
		mOpenHelper = new DatabaseHelper(getContext());
		return true;
	}
	
	private SQLiteDatabase getReadableDatabase(){
		try {
			File file = getContext().getDatabasePath(getDBName());
			if(file != null && !file.exists()){
				mOpenHelper = new DatabaseHelper(getContext());
			}
			SQLiteDatabase db = mOpenHelper.getReadableDatabase();
			return db;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private SQLiteDatabase getWritableDatabase(){
		try {
			File file = getContext().getDatabasePath(getDBName());
			if(file != null && !file.exists()){
				mOpenHelper = new DatabaseHelper(getContext());
			}
			SQLiteDatabase db = mOpenHelper.getWritableDatabase();
			return db;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private void checkSelectionArgs(String[] selectionArgs){
		if(selectionArgs != null){
			for (int i = 0; i < selectionArgs.length; i++) {
				if(selectionArgs[i] == null){
					selectionArgs[i] = "";
				}
			}
		}
	}
	
	@Override
	public Cursor query(Uri uri, String[] projection, String selection,String[] selectionArgs, String sortOrder) {
		checkAuthority(uri, TYPE_QUERY);
		String limit = uri.getQueryParameter("limit");
		SQLiteDatabase db = getReadableDatabase();
		if(db == null){
			LogUtil.i(TAG, "query() Failed to get SQLiteDatabase");
			return null;
		}
		checkSelectionArgs(selectionArgs);
		Cursor cursor = db.query(getTableName(), projection, selection, selectionArgs, null, null, sortOrder,limit);
		if (cursor != null) {
			cursor = new ProcessCursor(cursor);
        	LogUtil.i(TAG, "query() created cursor count :" + cursor.getCount() );
	    }else{
        	LogUtil.i(TAG, "query() failed in created cursor");
	    }
		return cursor;
	}

	@Override
	public String getType(Uri uri) {
		return null;
	}

	@Override
	public int bulkInsert(Uri uri, ContentValues[] values) {
		int numValues = values.length;
        for (int i = 0; i < numValues; i++) {
        	if(i == numValues - 1){
        		insert(uri, values[i],true);
        	}else{
        		insert(uri, values[i],false);
        	}
        }
        return numValues;
	}
	
	private Uri insert(Uri uri, ContentValues values,boolean isNeedNotify) {
		checkAuthority(uri, TYPE_INSERT);
		SQLiteDatabase db = getWritableDatabase();
		if(db == null){
			LogUtil.i(TAG, "insert() Failed to get SQLiteDatabase");
			return Uri.parse(uri + "/" + -1);
		}
		Context context = getContext();
		
		long rowID = db.insert(getTableName(), null, values);
		Uri ret = null;
		if(rowID != -1){
			ret = Uri.parse(uri + "/" + rowID);
			if(isNeedNotify){
				context.getContentResolver().notifyChange(uri, null);
			}
			LogUtil.i(TAG, "insert() rowID:"+rowID);
		}else{
			LogUtil.i(TAG, "insert() couldn't insert into downloads database");
		}
		return ret;
	}
	
	@Override
	public Uri insert(Uri uri, ContentValues values) {
		return insert(uri,values,true);
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		checkAuthority(uri, TYPE_DELETE);
		SQLiteDatabase db = getWritableDatabase();
		if(db == null){
			LogUtil.i(TAG, "delete() Failed to get SQLiteDatabase");
			return 0;
		}
		checkSelectionArgs(selectionArgs);
		Context context = getContext();
		int count = db.delete(getTableName(), selection, selectionArgs);
		LogUtil.i(TAG,"delete() count:"+count);
		if(count > 0){
			context.getContentResolver().notifyChange(uri, null);
		}
		return count;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,String[] selectionArgs) {
		checkAuthority(uri, TYPE_UPDATE);
		SQLiteDatabase db = getWritableDatabase();
		if(db == null){
			LogUtil.i(TAG, "update() Failed to get SQLiteDatabase");
			return 0;
		}
		checkSelectionArgs(selectionArgs);
		Context context = getContext();
		int count = db.update(getTableName(), values, selection, selectionArgs);
		LogUtil.i(TAG, "update() count:" + count);
		if(count > 0){
			context.getContentResolver().notifyChange(uri, null);
			LogUtil.i(TAG,"update() notifyChange Url ：" + uri);
		}
		return count;
	}
	
	private final class DatabaseHelper extends SQLiteOpenHelper {
		public DatabaseHelper(final Context context) {
            super(context, getDBName(), null, getDBVersion());
        }

        @Override
        public void onCreate(final SQLiteDatabase db) {
            createTable(db);
        }

        @Override
        public void onUpgrade(final SQLiteDatabase db, int oldV, final int newV) {
        	try {
            	upgrade(db, oldV, newV);
			} catch (Exception e) {}
        }
        
        @Override
    	public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        	try {
        		if (newVersion == 0) {
        			onCreate(db);
        		} else {
        			if (oldVersion > newVersion) {
        				downgrade(db, oldVersion, newVersion);
        			} else {
        				onUpgrade(db, oldVersion, newVersion);
        			}
        		}
			} catch (Exception e) {}
    	}
    }
	
	protected static String createFieldInteger(String name){
		return createFieldInteger(name, null, false);
	}
	
	protected static String createFieldIntegerPrimary(String name){
		return createFieldInteger(name, null, true);
	}
	
	protected static String createFieldInteger(String name,Integer defaultValue){
		return createFieldInteger(name, defaultValue, false);
	}
	
	protected static String createFieldInteger(String name,Integer defaultValue,boolean isPrimary){
		StringBuffer str = new StringBuffer();
		str.append(name);
		str.append(" INTEGER");
		if(isPrimary){
			str.append(" PRIMARY KEY AUTOINCREMENT");
		}else if(defaultValue != null){
			str.append(" DEFAULT " + defaultValue);
		}
		return str.toString();
	}
	
	protected static String createFieldText(String name){
		return createFieldText(name, null);
	}
	
	protected static String createFieldText(String name,String defaultValue){
		StringBuffer str = new StringBuffer();
		str.append(name);
		str.append(" TEXT");
		if(defaultValue != null){
			str.append(" DEFAULT '" + defaultValue + "'");
		}
		return str.toString();
	}
	
	protected static ContentResolver getContentResolver(){
		return BaseApp.getInstance().getContentResolver();
	}
}
