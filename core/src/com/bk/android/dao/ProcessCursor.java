package com.bk.android.dao;

import android.database.CrossProcessCursor;
import android.database.Cursor;
import android.database.CursorWindow;
import android.database.CursorWrapper;

public class ProcessCursor extends CursorWrapper implements CrossProcessCursor {
	private CrossProcessCursor mCursor;
	
    public ProcessCursor(Cursor cursor) {
        super(cursor);
        mCursor = (CrossProcessCursor) cursor;
    }
    
    @Override
    public void fillWindow(int pos, CursorWindow window) {
        mCursor.fillWindow(pos, window);
    }
    
    @Override
    public CursorWindow getWindow() {
        return mCursor.getWindow();
    }
    
    @Override
    public boolean onMove(int oldPosition, int newPosition) {
        return mCursor.onMove(oldPosition, newPosition);
    }
}

