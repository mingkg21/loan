package com.bk.android.dao;

import java.io.File;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;

import com.bk.android.util.LogUtil;

public abstract class DBKeyValueProvider extends ContentProvider {
	public static final String _KEY = "KEY";
	public static final String _VALUE = "VALUE";
	public static final String _TYPE = "TYPE";
	
	private static final int DB_VERSION = 2;
		
	private static final String TAG = DBKeyValueProvider.class.getSimpleName();
	
	private DatabaseHelper mOpenHelper;
	
	protected abstract String getTableName();
	
	protected abstract String getDBName();

	@Override
	public boolean onCreate() {
		mOpenHelper = new DatabaseHelper(getContext());
		return true;
	}
	
	private SQLiteDatabase getReadableDatabase(){
		try {
			File file = getContext().getDatabasePath(getDBName());
			if(file != null && !file.exists()){
				mOpenHelper = new DatabaseHelper(getContext());
			}
			SQLiteDatabase db = mOpenHelper.getReadableDatabase();
			return db;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private SQLiteDatabase getWritableDatabase(){
		try {
			File file = getContext().getDatabasePath(getDBName());
			if(file != null && !file.exists()){
				mOpenHelper = new DatabaseHelper(getContext());
			}
			SQLiteDatabase db = mOpenHelper.getWritableDatabase();
			return db;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private void checkSelectionArgs(String[] selectionArgs){
		if(selectionArgs != null){
			for (int i = 0; i < selectionArgs.length; i++) {
				if(selectionArgs[i] == null){
					selectionArgs[i] = "";
				}
			}
		}
	}
	
	@Override
	public Cursor query(Uri uri, String[] projection, String selection,String[] selectionArgs, String sortOrder) {
		synchronized (this) {
			SQLiteDatabase db = getReadableDatabase();
			if(db == null){
				LogUtil.i(TAG, "query() Failed to get SQLiteDatabase");
				return null;
			}
			Cursor cursor = null;
			try {
				checkSelectionArgs(selectionArgs);
				cursor = db.query(getTableName(), projection, selection, selectionArgs, null, null, sortOrder);
				if (cursor != null) {
		        	LogUtil.i(TAG, "query() created cursor count :" + cursor.getCount() );
			    }else{
		        	LogUtil.i(TAG, "query() failed in created cursor");
			    }
			} catch (Exception e) {
				e.printStackTrace();
			}
			return cursor;
		}
	}

	@Override
	public String getType(Uri uri) {
		return null;
	}

	@Override
	public int bulkInsert(Uri uri, ContentValues[] values) {
		int numValues = values.length;
        for (int i = 0; i < numValues; i++) {
        	if(i == numValues - 1){
        		insert(uri, values[i],true);
        	}else{
        		insert(uri, values[i],false);
        	}
        }
        return numValues;
	}
	
	private Uri insert(Uri uri, ContentValues values,boolean isNeedNotify) {
		synchronized (this) {
			SQLiteDatabase db = getWritableDatabase();
			if(db == null){
				LogUtil.i(TAG, "insert() Failed to get SQLiteDatabase");
				return Uri.parse(uri + "/" + -1);
			}
			Context context = getContext();
			Uri ret = null;
			try {
				long rowID = db.insert(getTableName(), null, values);
				if(rowID != -1){
					ret = Uri.parse(uri + "/" + rowID);
					if(isNeedNotify){
						context.getContentResolver().notifyChange(uri, null);
					}
					LogUtil.i(TAG, "insert() rowID:"+rowID);
				}else{
					LogUtil.i(TAG, "insert() couldn't insert into downloads database");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return ret;
		}
	}
	
	@Override
	public Uri insert(Uri uri, ContentValues values) {
		return insert(uri,values,true);
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		synchronized (this) {
			SQLiteDatabase db = getWritableDatabase();
			if(db == null){
				LogUtil.i(TAG, "delete() Failed to get SQLiteDatabase");
				return 0;
			}
			Context context = getContext();
			try {
				checkSelectionArgs(selectionArgs);
				int count = db.delete(getTableName(), selection, selectionArgs);
				LogUtil.i(TAG,"delete() count:"+count);
				if(count > 0){
					context.getContentResolver().notifyChange(uri, null);
				}
				return count;
			} catch (Exception e) {
				e.printStackTrace();
			}
			return -1;
		}
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,String[] selectionArgs) {
		synchronized (this) {
			SQLiteDatabase db = getWritableDatabase();
			if(db == null){
				LogUtil.i(TAG, "update() Failed to get SQLiteDatabase");
				return 0;
			}
			Context context = getContext();
			try {
				checkSelectionArgs(selectionArgs);
				int count = db.update(getTableName(), values, selection, selectionArgs);
				LogUtil.i(TAG, "update() count:" + count);
				if(count > 0){
					context.getContentResolver().notifyChange(uri, null);
					LogUtil.i(TAG,"update() notifyChange Url ：" + uri);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return 0;
		}
	}
	
	private final class DatabaseHelper extends SQLiteOpenHelper {
		public DatabaseHelper(final Context context) {
            super(context, getDBName(), null, DB_VERSION);
        }

        @Override
        public void onCreate(final SQLiteDatabase db) {
            createTable(db);
        }
        
        private void createTable(SQLiteDatabase db){
        	 db.execSQL("CREATE TABLE " + getTableName() + "(" +
        			 _KEY + " TEXT," +
        			 _VALUE + " TEXT," +
        			 _TYPE + " TEXT" +
                     ");");
        }

        @Override
        public void onUpgrade(final SQLiteDatabase db, int oldV, final int newV) {
        	if(oldV == 1){
        		String tempTable = getTableName() + "_OLD";
        		db.execSQL("ALTER TABLE " + getTableName() + " RENAME TO " + tempTable + ";");
        		createTable(db);
        		db.execSQL("INSERT INTO " + getTableName() + " SELECT * FROM " + tempTable + ";");
        		db.execSQL("DROP TABLE " + tempTable + ";");
        	}
        }
        
        @Override
    	public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    		if (newVersion == 0) {
    			onCreate(db);
    		} else {
    			if (oldVersion > newVersion) {
    				super.onDowngrade(db, oldVersion, newVersion);
    			} else {
    				onUpgrade(db, oldVersion, newVersion);
    			}
    		}

    	}
    }
}
