package com.bk.android.dao;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

public class DBKeyValueProviderProxy {
	private static final Object READ_LOCK = new Object();
	
	private ArrayList<WeakReference<DataObserver>> mDataObserver;
	private Context mContext;
	private Uri mContentUri;
	
	public DBKeyValueProviderProxy(Context context,Uri contentUri){
		mContext = context.getApplicationContext();
		mContentUri = contentUri;
		mDataObserver = new ArrayList<WeakReference<DataObserver>>();
	}
	
	private ContentResolver getContentResolver(){
		return mContext.getContentResolver();
	}
	
//	public Map<String, ?> getAll() {
//		return null;
//	}

//	public Set<String> getStringSet(String key, Set<String> defValues) {
//		return null;
//	}
	
	public void addDataObserver(DataObserver dataObserver){
		mDataObserver.add(new WeakReference<DataObserver>(dataObserver));
	}

	private Cursor queryByKey(String key,String type){
		try {
			return getContentResolver().query(mContentUri,null,DBKeyValueProvider._KEY + "=? AND " + DBKeyValueProvider._TYPE + "=?", new String[]{key,type}, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private Cursor queryByType(String type){
		try {
			return getContentResolver().query(mContentUri,null,DBKeyValueProvider._TYPE + "=?", new String[]{type}, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private Cursor queryLikeByType(String type,String like,Integer max){
		String limit = "";
		if(max != null){
			limit = "LIMIT "+(max + 1);
		}
		try {
			return getContentResolver().query(mContentUri,null,DBKeyValueBlobProvider._TYPE + "=? AND LIKE '%"+like+"%'", new String[]{type}, DBKeyValueBlobProvider._UPDATE_TIME + " DESC " + limit);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public ArrayList<String[]> getAllKeyValueByType(String type) {
		ArrayList<String[]> values = null;
		synchronized (READ_LOCK) {
			Cursor cursor = queryByType(type);
			if(cursor != null){
				values = new ArrayList<String[]>();
				for (;cursor.moveToNext();) {
					values.add(new String[]{cursor.getString(cursor.getColumnIndex(DBKeyValueProvider._KEY)),
							cursor.getString(cursor.getColumnIndex(DBKeyValueProvider._VALUE))});
				}
				cursor.close();
			}
			return values;
		}
	}
	
	public int getCountByType(String type) {
		int count = 0;
		synchronized (READ_LOCK) {
			Cursor cursor = queryByType(type);
			if(cursor != null){
				count = cursor.getCount();
				cursor.close();
			}
		}
		return count;
	}
	
	public ArrayList<String> getAllKeyByType(String type) {
		ArrayList<String> values = null;
		synchronized (READ_LOCK) {
			Cursor cursor = queryByType(type);
			if(cursor != null){
				values = new ArrayList<String>();
				for (;cursor.moveToNext();) {
					values.add(cursor.getString(cursor.getColumnIndex(DBKeyValueProvider._KEY)));
				}
				cursor.close();
			}
			return values;
		}
	}
	
	public ArrayList<String> getAllValueByType(String type) {
		ArrayList<String> values = null;
		synchronized (READ_LOCK) {
			Cursor cursor = queryByType(type);
			if(cursor != null){
				values = new ArrayList<String>();
				for (;cursor.moveToNext();) {
					values.add(cursor.getString(cursor.getColumnIndex(DBKeyValueProvider._VALUE)));
				}
				cursor.close();
			}
			return values;
		}
	}
	
	public ArrayList<String> getAllKeyLikeByType(String type,String like) {
		return getAllKeyLikeByType(type,like, null);
	}
	
	public ArrayList<String> getAllKeyLikeByType(String type,String like,Integer max) {
		ArrayList<String> values = null;
		synchronized (READ_LOCK) {
			Cursor cursor = queryLikeByType(type,like,max);
			if(cursor != null){
				values = new ArrayList<String>();
				for (;cursor.moveToNext();) {
					values.add(cursor.getString(cursor.getColumnIndex(DBKeyValueBlobProvider._KEY)));
				}
				cursor.close();
			}
			return values;
		}
	}
	
	public String getString(String key,String type, String defValue) {
		synchronized (READ_LOCK) {
			Cursor cursor = queryByKey(key,type);
			String value = null;
			if(cursor != null){
				if(cursor.moveToFirst()){
					value = cursor.getString(cursor.getColumnIndex(DBKeyValueProvider._VALUE));
				}
				cursor.close();
			}
			return value == null ? defValue : value;
		}
	}
	
	public int getInt(String key,String type, int defValue) {
		synchronized (READ_LOCK) {
			Cursor cursor = queryByKey(key,type);
			String value = null;
			if(cursor != null){
				if(cursor.moveToFirst()){
					value = cursor.getString(cursor.getColumnIndex(DBKeyValueProvider._VALUE));
				}
				cursor.close();
			}
			return value == null ? defValue : Integer.valueOf(value);
		}
	}

	public long getLong(String key,String type, long defValue) {
		synchronized (READ_LOCK) {
			Cursor cursor = queryByKey(key,type);
			String value = null;
			if(cursor != null){
				if(cursor.moveToFirst()){
					value = cursor.getString(cursor.getColumnIndex(DBKeyValueProvider._VALUE));
				}
				cursor.close();
			}
			return value == null ? defValue : Long.valueOf(value);
		}
	}

	public float getFloat(String key,String type, float defValue) {
		synchronized (READ_LOCK) {
			Cursor cursor = queryByKey(key,type);
			String value = null;
			if(cursor != null){
				if(cursor.moveToFirst()){
					value = cursor.getString(cursor.getColumnIndex(DBKeyValueProvider._VALUE));
				}
				cursor.close();
			}
			return value == null ? defValue : Float.valueOf(value);
		}
	}

	public boolean getBoolean(String key,String type, boolean defValue) {
		synchronized (READ_LOCK) {
			Cursor cursor = queryByKey(key,type);
			String value = null;
			if(cursor != null){
				if(cursor.moveToFirst()){
					value = cursor.getString(cursor.getColumnIndex(DBKeyValueProvider._VALUE));
				}
				cursor.close();
			}
			return value == null ? defValue : Boolean.valueOf(value);
		}
	}

	public boolean contains(String key,String type) {
		synchronized (READ_LOCK) {
			Cursor cursor = queryByKey(key,type);
			boolean isContains = false;
			if(cursor != null){
				isContains = cursor.getCount() > 0;
				cursor.close();
			}
			return isContains;
		}
	}
	
	public boolean hasData(String key,String type) {
		synchronized (READ_LOCK) {
			Cursor cursor = getContentResolver().query(mContentUri,null,
					DBKeyValueProvider._KEY + "=? AND " + DBKeyValueProvider._TYPE + "=? AND " + DBKeyValueProvider._VALUE + " is not null",
					new String[]{key,type}, null);
			boolean isContains = false;
			if(cursor != null){
				isContains = cursor.getCount() > 0;
				cursor.close();
			}
			return isContains;
		}
	}

//	public boolean putStringSet(String key, Set<String> values) {
//		return false;
//	}
	
	private boolean saveValue(String key, String value,String type){
		synchronized (READ_LOCK) {
			try {
				ContentValues values = new ContentValues();
				values.put(DBKeyValueProvider._KEY, key);
				values.put(DBKeyValueProvider._VALUE, value);
				values.put(DBKeyValueProvider._TYPE, type);
				boolean result = getContentResolver().insert(mContentUri, values) != null;
				notice(key, type);
				return result;
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}
		}
	}
	
	private boolean updateValue(String key, String value,String type){
		synchronized (READ_LOCK) {
			try {
				ContentValues values = new ContentValues();
				values.put(DBKeyValueProvider._KEY, key);
				values.put(DBKeyValueProvider._VALUE, value);
				values.put(DBKeyValueProvider._TYPE, type);
				int rows = getContentResolver().update(mContentUri, values,DBKeyValueProvider._KEY + "=? AND " + DBKeyValueProvider._TYPE + "=?", new String[]{key,type});
				notice(key, type);
				return rows > 0;
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}
		}
	}
	
	public boolean putString(String key, String value,String type) {
		if(contains(key,type)){
			return updateValue(key, value,type);
		}else{
			return saveValue(key, value,type);
		}
	}

	public boolean putInt(String key, int value,String type) {
		if(contains(key,type)){
			return updateValue(key, String.valueOf(value),type);
		}else{
			return saveValue(key, String.valueOf(value),type);
		}
	}

	public boolean putLong(String key, long value,String type) {
		if(contains(key,type)){
			return updateValue(key, String.valueOf(value),type);
		}else{
			return saveValue(key, String.valueOf(value),type);
		}
	}

	public boolean putFloat(String key, float value,String type) {
		if(contains(key,type)){
			return updateValue(key, String.valueOf(value),type);
		}else{
			return saveValue(key, String.valueOf(value),type);
		}
	}

	public boolean putBoolean(String key, boolean value,String type) {
		if(contains(key,type)){
			return updateValue(key, String.valueOf(value),type);
		}else{
			return saveValue(key, String.valueOf(value),type);
		}
	}

	public boolean remove(String key,String type) {
		boolean result = getContentResolver().delete(mContentUri,DBKeyValueProvider._KEY + "=? AND " + DBKeyValueProvider._TYPE + "=?", new String[]{key,type}) > 0;
		notice(key,null);
		return result;
	}

	public boolean clear() {
		boolean result = getContentResolver().delete(mContentUri,null,null) > 0;
		notice(null,null);
		return result;
	}
	
	public boolean clear(String type) {
		boolean result = getContentResolver().delete(mContentUri,DBKeyValueProvider._TYPE + "=?", new String[]{type}) > 0;
		notice(null,type);
		return result;
	}
	
	private void notice(String key,String type){
		for (WeakReference<DataObserver> dataObserver : mDataObserver) {
			if(dataObserver.get() != null){
				dataObserver.get().onDataChange(key, type);
			}
		}
	}
	
	public interface DataObserver{
		public void onDataChange(String key,String type);
	}
}
