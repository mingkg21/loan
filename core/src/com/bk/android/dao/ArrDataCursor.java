package com.bk.android.dao;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class ArrDataCursor<Data> extends BaseDataCursor{
	private List<Data> mDatas;
	private String[] mColumnNames;
	public ArrDataCursor(List<Data> datas,Class<Data> clazz){
		mDatas = datas;
		Field[] fields = clazz.getDeclaredFields();
		ArrayList<String> strArr = new ArrayList<String>();
		if(fields != null){
			for (int i = 0; i < fields.length; i++) {
				Field field = fields[i];
				if(!isConstant(field)){
					strArr.add(field.getName());
				}
			}
		}
		mColumnNames = new String[strArr.size()];
		strArr.toArray(mColumnNames);
	}
	
	@Override
	public int getCount() {
		return mDatas.size();
	}

	@Override
	public String[] getColumnNames() {
		return mColumnNames;
	}

	@Override
	public String getString(int column) {
		Object obj = getDataObject();
		try {
			Field field = obj.getClass().getDeclaredField(mColumnNames[column]);
			if(field != null){
				Object valueObj = field.get(obj);
				if(valueObj instanceof String){
					return (String) field.get(obj);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}


	@Override
	public long getLong(int column) {
		Object obj = getDataObject();
		try {
			Field field = obj.getClass().getDeclaredField(mColumnNames[column]);
			if(field != null){
				Object valueObj = field.get(obj);
				if(valueObj instanceof Boolean){
					return ((Boolean)valueObj) ? 1 : 0;
				}else if(valueObj instanceof Number){
					return ((Number) field.get(obj)).longValue();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public double getDouble(int column) {
		Object obj = getDataObject();
		try {
			Field field = obj.getClass().getDeclaredField(mColumnNames[column]);
			if(field != null){
				Object valueObj = field.get(obj);
				if(valueObj instanceof Boolean){
					return ((Boolean)valueObj) ? 1 : 0;
				}else if(valueObj instanceof Number){
					return ((Number) field.get(obj)).doubleValue();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public boolean isNull(int column) {
		try {
			Field field = getDataObject().getClass().getDeclaredField(mColumnNames[column]);
			return field == null || field.get(getDataObject()) == null;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}
	
	private Data getDataObject(){
		return mDatas.get(mPos);
	}
}
