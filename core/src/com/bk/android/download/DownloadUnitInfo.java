package com.bk.android.download;


/**
 * 数据库表实例化对象
 * @author linyiwei
 * @email 21551594@qq.com
 * @date 2011-11-1
 */
class DownloadUnitInfo {
	long mID;
	/**
	 * 本地文件存放路径
	 */
	String mFilePath;
	/**
	 * 文件网络地址
	 */
	String mDownloadUrl;
	/**
	 * 文件下载或上传状态
	 */
	int mState;
	/**
	 * 文件总大小
	 */
	long mFileByteSize;
	/**
	 * 文件当前大小
	 */
	long mFileByteCurrentSize;
	/**
	 * 文件名
	 */
	String mFileName;
	/**
	 * 文件动作类型，（如上传或下载  预留）
	 */
	int mActionType;
	boolean isDelete = false;
	DownloadUnitInfo(long mID, String mFilePath, String mDownloadUrl,
			int mState, long mFileByteSize, long mFileByteCurrentSize,
			String mFileName, int actionType,int isDelete) {
		super();
		this.mID = mID;
		this.mFilePath = mFilePath;
		this.mDownloadUrl = mDownloadUrl;
		this.mState = mState;
		this.mFileByteSize = mFileByteSize;
		this.mFileByteCurrentSize = mFileByteCurrentSize;
		this.mFileName = mFileName;
		this.mActionType = actionType;
		if(isDelete == 0)this.isDelete = false;
		if(isDelete == 1)this.isDelete = true;
	}
	void update(DownloadUnitInfo newData){
		this.mID = newData.mID;
		this.mFilePath = newData.mFilePath;
		this.mDownloadUrl = newData.mDownloadUrl;
		this.mState = newData.mState;
		this.mFileByteSize = newData.mFileByteSize;
		this.mFileByteCurrentSize = newData.mFileByteCurrentSize;
		this.mFileName = newData.mFileName;
		this.mActionType = newData.mActionType;
		this.isDelete = newData.isDelete;
	}
	@Override
	public boolean equals(Object o) {
		if(o instanceof Long){
			if((Long)o == this.mID){
				return true;
			}
		}
		if(o instanceof DownloadUnitInfo){
			DownloadUnitInfo downloadUnit = (DownloadUnitInfo) o;
			if(downloadUnit.mID == this.mID){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
}
