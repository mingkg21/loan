package com.bk.android.download;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

/**
 * DownloadUnit表字段名定义
 * 
 *	对外提供的ContentProvider操作参数及数据库字段
 * @author linyiwei
 * @email 21551594@qq.com
 * @date 2011-11-1
 *
 */
public class DownloadAPI{
	static String CONTENT_URI_AUTHORITIES = "";
	/**
	 * ContentProvider访问Uri
	 */
	public static Uri CONTENT_URI;
	
	/**
	 * ContentProvider访问Uri
	 */
	public static Uri CONTENT_URI_NOT_NOTICE;
	
	
	static Uri CONTENT_URI_ROOT;
	
	/**
	 * 必须在Application的onCreate里面执行
	 * @param context
	 */
	public static void init(Context context){
		try {
			ClassLoader classLoader = context.getClassLoader();
			PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(),PackageManager.GET_PROVIDERS);
			for (ProviderInfo providerInfo : packageInfo.providers) {
				Class<?> clazz = classLoader.loadClass(providerInfo.name);
				if(DownloadProvider.class.isAssignableFrom(clazz)){
					CONTENT_URI_AUTHORITIES = providerInfo.authority;
					CONTENT_URI = Uri.parse("content://"
							+ CONTENT_URI_AUTHORITIES + "/" + Constants.CONTENT_URI_PATH);
					CONTENT_URI_NOT_NOTICE = Uri.parse("content://"
							+ CONTENT_URI_AUTHORITIES + "/" + Constants.CONTENT_URI_PATH_ROOT);
					CONTENT_URI_ROOT = Uri.parse("content://"
							+ CONTENT_URI_AUTHORITIES + "/" + Constants.CONTENT_URI_PATH_ROOT);
					DownloadProvider.init();
					String packageName = context.getPackageName();
					ACTION_ON_DOWNLOAD_PROGRESS_CHANGE = packageName + ".action.downloadProgressChange";
					ACTION_ON_DOWNLOAD_STATE_CHANGE = packageName + ".action.downloadStateChange";
					ACTION_ON_DOWNLOAD_DELETE = packageName + ".action.downloadDelete";
					ACTION_START_DOWNLOAD = packageName + ".action.startDownload";
					ACTION_STOP_DOWNLOAD = packageName + ".action.stopDownload";
					return;
				}
			}
		} catch (Exception e) {}
	}
	
	//--------------------字段名----------------------
	public static final String _ID = "id";
	/**
	 * 本地文件存放路径 包括文件名
	 * 此字段不支持update操作
	 */
	public static final String FILE_PATH = "file_path";
	/**
	 * 文件网络地址
	 * 此字段不支持update操作
	 */
	public static final String DOWNLOAD_URL = "download_url";
	/**
	 * 文件下载或上传状态
	 */
	public static final String STATE = "state";
	/**
	 * 文件名
	 */
	public static final String FILE_NAME = "file_name";
	/**
	 * 文件动作类型，（如上传或下载  预留）
	 */
	public static final String ACTION_TYPE = "action_type";
	
	/**
	 * 文件总大小
	 */
	public static final String FILE_BYTE_SIZE = "file_byte_size";
	/**
	 * 文件当前大小
	 */
	public static final String FILE_BYTE_CURRENT_SIZE = "file_byte_current_size";
	/**
	 * 时间戳
	 */
	public static final String TIMES_TAMP  = "times_tamp";
	/**
	 * 删除标识,被标识为删除后对外隐藏
	 */
	static final String DELETE = "_delete";
	/**
	 *  预留字段0
	 */
	public static final String DATA0 = "data0";
	/**
	 *  预留字段1
	 */
	public static final String DATA1 = "data1";
	
	/**
	 *  预留字段2
	 */
	public static final String DATA2 = "data2";
	
	/**
	 *  预留字段3
	 */
	public static final String DATA3 = "data3";
	
	/**
	 *  预留字段4
	 */
	public static final String DATA4 = "data4";
	
	/**
	 *  预留字段5
	 */
	public static final String DATA5 = "data5";
	/**
	 *  预留字段6
	 */
	public static final String DATA6 = "data6";
	/**
	 *  预留字段7
	 */
	public static final String DATA7 = "data7";
	/**
	 *  预留字段8
	 */
	public static final String DATA8 = "data8";
	/**
	 *  预留字段9
	 */
	public static final String DATA9 = "data9";
	/**
	 *  预留字段10
	 */
	public static final String DATA10 = "data10";
	/**
	 *  预留字段11
	 */
	public static final String DATA11 = "data11";
	//----------------下载状态参数-------------
	/**
	 * 文件下载或上传状态值   准备 开始
	 */
	public static final int STATE_START = 0;
	/**
	 * 文件下载或上传状态值  开始下载
	 */
	public static final int STATE_STARTING = 1;
	/**
	 * 文件下载或上传状态值  暂停
	 */
	public static final int STATE_PAUSE = 2;
	/**
	 * 文件下载或上传状态值  完成
	 */
	public static final int STATE_FINISH = 3;
	/**
	 * 文件下载或上传状态值  错误
	 */
	public static final int STATE_FAIL= 4;
	/**
	 * 内存空间不足
	 */
	public static final int STATE_FAIL_OUT_MEMORY= 5;
	
	//---------------------Service 发出的广播 Action定义
	/**
	 * 当下载进度变化时
	 * 包含数据：
	 * 	BROAD_CAST_DATA_KEY_ID
	 * 	BROAD_CAST_DATA_KEY_FILE_BYTE_SIZE
	 * 	BROAD_CAST_DATA_KEY_FILE_BYTE_CURRENT_SIZE
	 */
	public static String ACTION_ON_DOWNLOAD_PROGRESS_CHANGE = ".action.downloadProgressChange";
	/**
	 * 当下载状态变化时
	 * 包含数据：
	 *	BROAD_CAST_DATA_KEY_ID
	 * 	BROAD_CAST_DATA_KEY_STATE
	 */
	public static String ACTION_ON_DOWNLOAD_STATE_CHANGE = ".action.downloadStateChange";
	
	public static String ACTION_ON_DOWNLOAD_DELETE = ".action.downloadDelete";
	//------------------------Service 控制  Action定义---------------
	public static String ACTION_START_DOWNLOAD = ".action.startDownload";
	
	public static String ACTION_STOP_DOWNLOAD = ".action.stopDownload";
	//-------------------------广播数据定义
	public static final String BROAD_CAST_DATA_KEY_ID = "id";
	
	public static final String BROAD_CAST_DATA_KEY_IDS = "ids";
	
	public static final String BROAD_CAST_DATA_KEY_FILE_BYTE_SIZE = "file_byte_size";
	
	public static final String BROAD_CAST_DATA_KEY_FILE_BYTE_CURRENT_SIZE = "file_byte_current_size";
	
	public static final String BROAD_CAST_DATA_KEY_STATE = "file_byte_current_size";
	
	public static class Setting {
		/** 最大任务数 */
		public static int sMaxThreadSize = 3;
		/** 允许处于等待状态的线程最大值 */
		public static int sMaxWaitThreadSize = 1;
		/** 服务器连接失败重连次数 */
		public static int sReconnectSize = 3;
		/** 网络实现类 */
		public static Class<?> sHttpHandler;
		public static ToastNoticeRunnable sToastNoticeRunnable;
		public static DBUpdateRunnable sDBUpdateRunnable;
		public static IDownloadNotification sDownloadNotification;
	}
	
	public static interface ToastNoticeRunnable{
		public void run(String msg);
	}
	
	public static interface DBUpdateRunnable{
		public void onCreate(final SQLiteDatabase db,String table);
		public void onUpgrade(final SQLiteDatabase db,String table);
	}
}
