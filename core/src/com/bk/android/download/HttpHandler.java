package com.bk.android.download;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import com.bk.android.util.FileUtil;

/**
 * @author linyiwei
 * @email 21551594@qq.com
 * @date 2011-11-1
 */
public abstract class HttpHandler{
	/**
	 * 缓冲大小
	 */
	private int BUFFER_SIZE = 10240;
	/**
	 * 是否停止
	 */
	private boolean isStop = false;
	/**
	 * 下载监听接口
	 */
	private OnDownloadListener mOnDownloadListener;
	/**
	 * 得到下载文件的输入流，需要模块外实现
	 * @param url下载地址
	 * @param currentByteSize当前
	 * @return
	 */
	public abstract DownloadInputStream getDownloadFileStream(String url, long currentByteSize,long byteSize,long id);
	
	public final static class DownloadInputStream {
		private InputStream mStream;
		private long mTotalBytes;
		private boolean isStrict = false;
		private IHttpController mHttpController;

		public DownloadInputStream(InputStream stream, long totalBytes,boolean isStrict,
				IHttpController httpController) {
			this.mStream = stream;
			this.mTotalBytes = totalBytes;
			this.mHttpController = httpController;
			this.isStrict = isStrict;
		}
		/**
		 * 中断网络
		 */
		public void shutdown() {
			if (mHttpController != null) {
				//有些机型，shutdown后会有网络动作，造成NetworkRunOnMainThread错误
				new Thread() {
					@Override
					public void run() {
						try {
							mHttpController.shutdown();
						} catch (Exception e) {}
					}
				}.start();
			}
		}
	}
	
	public interface IHttpController {
		public void shutdown();
	}

	void setIsStop(boolean isStop){
		this.isStop = isStop;
	}
	
	void setOnDownloadListener(OnDownloadListener downloadListener){
		mOnDownloadListener = downloadListener;
	}
	
	int startDownload(String url, String path, long currentByteSize,long byteSize,long id){
		if(mOnDownloadListener != null){
			mOnDownloadListener.onDownloading(id,currentByteSize,byteSize);
		}
		if(!FileUtil.checkPathDir(path)){
			return Constants.DOWNLOAD_RES_CODE_FAILURE_FILE;
		}
		if(FileUtil.getStorageSize() < BUFFER_SIZE){
			return Constants.DOWNLOAD_RES_CODE_FAILURE_FILE_OUT_MEMORY;
		}
		File file = new File(path);
		if(!file.exists()){
			file = new File((String) path.subSequence(0, path.lastIndexOf(".")) + ".tmp");
		}else{
			file.renameTo(new File((String) path.subSequence(0, path.lastIndexOf(".")) + ".tmp"));
			file = new File((String) path.subSequence(0, path.lastIndexOf(".")) + ".tmp");
		}
		boolean append = true;
		FileOutputStream outputStream = null;
		DownloadInputStream mDownloadInputStream = null;
		try{
			try{
				if(file.isFile() && file.exists() && byteSize != 0){
					currentByteSize = file.length();
				}else{
					currentByteSize = 0;
					append = false;
				}
				outputStream = new FileOutputStream(file,append);
			}catch (Exception e) {
				e.printStackTrace();
				if(mOnDownloadListener != null){
					mOnDownloadListener.onDownloadErr(id,null);
				}
				return Constants.DOWNLOAD_RES_CODE_FAILURE_FILE;
			}
			if(mOnDownloadListener != null && byteSize == file.length() && byteSize != 0){
				file.renameTo(new File(path));
				mOnDownloadListener.onDownloadProgressChange(id, byteSize ,byteSize);
				mOnDownloadListener.onDownloadFinish(id);
				return Constants.DOWNLOAD_RES_CODE_SUCCESS;
			}
			mDownloadInputStream = getDownloadFileStream( url,currentByteSize,byteSize,id);
			if(mDownloadInputStream == null){
				if(mOnDownloadListener != null){
					mOnDownloadListener.onDownloadErr(id,null);
				}
				return Constants.DOWNLOAD_RES_CODE_FAILURE_NETWORK;
			}
			long size = mDownloadInputStream.mTotalBytes;
			
			if(mOnDownloadListener != null && size == file.length() && size != 0 
					|| ( !mDownloadInputStream.isStrict && size <= file.length() && size != 0)){
				file.renameTo(new File(path));
				mOnDownloadListener.onDownloadProgressChange(id, size ,size);
				mOnDownloadListener.onDownloadFinish(id);
				return Constants.DOWNLOAD_RES_CODE_SUCCESS;
			}
			
			if(mDownloadInputStream.mStream == null || mDownloadInputStream.mTotalBytes == 0){
				if(mOnDownloadListener != null){
					mOnDownloadListener.onDownloadErr(id,null);
				}
				return Constants.DOWNLOAD_RES_CODE_FAILURE_NETWORK;
			}
			
			if(FileUtil.getStorageSize() < size){
				return Constants.DOWNLOAD_RES_CODE_FAILURE_FILE_OUT_MEMORY;
			}
			
			if(mOnDownloadListener != null){
				mOnDownloadListener.onDownloading(id,file.length(),size);
			}
//			bis = new BufferedInputStream(mInputStream);
			
			long count = 0;
			byte[] buffer = new byte[BUFFER_SIZE];
		    int n = 0;
		    while (-1 != (n = mDownloadInputStream.mStream.read(buffer, 0, BUFFER_SIZE)) && !isStop) {
		    	count += n;
		    	outputStream.write(buffer, 0, n);
				if(mOnDownloadListener != null){
					if(!mOnDownloadListener.onDownloadProgressChange(id, count + currentByteSize ,size)){
						break;
					}
				}
				Thread.sleep(20L);
		    }
		    if(-1 == n){
		    	 if( count + currentByteSize == size || !mDownloadInputStream.isStrict){
		    		 	file.renameTo(new File(path));
		    		 	if(mOnDownloadListener != null){
		    				mOnDownloadListener.onDownloadFinish(id);
		    			}
				    	return Constants.DOWNLOAD_RES_CODE_SUCCESS;
				    }else{
				    	file.delete();
				    	if(mOnDownloadListener != null){
							mOnDownloadListener.onDownloadErr(id,null);
						}
				    	return Constants.DOWNLOAD_RES_CODE_FAILURE_FILE;
				    }
		    }
		    return Constants.DOWNLOAD_RES_CODE_SUCCESS;
		}catch(IOException e){
			e.printStackTrace();
			if(mOnDownloadListener != null){
				mOnDownloadListener.onDownloadErr(id,e);
			}
			if(FileUtil.getStorageSize() < BUFFER_SIZE){
				return Constants.DOWNLOAD_RES_CODE_FAILURE_FILE_OUT_MEMORY;
			}
			return Constants.DOWNLOAD_RES_CODE_FAILURE_NETWORK;
		}catch(Exception e){
			e.printStackTrace();
			if(mOnDownloadListener != null){
				mOnDownloadListener.onDownloadErr(id,e);
			}
			if(FileUtil.getStorageSize() < BUFFER_SIZE){
				return Constants.DOWNLOAD_RES_CODE_FAILURE_FILE_OUT_MEMORY;
			}
			return Constants.DOWNLOAD_RES_CODE_FAILURE_FILE;
		}finally{
			try {
				if(mDownloadInputStream != null){
					mDownloadInputStream.shutdown();
					if(mDownloadInputStream.mStream != null){
						mDownloadInputStream.mStream.close();
					}
				}
				if(outputStream != null){
					outputStream.close();
				}
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}
	
	static HttpHandler getInstance(){
		if(DownloadAPI.Setting.sHttpHandler == null){
			return null;
		}
		try {
			return (HttpHandler) DownloadAPI.Setting.sHttpHandler.newInstance();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		}
		return null;
	};
	
	interface OnDownloadListener{
		boolean onDownloadProgressChange(long id,long currentSize,long size);
		void onDownloadErr(long id,Exception e);
		void onDownloading(long id,long currentSize,long size);
		void onDownloadFinish(long id);
	}
}
