package com.bk.android.download;

/**
* @author linyiwei
* @email 21551594@qq.com
* @date 2011-11-1
*/
class Constants {
	static final String CONTENT_URI_PATH = "download";
	static final String CONTENT_URI_PATH_ROOT = "downloadRoot";
	
	static final int DOWNLOAD_RES_CODE_SUCCESS = 1;
	static final int DOWNLOAD_RES_CODE_FAILURE_FILE = 2;
	static final int DOWNLOAD_RES_CODE_FAILURE_FILE_OUT_MEMORY = 3;
	static final int DOWNLOAD_RES_CODE_FAILURE_NETWORK = 4;
	/**
	 * 根据时间戳获取下载单元信息
	 */
	static final int WHAT_LOAD_DOWNLOAD_UNITS = 1;
	/**
	 *  删除下载单元
	 */
	static final int WHAT_DELETE_DOWNLOAD_UNITS = 2;
	
	/**
	 *  下载进度改变时
	 */
	static final int WHAT_ON_DOWNLOAD_PROGRESS_CHANGE = 3;
	
	/**
	 *  保存下载单元信息
	 */
	static final int WHAT_SAVE_DOWNLOAD = 4;
}
