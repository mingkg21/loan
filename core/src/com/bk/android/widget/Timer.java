package com.bk.android.widget;

import android.os.Handler;
import android.os.Looper;

public class Timer implements Runnable{
	private Handler mHandler = new Handler(Looper.getMainLooper());
	private long mStartTime;
	private TimerListener mTimerListener;
	private long mDelayed = 50;
	private boolean isInitDelayed;
	private boolean isInit;

	public Timer(boolean initDelayed, long delayed){
		mDelayed = delayed;
		isInitDelayed = initDelayed;
	}
	
	public void start(){
		if(mTimerListener != null){
			mTimerListener.onStart();
		}
		mStartTime = System.currentTimeMillis();
		run();
	}
	
	public long stop(){
		long time = System.currentTimeMillis() - mStartTime;
		mStartTime = -1;
		mHandler.removeCallbacks(this);
		if(mTimerListener != null){
			mTimerListener.onStop();
		}
		return time;
	}
	
	public boolean isStart(){
		return mStartTime > 0;
	}
	
	@Override
	public void run() {
		if(isStart()){
			long time = System.currentTimeMillis() - mStartTime;
			if(isInit || !isInitDelayed){
				if(mTimerListener != null){
					mTimerListener.onTiming(time);
				}
			}
			isInit = true;
			mHandler.postDelayed(this, mDelayed);
		}
	}
	
	public void setTimerListener(TimerListener l){
		mTimerListener = l;
	}
	
	public interface TimerListener{
		public void onStart();
		public void onStop();
		public void onTiming(long time);
	}
}
