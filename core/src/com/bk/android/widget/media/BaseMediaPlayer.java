package com.bk.android.widget.media;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;

import android.content.Context;
import android.media.AudioManager;
import android.media.AudioManager.OnAudioFocusChangeListener;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.Handler;
import android.os.Looper;

import com.bk.android.app.BaseApp;

public abstract class BaseMediaPlayer extends MediaPlayer implements OnErrorListener,OnCompletionListener,OnPreparedListener,Runnable{
	public static final byte STATE_START = 0;
	public static final byte STATE_STOP = 1;
	public static final byte STATE_ERROR = 2;
	public static final byte STATE_COMPLETION = 3;
	public static final byte STATE_PAUSE = 4;
	public static final byte STATE_PREPAREING = 5;
	private static final int DELAY_MILLIS = 100;
	
	protected int mState;
	
	private ArrayList<WeakReference<PlayerListener>> mPlayerListeners;
	private Timer mTimer = new Timer();
	private TimerTask mTimerTask;
	private int mStartTime = 0;
	private int mPauseTime = -1;
	private int mSeekToPosition = 0;
	private OnCompletionListener mOnCompletionListener;
	private OnErrorListener mOnErrorListener;
	private boolean isTimerTaskStart = false;
	private long lastSystemTime = 0;
	private Handler mHandler;
	private OnPreparedListener mOnPreparedListener;
	private boolean isTemporaryPause;
	private OnAudioFocusChangeListener mOnAudioFocusChangeListener;
	private AudioManager mAudioManager;

	public BaseMediaPlayer(){
		super();
		super.setOnErrorListener(this);
		super.setOnCompletionListener(this);
		super.setOnPreparedListener(this);
		mHandler = new Handler(Looper.getMainLooper());
		mPlayerListeners = new ArrayList<WeakReference<PlayerListener>>();
		mAudioManager = (AudioManager) BaseApp.getInstance().getSystemService(Context.AUDIO_SERVICE);
		mState = STATE_STOP;
	}
	
	@Override
	public void release() {
		super.release();
		if(mTimer != null){
			mTimer.cancel();
			mTimer.purge();
			mTimer = null;
		}
	}
	
	@Override
	public int getCurrentPosition() {
		try {
			if(Math.abs(super.getCurrentPosition() - mStartTime) < 1000){
				return super.getCurrentPosition();
			}
		} catch (Exception e) {}
		return mStartTime;
	}
	
	@Override
	public int getDuration() {
		try {
			return super.getDuration();
		} catch (Exception e) {}
		return 0;
	}

	@Override
	public void start() throws IllegalStateException {
		try {
			super.start();
			startTimer();
			performOnPlayStateChange(STATE_START , getCurrentPlaySrc());
		} catch (Exception e) {}
	}

	@Override
	public void stop() throws IllegalStateException {
		try {
			super.stop();
		} catch (Exception e) {}
		stopTimer();
		onStopPlay(STATE_STOP);
	}

	
	@Override
	public void seekTo(int msec) throws IllegalStateException {
		try {
			super.seekTo(msec);
			stopTimer();
			mSeekToPosition = msec;
		} catch (Exception e) {}
	}

	@Override
	public void pause() throws IllegalStateException {
		try {
			super.pause();
			mPauseTime = mStartTime;
			pauseTimer();
			if(lastSystemTime != 0){
				long timeDifference = System.currentTimeMillis() - lastSystemTime;
				if(timeDifference > DELAY_MILLIS){
					timeDifference = DELAY_MILLIS;
				}
				if(timeDifference < 0){
					timeDifference = 0;
				}
				mPauseTime += timeDifference;
			}
			onStopPlay(STATE_PAUSE);
		} catch (Exception e) {}
	}
	
	public int getState() {
		return mState;
	}

	@Override
	public void setOnErrorListener(OnErrorListener listener) {
		mOnErrorListener = listener;
	}

	@Override
	public void setOnCompletionListener(OnCompletionListener listener) {
		mOnCompletionListener = listener;
	}
	
	@Override
	public void setOnPreparedListener(OnPreparedListener listener){
	    mOnPreparedListener = listener;
	}
	 
	@Override
	public void onPrepared(MediaPlayer mp) {
		if(mOnPreparedListener != null){
			mOnPreparedListener.onPrepared(mp);
		}
	}

	@Override
	public boolean onError(MediaPlayer mp, int what, int extra) {
		boolean isHandle = false;
		stopTimer();
		onStopPlay(STATE_ERROR);
		if(mOnErrorListener != null){
			isHandle = mOnErrorListener.onError(mp, what, extra);
		}
		return isHandle;
	}
	
	@Override
	public void onCompletion(MediaPlayer mp) {
		stopTimer();
		onStopPlay(STATE_COMPLETION);
		if(mOnCompletionListener != null){
			mOnCompletionListener.onCompletion(mp);
		}
	}

	private void startTimer(){
		if(mPauseTime >= 0){
			mStartTime = mPauseTime;
		}else{
			mStartTime = mSeekToPosition;
		}
		mPauseTime = -1;
		if(mTimerTask != null){
			mTimerTask.cancel();
		}
		isTimerTaskStart = true;
		lastSystemTime = System.currentTimeMillis();
		mTimerTask = new TimerTask() {
			@Override
			public void run() {
				if(isTimerTaskStart && this.equals(mTimerTask)){
					lastSystemTime = 0;
					mStartTime += DELAY_MILLIS;
					mHandler.post(BaseMediaPlayer.this);
				}
			}
		};
		if(mTimer != null){
			mTimer.schedule(mTimerTask, DELAY_MILLIS,DELAY_MILLIS);
		}
	}
	
	private void pauseTimer(){
		isTimerTaskStart = false;
		if(mTimerTask != null){
			mTimerTask.cancel();
			mTimerTask = null;
		}
	}
	
	private void stopTimer(){
		pauseTimer();
		mSeekToPosition = 0;
		mPauseTime = -1;
		lastSystemTime = 0;
	}
	
	protected void onStopPlay(int state){
		performOnPlayStateChange(state,getCurrentPlaySrc());
	}

	@Override
	public final void run() {
		if(isTimerTaskStart){
			onProgressChange(this);
		}
	}
	
	protected void onProgressChange(BaseMediaPlayer mediaPlayer){
		performOnProgressChange(getCurrentPosition(), getDuration(),getCurrentPlaySrc());
	}
	
	protected abstract String getCurrentPlaySrc();
	
	public final void addPlayerListener(PlayerListener l){
		synchronized (mPlayerListeners) {
			if(!containsPlayerListeners(l)){
				mPlayerListeners.add(new WeakReference<PlayerListener>(l));
			}
		}
	}
	
	public final void removePlayerListener(PlayerListener l){
		synchronized (mPlayerListeners) {
			for (Iterator<WeakReference<PlayerListener>> iterator = mPlayerListeners.iterator(); iterator.hasNext();) {
				WeakReference<PlayerListener> playerListener = (WeakReference<PlayerListener>) iterator.next();
				if(playerListener.get() != null && playerListener.get().equals(l)){
					iterator.remove();
					return;
				}
			}
		}
	}
	
	private boolean containsPlayerListeners(PlayerListener l){
		for (WeakReference<PlayerListener> playerListener : mPlayerListeners) {
			if(playerListener.get() != null && playerListener.get().equals(l)){
				return true;
			}
		}
		return false;
	}
	
	public void performOnPlayStateChange(int state, String voiceSrc){
		if(mState == state){
			return;
		}
//		int oldState = mState;
//		if(state == STATE_STOP
//				|| state == STATE_COMPLETION
//				|| state == STATE_ERROR){
//			if(oldState != STATE_START && oldState != STATE_PAUSE && oldState != STATE_PREPAREING){
//				return;
//			}
//		}else if(state == STATE_PAUSE){
//			if(oldState != STATE_START && oldState != STATE_PREPAREING){
//				return;
//			}
//		}
		mState = state;
		for (WeakReference<PlayerListener> l : mPlayerListeners) {
			if(l.get() != null){
				l.get().onPlayStateChange(state, voiceSrc);
			}
		}
		
		if(mState == STATE_START){
			isTemporaryPause = false;
//			if(mOnAudioFocusChangeListener == null){
//				mOnAudioFocusChangeListener = new OnAudioFocusChangeListener() {
//					@Override
//					public void onAudioFocusChange(int focusChange) {
//						if(focusChange == AudioManager.AUDIOFOCUS_GAIN 
//								|| focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK){
//							resumeTemporaryPause();
//						}else{
//							temporaryPause();
//						}
//					}
//				};
//			}
//			mAudioManager.requestAudioFocus(mOnAudioFocusChangeListener, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
		}else if(mState == STATE_STOP){
//			if(mOnAudioFocusChangeListener != null){
//				mAudioManager.abandonAudioFocus(mOnAudioFocusChangeListener);
//				mOnAudioFocusChangeListener = null;
//			}
		}
	}
	
	public boolean isTemporaryPause(){
		return isTemporaryPause;
	}
	
	public void temporaryPause(){
		try {
			if(isPlaying()){
				pause();
				isTemporaryPause = true;
			}
		} catch (Exception e) {}
	}
	
	public void resumeTemporaryPause(){
		try {
			if(isTemporaryPause){
				start();
				isTemporaryPause = false;
			}
		} catch (Exception e) {}
	}
	
	public void performOnProgressChange(long currentPosition,long maxPosition, String voiceSrc){
		for (WeakReference<PlayerListener> l : mPlayerListeners) {
			if(l.get() != null){
				l.get().onProgressChange(currentPosition, maxPosition, voiceSrc);
			}
		}
	}
	public interface PlayerListener{
		public void onPlayStateChange(int state, String voiceSrc);
		public void onProgressChange(long currentPosition,long maxPosition, String voiceSrc);
	}
}
