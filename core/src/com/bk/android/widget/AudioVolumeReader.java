package com.bk.android.widget;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.Handler;
import android.os.Looper;

import com.bk.android.os.TerminableThread;

public class AudioVolumeReader{
	private static Object mAudioRecordLock = new Object();
	private static AudioRecord mAudioRecord;
    private static int SAMPLE_RATE_IN_HZ = 48000;  
    private int mMinBufferSize;  
    private byte[] mBuffer;
    private TerminableThread mTerminableThread;
	private VolumeListeners mVolumeListeners;
	private Handler mHandler;
	private long mLastTime;

	public AudioVolumeReader(){
		mHandler = new Handler(Looper.getMainLooper());
		mMinBufferSize = AudioRecord.getMinBufferSize(SAMPLE_RATE_IN_HZ,  
                AudioFormat.CHANNEL_IN_MONO,  
                AudioFormat.ENCODING_PCM_16BIT);
		if(mMinBufferSize <= 0){
			mMinBufferSize = SAMPLE_RATE_IN_HZ;
		}
		mBuffer = new byte[mMinBufferSize];
	}
	
	public void start(){
		if(mTerminableThread != null){
			return;
		}
		mTerminableThread = new TerminableThread(){
			@Override
			public void run() {
				synchronized (mAudioRecordLock) {
					try {
						mAudioRecord = new AudioRecord(MediaRecorder.AudioSource.MIC, SAMPLE_RATE_IN_HZ,  
				                AudioFormat.CHANNEL_IN_MONO,  
				                AudioFormat.ENCODING_PCM_16BIT, mMinBufferSize);
						mAudioRecord.startRecording();
						mLastTime = System.currentTimeMillis();
						while (true) {
							if(!isCancel()){
								long time = System.currentTimeMillis() - mLastTime;
								mLastTime = System.currentTimeMillis();
								final int volume = getVolume(mAudioRecord,time);
//								Log.e("消息","time="+time+" volume="+volume);
								mHandler.post(new Runnable() {
									@Override
									public void run() {
										if(mVolumeListeners != null && !isCancel()){
											mVolumeListeners.onVolumeChange(volume);
										}
									}
								});
							}else{
								break;
							}
							sleep();
						}
						mAudioRecord.stop();
						mAudioRecord.release();
					} catch (Exception e) {}
					mAudioRecord = null;
				}
			}
			
			@Override
			public boolean isCancel() {
 				return super.isCancel() || !this.equals(mTerminableThread);
			}

			@Override
			protected void finalize() throws Throwable {
				super.finalize();
				cancel();
			}
		};
		mTerminableThread.start();
	}
	
	public void stop(){
		if(mTerminableThread != null){
			mTerminableThread.cancel();
			mTerminableThread = null;
		}
	}
	
	private void sleep(){
		sleep(100);
	}
	
	private void sleep(int time){
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {}
	}
	
	public int getVolume(AudioRecord audioRecord,long time){
		int r = audioRecord.read(mBuffer, 0, mMinBufferSize);  
//		Log.e("消息","time="+time+" r="+r+" s="+mMinBufferSize);//640 50 26   4090 2
		int v = 0;  
		// 将 buffer 内容取出，进行平方和运算  
		for (int i = 0; i < r; i++) {  
		    // 这里没有做运算的优化，为了更加清晰的展示代码  
		    v += mBuffer[i] * mBuffer[i];  
		}
		if(r == 0){
			return 0;
		}
		//计算分贝值
//		return 10 * Math.log10(v / (double)r);
		// 平方和除以数据总长度，得到音量大小。可以获取白噪声值，然后对实际采样进行标准化。
		return v / r / 100;
	}
	
	public void setVolumeListeners(VolumeListeners l){
		mVolumeListeners = l;
	}
	
	public interface VolumeListeners{
		public void onVolumeChange(int volume);
	}
}
