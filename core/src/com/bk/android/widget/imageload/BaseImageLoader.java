package com.bk.android.widget.imageload;

import java.io.File;
import java.io.FileInputStream;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map.Entry;

import android.content.ContentResolver;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.net.Uri;
import android.os.Debug;
import android.os.SystemClock;
import android.os.Build.VERSION;
import android.text.TextUtils;
import android.util.SparseArray;

import com.bk.android.app.BaseApp;
import com.bk.android.util.BitmapUtil;
import com.bk.android.util.BitmapUtil.LoadNetCallback;
import com.bk.android.util.FileUtil;
import com.bk.android.util.LockUtil;
import com.bk.android.util.LogUtil;


public abstract class BaseImageLoader implements LoadTaskCallback{
	private final static String TAG = "BaseImageLoader";
	private final static long MAX_HEADSPACE = (long) (Runtime.getRuntime().maxMemory() * 0.01f);
	private final static long MIN_HEADSPACE = 1024L * 1024 * 1;
	private final static long SDCARD_MAX_HEADSPACE = 1024L * 1024 * 100;
	private final static long SDCARD_MAX_THUMB_HEADSPACE = 1024L * 1024 * 50;
	private final static String LOCAD_IMG_CHANNEL_1 = "LOCAD_IMG_CHANNEL_1";
	private final static String LOCAD_IMG_CHANNEL_2 = "LOCAD_IMG_CHANNEL_2";
	
	private HashMap<String, int[]> mSizeCache = new HashMap<String,int[]>();
	private SparseArray<LoadTask> mTaskMap = new SparseArray<LoadTask>();
	private LinkedList<LoadTask> mNetTasks = new LinkedList<LoadTask>();
	private Object mLock = new Object();
	private LinkedList<String> mCheckingSdcardList = new LinkedList<String>();
	private LinkedList<String> mCreateThumbList = new LinkedList<String>();
	private LoadThreadPool mSdcardPool;
	private LoadThreadPool mNetPool;
	private LoadThreadPool mCreateThumbPool;
	private LoadThreadPool mMainPool;
	private HashMap<String, WeakReference<Bitmap>> mBitmapSecondCache = new HashMap<String,WeakReference<Bitmap>>();
	private HashMap<String, BitmapCache> mBitmapCache = new HashMap<String,BitmapCache>();
	private LinkedList<BitmapCache> mBitmapCacheList = new LinkedList<BitmapCache>();
	private int mBitmapCacheTotalSpace;
	private long mLastCheckThumbTime = 0;
	private long mLastImgChannel = 1;
	private int mPixels;
	
	public BaseImageLoader(){
		checkSdcardImageSize();
		checkSdcardImageThumbSize();
		mBitmapCacheTotalSpace = 0;
		mCreateThumbPool = new LoadThreadPool(1, Thread.MIN_PRIORITY,false);
		mMainPool = new LoadThreadPool(1, Thread.NORM_PRIORITY,false);
		mNetPool = new LoadThreadPool(getNetPoolSize() , Thread.NORM_PRIORITY,true);
		mSdcardPool = new LoadThreadPool(getSdcardPoolSize() , Thread.NORM_PRIORITY,true);
		mPixels = (int) (BaseApp.getInstance().getResources().getDisplayMetrics().widthPixels * 1.3f);
	}

	private String getThumbPath(){
		return getThumbBasePath() + "_" + getThumbPathVersion() + "/";
	}
	
	public abstract String getThumbBasePath();

	public abstract String getThumbPathVersion();

	public void releaseCache(){
		synchronized (mBitmapCache) {
			mBitmapCache.clear();
		}
		synchronized (mBitmapCacheList) {
			mBitmapCacheList.clear();
		}
		synchronized (mBitmapSecondCache) {
			mBitmapSecondCache.clear();
		}
		System.gc();
	}
	
	public int getNetPoolSize(){
		return 5;
	}
	
	public int getSdcardPoolSize(){
		return 2;
	}
	
	public ImageBitmapInfo loadImage(final String imageUrl, final ImageCallback callback, final int quality) {
		return loadImage(imageUrl, callback, quality,0,0);
	}
	
	public ImageBitmapInfo loadImage(final String imageUrl, final ImageCallback callback, final int quality, int width, int height) {
		return loadImage(imageUrl, callback, quality, width, height, false);
	}
	
	public ImageBitmapInfo loadImage(final String imageUrl, final ImageCallback callback, final int quality, int width, int height, boolean force) {
		if(TextUtils.isEmpty(imageUrl)){
			return null;
		}
		String imageId = getId(imageUrl);
		final String imageCacheId = getCacheId(imageId, quality);
		
		ImageBitmapInfo imageInfo = null;
		if(force){
			imageInfo = getSizeImageInfo(imageUrl, imageCacheId, width, height, quality);
		}else{
			Bitmap bitmap = loadImageInCache(imageCacheId);
			if (bitmap != null) {
				imageInfo = new ImageBitmapInfo();
				imageInfo.mWidth = bitmap.getWidth();
				imageInfo.mHeight = bitmap.getHeight();
				imageInfo.mBitmap = bitmap;
				return imageInfo;
			}else{
				String thumbUrl = getThumbImageUrl(imageUrl);
				String thumbId = getId(thumbUrl);
				String thumbCacheId = getCacheId(thumbId, quality);
				if(bitmap == null){
					bitmap = getThumbCache(thumbId,thumbCacheId,imageId,imageCacheId,imageUrl,quality);
				}
				if(bitmap != null){
					imageInfo = new ImageBitmapInfo();
					imageInfo.mWidth = bitmap.getWidth();
					imageInfo.mHeight = bitmap.getHeight();
					imageInfo.mBitmap = bitmap;
				}else{
					imageInfo = getSizeImageInfo(imageUrl, imageCacheId, width, height, quality);
				}
			}
		}
		mMainPool.addTask(new Runnable() {
			@Override
			public void run() {
				synchronized (mLock) {
					int key = imageCacheId.hashCode();
					LoadTask task = mTaskMap.get(key);
					LogUtil.d(TAG, "id="+imageCacheId+" init hasTask="+(task != null));
					LoadTask oldTask = null;
					
					for (LoadTask taskTemp : mNetTasks) {
						if(!taskTemp.isCancel()){
							if(oldTask == null){
								oldTask = taskTemp;
							}
							if(taskTemp.getImageCacheId().equals(imageCacheId)){
								oldTask = null;
								break;
							}
						}
					}
					
					if(task != null && containsPoolQueue(task)){
						LogUtil.i(TAG, "id="+imageCacheId+" destroy");
						task.destroy();
						task = new LoadTask(task,true);
						mTaskMap.put(key, task);
						addTask(task);
					}
					
					if(task == null){
						task = new LoadTask(imageUrl, quality, BaseImageLoader.this);
						mTaskMap.put(key, task);
						addTask(task);
					}
					
					if(oldTask != null && !task.isLocal() && getCorePoolSize() <= getPoolActiveCount()){
						LogUtil.i(TAG, "id="+imageCacheId+" cancel task="+oldTask.getImageCacheId());
						oldTask.cancel();
					}
					task.addImageCallback(callback);
				}
			}
		});
		return imageInfo;
	}
	
	private Bitmap getThumbCache(String thumbId, String thumbCacheId, String imageId, String imageCacheId, String url, int quality){
		Bitmap bitmap = null;
		String tempUrl = handImageUrl(url, quality);
		if(!url.equals(tempUrl)){
			String tempThumbId = getId(tempUrl);
			String tempThumbCacheId = getCacheId(tempThumbId, quality);
			for (;quality <= BitmapUtil.IMG_QUALITY_4;) {
				bitmap = getThumbCache(tempThumbId, tempThumbCacheId);
				if(bitmap != null){
					break;
				}else{
					quality++;
					tempThumbId = getId(handImageUrl(tempUrl, quality));
					tempThumbCacheId = getCacheId(tempThumbId, quality);
				}
			}
		}
		if(bitmap == null && !TextUtils.isEmpty(thumbId)){
			bitmap = getThumbCache(thumbId, thumbCacheId);
		}
		if(bitmap == null && !TextUtils.isEmpty(imageId)){
			bitmap = getThumbCache(imageId, imageCacheId);
		}
		return bitmap;
	}
	
	private Bitmap getThumbCache(String thumbId, String thumbCacheId){
		if(TextUtils.isEmpty(thumbId)){
			return null;
		}
		synchronized (mBitmapCache) {
			Bitmap bitmap = null;
			thumbId = thumbId + "_";
			for (BitmapCache bitmapCache : mBitmapCacheList) {
				if(bitmapCache.mKey.indexOf(thumbId) != -1){
					bitmap = bitmapCache.mBitmap;
					break;
				}
			}
			if(bitmap == null){
				for (Iterator<Entry<String, WeakReference<Bitmap>>> iterator = mBitmapSecondCache.entrySet().iterator(); iterator.hasNext();) {
					Entry<String, WeakReference<Bitmap>> entry = (Entry<String, WeakReference<Bitmap>>) iterator.next();
					if(entry.getValue().get() != null){
						if(entry.getKey().indexOf(thumbId) != -1){
							bitmap = entry.getValue().get();
							if(bitmap != null){
								break;
							}
						}
					}else{
						iterator.remove();
					}
				}
			}
			return bitmap;
		}
	}
	
	private ImageBitmapInfo getSizeImageInfo(String url, String cacheId,int width,int height, int quality){
		if(TextUtils.isEmpty(url)){
			return null;
		}
		String path = url;
		if(url.startsWith("http://")){
			path = getPath(cacheId, url);
		}
		ImageBitmapInfo bitmapInfo = null;
		int[] size = null;
		synchronized (mSizeCache) {
			size = mSizeCache.get(cacheId);
		}
		if(size == null){
			if(width != 0 && height != 0){
				size = new int[]{width,height};
			}else{
				size = BitmapUtil.getLocalWH(path,quality);
				if(size != null){
					synchronized (mSizeCache) {
						mSizeCache.put(cacheId, size);
					}
				}
			}
		}
		if(size != null){
			bitmapInfo = new ImageBitmapInfo();
			bitmapInfo.mWidth = size[0];
			bitmapInfo.mHeight = size[1];
		}
		return bitmapInfo;
	}
	
	private void addTask(LoadTask task) {
		if(isResource(task.getUrl()) 
				|| FileUtil.isFileExists(getSdcardThumbnailPath(task.getUrl(), task.getQuality()))
				|| FileUtil.isFileExists(task.getImagePath())){
			task.setLocal();
			mSdcardPool.addTask(task);
		}else{
			mNetPool.addTask(task);
		}
	}

	private int getPoolActiveCount() {
		return mNetPool.getExecutor().getActiveCount();
	}

	private boolean containsPoolQueue(Object task){
		return mNetPool.getExecutor().getQueue().contains(task) || mSdcardPool.getExecutor().getQueue().contains(task);
	}
	
	private int getCorePoolSize(){
		return mNetPool.getCorePoolSize();
	}
	
	private void checkSdcardImageSize(){
		long maxSize = SDCARD_MAX_HEADSPACE;
		long storageSize = (long) (FileUtil.getStorageSize() * 0.5f);
		if(maxSize > storageSize){
			maxSize = storageSize;
		}
		checkSdcardImageSize(getPath("", ""),null,maxSize);
	}
	
	private void checkSdcardImageThumbSize(){
		if(SystemClock.uptimeMillis() - mLastCheckThumbTime < 60000){
			return;
		}
		mLastCheckThumbTime = SystemClock.uptimeMillis();
		long maxSize = SDCARD_MAX_THUMB_HEADSPACE;
		long storageSize = (long) (FileUtil.getStorageSize() * 0.3f);
		if(maxSize > storageSize){
			maxSize = storageSize;
		}
		checkSdcardImageSize(getThumbPath(),getThumbBasePath(),maxSize);
	}
	
	private void checkSdcardImageSize(final String path,final String basePath,final long maxSize){
		synchronized (mCheckingSdcardList) {
			if(mCheckingSdcardList.contains(path)){
				return;
			}
			mCheckingSdcardList.add(path);
			Thread thread = new Thread(){
				@Override
				public void run() {
					File fileDir = new File(path);
					if(!TextUtils.isEmpty(basePath)){
						File parentFile = fileDir.getParentFile();
						if(parentFile != null){
							File[] parentListFile = parentFile.listFiles();
							if(parentListFile != null && parentListFile.length > 0){
								for (File file : parentListFile) {
									if(file.isDirectory() && file.getAbsolutePath().indexOf(basePath) != -1 && !file.getAbsolutePath().equals(fileDir.getAbsolutePath())){
										synchronized (LockUtil.getFileLock(file.getAbsolutePath())) {
											FileUtil.delFiles(file.getAbsolutePath(),true);
										}
									}
								}
							}
						}
					}
					long totalSpace = 0;
					File[] fileArr = fileDir.listFiles();
					LinkedList<File> fileList = new LinkedList<File>();
					if(fileArr != null && fileArr.length > 0){
						for (File arrFile : fileArr) {
							totalSpace += arrFile.length();
							int i = 0;
							for (File listFile : fileList) {
								if(listFile.lastModified() >= arrFile.lastModified()){
									break;
								}
								i++;
							}
							fileList.add(i,arrFile);
						}
						for (;fileList.size() > 0 && totalSpace > maxSize;) {
							File file = fileList.removeFirst();
							synchronized (LockUtil.getFileLock(file.getAbsolutePath())) {
								long length = file.length();
								FileUtil.delFiles(file.getAbsolutePath());
								totalSpace -= length;
							}
						}
					}
					synchronized (mCheckingSdcardList) {
						mCheckingSdcardList.remove(path);
					}
				}
			};
			thread.setPriority(Thread.MIN_PRIORITY);
			thread.start();
		}
	}
	
	@Override
	public String getThumbImageUrl(String sourceUrl) {
		if(sourceUrl == null || sourceUrl.indexOf("http://") == -1){
			return null;
		}
		if(checkImageUrl(sourceUrl)){
			return handImageUrl(sourceUrl,null);
		}
		if(sourceUrl.indexOf("_min.") == -1){
			int start = sourceUrl.lastIndexOf(".");
			if(start != -1){
				return sourceUrl.substring(0, start) + "_min" + sourceUrl.substring(start, sourceUrl.length());
			}
		}
		return null;
	}
	
	@Override
	public String getId(String imageUrl) {
		if(TextUtils.isEmpty(imageUrl)){
			return "";
		}
		String id = imageUrl.hashCode() + "";
		if(BitmapUtil.isGifPath(imageUrl)){
			id += ".gifId";
		}else if(BitmapUtil.isPngPath(imageUrl)){
			id += ".pngId";
		}
		return id;
	}

	@Override
	public String getCacheId(String imageId, int quality) {
		return imageId + "_" + quality;
	}

	@Override
	public Bitmap loadImageInCache(String imageCacheId) {
		synchronized (mBitmapCache) {
			BitmapCache bitmapCache = mBitmapCache.get(imageCacheId);
			if(bitmapCache != null){
				mBitmapCacheList.remove(bitmapCache);
				mBitmapCacheList.addLast(bitmapCache);
				return bitmapCache.mBitmap;
			}else{
				WeakReference<Bitmap> weakBitmap = mBitmapSecondCache.remove(imageCacheId);
				if(weakBitmap != null){
					if(weakBitmap.get() != null){
						return weakBitmap.get();
					}
				}
			}
			
			return null;
		}
	}
	
	private void saveImageCache(Bitmap bitmap, String imageCacheId) {
		if(bitmap != null){
			long bitmapSize = bitmap.getRowBytes() * bitmap.getHeight();
			synchronized (mBitmapCache) {
				if(mBitmapCache.containsKey(imageCacheId)){
					return;
				}
				BitmapCache newCache = new BitmapCache(imageCacheId, bitmap);
				long totalSpace = mBitmapCacheTotalSpace + bitmapSize;
				long maxSize = getMaxSize();
				for (Iterator<Entry<String, WeakReference<Bitmap>>> iterator = mBitmapSecondCache.entrySet().iterator(); iterator.hasNext();) {
					Entry<String, WeakReference<Bitmap>> entry = (Entry<String, WeakReference<Bitmap>>) iterator.next();
					if(entry.getValue().get() == null){
						iterator.remove();
					}
				}
				
				for (;totalSpace > maxSize && totalSpace > 0;) {
					totalSpace = recycle(totalSpace);
				}
				
				mBitmapCache.put(imageCacheId, newCache);
				mBitmapCacheTotalSpace += bitmapSize;
				mBitmapCacheList.addLast(newCache);
				LogUtil.v(TAG, "Save ImageCache size=" + mBitmapCacheList.size() + " secondSize=" + mBitmapSecondCache.size() + " totalS=" + mBitmapCacheTotalSpace + " totalM=" + maxSize);
			}
			int[] size = new int[]{bitmap.getWidth(),bitmap.getHeight()};
			synchronized (mSizeCache) {
				mSizeCache.put(imageCacheId, size);
			}
		}
	}
	
	private long getMaxSize(){
		long maxSize = (long) (Debug.getNativeHeapSize() * 0.3f);
		if(maxSize < MIN_HEADSPACE){
			maxSize = MIN_HEADSPACE;
		}else if(maxSize >= MAX_HEADSPACE){
			maxSize = MAX_HEADSPACE;
		}
		return maxSize;
	}
	
	private long recycle(long totalSpace){
		if(!mBitmapCacheList.isEmpty()){
			BitmapCache bitmapCache = mBitmapCacheList.removeFirst();
			long byteCount = bitmapCache.mBitmap.getRowBytes() * bitmapCache.mBitmap.getHeight();
			totalSpace -= byteCount;
			mBitmapCacheTotalSpace -= byteCount;
			mBitmapCache.remove(bitmapCache.mKey);
			mBitmapSecondCache.put(bitmapCache.mKey, new WeakReference<Bitmap>(bitmapCache.mBitmap));
			LogUtil.d(TAG, "Release ImageCache size="+mBitmapCacheList.size()+" totalSpace="+mBitmapCacheTotalSpace);
			return totalSpace;
		}
		LogUtil.d(TAG, "Release ImageCache size="+mBitmapCacheList.size()+" totalSpace="+mBitmapCacheTotalSpace);
		return 0;
	}
	
	public boolean checkImageUrl(String url){
		try {
			int index = url.indexOf("http://osscdn2.banketime.com");
			if(index != -1){
				index = url.indexOf("@");
				if(index != -1){
					index = url.charAt(index + 1) == '!' ? -1 : 1;
				}
			}
			return index != -1;
		} catch (Exception e) {
			return false;
		}
	}
	
	public String handImageUrl(String url,Integer quality){
		if(TextUtils.isEmpty(url) || BitmapUtil.isGifPath(url) || BitmapUtil.isPngPath(url)){
			return url;
		}
		int index = url.indexOf("http://osscdn.banketime.com");
		if(index != -1){
			url = url.replace("http://osscdn.banketime.com", "http://osscdn2.banketime.com");
		}else{
			index = url.indexOf("http://osscdn2.banketime.com");
		}
		if(index != -1){
			if(url.indexOf("_min.") != -1){
				url = url.replace("_min.", ".");
			}
			index = url.lastIndexOf("@");
			if(index != -1){
				url = url.substring(0, index);
			}
			if(quality != null && quality >= BitmapUtil.IMG_QUALITY_NON && quality <= BitmapUtil.IMG_QUALITY_4){
				int w = (int) (mPixels * BitmapUtil.getQualityScale(quality));
				int h = w;
				url = url + "@" + w + "w_" + h + "h_1l" + ".jpg";
			}else{
				url = url + "@!_min.style";
			}
		}
		return url;
	}
	
	@Override
	public void loadImageFromNetwork(String cacheId,String path,String imageUrl,int quality, LoadNetCallback callback) {
		long time = System.currentTimeMillis();
		boolean isSuccess;
		synchronized (LockUtil.getFileLock(path)) {
			isSuccess = BitmapUtil.downloadImg(path, imageUrl, callback);
		}
		if(isSuccess){
			checkSdcardImageSize();
		}
		LogUtil.w(TAG, "id="+cacheId+" isSuccess="+isSuccess+ " time=" + (System.currentTimeMillis() - time) +" loadImageFromNetwork="+imageUrl);
	}
	
	@Override
	public Bitmap loadImageFromResource(String imageCacheId,String path, String url,int quality) {
		if(TextUtils.isEmpty(url)){
			return null;
		}
		mLastImgChannel = mLastImgChannel == 1 ? 2 : 1;
		if(mLastImgChannel == 1){
			synchronized (LOCAD_IMG_CHANNEL_1) {
				return handLoadImageFromSdcard(imageCacheId, path, url, quality);
			}
		}else{
			synchronized (LOCAD_IMG_CHANNEL_2) {
				return handLoadImageFromSdcard(imageCacheId, path, url, quality);
			}
		}
	}
	
	protected Bitmap handLoadImageFromSdcard(String imageCacheId,String path, String url,int quality){
		LogUtil.v(TAG, "id=" + getCacheId(getId(url), quality) + " loadImageFromSdcard");
		Bitmap bitmap = null;
		try {
			if(url.startsWith("http://")){
				int tempQuality = quality;
				if(imageCacheId == null){
					String tempUrl = handImageUrl(url, null);
					String tempPath = getPath(getId(tempUrl), tempUrl);
					for (;tempQuality <= BitmapUtil.IMG_QUALITY_4;) {
						bitmap = loadImageFromSdcard(tempPath,tempUrl,tempQuality);
						if(bitmap != null){
							break;
						}else{
							tempQuality++;
							tempUrl = handImageUrl(tempUrl, tempQuality);
							tempPath = getPath(getId(tempUrl), tempUrl);
						}
					}
				}else if(checkImageUrl(url)){
					if(tempQuality >= BitmapUtil.IMG_QUALITY_1){
						String thumbUrl = handImageUrl(url, null);
						String thumbPath = getPath(getId(thumbUrl), thumbUrl);
						bitmap = loadImageFromSdcard(thumbPath,thumbUrl,tempQuality);
					}
					for (;bitmap == null && tempQuality >= BitmapUtil.IMG_QUALITY_NON;) {
						String tempUrl = handImageUrl(url, tempQuality);
						String tempPath = getPath(getId(tempUrl), tempUrl);
						bitmap = loadImageFromSdcard(tempPath,tempUrl,tempQuality);
						tempQuality--;
					}
				}
				if(bitmap == null){
					bitmap = loadImageFromSdcard(path,url,quality);
				}
				BitmapUtil.getLocalWH(path);
			}else{
				Uri uri = Uri.parse(url);
				if(url.startsWith(ContentResolver.SCHEME_FILE) || TextUtils.isEmpty(uri.getScheme())){
					if(BitmapUtil.isMp4Path(url)){
						if(FileUtil.isFileExists(path)){
			        		bitmap = loadImageFromSdcard(path, url, quality);
						}else{
							bitmap = BitmapUtil.getBitmapInSdcard(uri.getPath(), quality);
							BitmapUtil.saveImage(path, bitmap, 100);
							bitmap = BitmapUtil.clipBitmap(bitmap, quality);
						}
					}else{
		        		bitmap = loadImageFromSdcard(uri.getPath(),url, quality);
					}
	        	}else{
	        		synchronized (LockUtil.getFileLock(url)) {
	        			bitmap = BitmapUtil.clipScreenBoundsBitmap(BaseApp.getInstance().getResources(),uri,BitmapUtil.getQualityScale(quality));
	        		}
	        	}
			}
		} catch (OutOfMemoryError ex) {
		} catch (Exception e) {}
		if(bitmap != null && imageCacheId != null){
			saveImageCache(bitmap, imageCacheId);
		}
		return bitmap;
	}

	@Override
	public String getSdcardThumbnailPath(String url, int quality){
		try {
			String path = Uri.parse(url).getPath();
			if(url.startsWith(ContentResolver.SCHEME_FILE) && path != null && FileUtil.isFileExists(path)){
				return getThumbPath() + "_" + url.hashCode() + "_" + quality + "_" + new File(path).lastModified();
	    	}
		} catch (Exception e) {}
		return getThumbPath() + "_" + url.hashCode() + "_" + quality;
	}
	
	public Bitmap loadImageFromSdcard(final String path,final String url,final int quality){
		synchronized (LockUtil.getFileLock(path)) {
			Bitmap bitmap = null;
			File file = new File(path);
			if(!TextUtils.isEmpty(path)){
				final String thumbnailPath = getSdcardThumbnailPath(url, quality);
				if(FileUtil.isFileExists(thumbnailPath) && file.exists()){
					try {
						Options opts = new Options();
						opts.inPreferredConfig = Bitmap.Config.RGB_565;
						if(VERSION.SDK_INT >= 14){
							opts.inMutable = true;
				        }
						bitmap = BitmapFactory.decodeStream(new FileInputStream(thumbnailPath),null,opts);
					} catch (Exception e) {}
				}
				if(bitmap == null){
					if(file.exists()){
						long time = System.currentTimeMillis();
						bitmap = BitmapUtil.getBitmapInSdcard(path, quality);
						if(bitmap != null){
							time = System.currentTimeMillis() - time;
							if(!url.startsWith("http://") && quality > BitmapUtil.IMG_QUALITY_0 && time > 200 && !BitmapUtil.isGifPath(url) && !BitmapUtil.isPngPath(url) && !FileUtil.isFileExists(thumbnailPath)){
								synchronized (mCreateThumbList) {
									if(!mCreateThumbList.contains(thumbnailPath)){
										mCreateThumbList.add(thumbnailPath);
										mCreateThumbPool.addTask(new Runnable() {
											@Override
											public void run() {
												synchronized (LockUtil.getFileLock(path)) {
													if(!FileUtil.isFileExists(thumbnailPath)){
														Bitmap bitmap = BitmapUtil.getBitmapInSdcard(path, quality);
														if(bitmap != null){
															BitmapUtil.saveImage(thumbnailPath, bitmap, 80);
															bitmap.recycle();
														}
														synchronized (mCreateThumbList) {
															mCreateThumbList.remove(thumbnailPath);
														}
													}
												}
											}
										});
									}
								}
								checkSdcardImageThumbSize();
							}
						}
					}
				}
			}
			return bitmap;
		}
	}
	
	@Override
	public void onStart(LoadTask task) {
		synchronized (mLock) {
			LogUtil.d(TAG, "id="+task.getImageCacheId()+" onStart");
			if(!task.isLocal()){
				mNetTasks.add(task);
			}
		}
	}

	@Override
	public void onEnd(LoadTask task,Bitmap bitmap) {
		synchronized (mLock) {
			mNetTasks.remove(task);
			int key = task.getImageCacheId().hashCode();
			LoadTask newTask = mTaskMap.get(key);
			mTaskMap.remove(key);
			if(task != null && newTask != null && newTask.equals(task) && task.isCancel()){
				task = new LoadTask(task);
				mTaskMap.put(key, task);
				addTask(task);
				LogUtil.i(TAG, "id="+task.getImageCacheId()+" restart");
			}
			LogUtil.d(TAG, "id="+task.getImageCacheId()+" onEnd ");
		}
	}

	private boolean isResource(String imageUrl){
		String scheme = null;
		Uri uri = null;
		try {
			uri = Uri.parse(imageUrl);
			if(uri != null){
				scheme = uri.getScheme();
			}
		} catch (Exception e) {}
		return ContentResolver.SCHEME_ANDROID_RESOURCE.equals(scheme)
		|| ContentResolver.SCHEME_CONTENT.equals(scheme)
        || ContentResolver.SCHEME_FILE.equals(scheme);
	}

	public static class ImageBitmapInfo{
		public int mWidth;
		public int mHeight;
		public Bitmap mBitmap;
	}
	
	public static class BitmapCache{
		public String mKey;
		public Bitmap mBitmap;
		public BitmapCache(String key, Bitmap bitmap) {
			this.mKey = key;
			this.mBitmap = bitmap;
		}
	}
	
	public interface ImageCallback{
		public void imageLoaded(Bitmap bitmap, String imageUrl, String cacheId);
		public void onDownloadProgress(String imageUrl,long p,long m);
	}
}
