package com.bk.android.widget.imageload;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

import com.bk.android.os.AbsThreadPool;

public class LoadThreadPool extends AbsThreadPool{
	private int mPoolSize;
	private int mPriority;
	private boolean mNeedPriorityTask;

	public LoadThreadPool(int poolSize,int priority,boolean needPriorityTask){
		mPoolSize = poolSize;
		mPriority = priority;
		mNeedPriorityTask = needPriorityTask;
		checkInit();
	}
	
	@Override
	protected int getCorePoolSize() {
		return mPoolSize;
	}

	@Override
	protected int getMaximumPoolSize() {
		return 128;
	}

	@Override
	protected long getKeepAliveTime() {
		return 1;
	}

	@Override
	protected TimeUnit getTimeUnit() {
		return TimeUnit.SECONDS;
	}

	@Override
	protected BlockingQueue<Runnable> newQueue() {
		if(!mNeedPriorityTask){
			return new LinkedBlockingQueue<Runnable>();
		}
		return new PriorityBlockingQueue<Runnable>();
	}
	
	@Override
	protected Runnable onAddTask(Runnable newTask) {
		if(!mNeedPriorityTask){
			return super.onAddTask(newTask);
		}
		if(newTask instanceof Comparable){
			return super.onAddTask(newTask);
		}
		return super.onAddTask(new SimplePriorityRunnable(newTask));
	}
	
	@Override
	protected ThreadFactory newThreadFactory() {
		return new DefaultThreadFactory(mPriority);
	}
}
