package com.bk.android.widget.imageload;

import android.graphics.Bitmap;

import com.bk.android.util.BitmapUtil.LoadNetCallback;

public interface LoadTaskCallback {
	public String getThumbImageUrl(String sourceUrl);

	public String getId(String imageUrl);
	
	public String getPath(String imageId, String imageUrl);

	public String getCacheId(String imageId, int quality);

	public Bitmap loadImageInCache(String imageCacheId);

	public Bitmap loadImageFromResource(String cacheId,String path,String url, int quality);

	public String getSdcardThumbnailPath(String url, int quality);
	
	public void loadImageFromNetwork(String cacheId,String path,String url,int quality, LoadNetCallback callback);
	/**
	 * 任务开始点
	 * @param url
	 */
	public void onStart(LoadTask task);
	/**
	 * 任务结束点
	 * @param bitmap
	 * @param url
	 * @param imageId
	 * @param imagePath
	 * @param imageCacheId
	 */
	public void onEnd(LoadTask task,Bitmap bitmap);
}
