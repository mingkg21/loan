package com.bk.android.widget.imageload;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import org.apache.http.impl.client.DefaultHttpClient;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;

import com.bk.android.app.BaseApp;
import com.bk.android.util.ApnUtil;
import com.bk.android.util.BitmapUtil.LoadNetCallback;
import com.bk.android.util.LockUtil;
import com.bk.android.util.LogUtil;
import com.bk.android.widget.imageload.BaseImageLoader.ImageCallback;


public class LoadTask implements Runnable,Comparable<LoadTask>,LoadNetCallback{
	private final static String TAG = "BaseImageLoader";
	private static Handler sHandler = new Handler(Looper.getMainLooper());
	private static int sLastPriority = 0;

	protected long mPriority;
	protected int mQuality;
	private String mUrl;
	private LoadTaskCallback mCallback;
	private boolean isCancel;
	private DefaultHttpClient mDefaultHttpClient;
	private ArrayList<WeakReference<ImageCallback>> mImageCallbacks = new ArrayList<WeakReference<ImageCallback>>();
	private boolean isDestroy;
	private String mImageId;
	private String mImagePath;
	private String mImageCacheId;
	private boolean isLocal;
	private Thread mCurrentThread;
	
	public LoadTask(LoadTask task){
		this(task, false);
	}
	
	public LoadTask(LoadTask task,boolean isNew){
		if(isNew){
			onSetPriority();
		}else{
			mPriority = task.mPriority;
		}
		mUrl = task.mUrl;
		mCallback = task.mCallback;
		mQuality = task.mQuality;
		mImageId = task.mImageId;
		mImagePath = task.mImagePath;
		mImageCacheId = task.mImageCacheId;
		synchronized (mImageCallbacks) {
			for (WeakReference<ImageCallback> weakReference : task.mImageCallbacks) {
				if(weakReference.get() != null){
					mImageCallbacks.add(weakReference);
				}
			}
		}
	}
	
	public LoadTask(String url,int quality,LoadTaskCallback callback){
		onSetPriority();
		mUrl = url;
		mCallback = callback;
		mQuality = quality;
		mImageId = mCallback.getId(mUrl);
		mImagePath = mCallback.getPath(mImageId, mUrl);
		mImageCacheId = mCallback.getCacheId(mImageId,mQuality);
	}
	
	void setLocal(){
		isLocal = true;
	}
	
	boolean isLocal(){
		return isLocal;
	}
	
	void addImageCallback(ImageCallback callback){
		synchronized (mImageCallbacks) {
			if(callback != null && !contains(callback)){
				synchronized (mImageCallbacks) {
					mImageCallbacks.add(new WeakReference<ImageCallback>(callback));
				}
			}
		}
	}
	
	private boolean contains(ImageCallback callback){
		synchronized (mImageCallbacks) {
			for (WeakReference<ImageCallback> weakReference : mImageCallbacks) {
				if(weakReference.get() != null && weakReference.get().equals(callback)){
					return true;
				}
			}
			return false;
		}
	}
	
	protected void onSetPriority() {
		mPriority = ++sLastPriority + (200 - mQuality * 10);
		if(sLastPriority == Integer.MAX_VALUE){
			sLastPriority = 0;
		}
	}

	@Override
	public int compareTo(LoadTask another) {
		boolean hasCallback = false;
		synchronized (mImageCallbacks) {
			for (WeakReference<ImageCallback> weakReference : mImageCallbacks) {
				if(weakReference.get() != null){
					hasCallback = true;
					break;
				}
			}
		}
		long priority = hasCallback ? mPriority : mPriority / 2;
		return (int) (another.mPriority - priority);
	}

	@Override
	public void run() {
		try{
			synchronized (LockUtil.getLock(getImageCacheId())) {
				if(isDestroy || TextUtils.isEmpty(getUrl())){
					return;
				}
				mCurrentThread = Thread.currentThread();
				Bitmap bitmap = mCallback.loadImageInCache(getImageCacheId());
				mCallback.onStart(this);
				if(bitmap != null){
					onEnd(bitmap);
				}else{
					if(getUrl().startsWith("http://")){
						bitmap = mCallback.loadImageFromResource(getImageCacheId(),getImagePath(),getUrl(),mQuality);
						if (bitmap == null) {
							String thumbUrl = mCallback.getThumbImageUrl(mUrl);
							String thumbId = mCallback.getId(thumbUrl);
							String thumbCacheId = mCallback.getCacheId(thumbId, mQuality);
							String thumbPath = mCallback.getPath(thumbId, thumbUrl);
							Bitmap thumb = mCallback.loadImageFromResource(null,thumbPath,thumbUrl,mQuality);
							if(thumb != null){
								onImageLoadedInUiThread(thumb, thumbUrl, thumbCacheId);
								LogUtil.e(TAG, "id=" + mImageCacheId + " loadThumb url="+thumbUrl);
							}
							//这里多执行一次loadImageFromResource是为了防止同步锁，锁定期间该图片已经下载完成
							bitmap = mCallback.loadImageFromResource(getImageCacheId(),getImagePath(),getUrl(),mQuality);
							if (bitmap == null) {
								if(!isCancel && !isDestroy){
									mCallback.loadImageFromNetwork(getImageCacheId(),getImagePath(),getUrl(),mQuality,this);
								}
								bitmap = mCallback.loadImageFromResource(getImageCacheId(),getImagePath(),getUrl(),mQuality);
								if(!isCancel && !isDestroy && bitmap == null && ApnUtil.isNetAvailable(getContext())){
									Thread.sleep(1000);
									if(!isCancel){
										mCallback.loadImageFromNetwork(getImageCacheId(),getImagePath(),getUrl(),mQuality,this);
									}
								}
							}
							if(bitmap == null){
								bitmap = mCallback.loadImageFromResource(getImageCacheId(),getImagePath(),getUrl(),mQuality);
							}
						}
						onEnd(bitmap);
					}else{
						bitmap = mCallback.loadImageFromResource(getImageCacheId(),getImagePath(),getUrl(),mQuality);
						onEnd(bitmap);
					}
				}
			}
		}catch (Exception e) {
			LogUtil.e(TAG,e);
			onEnd(null);
		}
	}
	
	private void onEnd(final Bitmap bitmap){
		mCurrentThread = null;
		sHandler.post(new Runnable() {
			@Override
			public void run() {
				onImageLoaded(bitmap, mUrl, mImageCacheId);
				mCallback.onEnd(LoadTask.this,bitmap);
			}
		});
	}
	
	private void onImageLoadedInUiThread(final Bitmap bitmap,final String url,final String imageCacheId){
		sHandler.post(new Runnable() {
			@Override
			public void run() {
				onImageLoaded(bitmap, url, imageCacheId);
			}
		});
	}
	
	private void onImageLoaded(Bitmap bitmap,String url,String imageCacheId){
		if(!isDestroy){
			synchronized (mImageCallbacks) {
				for (WeakReference<ImageCallback> weakReference : mImageCallbacks) {
					if(weakReference.get() != null){
						weakReference.get().imageLoaded(bitmap, url, imageCacheId);
					}
				}
			}
		}
	}
	
	private static Context getContext(){
		return BaseApp.getInstance();
	}

	@Override
	public void onStartLoad(DefaultHttpClient httpClient) {
		if(!isCancel){
			mDefaultHttpClient = httpClient;
		}
	}

	@Override
	public void onDownloadProgress(final String imageUrl,final long p,final long m) {
		sHandler.post(new Runnable() {
			@Override
			public void run() {
				if(isDestroy){
					return;
				}
				synchronized (mImageCallbacks) {
					for (WeakReference<ImageCallback> weakReference : mImageCallbacks) {
						if(weakReference.get() != null){
							weakReference.get().onDownloadProgress(imageUrl, p, m);
						}
					}
				}
			}
		});
	}

	@Override
	public boolean isCancel() {
		return isCancel;
	}
	
	void destroy(){
		if(isDestroy){
			return;
		}
		isDestroy = true;
		cancel();
	}
	
	void cancel(){
		if(isCancel){
			return;
		}
		isCancel = true;
		try {
			if(mDefaultHttpClient != null && mDefaultHttpClient.getConnectionManager() != null){
				mDefaultHttpClient.getConnectionManager().shutdown();
			}
		} catch (Exception e) {}
		if(mCurrentThread != null){
			mCurrentThread.interrupt();
			mCurrentThread = null;
		}
	}
	
	public int getQuality(){
		return mQuality;
	}
	
	public String getUrl(){
		return mUrl;
	}
	
	public String getImageCacheId(){
		return mImageCacheId;
	}
	
	public String getImagePath(){
		return mImagePath;
	}
}
