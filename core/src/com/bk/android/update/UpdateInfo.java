package com.bk.android.update;

import java.io.Serializable;

public class UpdateInfo implements Serializable{
	private static final long serialVersionUID = 5986219580274128566L;
	
	private boolean mustUpdate;//是否需要强制升级，true表达强制升级
	private boolean silenceUpdate;//是否需要静默升级，true表达强制升级
	private String updateMessage;//获取升级最新版本的说明
	private long updateSize;
	private String updateURL;//获取升级文件，这里通常是apk文件
	private String updateVersionName;//对应manifest中 的 versionName
	private String currentVersionName;//当前  versionName
	
	public UpdateInfo(boolean mustUpdate,boolean silenceUpdate, String updateMessage, long updateSize,
			String updateURL, String updateVersionName, String currentVersionName) {
		super();
		this.mustUpdate = mustUpdate;
		this.silenceUpdate = silenceUpdate;
		this.updateMessage = updateMessage;
		this.updateSize = updateSize;
		this.updateURL = updateURL;
		this.updateVersionName = updateVersionName;
		this.currentVersionName = currentVersionName;
	}
	/**
	 * @param mustUpdate the mustUpdate to set
	 */
	void setMustUpdate(boolean mustUpdate) {
		this.mustUpdate = mustUpdate;
	}

	/**
	 * @param updateMessage the updateMessage to set
	 */
	public void setUpdateMessage(String updateMessage) {
		this.updateMessage = updateMessage;
	}

	/**
	 * @param updateSize the updateSize to set
	 */
	void setUpdateSize(int updateSize) {
		this.updateSize = updateSize;
	}

	/**
	 * @param updateURL the updateURL to set
	 */
	void setUpdateURL(String updateURL) {
		this.updateURL = updateURL;
	}

	/**
	 * @param updateVersion the updateVersion to set
	 */
	void setUpdateVersionName(String updateVersion) {
		this.updateVersionName = updateVersion;
	}

	/**
	 * @param currentVersion the currentVersion to set
	 */
	void setCurrentVersionName(String currentVersion) {
		this.currentVersionName = currentVersion;
	}

	/**
	 * @return the mustUpdate
	 */
	public boolean isMustUpdate() {
		return mustUpdate;
	}

	/**
	 * @return the updateMessage
	 */
	public String getUpdateMessage() {
		return updateMessage;
	}

	/**
	 * @return the updateSize
	 */
	public long getUpdateSize() {
		return updateSize;
	}

	/**
	 * @return the updateURL
	 */
	public String getUpdateURL() {
		return updateURL;
	}

	/**
	 * @return the updateVersion
	 */
	public String getUpdateVersionName() {
		return updateVersionName;
	}

	/**
	 * @return the currentVersion
	 */
	public String getCurrentVersionName() {
		return currentVersionName;
	}
	/**
	 * @return the silenceUpdate
	 */
	public boolean isSilenceUpdate() {
		return silenceUpdate;
	}
	/**
	 * @param silenceUpdate the silenceUpdate to set
	 */
	public void setSilenceUpdate(boolean silenceUpdate) {
		this.silenceUpdate = silenceUpdate;
	}
}
