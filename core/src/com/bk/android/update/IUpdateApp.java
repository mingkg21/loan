package com.bk.android.update;

import android.content.Context;
import android.content.IntentFilter;

public interface IUpdateApp{
	/** 
	 * 获取退出应用广播过滤器
	 */
	public IntentFilter getCloseAppIntentFilter();
	
	/**
	 * 取消强制升级，通知退出应用，展示更新界面的Activity同时也会返回ActivityResultCode值是常量RESULT_CODE_EXIT
	 */
	public void exitApp(Context context);
	/**
	 * 清除不在提示下载的设置
	 * @param context
	 */
	public void clearShowUpdateAgain(Context context);
}
