package com.bk.android.update;

import android.app.Notification;
import android.content.Context;
import android.content.Intent;
/**
 * 后台通知回调
 * @author lyw
 *
 */
public interface INotification {
	/**
	 * 设置下载进度状态栏通知
	 * @param updateInfo 下载信息
	 * @param intent 点击状态栏发送的事件
	 * @param currentBytes 当前流大小
	 * @param totalBytes 总大小
	 * @return
	 */
	public Notification fillDownlaodInfoNotification(Context context,UpdateInfo updateInfo,Intent intent,long currentBytes, long totalBytes);
	/**
	 * 设置下载失败状态栏通知
	 * @param updateInfo 下载信息
	 * @param intent 点击状态栏发送的事件
	 * @return
	 */
	public Notification fillDownloadFailNotification(Context context,UpdateInfo updateInfo,Intent intent);
}
