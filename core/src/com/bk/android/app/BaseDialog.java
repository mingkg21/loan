package com.bk.android.app;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;

import com.bk.android.app.BaseActivity.LifeCycleListener;
/**
 * Dialog基类
 * @author linyiwei
 */
public class BaseDialog extends Dialog implements LifeCycleListener{
	public BaseActivity mAbsContextActivity;
	public Activity mOwnerActivity;
	public boolean isFullWidth;
	
	public BaseDialog(Context context) {
		super(context);
		init(context);
	}
	
	protected BaseDialog(Context context, boolean cancelable,
			OnCancelListener cancelListener) {
		super(context, cancelable, cancelListener);
		init(context);
	}
	
	public BaseDialog(Context context, int theme) {
		super(context, theme);
		init(context);
	}
	
	protected void onInit(){}
	
	private void init(Context context){
		if(context instanceof Activity){
			mOwnerActivity = (Activity) context;
		}
		if(context instanceof BaseActivity){
			mAbsContextActivity = (BaseActivity) context;
		}
		setGravity(getDefaultGravity());
		setFullWidth(true);
		onInit();
	}

	public Activity getActivity() {
		if(mOwnerActivity != null){
			return mOwnerActivity;
		}
		if(getContext() instanceof Activity){
			return (Activity) getContext();
		}else if(getContext() instanceof ContextThemeWrapper){
			Context context = ((ContextThemeWrapper) getContext()).getBaseContext();
			if(context instanceof Activity){
				return (Activity) context;
			}
		}
		if(getOwnerActivity() != null){
			return getOwnerActivity();
		}
		return null;
	}
	
	public void setGravity(int gravity){
		Window window = getWindow();    
		window.setGravity(gravity);
	}
	
	public void setFullWidth(boolean fullWidth){
		isFullWidth = fullWidth;
	}
	
	protected int getDefaultGravity(){
		return Gravity.BOTTOM;
	}
	
	private boolean isFinishing(){
		if(getActivity() != null){
			return getActivity().isFinishing();
		}
		return true;
	}
	
	@Override
	public void show() {
		if(isFinishing()){
			return;
		}
		super.show();
		setLayoutParams();
		try {
			if(mAbsContextActivity != null){
				mAbsContextActivity.registerLifeCycleListener(this);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected void setLayoutParams(){
		WindowManager.LayoutParams lp = getWindow().getAttributes();
		if(isFullWidth){
			lp.width = getContext().getResources().getDisplayMetrics().widthPixels;
		}else{
			lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
		}
		getWindow().setAttributes(lp);
	}
	
	@Override
	public void dismiss() {
		super.dismiss();
		if(mAbsContextActivity != null){
			mAbsContextActivity.unregisterLifeCycleListener(this);
		}
	}

	@Override
	public void onActivityDestroy() {
		if(isShowing()){
			super.dismiss();
		}
	}
}
