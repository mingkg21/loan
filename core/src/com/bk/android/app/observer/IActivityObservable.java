package com.bk.android.app.observer;

public interface IActivityObservable {
	/**
	 * 注册回调
	 * @param contextCallBack
	 */
	public void registerActivityObserver(IActivityObserver observer);
	/**
	 * 注销回调
	 * @param contextCallBack
	 */
	public void unregisterActivityObserver(IActivityObserver observer);
	/**
	 * 派遣按返回键事情
	 * @return 如果返回true 表示事件已消耗
	 */
	public boolean dispatchBackPressed();
	
	public boolean dispatchActivityPause();
	
	public boolean dispatchActivityResume();
}
