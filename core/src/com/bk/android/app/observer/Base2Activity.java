package com.bk.android.app.observer;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;

import com.bk.android.app.ActivityManage;
import com.bk.android.app.BaseApp;
import com.bk.android.util.ApnUtil;
import com.bk.android.util.ReleaseUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public abstract class Base2Activity extends AppCompatActivity implements IActivityObservable{
	protected Base2Activity this_ = this;
	private ArrayList<LifeCycleListener> mLifeCycleListeners = new ArrayList<LifeCycleListener>();
	private HashMap<String, BroadcastReceiver> mBroadcastReceivers = new HashMap<String, BroadcastReceiver>();
	private NetWorkListener mNetWorkListener = new NetWorkListener();
	private ActivityObservable mActivityObservable = new ActivityObservable();
	private Boolean isNetAvailable;
	private boolean isFirst;
	
	@Override
	protected void onCreate(Bundle arg0) {
		if(arg0 != null){
			arg0.putParcelable("android:support:fragments",null);
			arg0.putParcelable("android:fragments",null);
		}
		super.onCreate(arg0);
		isFirst = true;
		initBroadcastReceiver();
		isNetAvailable = ApnUtil.isNetAvailable(this_);
		if(!isActionBarEnabled()){
            requestWindowFeature(Window.FEATURE_NO_TITLE);
		}
	}
	
	protected boolean isActionBarEnabled(){
		return false;
	}
	
	@Override
	protected void onDestroy() {
		ArrayList<LifeCycleListener> temp = new ArrayList<LifeCycleListener>(mLifeCycleListeners);
		mLifeCycleListeners.clear();
		for (LifeCycleListener listener : temp) {
			listener.onActivityDestroy();
		}
		ActivityManage.removeActivity(this);
		unregisterAllBroadcastReceiver();
		mActivityObservable.release();
		super.onDestroy();
		try {
			ReleaseUtil.releaseDrawables(getWindow().getDecorView());
			Runtime.getRuntime().gc();
		} catch (Exception e) {}
	}

	@Override
	protected void onResume() {
		super.onResume();
		//后台运行时恢复情况,在onPause保存网络状态，在下一次onResume判断是否变化
		tryDispatchNetworkChange(ApnUtil.isNetAvailable(this_));
		registerBroadcastReceiver(android.net.ConnectivityManager.CONNECTIVITY_ACTION, mNetWorkListener);
		if(isFirst){
			isFirst = false;
		}
		ActivityManage.handlerResume(this);
		dispatchActivityResume();
	}
	
	@Override
	protected void onPause() {
		try {
			super.onPause();
		} catch (Exception e) {
			e.printStackTrace();
		}
		unregisterBroadcastReceiver(android.net.ConnectivityManager.CONNECTIVITY_ACTION);
		isNetAvailable = ApnUtil.isNetAvailable(this_);
		dispatchActivityPause();
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		List<Fragment> fragments = getSupportFragmentManager().getFragments();
		if(fragments != null){
			for (Fragment fragment : fragments) {
				if(fragment != null 
						&& fragment.isAdded() 
						&& fragment.getView() != null 
						&& fragment.getView().getWindowToken() != null
						&& !filterFragmentActivityResult(fragment)){
					fragment.onActivityResult(requestCode, resultCode, data);
				}
			}
		}
	}
	
	protected boolean filterFragmentActivityResult(Fragment fragment){
		return false;
	}
	
	@Override
	public Object getSystemService(String name) {
		if(Context.TELEPHONY_SERVICE.equals(name)){
			return BaseApp.getInstance().getTelephonyManager();
		}
		return super.getSystemService(name);
	}
	
	protected FragmentTransaction beginTransaction(){
		return getSupportFragmentManager().beginTransaction();
	}
	
	private void tryDispatchNetworkChange(boolean isAvailable){
		if(isNetAvailable == null || isNetAvailable != isAvailable){
			isNetAvailable = isAvailable;
			dispatchNetworkChange(isAvailable);
		}
	}
	
	protected abstract void dispatchNetworkChange(boolean isAvailable);

	protected abstract void onInitBroadcastReceiver();
	
	private void initBroadcastReceiver(){
		onInitBroadcastReceiver();
	}
	
	public void registerLifeCycleListener(LifeCycleListener l){
		if(!isFinishing() && l != null && !mLifeCycleListeners.contains(l)){
			mLifeCycleListeners.add(l);
		}
	}
	
	public void unregisterLifeCycleListener(LifeCycleListener l){
		if(l != null){
			mLifeCycleListeners.remove(l);
		}
	}
	
	protected void registerBroadcastReceiver(String action,BroadcastReceiver receiver){
		BroadcastReceiver oldReceiver = mBroadcastReceivers.get(action);
		if(oldReceiver == null){
			mBroadcastReceivers.put(action, receiver);
			super.registerReceiver(receiver, new IntentFilter(action));
		}
	}

	protected void unregisterBroadcastReceiver(String action){
		BroadcastReceiver receiver = mBroadcastReceivers.get(action);
		if(receiver != null){
			mBroadcastReceivers.remove(action);
			super.unregisterReceiver(receiver);
		}
	}
	
	protected void unregisterAllBroadcastReceiver(){
		for(BroadcastReceiver receiver: mBroadcastReceivers.values()){
			super.unregisterReceiver(receiver);
		}
		mBroadcastReceivers.clear();
	}
	
	@Override
	public void registerActivityObserver(IActivityObserver observer) {
		mActivityObservable.registerActivityObserver(observer);
	}

	@Override
	public void unregisterActivityObserver(IActivityObserver observer) {
		mActivityObservable.unregisterActivityObserver(observer);
	}

	@Override
	public boolean dispatchBackPressed() {
		return mActivityObservable.dispatchBackPressed();
	}
	
	@Override
	public boolean dispatchActivityResume() {
		return mActivityObservable.dispatchActivityResume();
	}
	
	@Override
	public boolean dispatchActivityPause() {
		return mActivityObservable.dispatchActivityPause();
	}
	
	@Override
	public void onBackPressed() {
		if(!dispatchBackPressed()){
			try {
				super.onBackPressed();
			} catch (Exception e) {}
		}
	}
	
	private class NetWorkListener extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			tryDispatchNetworkChange(ApnUtil.isNetAvailable(this_));
		}  
	}
	
	public interface LifeCycleListener{
		public void onActivityDestroy();
	}
}
