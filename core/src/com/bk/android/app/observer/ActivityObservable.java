package com.bk.android.app.observer;

import java.util.ArrayList;
import java.util.List;


public class ActivityObservable extends AbsObservable<IActivityObserver> implements IActivityObservable{
	@Override
	public void registerActivityObserver(IActivityObserver observer) {
		registerObserver(observer);
	}

	@Override
	public void unregisterActivityObserver(IActivityObserver observer) {
		unregisterObserver(observer);
	}
	
	@Override
	public boolean dispatchBackPressed() {
		boolean isHandler = false;
		List<IActivityObserver> list = new ArrayList<IActivityObserver>(getObservers());
		for(IActivityObserver observer : list){
			isHandler = observer.onBackPressed();
			if(isHandler){
				break;
			}
		}
		return isHandler;
	}
	
	@Override
	public boolean dispatchActivityResume() {
		boolean isHandler = false;
		List<IActivityObserver> list = new ArrayList<IActivityObserver>(getObservers());
		for(IActivityObserver observer : list){
			isHandler = observer.onActivityResume();
			if(isHandler){
				break;
			}
		}
		return isHandler;
	}
	
	@Override
	public boolean dispatchActivityPause() {
		boolean isHandler = false;
		List<IActivityObserver> list = new ArrayList<IActivityObserver>(getObservers());
		for(IActivityObserver observer : list){
			isHandler = observer.onActivityPause();
			if(isHandler){
				break;
			}
		}
		return isHandler;
	}
}
