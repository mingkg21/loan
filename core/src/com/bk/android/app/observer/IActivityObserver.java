package com.bk.android.app.observer;


public interface IActivityObserver {
	/**
	 * 传递 Activity 的 onBackPressed事件
	 * @return
	 */
	public boolean onBackPressed();

	public boolean onActivityResume();

	public boolean onActivityPause();
}
