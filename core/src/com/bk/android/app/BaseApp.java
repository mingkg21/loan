package com.bk.android.app;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import com.bk.android.util.AppUtil;
import com.bk.android.util.LogUtil;

import java.io.File;

import gueei.binding.Binder;

/** 基础的application类
 * 处理程序崩溃日志记录（目录是SDCARD/程序目录/crash.txt），程序日志记录（SDCARD/程序目录/log.txt）
 * @author lyw
 * @date 2014-1-10
 */
public abstract class BaseApp extends Application {
	private static BaseApp instance;
	
	private TelephonyManager mTelephonyManager;
	private boolean isEnterApp = false;
	private Handler mHandler;

	/**
	 * 获取应用程序App实例
	 * @return
	 */
	public static BaseApp getInstance(){
		return instance;
	}
	
	/**
	 * 获取全局主线程Handler
	 * @return
	 */
	public static Handler getHandler(){
		return instance.mHandler;
	}
	
	/**
	 * 运行在UI线程
	 * @param action
	 */
	public static void runOnUiThread(Runnable action) {
        if (Thread.currentThread() != instance.mHandler.getLooper().getThread()) {
        	instance.mHandler.post(action);
        } else {
            action.run();
        }
    }
	
	@Override
	public void onCreate() {
		super.onCreate();
		Binder.init(this);
		mHandler = new Handler(Looper.getMainLooper());
		instance = this;
		//处理程序崩溃日志记录
		catchError();
		//处理程序日志
		dealLog();
		mTelephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
	}
	
	public TelephonyManager getTelephonyManager(){
		return mTelephonyManager;
	}
	/**
	 *  异常扑捉的处理，记录日志
	 */
	private void catchError() {
		String fileDir = getAppSdcardDir();
		if(!TextUtils.isEmpty(fileDir)){
			new CrashCatchHandler(fileDir);
		}
	}
	/** 设置日志的输出到SDCARD和是否在LOGCAT打印
	 * 
	 */
	private void dealLog(){
		String TAG = getPackageName();
		String logFile = "";
		if (isWriteLogToSdcard()) {
			logFile = getLogFile();
		}
		LogUtil.init(TAG, isDebug(), logFile);
	}

	public String getLogFile() {
		String logDir = getAppSdcardDir() + "/log";
		File file = new File(logDir);
		if (!file.exists()) {
			file.mkdirs();
		}
		return logDir + "/log.txt";
	}
	
	public final void saveInstanceState(Bundle outState) {
		outState.putBoolean("isEnterApp", isEnterApp);
		onSaveInstanceState(outState);
	}
	
	public final void restoreInstanceState(Bundle inState) {
		isEnterApp = inState.getBoolean("isEnterApp",false);
		onRestoreInstanceState(inState);
	}
	
	
	protected void onSaveInstanceState(Bundle outState) {}
	protected void onRestoreInstanceState(Bundle inState) {}
	
	public final boolean isEnterApp(){
		return isEnterApp;
	}
	
	public final void handleEnterApp(Activity activity,Bundle inState){
		if(inState != null){
			restoreInstanceState(inState);
		}
		if(!isEnterApp && isCurrentProcess(this)){
			isEnterApp = true;
			onEnterApp(activity);
		}
	}
	
	public final void handleExitApp(){
		if(isEnterApp && isCurrentProcess(this)){
			isEnterApp = false;
			onExitApp();
		}
	}
	/** 提交给服务器的客户端版本号
	 * @return
	 */
	public int getApkVersionCode(){
		return AppUtil.getApkVersionCode(this);
	}
	/** 设置程序在SDCARD的目录
	 * @return
	 */
	public abstract String getAppSdcardDir();
	/** 设置是否日志写入SDCARD
	 * @return
	 */
	public abstract boolean isWriteLogToSdcard();
	/** 设置是否DEBUG模式
	 * @return
	 */
	public abstract boolean isDebug();
	/** 设置是否正式服务器，true为正式服务器，false为测试服务器
	 * @return
	 */
	public abstract boolean isNormalServer();
	/** 设置刷量统计类型
	 * @return
	 */
	public abstract String getBrushType();
	/**
	 * 进入应用程序的回调
	 */
	public abstract void onEnterApp(Activity activity);
	/**
	 * 退出应用程序的回调
	 */
	public abstract void onExitApp();
	
	public static boolean isCurrentProcess(Context context) {
		return isProcess(context,context.getPackageName());
	}
	
	public static boolean isProcess(Context context,String processName) {
		int pid = android.os.Process.myPid();
		ActivityManager activityMag = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		for (RunningAppProcessInfo appProcess : activityMag.getRunningAppProcesses()) {
			if (appProcess.pid == pid 
					&& appProcess.processName.equalsIgnoreCase(processName)) {
				return true;
			}
		}
		return false;
	}
}
