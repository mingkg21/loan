package com.bk.android.ui.widget.read;

import android.graphics.Canvas;
import android.graphics.Picture;
import android.os.SystemClock;

public class PagePicture extends Picture implements IPagePicture{
	private String mPageId;
	private int mState;
	private long mCreateCanvaTime;
	private long mDrawTime;
	
	public PagePicture(String pageId, int state){
		init(pageId, state);
	}
	
	@Override
	public void init(String pageId, int state){
		mPageId = pageId;
		mState = state;
	}
	
	@Override
	public void release() {}
	
	@Override
	public boolean equals(String pageId) {
		return mPageId != null && mPageId.equals(pageId);
	}

	@Override
	public Canvas getCanvas(int width, int height) {
		mCreateCanvaTime = SystemClock.uptimeMillis();
		mDrawTime = 0;
		return super.beginRecording(width, height);
	}

	@Override
	public void onDraw(Canvas canvas) {
		super.draw(canvas);
		if(mDrawTime == 0){
			mDrawTime = SystemClock.uptimeMillis();
		}
	}

	@Override
	public int getDrawState() {
		return mState;
	}
	
	@Override
	public long getCreateCanvaTime(){
		return mCreateCanvaTime;
	}
	
	@Override
	public long getDrawTime(){
		return mDrawTime;
	}

	@Override
	public void clearContent() {
	}
}

