package com.bk.android.ui.widget.read;

import android.graphics.Canvas;

public interface IPagePicture {
	public void init(String pageId, int state);

	public boolean equals(String pageId);
	
	public Canvas getCanvas(int width, int height);

	public void onDraw(Canvas canvas);
	
	public void release();
	
	public int getDrawState();
	
	public long getCreateCanvaTime();
	
	public long getDrawTime();
	
	public void clearContent();
}

