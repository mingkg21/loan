package com.bk.android.ui.widget.read;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.PorterDuff.Mode;
import android.os.SystemClock;

import com.bk.android.util.LogUtil;

public class PageBitmapPicture implements IPagePicture{
	private String mPageId;
	private Bitmap mBitmap;
	private Canvas mCanvas;
	private int mState;
	private long mCreateCanvaTime;
	private long mDrawTime;
	private Config mConfig;
	
	public PageBitmapPicture(String pageId, int state, Config config){
		mConfig = config;
		init(pageId, state);
	}
	
	@Override
	public void init(String pageId, int state){
		mPageId = pageId;
		mState = state;
	}

	@Override
	public void release() {
		if(mBitmap != null) {
			LogUtil.i("PageBitmapPicture", "<init> release bitmap");
			mBitmap.recycle();
			mBitmap = null;
			mCanvas = null;
		}
	}
	
	@Override
	public Canvas getCanvas(int width, int height) {
//		synchronized (this) {
			if(mBitmap == null || mBitmap.getWidth() != width || mBitmap.getHeight() != height){
				mBitmap = Bitmap.createBitmap(width, height, mConfig);
				if(mBitmap != null){
					mCanvas = new Canvas(mBitmap);
				}
			}
			mCreateCanvaTime = SystemClock.uptimeMillis();
			mDrawTime = 0;
			return mCanvas;
//		}
	}
	
	public void onDraw(Canvas canvas) {
//		synchronized (this) {
			if(mBitmap != null){
				canvas.drawBitmap(mBitmap, 0, 0, null);
				mDrawTime = SystemClock.uptimeMillis();
			}
//		}
	}

	@Override
	public boolean equals(String pageId) {
		return mPageId != null && mPageId.equals(pageId);
	}

	@Override
	public int getDrawState() {
		return mState;
	}
	
	@Override
	public long getCreateCanvaTime(){
		return mCreateCanvaTime;
	}
	
	@Override
	public long getDrawTime(){
		return mDrawTime;
	}
	
	@Override
	public String toString() {
		return "id:" + mPageId + " s:" + mState + " ct:" + mCreateCanvaTime + " dt:" + mDrawTime;
	}

	@Override
	public void clearContent() {
		synchronized (this) {
			if(mCanvas != null){
				mCanvas.drawColor(0,Mode.CLEAR);
			}
		}
	}
}
