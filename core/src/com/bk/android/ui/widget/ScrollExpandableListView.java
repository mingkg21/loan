package com.bk.android.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ExpandableListView;

/** 在ScrollView中能显示的ListdView
 * @author mingkg21
 * @date 2011-7-31
 * @email mingkg21@gmail.com
 */
public class ScrollExpandableListView extends ExpandableListView {
	public ScrollExpandableListView(Context context) {
		super(context);
	}

	public ScrollExpandableListView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public ScrollExpandableListView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2, MeasureSpec.AT_MOST);
		super.onMeasure(widthMeasureSpec, expandSpec);
	}
	
	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		try {
			super.onLayout(changed, l, t, r, b);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
