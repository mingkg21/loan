package com.bk.android.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ScrollView;

public class ScrollViewCanListen extends ScrollView {
	public ScrollChangedListener mScrollChangedListener;
	
	public ScrollViewCanListen(Context context) {
		super(context);
	}
	
	public ScrollViewCanListen(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	public ScrollViewCanListen(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	protected void onScrollChanged(int l, int t, int oldl, int oldt) {
		super.onScrollChanged(l, t, oldl, oldt);
		if(mScrollChangedListener != null){
			mScrollChangedListener.onScrollChanged(this, l, t, oldl, oldt);
		}
	}
	
	public void setScrollChangedListener(ScrollChangedListener l){
		mScrollChangedListener = l;
	}
	
	public interface ScrollChangedListener{
		public void onScrollChanged(ScrollView scrollView,int l, int t, int oldl, int oldt);

	}
}
