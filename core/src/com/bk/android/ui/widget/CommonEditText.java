package com.bk.android.ui.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.inputmethod.InputMethodManager;

public class CommonEditText extends PatchedEditText{
	private boolean mRequestInput;
	
	public CommonEditText(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onFocusChanged(boolean focused, int direction,Rect previouslyFocusedRect) {
		super.onFocusChanged(focused, direction, previouslyFocusedRect);
		if(focused && getText() != null && getSelectionStart() == 0 && getSelectionEnd() == 0){
			setSelection(getText().length());
		}
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		if(mRequestInput){
			mRequestInput = false;
			InputMethodManager im = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            im.showSoftInput(this, 0);
		}
	}

	public void requestInput(boolean newValue) {
		mRequestInput = newValue;
		invalidate();
	}
}
