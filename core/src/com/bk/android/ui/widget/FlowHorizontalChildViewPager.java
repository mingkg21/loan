package com.bk.android.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

import com.bk.android.ui.widget.viewflow.FlowIndicator;
import com.bk.android.ui.widget.viewflow.IViewFlow;
import com.bk.android.ui.widget.viewpager.HorizontalChildViewPager;

public class FlowHorizontalChildViewPager extends HorizontalChildViewPager implements IViewFlow{
	private FlowIndicator mIndicator;
	private OnPageChangeListener mOnPageChangeListener;

	public FlowHorizontalChildViewPager(Context context) {
		super(context);
		init();
	}
	
	public FlowHorizontalChildViewPager(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	private void init(){
		super.setOnPageChangeListener(new OnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {
				if(mOnPageChangeListener != null){
					mOnPageChangeListener.onPageSelected(position);
				}
				if(mIndicator != null){
					mIndicator.onSwitched(position);
				}
			}
			
			@Override
			public void onPageScrolled(int position, float positionOffset,
					int positionOffsetPixels) {
				if(mOnPageChangeListener != null){
					mOnPageChangeListener.onPageScrolled(position, positionOffset, positionOffsetPixels);
				}
				if(mIndicator != null){
					int hPerceived = positionOffsetPixels + getChildWidth() * position;
					mIndicator.onScrolled(hPerceived, 0, hPerceived, 0);
				}
			}
			
			@Override
			public void onPageScrollStateChanged(int state) {
				if(mOnPageChangeListener != null){
					mOnPageChangeListener.onPageScrollStateChanged(state);
				}
			}
		});
	}
	@Override
	public void requestLayout() {
		super.requestLayout();
		if(mIndicator != null){
			mIndicator.onSwitched(getCurrentItem());
		}
	}

	@Override
	public void setOnPageChangeListener(OnPageChangeListener listener) {
        mOnPageChangeListener = listener;
    }

	@Override 
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		if(mIndicator == null){
			findFlowIndicator((ViewGroup) getParent());
		}
	}
	
	
	private void findFlowIndicator(ViewGroup viewGroup){
		for (int i = 0; i < viewGroup.getChildCount(); i++) {
			View view = viewGroup.getChildAt(i);
			if(view instanceof FlowIndicator){
				setFlowIndicator((FlowIndicator) view);
				return;
			}else if(view instanceof ViewGroup){
				if(!view.equals(this)){
					findFlowIndicator((ViewGroup) view);
				}
			}
		}
	}
	
	private int getWidthPadding() {
		return getPaddingLeft() + getPaddingRight() + getHorizontalFadingEdgeLength() * 2;
	}
	
	@Override
	public int getChildWidth() {
		return getWidth() - getWidthPadding();
	}

	@Override
	public int getSelectedItemPosition() {
		return getCurrentItem();
	}

	@Override
	public int getCount() {
		return getAdapter() != null ? getAdapter().getCount() : 0;
	}
	
	public void setFlowIndicator(FlowIndicator flowIndicator) {
		mIndicator = flowIndicator;
		mIndicator.setViewFlow(this);
		int hPerceived = getChildWidth() * getCurrentItem();
		mIndicator.onScrolled(hPerceived, 0, hPerceived, 0);
	}
}
