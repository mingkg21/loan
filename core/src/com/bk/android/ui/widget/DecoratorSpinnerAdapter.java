package com.bk.android.ui.widget;

import android.view.View;
import android.view.ViewGroup;
import android.widget.SpinnerAdapter;


public class DecoratorSpinnerAdapter extends DecoratorAdapter<SpinnerAdapter> {
	public DecoratorSpinnerAdapter(SpinnerAdapter adapter) {
		super(adapter);
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		if(mAdapter == null){
			return super.getDropDownView(position, convertView, parent);
		}
		return mAdapter.getDropDownView(position, convertView, parent);
	}
}
