package com.bk.android.ui.widget;

import android.view.View;

public interface ScrollChildView {
	/**
	 * 
	 * @param viewPager
	 * @param dx
	 * @param x
	 * @param y
	 * @return true 是子View 可以滚动
	 */
	public boolean canScroll(View view,int dx,int dy, int x, int y);
}
