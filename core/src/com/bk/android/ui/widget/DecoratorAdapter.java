package com.bk.android.ui.widget;

import android.database.DataSetObserver;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.BaseAdapter;

public class DecoratorAdapter<T extends Adapter> extends BaseAdapter {
	protected T mAdapter;
	private MyDataSetObserver mObserver;
	private DataChangedListener mDataChangedListener;
	public DecoratorAdapter(T adapter){
		mObserver = new MyDataSetObserver();
		setAdapter(adapter);
	}
	
	public T getAdapter(){
		return mAdapter;
	}
	
	public void setAdapter(T adapter){
		if(mAdapter != null){
			mAdapter.unregisterDataSetObserver(mObserver);
		}
		mAdapter = adapter;
		if(mAdapter != null){
			mAdapter.registerDataSetObserver(mObserver);
		}
		notifyDataSetChanged();
	}
	
	@Override
	public boolean hasStableIds() {
		if(mAdapter == null){
			return super.hasStableIds();
		}
		return mAdapter.hasStableIds();
	}
	
	@Override
	public int getItemViewType(int position) {
		if(mAdapter == null){
			return super.getItemViewType(position);
		}
		return mAdapter.getItemViewType(position);
	}

	@Override
	public int getViewTypeCount() {
		if(mAdapter == null){
			return super.getViewTypeCount();
		}
		return mAdapter.getViewTypeCount();
	}

	@Override
	public boolean isEmpty() {
		if(mAdapter == null){
			return super.isEmpty();
		}
		return mAdapter.isEmpty();
	}

	@Override
	public int getCount() {
		if(mAdapter == null){
			return 0;
		}
		return mAdapter.getCount();
	}

	@Override
	public Object getItem(int position) {
		if(mAdapter == null){
			return null;
		}
		return mAdapter.getItem(position);
	}

	@Override
	public long getItemId(int position) {
		if(mAdapter == null){
			return position;
		}
		return mAdapter.getItemId(position);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if(mAdapter == null){
			return null;
		}
		return mAdapter.getView(position, convertView, parent);
	}
	
	private class MyDataSetObserver extends DataSetObserver{
		@Override
		public void onChanged() {
			super.onChanged();
			notifyDataSetChanged();
			if(mDataChangedListener != null){
				mDataChangedListener.onChanged();
			}
		}

		@Override
		public void onInvalidated() {
			super.onInvalidated();
			notifyDataSetInvalidated();
			if(mDataChangedListener != null){
				mDataChangedListener.onChanged();
			}
		}
	}
	
	public DecoratorAdapter<T> setDataChangedListener(DataChangedListener l){
		mDataChangedListener = l;
		return this;
	}
	
	public interface DataChangedListener{
		public void onChanged();
		public void onInvalidated();
	}
}
