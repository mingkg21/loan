package com.bk.android.ui.widget.viewflow;

public interface IViewFlow {
	public int getChildWidth();

	public int getSelectedItemPosition();

	public int getCount();
}
