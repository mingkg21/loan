package com.bk.android.ui.widget;

import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.TextView;

import com.bk.android.ui.widget.AbsDropDownListView.OnShowListener;
import com.bk.android.util.DimensionsUtil;

public abstract class AbsSpinner extends AdapterView<ListAdapter> implements OnDismissListener,OnShowListener,
						OnItemClickListener{
	private OnClickListener mOnClickListener;
	private AbsDropDownListView dropDownListView;
	private OnDismissListener mOnDismissListener;
	private OnShowListener mOnShowListener;
	private Adapter mAdapter;
	protected LayoutInflater mLayoutInflater;
	private View mConvertView;
	private int mSelection;
	
	public AbsSpinner(Context context) {
		super(context);
		init(context);
	}
	
	public AbsSpinner(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}
	
	public AbsSpinner(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context);
	}
	
	@SuppressWarnings("deprecation")
	protected void init(Context context){
		mLayoutInflater = LayoutInflater.from(getContext());
		setClickable(true);
		setFocusable(true);
		dropDownListView = newDropDownListView();
		dropDownListView.setOnDismissListener(this);
		dropDownListView.setOnShowListener(this);
		dropDownListView.setOnItemClickListener(this);
		mAdapter = new Adapter();
		dropDownListView.setAdapter(mAdapter);
		onInit();
		Drawable drawable = getDefaultBackgroundDrawable();
		if(drawable != null){
			setBackgroundDrawable(drawable);
		}
		updataContentView(-1);
	}

	@Override
	public void setOnClickListener(OnClickListener l) {
		mOnClickListener = l;
	}

	@Override
	public boolean performClick() {
		if(mOnClickListener != null){
			mOnClickListener.onClick(this);
		}
		if(!dropDownListView.isShowing()){
			dropDownListView.showDropDown();
		}else{
			dropDownListView.dismissDropDown();
		}
		return super.performClick();
	}

	protected void updataDropDown(){
		if(dropDownListView.isShowing()){
			dropDownListView.showDropDown();
		}
	}
	
	public void setOnDismissListener(OnDismissListener onDismissListener){
		mOnDismissListener = onDismissListener;
	}
	
	public void setOnShowListener(OnShowListener onShowListener){
		mOnShowListener = onShowListener;
	}
	
	@Override
	public ListAdapter getAdapter() {
		return mAdapter;
	}

	@Override
	public void setAdapter(ListAdapter listAdapter){
		mConvertView = null;
		mAdapter.setAdapter(listAdapter);
		updataContentView(-1);
	}
	
	public int getItemCount() {
		return dropDownListView.getItemCount();
	}
	
	@Override
	public View getSelectedView() {
		return mConvertView;
	}

	@Override
	public void setSelection(int position) {
		if(position >= 0 && position < dropDownListView.getItemCount() && mSelection != position){
			dropDownListView.setItemChecked(position, true);
			updataContentView(position);
			if(getOnItemSelectedListener() != null){
				getOnItemSelectedListener().onItemSelected(this, mConvertView, position, getItemIdAtPosition(position));
			}
		}
	}
	
	public Object getItem(int position){
		return mAdapter.getItem(position);
	}
	
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,long id) {
		setSelection(position);
		if(getOnItemClickListener() != null){
			getOnItemClickListener().onItemClick(parent, view, position, id);
		}
	}
	
	@Override
	public void onShow() {
		mAdapter.notifyDataSetChanged();
		if(mOnShowListener != null){
			mOnShowListener.onShow();
		}
	}

	@Override
	public void onDismiss() {
		if(mOnDismissListener != null){
			mOnDismissListener.onDismiss();
		}
	}
	
	@Override
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		int childCount = getChildCount();
		for(int i = 0;i < childCount;i++){
			View child = getChildAt(i);
			if(child != null){
				child.setEnabled(isEnabled());
			}
		}
	}
	
	@Override
	protected void onLayout(boolean changed, int left, int top, int right,
			int bottom) {
		super.onLayout(changed, left, top, right, bottom);
		if(getChildCount() > 0){
    		View child = getChildAt(0);
    		child.layout(left + getPaddingLeft(), top + getPaddingTop(), right - getPaddingRight(), bottom - getPaddingBottom());
    	}
	}
	
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    	super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    	if(getChildCount() > 0){
    		View child = getChildAt(0);
			measureChild(child,MeasureSpec.makeMeasureSpec(getMeasuredWidth(), MeasureSpec.AT_MOST)
					,MeasureSpec.makeMeasureSpec(getMeasuredHeight(), MeasureSpec.AT_MOST));
			setMeasuredDimension(child.getMeasuredWidth() + getPaddingLeft() + getPaddingRight()
					, child.getMeasuredHeight() + getPaddingTop() + getPaddingBottom());
    	}
    }
	
    private final void updataContentView(int checkedPosition){
		mSelection = checkedPosition;
    	if(!isShowSelectionView()){
    		checkedPosition = -1;
    	}
		if(mConvertView == null){
			if(getChildCount() > 0){
				super.removeAllViewsInLayout();
			}
			View contentView = onCreateContentView(checkedPosition);
			if(mAdapter.getAdapter() != null && checkedPosition >= 0){
				mConvertView = contentView;
			}
			if(contentView != null){
				ViewGroup.LayoutParams params = contentView.getLayoutParams();
		        if (params == null) {
		            params = generateDefaultLayoutParams();
		            if (params == null) {
		                throw new IllegalArgumentException("generateDefaultLayoutParams() cannot return null");
		            }
		        }
				addViewInLayout(contentView, -1, params);
				requestLayout();
			}
		}
		if(mConvertView != null){
			onUpdataContentView(checkedPosition,mConvertView);
		}
	}
	
	protected void performListClick(int position) {
		onItemClick(dropDownListView.getAdapterView(), mAdapter.getView(position,null,null), position, position);
	}
	/**
	 * 未选checkedPosition 为-1
	 * @param checkedPosition
	 * @return
	 */
	protected View onCreateContentView(int checkedPosition){
		return mAdapter.getView(checkedPosition,null,null);
	}
	
	protected void onUpdataContentView(int checkedPosition,View convertView){
		mAdapter.getView(checkedPosition,convertView,null);
		convertView.setEnabled(isEnabled());
	}
	
	protected boolean isShowSelectionView(){
		return true;
	}

	protected abstract void onInit();
		
	protected abstract View getDefaultEmptyView();
	
	protected abstract Drawable getDefaultBackgroundDrawable();
	
	protected abstract AbsDropDownListView newDropDownListView();
	
	@Override
	final protected boolean addViewInLayout(View child, int index, ViewGroup.LayoutParams params) {
		return super.addViewInLayout(child, index, params);
	}

	@Override
	final protected boolean addViewInLayout(View child, int index,
			ViewGroup.LayoutParams params, boolean preventRequestLayout) {
		return super.addViewInLayout(child, index, params, preventRequestLayout);
	}
	
	private class Adapter extends BaseAdapter{
		private ListAdapter mAdapter;
		private MyDataSetObserver mObserver;
		
		public Adapter(){
			mObserver = new MyDataSetObserver();
		}
		
		public void setAdapter(ListAdapter listAdapter){
			if(mAdapter != null){
				mAdapter.unregisterDataSetObserver(mObserver);
			}
			mAdapter = listAdapter;
			if(mAdapter != null){
				mAdapter.registerDataSetObserver(mObserver);
			}
			notifyDataSetChanged();
		}
		
		@Override
		public int getCount() {
			if(mAdapter != null){
				return mAdapter.getCount();
			}
			return 1;
		}

		@Override
		public Object getItem(int position) {
			if(mAdapter != null){
				return mAdapter.getItem(position);
			}
			return null;
		}

		@Override
		public long getItemId(int position) {
			if(mAdapter != null){
				return mAdapter.getItemId(position);
			}
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if(mAdapter != null && position >= 0){
				if(convertView != null && this.equals(convertView.getTag())){
					convertView = null;
				}
				convertView = mAdapter.getView(position, convertView, parent);
				return convertView;
			}
			if(convertView != null && !this.equals(convertView.getTag())){
				convertView = null;
			}
			if(convertView == null){
				convertView = getDefaultEmptyView();
			}
			if(convertView == null){
				TextView textView = new TextView(getContext());
				textView.setLayoutParams(new AbsListView.LayoutParams(android.view.ViewGroup.LayoutParams.WRAP_CONTENT, android.view.ViewGroup.LayoutParams.WRAP_CONTENT));
				textView.setPadding(DimensionsUtil.DIPToPX(10), DimensionsUtil.DIPToPX(5)
						, DimensionsUtil.DIPToPX(10), DimensionsUtil.DIPToPX(5));
				textView.setText("  ");
				convertView = textView;
			}
			if(convertView != null){
				convertView.setTag(this);
			}
			return convertView;
		}
		
		@Override
		public void notifyDataSetChanged() {
			super.notifyDataSetChanged();
			updataContentView(dropDownListView.getCheckedItemPosition());
		}

		private ListAdapter getAdapter(){
			return mAdapter;
		}

		private class MyDataSetObserver extends DataSetObserver{
			@Override
			public void onChanged() {
				super.onChanged();
				notifyDataSetChanged();
			}

			@Override
			public void onInvalidated() {
				super.onInvalidated();
				notifyDataSetInvalidated();
			}
		}
	}
}
