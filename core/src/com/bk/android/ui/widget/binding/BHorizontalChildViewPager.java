package com.bk.android.ui.widget.binding;

import gueei.binding.Binder;
import gueei.binding.Binder.InflateResult;
import gueei.binding.BindingType;
import gueei.binding.CollectionChangedEventArg;
import gueei.binding.CollectionObserver;
import gueei.binding.IBindableView;
import gueei.binding.IObservableCollection;
import gueei.binding.Utility;
import gueei.binding.ViewAttribute;
import gueei.binding.collections.ArrayListObservable;
import gueei.binding.listeners.ViewMulticastListener;
import gueei.binding.viewAttributes.ViewEventAttribute;
import gueei.binding.viewAttributes.templates.Layout;
import gueei.binding.viewAttributes.templates.SingleTemplateLayout;

import java.util.Collection;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

import com.bk.android.ui.widget.FlowHorizontalChildViewPager;
import com.bk.android.ui.widget.viewpager.AbsViewPagerAdapter;

public class BHorizontalChildViewPager extends FlowHorizontalChildViewPager implements IBindableView<BHorizontalChildViewPager>
	, CollectionObserver{
	public BHorizontalChildViewPager(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	private Layout mItemTemplateView;
	private ArrayListObservable<?> mItemSource;
	private InteriorViewPagerAdapter mViewPagerAdapter;
	
	@Override
	public ViewAttribute<? extends View, ?> createViewAttribute(String attributeId) {
		if (attributeId.equals("itemTemplate")) {
			return new ItemTemplateViewAttribute(this,attributeId);
		} else if (attributeId.equals("itemSource")) {
			return new ItemSourceAttribute(this,attributeId);
		} else if (attributeId.equals("selectedItem")) {
			return new SelectedItemAttribute(this,attributeId);
		} else if (attributeId.equals("onPagerSelected")) {
			return new OnViewPagerSelectedAttribute(this,attributeId);
		}
		return null;
	}


	@Override
	public void onCollectionChanged(IObservableCollection<?> collection,
			CollectionChangedEventArg args, Collection<Object> initiators) {
		if(mViewPagerAdapter != null){
			int currentItem = getCurrentItem();
			if(currentItem > getCount() - 1){
				currentItem = getCount() - 1;
			}
			setAdapter(mViewPagerAdapter);
			setCurrentItem(currentItem);
			clearDisappearingChildren();
		}
	}
	
	public static class OnViewPagerSelectedAttribute  extends ViewEventAttribute<ViewPager> implements OnPageChangeListener{
		public OnViewPagerSelectedAttribute(BHorizontalChildViewPager view,String attributeName) {
			super(view, attributeName);
		}

		@Override
		public void onPageScrolled(int position, float positionOffset,int positionOffsetPixels) {}

		@Override
		public void onPageSelected(int position) {
			this.invokeCommand(getView(),position);
		}

		@Override
		public void onPageScrollStateChanged(int state) {}

		@Override
		protected void registerToListener(ViewPager view) {
			Binder.getMulticastListenerForView(view, OnPageChangeListenerMulticast.class).register(this);
		}
	}
	
	public static class OnPageChangeListenerMulticast extends ViewMulticastListener<OnPageChangeListener> implements OnPageChangeListener{
		@Override
		public void registerToView(View v) {
			if (!(v instanceof ViewPager)) return;
			((ViewPager)v).setOnPageChangeListener(this);
		}

		@Override
		public void onPageScrolled(int position, float positionOffset,int positionOffsetPixels) {
			for(OnPageChangeListener l:listeners){
				l.onPageScrolled(position, positionOffset, positionOffsetPixels);
			}
		}

		@Override
		public void onPageSelected(int position) {
			for(OnPageChangeListener l:listeners){
				l.onPageSelected(position);
			}
		}

		@Override
		public void onPageScrollStateChanged(int state) {
			for(OnPageChangeListener l:listeners){
				l.onPageScrollStateChanged(state);
			}
		}
	}
	
	public static class SelectedItemAttribute extends ViewAttribute<BHorizontalChildViewPager, Integer> implements OnPageChangeListener {
		public SelectedItemAttribute(BHorizontalChildViewPager view,String attributeName) {
			super(Integer.class, view, attributeName);
			Binder.getMulticastListenerForView(view, OnPageChangeListenerMulticast.class)
			.registerWithHighPriority(this);
		}
		
		@Override
		public Integer get() {
			return getView().getCurrentItem();
		}
		
		@Override
		protected void doSetAttributeValue(Object newValue) {
			if (newValue instanceof Integer) {
				getView().setCurrentItem((Integer) newValue);
			}
		}

		@Override
		public void onPageScrolled(int position, float positionOffset,int positionOffsetPixels) {
			
		}

		@Override
		public void onPageSelected(int position) {
			this.notifyChanged();
		}

		@Override
		public void onPageScrollStateChanged(int state) {
		}
	}
	
	public class ItemTemplateViewAttribute extends ViewAttribute<BHorizontalChildViewPager, Layout> {
		public ItemTemplateViewAttribute(BHorizontalChildViewPager view,
				String attributeName) {
			super(Layout.class, view, attributeName);
		}

		@Override
		protected void doSetAttributeValue(Object newValue) {
			if(getView()==null) return;
			if (newValue instanceof Layout){
				mItemTemplateView = (Layout)newValue;
			}else if (newValue instanceof Integer){
				if ((Integer)newValue>0)
					mItemTemplateView = new SingleTemplateLayout((Integer)newValue);
			}else if (newValue instanceof CharSequence){
				int value = Utility.resolveLayoutResource(newValue.toString(), getView().getContext());
				if (value>0){
					mItemTemplateView = new SingleTemplateLayout((Integer)newValue);
				}
			}
			fillDate();
		}

		@Override
		public Layout get() {
			return mItemTemplateView;
		}

		@Override
		protected BindingType AcceptThisTypeAs(Class<?> type) {
			if (Integer.class.isAssignableFrom(type) ||
				CharSequence.class.isAssignableFrom(type)) 
				return BindingType.OneWay;
			if (Layout.class.isAssignableFrom(type)) return BindingType.TwoWay;
			return BindingType.NoBinding;
		}
	}
	
	public class ItemSourceAttribute extends ViewAttribute<BHorizontalChildViewPager,Object> {
		public ItemSourceAttribute(BHorizontalChildViewPager view,
				String attributeName) {
			super(Object.class, view, attributeName);
		}

		@Override
		protected void doSetAttributeValue(Object newValue) {
			if(getView()==null) return;
			if(newValue instanceof ArrayListObservable<?>){
				mItemSource = (ArrayListObservable<?>) newValue;
			}
			fillDate();
		}

		@Override
		public Object get() {
			return mItemSource;
		}

		@Override
		protected BindingType AcceptThisTypeAs(Class<?> type) {
			return BindingType.OneWay;
		}
	}
	
	private void fillDate(){
		if(mItemSource == null || mItemTemplateView == null){
			return;
		}
		mItemSource.subscribe(this);
		mViewPagerAdapter = new InteriorViewPagerAdapter();
		setAdapter(mViewPagerAdapter);
	}
	
	private class InteriorViewPagerAdapter extends AbsViewPagerAdapter{
		@Override
		public View getItemView(ViewGroup container, int position) {
			InflateResult inflatedView = Binder.inflateView(getContext(), mItemTemplateView.getDefaultLayoutId(), null, false);
			View view = Binder.bindView(getContext(), inflatedView, mItemSource.get(position));
			return view;
		}

		@Override
		public int getCount() {
			return mItemSource.size();
		}
	}
}
