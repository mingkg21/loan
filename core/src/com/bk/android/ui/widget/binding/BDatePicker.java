package com.bk.android.ui.widget.binding;

import gueei.binding.BindingType;
import gueei.binding.IBindableView;
import gueei.binding.ViewAttribute;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.DatePicker;
import android.widget.DatePicker.OnDateChangedListener;

public class BDatePicker extends DatePicker implements IBindableView<BDatePicker>, OnDateChangedListener {
	private GetYear mGetYear;
	private GetMonth mGetMonth;
	private GetDay mGetDay;

	public BDatePicker(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(getYear(), getMonth(), getDayOfMonth(), this);
	}

	@Override
	public void onDateChanged(DatePicker view, int year, int monthOfYear,int dayOfMonth) {
		if(mGetYear != null){
			mGetYear.notifyChanged();
		}
		if(mGetMonth != null){
			mGetMonth.notifyChanged();
		}
		if(mGetDay != null){
			mGetDay.notifyChanged();
		}
	}
	
	@Override
	public ViewAttribute<? extends View, ?> createViewAttribute(
			String attributeId) {
		if (attributeId.equals("getYear")) {
			if(mGetYear == null){
				mGetYear = new GetYear(this, attributeId);
			}
			return mGetYear;
		} else if (attributeId.equals("getMonth")) {
			if(mGetMonth == null){
				mGetMonth = new GetMonth(this, attributeId);
			}
			return mGetMonth;
		} else if (attributeId.equals("getDay")) {
			if(mGetDay == null){
				mGetDay = new GetDay(this, attributeId);
			}
			return mGetDay;
		}
		return null;
	}

	public static class GetYear extends ViewAttribute<BDatePicker, Integer> {
		public GetYear(BDatePicker view, String attributeId) {
			super(Integer.class, view, attributeId);
		}

		@Override
		public Integer get() {
			return getView().getYear();
		}

		@Override
		protected void doSetAttributeValue(Object newValue) {
		}

		@Override
		protected BindingType AcceptThisTypeAs(Class<?> type) {
			return BindingType.TwoWay;
		}
	}

	public static class GetMonth extends ViewAttribute<BDatePicker, Integer> {
		public GetMonth(BDatePicker view, String attributeId) {
			super(Integer.class, view, attributeId);
		}

		@Override
		public Integer get() {
			return getView().getMonth();
		}

		@Override
		protected void doSetAttributeValue(Object newValue) {
		}

		@Override
		protected BindingType AcceptThisTypeAs(Class<?> type) {
			return BindingType.TwoWay;
		}
	}

	public static class GetDay extends ViewAttribute<BDatePicker, Integer> {
		public GetDay(BDatePicker view, String attributeId) {
			super(Integer.class, view, attributeId);
		}

		@Override
		public Integer get() {
			return getView().getDayOfMonth();
		}

		@Override
		protected void doSetAttributeValue(Object newValue) {
		}

		@Override
		protected BindingType AcceptThisTypeAs(Class<?> type) {
			return BindingType.TwoWay;
		}
	}
}
