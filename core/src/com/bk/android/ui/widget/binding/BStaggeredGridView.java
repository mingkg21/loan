package com.bk.android.ui.widget.binding;

import gueei.binding.AttributeBinder;
import gueei.binding.Binder;
import gueei.binding.Binder.InflateResult;
import gueei.binding.BindingType;
import gueei.binding.IBindableView;
import gueei.binding.IObservable;
import gueei.binding.Observer;
import gueei.binding.Utility;
import gueei.binding.ViewAttribute;
import gueei.binding.viewAttributes.templates.Layout;

import java.util.Collection;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import com.etsy.android.grid.StaggeredGridView;

public class BStaggeredGridView extends StaggeredGridView implements IBindableView<BStaggeredGridView>{
	public BStaggeredGridView(Context context) {
		super(context);
	}

	public BStaggeredGridView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	public BStaggeredGridView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	public ViewAttribute<? extends View, ?> createViewAttribute(String attributeId) {
		if (attributeId.equals("headerTemplate")) {
			return new HeaderTemplateViewAttribute(this,attributeId);
		}else if (attributeId.equals("footerTemplate")) {
			return new FooterTemplateViewAttribute(this,attributeId);
		}else if (attributeId.equals("headerSource")) {
			return new HeaderSourceViewAttribute(this,attributeId,"headerTemplate");
		}else if (attributeId.equals("footerSource")) {
			return new HeaderSourceViewAttribute(this,attributeId,"footerTemplate");
		}
		return null;
	}
	
	public class HeaderTemplateViewAttribute extends ViewAttribute<StaggeredGridView, InflateResult> {
		public HeaderTemplateViewAttribute(StaggeredGridView view,
				String attributeName) {
			super(InflateResult.class, view, attributeName);
		}

		private InflateResult template;
		
		@Override
		protected void doSetAttributeValue(Object newValue) {
			if(getView()==null) return;
			if (newValue instanceof Layout){
				InflateResult result = Binder.inflateView(getView().getContext(), ((Layout) newValue).getDefaultLayoutId(), null, false);
				template = result;
				getView().addHeaderView(result.rootView);
				return;
			}else if (newValue instanceof Integer){
				if ((Integer)newValue>0){
					InflateResult result = Binder.inflateView(getView().getContext(), (Integer) newValue, null, false);
					template = result;
					getView().addHeaderView(result.rootView);
				}
				return;
			}else if (newValue instanceof CharSequence){
				int value = Utility.resolveLayoutResource(newValue.toString(), getView().getContext());
				if (value>0){
					InflateResult result = Binder.inflateView(getView().getContext(),value, null, false);
					template = result;
					getView().addHeaderView(result.rootView);
				}
				return;
			}
		}

		@Override
		public InflateResult get() {
			return template;
		}

		@Override
		protected BindingType AcceptThisTypeAs(Class<?> type) {
			if (Integer.class.isAssignableFrom(type) ||
				CharSequence.class.isAssignableFrom(type)) 
				return BindingType.OneWay;
			if (Layout.class.isAssignableFrom(type)) return BindingType.TwoWay;
			return BindingType.NoBinding;
		}
	}
	
	public class FooterTemplateViewAttribute extends ViewAttribute<StaggeredGridView, InflateResult> {
		public FooterTemplateViewAttribute(StaggeredGridView view,
				String attributeName) {
			super(InflateResult.class, view, attributeName);
		}

		private InflateResult template;
		
		@Override
		protected void doSetAttributeValue(Object newValue) {
			if(getView()==null) return;
			if (newValue instanceof Layout){
				InflateResult result = Binder.inflateView(getView().getContext(), ((Layout) newValue).getDefaultLayoutId(), null, false);
				template = result;
				getView().addFooterView(result.rootView);
				return;
			}else if (newValue instanceof Integer){
				if ((Integer)newValue>0){
					InflateResult result = Binder.inflateView(getView().getContext(), (Integer) newValue, null, false);
					template = result;
					getView().addFooterView(result.rootView);
				}
				return;
			}else if (newValue instanceof CharSequence){
				int value = Utility.resolveLayoutResource(newValue.toString(), getView().getContext());
				if (value>0){
					InflateResult result = Binder.inflateView(getView().getContext(),value, null, false);
					template = result;
					getView().addFooterView(result.rootView);
				}
				return;
			}
		}

		@Override
		public InflateResult get() {
			return template;
		}

		@Override
		protected BindingType AcceptThisTypeAs(Class<?> type) {
			if (Integer.class.isAssignableFrom(type) ||
				CharSequence.class.isAssignableFrom(type)) 
				return BindingType.OneWay;
			if (Layout.class.isAssignableFrom(type)) return BindingType.TwoWay;
			return BindingType.NoBinding;
		}
	}
	
	public class HeaderSourceViewAttribute extends ViewAttribute<StaggeredGridView, Object> {
		InflateResult template;
		ViewAttribute<?,InflateResult> itemTemplateAttr;
		Object mValue;

		private Observer templateObserver = new Observer(){
			public void onPropertyChanged(IObservable<?> prop,
					Collection<Object> initiators) {
				template = itemTemplateAttr.get();
				doSetAttributeValue(mValue);
			}
		};
		
		@SuppressWarnings("unchecked")
		public HeaderSourceViewAttribute(StaggeredGridView view, String attributeName, String templateId) {
			super(Object.class,view, attributeName);
			
			try{
				itemTemplateAttr = (ViewAttribute<?,InflateResult>)Binder.getAttributeForView(getView(),templateId);
				itemTemplateAttr.subscribe(templateObserver);
				template = itemTemplateAttr.get();
			}catch(Exception e){
				e.printStackTrace();
				return;
			}
		}

		@Override
		protected void doSetAttributeValue(Object newValue) {
			if(getView()==null) return;
			mValue = newValue;

			if (newValue == null)
				return;
			
			if (template==null) return;
			
			try {
				for (View view : template.processedViews) {
					AttributeBinder.getInstance().bindView(getView().getContext(), view, newValue);
				}
				return;
			} catch (Exception e) {
				e.printStackTrace();
				return;
			}
		}

		@Override
		public Object get() {
			return mValue;
		}

		@Override
		protected BindingType AcceptThisTypeAs(Class<?> type) {
			return BindingType.OneWay;
		}
	}
}
