package com.bk.android.ui.widget.binding;

import gueei.binding.IBindableView;
import gueei.binding.ViewAttribute;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import com.bk.android.ui.widget.CommonEditText;

public class BCommonEditText extends CommonEditText implements IBindableView<BStaggeredGridView>{
	public BCommonEditText(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	public ViewAttribute<? extends View, ?> createViewAttribute(
			String attributeId) {
		if (attributeId.equals("requestInput")) {//设置对于的属性值
			return new RequestInputAttribute(this,attributeId);
		}
		return null;
	}
	
	public static class RequestInputAttribute extends ViewAttribute<CommonEditText, Boolean>{
		public RequestInputAttribute(CommonEditText view,String attributeId){
			super(Boolean.class, view, attributeId);
		}
		
		@Override
		public Boolean get() {//获取值
			return null;
		}

		@Override
		protected void doSetAttributeValue(Object newValue) {//设置值
			if(newValue == null){
				return;
			}
			if (newValue instanceof Boolean) {
				getView().requestInput((Boolean) newValue);
			}
		}
	}
}
