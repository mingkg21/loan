package com.bk.android.ui.widget.binding;

import gueei.binding.IBindableView;
import gueei.binding.ViewAttribute;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;

public class BWebView extends RelativeLayout implements IBindableView<BWebView> {
	protected static final int WEB_VIEW_ID = "WEB_VIEW_ID".hashCode();
	protected WebView mWebView;

	public BWebView(Context context, AttributeSet attrs) {
		super(context, attrs);
		mWebView = new WebView(getContext());
		mWebView.setId(WEB_VIEW_ID);
		mWebView.setFocusable(true);
		addView(mWebView,LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT);
	}

	public WebView getWebView(){
		return mWebView;
	}
	
	public void stopLoading() {
		mWebView.stopLoading();
	}

	public void destroy() {
		try {
			clearView();
			mWebView.removeAllViews();
			mWebView.setVisibility(View.GONE);
		} catch (Exception e) {}
	}

	public void clearView() {
		mWebView.stopLoading();
		mWebView.loadData("<a></a>", "text/html", "utf-8"); 
	}

	public void loadData(String data, String mimeType, String encoding) {
		mWebView.loadData(data, mimeType, encoding);
	}

	public void loadUrl(String url) {
		mWebView.loadUrl(url);
	}
	
	public WebSettings getSettings() {
		return mWebView.getSettings();
	}
	
	public void setWebChromeClient(WebChromeClient client) {
		mWebView.setWebChromeClient(client);
	}
	
	public void onPause() {
		mWebView.onPause();
	}
	
	public void onResume() {
		mWebView.onResume();
	}
	
	public void setWebViewClient(WebViewClient webViewClient) {
		mWebView.setWebViewClient(webViewClient);
	}
	
	public void addJavascriptInterface(Object obj, String interfaceName) {
		mWebView.addJavascriptInterface(obj, interfaceName);
	}
	
	@Override
	public ViewAttribute<? extends View, ?> createViewAttribute(
			String attributeId) {
		if (attributeId.equals("url")) {//设置对于的属性值
			return new UrlAttribute(this,attributeId);
		}else if (attributeId.equals("data")) {//设置对于的属性值
			return new DataAttribute(this,attributeId);
		}
		return null;
	}
	
	public static class UrlAttribute extends ViewAttribute<BWebView, String>{
		public UrlAttribute(BWebView view,String attributeId){
			super(String.class, view, attributeId);
		}
		
		@Override
		public String get() {//获取值
			return null;
		}

		@Override
		protected void doSetAttributeValue(Object newValue) {//设置值
			if(newValue == null){
				return;
			}
			getView().stopLoading();
			if (newValue instanceof String) {
				getView().loadUrl((String) newValue);
			}else{
				getView().clearView();
			}
		}
	}
	
	public static class DataAttribute extends ViewAttribute<BWebView, String>{
		public DataAttribute(BWebView view,String attributeId){
			super(String.class, view, attributeId);
		}
		
		@Override
		public String get() {//获取值
			return null;
		}

		@Override
		protected void doSetAttributeValue(Object newValue) {//设置值
			if(newValue == null){
				return;
			}
			getView().stopLoading();
			if (newValue instanceof String) {
				getView().loadData((String) newValue,"text/html; charset=UTF-8", null);
			}else{
				getView().clearView();
			}
		}
	}
}
