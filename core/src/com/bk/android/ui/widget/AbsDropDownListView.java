package com.bk.android.ui.widget;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.util.SparseBooleanArray;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.FrameLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.PopupWindow.OnDismissListener;
/**
 * 下拉列表控件
 * @author linyiwei
 * @email 21551594@qq.com
 * @date 2012-07-27
 */
public abstract class AbsDropDownListView implements OnDismissListener{
	private PopupWindow mPopupWindow;
	private View mAnchor;
	private ListView mListView;
	private Context mContext;
	private OnItemClickListener mOnItemClickListener;
	private boolean isClickDismissEnabled = true;
	private int mPopupMaxWidth;
	private boolean isWidthChuange = false;
	private View mRoot;
	private int mScreenWidth = 0;
	private int mScreenHeight = 0;
	private WindowManager windowManager;
	private OnDismissListener mOnDismissListener;
	private OnShowListener mOnShowListener;
	private PopupViewContainer mContentLayout;
	private OnDispatchKeyEventListener mOnDispatchKeyEventListener;
	
	public AbsDropDownListView(View anchor){
		mContext = anchor.getContext();
		mPopupWindow = new PopupWindow(mContext);
		mContentLayout = new PopupViewContainer(mContext);
		mAnchor = anchor;
		windowManager = (WindowManager)mContext.getSystemService(Context.WINDOW_SERVICE);
		mScreenWidth = windowManager.getDefaultDisplay().getWidth();
		mScreenHeight = windowManager.getDefaultDisplay().getHeight();
		mPopupMaxWidth = mScreenWidth / 3 * 2;
		
		mPopupWindow.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
		mPopupWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
		mPopupWindow.setTouchable(true);
		mPopupWindow.setFocusable(true);
		mPopupWindow.setOnDismissListener(this);
		mPopupWindow.setContentView(mContentLayout);
		Drawable defaultBackgroundDrawable = getDefaultBackgroundDrawable();
		int defaultAnimationStyle = getDefaultAnimationStyle();
		mRoot = onCreateContentView();
		
		if(defaultBackgroundDrawable == null){
			defaultBackgroundDrawable = new ColorDrawable(Color.TRANSPARENT);
		}
		mPopupWindow.setBackgroundDrawable(defaultBackgroundDrawable);
		if(defaultAnimationStyle != 0){
			mPopupWindow.setAnimationStyle(defaultAnimationStyle);
		}
		if(mRoot != null){
			mContentLayout.addView(mRoot);
		}
	}
	
	public Context getContext(){
		return mContext;
	}
	
	public void setSelection(int position){
		if(position >= 0 && position < mListView.getCount()){
			mListView.setSelection(position);
		}
	}
	
	public void setSelectionFromTop(int position, int y){
		if(position >= 0 && position < mListView.getCount()){
			mListView.setSelectionFromTop(position, y);
		}
	}
	
	public int getItemCount() {
		return mListView.getCount();
	}
	
	public void setItemChecked(int position, boolean value){
		if(position >= 0 && position < mListView.getCount()){
			mListView.setItemChecked(position, value);
		}
	}
	
	public AdapterView<ListAdapter> getAdapterView() {
		return mListView;
	}
	
	public SparseBooleanArray getCheckedItemPositions(){
		return mListView.getCheckedItemPositions();
	}
	
	public long[] getCheckItemIds(){
		return mListView.getCheckItemIds();
	}
	
	public long[] getCheckedItemIds(){
		return mListView.getCheckedItemIds();
	}
	
	public int getCheckedItemPosition(){
		return mListView.getCheckedItemPosition();
	}
	
	private View onCreateContentView(){
		mListView = new ListView(mContext);
		mListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		mListView.setCacheColorHint(Color.TRANSPARENT);
		Drawable dividerDrawable = getListViewDivider();
		if(dividerDrawable != null){
			mListView.setDivider(dividerDrawable);
			mListView.setDividerHeight(1);
		}
		Drawable defaultBackgroundDrawable = getDefaultContentBackgroundDrawable();
		if(defaultBackgroundDrawable != null){
			mListView.setBackgroundDrawable(defaultBackgroundDrawable);
		}
		Drawable selectorDrawable = getListViewSelector();
		if(selectorDrawable != null){
			mListView.setSelector(selectorDrawable);
		}
		mListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				onDispatchItemClick(parent, view, position, id);
			}
		});
		return mListView;
	}
	
	public void setAdapter(ListAdapter adapter){
		isWidthChuange = true;
		if(adapter == null){
			mListView.setAdapter(adapter);
		}else{
			InteriorAdapter interiorAdapter= new InteriorAdapter(adapter);
			mListView.setAdapter(interiorAdapter);
		}
	}
	
	@Override
	public void onDismiss() {
		if(mOnDismissListener != null){
			mOnDismissListener.onDismiss();
		}
	}
	
	public void setOnDismissListener(OnDismissListener onDismissListener){
		mOnDismissListener = onDismissListener; 
	}
	
	public void setOnShowListener(OnShowListener onShowListener){
		mOnShowListener = onShowListener;
	}
	
	protected void onDispatchItemClick(AdapterView<?> parent, View view, int position, long id){
		ListAdapter listAdapter = (ListAdapter) parent.getAdapter();
		if(!listAdapter.isEnabled(position)){
			return;
		}
		if(mOnItemClickListener != null){
			mOnItemClickListener.onItemClick(parent, view, position, id);
		}
		if(isClickDismissEnabled){
			mAnchor.post(new Runnable() {
				@Override
				public void run() {
					dismissDropDown();
				}
			});
		}
	}

	public void setOnItemClickListener(OnItemClickListener listener){
		mOnItemClickListener = listener;
	}
    
    public void setOnItemLongClickListener(OnItemLongClickListener listener) {
		mListView.setOnItemLongClickListener(listener);
    }
	
	public abstract Drawable getListViewSelector();

	public abstract Drawable getListViewDivider();
	
	public abstract Drawable getDefaultContentBackgroundDrawable();
	
	public abstract Drawable getDefaultBackgroundDrawable();
	
	public abstract int getDefaultAnimationStyle();

	public void setBackgroundDrawable(Drawable backgroundDrawable){
		mPopupWindow.setBackgroundDrawable(backgroundDrawable);
	}
	
	public void setAnimation(int animationStyle){
		setAnimation(animationStyle,true);
	}
	
	public void setAnimation(int animationStyle,boolean isNeedUpdate){
		mPopupWindow.setAnimationStyle(animationStyle);
		if(mPopupWindow.isShowing() && isNeedUpdate){
			mPopupWindow.update();
		}
	}
	
	public void setClickDismissEnabled(boolean isClickDismissEnabled){
		this.isClickDismissEnabled = isClickDismissEnabled;
	}
	
	public boolean isShowing(){
		return mPopupWindow.isShowing();
	}
	
	public void showDropDown(){
		showDropDown(0,0);
	}
	
	public void showDropDown(int xoff, int yoff){
		if(!mPopupWindow.isShowing()){
			if(isWidthChuange){
				isWidthChuange = false;
				mPopupWindow.setWidth(Math.min(measureContentWidth(mListView.getAdapter()), mPopupMaxWidth));
			}
			showAsDropDown(xoff,yoff);
			if(mOnShowListener != null){
				mOnShowListener.onShow();
			}
		}
	}
	
	private void showAsDropDown(int xoff, int yoff){
		int x,y;
		int gravity;
		int popupWidth,popupHeight;
		int[] mDrawingLocation = new int[2];
        int[] mScreenLocation = new int[2];

		mAnchor.getLocationInWindow(mDrawingLocation );
        x = mDrawingLocation[0] + xoff;;
        y = mDrawingLocation[1] + mAnchor.getHeight() + yoff;
        popupWidth = mPopupWindow.getWidth();
        popupHeight = mPopupWindow.getHeight();
        if(popupHeight < 0){
        	popupHeight = 0;
        }
        if(popupWidth < 0){
        	popupWidth = 0;
        }
        boolean onTop = false;

        gravity = Gravity.LEFT | Gravity.TOP;
        
		mAnchor.getLocationOnScreen(mScreenLocation);
        final Rect displayFrame = new Rect();
        mAnchor.getWindowVisibleDisplayFrame(displayFrame);
        
        final View root = mAnchor.getRootView();
        onTop = (displayFrame.bottom - mScreenLocation[1] - mAnchor.getHeight() - yoff) <
                (mScreenLocation[1] - yoff - displayFrame.top);
        if (onTop) {
//        	yoff = -yoff;
            gravity = Gravity.LEFT | Gravity.BOTTOM;
            y = root.getHeight() - mDrawingLocation[1] + yoff;
        } else {
            y = mDrawingLocation[1] + mAnchor.getHeight() + yoff;
        }
        gravity |= Gravity.DISPLAY_CLIP_VERTICAL;
//        mPopupWindow.showAtLocation(root, gravity, x, y);
        
        //特例 居中
        mPopupWindow.showAtLocation(root, gravity, x - ((popupWidth - mAnchor.getWidth()) / 2) , y - dip2px(6));
	}
	
	private int dip2px(int dip) {
		 final float scale = mContext.getResources().getDisplayMetrics().density; 
         return (int)(dip * scale + 0.5f); 
	}
	
	public void dismissDropDown(){
		if(mPopupWindow.isShowing()){
			mPopupWindow.dismiss();
		}
	}
	
	private int measureContentWidth(ListAdapter adapter) {
	    // Menus don't tend to be long, so this is more sane than it looks.
		int width = 0;
		View itemView = null;
		final int widthMeasureSpec =
		    MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED);
		final int heightMeasureSpec =
		    MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED);
		final int count = adapter.getCount();
		for (int i = 0; i < count; i++) {
		    itemView = adapter.getView(i, itemView, mListView);
		    itemView.measure(widthMeasureSpec, heightMeasureSpec);
		    width = Math.max(width, itemView.getMeasuredWidth());
		}
		Rect padding = getDrawablePadding(mPopupWindow.getBackground());
		width += padding.left + padding.right;
		padding = getDrawablePadding(mListView.getSelector());
		width += padding.left + padding.right;
		padding = getDrawablePadding(mListView.getBackground());
		width += padding.left + padding.right;
		padding = getDrawablePadding(mListView.getSelector());
		width += padding.left + padding.right;
		return width;
	}
	
	private Rect getDrawablePadding(Drawable drawable){
		Rect padding = new Rect();
		if(drawable != null){
			drawable.getPadding(padding);
		}
		return padding;
	}
	
	public void setOnDispatchKeyEventListener(OnDispatchKeyEventListener l){
		mOnDispatchKeyEventListener = l;
	}
	
	public class InteriorAdapter extends DecoratorListAdapter{
		public InteriorAdapter(ListAdapter adapter) {
			super(adapter);
		}

		@Override
		public void notifyDataSetChanged() {
			super.notifyDataSetChanged();
			isWidthChuange = true;
		}
	}
	
	public interface OnShowListener{
		public void onShow();
	}
	
	public interface OnDispatchKeyEventListener{
		public boolean onDispatchKeyEvent(KeyEvent event);
	}
	
	private class PopupViewContainer extends FrameLayout {

        public PopupViewContainer(Context context) {
            super(context);
        }

		@Override
		public boolean dispatchKeyEvent(KeyEvent event) {
			if(mOnDispatchKeyEventListener != null && mOnDispatchKeyEventListener.onDispatchKeyEvent(event)){
				return true;
			}
			return super.dispatchKeyEvent(event);
		}
	}
}
