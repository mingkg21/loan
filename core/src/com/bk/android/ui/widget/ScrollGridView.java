package com.bk.android.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.GridView;

/** 在ScrollView中能显示的GridView
 * @author mingkg21
 * @date 2011-7-31
 * @email mingkg21@gmail.com
 */
public class ScrollGridView extends GridView {
	public ScrollGridView(Context context) {
		super(context);
	}
	
	public ScrollGridView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public ScrollGridView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2, MeasureSpec.AT_MOST);
		super.onMeasure(widthMeasureSpec, expandSpec);
	}

}
