package com.bk.android.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;

public class PatchedEditText extends EditText {
	public PatchedEditText(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public PatchedEditText(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public PatchedEditText(Context context) {
		super(context);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		try {
			super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		} catch (ArrayIndexOutOfBoundsException e) {
			e.printStackTrace();
			if(getText() != null){
				setText(getText().toString());
				super.onMeasure(widthMeasureSpec, heightMeasureSpec);
			}
		}
	}

	@Override
	public void setGravity(int gravity) {
		try {
			super.setGravity(gravity);
		} catch (ArrayIndexOutOfBoundsException e) {
			e.printStackTrace();
			if(getText() != null){
				setText(getText().toString());
				super.setGravity(gravity);
			}
		}
	}

	@Override
	public void setText(CharSequence text, BufferType type) {
		try {
			super.setText(text, type);
		} catch (ArrayIndexOutOfBoundsException e) {
			e.printStackTrace();
			if(text != null){
				setText(text.toString());
			}
		}
	}


}
