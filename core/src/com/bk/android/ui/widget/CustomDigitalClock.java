package com.bk.android.ui.widget;

import java.util.Calendar;

import android.content.Context;
import android.database.ContentObserver;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.Settings;
import android.text.format.DateFormat;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * @author mingkg21
 * 
 */
public class CustomDigitalClock extends TextView {
	
	Calendar mCalendar;
	private final static String m12 = "h:mm aa";
	private final static String m24 = "k:mm";
	private FormatChangeObserver mFormatChangeObserver;

	private Runnable mTicker;
	private Handler mHandler;

	private boolean mTickerStopped = false;
	String mFormat;
	private String mExtraText;

	public CustomDigitalClock(Context context) {
		super(context);
		initClock(context);
	}

	public CustomDigitalClock(Context context, AttributeSet attrs) {
		super(context, attrs);
		initClock(context);
	}

	public void setExtraText(String extraText){
		mExtraText = extraText;
		invalidateTime(true);
	}
	
	private void initClock(Context context) {
		if (mCalendar == null) {
			mCalendar = Calendar.getInstance();
			mCalendar.setTimeInMillis(System.currentTimeMillis());
		}
		setFormat();
	}

	@Override
	protected void onAttachedToWindow() {
		mTickerStopped = false;
		super.onAttachedToWindow();
		if(mFormatChangeObserver == null){
			 mFormatChangeObserver = new FormatChangeObserver();
			 getContext().getContentResolver().registerContentObserver(
			 Settings.System.CONTENT_URI, true, mFormatChangeObserver);
		}
		mHandler = new Handler();

		/**
		 * requests a tick on the next hard-second boundary
		 */
		mTicker = new Runnable() {
			@Override
			public void run() {
				if (mTickerStopped)
					return;
				mCalendar.setTimeInMillis(System.currentTimeMillis());
				if(mCalendar.getTime().getSeconds() < 2 || getText() == null ||  getText().length() == 0){
					invalidateTime();
				}
				long now = SystemClock.uptimeMillis();
				long next = now + (1000 - now % 1000);
				mHandler.postAtTime(mTicker, next);
			}
		};
		mTicker.run();
	}

	private void invalidateTime(){
		invalidateTime(false);
	}
	
	private void invalidateTime(boolean isrRequestLayout){
		String dataString = "";
		if(mExtraText != null){
			dataString += mExtraText + " ";
		}
		dataString += DateFormat.format(mFormat, mCalendar).toString().toUpperCase();
		setText(dataString);
		if(isrRequestLayout){
			requestLayout();
		}else{
			invalidate();
		}
	}
	
	@Override
	protected void onDetachedFromWindow() {
		super.onDetachedFromWindow();
		if(mFormatChangeObserver != null){
			getContext().getContentResolver().unregisterContentObserver(mFormatChangeObserver);
			mFormatChangeObserver = null;
		}
		mTickerStopped = true;
	}

	/**
	 * Pulls 12/24 mode from system settings
	 */
	private boolean get24HourMode() {
		return android.text.format.DateFormat.is24HourFormat(getContext());
	}

	private void setFormat() {
		if (get24HourMode()) {
			mFormat = m24;
		} else {
			mFormat = m12;
		}
	}

	private class FormatChangeObserver extends ContentObserver {
		public FormatChangeObserver() {
			super(new Handler());
		}

		@Override
		public void onChange(boolean selfChange) {
			setFormat();
			invalidateTime();
		}
	}
}
