package com.bk.android.ui.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PaintFlagsDrawFilter;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class FingerDrawingView extends View {
	private static final float TOUCH_TOLERANCE = 1;
	private float mX, mY;
	private Paint mPaint;
	private Path mPath;
	private int mPaintSize;
	private int mPaintColor;
	private boolean isEraser;
	private Bitmap mBitmap;
    private Canvas mCanvas;
    private Paint mBitmapPaint;

    public FingerDrawingView(Context context) {
		super(context);
		init();
	}
	
    public FingerDrawingView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	private void init(){
		mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        
        setPaintSize(12);
        setPaintColor(0xFFFF0000);
        
        mPath = new Path();
        mBitmapPaint = new Paint(Paint.DITHER_FLAG);
        
        setFocusable(true);
        setFocusableInTouchMode(true);
        requestFocus();
	}
	
	public void setPaintSize(int size){
		mPaintSize = size;
        mPaint.setStrokeWidth(mPaintSize);
	}
	
	public void setPaintColor(int color){
		mPaintColor = color;
        mPaint.setColor(mPaintColor);
	}
	
	public void setEraser(boolean eraser){
		isEraser = eraser;
		if(isEraser){
			mPaint.setAlpha(0);
	        mPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
		}else{
			mPaint.setAlpha(255);
	        mPaint.setXfermode(null);
		}
	}

	public boolean isEraser(){
		return isEraser;
	}
	
	public void clear(){
		if(mCanvas != null){
			mCanvas.drawColor(0,Mode.CLEAR);
			invalidate();
		}
	}
	
	private void touch_start(float x, float y) {
    	mPath.reset();
        mPath.moveTo(x, y);
        mX = x;
        mY = y;
    }
    
    private void touch_move(float x, float y) {
    	float dx = Math.abs(x - mX);
        float dy = Math.abs(y - mY);
        if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
            mPath.quadTo(mX, mY, (x + mX)/2, (y + mY)/2);
            mX = x;
            mY = y;
        }
    }
    
    private void touch_up() {
    	if(mCanvas != null){
    		mPath.lineTo(mX, mY);
            // commit the path to our offscreen
            mCanvas.drawPath(mPath, mPaint);
            // kill this so we don't double draw
            mPath.reset();
    	}
    }
    
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float x = event.getX();
        float y = event.getY();
        
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
            	getParent().requestDisallowInterceptTouchEvent(true);
                touch_start(x, y);
                invalidate();
                break;
            case MotionEvent.ACTION_MOVE:
                touch_move(x, y);
                invalidate();
                break;
            case MotionEvent.ACTION_UP:
            	getParent().requestDisallowInterceptTouchEvent(false);
                touch_up();
                invalidate();
                break;
        }
        return true;
    }

	@Override
    protected void onDraw(Canvas canvas) {
		if(mBitmap == null){
			mBitmap = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
            mCanvas = new Canvas(mBitmap);
			mCanvas.setDrawFilter(new PaintFlagsDrawFilter(0, Paint.ANTI_ALIAS_FLAG|Paint.FILTER_BITMAP_FLAG));
		}

		if(isEraser){
			mCanvas.drawPath(mPath, mPaint);
	    }
        canvas.drawBitmap(mBitmap, 0, 0, mBitmapPaint);
        if(!isEraser){
	        canvas.drawPath(mPath, mPaint);
	    }
    }
	/**
	 * @see android.view.View#measure(int, int)
	 */
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		setMeasuredDimension(measureWidth(widthMeasureSpec),
				measureHeight(heightMeasureSpec));
	}

	/**
	 * Determines the mWidth of this view
	 * 
	 * @param measureSpec
	 *            A measureSpec packed into an int
	 * @return The mWidth of the view, honoring constraints from measureSpec
	 */
	private int measureWidth(int measureSpec) {
		int result = 0;
		int specMode = MeasureSpec.getMode(measureSpec);
		int specSize = MeasureSpec.getSize(measureSpec);

		if (specMode == MeasureSpec.EXACTLY) {
			result = specSize;
		} else {
			result = getSuggestedMinimumWidth();
			if (specMode == MeasureSpec.AT_MOST) {
				result = Math.min(result, specSize);
			}
		}

		return result;
	}

	/**
	 * Determines the mHeight of this view
	 * 
	 * @param measureSpec
	 *            A measureSpec packed into an int
	 * @return The mHeight of the view, honoring constraints from measureSpec
	 */
	private int measureHeight(int measureSpec) {
		int result = 0;
		int specMode = MeasureSpec.getMode(measureSpec);
		int specSize = MeasureSpec.getSize(measureSpec);

		if (specMode == MeasureSpec.EXACTLY) {
			result = specSize;
		} else {
			result = getSuggestedMinimumHeight();
			if (specMode == MeasureSpec.AT_MOST) {
				result = Math.min(result, specSize);
			}
		}
		return result;
	}
}
