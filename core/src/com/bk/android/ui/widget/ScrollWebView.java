package com.bk.android.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.webkit.WebView;

/**
 * Created by mingkg21 on 2017/4/24.
 */

public class ScrollWebView extends WebView {

    private ScrollChangedListener mScrollChangedListener;

    public ScrollWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);
        if(mScrollChangedListener != null){
            mScrollChangedListener.onScrollChanged(this, l, t, oldl, oldt);
        }
    }

    public void setScrollChangedListener(ScrollChangedListener l){
        mScrollChangedListener = l;
    }

    public interface ScrollChangedListener{
        public void onScrollChanged(WebView scrollView, int l, int t, int oldl, int oldt);

    }
}
