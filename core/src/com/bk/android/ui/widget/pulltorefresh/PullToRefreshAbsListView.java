package com.bk.android.ui.widget.pulltorefresh;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Adapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SpinnerAdapter;

import com.bk.android.ui.widget.DecoratorAdapter.DataChangedListener;
import com.bk.android.ui.widget.DecoratorListAdapter;
import com.bk.android.ui.widget.DecoratorSpinnerAdapter;

public abstract class PullToRefreshAbsListView<T extends AbsListView> extends PullToRefreshBase<T> {
	public PullToRefreshAbsListView(Context context, AttributeSet attrs) {
		super(context, attrs,AnimationStyle.FLIP);
		setMode(Mode.PULL_FROM_START);
	}

	@Override
	public Orientation getPullToRefreshScrollDirection() {
		return Orientation.VERTICAL;
	}
	
	@Override
	protected boolean isReadyForPullEnd() {
		return isLastItemVisible();
	}

	@Override
	protected boolean isReadyForPullStart() {
		return isFirstItemVisible();
	}
	
	private boolean isFirstItemVisible() {
		final Adapter adapter = getRefreshableView().getAdapter();

		if (null == adapter || adapter.getCount() == 0) {
			return true;

		} else {

			/**
			 * This check should really just be:
			 * getRefreshableView().getFirstVisiblePosition() == 0, but PtRListView
			 * internally use a HeaderView which messes the positions up. For
			 * now we'll just add one to account for it and rely on the inner
			 * condition which checks getTop().
			 */
			if (getRefreshableView().getFirstVisiblePosition() < 1) {
				final View firstVisibleChild = getRefreshableView().getChildAt(0);
				if (firstVisibleChild != null) {
					return firstVisibleChild.getTop() >= getRefreshableView().getTop();
				}
			}
		}

		return false;
	}

	private boolean isLastItemVisible() {
		final Adapter adapter = getRefreshableView().getAdapter();

		if (null == adapter || adapter.getCount() == 0) {
			return true;
		} else {
			final int lastItemPosition = getRefreshableView().getCount() - 1;
			final int lastVisiblePosition = getRefreshableView().getLastVisiblePosition();

			/**
			 * This check should really just be: lastVisiblePosition ==
			 * lastItemPosition, but PtRListView internally uses a FooterView
			 * which messes the positions up. For me we'll just subtract one to
			 * account for it and rely on the inner condition which checks
			 * getBottom().
			 */
			if (lastVisiblePosition > lastItemPosition - 1) {
				final int childIndex = lastVisiblePosition - getRefreshableView().getFirstVisiblePosition();
				final View lastVisibleChild = getRefreshableView().getChildAt(childIndex);
				if (lastVisibleChild != null) {
					return lastVisibleChild.getBottom() <= getRefreshableView().getBottom();
				}
			}
		}
		return false;
	}
	
	protected AbsListView createListView(Context context, AttributeSet attrs) {
		final AbsListView lv;
		if (VERSION.SDK_INT >= VERSION_CODES.GINGERBREAD) {
			lv = new InternalListViewSDK9(context, attrs);
		} else {
			lv = new InternalListView(context, attrs);
		}
		return lv;
	}
	
	@TargetApi(9)
	final class InternalListViewSDK9 extends InternalListView {
		public InternalListViewSDK9(Context context, AttributeSet attrs) {
			super(context, attrs);
		}

		@Override
		protected boolean overScrollBy(int deltaX, int deltaY, int scrollX, int scrollY, int scrollRangeX,
				int scrollRangeY, int maxOverScrollX, int maxOverScrollY, boolean isTouchEvent) {

			final boolean returnValue = super.overScrollBy(deltaX, deltaY, scrollX, scrollY, scrollRangeX,
					scrollRangeY, maxOverScrollX, maxOverScrollY, isTouchEvent);

			// Does all of the hard work...
			OverscrollHelper.overScrollBy(PullToRefreshAbsListView.this, deltaX, scrollX, deltaY, scrollY, isTouchEvent);

			return returnValue;
		}
	}

	protected class InternalListView extends ListView{
		private PullHeadViewHelper<ListAdapter> mPullHeadViewHelper;

		public InternalListView(Context context, AttributeSet attrs) {
			super(context, attrs);
			mPullHeadViewHelper = new PullHeadViewHelper<ListAdapter>();
		}
		
		@Override
		public void setAdapter(ListAdapter adapter) {
			super.setAdapter(mPullHeadViewHelper.setAdapter(adapter));
		}
		
		@Override
		protected void dispatchDraw(Canvas canvas) {
			try {
				super.dispatchDraw(canvas);
			} catch (IndexOutOfBoundsException e) {
				e.printStackTrace();
			}
			mPullHeadViewHelper.handPullHead(this);
		}

		@Override
		public boolean dispatchTouchEvent(MotionEvent ev) {
			try {
				mPullHeadViewHelper.mRunnable = null;
				return super.dispatchTouchEvent(ev);
			} catch (IndexOutOfBoundsException e) {
				e.printStackTrace();
				return false;
			}
		}
	}
	
	public class PullHeadViewHelper<TAdapter extends Adapter> implements DataChangedListener{
		private boolean isNeedPull;
		private Runnable mRunnable;
		
		@SuppressWarnings("unchecked")
		public TAdapter setAdapter(TAdapter adapter){
			isNeedPull = true;
			if(adapter instanceof ListAdapter){
				return (TAdapter) new DecoratorListAdapter((ListAdapter) adapter).setDataChangedListener(this);
			}else if(adapter instanceof SpinnerAdapter){
				return (TAdapter) new DecoratorSpinnerAdapter((SpinnerAdapter) adapter).setDataChangedListener(this);
			}
			return adapter;
		}
		
		public void handPullHead(final AbsListView absListView) {
			if(absListView.getChildCount() > 0 && isNeedPull){
				if(mRunnable != null){
					removeCallbacks(mRunnable);
					mRunnable = null;
				}
				mRunnable = new Runnable() {
					@Override
					public void run() {
						if(absListView.getChildCount() > 0 && this.equals(mRunnable)){
							View firstView = absListView.getChildAt(0);
							PullHeadView pullHeadView = findPullHeadView(firstView);
							if(pullHeadView != null){
								int[] location = new int[2];
								absListView.getLocationInWindow(location);
								int[] pullHeadLocation = new int[2];
								pullHeadView.getLocationInWindow(pullHeadLocation);
								int difference = location[1] - pullHeadLocation[1] - pullHeadView.getHeight();
								if(difference != 0){
									absListView.smoothScrollBy(-difference, 500);
								}
							}
						}
						mRunnable = null;
					}
				};
				postDelayed(mRunnable,1000);
				isNeedPull = false;
			}
		}
		
		private PullHeadView findPullHeadView(View view){
			if(view instanceof PullHeadView){
				return (PullHeadView) view;
			}else if(view instanceof ViewGroup){
				ViewGroup viewGroup = (ViewGroup) view;
				for (int i = 0; i < viewGroup.getChildCount(); i++) {
					view = viewGroup.getChildAt(i);
					PullHeadView pullHeadView = findPullHeadView(view);
					if(pullHeadView != null){
						return pullHeadView;
					}
				}
			}
			return null;
		}

		@Override
		public void onChanged() {
			isNeedPull = true;
		}

		@Override
		public void onInvalidated() {
			isNeedPull = true;
		}
	}
}
