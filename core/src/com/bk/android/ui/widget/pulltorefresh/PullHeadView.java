package com.bk.android.ui.widget.pulltorefresh;

public interface PullHeadView {
	public void getLocationInWindow(int[] location);
	public int getHeight();
}
