package com.bk.android.ui.widget.pulltorefresh;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

public class PullHeadLinearLayout extends LinearLayout implements PullHeadView{
	public PullHeadLinearLayout(Context context) {
		super(context);
	}
	public PullHeadLinearLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
}
