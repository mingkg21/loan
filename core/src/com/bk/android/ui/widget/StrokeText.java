/*
 * ========================================================
 * ClassName:StrokeText.java* 
 * Description:
 * Copyright (C) 
 * ========================================================
 * Copyright (C) 2006 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *===================================================================*
 * Revision History
 *
 * Modification                    Tracking
 * Date         Author          Number       Description of changes
 *____________________________________________________________________
 * 
 * 2013-9-13     chendt          #00000       create
 */
package com.bk.android.ui.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.FontMetrics;
import android.util.AttributeSet;
import android.widget.TextView;

public class StrokeText extends TextView {

	private float mBigFontBottom;
	private float mBigFontHeight;

	private String text;
	private Paint mPaint;
	private int strokeSize = 1;

	public StrokeText(Context context) {
		super(context);
		init();
	}

	public StrokeText(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public StrokeText(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	private void init() {
		mPaint = new Paint();
		mPaint.setAntiAlias(true);
		mPaint.setTextSize(getTextSize());
		mPaint.setColor(Color.parseColor("#991A7A80"));
		FontMetrics fm = mPaint.getFontMetrics();
		mBigFontBottom = fm.bottom;
		mBigFontHeight = fm.bottom - fm.top;
	}

	@Override
	protected void onDraw(Canvas canvas) {
		if (strokeSize > 0 && strokeSize < 4) {
			float y = getPaddingTop() + mBigFontHeight - mBigFontBottom;
			canvas.drawText(text, 0, y - strokeSize, mPaint);
			canvas.drawText(text, 0, y + strokeSize, mPaint);
			canvas.drawText(text, 0 + strokeSize, y, mPaint);
			canvas.drawText(text, 0 + strokeSize, y + strokeSize, mPaint);
			canvas.drawText(text, 0 + strokeSize, y - strokeSize, mPaint);
			canvas.drawText(text, 0 - strokeSize, y, mPaint);
			canvas.drawText(text, 0 - strokeSize, y + strokeSize, mPaint);
			canvas.drawText(text, 0 - strokeSize, y - strokeSize, mPaint);
		}

		super.onDraw(canvas);
	}

	@Override
	public void setText(CharSequence text, BufferType type) {
		super.setText(text, type);
		this.text = text.toString();
		invalidate();
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		if (strokeSize > 0 && strokeSize < 4) {
			setMeasuredDimension(getMeasuredWidth() + strokeSize,
					getMeasuredHeight());
		}
	}

}