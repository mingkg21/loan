package com.bk.android.ui.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ExpandableListView;

/** 在ScrollView中能显示的ListdView
 * @author mingkg21
 * @date 2011-7-31
 * @email mingkg21@gmail.com
 */
public class CommonExpandableListView extends ExpandableListView {
	public CommonExpandableListView(Context context) {
		super(context);
	}
	
	public CommonExpandableListView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public CommonExpandableListView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

//	@Override
//	protected void dispatchDraw(Canvas canvas) {
//		super.dispatchDraw(canvas);
//		for (int i = 0;i < getCount();i++) {
//			long expandableId = getExpandableListPosition(i);
//			int type = getPackedPositionType(expandableId);
//			int groupPosition = getPackedPositionGroup(expandableId);
//			if(type == PACKED_POSITION_TYPE_GROUP && !isGroupExpanded(groupPosition)){
//				expandGroup(groupPosition);
//			}
//		}
//	}
	
	@Override
	public boolean performItemClick(View v, int position, long id) {
		long expandableId = getExpandableListPosition(position);
		int type = getPackedPositionType(expandableId);
		if(type == PACKED_POSITION_TYPE_CHILD){
			return super.performItemClick(v, position, id);
		}else if(getOnItemClickListener() != null){
			getOnItemClickListener().onItemClick(this, v, getPackedPositionGroup(expandableId), id);
		}
		return true;
	}
}
