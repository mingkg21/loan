package com.bk.android.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.TextView;

import com.bk.android.code.R;

/** 带有图片的tab item
 * Created by mingkg21 on 2017/4/7.
 */

public class TabView extends TextView {

    private int invertColor;

    public TabView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.TabView);
        invertColor = typedArray.getColor(R.styleable.TabView_invertColor, Color.BLACK);
        typedArray.recycle();
    }

    @Override
    protected void onDraw(Canvas canvas) {

        Drawable drawable = null;

        Drawable[] drawables = getCompoundDrawables();
        drawable = drawables[1];
        if (drawable == null) {
            drawable = drawables[0];
            if (drawable == null) {
                drawable = drawables[2];
                if (drawable == null) {
                    drawable = drawables[3];
                }
            }
        }

        if (drawable != null) {
            if (isSelected()) {
                drawable.setColorFilter(invertColor, PorterDuff.Mode.SRC_ATOP);
            } else {
                drawable.clearColorFilter();
            }
        }
        super.onDraw(canvas);
    }

}
