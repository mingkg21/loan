package com.bk.android.ui.widget.viewpager;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

import com.bk.android.ui.widget.ScrollChildView;

public class HorizontalChildViewPager extends ViewPager{
	public HorizontalChildViewPager(Context context) {
		super(context);
	}

	public HorizontalChildViewPager(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	@Override
	protected boolean canScroll(View v, boolean checkV, int dx, int x, int y) {
		int childCount = getChildCount();
		for (int i = 0; i < childCount; i++) {
            final View child = getChildAt(i);
            final int scrollX = v.getScrollX();
            if(child.getLeft() <= x + scrollX && child.getRight() >= x + scrollX){
               if(canScrollChild(child, true, dx, x, y)){
            	   return true;
               }
            }
        }
		return checkV;
	}
	
	protected boolean canScrollChild(View v, boolean checkV, int dx, int x, int y) {
		boolean canScroll = false;
		if(v instanceof ScrollChildView && ((ScrollChildView) v).canScroll(this, dx,0, x, y)){
			return true;
		}
        if (v instanceof ViewGroup) {
        	if(v instanceof HorizontalChildViewPager){
        		HorizontalChildViewPager viewPager = (HorizontalChildViewPager) v;
        		PagerAdapter adapter = viewPager.getAdapter();
        		int childCount = viewPager.getChildCount();
        		if(adapter == null || adapter.getCount() <= 1 || childCount <= 1){
        			canScroll = !checkV;
        		}
        		int count = adapter.getCount();
        		final int widthWithMargin = viewPager.getWidth() + viewPager.getPageMargin();
                final int lastItemIndex = count - 1;
    			final float leftBound = Math.max(0, (viewPager.getCurrentItem() - 1) * widthWithMargin);
                final float rightBound = Math.min(viewPager.getCurrentItem() + 1, lastItemIndex) * widthWithMargin;
//        		if(dx > 0){//向右
//        			if(viewPager.getCurrentItem() != 0 || viewPager.getScrollX() < leftBound){
//        				canScroll = checkV;
//        			}
//        		}else{
//        			if(viewPager.getCurrentItem() != count - 1 || viewPager.getScrollX() > rightBound){
//        				canScroll = checkV;
//        			}
//        		}
        		if(dx > 0){//向右
        			if(viewPager.getCurrentItem() != 0){
        				canScroll = checkV;
        			}
        		}else{
        			if(viewPager.getCurrentItem() != count - 1){
        				canScroll = checkV;
        			}
        		}
        	}
            final ViewGroup group = (ViewGroup) v;
            final int scrollX = v.getScrollX();
            final int scrollY = v.getScrollY();
            final int count = group.getChildCount();
            for (int i = count - 1; i >= 0; i--) {
                final View child = group.getChildAt(i);
                if (canScrollChild(child, true, dx, x + scrollX - child.getLeft(),
                                y + scrollY - child.getTop()) || canScroll) {
                    return true;
                }
            }
        }
        return false;
	}
	
	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		try {
			super.onLayout(changed, l, t, r, b);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}