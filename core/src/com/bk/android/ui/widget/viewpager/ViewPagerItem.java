package com.bk.android.ui.widget.viewpager;

public interface ViewPagerItem {
	public void setLifeCycleEnabled(boolean enabled);
	public boolean onCreateItem();
	public void onResumeItem();
	public void onPauseItem();
	public void onDestroyItem();
}
