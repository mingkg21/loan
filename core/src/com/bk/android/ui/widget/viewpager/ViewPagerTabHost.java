package com.bk.android.ui.widget.viewpager;


import android.content.Context;
import android.database.DataSetObserver;
import android.os.Parcelable;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.bk.android.ui.widget.BaseTabHost;
import com.bk.android.ui.widget.TabHost;

public class ViewPagerTabHost extends BaseTabHost {
    private InteriorViewPager mViewPager;
    private FrameLayout mContentLayout;
    private OnPageChangeListener mOnPageChangeListener;
    private SlideTabWidget mSlideTabWidget;
    private InteriorPagerAdapter mInteriorPagerAdapter;
    private OnTabChangeListener mOnTabChangeListener;

    public ViewPagerTabHost(Context context) {
        super(context);
    }

    public ViewPagerTabHost(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onInit() {
        super.onInit();
        mInteriorPagerAdapter = new InteriorPagerAdapter(this);
        mViewPager = new InteriorViewPager(getContext());
        mViewPager.setId(mViewPager.hashCode());
        mViewPager.setAdapter(mInteriorPagerAdapter);
        mViewPager.setOnPageChangeListener(new OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                dispatchPageSelected(position);
            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                dispatchPageScrolled(position, positionOffset, positionOffsetPixels);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                dispatchPageScrollStateChanged(state);
            }
        });
        setOffscreenPageLimit(1);
    }

    public OnPageChangeListener getOnPageChangeListener() {
        return mOnPageChangeListener;
    }

    public void setOnPageChangeListener(OnPageChangeListener onPageChangeListener) {
        mOnPageChangeListener = onPageChangeListener;
    }

    public void setOffscreenPageLimit(int limit) {
        mViewPager.setOffscreenPageLimit(limit);
    }

    public int getOffscreenPageLimit() {
        return mViewPager.getOffscreenPageLimit();
    }

    public void setAdapter(AbsPagerTabAdapter adapter) {
        if (super.getTabContentView() == null) {
            setup();
        }
        mInteriorPagerAdapter.setAdapter(adapter);
    }

    public AbsPagerTabAdapter getAdapter() {
        if (mInteriorPagerAdapter != null) {
            return mInteriorPagerAdapter.getAdapter();
        }
        return null;
    }

    public ViewPager getViewPager() {
        return mViewPager;
    }

    @Override
    public FrameLayout getTabContentView() {
        return mContentLayout;
    }

    @Override
    public void setup() {
        super.setup();
        View view = findViewById(android.R.id.tabs);
        if (view instanceof SlideTabWidget) {
            mSlideTabWidget = (SlideTabWidget) view;
        }
        mContentLayout = (FrameLayout) findViewById(android.R.id.tabcontent);
        if (mContentLayout.getParent() != null) {
            int index = -1;
            for (int i = 0; i < getChildCount(); i++) {
                if (mContentLayout.equals(getChildAt(i))) {
                    index = i;
                    break;
                }
            }
            ViewGroup parent = (ViewGroup) mContentLayout.getParent();
            parent.removeView(mContentLayout);
            parent.addView(mViewPager, index, mContentLayout.getLayoutParams());
        }
    }

    public int getTabIndexByTag(String tag) {
        int i = -1;
        for (i = 0; i < mInteriorPagerAdapter.getCount(); i++) {
            if (mInteriorPagerAdapter.getTab(i).equals(tag)) {
                break;
            }
        }
        return i;
    }

    @Override
    protected void dispatchTabChanged(String tabId) {
        super.dispatchTabChanged(tabId);
        int position = getCurrentTab();
        mViewPager.setCurrentItem(position, false);
    }

    @Override
    public void setOnTabChangedListener(OnTabChangeListener l) {
        mOnTabChangeListener = l;
    }

    protected void dispatchPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        if (mOnPageChangeListener != null) {
            mOnPageChangeListener.onPageScrolled(position, positionOffset, positionOffsetPixels);
        }
        if (mSlideTabWidget != null) {
            mSlideTabWidget.dispatchPageScrolled(position, positionOffset, positionOffsetPixels);
        }
    }

    protected void dispatchPageSelected(int position) {
        if (mOnPageChangeListener != null) {
            mOnPageChangeListener.onPageSelected(position);
        }
        super.setCurrentTab(position);
    }

    protected void dispatchPageScrollStateChanged(int state) {
        if (mOnPageChangeListener != null) {
            mOnPageChangeListener.onPageScrollStateChanged(state);
        }
    }

    public void releaseRes() {
        mViewPager.setAdapter(null);
    }

    private class InteriorPagerAdapter extends AbsPagerTabAdapter {
        private AbsPagerTabAdapter mAdapter;
        private DataSetObserver mObserver = null;
        private TabHost mTabHost;
        private int mOldPosition = -1;

        public InteriorPagerAdapter(TabHost tabHost) {
            mTabHost = tabHost;
            mObserver = new DataSetObserver() {
                @Override
                public void onChanged() {
                    InteriorPagerAdapter.this.notifyDataSetChanged();
                    setupTabWidget(mAdapter);
                }
            };
        }

        private void setupTabWidget(AbsPagerTabAdapter adapter) {
            if (mTabHost == null) {
                return;
            }
            mTabHost.setCurrentTab(0);
            mTabHost.clearAllTabs();
            if (adapter == null) {
                return;
            }
            int size = adapter.getCount();
            for (int i = 0; i < size; i++) {
                mTabHost.addTab(mTabHost.newTabSpec(adapter.getTab(i)).setIndicator(adapter.getIndicator(i)).setContent(new TabContentFactory() {
                    @Override
                    public View createTabContent(String tag) {
                        return new View(getContext());
                    }
                }));
            }
        }

        public AbsPagerTabAdapter getAdapter() {
            return mAdapter;
        }

        public void setAdapter(AbsPagerTabAdapter adapter) {
            if (mAdapter != null) {
                mAdapter.unregisterObserver(mObserver);
            }
            mAdapter = adapter;
            if (mAdapter != null) {
                mAdapter.registerObserver(mObserver);
                mAdapter.notifyDataSetChanged();
            } else {
                setupTabWidget(mAdapter);
                this.notifyDataSetChanged();
            }
        }

        @Override
        public int getCount() {
            if (mAdapter != null) {
                return mAdapter.getCount();
            }
            return 0;
        }

        @Override
        public View getIndicator(int position) {
            if (mAdapter != null) {
                return mAdapter.getIndicator(position);
            }
            return null;
        }

        @Override
        public String getTab(int position) {
            if (mAdapter != null) {
                return mAdapter.getTab(position);
            }
            return null;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            if (mAdapter != null) {
                mAdapter.destroyItem(container, position, object);
                return;
            }
            super.destroyItem(container, position, object);
        }

        @Override
        public void finishUpdate(ViewGroup container) {
            if (mAdapter != null) {
                mAdapter.finishUpdate(container);
                return;
            }
            super.finishUpdate(container);
        }

        @Override
        public int getItemPosition(Object object) {
            if (mAdapter != null) {
                return mAdapter.getItemPosition(object);
            }
            return super.getItemPosition(object);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if (mAdapter != null) {
                return mAdapter.getPageTitle(position);
            }
            return super.getPageTitle(position);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            if (mAdapter != null) {
                return mAdapter.instantiateItem(container, position);
            }
            return super.instantiateItem(container, position);
        }

        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            if (mAdapter != null) {
                return mAdapter.isViewFromObject(arg0, arg1);
            }
            return false;
        }

        @Override
        public void notifyDataSetChanged() {
            super.notifyDataSetChanged();
        }

        @Override
        public void restoreState(Parcelable state, ClassLoader loader) {
            if (mAdapter != null) {
                mAdapter.restoreState(state, loader);
                return;
            }
            super.restoreState(state, loader);
        }

        @Override
        public Parcelable saveState() {
            if (mAdapter != null) {
                return mAdapter.saveState();
            }
            return super.saveState();
        }

        @Override
        public void setPrimaryItem(ViewGroup container, int position,
                                   Object object) {
            if (mAdapter != null) {
                mAdapter.setPrimaryItem(container, position, object);
                if (mOnTabChangeListener != null && position != mOldPosition) {
                    mOnTabChangeListener.onTabChanged(mAdapter.getTab(position));
                }
                mOldPosition = position;
                return;
            }
            super.setPrimaryItem(container, position, object);
        }

        @Override
        public void startUpdate(ViewGroup container) {
            if (mAdapter != null) {
                mAdapter.startUpdate(container);
                return;
            }
            super.startUpdate(container);
        }
    }

    public void setCanSlide(boolean canSlide) {
        if (mViewPager != null) {
            mViewPager.setCanSlide(canSlide);
        }
    }

    private class InteriorViewPager extends HorizontalChildViewPager {
        private boolean canSlide = true;

        public InteriorViewPager(Context context) {
            super(context);
        }

        @Override
        public void setVisibility(int visibility) {
        }

        public void setCanSlide(boolean canSlide) {
            this.canSlide = canSlide;
        }

        @Override
        public boolean onInterceptTouchEvent(MotionEvent ev) {
            if (!canSlide) {
                return false;
            }
            return super.onInterceptTouchEvent(ev);
        }

        @Override
        public boolean onTouchEvent(MotionEvent ev) {
            if (!canSlide) {
                return false;
            }
            return super.onTouchEvent(ev);
        }
    }
}
