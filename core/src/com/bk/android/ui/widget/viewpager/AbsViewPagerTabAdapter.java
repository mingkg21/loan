package com.bk.android.ui.widget.viewpager;

import android.os.Parcelable;
import android.view.View;
import android.view.ViewGroup;

import com.bk.android.ui.widget.viewpager.ViewPagerAdapterManager.IViewPagerAdapter;

public abstract class AbsViewPagerTabAdapter extends AbsPagerTabAdapter implements IViewPagerAdapter{
	private ViewPagerAdapterManager mViewPagerAdapterManager = new ViewPagerAdapterManager(this);
	
	@Override
	public void startUpdate(ViewGroup container) {
		mViewPagerAdapterManager.startUpdate(container);
	}

	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		return mViewPagerAdapterManager.instantiateItem(container, position);
	}

	@Override
	public void setPrimaryItem(ViewGroup container, int position, Object object) {
		mViewPagerAdapterManager.setPrimaryItem(container, position, object);
	}
	
	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		mViewPagerAdapterManager.destroyItem(container, position, object);
	}

	@Override
	public void finishUpdate(final ViewGroup container) {
		mViewPagerAdapterManager.finishUpdate(container);
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return mViewPagerAdapterManager.isViewFromObject(view, object);
	}

	@Override
	public Parcelable saveState() {
		return mViewPagerAdapterManager.saveState();
	}

	@Override
	public void restoreState(Parcelable state, ClassLoader loader) {
		mViewPagerAdapterManager.restoreState(state, loader);
	}
}
