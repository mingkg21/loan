package com.bk.android.ui.widget.viewpager;

import java.util.ArrayList;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;

public class FragmentPagerAdapterManager {
	private static final String TAG = "FragmentStatePagerAdapter";
    private static final boolean DEBUG = false;

    private final FragmentManager mFragmentManager;
    private FragmentTransaction mCurTransaction = null;

    private ArrayList<Fragment.SavedState> mSavedState = new ArrayList<Fragment.SavedState>();
    private ArrayList<Fragment> mFragments = new ArrayList<Fragment>();
    private Fragment mCurrentPrimaryItem = null;
	private SparseArray<Fragment> mItemCreatedFragments = new SparseArray<Fragment>();
	private SparseArray<Fragment> mItemResumeItemFragments = new SparseArray<Fragment>();

	private IFragmentPagerAdapter mFragmentPagerAdapter;
	private LifeCycleRunnable mLifeCycleRunnable;
	
	public FragmentPagerAdapterManager(FragmentManager fm,IFragmentPagerAdapter fragmentPagerAdapter) {
        mFragmentManager = fm;
        mFragmentPagerAdapter = fragmentPagerAdapter;
    }
	
    public void startUpdate(ViewGroup container) {
    }

    public Object instantiateItem(ViewGroup container, int position) {
        if (mFragments.size() > position) {
            Fragment f = mFragments.get(position);
            if (f != null) {
                return f;
            }
        }

        if (mCurTransaction == null) {
            mCurTransaction = mFragmentManager.beginTransaction();
        }

        Fragment fragment = mFragmentPagerAdapter.getItem(position);
        if(fragment instanceof ViewPagerItem){
			((ViewPagerItem) fragment).setLifeCycleEnabled(true);
		}
        if (DEBUG) Log.v(TAG, "Adding item #" + position + ": f=" + fragment);
        if (mSavedState.size() > position) {
            Fragment.SavedState fss = mSavedState.get(position);
            if (fss != null) {
                fragment.setInitialSavedState(fss);
            }
        }
        while (mFragments.size() <= position) {
            mFragments.add(null);
        }
        fragment.setMenuVisibility(false);
        fragment.setUserVisibleHint(false);
        mFragments.set(position, fragment);
        mCurTransaction.add(container.getId(), fragment , mFragmentPagerAdapter.getFragmentTag(position));
        return fragment;
    }

    public void destroyItem(ViewGroup container, int position, Object object) {
        Fragment fragment = (Fragment)object;

        if (mCurTransaction == null) {
            mCurTransaction = mFragmentManager.beginTransaction();
        }
        if (DEBUG) Log.v(TAG, "Removing item #" + position + ": f=" + object
                + " v=" + ((Fragment)object).getView());
        while (mSavedState.size() <= position) {
            mSavedState.add(null);
        }
        mSavedState.set(position, mFragmentManager.saveFragmentInstanceState(fragment));
        mFragments.set(position, null);
        Fragment createdFragment = mItemCreatedFragments.get(position);
		if(createdFragment instanceof ViewPagerItem){
			((ViewPagerItem) createdFragment).onDestroyItem();
		}
		mItemCreatedFragments.remove(position);
        mCurTransaction.remove(fragment);
    }

    public void setPrimaryItem(ViewGroup container,final int position, Object object) {
    	final Fragment fragment = (Fragment)object;
        if(fragment == null || !fragment.isAdded() || container == null){
        	return;
        }
        if(mLifeCycleRunnable != null){
            container.removeCallbacks(mLifeCycleRunnable);
        }
        
        if (fragment != mCurrentPrimaryItem) {
        	Fragment oldFragment = mCurrentPrimaryItem;
            if (mCurrentPrimaryItem != null) {
                mCurrentPrimaryItem.setMenuVisibility(false);
                mCurrentPrimaryItem.setUserVisibleHint(false);
            }
            if (fragment != null) {
                fragment.setMenuVisibility(true);
                fragment.setUserVisibleHint(true);
            }
            mCurrentPrimaryItem = fragment;
            if(mLifeCycleRunnable != null){
            	mLifeCycleRunnable.forceRun();
            }
            
            //取消延迟执行
            mLifeCycleRunnable = new LifeCycleRunnable(position,oldFragment, mCurrentPrimaryItem);
            mLifeCycleRunnable.run();
        }
        
        if(mLifeCycleRunnable != null){
        	container.postDelayed(mLifeCycleRunnable,10);
        }
    }

    public void finishUpdate(ViewGroup container) {
    	try {
    		if (mCurTransaction != null) {
                mCurTransaction.commitAllowingStateLoss();
                mCurTransaction = null;
                mFragmentManager.executePendingTransactions();
            }
		} catch (Exception e) {}
    }
    
    public void dispatchOnResume(){
    	for (int i = 0; i < mItemCreatedFragments.size(); i++) {
    		Fragment fragment = mItemCreatedFragments.valueAt(i);
    		int key = mItemCreatedFragments.keyAt(i);
    		if(mItemResumeItemFragments.indexOfValue(fragment) == -1 && fragment.getUserVisibleHint()){
        		((ViewPagerItem) fragment).onResumeItem();
        		mItemResumeItemFragments.put(key, fragment);
    		}
		}
    }
    
    public void dispatchOnPause(){
    	for (int i = 0; i < mItemCreatedFragments.size(); i++) {
    		Fragment fragment = mItemCreatedFragments.valueAt(i);
    		int key = mItemCreatedFragments.keyAt(i);
    		if(mItemResumeItemFragments.indexOfValue(fragment) != -1){
        		((ViewPagerItem) fragment).onPauseItem();
        		mItemResumeItemFragments.delete(key);
    		}
		}
    }
    
    public boolean isViewFromObject(View view, Object object) {
        return ((Fragment)object).getView() == view;
    }
    
    interface IFragmentPagerAdapter{
        public Fragment getItem(int position);
        public String getFragmentTag(int position);
    }
    
    private class LifeCycleRunnable implements Runnable{
    	private boolean isForce;
    	private int mPosition;
    	private Fragment mOldFragment;
    	private Fragment mNewFragment;
    	private LifeCycleRunnable(int position,Fragment oldFragment,Fragment newFragment){
    		mOldFragment = oldFragment;
    		mNewFragment = newFragment;
    		mPosition = position;
    	}
    	
    	private void forceRun(){
    		isForce = true;
    		run();
    	}
    	
		@Override
		public void run() {
			if(mOldFragment instanceof ViewPagerItem){
				int index = mItemResumeItemFragments.indexOfValue(mOldFragment);
				if(index != -1){
			    	((ViewPagerItem) mOldFragment).onPauseItem();
			    	int key = mItemResumeItemFragments.keyAt(index);
			    	mItemResumeItemFragments.remove(key);
				}
			}
			
	        Fragment createdFragment = mItemCreatedFragments.get(mPosition);
	        if(mNewFragment instanceof ViewPagerItem){
	        	if(createdFragment == null && !isForce){
					if(!((ViewPagerItem) mNewFragment).onCreateItem()){
						mItemCreatedFragments.put(mPosition,mNewFragment);
						createdFragment = mNewFragment;
					}
				}
			    if(createdFragment != null){
			    	((ViewPagerItem) mNewFragment).onResumeItem();
			    	mItemResumeItemFragments.put(mPosition, createdFragment);
				}
	        }
		    mLifeCycleRunnable = null;
		}
    }
}
