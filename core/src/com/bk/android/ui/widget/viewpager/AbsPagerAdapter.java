package com.bk.android.ui.widget.viewpager;

import android.database.DataSetObservable;
import android.database.DataSetObserver;
import android.support.v4.view.PagerAdapter;

public abstract class AbsPagerAdapter extends PagerAdapter {
	private DataSetObservable mObservable = new DataSetObservable();
	
    void registerObserver(DataSetObserver observer) {
        mObservable.registerObserver(observer);
    }
    
    void unregisterObserver(DataSetObserver observer) {
        mObservable.unregisterObserver(observer);
    }
    
	@Override
    public void notifyDataSetChanged() {
        mObservable.notifyChanged();
        super.notifyDataSetChanged();
    }
}
