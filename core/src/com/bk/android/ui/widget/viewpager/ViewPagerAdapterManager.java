package com.bk.android.ui.widget.viewpager;

import java.lang.ref.WeakReference;

import android.os.Parcelable;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;

import com.bk.android.util.LogUtil;

class ViewPagerAdapterManager {
	private static final String TAG = AbsViewPagerTabAdapter.class.getSimpleName();
	private WeakReference<ViewGroup> mContainer;
	private SparseArray<View> mItemCreatedViews = new SparseArray<View>();
    private View mCurrentPrimaryItem = null;
    private IViewPagerAdapter mViewPagerAdapter;
	private LifeCycleRunnable mLifeCycleRunnable;

    ViewPagerAdapterManager(IViewPagerAdapter viewPagerAdapter){
    	mViewPagerAdapter = viewPagerAdapter;
    }
    
	public void startUpdate(ViewGroup container) {
		LogUtil.v(TAG, "startUpdate");
		if(mContainer == null || mContainer.get() == null){
			mContainer = new WeakReference<ViewGroup>(container);
		}
	}

	public Object instantiateItem(ViewGroup container, int position) {
		LogUtil.i(TAG, "instantiateItem : position="+position);
		View contentView = mViewPagerAdapter.getItemView(container,position);
		if(contentView.getParent() == null){
			container.addView(contentView);
		}else if(!contentView.getParent().equals(container)){
			((ViewGroup)contentView.getParent()).removeView(contentView);
			container.addView(contentView);
		}
		if(contentView instanceof ViewPagerItem){
			((ViewPagerItem) contentView).setLifeCycleEnabled(true);
		}
		return contentView;
	}

	public void setPrimaryItem(ViewGroup container, int position, Object object) {
		LogUtil.v(TAG, "setPrimaryItem : position="+position+" object="+object);
		if(object == null){
			return;
		}
		View contentView = (View) object;
		if(mLifeCycleRunnable != null){
            container.removeCallbacks(mLifeCycleRunnable);
        }
		
		if (contentView != mCurrentPrimaryItem) {
			View oldView = mCurrentPrimaryItem;
            mCurrentPrimaryItem = contentView;
            if(mLifeCycleRunnable != null){
            	mLifeCycleRunnable.forceRun();
            }
            mLifeCycleRunnable = new LifeCycleRunnable(position,oldView, mCurrentPrimaryItem);
        }
		
		if(mLifeCycleRunnable != null){
        	container.postDelayed(mLifeCycleRunnable, 500);
        }
	}
	
	public void destroyItem(ViewGroup container, int position, Object object) {
		LogUtil.i(TAG, "destroyItem : position="+position+" object="+object);
		if(object == null){
			return;
		}
		View contentView = (View) object;
		View view = mItemCreatedViews.get(position);
		if(view instanceof ViewPagerItem){
			((ViewPagerItem) view).onDestroyItem();
		}
		mItemCreatedViews.remove(position);
		if(contentView.getParent() != null){
			((ViewGroup)contentView.getParent()).removeView(contentView);
		}
	}

	public void finishUpdate(final ViewGroup container) {
		LogUtil.v(TAG, "finishUpdate");
	}
	
	public boolean isViewFromObject(View view, Object object) {
		return view.equals(object);
	}

	public Parcelable saveState() {
		return null;
	}

	public void restoreState(Parcelable state, ClassLoader loader) {
	}
	
    interface IViewPagerAdapter{
    	public View getItemView(ViewGroup container,int position);
    }
    
    private class LifeCycleRunnable implements Runnable{
    	private boolean isForce;
    	private int mPosition;
    	private View mOldView;
    	private View mNewView;
    	private LifeCycleRunnable(int position,View oldView,View newView){
    		mOldView = oldView;
    		mNewView = newView;
    		mPosition = position;
    	}
    	
    	private void forceRun(){
    		isForce = true;
    		run();
    	}
    	
		@Override
		public void run() {
		    if(mOldView instanceof ViewPagerItem){
				if(mItemCreatedViews.indexOfValue(mOldView) != -1){
			    	((ViewPagerItem) mOldView).onPauseItem();
				}
			}
			
		    View createdView = mItemCreatedViews.get(mPosition);
	        if(mNewView instanceof ViewPagerItem){
	        	if(createdView == null && !isForce){
					if(!((ViewPagerItem) mNewView).onCreateItem()){
						mItemCreatedViews.put(mPosition,mNewView);
						createdView = mNewView;
					}
				}
			    if(createdView != null){
			    	((ViewPagerItem) mNewView).onResumeItem();
				}
	        }
		    mLifeCycleRunnable = null;
		}
    }
}
