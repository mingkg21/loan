package com.bk.android.ui.widget.viewpager;

import android.view.View;

public abstract class AbsPagerTabAdapter extends AbsPagerAdapter {
	public abstract View getIndicator(int position);
	public abstract String getTab(int position);
}
