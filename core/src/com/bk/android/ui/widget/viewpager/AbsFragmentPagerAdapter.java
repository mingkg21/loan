package com.bk.android.ui.widget.viewpager;

import android.support.v4.app.FragmentManager;
import android.view.View;
import android.view.ViewGroup;

import com.bk.android.ui.widget.viewpager.FragmentPagerAdapterManager.IFragmentPagerAdapter;

public abstract class AbsFragmentPagerAdapter extends AbsPagerAdapter implements IFragmentPagerAdapter {
    private FragmentPagerAdapterManager mFragmentPagerAdapterManager;

	public AbsFragmentPagerAdapter(FragmentManager fm) {
        mFragmentPagerAdapterManager = new FragmentPagerAdapterManager(fm, this);
    }

    @Override
    public void startUpdate(ViewGroup container) {
    	mFragmentPagerAdapterManager.startUpdate(container);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
    	return mFragmentPagerAdapterManager.instantiateItem(container, position);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
    	mFragmentPagerAdapterManager.destroyItem(container, position, object);
    }

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
    	mFragmentPagerAdapterManager.setPrimaryItem(container, position, object);
    }

    @Override
    public void finishUpdate(ViewGroup container) {
    	mFragmentPagerAdapterManager.finishUpdate(container);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return mFragmentPagerAdapterManager.isViewFromObject(view, object);
    }

    public void dispatchOnResume(){
    	mFragmentPagerAdapterManager.dispatchOnResume();
    }
    
    public void dispatchOnPause(){
    	mFragmentPagerAdapterManager.dispatchOnPause();
    }
    
    @Override
	public String getFragmentTag(int position) {
		return "tag="+position;
	}
}
