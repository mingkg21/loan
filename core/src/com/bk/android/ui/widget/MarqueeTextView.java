package com.bk.android.ui.widget;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.TextView;
/**
 * 跑马灯
 * @author linyiwei
 */
public class MarqueeTextView extends TextView{
	public MarqueeTextView(Context context) {
		this(context,null);
	}

	public MarqueeTextView(Context context, AttributeSet attrs) {
		this(context, attrs,android.R.attr.textViewStyle);
	}
	
	public MarqueeTextView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}
	
	private void init(){
		setEllipsize(TextUtils.TruncateAt.MARQUEE);
	}
	
	@Override
	public void setSelected(boolean selected) {
		super.setSelected(true);
	}

	@Override
	protected void onAttachedToWindow() {
		super.onAttachedToWindow();
		super.setSelected(true);
	}

	@Override
	protected void onDetachedFromWindow() {
		super.onDetachedFromWindow();
		super.setSelected(false);
	}
}
