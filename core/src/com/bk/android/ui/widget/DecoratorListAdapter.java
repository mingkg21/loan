package com.bk.android.ui.widget;

import android.widget.ListAdapter;


public class DecoratorListAdapter extends DecoratorAdapter<ListAdapter> {
	public DecoratorListAdapter(ListAdapter adapter) {
		super(adapter);
	}

	@Override
	public boolean areAllItemsEnabled() {
		if(mAdapter == null){
			return super.areAllItemsEnabled();
		}
		return mAdapter.areAllItemsEnabled();
	}

	@Override
	public boolean isEnabled(int position) {
		if(mAdapter == null){
			return super.isEnabled(position);
		}
		return mAdapter.isEnabled(position);
	}
}
