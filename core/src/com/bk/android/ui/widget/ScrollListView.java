package com.bk.android.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ListAdapter;
import android.widget.ListView;

/** 在ScrollView中能显示的ListdView
 * @author mingkg21
 * @date 2011-7-31
 * @email mingkg21@gmail.com
 */
public class ScrollListView extends ListView {
	private DecoratorListAdapter mInteriorAdapter;

	public ScrollListView(Context context) {
		super(context);
		init();
	}
	
	public ScrollListView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public ScrollListView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2, MeasureSpec.AT_MOST);
		super.onMeasure(widthMeasureSpec, expandSpec);
	}
	
	private void init(){
		mInteriorAdapter = new DecoratorListAdapter(null);
		super.setAdapter(mInteriorAdapter);
		setFocusable(false);
		setLongClickable(true);
	}

	@Override
	public void setAdapter(ListAdapter adapter) {
		ListAdapter oldAdapter = mInteriorAdapter.getAdapter();
		mInteriorAdapter.setAdapter(adapter);
		if(oldAdapter == null || adapter == null || !oldAdapter.getClass().equals(adapter.getClass())){
			super.setAdapter(mInteriorAdapter);
		}
	}
	
	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		if(isDuplicateParentStateEnabled()){
			return false;
		}
		return super.dispatchTouchEvent(ev);
	}
}
